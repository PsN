DIAGRAMS=llp.dia tool.dia model.dia output.dia data.dia xrsl_file.dia fcon.dia nonmem.dia moshog_client.dia

DIAGRAMS=$(addprefix diagrams/,$(DIAGRAMS))

BIN=bootstrap cdd execute llp scm

HTML_STUBS=_synopsis.php _description.php _options.php _examples.php

DOCUMENTS=$(foreach pre,$(BIN),$(foreach suff,$(HTML_STUBS),$(addprefix html/$(pre),$(suff))))

DIA2CODE=dia2code
FILLSCRIPT=perl ./bin/fill_diacode.pl
DIRPM=libgen/
DIRSUBS=lib/
PERLDIRS=-I/home/pontus/perl
LIBFILES=debug.pm \
	ui.pm \
	status_bar.pm \
	nonmem.pm \
	moshog_client.pm \
	file.pm \
	data/individual.pm \
	data.pm \
	output/problem/subproblem.pm \
	output/problem.pm \
	output.pm \
	grid/nordugrid/xrsl_file.pm \
	fcon.pm \
	table_file.pm \
	extra_data.pm \
	model/problem/record/option.pm \
	model/problem/record/init_option.pm \
	model/problem/record/theta_option.pm \
	model/problem/record.pm \
        model/problem/code_record.pm \
	model/problem/init_record.pm \
        model/problem/abbreviated.pm \
	model/problem/aes.pm \
        model/problem/aesinit.pm \
	model/problem/contr.pm \
        model/problem/covariance.pm \
	model/problem/data.pm \
        model/problem/des.pm \
	model/problem/error.pm \
        model/problem/estimation.pm \
	model/problem/infn.pm \
        model/problem/input.pm \
	model/problem/msfi.pm \
        model/problem/mix.pm \
	model/problem/model.pm \
	model/problem/nonparametric.pm \
        model/problem/omega.pm \
	model/problem/pk.pm \
        model/problem/prior.pm \
        model/problem/problem.pm \
	model/problem/pred.pm \
        model/problem/scatter.pm \
	model/problem/sigma.pm \
        model/problem/simulation.pm \
	model/problem/subroutine.pm \
	model/problem/table.pm \
	model/problem/theta.pm \
        model/problem/tol.pm \
	model/cwres_module.pm \
	model/mirror_plot_module.pm \
	model/iofv_module.pm \
	model/shrinkage_module.pm \
	model/nonparametric_module.pm \
	model/problem.pm \
	model.pm \
	tool.pm \
	tool/modelfit.pm \
	tool/llp.pm \
	tool/cdd.pm \
	tool/cdd/jackknife.pm \
	tool/sse.pm \
	tool/npc.pm \
	tool/pind.pm \
	tool/bootstrap.pm \
	tool/nonpb.pm \
	tool/scm/config_file.pm \
	tool/scm.pm \
	tool/xv_step.pm \
	tool/xv.pm \

PERLFILES=$(addprefix lib/,$(LIBFILES))

RELFILES=$(addprefix PsN-Source/lib/,$(LIBFILES)) \
	$(addprefix PsN-Source/, \
	lib/PsN_template.pm \
	lib/common_options.pm \
	lib/psn.conf \
	lib/OSspecific.pm \
	lib/hotkey.pm \
	lib/ext/Math/SigFigs.pm \
	lib/ext/Math/MatrixReal.pm \
	lib/ext/Statistics/Distributions.pm \
	lib/ext/Parallel/ForkManager.pm \
	lib/ext/Config/Tiny.pm \
	lib/ext/Color/Output.pm \
	lib/ext/File/HomeDir.pm \
	lib/ext/IPC/Run3.pm \
	lib/matlab/bca.m \
	lib/matlab/histograms.m \
	lib/matlab/profiles.m \
	lib/R-scripts/llp.R \
	lib/R-scripts/cdd.R \
	lib/R-scripts/bootstrap.R \
	bin/execute \
	bin/cdd \
	bin/mc_cdd \
	bin/sse \
	bin/pind \
	bin/nonpb \
	bin/npc \
	bin/vpc \
	bin/llp \
	bin/scm \
	bin/bootstrap \
	bin/sumo \
	bin/single_valued_columns \
	bin/check_termination \
	bin/create_extra_data_model \
	bin/create_cont_model \
	bin/create_cont_data \
	bin/create_subsets \
	bin/unwrap_data \
	bin/data_stats \
	bin/gam42toconf \
	bin/se_of_eta \
	bin/update_inits \
	doc/SSE_userguide.pdf \
	doc/NPC_VPC_userguide.pdf \
	setup.pl \
	README.txt )

GENFILES=$(addprefix libgen/,$(LIBFILES))

all: libgen $(PERLFILES)

.PRECIOUS: $(GENFILES)

libgen:
	@ mkdir -p libgen

lib/%.pm: libgen/%.pm lib/%_subs.pm
	 @ cp libgen/$*.pm libgen/$*_temp.pm
	 @ $(FILLSCRIPT) libgen/$*_temp.pm lib/$*_subs.pm
	 @ cp libgen/$*_temp.pm $@
	 @ perl $(PERLDIRS) -I./$(@D) -I./lib -c $@ -W -t -T|| (rm $@ && false)

.PHONY : clean

clean:
	@-rm -rf $(PERLFILES) $(DOCUMENTS) libgen PsN-Source

libgen/debug.pm : diagrams/debug.dia
	$(DIA2CODE) -t perl -d $(DIRPM) diagrams/debug.dia

libgen/ui.pm : diagrams/ui.dia
	$(DIA2CODE) -t perl -d $(DIRPM) diagrams/ui.dia

libgen/status_bar.pm : diagrams/status_bar.dia
	$(DIA2CODE) -t perl -d $(DIRPM) diagrams/status_bar.dia

libgen/nonmem.pm : diagrams/nonmem.dia
	$(DIA2CODE) -t perl -d $(DIRPM) diagrams/nonmem.dia	

libgen/moshog_client.pm : diagrams/moshog_client.dia
	$(DIA2CODE) -t perl -d $(DIRPM) diagrams/moshog_client.dia

libgen/file.pm : diagrams/file.dia
	$(DIA2CODE) -t perl -d $(DIRPM) diagrams/file.dia

libgen/tool/modelfit.pm : diagrams/modelfit.dia
	$(DIA2CODE) -t perl -d $(DIRPM) diagrams/modelfit.dia

libgen/tool/llp.pm : diagrams/llp.dia
	$(DIA2CODE) -t perl -d $(DIRPM) diagrams/llp.dia

libgen/tool/cdd/jackknife.pm libgen/tool/cdd.pm : diagrams/cdd.dia
	$(DIA2CODE) -t perl -d $(DIRPM) diagrams/cdd.dia

libgen/tool/sse.pm : diagrams/sse.dia
	$(DIA2CODE) -t perl -d $(DIRPM) diagrams/sse.dia

libgen/tool/npc.pm : diagrams/npc.dia
	$(DIA2CODE) -t perl -d $(DIRPM) diagrams/npc.dia

libgen/tool/pind.pm : diagrams/pind.dia
	$(DIA2CODE) -t perl -d $(DIRPM) diagrams/pind.dia

libgen/tool/nonpb.pm : diagrams/nonpb.dia
	$(DIA2CODE) -t perl -d $(DIRPM) diagrams/nonpb.dia

libgen/tool/bootstrap.pm : diagrams/bootstrap.dia
	$(DIA2CODE) -t perl -d $(DIRPM) diagrams/bootstrap.dia

libgen/tool/scm/config_file.pm libgen/tool/scm.pm : diagrams/scm.dia
	$(DIA2CODE) -t perl -d $(DIRPM) diagrams/scm.dia

libgen/tool/xv_step.pm libgen/tool/xv.pm : diagrams/xv.dia
	$(DIA2CODE) -t perl -d $(DIRPM) diagrams/xv.dia

libgen/tool.pm : diagrams/tool.dia
	$(DIA2CODE) -t perl -d $(DIRPM) diagrams/tool.dia

libgen/table_file.pm libgen/extra_data.pm libgen/model%.pm : diagrams/model.dia
	$(DIA2CODE) -t perl -d $(DIRPM) diagrams/model.dia

libgen/output%.pm : diagrams/output.dia
	$(DIA2CODE) -t perl -d $(DIRPM) diagrams/output.dia

libgen/data%.pm : diagrams/data.dia
	$(DIA2CODE) -t perl -d $(DIRPM) diagrams/data.dia

libgen/grid/nordugrid/xrsl_file.pm : diagrams/xrsl_file.dia
	$(DIA2CODE) -t perl -d $(DIRPM) diagrams/xrsl_file.dia

libgen/fcon.pm : diagrams/fcon.dia
	$(DIA2CODE) -nd -t perl -d $(DIRPM) diagrams/fcon.dia

documents: $(DOCUMENTS)

$(addprefix html/bootstrap,$(HTML_STUBS)) : bin/bootstrap lib/common_options.pm
	perl $< --help --html

$(addprefix html/cdd,$(HTML_STUBS)) : bin/cdd lib/common_options.pm
	perl $< --help --html

$(addprefix html/execute,$(HTML_STUBS)) : bin/execute lib/common_options.pm
	perl $< --help --html

$(addprefix html/llp,$(HTML_STUBS)) : bin/llp lib/common_options.pm
	perl $< --help --html

$(addprefix html/scm,$(HTML_STUBS)) : bin/scm lib/common_options.pm
	perl $< --help --html


release: libgen rel_dir $(RELFILES) documents
	@ cp -ar modules/Math-Random \
		 modules/Math-Random-Win \
		 modules/Storable PsN-Source/modules
	@ zip -r PsN-Source PsN-Source/
	@ tar czf PsN-Source.tar.gz PsN-Source/

PsN-Source/setup.pl: bin/setup.pl
	@ cp bin/setup.pl $@

PsN-Source/lib/matlab/% : matlab/%
	@ cp matlab/$* $@

PsN-Source/lib/R-scripts/% : R-scripts/%
	@ cp R-scripts/$* $@

PsN-Source/%: %
	@ cp -ar $* $@

rel_dir:
	@mkdir -p $(sort $(dir $(RELFILES))) PsN-Source/modules

