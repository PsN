use strict;
use Config;
use CPAN;
use File::Spec;

my $version = '2.2.6_rc2';

my $name_safe_version = $version;
$name_safe_version =~ s/\./_/g;
my @utilities = ('bootstrap', 'cdd', 'execute', 'llp', 'scm', 'sumo', 'sse', 'mc_cdd',
                'data_stats', 'create_extra_data_model', 'single_valued_columns',
                'gam42toconf', 'create_cont_model', 'create_cont_data', 'unwrap_data', 'create_subsets',
                'check_termination','se_of_eta','update_inits','npc','vpc','pind','nonpb' );

my @win_modules = ('Math::Random');
my @nix_modules = ('Math::Random','Storable');

my @modules;

sub confirm {
 my $input = <STDIN>;
 if( $input =~ /^\s*[yY]\s*$/ ){
   return 1;
 }
 return 0;
}

sub get_input {
 my $default = shift;
 my $input = <STDIN>;
 chomp( $input );
 if( $input =~ /^\s*$/ ){
   return( $default );
 } else {
   return( $input );
 }

}

$| = 1; # Make sure autoflush is on
$, = ',';
print "\nThis is the PsN installer. I will install version $version of PsN Core,
Toolkit and the following utilities: \n", @utilities, ".
You will be presented with a few questions. Just pressing ENTER means
that you accept the default values, except for YES and NO questions,
in which you must be explicit.\n\n";
$, ='';

if( $Config{osname} eq 'linux' ){
 my $user = `whoami`;
 chomp($user);
 unless( $user =~ /^root$/ ){
   print "Hi $user, you don't look like root. You need root privileges to install PsN systemwide. Would you like to try anyway [y/n] ?\n";
   exit unless( confirm() );
 }
}

my $binary_dir;
my $library_dir;
my $perl_binary;

print "PsN Utilities installation directory [$Config{bin}]:";

$binary_dir = get_input( $Config{bin} );

unless( -e $binary_dir ){
 print "Directory $binary_dir does not exist! Would you like to create it?[y/n]\n";
 if( confirm() ){
   unless( mkdir( $binary_dir ) ){ print "Unable to create $binary_dir!!\n"; die };
 } else {
   exit;
 }
}

print "Path to perl binary used to run Utilities [$Config{perlpath}]:";

$perl_binary = get_input( $Config{perlpath} );

print "PsN Core and Toolkit installation directory [$Config{sitelib}]:";

$library_dir = get_input( $Config{sitelib} );

unless( -e $library_dir ){
 print "Directory $library_dir does not exist! Would you like to create it? [y/n]\n";
 if( confirm() ){
   unless( mkdir( $library_dir ) ){ print "Unable to create $library_dir!!\n"; die };
 } else {
   exit;
 }
}

my $overwrite = 0;
my $old_version = 'X_X_X';
my $keep_conf = 0;

unless( mkdir( "$library_dir/PsN_$name_safe_version" ) ){
 print "Failed to create $library_dir/PsN_$name_safe_version: $!\n";
 print "PsN is probably already installed. Would you like to continue anyway [y/n] ?";
 exit unless( confirm() );
}

if( -e "$library_dir/PsN_$name_safe_version/psn.conf" ){
 print "An older version of psn.conf exists in $library_dir/PsN_$name_safe_version.\n";
 print "Keep the old version [y/n] ?";
 $keep_conf = confirm();
}

my $copy_cmd;
my $copy_recursive_cmd;

if( $Config{osname} eq 'MSWin32' ){
 $copy_cmd = "copy /Y";
 $copy_recursive_cmd = "xcopy /Y /E";
 @modules = @win_modules;
} else {
 @modules = @nix_modules;
 $copy_cmd = "cp";
 $copy_recursive_cmd = "cp -r";
}

system( $copy_cmd . " \"" . File::Spec -> catfile( $library_dir, "PsN_$name_safe_version", "psn.conf" ) . "\" old.conf" ) if $keep_conf;
system( $copy_recursive_cmd . " " . File::Spec -> catfile( "lib", "*" ) . " \"" . File::Spec -> catdir( $library_dir, "PsN_$name_safe_version" ) . "\"" );
system( $copy_cmd . " old.conf \"" . File::Spec -> catfile( $library_dir, "PsN_$name_safe_version", "psn.conf" ) . "\"" ) if $keep_conf;

my $confirmed = 0;
foreach my $file ( @utilities ){

 #system( $copy_cmd . " " . File::Spec -> catfile( "bin", $file ) . " " . File::Spec -> catfile( $binary_dir, "$file-$version" ) );

 open( INST , "<" , File::Spec -> catfile( "bin", $file ) ) or die "Unable to install $file!\n";
 open( UTIL , ">" , File::Spec -> catfile( $binary_dir, "$file-$version" ) ) or die "Unable to install $file!\n";
 my $replace_line_found = 0;
 while( <INST> ){
   if( /\# Everything above this line will be replaced \#/ ){
     print UTIL "\#!" , $perl_binary , "\n";
     print UTIL "use lib '$library_dir';\n\n";
     print UTIL "\# Everything above this line was entered by the setup script \#\n";
     $replace_line_found = 1;
   } elsif( /use PsN/ ){
     print UTIL "use PsN_$name_safe_version;\n";
   } elsif( /require PsN/ ){
     print UTIL "require PsN_$name_safe_version;\n";
   } elsif( $replace_line_found ){
     print UTIL $_;
   }
 }
 close UTIL;
 close INST;

 chmod( 0755, File::Spec -> catfile( $binary_dir, "$file-$version" ) );

 if( -e "$binary_dir/$file" ){

   if( $Config{osname} ne 'MSWin32' ){
     my $link = readlink( "$binary_dir/execute" );
     if( $old_version eq 'X_X_X' and not($link eq '') ) {
       $link =~ /-(\d+\.\d+\.\d+)/;
       $old_version = $1;
     }
   }
   next if( $old_version eq $version );

   if( not $confirmed ){

     print( "\nAn older version($old_version) of PsN is installed. Would you like to\n",
            "make this version ($version) the default?  Your old version will not be\n",
            "removed - it will be available as\n",
            "[bootstrap|cdd|execute|llp|scm|sumo]-$old_version [y/n]" );

     $confirmed = 1;
     $overwrite = confirm();
   }
   if( $overwrite ){
     unlink( "$binary_dir/$file" );
     if( $Config{osname} eq 'MSWin32' ){
       system( "copy /Y \"$binary_dir\\$file-$version\" \"$binary_dir\\$file\"" );
       system( "copy /Y \"$Config{bin}\\runperl.bat\" \"$binary_dir\\$file.bat\"" );
     } else {
       symlink( "$binary_dir/$file-$version", "$binary_dir/$file" );
     }
   }
 } else {

   if( $Config{osname} eq 'MSWin32' ){
     system( "copy /Y \"$binary_dir\\$file-$version\" \"$binary_dir\\$file\"" );
     system( "copy /Y \"$Config{bin}\\runperl.bat\" \"$binary_dir\\$file.bat\"" );
   } else {
     symlink( "$binary_dir/$file-$version", "$binary_dir/$file" );
   }
 }
}

open( TEMPLATE, "lib/PsN_template.pm" ) or die "Unable to open PsN_template.pm in $library_dir: $!\n";
open( PSN, '>', "$library_dir" . "/PsN_$name_safe_version.pm" ) or die "Unable to install PsN-$name_safe_version.pm in $library_dir: $!\n";

if( $Config{osname} eq 'MSWin32' ){
  require Win32;
  $library_dir = Win32::GetShortPathName($library_dir);
}

print( PSN "package PsN;\n" );
print( PSN "use lib '$library_dir/PsN_$name_safe_version';\n" );
print( PSN "\$lib_dir = '$library_dir/PsN_$name_safe_version';\n" );
print( PSN "\$config_file = '$library_dir/PsN_$name_safe_version/psn.conf';\n" );
print( PSN "\$version = '$version';\n" );

for ( <TEMPLATE> ) {
 print PSN $_;
}
close( PSN );
close( TEMPLATE );


if( -e "$library_dir/PsN.pm" ){
 if( $overwrite ){
   unlink( "$library_dir/PsN.pm" );
   if( $Config{osname} eq 'MSWin32' ){
     system( "copy \"$library_dir\\PsN_$name_safe_version.pm\" \"$library_dir\\PsN.pm\"" );
   } else {
     symlink( "$library_dir/PsN_$name_safe_version.pm", "$library_dir/PsN.pm" );
   }
 }
} else {
 if( $Config{osname} eq 'MSWin32' ){
   system("copy \"$library_dir\\PsN_$name_safe_version.pm\" \"$library_dir\\PsN.pm\"" );
 } else {
   symlink( "$library_dir/PsN_$name_safe_version.pm", "$library_dir/PsN.pm" );
 }
}

open( PSN, '<', "$library_dir" . "/PsN_$name_safe_version/nonmem.pm" ) or die "Unable to install PsN-$name_safe_version/nonmem.pm in $library_dir: $!\n";

my @nonmem_pm = <PSN>;

close( PSN );

open( PSN, '>', "$library_dir" . "/PsN_$name_safe_version/nonmem.pm" ) or die "Unable to install PsN-$name_safe_version/nonmem.pm in $library_dir: $!\n";

for( @nonmem_pm ){
 if( /require PsN/ ){
   print PSN "require PsN_$name_safe_version;\n";
 } else {
   print PSN;
 }
}
close PsN;
print "\nPsN has been successfully installed.

The next step is to install module dependencies. If you
know that all dependencies are installed or you'd
like to install them yourself, then you are finished now.

Would you like this script to install dependencies [y/n]?";

if(confirm()){

#  my $inst = ExtUtils::Installed->new();
#  my @modules = $inst -> modules();

#  my %modules;

#  foreach my $module ( @modules ){
#    $modules{$module} = 1;
#  }

 foreach my $module( @modules ){

   my $mod_file = $module;
   $mod_file =~ s/::/-/g;
   print "\nInstalling $mod_file\n", "=" x 80, "\n";
   if( $Config{osname} eq 'MSWin32' ){
     if( -e "modules/$mod_file-Win" ){
       chdir( "modules/$mod_file-Win" );
       system( "ppm install $mod_file.ppd" );
       chdir( "../.." );
     } else {
       print( "$module might not be installed and you don't have a source distribution. Would you like to try installing using ppm over internet?[y/n]" );
       next unless( confirm() );
       system( "ppm install $mod_file" );
     }
   } elsif ( -e "modules/$mod_file" ){
     chdir( "modules/$mod_file" );
     system( "$Config{bin}/perl Makefile.PL" );
     system( "make install" );
     chdir( "../.." );
   } else {
     print( "$module might not be installed and you don't have a source distribution. Would you like to try installing from CPAN?[y/n]" );
     next unless( confirm() );
     CPAN::install( $module );
   }
   print "=" x80, "\n";
 }
}

print "\nInstallation complete!\n";
print "\nNow you should edit $library_dir/PsN_$name_safe_version/psn.conf so that PsN can find your NONMEM installations.\n\nPress the ENTER key to finish.";

confirm(); # Wait here so windows users see the message

# Uninstall ?
# rm -rf /usr/lib/perl5/site_perl/5.8.4/PsN* /usr/bin/*-2.1.?.pl /usr/bin/execute.pl /usr/bin/llp.pl /usr/bin/cdd.pl /usr/bin/bootstrap.pl /usr/bin/scm.pl /usr/bin/sumo.pl
