# {{{ include statements

start include statements
use tool::cdd;
use tool::modelfit;
use Math::Random;
use Data::Dumper;
end include statements

# }}} include statements

# {{{ new

start new
foreach my $attribute ( 'logfile', 'raw_results_file' ) {
  if ( not( ref($this -> {$attribute}) eq 'ARRAY' or
	    ref($this -> {$attribute}) eq 'HASH' ) ) {
    my $tmp = $this -> {$attribute};
    if ( not defined $tmp and $attribute eq 'logfile' ) {
	$tmp = 'mcs.log';
    }
    $this -> {$attribute} = [];
    for ( my $i = 1; $i <= scalar @{$this -> {'models'}}; $i++ ) {
      my $name = $tmp;
      if ( $name =~ /\./ ) {
	$name =~ s/\./$i\./;
      } else {
	$name = $name.$i;
      }
      my $ldir;
      ( $ldir, $name ) =
	  OSspecific::absolute_path( $this -> {'directory'}, $name );
      push ( @{$this -> {$attribute}}, $ldir.$name ) ;
    }
  }
}

if ( scalar (@{$this -> {'models'}->[0]-> problems}) != 1 ){
    'debug' -> die(message => 'The simulation model must contain exactly one problem.');
}

foreach my $alt (@{$this -> {'alternative_models'}}){
    if ( scalar (@{$alt -> problems}) != 1 ){
	'debug' -> die(message => 'The alternative models must each contain exactly one problem.');
    }
}

if ((scalar(@{$this -> {'alternative_models'}}) < 1) && (not $this->{'estimate_simulation'})){
  'debug' -> die(message => "Must estimate at least one model, ".
		 "either an alternative or the simulation model.");
}

if ($this->{'estimate_simulation'} && (defined $this->{'ref_ofv'})){
  'debug'-> die(message => "Not allowed to use a reference ofv-value ".
		"when estimating the simulation model.");
}
if ($this->{'samples'} < 1){
  'debug'-> die(message => "Must set -samples to at least 1.");
}
  
end new

# }}}

# {{{ cdd_setup

start cdd_setup

my @subm_threads;
if (ref( $self -> {'threads'} ) eq 'ARRAY') {
  @subm_threads = @{$self -> {'threads'}};
  shift(@subm_threads);
} else {
  @subm_threads = ($self -> {'threads'});
}
$self -> general_setup( model_number => $model_number,
			class        => 'tool::cdd',
			subm_threads => \@subm_threads );

end cdd_setup

# }}} cdd_setup

# {{{ modelfit_setup

start modelfit_setup

my @subm_threads;
if (ref( $self -> {'threads'} ) eq 'ARRAY') {
  @subm_threads = @{$self -> {'threads'}};
  shift(@subm_threads);
} else {
  @subm_threads = ($self -> {'threads'});
}
$self -> general_setup( model_number => $model_number,
			class        => 'tool::modelfit',
			subm_threads => $subm_threads[$model_number-1] );

end modelfit_setup

# }}} modelfit_setup

# {{{ general_setup

start general_setup
{ 
  my $model = $self -> {'models'} -> [$model_number-1];
  my @alternatives = @{$self -> {'alternative_models'}}; #may be empty
  my ( @seed, $new_datas, $skip_ids, $skip_keys, $skip_values );
  my ( @orig_est_models, @alt_est_models );
  my ( $sim_model, $est_alternative );
  my $alternative;
  my $time_in_input=0;
  my $datx_in_input=0;
  
  my $done = ( -e $self -> {'directory'}."/m$model_number/done" ) ? 1 : 0;
  
  if ( not $done ) {
    
    # {{{ this is code for the first run , i.e. not a restart.

    my @sim_models;
    my @table_header;
    my @all_simulated_files;
    my ( @orig_table_names, @alt_table_names );
    
    for( my $sim_no = 1; $sim_no <= $self -> {'samples'}; $sim_no++ ) {
      
      # {{{ Copy the model to new simulation models

      my $est_original;
      my $sim_name = "mc-$sim_no.sim";
      my $orig_name = "mc-original-$sim_no.mod";
      my $out_name = "mc-$sim_no.lst";
      my $orig_out = "mc-original-$sim_no.lst";
      if( $sim_no == 1 ) {
	$sim_model = $model ->
	    copy( filename    => $self -> {'directory'}.'m'.$model_number.'/'.$sim_name,
		  target      => 'disk',
		  copy_data   => 1,
		  copy_output => 0);
	$sim_model -> drop_dropped;

	if ($sim_model-> is_option_set(record=>'input',name=>'TIME')){
	  #this assumes no synonym, and TIME is always option, not value.
	  $time_in_input=1;
	}
	foreach my $col ('DATE','DAT1','DAT2','DAT3'){
	  if ($sim_model-> is_option_set(record=>'input',name=>$col)){
	    #this assumes no synonym, and name always options, not value.
	    $datx_in_input=1;
	    last;
	  }
	}

	if( $self -> {'estimate_simulation'} ) {
	  $est_original = $model ->
	      copy( filename    => $self -> {'directory'}.'m'.$model_number.'/'.$orig_name,
		    target      => 'disk',
		    copy_data   => 0,
		    copy_output => 0);
	  $est_original -> drop_dropped;
	  $est_original -> remove_records( type => 'simulation' );
	  
	  #ignore @ since simdata contains header rows. can skip old ignores since filtered
	  $est_original -> set_option( record_name  => 'data',
				       option_name  => 'IGNORE',
				       option_value => '@');

	  #remove any DATX from est_original $INPUT
	  if ($datx_in_input){
	    foreach my $col ('DATE','DAT1','DAT2','DAT3'){
	      $est_original -> remove_option(record_name => 'input',
					     option_name => $col);
	    }
	    unless ($time_in_input){
	      $est_original -> set_option(record_name => 'input',
					  option_name => 'TIME');
	    }
	  }
	}
	
	my $tbl_nm_ref = $model -> table_names();
	@orig_table_names = @{$tbl_nm_ref} if( defined $tbl_nm_ref );

      } else {
	$sim_model = $sim_models[0] ->
	    copy( filename    => $self -> {'directory'}.'m'.$model_number.'/'.$sim_name,
		  target      => 'disk',
		  copy_data   => 0,
		  copy_output => 0);

	if( $self -> {'estimate_simulation'} ){
	  $est_original = $orig_est_models[0] ->
	      copy( filename    => $self -> {'directory'}.'m'.$model_number.'/'.$orig_name,
		    target      => 'disk',
		    copy_data   => 0,
		    copy_output => 0);

	}
      }
      
      $sim_model -> ignore_missing_files( 1 );
      $sim_model -> outputfile( $self -> {'directory'}.'m'.$model_number.'/'.$out_name );
      $sim_model -> ignore_missing_files( 0 );
      
      if( $self -> {'estimate_simulation'} ) {
	$est_original -> ignore_missing_files( 1 );
	$est_original -> outputfile( $self -> {'directory'}.'m'.$model_number.'/'.$orig_out );
	$est_original -> ignore_missing_files( 0 );
	
	my @new_orig_table_names;
	
	for( my $pr = 0; $pr <= $#orig_table_names; $pr++ ) {
	  for( my $tbl = 0; $tbl < scalar @{$orig_table_names[$pr]}; $tbl++ ) {
	    $new_orig_table_names[$pr][$tbl] = $orig_table_names[$pr][$tbl].'-'.$sim_no;
	  }
	}
	
	if( $#new_orig_table_names >= 0 ) {
	  $est_original -> table_names( new_names => \@new_orig_table_names );
	}
	
      }

      if( $self -> shrinkage() ) {
	$est_alternative -> shrinkage_stats( enabled => 1 );
	
	if( $self -> {'estimate_simulation'} ){
	  $est_original -> shrinkage_stats( enabled => 1 );
	  my @problems = @{$est_original -> problems};
	  for( my $i = 1; $i <= scalar @problems; $i++ ) {
	    $problems[ $i-1 ] -> shrinkage_module -> model( $est_original );
	  }
	}
      }
      
      my @simulated_files;
      for( my $j = 0; $j < scalar @{$sim_model -> problems}; $j++ ) {
	my $prob = $sim_model -> problems -> [$j];
	
	# set $SIMULATION record
	
	my $sim_record = $sim_model -> record( problem_number => $j,
					       record_name => 'simulation' );
	
	if( scalar(@{$sim_record}) > 0 ){
	  
	  my @new_record;
	  foreach my $sim_line ( @{$sim_record -> [0]} ){
	    my $new_line;
	    while( $sim_line =~ /([^()]*)(\([^()]+\))(.*)/g ){
	      my $head = $1;
	      my $old_seed = $2;
	      $sim_line = $3;
	      $new_line .= $head;
	      
	      while( $old_seed =~ /(\D*)(\d+)(.*)/ ){
		$new_line .= $1;
		$new_line .= random_uniform_integer( 1, 0, 1000000 ); # Upper limit is from nmhelp 
		$old_seed = $3;
	      }
	      
	      $new_line .= $old_seed;
	      
	    }
	    
	    push( @new_record, $new_line.$sim_line );
	  }
	  
	  $prob -> set_records( type => 'simulation',
				record_strings => \@new_record );
	  
	} else {
	  
	  my $seed = random_uniform_integer( 1, 0, 1000000 ); # Upper limit is from nmhelp
	  $prob -> set_records( type           => 'simulation',
				record_strings => [ '(' . $seed .
						    ') ONLYSIMULATION' ] );
	}
	
	if( $sim_no == 1 ) {
	  # remove $EST and $COV
	  
	  if( $sim_model -> is_option_set( problem_number => $j,record => 'estimation',
					   name => 'LIKELIHOOD',fuzzy_match => 1 )
	      or
	      $sim_model -> is_option_set( problem_number => $j,record => 'estimation',
					   name => '-2LOGLIKELIHOOD',fuzzy_match => 1 )
	      or
	      $sim_model -> is_option_set( problem_number => $j, record => 'estimation',
					   name => '-2LLIKELIHOOD',fuzzy_match => 1 )
	      or
	      $sim_model -> is_option_set( problem_number => $j, record => 'estimation',
					   name => 'LAPLACIAN',fuzzy_match => 1 )
	      ){
	    
	    $sim_model -> set_option( problem_numbers => [$j], record_name => 'estimation',
				      option_name => 'MAXEVALS',fuzzy_match => 1, 
				      option_value => 0 );
	    
	  } else {
	    
	    $prob -> remove_records(type => 'estimation');
	    
	  }
	  
	  $prob -> remove_records(type => 'covariance');
	  
	  # set $TABLE record
	  #when copying $INPUT to $TABLE: remove DATX
	  #if not TIME present, remove TIME
	  #if not TIME present but DATX then add back TIME at the end

	  if( defined $prob -> inputs and defined $prob -> inputs -> [0] -> options ) {
	    foreach my $option ( @{$prob -> inputs -> [0] -> options} ) {
	      push( @table_header, $option -> name ) unless 
		  (($option -> name =~ /DAT(E|1|2|3)/) ||
		   ((not $time_in_input) && ($option -> name =~ /TIME/)));
	    }
	    if ((not $time_in_input) && ($datx_in_input )){
	      push( @table_header, 'TIME');
	    }
	  } else {
	    debug -> die( message => "Trying to construct table for monte-carlo simulation".
			  " but no headers were found in \$model_number-INPUT" );
	  }
	}
	my $simulated_file = "mc-sim-".($j+1)."-$sim_no.dat";
	$prob -> set_records( type           => 'table',
			      record_strings => [ join( ' ', @table_header ).
						  ' NOPRINT NOAPPEND ONEHEADER FILE='.
						  $simulated_file ] );
	push( @simulated_files, $self -> {'directory'}.'m'.$model_number.'/'.
	      $simulated_file );
      }
      $sim_model -> _write( write_data => $sim_no == 1 ? 1 : 0 );
      push( @sim_models, $sim_model );
      if( defined $est_original ){
	push( @orig_est_models, $est_original );
      }
      push( @all_simulated_files, \@simulated_files );
      
      # }}}
      
    }

    my $threads = $self -> {'parallel_simulations'} ? $self -> {'parallel_simulations'} : 1;

    my $mod_sim = tool::modelfit -> new( top_tool         => 0,
					 reference_object => $self,
					 models           => \@sim_models,
					 base_directory   => $self -> {'directory'},
					 directory        => $self -> {'directory'}.
					 'simulation_dir'.$model_number, 
					 parent_tool_id   => $self -> {'tool_id'},
					 retries          => 1,
					 logfile	         => undef,
					 raw_results           => undef,
					 prepared_models       => undef,
					 threads          => $threads );
    
    $mod_sim -> run;
    
    for( my $j = 0; $j <= $#orig_est_models; $j++ ) {
      $orig_est_models[$j] -> datafiles( new_names => $all_simulated_files[$j] );
      $orig_est_models[$j] -> _write;
      $orig_est_models[$j] -> flush_data();
    }
    @{$self -> {'mc_models'}} = ( @orig_est_models );
    
    my $alternative_counter=0;
    foreach $alternative (@alternatives){
      $alternative_counter++;
      # {{{ create copies of the alternative models
      @alt_table_names = ();
      @alt_est_models =();
#      my $filestem = 'mc-alt-' . $alternative -> filename();
#      $filestem =~ s/\.[^.]*$//;
      my $filestem = 'mc-alternative_' . $alternative_counter;
      for( my $sim_no = 1; $sim_no <= $self -> {'samples'}; $sim_no++ ) {
	my $alt_name = "$filestem-$sim_no.mod";
	my $alt_out = "$filestem-$sim_no.lst"; 
	
	if( $sim_no == 1 ) {
	  $est_alternative = $alternative ->
	      copy( filename    => $self -> {'directory'}.'m'.$model_number.'/'.$alt_name,
		    target      => 'disk',
		    copy_data   => 0,
		    copy_output => 0);
	  $est_alternative -> drop_dropped;
	  #remove any DATX in $INPUT (drop_dropped does not)
	  foreach my $col ('DATE','DAT1','DAT2','DAT3'){
	    $est_alternative -> remove_option(record_name => 'input',
					      option_name => $col);
	  }
	  #if added time then remove TIME (if present) and then add TIME (must be last in list)
	  if ((not $time_in_input) && ($datx_in_input)){
	    $est_alternative -> remove_option(record_name => 'input',
					      option_name => 'TIME');
	    $est_alternative -> set_option(record_name => 'input',
					   option_name => 'TIME');
	  }

	  $est_alternative -> remove_records( type => 'simulation' );
	  
	  #ignore @ since simdata contains header rows. cannot skip other old ignores!!
	  
	  my $ig_list = $est_alternative -> get_option_value( record_name  => 'data',
							      option_name  => 'IGNORE',
							      option_index => 'all');
	  $est_alternative -> remove_option( record_name  => 'data',
					     option_name  => 'IGNORE');
	  
	  if (scalar (@{$ig_list})>0){
	    foreach my $val (@{$ig_list}){
	      unless ($val =~ /^.$/){
		$est_alternative -> add_option( record_name  => 'data',
						option_name  => 'IGNORE',
						option_value => $val);
	      }
	    }
	  }
	  $est_alternative -> add_option( record_name  => 'data',
					  option_name  => 'IGNORE',
					  option_value => '@');
	  ##done fixing ignore

	  
	  
	  my $tbl_nm_ref = $alternative -> table_names();
	  @alt_table_names = @{$tbl_nm_ref} if( defined $tbl_nm_ref );
	} else {
	  $est_alternative = $alt_est_models[0] ->
	      copy( filename    => $self -> {'directory'}.'m'.$model_number.'/'.$alt_name,
		    target      => 'disk',
		    copy_data   => 0,
		    copy_output => 0);
	}
	$est_alternative -> ignore_missing_files( 1 );
	$est_alternative -> outputfile( $self -> {'directory'}.'m'.$model_number.'/'.$alt_out );
	$est_alternative -> ignore_missing_files( 0 );
	my @new_alt_table_names;
	for( my $pr = 0; $pr <= $#alt_table_names; $pr++ ) {
	  for( my $tbl = 0; $tbl < scalar @{$alt_table_names[$pr]}; $tbl++ ) {
	    $new_alt_table_names[$pr][$tbl] = $alt_table_names[$pr][$tbl].'-'.$sim_no;
	  }
	}
	if( $#new_alt_table_names >= 0 ) {
	  $est_alternative -> table_names( new_names => \@new_alt_table_names );
	}
	if( $self -> shrinkage() ) {
	  my @problems = @{$est_alternative -> problems};
	  for( my $i = 1; $i <= scalar @problems; $i++ ) {
	    $problems[ $i-1 ] -> shrinkage_module -> model( $est_alternative );
	  }
	}
	push( @alt_est_models, $est_alternative );
      } #end loop over sim_no

      for( my $j = 0; $j <= $#alt_est_models; $j++ ) {
	$alt_est_models[$j] -> datafiles( new_names => $all_simulated_files[$j] );
	$alt_est_models[$j] -> _write;
	$alt_est_models[$j] -> flush_data();
      }
      push (@{$self -> {'mc_models'}}, @alt_est_models);
      
      # }}}
      
    } #end loop over alternatives
    
    # Create a checkpoint.
    open( DONE, ">".$self -> {'directory'}."/m$model_number/done" ) ;
    print DONE "Simulation from ",$sim_models[0] -> filename," through ",
    $sim_models[$#sim_models] -> filename," performed\n";
    print DONE $self -> {'samples'}." samples\n";
    @seed = random_get_seed();
    print DONE "seed: @seed\n";
    close( DONE );

    # }}}
    
  } else {
    
    # {{{ code for restarts
    
    #$done=true
    # Recreate the datasets and models from a checkpoint
    ui -> print( category => 'mc',
		 message  => "Recreating models from a previous run" );
    open( DONE, $self -> {'directory'}."/m$model_number/done" );
    my @rows = <DONE>;
    close( DONE );
    my ( $junk, $junk, $stored_filename1, $junk,
	 $stored_filename2, $junk ) = split(' ',$rows[0],4);
    my ( $stored_samples, $junk ) = split(' ',$rows[1],2);
    @seed = split(' ',$rows[2]);
    shift( @seed ); # get rid of 'seed'-word
    # Reinitiate the model objects
    for ( my $j = 1; $j <= $stored_samples; $j++ ) {
      my ($model_dir, $orig_name) = OSspecific::absolute_path( $self -> {'directory'}.'/m'.
							       $model_number,
							       "mc-original-$j.mod" );
      my ($out_dir, $orig_out) = OSspecific::absolute_path( $self -> {'directory'}.'/m'.
							    $model_number,
							    "mc-original-$j.lst" );
      my $est_original = model ->
	  new( directory   => $model_dir,
	       filename    => $orig_name,
	       outputfile  => $orig_out,
	       extra_files => $model -> extra_files,
	       target      => 'disk',
	       ignore_missing_files => 1,
	       extra_output => $model -> extra_output(),
	       cwres       => $model -> cwres(),
	       quick_reload => 1 );
      push( @orig_est_models, $est_original );
      my $nl = $j == $stored_samples ? "" : "\r"; 
      ui -> print( category => 'mc',
		   message  => ui -> status_bar( sofar => $j+1,
						 goal  => $stored_samples+1 ).$nl,
		   wrap     => 0,
		   newline  => 0 );
    } #end loop over samples
    @{$self -> {'mc_models'}} = ( @orig_est_models);
    
    #start alternatives section
    my $alternative_counter=0;
    foreach $alternative (@alternatives){
      $alternative_counter++;
#      my $filestem = 'mc-alt-' . $alternative -> filename();
#      $filestem =~ s/\.[^.]*$//;
      my $filestem = 'mc-alternative_' . $alternative_counter;
      @alt_est_models =();
      for ( my $j = 1; $j <= $stored_samples; $j++ ) {
	my ($model_dir, $alt_name) = OSspecific::absolute_path( $self -> {'directory'}.'/m'.
								$model_number,
								"$filestem-$j.mod" );
	my ($out_dir, $alt_out) = OSspecific::absolute_path( $self -> {'directory'}.'/m'.
							     $model_number,
							     "$filestem-$j.lst" );
	my $est_alternative = model ->
	    new( directory   => $model_dir,
		 filename    => $alt_name,
		 outputfile  => $alt_out,
		 extra_files => $alternative -> extra_files,
		 target      => 'disk',
		 ignore_missing_files => 1,
		 extra_output => $model -> extra_output(),
		 cwres       => $model -> cwres(),
		 quick_reload => 1 );
	push( @alt_est_models, $est_alternative );
	my $nl = $j == $stored_samples ? "" : "\r"; 
	ui -> print( category => 'mc',
		     message  => ui -> status_bar( sofar => $j+1,
						   goal  => $stored_samples+1 ).$nl,
		     wrap     => 0,
		     newline  => 0 );
      } #end loop over samples
      push (@{$self -> {'mc_models'}}, @alt_est_models );
      
    } #end loop over alternatives
    
    ui -> print( category => 'mc', message  => " ... done." );
    random_set_seed( @seed );
    ui -> print( category => 'mc',
		 message  => "Using $stored_samples data sets, previously simulated ".
		 "from $stored_filename1 through $stored_filename2" )
	unless $self -> {'parent_threads'} > 1;
    
    # }}}
    
  } #end $done
  
  my $subdir = $class;
  $subdir =~ s/tool:://;
  my @subtools = @{$self -> {'subtools'}};
  shift( @subtools );
  my %subargs = ();
  if ( defined $self -> {'subtool_arguments'} ) {
    %subargs = %{$self -> {'subtool_arguments'}};
  }
  
  push( @{$self -> {'tools'}},
	$class -> new
	( reference_object => $self,
	  top_tool         => 0,
	  logfile	         => undef,
	  raw_results      => undef,
	  prepared_models  => undef,
	  models         => $self -> {'mc_models'},
	  nm_version     => $self -> {'nm_version'},
	  parent_tool_id   => $self -> {'tool_id'},
	  base_directory => $self -> {'directory'},
	  directory      => $self -> {'directory'}.'/'.$subdir.'_dir'.$model_number,
	  subtools       => $#subtools >= 0 ? \@subtools : undef,
	  shrinkage      => $self -> {'shrinkage'},
	  _raw_results_callback => $self ->
	  _modelfit_raw_results_callback( model_number => $model_number ),
	  %subargs ) );
}
end general_setup

# }}} general_setup

# {{{ cdd_analyze
start cdd_analyze
    $self -> {'tools'}[0] -> print_results; 
end cdd_analyze
# }}} cdd_analyze

# {{{ modelfit_analyze

start modelfit_analyze
$self -> {'tools'}[0] -> print_results; 
end modelfit_analyze

# }}}

# {{{ _modelfit_raw_results_callback

start _modelfit_raw_results_callback

# Use the mc's raw_results file.
my ($dir,$file) = 
    OSspecific::absolute_path( $self -> {'directory'},
			       $self -> {'raw_results_file'}[$model_number-1] );
my $orig_mod = $self -> {'models'}[$model_number-1];
$subroutine = sub {
  my $modelfit = shift;
  my $mh_ref   = shift;
  my %max_hash = %{$mh_ref};
  $modelfit -> raw_results_file( $dir.$file );

  # {{{ New header

  if ( defined $modelfit -> raw_results() ) {
    my @rows = @{$modelfit -> raw_results()};
    
    #the first 'samples' (a scalar) number of rows is null/simulation models
    if( $self -> {'estimate_simulation'} ){
      for( my $i = 0; $i < $self -> {'samples'}; $i++ ) {
	unshift( @{$rows[$i]}, 'simulation' );
      }
    }
    #remaining rows, if any, belong to alternatives model(s)
    my $offset;
    if( $self -> {'estimate_simulation'} ){
      $offset = $self -> {'samples'};
    } else {
      $offset = 0;
    }
    my $alternative_counter=0;
    foreach my $alternative (@{$self -> {'alternative_models'}}){
      $alternative_counter++;
      if (($offset + $self -> {'samples'}-1)> $#rows){
	debug -> die( message => "Too few rows in raw results file." );
      }
      my $altname = 'mc-alternative_'.$alternative_counter;
      for( my $i = 0; $i < $self -> {'samples'}; $i++ ) {
	unshift( @{$rows[($offset+$i)]}, $altname );
      }
      $offset += $self -> {'samples'};   
    }
    if (($offset -1)< $#rows){
      debug -> die( message => "Too many rows in raw results file." );
    }

    unshift( @{$modelfit -> {'raw_results_header'}}, 'hypothesis' );

  }
  @{$self -> {'raw_results_header'}} = @{$modelfit -> {'raw_results_header'}};
  # }}} New header
  
};
return $subroutine;

end _modelfit_raw_results_callback

# }}} _modelfit_raw_results_callback

# {{{ prepare_results

start prepare_results
{ 
  
  $self -> {'raw_results'} = $self -> {'tools'}[0] -> {'raw_results'};

  my $model = $self -> {'models'} -> [0];
  my @alternatives = @{$self -> {'alternative_models'}};

  my $samples = $self -> {'samples'};

  my $raw_line_struct = $self -> {'tools'}[0] -> {'raw_line_structure'};

  my %orig_results_section;
  my @alt_results_sections;

  ### Retrieve initial parameter estimates from original model.

  my %initial_values;
  my %n_initials;
  my (@thetalist,@omegalist,@sigmalist);
  my $tmp;
  
  $n_initials{'theta'} = $model->nthetas(problem_number => 1);
  $tmp = $model->initial_values(problem_numbers => [1],	parameter_type => 'theta');
  @thetalist=@{$tmp->[0]};
  'debug'->die(message=>'Error number of initial thetas.') 
      unless (scalar(@thetalist) == $n_initials{'theta'});
  $initial_values{'theta'}=\@thetalist;

  $tmp = $model->nomegas(problem_numbers => [1],
			 with_correlations => 1);
  $n_initials{'omega'} = $tmp->[0];
  $tmp = $model->initial_values(problem_numbers => [1],	parameter_type => 'omega');
  @omegalist=@{$tmp->[0]};
  'debug'->die(message=>'Error number of initial omegas.') 
      unless (scalar(@omegalist) == $n_initials{'omega'});
  $initial_values{'omega'}=\@omegalist;

  $tmp = $model->nsigmas(problem_numbers => [1],
			 with_correlations => 1);
  $n_initials{'sigma'} = $tmp->[0];
  $tmp = $model->initial_values(problem_numbers => [1],	parameter_type => 'sigma');
  @sigmalist=@{$tmp->[0]};
  'debug'->die(message=>'Error number of initial sigmas.') 
      unless (scalar(@sigmalist) == $n_initials{'sigma'});
  $initial_values{'sigma'}=\@sigmalist;

  $n_initials{'ofv'}=0;

  ## Prepare general run info for output file
  my %return_section;
  $return_section{'name'} = 'SSE run info';
  my $modelname=$self-> {'models'}->[0] ->filename();
  $return_section{'labels'} = [[],['Date','samples','simulation model',
				   'PsN version','NONMEM version']];
  my @datearr=localtime;
  my $the_date=($datearr[5]+1900).'-'.($datearr[4]+1).'-'.($datearr[3]);
  $return_section{'values'} = [[$the_date,$self->{'samples'},$modelname,
				$PsN::version,$self->{'nm_version'}]];
  push( @{$self -> {'results'}[0]{'own'}},\%return_section );


  ### Prepare result section with ofv reference value , if used, and true parameter values for sim
  my %reference_section;
  $reference_section{'name'} = 'Reference/True values';
  my @reference_headers=('ofv');
  my @reference_values;
  if (defined $self->{'ref_ofv'}){
    @reference_values=($self->{'ref_ofv'});
  }elsif ($self->{'estimate_simulation'}){
    @reference_values=('Sim. model result');
  } else{
    @reference_values=('Alt. model 1 result');
  }
  foreach my $measure ( 'theta','omega','sigma' ){
    for (my $i=1; $i<=$n_initials{$measure};$i++){
      push(@reference_headers, uc(substr($measure, 0,2))."_$i" );
      push(@reference_values,$initial_values{$measure}->[$i-1]);
    }
  }

  $reference_section{'labels'}=[[],\@reference_headers];
  $reference_section{'values'}=[\@reference_values];
  push( @{$self -> {'results'}[0]{'own'}},\%reference_section );

  ### Loop over the original model (the zero) and over all alternative
  ### models.

  my $end_index = ( $self -> {'estimate_simulation'})? scalar(@alternatives): scalar(@alternatives) - 1;

  for my $model_index ( 0..$end_index ){
    
    ### Foreach such model, create a %results_section

    my %results_section;

    ### Each results_section contains statistics of ofv and
    ### parameters.

    my @values;
    my @labels;

    @{$labels[0]} = ('mean','median','sd','max','min','skewness',
		     'kurtosis','rmse','bias','rse');
    my @ci_list=(0.5,2.5,5,95,97.5,99.5);
    my @z_list=(-2.58,-1.96,-1.645,1.645,1.96,2.58);
    push(@{$labels[0]},('standard error CI'));
    foreach my $ci (@ci_list){
      push(@{$labels[0]},"$ci\%");
    }

    if( $model_index == 0 and $self -> {'estimate_simulation'}){
      $results_section{'name'} = "\nSimulation model";
    } else {
      my $alt_index;
      if( $self -> {'estimate_simulation'} ){
	$alt_index = $model_index; 
      } else {
	$alt_index = $model_index + 1;
      }
      $results_section{'name'} = "\nAlternative model ".$alt_index;
    }

    $results_section{'labels'} = \@labels;
    $results_section{'values'} = \@values;
    
    push( @{$self -> {'results'}[0]{'own'}},\%results_section );

    MEASURE: foreach my $measure ( 'ofv','theta','omega','sigma' ){

      # Create a header, we might use max_hash from modelfit for this
      # later on.

      my $start=0;
      my $length=0;

      for( 0..($samples-1) ){
	my ($s, $l) = split(/,/, $raw_line_struct -> {1+$model_index*$samples + $_} -> {$measure});
	
	if( defined $s and $start != 0 and $s != $start ){
	  'debug' -> die( message => "Internal structure corrupted at $model_index, ".
			  "$samples, $_, $measure, this is a bug" );
	} else {
	  $start = $s;
	}

	if( defined $l and $l > $length ){
	  $length = $l;
	}
	if( $start == 0 ){
	  for( 0..(10+scalar(@z_list)) ){
	    # At least two reasons for this. Either the "measure" does
	    # not exist or the model has not terminated.
	    push(@{$values[$_]},'NA');
	  }
	  push( @{$labels[1]},$measure );
	  next MEASURE;
	}
	
      }


      if( $measure eq 'ofv' ){
	push( @{$labels[1]}, $measure );
      } else {
	for( 1..$length ){
	  push( @{$labels[1]}, uc(substr($measure, 0,2))."_$_" );
	}
      }

      my $initial_index=0;
      foreach my $col ( $start..($start + $length-1) ){
	my ($skew, $kurt, $mean, $stdev) 
	    = $self -> skewness_and_kurtosis( column_index => $col+1,
					      start_row_index => $model_index*$samples, 
					      end_row_index => $model_index*$samples+$samples-1 );
	my ($maximum,$minimum) 
	    = $self -> max_and_min( column_index => $col+1,
				    start_row_index => $model_index*$samples, 
				    end_row_index => $model_index*$samples+$samples-1 );

	
	push( @{$values[0]}, $mean );
	push( @{$values[2]}, $stdev );
	push( @{$values[3]}, $maximum );
	push( @{$values[4]}, $minimum );
	push( @{$values[5]}, $skew );
	push( @{$values[6]}, $kurt );

	my $median = $self -> median( column_index => $col+1,
				      start_row_index => $model_index*$samples, 
				      end_row_index => $model_index*$samples+$samples-1 );

	push( @{$values[1]}, $median );

	if( $initial_index < $n_initials{$measure}){
	    
	  my $initial_value= $initial_values{$measure}->[$initial_index];
	  
	  $initial_index++;

	  my $rmse = ' ';
	  my $bias = ' ';
	  my $rse = ' ';
	  
	  if( defined $initial_value ){
	    $rmse = $self -> rmse_percent( column_index => $col+1,
					   start_row_index => $model_index*$samples, 
					   end_row_index => $model_index*$samples+$samples-1,
					   initial_value => $initial_value );
	    
	    $bias = $self -> bias_percent( column_index => $col+1,
					   start_row_index => $model_index*$samples, 
					   end_row_index => $model_index*$samples+$samples-1,
					   initial_value => $initial_value );

	    $rse = ($initial_value != 0)? 100*$stdev/$initial_value : 'NA';
	  }
  
	  push( @{$values[7]}, $rmse );	  
	  push( @{$values[8]}, $bias );
	  push( @{$values[9]}, $rse );

	  my $label_index=11; #leave 10 empty
	  foreach my $zval (@z_list){
	    if( defined $initial_value ){
	      push( @{$values[$label_index]},($bias+$zval*$rse/sqrt($samples)));
	    } else {
	      push( @{$values[$label_index]}, ' ' );
	    }
	    $label_index++;
	  }
	  
	} else {
	  #push placeholders so that values will stay in correct column
	  push( @{$values[7]}, ' ' );
	  push( @{$values[8]}, ' ' );
	  push( @{$values[9]}, ' ' );
	  my $label_index=11; #leave 10 empty
	  foreach my $zval (@z_list){
	    push( @{$values[$label_index]}, ' ' );
	    $label_index++;
	  }
	}
      }
    }
  }
  
  ## Calculate OFV mean calculations
  #if at least two estimated models or ref_ofv given
  if( $end_index > 0 or defined $self -> {'ref_ofv'} ){
  
    my @ofv_limits = (0,3.84,5.99,7.81,9.49);
    
    my (%results_section_below, %results_section_above);
    my (@labels_below, @labels_above, @values_below, @values_above);
    
    @{$labels_above[1]} = ('mean delta_OFV');
    @{$labels_below[1]} = ('mean delta_OFV');
    
    foreach my $limit( @ofv_limits ){
      push( @{$labels_above[1]}, "$limit" );
      push( @{$labels_below[1]}, "$limit" );
    }
    
    $results_section_above{'name'} = "\nOFV Statistics\nType I error rate\n,,Percent delta_OFV > N";
    $results_section_above{'labels'} = \@labels_above;
    $results_section_above{'values'} = \@values_above;
    
    $results_section_below{'name'} = "\n1 - type II error rate (power)\n,,Percent delta_OFV < N";
    $results_section_below{'labels'} = \@labels_below;
    $results_section_below{'values'} = \@values_below;
    
    
    push( @{$self -> {'results'}[0]{'own'}},\%results_section_above );
    push( @{$self -> {'results'}[0]{'own'}},\%results_section_below );

    
    my $reference_string;
    my $alt_index;
    if ( defined $self -> {'ref_ofv'} ){
      $reference_string='reference ofv -';
      $alt_index = 1;
    } elsif( $self -> {'estimate_simulation'} ){
      $reference_string= 'simulation -';
      $alt_index=1;
    } else {
      $reference_string= 'alternative1 -';
      $alt_index = 2;
    }

    # $end_index is already set.
    my $start_index = (defined $self -> {'ref_ofv'})? 0 : 1;

    ALTMODEL: foreach my $model_index( $start_index..$end_index ){

      push( @{$labels_above[0]}, "$reference_string alternative".($alt_index));
      push( @{$labels_below[0]}, "$reference_string alternative".($alt_index));
      $alt_index++;

      my $start=0;
      my $delta_sum=0;      
      my %nr_below;
      my %nr_above;
      my $nr_terminated=0;
      my $org_ofv;
      
      foreach my $sample( 0..($samples-1) ){
	
	my ($s, $l) = split(/,/, $raw_line_struct -> {1+$model_index*$samples + $sample} -> {'ofv'});
	my @dummy= split(/,/, $raw_line_struct -> {$model_index*$samples + $sample} -> {'ofv'});
	if( $s ne '' and $start != 0 and $s != $start ){
	  'debug' -> die( message => "OFV: Internal structure corrupted at ".
			  "$model_index, $samples, $sample, this is a bug" );
	} else {
	  $start = $s;
	}
	if($start eq '' ){
	  # This model probably did not terminate. 
	  push(@{$values_below[$model_index-$start_index]},"Alternative model not terminated for all samples");
	  push(@{$values_above[$model_index-$start_index]},"Alternative model not terminated for all samples");
	  next ALTMODEL;
	}
			   
	if ( defined $self -> {'ref_ofv' } ){
	  $org_ofv = $self -> {'ref_ofv'};
	} else {
	  #check reference model terminated
	  my ($s, $l) = split(/,/, $raw_line_struct -> {1+ $sample} -> {'ofv'});
	  if( $s ne ''){
	    $org_ofv = $self -> {'raw_results'} -> [$sample][$start+1];
	  }else {
	    next; #sample
	  }
	}

	my $alt_ofv = $self -> {'raw_results'} -> [$model_index*$samples + $sample][$start+1];
	$nr_terminated++;
	my $delta = $org_ofv - $alt_ofv;
	$delta_sum += $delta;
	
	foreach my $limit ( @ofv_limits ){
	  
	  if( $delta < -$limit ){
	    $nr_below{$limit}++;
	  }
	  
	  if( $delta > $limit ) {
	    $nr_above{$limit}++;
	  }
	  
	}
	
      } #end loop over samples

      if ($nr_terminated > 0){
	push( @{$values_below[$model_index-$start_index]}, ($delta_sum / $nr_terminated) );
	foreach my $limit( @ofv_limits ){
	  push( @{$values_below[$model_index-$start_index]}, ($nr_below{$limit} / $nr_terminated)*100 . "%" );
	}
	
	push( @{$values_above[$model_index-$start_index]}, ($delta_sum / $nr_terminated) );
	foreach my $limit( @ofv_limits ){
	  push( @{$values_above[$model_index-$start_index]}, ($nr_above{$limit} / $nr_terminated)*100 . "%" );
	}
      } else {
	push( @{$values_below[$model_index-$start_index]},"Reference model not terminated for any sample" );
	push( @{$values_above[$model_index-$start_index]},"Reference model not terminated for any sample");
      }
    }
  }

}
end prepare_results

# }}}

# {{{ rmse_percent

start rmse_percent
{
  #input is integers $column_index, $start_row_index, $end_row_index and scalar float $initial_value
  #output is scalar $rmse_percent

  unless( $end_row_index ){
    $end_row_index = $#{$self -> {'raw_results'}};
  }

  debug -> die( message => "Bad row index input") if ($start_row_index > $end_row_index);
  my $row_count = $end_row_index - $start_row_index +1;
  my $sum_squared_errors=0;
  for (my $i=$start_row_index; $i<=$end_row_index; $i++){
    $sum_squared_errors += ($self->{'raw_results'}->[$i][$column_index] - $initial_value)**2;
  }
  
  if ($initial_value == 0){
    debug -> warn( level => 2,
		   message => "Initial value 0, returning absolute rmse instead of relative.");
    $rmse_percent= sqrt($sum_squared_errors/$row_count);
  }else{
    $rmse_percent= (sqrt($sum_squared_errors/$row_count))*100/abs($initial_value);
  }
}
end rmse_percent

# }}} rmse_percent

# {{{ bias_percent

start bias_percent
{
  #input is integers $column_index, $start_row_index, $end_row_index and scalar float $initial_value
  #output is scalar $bias_percent

  unless( $end_row_index ){
    $end_row_index = $#{$self -> {'raw_results'}};
  }
  
  debug -> die( message => "Bad row index input") if ($start_row_index > $end_row_index);
  
  my $row_count = $end_row_index - $start_row_index +1;
  my $sum_errors=0;
  for (my $i=$start_row_index; $i<=$end_row_index; $i++){
    $sum_errors += ($self->{'raw_results'}->[$i][$column_index] - $initial_value);
  }

  if ($initial_value == 0){
    debug -> warn( level => 2,
		   message => "Initial value 0, returning absolute bias instead of relative.");
    $bias_percent= ($sum_errors/$row_count);
  }else{
    $bias_percent= ($sum_errors/$row_count)*100/abs($initial_value);
  }
}
end bias_percent

# }}} bias_percent

# {{{ skewness_and_kurtosis

start skewness_and_kurtosis
{
  #input is integers $column_index, $start_row_index, $end_row_index 
  
  unless( $end_row_index ){
    $end_row_index = $#{$self -> {'raw_results'}};
  }

  debug -> die( message => "Bad row index input") if ($start_row_index >= $end_row_index);

  my $row_count = $end_row_index - $start_row_index +1;
  my $sum_values=0;
  for (my $i=$start_row_index; $i<=$end_row_index; $i++){
    $sum_values += $self->{'raw_results'}->[$i][$column_index];
  }

  $mean=$sum_values/$row_count;

  my $error=0;
  my $sum_errors_pow2=0;
  my $sum_errors_pow3=0;
  my $sum_errors_pow4=0;

  for (my $i=$start_row_index; $i<=$end_row_index; $i++){
    $error = ($self->{'raw_results'}->[$i][$column_index] - $mean);
    $sum_errors_pow2 += $error**2;
    $sum_errors_pow3 += $error**3;
    $sum_errors_pow4 += $error**4;
  }

  ## TODO fråga om missing values. och om SD

  $stdev=0;
  unless( $sum_errors_pow2 == 0 ){

    $stdev= sqrt ($sum_errors_pow2/($row_count-1));
    $skewness = $sum_errors_pow3/($row_count*($stdev**3));
    $kurtosis = -3 + $sum_errors_pow4/($row_count*($stdev**4));

  }
}
end skewness_and_kurtosis

# }}} skewness_and_kurtosis

# {{{ max_and_min

start max_and_min
{
  #input is integers $column_index, $start_row_index, $end_row_index 
  
  unless( $end_row_index ){
    $end_row_index = $#{$self -> {'raw_results'}};
  }

  debug -> die( message => "Bad row index input") if ($start_row_index >= $end_row_index);

  $maximum=$self->{'raw_results'}->[$start_row_index][$column_index];
  $minimum=$self->{'raw_results'}->[$start_row_index][$column_index];
  for (my $i=$start_row_index+1; $i<=$end_row_index; $i++){
    $maximum = $self->{'raw_results'}->[$i][$column_index] if
	($self->{'raw_results'}->[$i][$column_index] > $maximum); 
    $minimum = $self->{'raw_results'}->[$i][$column_index] if
	($self->{'raw_results'}->[$i][$column_index] < $minimum); 
  }

}
end max_and_min

# }}} max_and_min


# {{{ median

start median
{
  #input is integers $column_index, $start_row_index, $end_row_index 
  
  unless( $end_row_index ){
    $end_row_index = $#{$self -> {'raw_results'}};
  }
  
  debug -> die( message => "Bad row index input") if ($start_row_index >= $end_row_index);

  my @temp;
  
  for (my $i=$start_row_index; $i<=$end_row_index; $i++){
    push( @temp, $self->{'raw_results'}->[$i][$column_index] );
  }

  @temp = sort({$a <=> $b} @temp);
  if( scalar( @temp ) % 2 ){
    $median = $temp[$#temp/2];
  } else {
    $median = ($temp[@temp/2]+$temp[(@temp-2)/2]) / 2;
  }
 
}
end median

# }}} median
