# {{{ include

start include statements
use strict;
use File::Copy 'cp';
use data;
use OSspecific;
use tool::llp;
use tool::cdd::jackknife;
use ext::Statistics::Distributions 'udistr', 'uprob';
use Math::Random;
use Data::Dumper;
end include

# }}} include statements

# {{{ new

start new
      {
	foreach my $attribute ( 'logfile', 'raw_results_file','raw_nonp_file' ) {
	  if ( not( ref($this -> {$attribute}) eq 'ARRAY' or
		    ref($this -> {$attribute}) eq 'HASH' ) ) {
	    my $tmp = $this -> {$attribute};
	    $this -> {$attribute} = [];
	    for ( my $i = 1; $i <= scalar @{$this -> {'models'}}; $i++ ) {
	      my $name = $tmp;
	      if ( $name =~ /\./ ) {
		$name =~ s/\./$i\./;
	      } else {
		$name = $name.$i;
	      }
	      my $ldir;
	      ( $ldir, $name ) =
		OSspecific::absolute_path( $this -> {'directory'}, $name );
	      push ( @{$this -> {$attribute}}, $ldir.$name ) ;
	    }
	  }
	}
      }
end new

# }}} new

# {{{ general_pre_fork_setup

start general_pre_fork_setup
    {
      # These attributes can be given as a
      # 1. A scalar       : used for all models and problems (subjects can be a scalar ref to a hash)
      # 2. A 1-dim. array : specified per problem but same for all models
      # 3. A 2-dim. array : specified per problem and model

      foreach my $attribute_string( 'stratify_on', 'samples', 'subjects' ){
	my $attribute = $self -> {$attribute_string};
	if ( defined $attribute ) {
	  # If stratification is given we check at what level it is
	  # given.
	  unless ( ref( \$attribute ) eq 'SCALAR' or
		   ref( \$attribute ) eq 'REF' or
		   ( ref( $attribute ) eq 'ARRAY' and scalar @{$attribute} > 0 ) ) {
	    # Here we know its neither scalar or a correct array. But we
	    # seem to assume the lenght be equal to the number of
	    # models.
	    debug -> die( message => "attribute $attribute_string is " .
			  "defined as a " . ref( $attribute ) .
			  " but is neither a scalar or a non-zero size array" );
	  } elsif ( ref( \$attribute ) eq 'SCALAR' or ref( \$attribute ) eq 'REF' ) {
	    # If it is a scalar we copy the scalar value into an array
	    # for each model, with one value for each problem.
	    my @mo_attribute = ();
	    foreach my $model ( @{$self -> {'models'}} ) {
	      my @pr_attribute = ();
	      foreach my $problem ( @{$model -> problems}  ) {
		push( @pr_attribute, $attribute );
	      }
	      push( @mo_attribute, \@pr_attribute );
	    }
	    $self -> {$attribute_string} = \@mo_attribute;
	  } elsif ( ref( $attribute ) eq 'ARRAY' ) {
	    # If it is an array we check if the it is an array of
	    # scalars or of arrays. If it is of arrays we seem to
	    # assume that the arrays have a length the matches the
	    # number of problems. It it is an array of scalars, we
	    # copy that scalar into arrays for each problem.
	    unless ( ref( \$attribute -> [0] ) eq 'SCALAR' or
		     ( ref( $attribute -> [0] ) eq 'ARRAY' and scalar @{$attribute -> [0]} > 0 ) ) {
	      debug -> die( message => "attribute $attribute is ",
			    "defined as a ",ref( $attribute -> [0] ),
			    "and is neither a scalar or a non-zero size array" );
	    } elsif ( ref(\$attribute -> [0]) eq 'SCALAR' ) {
	      my @mo_attribute = ();
	      foreach my $model ( @{$self -> {'models'}} ) {
		push( @mo_attribute, $attribute );
	      }
	      $self -> {$attribute_string} = \@mo_attribute;
	    }
	  }
	} else {
	  # If stratify is not defined we copy an undefined value to
	  # arrays corresponding to models and subproblem. I wonder if
	  # this is really necessary.
	  my @mo_attribute = ();
	  foreach my $model ( @{$self -> {'models'}} ) {
	    my @pr_attribute = ();
	    foreach my $data ( @{$model -> datas}  ) {
	      if( $attribute_string eq 'stratify_on' ){
		push( @pr_attribute, undef );
	      } elsif ( $attribute_string eq 'samples' ){
		push( @pr_attribute, 200 );
	      } elsif ( $attribute_string eq 'subjects' ){
		push( @pr_attribute, {'default'=>($data -> count_ind)} );
	      }
	    }
	    push( @mo_attribute, \@pr_attribute );
	  }
	  $self -> {$attribute_string} = \@mo_attribute;
	}
      }
    }
end general_pre_fork_setup

# }}}

# {{{ modelfit_pre_fork_setup

start modelfit_pre_fork_setup
      {
	$self -> general_pre_fork_setup( model_number => $model_number );
      }
end modelfit_pre_fork_setup

# }}}

# {{{ llp_pre_fork_setup

start llp_pre_fork_setup
      {
	$self -> general_pre_fork_setup( model_number => $model_number );
      }
end llp_pre_fork_setup

# }}}

# {{{ modelfit_setup

start modelfit_setup
      {

	my $subm_threads = ref( $self -> {'threads'} ) eq 'ARRAY' ? 
	  $self -> {'threads'} -> [1]:$self -> {'threads'};
	$self -> general_setup( model_number => $model_number,
				class        => 'tool::modelfit',
			        subm_threads => $subm_threads );
      }
end modelfit_setup

# }}}

# {{{ general_setup

start general_setup
      {	

	# If the number of threads are given per tool, e.g. [2,5] meaning 2 threads for
	# scm and 5 for the modelfit.
	my $subm_threads = ref( $self -> {'threads'} ) eq 'ARRAY' ?
	  $self -> {'threads'} -> [1]:$self -> {'threads'};
	my $own_threads = ref( $self -> {'threads'} ) eq 'ARRAY' ?
	  $self -> {'threads'} -> [0]:$self -> {'threads'};
	# More threads than models?
	my $num = scalar @{$self -> {'models'}};
	$own_threads = $num if ( $own_threads > $num );

	# Sub tool threads can be given as scalar or reference to an array?
	#my $subm_threads = $parm{'subm_threads'};
	#my $own_threads = ref( $self -> {'threads'} ) eq 'ARRAY' ? 
	#  $self -> {'threads'} -> [0]:$self -> {'threads'};
	# Group variable names are matched in the model, not the data!
	my $model	= $self   -> {'models'}      -> [$model_number-1];
	my @samples	= @{$self -> {'samples'}     -> [$model_number-1]};
	my @stratify_on	= @{$self -> {'stratify_on'} -> [$model_number-1]};
	my @subjects	= @{$self -> {'subjects'}    -> [$model_number-1]};

	# Check which models that hasn't been run and run them This
	# will be performed each step but will only result in running
	# models at the first step, if at all.

	# If more than one process is used, there is a VERY high risk
	# of interaction between the processes when creating
	# directories for model fits. Therefore the {'directory'}
	# attribute is given explicitly below.

	# ------------------------  Run original run  -------------------------------

	# {{{ orig run

	unless ( $model -> is_run ) {
	  my %subargs = ();
	  if ( defined $self -> {'subtool_arguments'} ) {
	    %subargs = %{$self -> {'subtool_arguments'}};
	  }

	  if( $self -> {'nonparametric_etas'} or
	      $self -> {'nonparametric_marginals'} ) {
	    $model -> add_nonparametric_code;
	  }
	  
	  my $orig_fit = tool::modelfit ->
	      new( reference_object      => $self,
		   base_directory	 => $self -> {'directory'},
		   directory		 => $self -> {'directory'}.
		   '/orig_modelfit_dir'.$model_number,
		   models		 => [$model],
		   threads               => $subm_threads,
		   parent_threads        => $own_threads,
		   parent_tool_id        => $self -> {'tool_id'},
		   logfile	         => undef,
		   raw_results           => undef,
		   prepared_models       => undef,
		   top_tool              => 0,
		   %subargs );
	  
	  ui -> print( category => 'bootstrap',
		       message => 'Executing base model.' );

	  $orig_fit -> run;

	}

	my $output = $model -> outputs -> [0];

	# }}} orig run

	# ------------------------  Print a log-header  -----------------------------

	# {{{ log header

 	my @orig_datas = @{$model -> datas};
 	my @problems   = @{$model -> problems};
 	my @new_models;

	# Print a log-header
	# Lasse 2005-04-21: The minimization_message print will probably not work anymore
	open( LOG, ">>".$self -> {'logfile'}[$model_number-1] );
	my $ui_text = sprintf("%-5s",'RUN').','.sprintf("%20s",'FILENAME  ').',';
	print LOG sprintf("%-5s",'RUN'),',',sprintf("%20s",'FILENAME  '),',';
	foreach my $param ( 'ofv', 'minimization_message', 'covariance_step_successful' ) {
	  my $orig_ests   = $model -> outputs -> [0] -> $param;
	  # Loop the problems
	  for ( my $j = 0; $j < scalar @{$orig_ests}; $j++ ) {
	    if ( ref( $orig_ests -> [$j][0] ) ne 'ARRAY' ) {
	      my $label = uc($param)."_".($j+1);
	      $ui_text = $ui_text.sprintf("%12s",$label).',';
	      print LOG sprintf("%12s",$label),',';
	    } else {
	      # Loop the parameter numbers (skip sub problem level)
	      for ( my $num = 1; $num <= scalar @{$orig_ests -> [$j][0]}; $num++ ) {
		my $label = uc($param).$num."_".($j+1);
		$ui_text = $ui_text.sprintf("%12s",$label).',';
		print LOG sprintf("%12s",$label),',';
	      }
	    }
	  }
	}
	print LOG "\n";

	# }}} log header

	# ------------------------  Log original run  -------------------------------

	# {{{ Log original run

	# Lasse 2005-04-21: The minimization_message print will probably not work anymore
	open( LOG, ">>".$self -> {'logfile'}[$model_number-1] );
	$ui_text = sprintf("%5s",'0').','.sprintf("%20s",$model -> filename).',';
	print LOG sprintf("%5s",'0'),',',sprintf("%20s",$model -> filename),',';
	foreach my $param ( 'ofv', 'minimization_message', 'covariance_step_successful' ) {
	  my $orig_ests   = $model -> outputs -> [0] -> $param;
	  # Loop the problems
	  for ( my $j = 0; $j < scalar @{$orig_ests}; $j++ ) {
	    if ( ref( $orig_ests -> [$j][0] ) ne 'ARRAY' ) {
	      $orig_ests -> [$j][0] =~ s/\n//g;
	      $ui_text = $ui_text.sprintf("%12s",$orig_ests -> [$j][0]).',';
	      print LOG sprintf("%12s",$orig_ests -> [$j][0]),',';
	    } else {
	      # Loop the parameter numbers (skip sub problem level)
	      for ( my $num = 0; $num < scalar @{$orig_ests -> [$j][0]}; $num++ ) {
		$ui_text = $ui_text.sprintf("%12f",$orig_ests -> [$j][0][$num]).',';
		print LOG sprintf("%12f",$orig_ests -> [$j][0][$num]),',';
	      }
	    }
	  }
	}
	print LOG "\n";

	# }}} Log original run

 	# TODO: In this loop we loose one dimension (problem) in that
 	# all new models with new data sets are put in the same array,
 	# regardless of which problem the initially belonged to. Fix
 	# this.

	if ( $#orig_datas < 0 ) {
	  debug -> warn( level   => 1,
			 message => "No data files to resample from" );
	} else {
	  debug -> warn( level   => 1,
			 message => "Starting bootstrap sampling" );
	}
	for ( my $i = 1; $i <= scalar @orig_datas; $i++ ) {
	  my $orig_data = $orig_datas[$i-1];

	  if ( $self -> {'drop_dropped'} ) {
	    my $model_copy = $model -> copy( copy_data => 1,
					     filename => "drop_copy_$i.mod",
					     directory => $self -> {'directory'} );

	    $model_copy -> drop_dropped;
	    $model_copy -> _write( write_data => 1 );
	    $model_copy -> datas -> [0] -> flush();
	    $orig_data = $model -> datas -> [0];
	    $model = $model_copy;
	  }

	  my ( @seed, $new_datas, $incl_ids, $incl_keys, $new_mod );

	  my $done = ( -e $self -> {'directory'}."/m$model_number/done.$i" ) ? 1 : 0;
	  
	  if ( not $done ) {
	    ui -> print( category => 'bootstrap',
			 message  => "Resampling from ".$orig_data -> filename );

	    ( $new_datas, $incl_ids, $incl_keys )
		= $orig_data -> bootstrap( directory   => $self -> {'directory'}.'/m'.$model_number,
					   name_stub   => 'bs_pr'.$i,
					   samples     => $samples[$i-1],
					   subjects    => $subjects[$i-1],
					   stratify_on => $stratify_on[$i-1], 
					   target	     => 'disk',
					   model_ids   => $self -> {'prepared_model_ids'} );


	    for ( my $j = 0; $j < $samples[$i-1]; $j++ ) {
	      my ($model_dir, $filename) = OSspecific::absolute_path( $self -> {'directory'}.'/m'.$model_number, 
								      'bs_pr'.$i.'_'.($j+1).'.mod' );
#	      my ($out_dir, $outfilename) = OSspecific::absolute_path( $self -> {'directory'}.'/m'.$model_number , 
#								       '/bs_pr'.$i.'_'.$j.'.lst' );
	      my $prob_copy = Storable::dclone($problems[$i-1]);
#	      $Data::Dumper::Maxdepth = 2;
#	      print Dumper $model; die;

	      $new_mod = model ->
		  new(reference_object     => $model,
		      sde                  => 0,
		      outputs              => undef,
		      datas                => undef,
		      synced               => undef,
		      problems             => undef,
		      active_problems      => undef,
		      directory            => $model_dir,
		      filename             => $filename,
		      outputfile           => undef,
		      problems             => [$prob_copy],
		      extra_files          => $model -> extra_files,
		      target               => 'disk',
		      ignore_missing_files => 1 );

	      if( $self -> {'shrinkage'} ) {
		$new_mod -> shrinkage_stats( enabled => 1 );
		my @problems = @{$new_mod -> problems};
		for( my $i = 1; $i <= scalar @problems; $i++ ) {
		  $problems[ $i-1 ] -> shrinkage_module -> model( $new_mod );
		}
	      }

	      my $model_id  = $new_mod -> register_in_database;
	      push( @{$self -> {'prepared_model_ids'}}, $model_id );

	      $self -> register_tm_relation( model_ids       => [$model_id],
					     prepared_models => 1 );

	      $new_mod -> datas ( [$new_datas -> [$j]] );

	      if( $self -> {'nonparametric_etas'} or
		  $self -> {'nonparametric_marginals'} ) {
		$new_mod -> add_nonparametric_code;
	      }

	      $new_mod -> update_inits( from_output => $output );
	      $new_mod -> _write;

	      push( @new_models, $new_mod );
	    }
	    # Create a checkpoint. Log the samples and individuals.
	    open( DONE, ">".$self -> {'directory'}."/m$model_number/done.$i" ) ;
	    print DONE "Resampling from ",$orig_data -> filename, " performed\n";
	    print DONE "$samples[$i-1] samples\n";
	    while( my ( $strata, $samples ) = each %{$subjects[$i-1]} ) {
	      print DONE "Strata $strata: $samples sample_size\n";
	    }
	    print DONE "Included individuals:\n";
	    @seed = random_get_seed;
	    print DONE "seed: @seed\n";
	    for( my $k = 0; $k < scalar @{$incl_ids}; $k++ ) {
	      print DONE join(',',@{$incl_ids -> [$k]}),"\n";
	    }
	    print DONE "Included keys:\n";
	    for( my $k = 0; $k < scalar @{$incl_keys}; $k++ ) {
	      print DONE join(',',@{$incl_keys -> [$k]}),"\n";
	    }
	    close( DONE );
	    open( INCL, ">".$self -> {'directory'}."included_individuals".$model_number.".csv" ) ;
	    for( my $k = 0; $k < scalar @{$incl_ids}; $k++ ) {
	      print INCL join(',',@{$incl_ids -> [$k]}),"\n";
	    }
	    close( INCL );
	    open( KEYS, ">".$self -> {'directory'}."included_keys".$model_number.".csv" ) ;
	    open( SAMPLEKEYS, ">".$self -> {'directory'}."sample_keys".$model_number.".csv" ) ;
	    my $ninds= ($orig_data -> count_ind());
	    for( my $k = 0; $k < scalar @{$incl_keys}; $k++ ) {
	      my %sample_keys;
	      my $sample_size = scalar @{$incl_keys -> [$k]};
	      for ( my $l = 0; $l < $ninds; $l++ ) {
		$sample_keys{$incl_keys -> [$k][$l]}++;
	      }
	      for ( my $l = 0; $l < $ninds; $l++ ) {
		my $val   = defined $sample_keys{$l} ? $sample_keys{$l} : 0;
		my $extra = ($l == ($ninds-1)) ? "\n" : ',';
		print SAMPLEKEYS $val,$extra;
	      }
	      print KEYS join(',',@{$incl_keys -> [$k]}),"\n";
	    }
	    close( KEYS );
	    close( SAMPLEKEYS );
	  } else {
	    ui -> print( category => 'bootstrap',
			 message  => "Recreating bootstrap from previous run." );

	    # Recreate the datasets and models from a checkpoint
	    my ($stored_filename, $stored_samples, %stored_subjects);
	    my @seed;
	    my ($stored_filename_found, $stored_samples_found, $stored_subjects_found, $stored_seed_found);
	    open( DONE, $self -> {'directory'}."/m$model_number/done.$i" );
	    while( <DONE> ){
	      if( /^Resampling from (.+) performed$/ ){
		$stored_filename = $1;
		$stored_filename_found = 1;
		next;
	      }
	      if( /^(\d+) samples$/ ){
		ui -> print( category => 'bootstrap',
			     message  => "Samples: $1" );
		$stored_samples = $1;
		$stored_samples_found = 1;
		next;
	      }
	      if( /^(\d+) subjects$/ ){
		  # Old format (pre 2.2.2)
		$stored_subjects{'default'} = $1;
		$stored_subjects_found = 1;
		next;
	      }
	      if( /^Strata (\w+): (\d+) sample_size$/ ){
		ui -> print( category => 'bootstrap',
			     message  => "Strata $1, samples size: $2" );
		$stored_subjects{$1} = $2;
		$stored_subjects_found = 1;
		next;
	      }
	      if( /^seed: (\d+) (\d+)$/ ){
		@seed = ($1, $2);
		$stored_seed_found = 1;
		next;
	      }

	      if( $stored_filename_found and $stored_samples_found 
		  and $stored_subjects_found and $stored_seed_found ){
		last;
	      }		
	    }
	    close( DONE );
	    unless( $stored_filename_found and $stored_samples_found 
		    and $stored_samples_found and $stored_seed_found ){
	      debug -> die( level => 1,
			     message => "The bootstrap/m1/done file could not be parsed." );
	    }

	    if ( $stored_samples < $samples[$i-1] ) {
	      debug -> die( message => "The number of samples saved in previous run ($stored_samples) ".
			    "are bigger than the number of samples specified for this run (".
			    $samples[$i-1].")" );
	    }
	    while( my ( $strata, $samples ) = each %{$subjects[$i-1]} ) {
	      if ( $stored_subjects{$strata} != $samples ) {
		debug -> die( message => "The number of individuals sampled i strata $strata ".
			      "in previous run (".
			      $stored_subjects{$strata}.
			      ") does not match the number of individuals specified ".
			      "for this run (".$samples.")" );
	      }
	    }
	    while( my ( $strata, $samples ) = each %stored_subjects ) {
	      if ( $subjects[$i-1]->{$strata} != $samples ) {
		debug -> die( message => "The number of individuals sampled i strata $strata ".
			      "in previous run (". $samples .
			      ") does not match the number of individuals specified ".
			      "for this run (".$subjects[$i-1]->{$strata}.")" );
	      }
	    }
#	    my ( @stored_ids, @stored_keys );
#	    for ( my $k = 4; $k < 4+$stored_samples; $k++ ) {
#	      chomp($rows[$k]);
#	      my @sample_ids = split(',', $rows[$k] );
#	      push( @stored_ids, \@sample_ids );
#	    }
#	    for ( my $k = 5+$stored_samples; $k < 5+2*$stored_samples; $k++ ) {
#	      chomp($rows[$k]);
#	      my @sample_keys = split(',', $rows[$k] );
#	      push( @stored_keys, \@sample_keys );
#	    }
#	    @seed = split(' ',$rows[5+2*$stored_samples]);
#	    $incl_ids = \@stored_ids;
#	    $incl_keys = \@stored_keys;
#	    shift( @seed ); # get rid of 'seed'-word
	    # Reinitiate the model objects
	    for ( my $j = 1; $j <= $samples[$i-1]; $j++ ) {
	      my ($model_dir, $filename) = OSspecific::absolute_path( $self -> {'directory'}.'/m'.
								      $model_number,
								      'bs_pr'.$i.'_'.$j.'.mod' );
#	      my ($out_dir, $outfilename) = OSspecific::absolute_path( $self -> {'directory'}.'/m'.
#								       $model_number,
#								       '/bs_pr'.$i.'_'.$j.'.lst' );
	      $new_mod = model ->
		new( directory   => $model_dir,
		     filename    => $filename,
#		     outputfile  => $outfilename,
		     extra_files => $model -> extra_files,
		     target      => 'disk',
		     ignore_missing_files => 1 );
#	      $new_mod -> target( 'disk' );
	      push( @new_models, $new_mod );
#	      print "$j\n";
#	      print Dumper $new_mod;
#	      sleep(10);
	    }
	    random_set_seed( @seed );
	    ui -> print( category => 'bootstrap',
			 message  => "Using $stored_samples previously resampled ".
			 "bootstrap sets from $stored_filename" )
	      unless $self -> {'parent_threads'} > 1;
	  }
#	  push( @{$self -> {'included_inds'} -> [$model_number-1]}, $incl_ids );
#	  push( @{$self -> {'included_keys'} -> [$model_number-1]}, $incl_keys );
 	}
 	$self -> {'prepared_models'}[$model_number-1]{'own'} = \@new_models;

	# ---------------------  Create the sub tools  ------------------------------

	# {{{ sub tools

 	my $subdir = $class;
 	$subdir =~ s/tool:://;
 	my @subtools = @{$self -> {'subtools'}};
 	shift( @subtools );
 	my %subargs = ();
 	if ( defined $self -> {'subtool_arguments'} ) {
	  %subargs = %{$self -> {'subtool_arguments'}};
	}
	push( @{$self -> {'tools'}},
	      $class ->
	      new( reference_object      => $self,
		   models		 => \@new_models,
		   threads               => $subm_threads,
		   directory             => $self -> {'directory'}.'/'.$subdir.'_dir'.$model_number,
		   _raw_results_callback => $self ->
		          _modelfit_raw_results_callback( model_number => $model_number ),
		   subtools              => \@subtools,
		   parent_threads        => $own_threads,
		   parent_tool_id        => $self -> {'tool_id'},
		   logfile		 => $self -> {'logfile'}[$model_number-1],
		   raw_results           => undef,
		   prepared_models       => undef,
		   top_tool              => 0,
		   %subargs ) );

# 	      ( clean            => $self -> {'clean'},
# 		base_directory	 => $self -> {'directory'},
#  		compress	 => $self -> {'compress'},
# 		directory	 => $self -> {'directory'}.'/'.$subdir.'_dir'.$model_number,
# 		drop_dropped     => $self -> {'drop_dropped'},
# 		wrap_data        => $self -> {'wrap_data'},
# 		run_on_nordugrid => $self -> {'run_on_nordugrid'},
# 		cpu_time	 => $self -> {'cpu_time'},
# 		run_on_lsf       => $self -> {'run_on_lsf'},
# 		lsf_queue        => $self -> {'lsf_queue'},
# 		lsf_options      => $self -> {'lsf_options'},
# 		lsf_job_name     => $self -> {'lsf_job_name'},
# 		lsf_project_name => $self -> {'lsf_project_name'},

# 		parent_tool_id   => $self -> {'tool_id'},
		
# 		models		 => \@new_models,
# 		grid_batch_size	 => $self -> {'grid_batch_size'},
# 		nm_version	 => $self -> {'nm_version'},
# 		picky		 => $self -> {'picky'},
# 		retries		 => $self -> {'retries'},
# 		tweak_inits	 => $self -> {'tweak_inits'},
# 		handle_maxevals	 => $self -> {'handle_maxevals'},
# 		_raw_results_callback => $self ->
# 		_modelfit_raw_results_callback( model_number => $model_number ),
# 		threads		      => $subm_threads,
# 		subtools	      => \@subtools,
# 		logfile		      => $self -> {'logfile'}[$model_number-1],
# 		parent_threads	      => $own_threads,
# 		%subargs ) );

	# }}} sub tools

      }
end general_setup

# }}}

# {{{ llp_setup
start llp_setup
      {
	my @subm_threads;
	if (ref( $self -> {'threads'} ) eq 'ARRAY') {
	  @subm_threads = @{$self -> {'threads'}};
	  unshift(@subm_threads);
	} else {
	  @subm_threads = ($self -> {'threads'});
	}
	$self -> general_setup( model_number => $model_number,
				class        => 'tool::llp',
			        subm_threads => \@subm_threads );
      }
end llp_setup
# }}} llp_setup

# {{{ _jackknife_raw_results_callback

start _jackknife_raw_results_callback
      {
	# Use the bootstrap's raw_results file.
	my ($dir,$file) =
	  OSspecific::absolute_path( $self -> {'directory'},
				     $self -> {'raw_results_file'}[$model_number-1] );
	my ($dir,$nonp_file) =
	  OSspecific::absolute_path( $self -> {'directory'},
				     $self -> {'raw_nonp_file'}[$model_number-1] );
	$subroutine = sub {
	  my $jackknife = shift;
	  my $modelfit  = shift;
	  $modelfit -> raw_results_file( $dir.$file );
	  $modelfit -> raw_nonp_file( $dir.$nonp_file );
	  $modelfit -> raw_results_append( 1 );
	  my ( @new_header, %param_names );
	  foreach my $row ( @{$modelfit -> {'raw_results'}} ) {
	    unshift( @{$row}, 'jackknife' );
	  }
	  $modelfit -> {'raw_results_header'} = [];
	};
	return $subroutine;
      }
end _jackknife_raw_results_callback

# }}} _jackknife_raw_results_callback

# {{{ _modelfit_raw_results_callback

start _modelfit_raw_results_callback
      {
	# Use the bootstrap's raw_results file.
	my ($dir,$file) = 
	  OSspecific::absolute_path( $self -> {'directory'},
				     $self -> {'raw_results_file'}[$model_number-1] );
	my ($dir,$nonp_file) = 
	  OSspecific::absolute_path( $self -> {'directory'},
				     $self -> {'raw_nonp_file'}[$model_number-1] );
	my $orig_mod = $self -> {'models'}[$model_number-1];
	my $type = $self -> {'type'};
	$subroutine = sub {
	  my $modelfit = shift;
	  my $mh_ref   = shift;
	  my %max_hash = %{$mh_ref};
	  $modelfit -> raw_results_file( $dir.$file );
	  $modelfit -> raw_nonp_file( $dir.$nonp_file );

	  # The prepare_raw_results in the modelfit will fix the
	  # raw_results for each bootstrap sample model, we must add
	  # the result for the original model.

	  my %dummy;

	  my ($raw_results_row, $nonp_rows) = $self -> create_raw_results_rows( max_hash => $mh_ref,
										model => $orig_mod,
										raw_line_structure => \%dummy );

	  $orig_mod -> outputs -> [0] -> flush;
	  
	  unshift( @{$modelfit -> {'raw_results'}}, @{$raw_results_row} );

	  # {{{ New header

	  if ( $type eq 'bca' ) {
	    foreach my $row ( @{$modelfit -> {'raw_results'}} ) {
	      unshift( @{$row}, 'bootstrap' );
	    }
	    unshift( @{$modelfit -> {'raw_results_header'}}, 'method' );
	  }

	  @{$self -> {'raw_results_header'}} = @{$modelfit -> {'raw_results_header'}};

	  # }}} New header

	};
	return $subroutine;
      }
end _modelfit_raw_results_callback

# }}} _modelfit_raw_results_callback

# {{{ modelfit_analyze

start modelfit_analyze
      {
	my @params = @{$self -> {'parameters'}};
	my @diagnostic_params = @{$self -> {'diagnostic_parameters'}};
	my ( @print_order, @calculation_order );

	if ( $self -> {'type'} eq 'bca' ) {

	  # --------------------------  BCa method  ---------------------------------
	  
	  # {{{ BCa

	  @calculation_order = @{$self -> {'bca_calculation_order'}};
	  @print_order = @{$self -> {'bca_print_order'}};
	  my $jk_threads = ref( $self -> {'threads'} ) eq 'ARRAY' ? 
	    $self -> {'threads'} -> [1]:$self -> {'threads'};
	  my $done = ( -e $self -> {'directory'}."/jackknife_done.$model_number" ) ? 1 : 0;
	  if ( not $done ) {

	    # {{{ Create Jackknife

	    ui -> print( category => 'bootstrap',
			 message  => "Running a Jackknife for the BCa estimates" );
	    $self -> {'jackknife'} = tool::cdd::jackknife ->
	      new( models           => [$self -> models -> [$model_number -1]],
		   case_columns     => $self -> models -> [$model_number -1]
		   -> datas -> [0] -> idcolumn,
		   _raw_results_callback => $self ->
		   _jackknife_raw_results_callback( model_number => $model_number ),
		   nm_version       => $self -> {'nm_version'},
		   parent_tool_id   => $self -> {'tool_id'},
		   threads          => $jk_threads,
		   bca_mode         => 1,
		   shrinkage        => $self -> {'shrinkage'},
		   nonparametric_marginals => $self -> {'nonparametric_marginals'},
		   nonparametric_etas => $self -> {'nonparametric_etas'},
		   adaptive         => $self -> {'adaptive'},
		   rerun            => $self -> {'rerun'},
		   verbose          => $self -> {'verbose'},
		   cross_validate   => 0 );
	    # Create a checkpoint. Log the samples and individuals.
	    open( DONE, ">".$self -> {'directory'}."/jackknife_done.$model_number" ) ;
	    print DONE "Jackknife directory:\n";
	    print DONE $self -> {'jackknife'} -> directory,"\n";
	    my @seed = random_get_seed;
	    print DONE "seed: @seed\n";
	    close( DONE );

	    # }}} Create Jackknife

	  } else {

	    # {{{ Recreate Jackknife

	    open( DONE, $self -> {'directory'}."/jackknife_done.$model_number" );
	    my @rows = <DONE>;
	    close( DONE );
	    my ( $stored_directory ) = $rows[1];
	    chomp( $stored_directory );
	    if ( not -e $stored_directory ) {
	      debug -> die( message => "The Jackknife directory ".$stored_directory.
			    "indicated by ".$self -> {'directory'}.
			    "/jackknife_done.$model_number".
			    " from the old bootstrap run in ".
			    $self -> {'directory'}." does not exist" );
	    }
	    my @seed = split(' ',$rows[2]);
	    shift( @seed ); # get rid of 'seed'-word
	    $self -> {'jackknife'} = tool::cdd::jackknife ->
	      new( models           => [$self -> models -> [$model_number -1]],
		   case_columns     => $self -> models -> [$model_number -1]
		   -> datas -> [0] -> idcolumn,
		   _raw_results_callback => $self ->
		   _jackknife_raw_results_callback( model_number => $model_number ),
		   threads          => $jk_threads,
		   parent_tool_id   => $self -> {'tool_id'},
		   directory        => $stored_directory,
		   bca_mode         => 1,
		   shrinkage        => $self -> {'shrinkage'},
		   nm_version       => $self -> {'nm_version'},
		   nonparametric_marginals => $self -> {'nonparametric_marginals'},
		   nonparametric_etas => $self -> {'nonparametric_etas'},
		   adaptive         => $self -> {'adaptive'},
		   rerun            => $self -> {'rerun'},
		   verbose          => $self -> {'verbose'},
		   cross_validate   => 0 );
	    random_set_seed( @seed );
	    ui -> print( category => 'bootstrap',
			 message  => "Restarting BCa Jackknife from ".
			 $stored_directory )
	      unless $self -> {'parent_threads'} > 1;

	    # }}} Recreate Jackknife

	  }

	  $self -> {'jackknife'} -> run;
	  $self -> {'jackknife_results'} = $self -> {'jackknife'} -> {'results'};
	  $self -> {'jackknife_prepared_models'} = $self -> {'jackknife'} -> {'prepared_models'};

	  $self -> {'jackknife_raw_results'}[$model_number-1] =
	    $self -> {'jackknife'} -> raw_results;
#	    $self -> {'jackknife'} -> raw_results -> [$model_number-1];
#	  $Data::Dumper::Maxdepth = 0;
#	  print Dumper $self -> {'jackknife_raw_results'};

	  # }}} BCa

	} else {
	  @calculation_order = @{$self -> {'calculation_order'}};
	  @print_order = @{$self -> {'print_order'}};
	  $self -> {'bootstrap_raw_results'}[$model_number-1] =
	    $self -> {'tools'} -> [0] -> raw_results;
#	    $self -> {'tools'} -> [0] -> raw_results -> [$model_number-1];
	}
	unless( ref($self -> {'raw_results_header'}[0]) eq 'ARRAY' ) {
	  my $tmp = $self -> {'raw_results_header'};
	  $self -> {'raw_results_header'} = [];
	  $self -> {'raw_results_header'}[$model_number-1] = $tmp;
	}

	my @param_names = @{$self -> models -> [$model_number -1] -> outputs -> [0] -> labels};
	my ( @diagnostic_names, @tmp_names );
	foreach my $param ( @diagnostic_params ) {
	  push( @tmp_names, $param );
	  $tmp_names[$#tmp_names] =~ s/_/\./g;
	}
	for ( my $i = 0; $i <= $#param_names; $i++ ) {
	  unshift( @{$param_names[$i]}, 'OFV' );
	  push( @{$diagnostic_names[$i]}, @tmp_names );
	}
	if( defined $PsN::config -> {'_'} -> {'R'} and
	    -e $PsN::lib_dir . '/R-scripts/bootstrap.R' ) {
	  # copy the bootstrap R-script
	  cp ( $PsN::lib_dir . '/R-scripts/bootstrap.R', $self -> {'directory'} );
	  # Execute the script
	  system( $PsN::config -> {'_'} -> {'R'}." CMD BATCH bootstrap.R" );
	}
      }
end modelfit_analyze

# }}}

# {{{ prepare_results

start prepare_results
      {
	# {{{ definitions

	# The '3' is there to skip model, problem and subproblem numbers
	my $skip_mps = 3;
	my ( @calculation_order, @print_order, %diag_idx );
	if ( $self -> {'type'} eq 'bca' ) {
	  @calculation_order = @{$self -> {'bca_calculation_order'}};
	  @print_order = @{$self -> {'bca_print_order'}};
	} else {
	  @calculation_order = @{$self -> {'calculation_order'}};
	  @print_order = @{$self -> {'print_order'}};
	}
	if ( $self -> {'type'} eq 'bca' ) {
#	  $self -> read_raw_results();
	  $self -> bca_read_raw_results();
#	    if ( not defined $self -> {'bootstrap_raw_results'} );
	} else {
#	  if ( not defined $self -> {'raw_results'} ) {
	    $self -> read_raw_results();
	    $self -> {'bootstrap_raw_results'} = $self -> {'raw_results'};
#	  }
	}

	for ( my $i = 0; $i < scalar @{$self -> {'diagnostic_parameters'}}; $i++ ) {
	  $diag_idx{$self -> {'diagnostic_parameters'} -> [$i]} = $i + $skip_mps;
	}

	# }}} definitions

	# ---------------------  Get data from raw_results  -------------------------

	# Divide the data into diagnostics and estimates. Included in estimates are
	# the parametric estimates, the standard errors of these, the nonparametric
	# estimates, the shrinkage in eta and the shrinkage in wres
        # The diagnostics end up in {'bootstrap_diagnostics'} and
	# {'jackknife_diagnostics'}. The estimates in {'bootstrap_estimates'} and
	# {'jackknife_estimates'}.
        # The number of runs that are selected for calculation of the results is
	# saved.

        # {{{ Get the data from the runs

	foreach my $tool ( 'bootstrap', 'jackknife' ) {
	  if ( defined $self -> {$tool.'_raw_results'} ) {
	    for ( my $i = 0; $i < scalar @{$self->{$tool.'_raw_results'}}; $i++ ) { # All models

	      # {{{ Get the number of columns with estimates 

	      my $cols_orig = 0;
	      foreach my $param ( 'theta', 'omega', 'sigma' ) {
		my $labels =
		  $self -> {'models'} -> [$i] -> labels( parameter_type => $param );
		# we can't use labels directly since different models may have different
		# labels (still within the same modelfit)
		my $numpar = scalar @{$labels -> [0]} if ( defined $labels and
							   defined $labels -> [0] );
		$cols_orig += ( $numpar*3 ); # est + SE + eigen values
	      }
	      # nonparametric omegas and shrinkage
	      my $nomegas = $self -> {'models'} -> [$i] -> nomegas;
	      my $numpar = $nomegas -> [0];

              # shrinkage omega + wres shrinkage
	      $cols_orig += $numpar + 1; 
#	      $cols_orig += ($numpar*($numpar+1)/2 + $numpar + 1); 
	      
              $cols_orig++; # OFV

	      # }}} Get the number of columns with estimates 

	      # {{{ Loop, choose and set diagnostics and estimates

	      my %return_section;
	      $return_section{'name'} = 'Warnings';
	      my ( $skip_term, $skip_cov, $skip_warn, $skip_bound );
	      my $included = 0;
#	      print Dumper $self->{$tool.'_raw_results'};
	      for ( my $j = 0; $j < scalar @{$self->{$tool.'_raw_results'}->[$i]}; $j++ ) { # orig model + prepared_models
		my $columns = scalar @{$self->{$tool.'_raw_results'}->[$i][$j]};

		# -----------------------  Diagnostics  -----------------------------

		for ( my $m = $skip_mps; $m < scalar @{$self -> {'diagnostic_parameters'}} + $skip_mps; $m++ ) { # value
		  $self->{$tool.'_diagnostics'}->[$i][$j][$m-$skip_mps] =
		      $self->{$tool.'_raw_results'}->[$i][$j][$m];
		}
		my $use_run = 1;
		if ( $self -> {'skip_minimization_terminated'} and 
		     ( not defined $self->{$tool.'_raw_results'}->
		       [$i][$j][$diag_idx{'minimization_successful'}]
		       or not $self->{$tool.'_raw_results'}->
		       [$i][$j][$diag_idx{'minimization_successful'}] ) ) {
		  $skip_term++;
		  $use_run = 0;
		} elsif ( $self -> {'skip_covariance_step_terminated'} and not
			  $self->{$tool.'_raw_results'}->
			  [$i][$j][$diag_idx{'covariance_step_successful'}] ) {
		  $skip_cov++;
		  $use_run = 0;
		} elsif ( $self -> {'skip_with_covstep_warnings'} and
			  $self->{$tool.'_raw_results'}->
			  [$i][$j][$diag_idx{'covariance_step_warnings'}] ) {
		  $skip_warn++;
		  $use_run = 0;
		} elsif ( $self -> {'skip_estimate_near_boundary'} and
			  $self->{$tool.'_raw_results'}->
			  [$i][$j][$diag_idx{'estimate_near_boundary'}] ) {
		  $skip_bound++;
		  $use_run = 0;
		} 

		# ------------------------  Estimates  ------------------------------

		if( $use_run ) {
		  for ( my $m = scalar @{$self -> {'diagnostic_parameters'}} + $skip_mps; $m < $columns; $m++ ) { # value
		    my $val = $self->{$tool.'_raw_results'}->[$i][$j][$m];
		    $self->{$tool.'_estimates'}->
			[$i][$included][$m-(scalar @{$self -> {'diagnostic_parameters'}} + $skip_mps)] = $val;
		  }
		  $included++;
		}
	      }

	      # }}} Loop, choose and set diagnostics and estimates

	      # {{{ push #runs to results

	      if ( defined $skip_term ) {
		push( @{$return_section{'values'}}, "$skip_term runs with miminization ".
		      "terminated were skipped when calculating the $tool results" );
	      }
	      if ( defined $skip_cov ) {
		push( @{$return_section{'values'}}, "$skip_cov runs with aborted ".
		      "covariance steps were skipped when calculating the $tool results" );
	      }
	      if ( defined $skip_warn ) {
		push( @{$return_section{'values'}}, "$skip_warn runs with errors from ".
		      "the covariance step were skipped when calculating the $tool results" );
	      }
	      if ( defined $skip_bound ) {
		push( @{$return_section{'values'}}, "$skip_bound runs with estimates ".
		      "near a boundary were skipped when calculating the $tool results" );
	      }
	      $return_section{'labels'} = [];
	      push( @{$self -> {'results'}[$i]{'own'}},\%return_section );

	      # }}} push #runs to results

	    }
	  }
	}

#	  $Data::Dumper::Maxdepth = 5;
#	  die Dumper $self -> {'bootstrap_diagnostics'};

        # }}} Get the data from the runs

	# ----------------------  Calculate the results  ----------------------------

 	# {{{ Result calculations

	for ( my $i = 0; $i < scalar @{$self -> {'bootstrap_raw_results'}} ; $i++ ) { # All models

	  my $mps_offset = $self -> {'bca'} ? 4 : 3; # <- this is the offset to
	                                             # diagonstic_parameters,
                                                     # which is one more for
                                                     # the method column added
                                                     # with a bca run.

	  my @param_names = @{$self -> {'raw_results_header'}[$i]}[($mps_offset + scalar @{$self -> {'diagnostic_parameters'}}) .. (scalar @{$self -> {'raw_results_header'}[$i]} - 1)];
	  my ( @diagnostic_names, @tmp_names );
	  foreach my $param ( @{$self -> {'diagnostic_parameters'}} ) {
	    push( @tmp_names, $param );
	    $tmp_names[$#tmp_names] =~ s/_/\./g;
	  }

	  @diagnostic_names = @tmp_names;
	  foreach my $result_type ( @calculation_order ) {
	    my @names = $result_type eq 'diagnostic_means' ?
		@diagnostic_names : @param_names;
	    my $calc = 'calculate_'.$result_type;
	    $self -> $calc( model_number    => ($i+1),
			    parameter_names => \@names );
	  }
	  foreach my $result_type ( @print_order ) {
	    my $name = $result_type;
	    $name =~ s/_/\./g;
	    my %return_section;
	    $return_section{'name'} = $name;
	    $return_section{'values'} = $self -> {$result_type} -> [$i];
	    $return_section{'labels'} = $self -> {$result_type.'_labels'} -> [$i];
	    push( @{$self -> {'results'}[$i]{'own'}},\%return_section );
	  }
	}

	# }}} Result calculations

      }
end prepare_results

# }}} prepare_results

# {{{ print_summary

start print_summary
      {
	sub acknowledge {
	  my $name    = shift;
	  my $outcome = shift;
	  my $file    = shift;
	  my $l = (7 - length( $outcome ))/2;
	  my $c_num = '00';
	  $c_num    = '07' if ( $outcome eq 'OK' );
	  $c_num    = '13' if ( $outcome eq 'WARNING' );
	  $c_num    = '05' if ( $outcome eq 'ERROR' );
	  # my $text = sprintf( "%-66s%2s%7s%-5s\n\n", $name, '[ ', $outcome. ' ' x $l, ' ]' );
	  my $text = sprintf( "%-66s%2s%7s%-5s", $name, '[ ', $outcome. ' ' x $l, ' ]' );
	  #  cprintf( "%-66s%2s\x03$c_num%7s\x030%-5s", $name, '[ ', $outcome. ' ' x $l, ' ]' );
	  #  my $text = cprintf( "%-66s%2s\x03$c_num%7s\x030%-5s", $name, '[ ', $outcome. ' ' x $l, ' ]' );
	  print $text, "\n\n";
	  print $file $text if defined $file;
	}

	sub sum {
	  my $arr = shift;
	  my $sum = 0;
	  for ( @{$arr} ) {
	    $sum += $_;
	  }
	  return $sum;
	}

	my $diag_number = scalar @{$self -> {'diagnostic_parameters'}} - 1;
	my %diag_idxs;
	for ( my $i = 0; $i <= $diag_number; $i++ ) {
	  $diag_idxs{$self -> {'diagnostic_parameters'} -> [$i]} = $i;
	}

	open( my $log, ">test.log" );
	for ( my $i = 0; $i < scalar @{$self -> {'bootstrap_raw_results'}} ; $i++ ) { # All models
	  print "MODEL ",$i+1,"\n" if ( scalar @{$self -> {'bootstrap_raw_results'}} > 1 );
	  if ( $self -> {'diagnostic_means'} ->
	       [$i][0][$diag_idxs{'minimization_successful'}] >
	       $self -> {'minimization_successful_limit'} ) {
	    acknowledge( 'Successful minimization ratio = '.
			 $self -> {'diagnostic_means'} ->
			 [$i][0][$diag_idxs{'minimization_successful'}], 'OK', $log );
	  } else {
	    acknowledge( 'Termination problems in '.
			 sprintf("%4.2f", (100-($self -> {'diagnostic_means'} ->
						[$i][0][$diag_idxs{'minimization_successful'}]*100)))
			 .'% of the bootstrap runs', 'WARNING', $log );
	  }

	  if ( $self -> {'diagnostic_means'} ->
	       [$i][0][$diag_idxs{'covariance_step_successful'}] >
	       $self -> {'covariance_step_successful_limit'} ) {
	    acknowledge( 'Successful covariance step ratio = '.$self -> {'diagnostic_means'} ->
			 [$i][0][$diag_idxs{'covariance_step_successful'}], 'OK', $log );
	  } else {
	    acknowledge( 'Covariance step terminated in '.
			 sprintf("%4.2f", (100-($self -> {'diagnostic_means'} ->
						[$i][0][$diag_idxs{'covariance_step_successful'}]*100)))
			 .'% of the bootstrap runs', 'WARNING', $log );
	  }

	  if ( $self -> {'diagnostic_means'} ->
	       [$i][0][$diag_idxs{'covariance_step_warnings'}] <
	       $self -> {'covariance_step_warnings_limit'} ) {
	    acknowledge( 'Covariance step warnings ratio = '.$self -> {'diagnostic_means'} ->
			 [$i][0][$diag_idxs{'covariance_step_warnings'}], 'OK', $log );
	  } else {
	    acknowledge( 'Covariance step warnings in '.
			 sprintf("%4.2f", ($self -> {'diagnostic_means'} ->
					   [$i][0][$diag_idxs{'covariance_step_warnings'}]*100))
			 .'% of the bootstrap runs', 'WARNING', $log );
	  }

	  if ( $self -> {'diagnostic_means'} ->
	       [$i][0][$diag_idxs{'estimate_near_boundary'}] <
	       $self -> {'estimate_near_boundary_limit'} ) {
	    acknowledge( 'Estimate near boundary ratio = '.$self -> {'diagnostic_means'} ->
			 [$i][0][$diag_idxs{'estimate_near_boundary'}], 'OK', $log );
	  } else {
	    acknowledge( 'Estimate near boundary found in '.
			 sprintf("%4.2f", ($self -> {'diagnostic_means'} ->
					   [$i][0][$diag_idxs{'estimate_near_boundary'}]*100))
			 .'% of the bootstrap runs', 'WARNING', $log );
	  }
	  my $sum = sum( $self -> {'within_se_confidence_intervals'}->
			 [$i]{$self -> {'se_confidence_intervals_level'}} );
	  if ( not defined $sum or $sum < 1 ) {
	    acknowledge( 'No '.(100-$self -> {'se_confidence_intervals_level'}).
			 '% confidence intervals based on the'.
			 ' bootstrap standard errors include '.
			 $self -> {'se_confidence_intervals_check'}, 'OK', $log );
	  } else {
	    acknowledge( "$sum ".(100-$self -> {'se_confidence_intervals_level'}).
			 "% confidence intervals based on the bootstrap".
			 " SE's include ".
			 $self -> {'se_confidence_intervals_check'}, 'WARNING', $log );
	    my $found =
	      scalar @{$self -> {'within_se_confidence_intervals'}->
			 [$i]{$self -> {'se_confidence_intervals_level'}}} - 1;
	    for ( my $l = 0; $l <= $found ; $l++ ) {
	      if ( $self -> {'within_se_confidence_intervals'}->
		   [$i]{$self -> {'se_confidence_intervals_level'}}[$l] ) {
		printf( "\t%-20s\n",
			$self -> {'standard_error_confidence_intervals_labels'} ->
			[$i][1][$l] );
		print $log
		  sprintf( "\t%-20s\n",
			   $self -> {'standard_error_confidence_intervals_labels'} ->
			   [$i][1][$l] );
		print "\n" if ( $l == $found );
		print $log "\n" if ( $l == $found );
	      }
	    }
	  }

	  my $sum = sum( $self -> {'large_bias'}-> [$i][0] );
	  if ( not defined $sum or $sum < 1 ) {
	    acknowledge( 'No bias larger than '.
			 ($self -> {'large_bias_limit'}*100).'% found', 'OK', $log );
	  } else {
	    acknowledge( "$sum estimates were found to have a relative bias > ".
			 $self -> {'large_bias_limit'}, 'WARNING', $log );
	    my $found =
	      scalar @{$self -> {'large_bias'}->
			 [$i][0]} - 1;
	    for ( my $l = 0; $l <= $found ; $l++ ) {
	      if ( $self -> {'large_bias'}->
		   [$i][0][$l] ) {
		printf( "\t%-20s%3.2f %\n", $self -> {'bias_labels'} ->
			[$i][1][$l], ($self -> {'bias'} -> [$i][0][$l]/
				      $self->{'bootstrap_estimates'} -> [$i][0][$l])*100 );
		#		    print $log
		#		      sprintf( "\t%-20s\n",
		#			       $self -> {'percentile_confidence_intervals_labels'} ->
		#			       [$i][1][$l] );
		print "\n" if ( $l == $found );
		print $log "\n" if ( $l == $found );
	      }
	    }
	  }

	  if ( $self -> {'type'} eq 'bca' ) {
	    my $sum = sum( $self -> {'within_bca_confidence_intervals'}->
			   [$i]{$self -> {'bca_confidence_intervals_level'}} );
	    if ( not defined $sum or $sum < 1 ) {
	      acknowledge( 'No '.(100-$self -> {'bca_confidence_intervals_level'}).
			   '% BCa confidence intervals include '.
			   $self -> {'bca_confidence_intervals_check'}, 'OK', $log );
	    } else {
	      acknowledge( "$sum ".(100-$self -> {'bca_confidence_intervals_level'}).
			   "% BCa confidence intervals include ".
			   $self -> {'bca_confidence_intervals_check'}, 'WARNING', $log );
	      my $found =
		scalar @{$self -> {'within_bca_confidence_intervals'}->
			   [$i]{$self -> {'bca_confidence_intervals_level'}}} - 1;
	      for ( my $l = 0; $l <= $found ; $l++ ) {
		if ( $self -> {'within_bca_confidence_intervals'}->
		     [$i]{$self -> {'bca_confidence_intervals_level'}}[$l] ) {
		  printf( "\t%-20s\n",
			  $self -> {'bca_confidence_intervals_labels'} ->
			  [$i][1][$l] );
		  print $log
		    sprintf( "\t%-20s\n",
			     $self -> {'bca_confidence_intervals_labels'} ->
			     [$i][1][$l] );
		  print "\n" if ( $l == $found );
		  print $log "\n" if ( $l == $found );
		}
	      }
	    }
	  } else {
	    my $sum = sum( $self -> {'within_percentiles'}->
			   [$i]{$self -> {'percentile_confidence_intervals_level'}} );
	    if ( not defined $sum or $sum < 1 ) {
	      acknowledge( 'No '.(100-$self -> {'percentile_confidence_intervals_level'}).
			   '% confidence intervals based on the'.
			   ' bootstrap percentiles include '.
			   $self -> {'percentile_confidence_intervals_check'}, 'OK', $log );
	    } else {
	      acknowledge( "$sum ".(100-$self -> {'percentile_confidence_intervals_level'}).
			   "% confidence intervals based on the percentiles".
			   " include ".
			   $self -> {'percentile_confidence_intervals_check'}, 'WARNING', $log );
	      my $found =
		scalar @{$self -> {'within_percentiles'}->
			   [$i]{$self -> {'percentile_confidence_intervals_level'}}} - 1;
	      for ( my $l = 0; $l <= $found ; $l++ ) {
		if ( $self -> {'within_percentiles'}->
		     [$i]{$self -> {'percentile_confidence_intervals_level'}}[$l] ) {
		  printf( "\t%-20s\n",
			  $self -> {'percentile_confidence_intervals_labels'} ->
			  [$i][1][$l] );
		  print $log
		    sprintf( "\t%-20s\n",
			     $self -> {'percentile_confidence_intervals_labels'} ->
			     [$i][1][$l] );
		  print "\n" if ( $l == $found );
		  print $log "\n" if ( $l == $found );
		}
	      }
	    }
	  }
	}
	close ( $log );
      }
end print_summary

# }}} print_summary

# {{{ bca_read_raw_results

start bca_read_raw_results
      {
	$self -> {'raw_results_header'} = [];
	for ( my $i = 1; $i <= scalar @{$self->{'models'}}; $i++ ) { # All models
	  if ( -e $self -> {'directory'}.'raw_results'.$i.'.csv' ) {
	    open( RRES, $self -> {'directory'}.'raw_results'.$i.'.csv' );
	    my @file = <RRES>;
	    close( RRES );
	    map { chomp; my @tmp = split(',',$_); $_ = \@tmp } @file ;
	    
	    my $header = shift @file;
	    
	    # Get rid of 'method' column
	    my $cols = scalar(@{$header})-1;
	    @{$self -> {'raw_results_header'}[$i-1]} = @{$header}[1..$cols];
	    $self -> {'raw_results'} -> [$i-1] = \@file;
	    for( my $j = 0; $j <= $#file; $j++ ) {
	      if ( $file[$j][0] eq 'jackknife' ) {
		shift( @{$file[$j]} );
#		$self -> {'jackknife_raw_results'}[$i-1] = \@file;
		push( @{$self -> {'jackknife_raw_results'}[$i-1]}, $file[$j]);
	      } else {
		shift( @{$file[$j]} );
#		$self -> {'bootstrap_raw_results'}[$i-1] = \@file;
		push( @{$self -> {'bootstrap_raw_results'}[$i-1]}, $file[$j] );
	      }
	    }
	  }
	}
      }
end bca_read_raw_results

# }}} bca_read_raw_results

# {{{ calculate_diagnostic_means

start calculate_diagnostic_means
      {
	my ( @sum, @diagsum, %diag_idx );
	for ( my $i = 0; $i < scalar @{$self -> {'diagnostic_parameters'}}; $i++ ) {
	  $diag_idx{$self -> {'diagnostic_parameters'} -> [$i]} = $i;
	}

	my $def = 0;
	# Prepared model, skip the first (the original)
	for ( my $k = 1; $k < scalar @{$self -> {'bootstrap_diagnostics'} ->
					 [$model_number-1]}; $k++ ) {
	  # Diagnostics
	  if( defined $self -> {'bootstrap_diagnostics'} ->
	      [$model_number-1][$k] ) {
	    $def++;
	    for ( my $l = 0; $l < scalar @{$self -> {'bootstrap_diagnostics'} ->
					       [$model_number-1][$k]}; $l++ ) {
	      $sum[$l] += $self -> {'bootstrap_diagnostics'} ->
		  [$model_number-1][$k][$l];
	    }
	  }
	}

	# divide by the number of bootstrap samples (-1 to get rid of the original
	# model) The [0] in the index is there to indicate the 'model' level. Mostly
	# used for printing
	for ( my $l = 0; $l <= $#sum; $l++ ) {
	  if( $l == $diag_idx{'significant_digits'} ) {
	    $self -> {'diagnostic_means'} -> [$model_number-1][0][$l] =
		$sum[$l] / $def;
	  } else {
	    $self -> {'diagnostic_means'} -> [$model_number-1][0][$l] =
		$sum[$l] / ( scalar @{$self -> {'bootstrap_diagnostics'} ->
					  [$model_number-1]} - 1);
	  }
	}
	$self -> {'diagnostic_means_labels'} -> [$model_number-1] =
	  [[],\@parameter_names];
      }
end calculate_diagnostic_means

# }}} calculate_diagnostic_means

# {{{ calculate_means

start calculate_means
      {
	my ( @sum, @diagsum );
	# Prepared model, skip the first (the original)
	for ( my $k = 1; $k < scalar @{$self -> {'bootstrap_estimates'} ->
					 [$model_number-1]}; $k++ ) {
	  # Estimates
	  for ( my $l = 0; $l < scalar @{$self -> {'bootstrap_estimates'} ->
					   [$model_number-1][$k]}; $l++ ) {
	    $sum[$l] += $self -> {'bootstrap_estimates'} ->
	      [$model_number-1][$k][$l];
	  }
	}
	# divide by the number of bootstrap samples (-1 to get rid of the original
	# model) The [0] in the index is there to indicate the 'model' level. Mostly
	# used for printing
	my $samples = scalar @{$self -> {'bootstrap_estimates'} ->
				 [$model_number-1]} - 1;
	for ( my $l = 0; $l <= $#sum; $l++ ) {
	  my $mean = $sum[$l] / $samples;
	  $self -> {'means'} -> [$model_number-1][0][$l] = $mean;
	  my $bias = $mean - $self ->
	    {'bootstrap_estimates'} -> [$model_number-1][0][$l];
	  $self -> {'bias'} -> [$model_number-1][0][$l] = $bias;
	  if ( $self->{'bootstrap_estimates'} -> [$model_number-1][0][$l] != 0 and
	       $bias/$self->{'bootstrap_estimates'} -> [$model_number-1][0][$l]
	       > $self -> {'large_bias_limit'} ) {
	    $self -> {'large_bias'} -> [$model_number-1][0][$l] = 1;
	  } else {
	    $self -> {'large_bias'} -> [$model_number-1][0][$l] = 0;
	  }
	}
	$self -> {'means_labels'} -> [$model_number-1] =
	  [[],\@parameter_names];

	$self -> {'bias_labels'} -> [$model_number-1] =
	  [[],\@parameter_names];
      }
end calculate_means

# }}} calculate_means

# {{{ calculate_jackknife_means

start calculate_jackknife_means
      {
	my @sum;
	# Prepared model, skip the first (the original)
	if( defined $self -> {'jackknife_estimates'} ){
	  for ( my $k = 1; $k < scalar @{$self -> {'jackknife_estimates'}->[$model_number-1]}; $k++ ) {
	    # Estimate
	    for ( my $l = 0; $l <
		  scalar @{$self -> {'jackknife_estimates'}->[$model_number-1][$k]}; $l++ ) {
	      $sum[$l] += $self -> {'jackknife_estimates'}->[$model_number-1][$k][$l];
	    }
	  }
	  # divide by the number of jackknife samples (-1 to get rid of the original model)
	  # The [0] in the index is there to indicate the 'model' level. Mostly used for printing
	  for ( my $l = 0; $l <
		scalar @{$self -> {'jackknife_estimates'}->[$model_number-1][0]}; $l++ ) {
	    if( ( scalar @{$self -> {'jackknife_estimates'}->[$model_number-1]} - 1) != 0 ) {
	      $self -> {'jackknife_means'} -> [$model_number-1][0][$l] =
		  $sum[$l] / ( scalar @{$self -> {'jackknife_estimates'}->[$model_number-1]} - 1);
	    }
	  }
	  $self -> {'jackknife_means_labels'} -> [$model_number-1] = [[],\@parameter_names];
	}
      }
end calculate_jackknife_means

# }}} calculate_jackknife_means

# {{{ calculate_medians
start calculate_medians
      {
	my @medians;
	# Loop the parameters
	for ( my $l = 0; $l < scalar @{$self -> {'bootstrap_estimates'}->
					 [$model_number-1][0]}; $l++ ) {
	  my @parameter_array;
	  # From 1 to get rid of original model
	  for ( my $k = 1; $k < scalar @{$self -> {'bootstrap_estimates'}->
					   [$model_number-1]}; $k++ ) {
	    $parameter_array[$k-1] =
	      $self -> {'bootstrap_estimates'}->[$model_number-1][$k][$l];
	  }
	  my @sorted = sort {$a <=> $b} @parameter_array;
	  # median postition is half the ( array length - 1 ).
	  my $median_position = ( $#sorted ) / 2;
	  my ($int_med,$frac_med)   = split(/\./, $median_position );
	  $frac_med = eval("0.".$frac_med);
	  my $median_low  = $sorted[ $int_med ];
	  my $median_high = ( $sorted[ $int_med + 1 ] - $sorted[ $int_med ] ) * $frac_med;
	  $medians[$l] = $median_low + $median_high;
	}
	# The [0] in the index is there to indicate the 'model' level. Mostly used for printing
	$self -> {'medians'} -> [$model_number-1][0] = \@medians;
	$self -> {'medians_labels'} -> [$model_number-1] = [[],\@parameter_names];
      }
end calculate_medians
# }}} calculate_medians

# {{{ calculate_standard_error_confidence_intervals
start calculate_standard_error_confidence_intervals
      {
	# Sort the limits from the inside out
	my @limits = sort { $b <=> $a } keys %{$self -> {'confidence_limits'}};
	foreach my $limit ( @limits ) {
	  my ( @lower_limits, @upper_limits, @within_ci );
	  # Loop the estimates of the first (original) model
	  for ( my $l = 0; $l < scalar @{$self -> {'bootstrap_estimates'}->
					   [$model_number-1][0]}; $l++ ) {
	    my $lower_limit =
	      $self -> {'bootstrap_estimates'}->[$model_number-1][0][$l] -
		$self -> {'standard_errors'}->[$model_number-1][0][$l] *
		  $self -> {'confidence_limits'} ->{$limit};
	    my $upper_limit =
	      $self -> {'bootstrap_estimates'}->[$model_number-1][0][$l] +
		$self -> {'standard_errors'}->[$model_number-1][0][$l] *
		  $self -> {'confidence_limits'} ->{$limit};
	    push( @lower_limits, $lower_limit );
	    push(  @upper_limits, $upper_limit );
	    if ( $self -> {'se_confidence_intervals_check'} < $upper_limit and
		 $self -> {'se_confidence_intervals_check'} > $lower_limit ) {
	      push( @within_ci , 1 );
	    } else {
	      push( @within_ci , 0 );
	    }
	  }
	  unshift( @{$self -> {'standard_error_confidence_intervals'} ->
		       [$model_number-1]}, \@lower_limits );
	  push( @{$self -> {'standard_error_confidence_intervals'} ->
		    [$model_number-1]}, \@upper_limits );
	  $self -> {'within_se_confidence_intervals'} ->
	    [$model_number-1]{$limit} = \@within_ci;
	  unshift( @{$self -> {'standard_error_confidence_intervals_labels'} ->
		       [$model_number-1][0]}, ($limit/2).'%' );
	  push( @{$self -> {'standard_error_confidence_intervals_labels'} ->
		    [$model_number-1][0]}, (100-($limit/2)).'%' );
	  push( @{$self -> {'within_se_confidence_intervals_labels'} ->
		    [$model_number-1][0]}, $limit.'%' );
	}
	$self -> {'standard_error_confidence_intervals_labels'} -> [$model_number-1][1] =
	  \@parameter_names;
	$self -> {'within_se_confidence_intervals_labels'} -> [$model_number-1][1] =
	  \@parameter_names;
      }
end calculate_standard_error_confidence_intervals
# }}} calculate_standard_error_confidence_intervals

# {{{ calculate_standard_errors

start calculate_standard_errors
      {
	my @se;
	# Prepared model, skip the first (the original)
	for ( my $k = 1; $k < scalar @{$self -> {'bootstrap_estimates'}->[$model_number-1]}; $k++ ) {
	  # Estimate
	  for ( my $l = 0; $l <
		scalar @{$self -> {'bootstrap_estimates'}->[$model_number-1][$k]}; $l++ ) {
	    $se[$l] += ( $self -> {'bootstrap_estimates'}->[$model_number-1][$k][$l] - 
			 $self -> {'means'}->[$model_number-1][0][$l] )**2;
	  }
	}
	# divide by the number of bootstrap samples -1 (-2 to get rid of the original model)
	# The [0] in the index is there to indicate the 'model' level.
	for ( my $l = 0; $l <
	      scalar @{$self -> {'bootstrap_estimates'}->[$model_number-1][0]}; $l++ ) {
	  my $div = ( scalar @{$self -> {'bootstrap_estimates'}->[$model_number-1]} - 2 );
	  if( defined $div and not $div == 0 ) { 
	    $self -> {'standard_errors'} -> [$model_number-1][0][$l] =
		($se[$l] / $div  )**0.5;
	  } else {
	    $self -> {'standard_errors'} -> [$model_number-1][0][$l] = undef;
	  }
	}
	$self -> {'standard_errors_labels'} -> [$model_number-1] = [[],\@parameter_names];
      }
end calculate_standard_errors

# }}} calculate_standard_errors

# {{{ calculate_bca_confidence_intervals

start calculate_bca_confidence_intervals
      {
	sub c_get_z0 {
	  my $arr_ref  = shift;
	  my $orig_value = shift;
	  my $num_less_than_orig = 0;
	  my $nvalues = 0;
	  my $z0;
	  foreach my $value ( @{$arr_ref} ) {
	    if ( defined $value and $value ne '' ) {
	      $num_less_than_orig++ if ( $value < $orig_value );
	      $nvalues ++;
	    }
	  }

	  unless ( $nvalues == 0 ) {
	    if ( ($num_less_than_orig / $nvalues ) == 0 ) {
	      $z0 = -100;
	    } elsif ( ($num_less_than_orig / $nvalues ) == 1 ) {
	      $z0 = 100;
	    } else {
	      $z0 = udistr( 1 - ($num_less_than_orig / $nvalues ) );
	    }
	  }
#	  return ( $z0, $nvalues );
#	  print sprintf( "%4s:%5.0f,%4s:%15.8g  ",'N:',$num_less_than_orig,'ZO', $z0);
	  return $z0;
	}

	sub c_get_acc {
	  my $arr_ref = shift;
	  my $jk_mean = shift;
	  my $acc_upper = 0;
	  my $acc_lower = 0;
	  my $nvalues = 0;
	  my $acc;
	  foreach my $value ( @{$arr_ref} ){
	    if ( defined $value and $value ne '' ) {
	      $acc_upper = $acc_upper + ($jk_mean-$value)**3;
	      $acc_lower = $acc_lower + ($jk_mean-$value)**2;
	      $nvalues ++;
	    }
	  }
	  $acc_lower = 6*($acc_lower**(3/2));
	  unless ( $acc_lower == 0 ) {
	    $acc = $acc_upper / $acc_lower;
	  } else {
	    $acc = $acc_upper / 0.001;
	  }
#	  return ( $acc, $nvalues );
#	  print sprintf( "%4s:%15.8g%4s%8.5g\n",'ACC', $acc,'JKm', $jk_mean);
	  return $acc;
	}

	sub c_get_alphas {
	  my $old_alphas = shift;
	  my $acc = shift;
	  my $z0 = shift;
	  my $denom;
	  my @new_alphas = ();
	  foreach my $position ( @{$old_alphas} ) {
	    if ( $position == 0 ){
	      $denom = -100;
	    } elsif ( $position == 100 ) {
	      $denom = 100;
	    } else {
	      $denom = $z0 + udistr( 1 - $position/100 );
	    }
	    my $nom     = 1 - $acc * $denom;
	    my $lim = 100*uprob( - ( $z0 + $denom / $nom ) );
	    push( @new_alphas, $lim );
	  }
#	  print "@new_alphas\n";
	  return \@new_alphas;
	}

	my @limits = sort { $a <=> $b } keys %{$self -> {'confidence_limits'}};
	# Add the upper limits
	my $limnum = $#limits;
	for ( my $i = $limnum; $i >= 0; $i-- ) {
	  $limits[$i] = $limits[$i]/2;
	  push( @limits, 100-$limits[$i] );
	}
	my ( @bootstrap_array, @jackknife_array, @new_alphas, @z0, @acc );
	# Loop the estimates of the first (original) model
	for ( my $l = 0; $l < scalar @{$self -> {'bootstrap_estimates'}->
					 [$model_number-1][0]}; $l++ ) {
	  my ( @unsorted_array1, @unsorted_array2 );
	  # Loop the bootstrap samples from 1 to get rid of original model
	  for ( my $k = 1; $k < scalar @{$self -> {'bootstrap_estimates'}->
					   [$model_number-1]}; $k++ ) {
	    $unsorted_array1[$k-1] =
	      $self -> {'bootstrap_estimates'}->[$model_number-1][$k][$l];
	  }
	  @{$bootstrap_array[$l]} = sort {$a <=> $b} @unsorted_array1;

	  # Loop the jackknife samples from 1 to get rid of original model
	  for ( my $k = 1; $k < scalar @{$self -> {'jackknife_estimates'}->
					   [$model_number-1]}; $k++ ) {
	    $unsorted_array2[$k-1] =
	      $self -> {'jackknife_estimates'}->[$model_number-1][$k][$l];
	  }
	  @{$jackknife_array[$l]} = sort {$a <=> $b} @unsorted_array2;
	  $z0[$l]         = c_get_z0     ( $bootstrap_array[$l],
					   $self -> {'bootstrap_estimates'} ->
					   [$model_number-1][0][$l] );
	  $acc[$l]        = c_get_acc    ( $jackknife_array[$l],
					   $self -> {'jackknife_means'} ->
					   [$model_number-1][0][$l] );
	  $new_alphas[$l] = c_get_alphas ( \@limits, $acc[$l], $z0[$l] );
	}
	# Loop limits
	for ( my $lim_idx = 0; $lim_idx <= $#limits; $lim_idx++ ) {
	  my @percentiles;
	  # Loop parameters
	  for ( my $l = 0; $l <= $#bootstrap_array; $l++ ) {
	    my $limit = $new_alphas[$l][$lim_idx]/100;
	    my $position = ( scalar @{$bootstrap_array[$l]} + 1 ) * $limit;
	    my $percentile;
	    if ( $position < 1 ) {
	      $percentile = undef;
	    } elsif ( $position > scalar @{$bootstrap_array[$l]} ) {
	      $percentile = undef;
	    } else {
	      my ($int_med,$frac_med)   = split(/\./, $position );
	      $frac_med = eval("0.".$frac_med);
	      my $percentile_low  = $bootstrap_array[$l][ $int_med - 1];
	      my $percentile_high = ( $bootstrap_array[$l][ $int_med ] -
				      $bootstrap_array[$l][ $int_med - 1] ) * $frac_med;
	      $percentile = $percentile_low + $percentile_high;
	    }
	    push( @percentiles, $percentile );
	  }
	  push( @{$self -> {'bca_confidence_intervals'} -> [$model_number-1]},
		\@percentiles );
	  push( @{$self -> {'bca_confidence_intervals_labels'}->[$model_number-1][0]},
		$limits[$lim_idx].'%');
	}
	# Check the intervals
	for ( my $lim_idx = 0; $lim_idx <= $limnum; $lim_idx++ ) {
	  my @within_ci;
	  for ( my $l = 0; $l <= $#bootstrap_array; $l++ ) {
	    my $lower_limit = $self -> {'bca_confidence_intervals'} ->
	      [$model_number-1][$lim_idx][$l];
	    my $upper_limit = $self -> {'bca_confidence_intervals'} ->
	      [$model_number-1][($limnum*2+1)-$lim_idx][$l];
	    if ( $self -> {'bca_confidence_intervals_check'} < $upper_limit and
		 $self -> {'bca_confidence_intervals_check'} > $lower_limit ) {
	      push( @within_ci , 1 );
	    } else {
	      push( @within_ci , 0 );
	    }
	  }
	  $self -> {'within_bca_confidence_intervals'} ->
	    [$model_number-1]{$limits[$lim_idx]*2} = \@within_ci;
	}
	$self -> {'bca_confidence_intervals_labels'} -> [$model_number-1][1] =
	  \@parameter_names;
      }
end calculate_bca_confidence_intervals

# }}} calculate_bca_confidence_intervals

# {{{ calculate_percentile_confidence_intervals

start calculate_percentile_confidence_intervals
      {
	# Sort the limits from the inside out
	my @limits = sort { $b <=> $a } keys %{$self -> {'confidence_limits'}};
	foreach my $limit ( @limits ) {
	  my ( @lower_limits, @upper_limits, @within_ci );
	  # Loop the estimates of the first (original) model
	  for ( my $l = 0; $l < scalar @{$self -> {'bootstrap_estimates'}->
					   [$model_number-1][0]}; $l++ ) {
	    my @parameter_array;
	    # Loop the bootstrap samples from 1 to get rid of original model
	    for ( my $k = 1; $k < scalar @{$self -> {'bootstrap_estimates'}->
					     [$model_number-1]}; $k++ ) {
	      my $val = $self -> {'bootstrap_estimates'}->[$model_number-1][$k][$l];
	      # get rid of undefined values (these were probably deleted
	      # when the bootstrap_estimates was created
	      push( @parameter_array, $val ) if( defined $val );
	    }
	    my @sorted = sort {$a <=> $b} @parameter_array;
	    for my $side ( 'lower', 'upper' ) {
	      my $use_limit = $side eq 'lower' ? $limit/200 : 1-($limit/200);
	      # percentile postition is:
	      my $percentile_position = ( $#sorted + 2 ) * $use_limit;
	      my $percentile;
	      if ( $percentile_position < 1 ) {
		$percentile = undef;
	      } elsif ( $percentile_position > $#sorted +1) {
		$percentile = undef;
	      } else {
		my ($int_med,$frac_med)   = split(/\./, $percentile_position );
		$frac_med = eval("0.".$frac_med);
		my $percentile_low  = $sorted[ $int_med - 1];
		my $percentile_high = ( $sorted[ $int_med ] - $sorted[ $int_med - 1] ) * $frac_med;
		$percentile = $percentile_low + $percentile_high;
	      }
	      push( @lower_limits, $percentile ) if ( $side eq 'lower' );
	      push( @upper_limits, $percentile ) if ( $side eq 'upper' );
	    }
	    if ( $self -> {'percentile_confidence_intervals_check'} < $upper_limits[$#upper_limits] and
		 $self -> {'percentile_confidence_intervals_check'} > $lower_limits[$#lower_limits] ) {
	      push( @within_ci , 1 );
	    } else {
	      push( @within_ci , 0 );
	    }
	  }
	  unshift( @{$self -> {'percentile_confidence_intervals'} ->
		       [$model_number-1]}, \@lower_limits );
	  push( @{$self -> {'percentile_confidence_intervals'} ->
		    [$model_number-1]}, \@upper_limits );
	  unshift( @{$self -> {'percentile_confidence_intervals_labels'}->
		       [$model_number-1][0]}, ($limit/2).'%' );
	  push( @{$self -> {'percentile_confidence_intervals_labels'}->
		    [$model_number-1][0]},(100-($limit/2)).'%');
	  $self -> {'within_percentiles'}->[$model_number-1]{$limit}=\@within_ci;
	}
	$self -> {'percentile_confidence_intervals_labels'} ->
	  [$model_number-1][1] = \@parameter_names;
      }
end calculate_percentile_confidence_intervals

# }}} calculate_percentile_confidence_intervals

# {{{ modelfit_post_fork_analyze

start modelfit_post_fork_analyze
end modelfit_post_fork_analyze

# }}}

# {{{ resample

start resample
      {
	my $dataObj = $model -> datas -> [0];
	for( my $i = 1; $i <= $self -> {'samples'}; $i++ ) {
	  my ($bs_dir, $bs_name) = OSspecific::absolute_path( $self -> {'directory'}, "bs$i.dta" );
	  my $new_name = $bs_dir . $bs_name;  
	  my $boot_sample = $dataObj -> resample( 'subjects' => $self -> {'subjects'},
						  'new_name' => $new_name,
						  'target'   => $target );
	  my $newmodel = $model -> copy( filename => "bs$i.mod",
					 target   => $target,
					 ignore_missing_files => 1 );
	  $newmodel -> datafiles( new_names => ["bs$i.dta"] );
	  $newmodel -> datas -> [0] = $boot_sample ;
	  $newmodel -> write;
	  push( @resample_models, $newmodel );
	}
      }
end resample

# }}} resample

# {{{ _sampleTools

start _sampleTools
      {
	foreach my $tool ( @{$self -> {'tools'}} ) {
	  my @models = @{$tool -> models};
	  foreach my $model (@models){
	    my $dataObj = $model -> datas -> [0];
	    for( my $i = 1; $i <= $samples; $i++ ) {
	      my $boot_sample = $dataObj -> resample( 'subjects' => $self -> {'subjects'},
						      'new_name' => "bs$i.dta",
						      'target' => $target );
	      my $newmodel;
	      $newmodel = $model -> copy( filename => "bs$i.mod" );
	      $newmodel -> datafiles( new_names => ["bs$i.dta"] );
	      $newmodel -> datas -> [0] = $boot_sample ;
	      $newmodel -> write;
	      if( defined( $tool -> models ) ){
		push( @{$tool -> models}, $newmodel );
	      } else {
		$tool -> models( [ $newmodel ] );
	      }
	    }
	  }
	}
      }
end _sampleTools

# }}} _sampleTools

# {{{ create_matlab_scripts

start create_matlab_scripts
    {
      if( defined $PsN::lib_dir ){
	unless( -e $PsN::lib_dir . '/histograms.m' and
		-e $PsN::lib_dir . '/bca.m' ){
	  'debug' -> die( message => 'Bootstrap matlab template scripts are not installed, no matlab scripts will be generated.' );
	  return;
	}

	open( PROF, $PsN::lib_dir . '/histograms.m' );
	my @file = <PROF>;
	close( PROF );
	my $found_code;
	my $code_area_start=0;
	my $code_area_end=0;


	for(my $i = 0;$i < scalar(@file); $i++) {
	  if( $file[$i] =~ /% ---------- Autogenerated code below ----------/ ){
	    $found_code = 1;
	    $code_area_start = $i;
	  }
	  if( $file[$i] =~ /% ---------- End autogenerated code ----------/ ){
	    unless( $found_code ){
	      'debug' -> die ( message => 'Bootstrap matlab template script is malformated, no matlab scripts will be generated' );
	      return;
	    }
	    $code_area_end = $i;
	  }
	}
	
	my @auto_code;
	if( $self -> {'type'} eq 'bca' ){
	  push( @auto_code, "use_bca = 1;				% Was a BCa-type of\n" );
	} else {
	  push( @auto_code, "use_bca = 0;				% Was a BCa-type of\n" );
	}

        push( @auto_code, "                                % bootstrap run?\n" );
	if( ref $self -> {'samples'} eq 'ARRAY' ) {
	  push( @auto_code, "bs_samples = ".$self -> {'samples'}->[0][0].";			% Number of bootstrap samples\n" );
	} else {
	  push( @auto_code, "bs_samples = ".$self -> {'samples'}.";			% Number of bootstrap samples\n" );
	}	  
	if( $self -> {'type'} eq 'bca' ){
	  my $ninds = $self -> models -> [0]
	      -> datas -> [0] -> count_ind;
	  push( @auto_code, "jk_samples = $ninds;			% Number of (BCa) jackknife samples\n\n" );
	}
	
	push( @auto_code, "col_names = { 'Significant Digits',\n" );
        push( @auto_code, "	         'Condition Number',\n" );
        push( @auto_code, "	         'OFV',\n" );

	my $nps = $self -> {'models'} -> [0] -> nomegas -> [0];

	my %param_names;
	my( @par_names, @se_names, @np_names, @sh_names );
	foreach my $param ( 'theta','omega','sigma' ) {
	  my $labels = $self -> {'models'} -> [0] -> labels( parameter_type => $param );
	  if ( defined $labels ){
	    foreach my $label ( @{$labels -> [0]} ){
	      push( @par_names, "	         '",$label,"',\n" );
	      push( @se_names, "	         '",'se-'.$label,"',\n" );
	    }
	  }
	}

	for( my $i = 1; $i <= ($nps*($nps+1)/2); $i++ ) {
	  push( @np_names, "	         '",'np-om'.$i,"',\n" );
	}

	for( my $i = 1; $i <= $nps; $i++ ) {
	  push( @sh_names, "	         '",'shrinkage-eta'.$i,"',\n" );
	}

	push( @sh_names, "	         '",'shrinkage-iwres',"'\n" );

# NP not used for now

	push( @auto_code,(@par_names, @se_names, @sh_names));
#	push( @auto_code,(@par_names, @se_names, @np_names, @sh_names));
        push( @auto_code, "	      };\n\n" );

	my @np_columns = (0) x ($nps*($nps+1)/2);
	my @sh_columns = (0) x ($nps+1);

	if( $self -> {'type'} eq 'bca' ){
	  push( @auto_code, "fixed_columns = [ 0, 0, 0, " );
	} else {
	  push( @auto_code, "fixed_columns = [ 0, 0, 0, " );
	}
	my ( @fixed_columns, @same_columns, @adjust_axes );
	foreach my $param ( 'theta','omega','sigma' ) {
	  my $fixed = $self -> {'models'} -> [0] -> fixed( parameter_type => $param );

	  if ( defined $fixed ){
	    push( @fixed_columns, @{$fixed -> [0]} );
	    if( $param eq 'theta' ) {
	      push( @same_columns, (0) x scalar( @{$fixed -> [0]} ) );
	    }
	  }
	}

	@adjust_axes = (1) x ( ($#fixed_columns + 1) * 2 +
			       $#sh_columns + 1 );
#			       $#np_columns + $#sh_columns + 2 );

        push( @auto_code , join( ', ' , @fixed_columns).', '.
	      join( ', ' , @fixed_columns).', '.
#	      join( ', ' , @np_columns).', '.
	      join( ', ' , @sh_columns)."];\n\n" );

	if( $self -> {'type'} eq 'bca' ){
	  push( @auto_code, "same_columns  = [ 0, 0, 0, " );
	} else {
	  push( @auto_code, "same_columns  = [ 0, 0, 0, " );
	}
	foreach my $param ( 'omegas','sigmas' ) {
	  my $parameters = $self -> {'models'} -> [0] -> problems -> [0] -> $param;
	  foreach my $parameter ( @{$parameters} ){
	    if( $parameter -> same() ){
	      push( @same_columns, (1) x $parameter -> size() );
	    } else {
	      push( @same_columns, (0) x scalar @{$parameter -> options} );
	    }
	  }
	}
        push( @auto_code , join( ', ' , @same_columns ).', '.
	      join( ', ' , @same_columns).', '.
#	      join( ', ' , @np_columns).', '.
	      join( ', ' , @sh_columns)."];\n\n" );

        push( @auto_code , "adjust_axes   = [ 1, 1, 1, ".join( ', ' , @adjust_axes)."];\n\n" );

#        push( @auto_code , 'npomegas = '.($nps*($nps+1)/2).";\n\n" );
        push( @auto_code , "npomegas = 0;\n\n" );
	

        push( @auto_code, "minimization_successful_col    = 5;	% Column number for the\n" );
        push( @auto_code, "                                                     % minimization sucessful flag\n" );
        push( @auto_code, "covariance_step_successful_col = 6;	% As above for cov-step warnings\n" );
        push( @auto_code, "covariance_step_warnings_col   = 7;	% etc\n" );
        push( @auto_code, "estimate_near_boundary_col     = 8;	% etc\n" );

        push( @auto_code, "not_data_cols = 13;			   % Number of columns in the\n" );
        push( @auto_code, "                                        % beginning that are not\n" );
        push( @auto_code, "                                        % parameter estimates.\n" );

        push( @auto_code, "filename = 'raw_results_matlab.csv';\n" );
	
	splice( @file, $code_area_start, ($code_area_end - $code_area_start), @auto_code );	
	open( OUTFILE, ">", $self -> {'directory'} . "/histograms.m" );
	print OUTFILE "addpath " . $PsN::lib_dir . ";\n";
	print OUTFILE @file ;
	close OUTFILE;

	open( OUTFILE, ">", $self -> {'directory'} . "/raw_results_matlab.csv" );
	for( my $i = 0; $i < scalar ( @{$self -> {'raw_results'} -> [0]} ); $i ++ ){
# 	  $self -> {'raw_results'} -> [0] -> [$i][0] =
# 	      $self -> {'raw_results'} -> [0] -> [$i][0] eq 'bootstrap' ?
# 	      1 : $self -> {'raw_results'} -> [0] -> [$i][0];
# 	  $self -> {'raw_results'} -> [0] -> [$i][0] =
# 	      $self -> {'raw_results'} -> [0] -> [$i][0] eq 'jackknife' ?
# 	      2 : $self -> {'raw_results'} -> [0] -> [$i][0];
	  map( $_ = $_ eq 'NA' ? 'NaN' : $_, @{$self -> {'raw_results'} -> [0] -> [$i]} );
	  map( $_ = not( defined $_ ) ? 'NaN' : $_, @{$self -> {'raw_results'} -> [0] -> [$i]} );
	  print OUTFILE join( ',', @{$self -> {'raw_results'} -> [0] -> [$i]} ), "\n";
	}
	close OUTFILE;

      } else {
	'debug' -> die( message => 'matlab_dir not configured, no matlab scripts will be generated.');
	return;
      }
    }
end create_matlab_scripts

# }}}

# {{{ create_R_scripts
start create_R_scripts
    {
      unless( -e $PsN::lib_dir . '/R-scripts/bootstrap.R' ){
	'debug' -> die( message => 'Bootstrap R-script are not installed, no R-script will be generated.' );
	return;
      }
      cp ( $PsN::lib_dir . '/R-scripts/bootstrap.R', $self -> {'directory'} );
    }
end create_R_scripts
# }}}
