# TODO, possible improvements.
# * Interface for gof-function needs an overhaul
# * General readability improvments in setup would be nice, but not
# * necessary.
#
# 2004-06-10:
# - The header in the data file is still needed. Make the scm
# calculate the correct columns from the $INPUT record.
# - Make it possible to supply statistics (min, max, median) of
# covariates as arguments to new().

# 2004-08-26
# Verkar som om kovariatnamnen m�ste vara unika. (Se substr i
# _option_val_pos i problem_subs) Annars kan kovariater som anv�nds
# droppas n�r en annan kovariat med kortare namn som b�rjar lika dant
# skall droppas (CANT och CA tex)

# Attributes:
#
# Name:               Type:                   Default Value:
# 'label_type'        Hash of strings
#
# label_type has keys named $parm.$cov, e.g CLWGT or VAGE. Values are
# either 'con' for continuous or 'cat' for categorical.
#
# Name:               Type:                   Default Value:
# 'work_queue'        Array of step objects
#
# Name:               Type:                   Default Value:
# 'spec_code'         Array of step objects
#

# {{{ include

start include statements
use strict;
use Cwd;
use tool::modelfit;
use OSspecific;
use Data::Dumper;
use File::Copy 'cp';
use status_bar;
end include statements

# }}}

# {{{ new

start new
      {
	# <I>test_relations, fix and p_value</I> can be specified
	# as either a reference to a hash or as a reference to an
	# array (for different settings for each model).

	if( defined $this -> {'config_file_name'} or defined $this -> {'config_file'} ){
	  $this -> read_config_file;
	}

	# This block is a duplicate of the settign of the 'directory' attribute in tool.pm
	# It needs to be here too to make sure that the correct directory is set when
	# resuming an scm.
	if ( defined $parm{'directory'} ) {
	  my $dummy;
	  ( $this -> {'directory'}, $dummy ) = OSspecific::absolute_path( $parm{'directory'}, '');
	}

	debug -> die( message => "You need to specify \'models'\ either as argument or in the config file.")
	    unless ( defined $this -> {'models'} and scalar @{$this -> {'models'}} > 0 );

#	debug -> die( message => "tool::scm->new: You need to specify \'parameters\' either".
#		      " as argument or in the config file.")
#	    unless ( defined $this -> {'parameters'} and scalar @{$this -> {'parameters'}} > 0 );

	if ( not( ref($this -> {'p_value'}) eq 'ARRAY' or
		  ref($this -> {'p_value'}) eq 'HASH' ) ) {
	  if ( not defined $this -> {'p_value'} or
	       $this -> {'p_value'} eq '' ) {
	    $this -> {'p_value'} = 0.05;
	  }
	  my $p_value = $this -> {'p_value'};
	  $this -> {'p_value'} = [];
	  foreach my $model ( @{$this -> {'models'}} ) {
	    push ( @{$this -> {'p_value'}}, $p_value ) ;
	  }
	}

	if ( not( ref($this -> {'fix'}) eq 'ARRAY' or
		ref($this -> {'fix'}) eq 'HASH' ) ) {
	  my $fix = $this -> {'fix'};
	  $this -> {'fix'} = [];
	  foreach my $model ( @{$this -> {'models'}} ) {
	    push ( @{$this -> {'fix'}}, $fix ) ;
	  }
	}

	foreach my $file ( 'logfile', 'short_logfile', 'raw_results_file',
			   'final_model_name', 'covariate_statistics_file',
			   'relations_file' ) {
	  if ( not( ref($this -> {$file}) eq 'ARRAY' or
		    ref($this -> {$file}) eq 'HASH' ) ) {
	    my ($ldir, $filename) = OSspecific::absolute_path( $this -> {'directory'},
							       $this -> {$file} );
	    $this -> {$file} = [];
	    for ( my $i = 1; $i <= scalar @{$this -> {'models'}}; $i++ ) {
	      my $tmp = $filename;
	      $tmp =~ s/\./$i\./;
	      push ( @{$this -> {$file}}, $ldir.$tmp ) ;
	    }
	  }
	}

# 	if ( not( ref($this -> {'final_model_name'}) eq 'ARRAY' or
# 		  ref($this -> {'final_model_name'}) eq 'HASH' ) ) {
# 	  my ($ldir, $final_model_name) = OSspecific::absolute_path( $this -> {'directory'},
# 								     $this -> {'final_model_name'} );
# 	  $this -> {'final_model_name'} = [];
# 	  for ( my $i = 1; $i <= scalar @{$this -> {'models'}}; $i++ ) {
# 	    my $tmp = $final_model_name;
# 	    $tmp =~ s/\./$i\./;
# 	    push ( @{$this -> {'final_model_name'}}, $ldir.$tmp ) ;
# 	  }
# 	}

	if ( defined $this -> {'test_relations'} )  {
	  if ( ref ( $this -> {'test_relations'} ) eq 'HASH' ) {
	    my $relations = $this-> {'test_relations'};
	    $this -> {'test_relations'} = [];
	    foreach my $model ( @{$this -> {'models'}} ) {
	      push ( @{$this -> {'test_relations'}}, $relations ) ;
	    }
	  }
	} else {
	  debug -> die( message => "You need to specify \'test_relations'\ either as argument or in the config file." );
	}

	# Check Errors and init
	unless ( defined( $this -> {'categorical_covariates'} ) 
		 or defined( $this -> {'continuous_covariates'} )) {
	  debug -> die( message => "You must specify either " .
			"categorical and/or continuous covariates either as argument or in the config file" );
	}
	unless ( $this -> {'search_direction'} eq 'forward' or
		 $this -> {'search_direction'} eq 'backward' or
		 $this -> {'search_direction'} eq 'both' ) {
	  debug -> die( message => "You must specify the search direction ",
			"either as \"forward\" or \"backward\". Default is \"forward\"." );
	}

	if ( ref(\$this -> {'gof'}) eq 'SCALAR' ) {
	  if ( lc($this -> {'gof'}) eq 'ofv' ) {
	    $this -> {'gof'} = \&gof_ofv;
	  } elsif ( lc($this -> {'gof'}) eq 'crc') {
	    $this -> {'gof'} = \&gof_ofv;
	  } 
	}

	# check the validity of the covariates and the relations to be tested
	for ( my $i = 0; $i < scalar @{$this -> {'models'}}; $i++ ) {
	  my $extra_data_headers = $this -> {'models'}[$i] -> extra_data_headers;

	  if ( defined $this -> {'continuous_covariates'} or defined $this -> {'categorical_covariates'} ) {

	    my @continuous = defined $this -> {'continuous_covariates'} ? @{$this -> {'continuous_covariates'}} : ();
	    my @categorical = defined $this -> {'categorical_covariates'} ? @{$this -> {'categorical_covariates'}} : ();

	    my @not_found = ();
	    foreach my $cov ( @continuous, @categorical ) {
	      my $extra_data_test = 0;
	      if ( defined $extra_data_headers and defined $extra_data_headers -> [0] ) {
		foreach my $head ( @{$extra_data_headers->[0]} ) {
		  $extra_data_test = 1 and last if ( $cov eq $head );
		}
	      }
	      push( @not_found, $cov )
		  unless ( $extra_data_test or 
			   $this -> {'models'}[$i] -> is_option_set( record => 'input',
								     name => $cov ) );
	    }
	    if ( scalar @not_found and
		 ( not defined $this -> {'models'}[$i] -> extra_files or 
		   scalar @{$this -> {'models'}[$i] -> extra_files} == 0 ) ) {
	      debug -> die(  message => "Covariate(s) [ ". join(',', @not_found). " ] specified is not defined in " . 
			     $this ->  {'models'}[$i] -> filename );
	    }
	  }

	  if ( defined $this -> {'test_relations'} ) {
	    foreach my $par ( sort keys %{$this -> {'test_relations'}[$i]} ){
	      my @not_found = ();
	      foreach my $cov ( @{$this -> {'test_relations'}[$i]{$par}} ){
		my @continuous = defined $this -> {'continuous_covariates'} ? @{$this -> {'continuous_covariates'}} : ();
		my @categorical = defined $this -> {'categorical_covariates'} ? @{$this -> {'categorical_covariates'}} : ();

		my $covariate_test = 0;
		foreach my $specified_cov ( @continuous, @categorical ) {
		  $covariate_test = 1 and last if( $cov eq $specified_cov );
		}
		push( @not_found, $cov ) unless ( $covariate_test );
	      }
	      if ( scalar @not_found and
		   ( not defined $this -> {'models'}[$i] -> extra_files or 
		     scalar @{$this -> {'models'}[$i] -> extra_files} == 0 ) ) {
		debug -> die( message => "Covariate(s) [ " . join( ',', @not_found ). " ] specified for parameter $par " . 
			      "in test_relations is not defined as a covariate" );
	      }
	    }
	  }
	}
	
	# If no previous information on the statistics of the
	# covariates is available, initiate this.
	# First; the continuous covariates:
	for ( my $i = 0; $i < scalar @{$this -> {'models'}}; $i++ ) {
	  if( -e $this -> {'covariate_statistics_file'}[$i] ) {
	    open( STAT, '<'.$this -> {'covariate_statistics_file'}[$i] );
	    my $tmp;
	    for ( <STAT> ) {
	      $tmp = $tmp.$_;
	    }
	    close( STAT );
	    my $VAR1;
	    eval( $tmp );
	    $this -> {'covariate_statistics'}[$i] = $VAR1;
	  } else {

	    my $model = $this -> {'models'} -> [$i];
	    # Assume one $PROBLEM

	    my $data = $model -> datas -> [0];
	    my $extra_data;
	    if ( defined $model -> problems -> [0] -> extra_data ) {
	      $extra_data = $model -> problems -> [0] -> extra_data;
	    }
	    unless ( defined $this -> {'covariate_statistics'}[$i] ) {
	      $data -> target('mem');
	      $extra_data -> target('mem') if defined $extra_data;
	      if ( defined $this -> {'continuous_covariates'} ) {
		ui -> print( category => 'scm',
			     message  => "Calculating continuous covariate statistics" );
		my $ncov = scalar @{$this -> {'continuous_covariates'}};
		my $status_bar = status_bar -> new( steps => $ncov );
		ui -> print( category => 'scm',
			     message  => $status_bar -> print_step(),
			     newline  => 0);
		
		for ( my $j = 1; $j <= $ncov; $j++ ) {
		  my $cov = $this -> {'continuous_covariates'} -> [$j-1];
		  # Factors
		  $this -> {'covariate_statistics'}[$i]{$cov}{'factors'} =
		      $model -> factors( problem_number       => 1, 
					 return_occurences    => 1,
					 unique_in_individual => 0,
					 column_head          => $cov );
		  # Statistics
		  $this -> {'covariate_statistics'}[$i]{$cov}{'have_missing_data'} =
		      $model -> have_missing_data( column_head => $cov );
		  
		  ( $this -> {'covariate_statistics'}[$i]{$cov}{'median'},
		    $this -> {'covariate_statistics'}[$i]{$cov}{'min'},
		    $this -> {'covariate_statistics'}[$i]{$cov}{'max'} ) =
			$this -> calculate_continuous_statistics( model => $model,
								  covariate => $cov );
		  #my $nl = $j == $ncov ? "" : "\r"; 
		  if( $status_bar -> tick () ){
		    ui -> print( category => 'scm',
				 message  => $status_bar -> print_step(),
				 wrap     => 0,
				 newline  => 0 );
		  }
		}
		ui -> print( category => 'scm',
			     message  => " ... done\n" );
	      }
	      if ( defined $this -> {'categorical_covariates'} ) {
		ui -> print( category => 'scm',
			     message  => "Calculating categorical covariate statistics" );
		my $ncov = scalar @{$this -> {'categorical_covariates'}};

		my $status_bar = status_bar -> new( steps => $ncov );
		ui -> print( category => 'scm',
			     message  => $status_bar -> print_step(),
			     newline  => 0);
		
		for ( my $j = 1; $j <= $ncov; $j++ ) {
		  my $cov = $this -> {'categorical_covariates'} -> [$j-1];
		  # Factors
		  $this -> {'covariate_statistics'}[$i]{$cov}{'factors'} =
		      $model -> factors( problem_number       => 1,
					 return_occurences    => 1,
					 unique_in_individual => 0,
					 column_head          => $cov );
		  # Fractions
		  $this -> {'covariate_statistics'}[$i]{$cov}{'fractions'} =
		      $model -> fractions( unique_in_individual => 1,
					   column_head          => $cov,
					   ignore_missing       => 1 );
		  # Statistics
		  $this -> {'covariate_statistics'}[$i]{$cov}{'have_missing_data'} =
		      $model -> have_missing_data( column_head => $cov );
		  
		  ( $this -> {'covariate_statistics'}[$i]{$cov}{'median'},
		    $this -> {'covariate_statistics'}[$i]{$cov}{'min'},
		    $this -> {'covariate_statistics'}[$i]{$cov}{'max'} ) =
			$this -> calculate_categorical_statistics
			( factors => $this -> {'covariate_statistics'}[$i]{$cov}{'factors'},
			  have_missing_data => $this -> {'covariate_statistics'}[$i]{$cov}{'have_missing_data'},
			  fix => $this -> {'fix'}[$i] );
		  
		  if( $status_bar -> tick () ){
		    ui -> print( category => 'scm',
				 message  => $status_bar -> print_step(),
				 wrap     => 0,
				 newline  => 0 );
		  }
		}
		ui -> print( category => 'scm',
			     message  => " ... done" );
	      }
	      $data -> target('disk');
	      $extra_data -> target('disk') if defined $extra_data;
	    }
	    open( STAT, '>'.$this -> {'covariate_statistics_file'}[$i] );
	    $Data::Dumper::Purity = 1;
	    print STAT Dumper $this -> {'covariate_statistics'}[$i];
	    $Data::Dumper::Purity = 0;
	    close( STAT );
	  }
	}

	# Default ofv drops at desired p-values (assuming chi-squared
	# distribution of hirerchical models)
	my %p_values;
	#For unlimited stepping: p = 100% /JR
	$p_values{'1'}     = {1=>0,
			      2=>0,
			      3=>0,
			      4=>0,
			      5=>0,
			      6=>0,
			      7=>0,
			      8=>0,
			      9=>0,
			      10=>0};
	## p= 0.05
	$p_values{'0.05'}  = {1=>3.84,
			      2=>5.99,
			      3=>7.81,
			      4=>9.49,
			      5=>11.07,
			      6=>12.59,	
			      7=>14.07,
			      8=>15.51,	
			      9=>16.92,	
			      10=>18.31};
	## p=0.01
	$p_values{'0.01'}  = {1=>6.63,
			      2=>9.21,
			      3=>11.34,
			      4=>13.28,
			      5=>15.09,
			      6=>16.81,
			      7=>18.48,
			      8=>20.09,
			      9=>21.67,
			      10=>23.21};
	## p=0.005
	$p_values{'0.005'} = {1=>7.88,
			      2=>10.60,
			      3=>12.84,
			      4=>14.86,
			      5=>16.75,
			      6=>18.55,
			      7=>20.28,
			      8=>21.95,
			      9=>23.59,
			      10=>25.19};
	## p=0.001
	$p_values{'0.001'} = {1=>10.83,
			      2=>13.82,
			      3=>16.27,
			      4=>18.47,
			      5=>20.52,
			      6=>22.46,
			      7=>24.32,
			      8=>26.12,
			      9=>27.88,
			      10=>29.59};


	# If no previous information on the relations is available,
	# create the basic parameter-covariate relation data structure
	# including the information about states (1=not included,
	# 2=linear relation, 3=hockey-stick relation).
#	unless ( defined $this -> {'relations'} ) {
	  for ( my $i = 0; $i < scalar @{$this -> {'models'}}; $i++ ) {
	    my $first = 1;
	    if( -e $this -> {'relations_file'}[$i] ) {
	      open( RELATIONS, '<'.$this -> {'relations_file'}[$i] );
	      my $tmp;
	      for ( <RELATIONS> ) {
		$tmp = $tmp.$_;
	      }
	      close( RELATIONS );
	      my $VAR1;
	      eval( $tmp );
	      $this -> {'relations'}[$i] = $VAR1;
	      foreach my $par ( sort keys %{$this -> {'test_relations'}[$i]} ) {
		foreach my $cov ( @{$this -> {'test_relations'}[$i]{$par}} ){
		  # Is this covariate continuous or not?
		  my $continuous = 1;
		  foreach my $cat ( @{$this -> {'categorical_covariates'}} ) {
		    $continuous = 0 if ( $cov eq $cat );
		  }
		  my @valid_states;
		  if ( $continuous ) {
		    @valid_states = @{$this -> {'valid_states'}{'continuous'}};
		  } else {
		    @valid_states = @{$this -> {'valid_states'}{'categorical'}};
		  }
		  $this -> {'relations'}[$i]{$par}{$cov}{'state'} = 1;
		  foreach my $state ( @valid_states ) {
		    if ( exists $this -> {'included_relations'}[$i]{$par} and
			 exists $this -> {'included_relations'}[$i]{$par}{$cov} and
			 $this -> {'included_relations'}[$i]{$par}{$cov}{'state'} == $state ) {
		      $this -> {'included_relations'}[$i]{$par}{$cov}{'code'} =
			  $this -> {'relations'}[$i]{$par}{$cov}{'code'}{$state};
		      $this -> {'included_relations'}[$i]{$par}{$cov}{'nthetas'} =
			  $this -> {'relations'}[$i]{$par}{$cov}{'nthetas'}{$state};
		      $this -> {'included_relations'}[$i]{$par}{$cov}{'inits'} =
			  $this -> {'relations'}[$i]{$par}{$cov}{'inits'}{$state};
		      $this -> {'included_relations'}[$i]{$par}{$cov}{'bounds'} =
			  $this -> {'relations'}[$i]{$par}{$cov}{'bounds'}{$state};
		    }
		  }
		}
	      }
	    } else {
	      foreach my $par ( sort keys %{$this -> {'test_relations'}[$i]} ) {
		foreach my $cov ( @{$this -> {'test_relations'}[$i]{$par}} ){
		  # Here the ofv-drops should be defined
		  # I've started 2004-11-09
		  my $ofv_changes = $p_values{$this -> {'p_value'}[$i]};
		  if ( defined $this -> {'ofv_change'} ) {
		    if ( ref($this -> {'ofv_change'}) eq 'HASH' ) {
		      # If only one ofv_drop given for all models and all relations
		      while ( my ( $df, $ofv ) = each %{$this -> {'ofv_change'}} ) {
			$ofv_changes -> {$df} = $ofv;
		      }
		      if ( $first ) {
			open( LOG, ">>".$this -> {'logfile'} -> [$i] );
			print LOG "Using user-defined ofv change criteria\n";
			print LOG "Degree of freedom  |  Required ofv change\n";
			my @dfs = sort {$a <=> $b} keys %{$ofv_changes};
			foreach my $df ( @dfs ) {
			  print LOG "         $df         -          ",
			  $ofv_changes -> {$df},"\n";
			}
			close( LOG );
		      }
		    } elsif ( ref($this -> {'ofv_change'}) eq 'ARRAY' and
			      scalar @{$this -> {'ofv_change'}} > 0 ) {
		      # If ofv_drops given for all relations
		      if ( defined $this -> {'ofv_change'} -> [$i] and 
			   ref( $this -> {'ofv_change'} -> [$i]) eq 'HASH' and
			   defined $this -> {'ofv_change'} -> [$i]{$par} and
			   ref( $this -> {'ofv_change'} -> [$i]{$par}) eq 'ARRAY' and
			   defined $this -> {'ofv_change'} -> [$i]{$par}{$cov} and
			   ref( $this -> {'ofv_change'} -> [$i]{$par}{$cov}) eq 'HASH' ) {
			while ( my ( $df, $ofv ) = each 
				%{$this -> {'ofv_change'} -> [$i]{$par}{$cov}} ) {
			  $ofv_changes -> {$df} = $ofv;
			}
		      }
		    } else {
		      # Attempt to give ofv drops for all relations, but wrong structure
		      debug -> warn( level => 2, 
				     message => "Incorrect structure used for ofv_change, using default" );
		    }
		  }
		  $this -> {'relations'}[$i]{$par}{$cov}{'ofv_changes'} = $ofv_changes;
		  # Is this covariate continuous or not?
		  my $continuous = 1;
		  foreach my $cat ( @{$this -> {'categorical_covariates'}} ) {
		    $continuous = 0 if ( $cov eq $cat );
		  }
		  $this -> {'relations'}[$i]{$par}{$cov}{'continuous'} = $continuous;
		  my @valid_states;
		  if ( $continuous ) {
		    @valid_states = @{$this -> {'valid_states'}{'continuous'}};
		  } else {
		    @valid_states = @{$this -> {'valid_states'}{'categorical'}};
		  }
		  $this -> {'relations'}[$i]{$par}{$cov}{'state'} = 1;
		  foreach my $state ( @valid_states ) {
		    if ( defined $this -> {'relations'}[$i]{$par}{$cov}{'code'}{$state} ) {
		      if ( not ref $this -> {'relations'}[$i]{$par}{$cov}{'code'}{$state} eq 'ARRAY' ) {
			debug -> die( message => "The code specified for $par $cov $state is not ".
				      "an array\n" );
		      } else {
			for ( @{$this -> {'relations'}[$i]{$par}{$cov}{'code'}{$state}} ) {
			  s/PARCOV/$par$cov/g;
			  s/PAR/$par/g;
			  s/COV/$cov/g;
			}
		      }
		    } else {
		      $this -> {'relations'}[$i]{$par}{$cov}{'code'}{$state} = [];
		    }
		    $this -> {'relations'}[$i]{$par}{$cov}{'inits'}{$state} = [] unless
			( defined $this -> {'relations'}[$i]{$par}{$cov}{'inits'}{$state} );
		    $this -> {'relations'}[$i]{$par}{$cov}{'bounds'}{$state} = {} unless
			( defined $this -> {'relations'}[$i]{$par}{$cov}{'bounds'}{$state} );
		    ( $this -> {'relations'}[$i]{$par}{$cov}{'code'}{$state},
		      $this -> {'relations'}[$i]{$par}{$cov}{'nthetas'}{$state},
		      $this -> {'relations'}[$i]{$par}{$cov}{'inits'}{$state},
		      $this -> {'relations'}[$i]{$par}{$cov}{'bounds'}{$state} ) =
			  $this -> create_code( start_theta => 1,
						parameter   => $par,
						covariate   => $cov,
						continuous   => $continuous,
						state       => $state,
						code        => $this -> {'relations'}[$i]{$par}{$cov}{'code'}{$state},
						inits       => $this -> {'relations'}[$i]{$par}{$cov}{'inits'}{$state},
						bounds      => $this -> {'relations'}[$i]{$par}{$cov}{'bounds'}{$state},
						statistics  => $this -> {'covariate_statistics'}[$i]{$cov},
						missing_data_token => $this -> {'missing_data_token'},
						fix         => $this -> {'fix'}[$i] );
		    if ( exists $this -> {'included_relations'}[$i]{$par} and
			 exists $this -> {'included_relations'}[$i]{$par}{$cov} and
			 $this -> {'included_relations'}[$i]{$par}{$cov}{'state'} == $state ) {
		      $this -> {'included_relations'}[$i]{$par}{$cov}{'code'} =
			  $this -> {'relations'}[$i]{$par}{$cov}{'code'}{$state};
		      $this -> {'included_relations'}[$i]{$par}{$cov}{'nthetas'} =
			  $this -> {'relations'}[$i]{$par}{$cov}{'nthetas'}{$state};
		      $this -> {'included_relations'}[$i]{$par}{$cov}{'inits'} =
			  $this -> {'relations'}[$i]{$par}{$cov}{'inits'}{$state};
		      $this -> {'included_relations'}[$i]{$par}{$cov}{'bounds'} =
			  $this -> {'relations'}[$i]{$par}{$cov}{'bounds'}{$state};
		    }
		  }
		  $first = 0;
		}
	      }
	      open( RELATIONS, '>'.$this -> {'relations_file'}[$i] );
	      $Data::Dumper::Purity = 1;
	      print RELATIONS Dumper $this -> {'relations'}[$i];
	      $Data::Dumper::Purity = 0;
	      close( RELATIONS );
	    }
	  }
#	}
      }
end new

# }}}

# {{{ register_in_database
start register_in_database
  {
    if ( $PsN::config -> {'_'} -> {'use_database'} ) {
      tool::register_in_database( $self );
	my $dbh = DBI -> connect("DBI:mysql:host=".$PsN::config -> {'_'} -> {'database_server'}.
				 ";databse=".$PsN::config -> {'_'} -> {'project'},
				 $PsN::config -> {'_'} -> {'user'},
				 $PsN::config -> {'_'} -> {'password'},
				 {'RaiseError' => 1});
	my $sth;
	$sth = $dbh -> prepare("INSERT INTO ".$PsN::config -> {'_'} -> {'project'}.
			       ".scm (tool_id,direction,step_number) ".
			       "VALUES (".$self -> {'tool_id'}.", '".
			       $self -> {'search_direction'}."', '".
			       $self -> {'step_number'}."' )");
	$sth -> execute;
	$self -> {'scm_id'} = $sth->{'mysql_insertid'};
	$sth -> finish;
	$dbh -> disconnect;
      }
  }
end register_in_database
# }}} register_in_database

# {{{ read_config_file

start read_config_file
    {
      unless( defined $self -> {'config_file'} ){
	$self -> {'config_file'} = config_file -> new( file -> new( path => './', name => $self -> {'config_file_name'} ) );
      }

      my $config_file = $self -> {'config_file'};

      if( defined( $config_file -> relations ) ){
	$self -> {'relations'} = [$config_file -> relations];
      }

      foreach my $config_option ( keys %{$config_file -> valid_scalar_options}, 
				  keys %{$config_file -> valid_array_options},
				  keys %{$config_file -> valid_hash_options},
				  keys %{$config_file -> valid_code_options} ){

	# These are options passed to the modelfile in bin/scm.pl
	next if( $config_option eq 'extra_data_files' );
	next if( $config_option eq 'extra_files' );

	# Handle some special cases (where the option is per model)
	if( $config_option eq 'base_criteria_values' and defined $config_file -> base_criteria_values ){
	  @{$self -> {'base_criteria_values'}}[0] = $config_file -> base_criteria_values;
	} elsif( $config_option eq 'test_relations' and defined $config_file -> test_relations ){
	  @{$self -> {'test_relations'}}[0] = $config_file -> test_relations;
	} elsif( $config_option eq 'included_relations' and defined $config_file -> included_relations ){
	  @{$self -> {'included_relations'}}[0] = $config_file -> included_relations;
	} elsif( $config_option eq 'valid_states' and defined $config_file -> valid_states ){
	  if( not defined $config_file -> valid_states -> {'continuous'} ) {
	    debug -> warn( level   => 1,
			   message => "The valid_states section is defined in the configuration file but ".
			   "no states were defined for continuous covariates. Assuming the default valid states: ".
			   join( ', ',@{$self -> { $config_option } -> {'continuous'}}) );
	  } else {
	    $self -> {'valid_states'} -> {'continuous'} = $config_file -> {'valid_states'} -> {'continuous'};
	  }
	  if( not defined $config_file -> valid_states -> {'categorical'} ) {
	    debug -> warn( level   => 1,
			   message => "The valid_states section is defined in the configuration file but ".
			   "no states were defined for categorical covariates. Assuming the default valid states: ".
			   join( ', ',@{$self -> { $config_option } -> {'categorical'}}) );
	  } else {
	    $self -> {'valid_states'} -> {'categorical'} = $config_file -> {'valid_states'} -> {'categorical'};
	  }
	} elsif( defined $config_file -> $config_option  ){

	  # This is the general case where we just copy the option.
	  $self -> { $config_option } = $config_file -> $config_option;
	}
      }
    }
end read_config_file

# }}}

# {{{ _read_scm_file

start _read_scm_file
      {
	# Do we need to set an absolute bath of this file somewhere?
	open(OLDFILE, "<" . $self -> {'scm_file'}) ||
	  die "Unable to open oldstyle file".$self -> {'scm_file'}."for parsing\n";
	my $have_pval = 0;
	while ( <OLDFILE> ) {
	  chomp;
	  if ( /^\s*;;;\s*DECL:\s*(.*)\s*$/ ) { # Find DECL lines, $1 is string of covs
	    foreach (split(/\s+/, $1)) {
	      $self -> {'labels'}{$_} = '';
	    }
	    # ...else Find BOUNDS lines, $1 is bounds, $2 is cov
	  } elsif ( /^\s*;;;\s*BOUNDS:\s*(.*)\s*;\s*(.*)\s*$/ ) {
	    my $label = $2;
	    $1 =~ /\s*\((.*),(.*),(.*)\)/;
	    $self -> {'bounds'}{$label} = [$1,$3];
	    if ( length($2) > 0 ) {
	      $self -> {'inits'}{$label} = $2;
	    }
	  } elsif (/^\s*;;;\s*GLOBAL_INIT:\s*(.*)\s*$/) {
	    if ( defined $self -> {'global_init'} ) {
	      debug -> warn( level => 1, 
			       message => "Global init already specified, ".
			       "either as parameter to scm -> new, or multiple".
			       " times in scm file, using last" );
	    }
	    $self -> {'global_init'} = $1;
	  } elsif ( /^\s*;;;\s*DYNAMIC_INIT\s*$/) {
	    $self -> {'dynamic_init'} = 1;
	    # ... else Find MISS lines, $1 is missing data token	
	  } elsif ( /^\s*;;;\s*MISS:\s*(.*)\s*/) {
	    debug -> warn( level => 2,
			     message => "Found MISS: $1" );
	  } elsif ( /^\s*;;;\s*GOF:\s*(.*)\s*/) {
	    if ( not lc($1) eq 'extern' ) {
	      $self -> {'gof'} = $1;
	    }	    
	    if ( $have_pval and not lc($self -> {'gof'}) eq 'ofv' ) {
	      debug -> warn( level => 1,
			       message => "SCM Warning: PFORW and/or PBACK given in modelfile, but gof-function is not specified to OFV" );
	    }
	  } elsif ( /^\s*;;;\s*TASK:\s*(.*)\s*/) {
	    $self -> {'search_direction'} = $1;
	  } elsif ( /^\s*;;;\s*FIX:\s*(.*)\s*/) {
	    # Must be updated for array format of fix
	    $self -> {'fix'} = $1;
	  } elsif ( /^\s*;;;\s*(\w+)(\d+)\s*START/ ) {
	    my $label = $1;
	    my $level = $2;
	    my @code_array;
	    while (<OLDFILE>) {
	      last if /\s*;;;\s*.*\s*END\s*/;
	      push( @code_array, $_ );
	    }
	    $self -> {'spec_code'}{$label}{$level} = \@code_array;
	  } elsif ( /^\s*;;;\s*PFORW:\s*(\d+)/ ) {
	    if ( defined $self -> {'gof_data'} ) {
	      if ( $have_pval ) {
		$self -> {'gof_data'} -> {'pforw'} = $1;
	      } else {
		debug -> warn( level => 1,
				 message => "SCM Warning: both PFORW and \"gof_data\" ".
				 "is specified, PFORW value will be ignored\n" );
	      }
	    } else {
	      $self -> {'gof_data'} -> {'pforw'} = $1;
	      $have_pval = 1;
	    }
	  } elsif ( /^\s*;;;\s*PBACK:\s*(\d+)/ ) {
	    if ( defined $self -> {'gof_data'} ) {
	      if ( $have_pval ) {
		$self -> {'gof_data'} -> {'pback'} = $1;
	      } else {
		debug -> warn( level => 1,
				 message => "SCM Warning: both PBACK and \"gof_data\" ".
				 "is specified, PBACK value will be ignored\n" );
	      }
	    } else {
	      $self -> {'gof_data'} -> {'pback'} = $1;
	      $have_pval = 1;
	    }
	  } elsif ( /^\s*;;;\s*LST:\s*(.*)\s*$/ ) {
	    $self -> {'listfile'} = $1;
	  }
	}
	if ( defined $self -> {'models'} and
	     scalar @{$self -> {'models'}} > 0 ) {
	  die "Error in scm -> _read_scm_file: You can't ",
	    "specify both a modelobject and a scm modelfile\n";
	} else {
	  my $scm_model = model -> new( 'filename' => $self -> {'scm_file'},
					'outputfile' => $self -> {'listfile'},
					'target' => 'disk',
					'ignore_missing_files' => 1 );

	  my @code_block = @{$scm_model -> pk};	    my @new_code_block;
	  my $prev_code_row = '';
	  my $in_block = 0;
	  foreach my $code_row ( @code_block ) {
	    if ( $code_row =~ /^\s*;;;\s*(\w+)(\d+)\s*START/ ) {
	      $in_block = 1;
	    }
	    if ( $in_block and $code_row =~ /\s*;;;\s*.*\s*END\s*/ ) {
	      $in_block = 0;
	    }
	    unless( ($prev_code_row =~ /^$/ and $code_row =~ /^$/) ){
	      unless( $code_row =~ /^\s*;;;/ or $in_block ){
		push(@new_code_block, $code_row);
		$prev_code_row = $code_row;
	      }
	    }
	  }	    
	  $scm_model -> pk( 'new_pk' => \@new_code_block);
	  push( @{$self -> {'models'}}, $scm_model);
	}
      }
end _read_scm_file

# }}} _read_scm_file

# {{{ _raw_results_callback

start _raw_results_callback
      {
	# The goal is to transfer the default modelfit raw_results format of
	# diagnostic_values-all_thetas-omegas-sigmas
	# to the format:
	# diagnostic_values-orig_thetas-omegas-sigmas-scm_thetas
	# where the scm_thetas are formatted to only hold values where there are active
	# relations in this step. Do a "print Dumper $self -> {'raw_results'}" before and
	# after to see the transformation

	my $orig_mod = $self -> {'models'}[$model_number-1];
	my ( %param_names, %npar_orig );
	my @params = ( 'theta', 'omega', 'sigma' );
	my $cols_orig = 0;
	foreach my $param ( @params ) {
	  my $labels = $orig_mod -> labels( parameter_type => $param );
	  if ( defined $labels ) {
	    $param_names{$param} = $labels -> [0];
	    $npar_orig{$param}   = scalar @{$param_names{$param}};
	    $cols_orig          += $npar_orig{$param};
	  }
	}

	my ( @rel_header, @rel_flag, @step_rel_names, %npars );

	# In this loop we create a mesh of all (allowed and) possible parameter-covariate
	# relations. The active relations of each model [$i] is stored in $rel_flag[$i] as
	# a 1. All inactive relations are indicated by a 0. A header for the raw_results
	# file is stored in @rel_header. %npars is a bit superfluous since this
	# information may be reach through relations-{par}{cov}{'nthetas'}{state} later on. 
	foreach my $parameter ( sort keys %{$self -> {'relations'}[$model_number-1]} ) {
	  foreach my $covariate ( sort keys %{$self -> {'relations'}[$model_number-1]{$parameter}} ) {
	    my ( $in_step, $in_step_state ) = ( 0, undef );
	    my $type = $self -> {'relations'}[$model_number-1]{$parameter}{$covariate}{'continuous'} == 1 ?
	      'continuous' : 'categorical';
	    for ( my $j = 0; $j < scalar @{$self -> {'valid_states'}{$type}}; $j++ ) {
	      my $state = $self -> {'valid_states'}{$type}[$j];
	      my $npar =
		$self -> {'relations'}[$model_number-1]{$parameter}{$covariate}{'nthetas'}{$state};
	      if ( defined $npar ) { # Skip states without parameters
		$npars{$parameter}{$covariate}{$state} = $npar;
		for ( my $k = 1; $k <= $npar; $k++ ) {
		  push( @rel_header, $parameter.$covariate.'-'.$state.'-'.$k );
		  for ( my $i = 0; $i < scalar @{$self -> {'step_relations'}}; $i++ ) {
		    if ( $parameter eq $self -> {'step_relations'} -> [$i]{'parameter'} and
			 $covariate eq $self -> {'step_relations'} -> [$i]{'covariate'} and
		         $state     eq $self -> {'step_relations'} -> [$i]{'state'} or
			 ( defined $self -> {'included_relations'}[$model_number-1]{$parameter} and
			   defined $self -> {'included_relations'}[$model_number-1]{$parameter}{$covariate} and
			   $state eq $self -> {'included_relations'}[$model_number-1]{$parameter}{$covariate}{'state'} and not
			   ( $parameter eq $self -> {'step_relations'} -> [$i]{'parameter'} and
			     $covariate eq $self -> {'step_relations'} -> [$i]{'covariate'} ) ) ) {
		      push( @{$rel_flag[$i]}, 1);
		    } else {
		      push( @{$rel_flag[$i]}, 0);
		    }
		  }
		}
	      }
	    }
	  }
	}

	# Add up all relations and save the maximum number of thetas used. This number is
	# used to count backwards from the the end of each raw_results row produced by the
	# model fit.
	# IMPORTANT: This "counting backwards" from the end of the raw_results of each
	# model fit requires that the raw_results from the modelfit object is allways
	# well-formatted, no matter how the results from each individual model fit are.
	my $nmax = 0;
	for ( my $i = 0; $i <= $#rel_flag; $i++ ) {
	  my $sum = 0;
	  for ( my $j = 0; $j < scalar @{$rel_flag[$i]}; $j++ ) {
	    $sum += $rel_flag[$i][$j];
	  }
	  $nmax = $nmax > $sum ? $nmax : $sum;
	}

	# Use the scm's raw_results file.
	my ($dir,$file) = 
	  OSspecific::absolute_path( $self -> {'directory'},
				     $self -> {'raw_results_file'}[$model_number-1] );
	my $step_number = $self -> {'step_number'};
 	for ( my $i = 0; $i < scalar @{$self -> {'step_relations'}}; $i++ ) {
 	  push( @step_rel_names, $self -> {'step_relations'} -> [$i]{'parameter'}.
		$self -> {'step_relations'} -> [$i]{'covariate'}.'-'.
		$self -> {'step_relations'} -> [$i]{'state'} );
 	}
	my %included_relations = %{$self -> {'included_relations'}[$model_number-1]};
	my %relations          = %{$self -> {'relations'}[$model_number-1]};
	my @step_relations     = @{$self -> {'step_relations'}};
	my %valid_states       = %{$self -> {'valid_states'}};
	my $action = $self -> {'search_direction'} eq 'forward' ? 'added' : 'removed';
 	$subroutine = sub {
	  my $modelfit = shift;
	  my $mh_ref   = shift;
	  my %max_hash = %{$mh_ref};
	  my @new_header = ('step.number', $action.'.relation' );
	  $modelfit -> raw_results_file( $dir.$file );
	  $modelfit -> raw_results_append( 1 ) if ( $step_number > 1 );

	  my $raw_results_header = $modelfit -> raw_results_header;
	  my $raw_results = $modelfit -> raw_results;
	  my $cols = scalar @{$modelfit -> {'raw_results'} -> [0]}; # first non-header row
	  for ( my $i = 0; $i < scalar @{$modelfit -> {'raw_results'}}; $i++ ) {
	    my @new_raw_results = ( $step_number, $step_rel_names[$i] );

	    my ( @diagnostics, @thetas, @omsi, @sethetas, @seomsi );


	    # {{{ Get diagnostic results:

	    for ( my $j = 0; $j < 16; $j++ ) {
	      push( @diagnostics, $modelfit -> {'raw_results'} -> [$i][$j] );
	    }

	    # }}}

	    # {{{ Get the thetas that were present in the original model 

	    for ( my $j = 16;
		  $j < (16+$npar_orig{'theta'}); $j++ ) {
	      push( @thetas, $modelfit -> {'raw_results'} -> [$i][$j] );
	    }

	    # }}}

	    # {{{ Get the results for all par-cov-relation

	    # Initiate $j as starting position for the relation thetas
	    my %res;
	    
	    foreach my $kind ( 'estimate', 'se' ) {
	      my $j = $kind eq 'estimate' ?
		  0 : ($max_hash{'theta'} + $npar_orig{'omega'} + $npar_orig{'sigma'});
	      $j += (16+$npar_orig{'theta'});
	      # Important to loop over the sorted hash
	      # Add all included relations estimates
	      foreach my $incl_par ( sort keys %included_relations ) {
		foreach my $incl_cov ( sort keys %{$included_relations{$incl_par}} ) {
		  next if ( $incl_par eq $step_relations[$i]->{'parameter'} and
			    $incl_cov eq $step_relations[$i]->{'covariate'} );
		  my $npar  = $included_relations{$incl_par}{$incl_cov}{'nthetas'};
		  my $state = $included_relations{$incl_par}{$incl_cov}{'state'};
		  for ( my $l = 1; $l <= $npar; $l++ ) {
		    push( @{$res{$incl_par}{$incl_cov}{$state}{$kind}},
			  $modelfit -> {'raw_results'} -> [$i][$j++] );
		  }
		}
	      }

	      # Add the estimates of the relation unique to the model [$i]
	      for ( my $l = 1; $l <= $npars{$step_relations[$i]->{'parameter'}}
		    {$step_relations[$i]->{'covariate'}}
		    {$step_relations[$i]->{'state'}}; $l++ ) {
		push( @{$res{$step_relations[$i]->{'parameter'}}
			{$step_relations[$i]->{'covariate'}}
			{$step_relations[$i]->{'state'}}{$kind}},
		      $modelfit -> {'raw_results'} -> [$i][$j++] );
	      }

	      # Sort the results accorrding to the order they appear in (a sorted) $self ->
	      # {'relations'}
	      foreach my $par ( sort keys %npars ) {
		foreach my $cov ( sort keys %{$npars{$par}} ) {
		  my $type = $relations{$par}{$cov}{'continuous'} == 1 ?
		      'continuous' : 'categorical';
		  foreach my $state ( @{$valid_states{$type}} ) {
		    my $val = ( defined $res{$par} and defined $res{$par}{$cov} and
				defined $res{$par}{$cov}{$state} and
				defined $res{$par}{$cov}{$state}{$kind} ) ?
				$res{$par}{$cov}{$state}{$kind} :
				[(undef) x $npars{$par}{$cov}{$state}];
		    push( @thetas, @{$val} ) if( defined $val and $kind eq 'estimate' );
		    push( @sethetas, @{$val} ) if( defined $val and $kind eq 'se' );
		  }
		}
	      }
	    }

	    # }}} 

	    # {{{ Get all the omegas and sigmas
	    
	    for ( my $j = (16+$max_hash{'theta'}); $j <
		  ( 16+$max_hash{'theta'} + $npar_orig{'theta'} +
		    $npar_orig{'omega'} + $npar_orig{'sigma'} ); $j++ ) {
	      push( @omsi, $modelfit -> {'raw_results'} -> [$i][$j] );
	    }

	    # }}}

	    # {{{ Get all the standard errors of the omegas and sigmas
	    
	    for ( my $j = (16+($max_hash{'theta'}*2 + $npar_orig{'omega'} + $npar_orig{'sigma'}));
		  $j < (16+($max_hash{'theta'} + $npar_orig{'omega'} + $npar_orig{'sigma'})*2); $j++ ) {
	      push( @seomsi, $modelfit -> {'raw_results'} -> [$i][$j] );
	    }

	    # }}}

	    push( @new_raw_results, ( @diagnostics, @thetas, @omsi,
				      @sethetas, @seomsi ) );

	    $modelfit -> {'raw_results'} -> [$i] = \@new_raw_results;
	  }
	  if ( $step_number == 1 ) {
	    # Loop through the unchanged header and use the header names as accessors
	    # for the original model raw results.
	    my @orig_res = ( 0, undef, 1, 1, 1 );
	    foreach my $param ( @{$raw_results_header} ){
	      next if( $param eq 'model' or $param eq 'problem' or $param eq 'subproblem' );
	      my ( $accessor, $res );
	      if ( $param eq 'theta' or $param eq 'omega' or $param eq 'sigma' or
		   $param eq 'setheta' or $param eq 'seomega' or $param eq 'sesigma' or
		   $param eq 'npomega' or $param eq 'eigen' ) {
		$accessor = $param.'s';
		$res = $orig_mod -> {'outputs'} -> [0] -> $accessor;

	      } elsif ( $param eq 'shrinkage_etas' ) {
		
		# Shrinkage does not work for subproblems right now.
		$res = $orig_mod -> eta_shrinkage;

	      } elsif ( $param eq 'shrinkage_wres' ) {

		$res = $orig_mod -> wres_shrinkage;

	      } else {

		$res = $orig_mod -> {'outputs'} -> [0] -> $param;

	      }
	      # To handle settings on problem level.

	      if( defined $res and ref $res -> [0] eq 'ARRAY' ){
		$res = $res -> [0][0];
	      } else {
		$res = $res -> [0];
	      }

	      if( $max_hash{$param} > 1 or ref $res eq 'ARRAY' ) {
		if( defined $res ) {
		  push( @orig_res, @{$res} );
		  push( @orig_res, (undef) x ($max_hash{$param} - scalar @{$res}) )
		      unless ( $param eq 'theta' or $param eq 'setheta' );
		} else {
		  push( @orig_res, (undef) x $max_hash{$param} )
		      unless ( $param eq 'theta' or $param eq 'setheta' );	
		}
	      } else {
		push( @orig_res, $res );
	      }

	      if( $param eq 'theta' or $param eq 'setheta' ) {
		# Push undef's for all possible relations
		foreach my $par ( sort keys %npars ) {
		  foreach my $cov ( sort keys %{$npars{$par}} ) {
		    my $type = $relations{$par}{$cov}{'continuous'} == 1 ?
			'continuous' : 'categorical';
		    foreach my $state ( @{$valid_states{$type}} ) {
		      my $val = [(undef) x $npars{$par}{$cov}{$state}];
		      push( @orig_res, @{$val} );
		    }
		  }
		}
	      }

	    }
	    unshift( @{$raw_results}, \@orig_res );

	    my @new_header = ('step.number', $action.'.relation' );
	    foreach my $name ( @{$raw_results_header} ) {
	      my @new_names = ();
	      foreach my $param ( @params ) {
		if ( $name eq $param ) {
		  @new_names = @{$param_names{$param}};
		  if( $param eq 'theta' ) {
		    push( @new_names, @rel_header );
		  }
		  last;
		}
		if ( $name eq 'se'.$param ) {
		  if( $param eq 'theta' ) {
		    foreach my $head_str ( @{$param_names{$param}}, @rel_header ) {
		      push( @new_names, 'se'.$head_str );
		    }
		  }
		  last;
		}
	      }
	      if ( $#new_names >= 0 ) {
		push( @new_header, @new_names );
	      } else {
		push( @new_header, $name );
	      }
	    }
	    $modelfit -> {'raw_results_header'} = \@new_header;
	  }

#	  $modelfit -> {'raw_results_header'} = [@new_header, @rel_header];
 	};
	return $subroutine;
      }
end _raw_results_callback

# }}} _raw_results_callback

# {{{ modelfit_setup

start modelfit_setup
      {
	my $model = $self -> {'models'} -> [$model_number-1];
	# If the number of threads are given per tool, e.g. [2,5] meaning 2 threads for
	# scm and 5 for the modelfit.
	my $mfit_threads = ref( $self -> {'threads'} ) eq 'ARRAY' ?
	  $self -> {'threads'} -> [1]:$self -> {'threads'};
	my $own_threads = ref( $self -> {'threads'} ) eq 'ARRAY' ?
	  $self -> {'threads'} -> [0]:$self -> {'threads'};
	# More threads than models?
	my $num = scalar @{$self -> {'models'}};
	$own_threads = $num if ( $own_threads > $num );

	# Check which models that hasn't been run and run them
	# This will be performed each step but will only result in running
	# models at the first step, if at all.

	# If more than one process is used, there is a VERY high risk of interaction
	# between the processes when creating directories for model fits. Therefore
	# the {'directory'} attribute is given explicitly below.

	unless ( $model -> is_run ) {
	  my $orig_fit = tool::modelfit -> new
	    ( reference_object => $self,
	      prepared_models  => undef,
	      subtools         => undef,
	      results          => undef,
	      logfile          => undef,
	      raw_results => undef,
	      base_directory => $self -> {'directory'},
	      directory      => $self -> {'directory'}.'/orig_modelfit_dir'.$model_number.'/',
	      models         => [$model],
	      subtools       => undef,
	      top_tool       => 0,
	      parent_tool_id   => $self -> {'tool_id'},
	      threads        => $mfit_threads,
	      parent_threads => $own_threads );
	  ui -> print( category => 'scm',
		       message  => "Evaluating basic model" ) unless ( $self -> {'parent_threads'} > 1 );
	  $orig_fit -> run;
	  if( $self -> {'scm_file'} and not -e $self -> {'scm_file'} ){
 	    $self -> _write( model_number => $model_number,
			     filename => $self -> {'scm_file'} );
	  }
	}
	( $self -> {'prepared_models'}[$model_number-1]{'own'}, $self -> {'step_relations'} ) =
	    $self -> _create_models( model_number => $model_number,
				     orig_model   => $self -> {'models'} -> [$model_number-1],
				     base_model   => $self -> {'base_models'} -> [$model_number-1],
				     relations    => $self -> {'relations'}[$model_number-1],
				     included_relations =>
				     $self -> {'included_relations'}[$model_number-1]);
	# Create a modelfit tool for all the models of this step.
	# This is the last setup part before running the step.
	my %subargs = ();
	if ( defined $self -> {'subtool_arguments'} ) {
	  %subargs = %{$self -> {'subtool_arguments'}};
	}
	
	push( @{$self -> {'tools'}},
	      tool::modelfit -> new
	      ( reference_object => $self,
		prepared_models => undef,
		subtools => undef,
		results => undef,
		raw_results => undef,
		_raw_results_callback => $self ->
		_raw_results_callback( model_number => $model_number ),
		models         => $self -> {'prepared_models'}[$model_number-1]{'own'},
		threads        => $mfit_threads,
		logfile        => $self -> {'directory'}."/modelfit".$model_number.".log",
		base_directory => $self -> {'directory'},
 		directory      => $self -> {'directory'}.'/modelfit_dir'.$model_number,
		parent_threads => $own_threads,
		parent_tool_id   => $self -> {'tool_id'},
		top_tool       => 0,
		%subargs ) );
      }
end modelfit_setup

# }}}

# {{{ modelfit_analyze

start modelfit_analyze
    {
      # Own_threads is used to set parent_threads for child tools
      my $own_threads = ref( $self -> {'threads'} ) eq 'ARRAY' ?
	  $self -> {'threads'} -> [0]:$self -> {'threads'};
      # More threads than models?
      my $num = scalar @{$self -> {'models'}};
      $own_threads = $num if ( $own_threads > $num );
      
      my @results = @{$self -> {'results'}};
      
      my @minimizations;
      for ( my $i = 0; $i < scalar @{$self -> {'prepared_models'}[$model_number-1]{'own'}}; $i++ ) {
	my $term = $self -> {'prepared_models'}[$model_number-1]{'own'}[$i] ->
	    outputs -> [0] -> minimization_successful;
	push( @minimizations, $term->[0][0] );
      }
      # This loop checks the termination of all model fits
      foreach my $mod_return_type ( @{$results[$model_number-1]{'subtools'}[0]} ){
	my $crash = 0;
	my $crash_name;
	foreach my $type ( @{$mod_return_type -> {'own'}} ) {
	  if ( $type -> {'name'} eq 'minimization_successful' ){
	    for ( my $i = 0; $i < scalar @{$type -> {'values'}}; $i++ ) {
	      for ( my $j = 0; $j < scalar @{$type -> {'values'}[$i]}; $j++ ) {
		if ( $self -> {'picky'} ) {
# Did minimization just loose one dimension?
#		  if ( not defined $type -> {'values'}[$i][$j][0] or 
#		       $type -> {'values'}[$i][$j][0] != 1 ) {
		  if ( not defined $type -> {'values'}[$i][$j] or 
		       $type -> {'values'}[$i][$j] != 1 ) {
		    $crash = 2;
		  }
		} else {
#		  if ( not defined $type -> {'values'}[$i][$j][0] or 
#		       $type -> {'values'}[$i][$j][0] <= 0 ) {
		  if ( not defined $type -> {'values'}[$i][$j] or 
		       $type -> {'values'}[$i][$j] <= 0 ) {
		    $crash = 1;
		  }
		}
	      }
	    }
	  }
	  $crash_name = $type -> {'values'}[0] if ( $type -> {'name'} eq 'filename' );
	}
	if ( $crash == 2 and $self -> {'abort_on_fail'} ) {
	  die "The execution of the modelfile ",
	  $crash_name," failed (picky==1), aborting SCM, sorry\n";
	} elsif ( $crash == 1 and $self -> {'abort_on_fail'} ) {
	  die "The execution of the modelfile ",
	  $crash_name," failed (picky==0), aborting SCM, sorry\n";
	}
      }
      
      my %return_section;
      $return_section{'name'} = 'ofv.drop';
      $return_section{'values'} = [];
      $return_section{'labels'} = [];
      
      # Collect the drops. The $i is needed since the prepared
      # models are not indexed by parameters and covariate
      # combination
      my $base_ofv;
      if ( defined $self -> {'base_criteria_values'} and 
	   defined $self -> {'base_criteria_values'} ->
	   [$model_number-1] -> {'ofv'}) {
	$base_ofv = $self -> {'base_criteria_values'} ->
	    [$model_number-1] -> {'ofv'}
      } else {
	$base_ofv = $self -> {'models'}[$model_number-1] ->
	    outputs -> [0] -> ofv -> [0][0];
      }
      my $i = 0;

      foreach my $parameter ( sort keys %{$self -> {'relations'}[$model_number-1]} ) {
	foreach my $covariate ( sort keys %{$self -> {'relations'}[$model_number-1]{$parameter}} ) {
	  # Is this covariate continuous or not?
	  my $continuous = 1;
	  foreach my $cat ( @{$self -> {'categorical_covariates'}} ) {
	    $continuous = 0 if ( $covariate eq $cat );
	  }
	  my @valid_states;
	  if ( $continuous ) {
	    @valid_states = @{$self -> {'valid_states'}{'continuous'}};
	  } else {
	    @valid_states = @{$self -> {'valid_states'}{'categorical'}};
	  }
	  
	  my $state;
	  # Important: just setting $state to $self->incl_rel....{'state'} initiates
	  # included_relations for this parameter and covariate. Avoid this.
	  if ( defined $self -> {'included_relations'}[$model_number-1]{$parameter}{$covariate} ) {
	    $state = $self -> {'included_relations'}[$model_number-1]{$parameter}{$covariate}{'state'};
	  }
	  $state = defined $state ? $state : $valid_states[0];
	  
	  next if ( ( $self -> {'search_direction'} eq 'forward' and
		      $state == $valid_states[$#valid_states] ) or
#			$state == $self -> {'valid_states'} ->
#			[scalar @{$self -> {'valid_states'}}-1] ) or
		    ( $self -> {'search_direction'} eq 'backward' and
		      $state == $valid_states[0] ) );
#			$state == $self -> {'valid_states'} -> [0] ) );
	  my $new_ofv = $self -> {'prepared_models'}[$model_number-1]{'own'}[$i] -> outputs -> [0] -> ofv -> [0][0];
	  $new_ofv = $base_ofv unless ( defined $new_ofv );
	  # Only one problem and one sub problem
	  push( @{$return_section{'values'}[0][0]}, $base_ofv - $new_ofv );
	  $i++;
	}
      }
      push( @{$self -> {'results'}[$model_number-1]{'own'}},
	    \%return_section );
      my @ofv_changes;
      foreach my $par ( sort keys %{$self -> {'test_relations'}[$model_number-1]} ) {
	foreach my $cov ( @{$self -> {'test_relations'}[$model_number-1]{$par}} ){
	  push( @ofv_changes,
		$self -> {'relations'}[$model_number-1]{$par}{$cov}{'ofv_changes'} );
	}
      }
      my ( $chosen_parameter,
	   $chosen_covariate,
	   $sign_models_ref,
	   $test_vals_ref,
	   $criterion,
	   $test_log_ref,
	   $new_base_crit_val_ref,
	   $short_log_text,
	   $short_log_ref ) = ( undef, undef, undef, undef,
				undef, undef, undef, undef, undef );
      ( $self -> {'resulting_model'},
	$chosen_parameter,
	$chosen_covariate,
	$sign_models_ref,
	$test_vals_ref,
	$criterion,
	$test_log_ref,
	$new_base_crit_val_ref,
	$short_log_text,
	$short_log_ref )
	  = $self -> {'gof'} -> ( $self,
				  $self -> {'search_direction'},
				  $self -> {'models'} -> [$model_number-1],
				  $model_number,
				  \@ofv_changes);
	
      # Print a short summary of the step (All logging should be kept in a log-method in the future)
      open( LOG, ">>".$self -> {'short_logfile'} -> [$model_number-1] );
      print LOG $short_log_text;
      close( LOG );

      my %return_section;
      $return_section{'name'} = 'relation.chosen.in.step';
      $return_section{'values'} = [];
      $return_section{'labels'} = [];
      if ( defined $chosen_parameter and defined  $chosen_covariate ) {
	$return_section{'values'}[0][0] = $chosen_parameter.$chosen_covariate;
	my $task = $self -> {'search_direction'} eq 'forward' ? 'Adding' : 'Removing';
	ui -> print( category => 'scm',
		     message  => "$task $chosen_covariate on $chosen_parameter" )
	    unless $self -> {'parent_threads'} > 1;
      }
      push( @{$self -> {'results'}[$model_number-1]{'own'}},
	    \%return_section );

# 	my $i = 0;
# 	if ( defined $chosen_parameter and defined  $chosen_covariate ) {
# 	  while ( my( $parameter, $cov_ref ) = each %{$self -> {'relations'}[$model_number-1]} ) {
# 	    while ( my( $covariate, $state_ref ) = each %{$self -> {'relations'}[$model_number-1]{$parameter}} ) {
# 	      if ( $parameter eq $chosen_parameter and
# 		   $covariate eq $chosen_covariate ) {
# 		$self -> {'resulting_model'} = $self -> {'prepared_models'}[$model_number-1]{'own'}[$i];
# 	      }
# 	      $i++;
# 	    }
# 	  }
# 	}



      my $final_model;
      if ( defined $self -> {'resulting_model'} ) {
	# Promote and log the included relation:
	# Is this covariate continuous or not?
	my $continuous = 1;
	foreach my $cat ( @{$self -> {'categorical_covariates'}} ) {
	  $continuous = 0 if ( $chosen_covariate eq $cat );
	}
	my @valid_states;
	if ( $continuous ) {
	  @valid_states = @{$self -> {'valid_states'}{'continuous'}};
	} else {
	  @valid_states = @{$self -> {'valid_states'}{'categorical'}};
	}

	my $state;
	# Important: just setting $state to $self->incl_rel....{'state'} initiates
	# included_relations for this parameter and covariate. Avoid this.
	if ( defined $self -> {'included_relations'}[$model_number-1]{$chosen_parameter}
	     {$chosen_covariate} ) {
	  $state = $self -> {'included_relations'}[$model_number-1]{$chosen_parameter}
	  {$chosen_covariate}{'state'};
	}
	$state = defined $state ? $state : $valid_states[0];
#	  my $state = $self -> {'included_relations'}[$model_number-1]{$chosen_parameter}
#	  {$chosen_covariate}{'state'};
	if ( $self -> {'search_direction'} eq 'forward' ) {
	  my $flag = 0;
	  for( my $s_idx = 0; $s_idx <= $#valid_states; $s_idx++ ) {
	    if ( $flag ) {
	      $state = $valid_states[$s_idx];
	      last;
	    }
	    $flag = 1 if( $state == $valid_states[$s_idx] );
	  }
	} elsif ( $self -> {'search_direction'} eq 'backward' ) {
	  my $flag = 0;
	  for( my $s_idx = $#valid_states; $s_idx >= 0; $s_idx-- ) {
	    if ( $flag ) {
	      $state = $valid_states[$s_idx];
	      last;
	    }
	    $flag = 1 if( $state == $valid_states[$s_idx] );
	  }
	}
	# If the state is 1 (not included); remove the relation.
	if ( $state == 1 ) {
	  # Check if this relation is the last for the parameter
	  if ( scalar keys %{$self -> {'included_relations'}[$model_number-1]
			     {$chosen_parameter}} == 1 ) {
	    delete $self -> {'included_relations'}[$model_number-1]{$chosen_parameter};
	  } else {
	    delete $self -> {'included_relations'}[$model_number-1]{$chosen_parameter}
	    {$chosen_covariate};
	  }
	} else {
	  $self -> {'included_relations'}[$model_number-1]{$chosen_parameter}{$chosen_covariate}{'state'} = $state;
	  $self -> {'included_relations'}[$model_number-1]{$chosen_parameter}{$chosen_covariate}{'theta_estimates'} =
	      $self -> {'resulting_model'} -> outputs -> [0] -> thetas -> [0][0];
	  $self -> {'included_relations'}[$model_number-1]{$chosen_parameter}{$chosen_covariate}{'code'} =
	      $self -> {'relations'}[$model_number-1]{$chosen_parameter}{$chosen_covariate}{'code'}{$state};
	  $self -> {'included_relations'}[$model_number-1]{$chosen_parameter}{$chosen_covariate}{'nthetas'} =
	      $self -> {'relations'}[$model_number-1]{$chosen_parameter}{$chosen_covariate}{'nthetas'}{$state};
	  $self -> {'included_relations'}[$model_number-1]{$chosen_parameter}{$chosen_covariate}{'inits'} =
	      $self -> {'relations'}[$model_number-1]{$chosen_parameter}{$chosen_covariate}{'inits'}{$state};
	  $self -> {'included_relations'}[$model_number-1]{$chosen_parameter}{$chosen_covariate}{'bounds'} =
	      $self -> {'relations'}[$model_number-1]{$chosen_parameter}{$chosen_covariate}{'bounds'}{$state};
	}

	$self -> write_log
	    ( direction          => $self -> {'search_direction'},
	      logfile            => $self -> {'logfile'}[$model_number-1],
	      included_relations => $self -> {'included_relations'}[$model_number-1],
	      chosen_parameter   => $chosen_parameter,
	      chosen_covariate   => $chosen_covariate,
	      results            => $self -> {'results'}[$model_number-1]{'own'},
	      criterion           => $criterion,
	      test_log           => $test_log_ref,
	      append             => $self -> {'append_log'} );

	# Check if there still are some states to test
	# To do, maybe fixed 2005-01-12: this is only valid for the forward search, fix the backward 
	my $still_one_left = 0;
	foreach my $par ( sort keys %{$self -> {'test_relations'}
						    [$model_number-1]} ) {
	  foreach my $cov ( @{$self -> {'test_relations'}[$model_number-1]{$par}} ){
	    # bad coding, I know. /Lasse
	    my $kind = 'continuous';
	    foreach my $cat ( @{$self -> {'categorical_covariates'}} ) {
	      $kind = 'categorical' if ( $cov eq $cat );
	    }
	    if ( defined $self -> {'included_relations'}[$model_number-1]{$par}{$cov} ) {
	      $still_one_left = 1 unless
		  ( $self -> {'included_relations'}[$model_number-1]{$par}{$cov}{'state'} ==
		    $self -> {'valid_states'}{$kind} ->
		    [ scalar @{$self -> {'valid_states'}{$kind}} - 1] );
	    } else {
	      $still_one_left = 1;
	    }
	  }
	}

	my ( $returns, $prep_models );
	if ( $still_one_left ) {
	  my $cpu_time = defined $self -> {'cpu_time'} ? int($self -> {'cpu_time'}*1.2) : undef;
	  my $internal_scm =
	      tool::scm ->
	      new( reference_object     => $self,
		   cpu_time             => $cpu_time,
		   covariate_statistics => [$self -> {'covariate_statistics'}[$model_number-1]],
		   directory            => $self -> {'directory'}.'/scm_dir'.$model_number,
		   models               => [$self -> {'models'}[$model_number-1]],
		   relations            => [$self -> {'relations'}[$model_number-1]],
		   base_models          => [$self -> {'resulting_model'}],
		   included_relations   => [$self -> {'included_relations'}[$model_number-1]],
		   fix                  => [$self -> {'fix'}[$model_number-1]],
		   append_log           => 1,
		   step_number          => ($self -> {'step_number'} + 1),
		   raw_results_file     => [$self -> {'raw_results_file'}[$model_number-1]],
		   logfile              => [$self -> {'logfile'}[$model_number-1]],
		   base_criteria_values => [$new_base_crit_val_ref],
		   parent_tool_id       => $self -> {'tool_id'},
		   parent_threads       => $own_threads , 
		   top_tool             => 0,
		   config_file          => undef,
		   resulting_model      => undef,
		   tools                => undef,
		   prepared_models      => undef,
		   results              => undef );

	  ui -> print( category => 'scm',
		       message  => "Taking a step " . $self -> {'search_direction'} )
	      unless $self -> {'parent_threads'} > 1;
	  $internal_scm -> run;
	  $returns = $internal_scm -> {'results'};
	  $prep_models = $internal_scm -> {'prepared_models'};
	  ui -> print( category => 'scm',
		       message  => $self -> {'search_direction'} . " step done." )
	      unless $self -> {'parent_threads'} > 1;
	  
	  foreach my $return ( @{$returns ->[0]{'own'}} ) {
	    if ( $return -> {'name'} eq 'base.criteria.values' ){
	      $self -> base_criteria_values( $return -> {'values'} );
	    }
	  }
	} else {
	  my @tmp_ret;
	  $tmp_ret[0]{'own'}[0]{'name'}   = 'final.model';
	  $tmp_ret[0]{'own'}[0]{'values'}[0][0] = 'basic.model';
	  $returns = \@tmp_ret;

	  $self -> base_criteria_values( [$new_base_crit_val_ref] );
	}

	if ( defined $prep_models ) {
	  debug -> warn( level => 2,
			 message => " have called internal scm " .
			 scalar @{$prep_models} );

	  # Enclose $prep_models in array ref to reflect the
	  # per-tool level, even though a modelfit does not
	  # prepare any models itself
	  #push ( @{$self -> {'prepared_models'}[$model_number-1]{'subtools'}}, $prep_models -> [0]);

	  # Push the results of the internal scm on the results attribute:
	  #push( @{$self -> {'results'}[$model_number-1]{'subtools'}}, $returns );
	} else {
	  debug -> warn( level => 2,
			 message =>	" no prep_models defined from internal scm " );
	}
	
	# set final model to this steps' best model if the internal scm returned 'basic_model'.
	foreach my $return ( @{$returns ->[0]{'own'}} ) {
	  $final_model = $return -> {'values'}[0][0] if ( $return -> {'name'} eq 'final.model' );
	}

	if ( not defined $final_model or $final_model eq 'basic_model' ) {
	  $final_model         = $self -> {'resulting_model'};
	  my ( $fdir, $fname ) = OSspecific::absolute_path( '.',$self -> {'final_model_name'}[$model_number-1] );
	  $final_model -> filename( $fname );
	  $final_model -> directory( $fdir );
	  $fname =~ s/\.mod/\.lst/;
	  cp( $self -> {'resulting_model'} -> outputfile, "$fdir$fname" );
	  $final_model -> outputfile( "$fdir$fname" );
	  $final_model -> _write;
	  $self -> write_log
	      ( direction          => $self -> {'search_direction'},
		logfile            => $self -> {'short_logfile'}[$model_number-1],
		included_relations => $self -> {'included_relations'}[$model_number-1],
		chosen_parameter   => $chosen_parameter,
		chosen_covariate   => $chosen_covariate,
		results            => $self -> {'results'}[$model_number-1]{'own'},
		criterion          => $criterion,
		test_log           => $test_log_ref,
		append             => 1 );
	}
      } else {
	# This is the last step.
	$self -> write_log ( direction         => $self -> {'search_direction'},
			     logfile            => $self -> {'logfile'}[$model_number-1],
			     results            => $self -> {'results'}[$model_number-1]{'own'},
			     append             => $self -> {'append_log'} );

	# Leave base_criteria_values as they are
      }

      my %return_section;
       $return_section{'name'} = 'base.criteria.values';
       $return_section{'values'} = $self -> base_criteria_values;
       $return_section{'labels'} = undef;
       push( @{$self -> {'results'}[$model_number-1]{'own'}},\%return_section );

      my %return_section;
      $return_section{'name'} = 'included.relations';
      $return_section{'values'} = $self -> {'included_relations'}[$model_number-1];
      $return_section{'labels'} = undef;
      push( @{$self -> {'results'}[$model_number-1]{'own'}},\%return_section );

      
      # This loop tries to minimize the data written to disc.
      for ( my $i = 0; $i < scalar @{$self -> {'prepared_models'}[$model_number-1]{'own'}}; $i++ ) {
	$self -> {'prepared_models'}[$model_number-1]{'own'}[$i] -> {'datas'} = undef;
	$self -> {'prepared_models'}[$model_number-1]{'own'}[$i] -> {'outputs'} = undef;
      }

      my %return_section;
      $return_section{'name'} = 'final.model';
      $return_section{'values'}[0][0] = $final_model;
      $return_section{'labels'} = undef;
      push( @{$self -> {'results'}[$model_number-1]{'own'}},\%return_section );

      debug -> warn( level => 2, 
		     message => "Finished in modelfit_analyze!" );
      }
end modelfit_analyze

# }}} modelfit_analyze

# {{{ modelfit_post_fork_analyze

start modelfit_post_fork_analyze
      {
	# Check the thread number of this tool level:
	my $threads = ref( $self -> {'threads'} ) eq 'ARRAY' ? 
	  $self -> {'threads'} -> [0]:$self -> {'threads'};
	# More threads than models?
	my @models = @{$self -> {'models'}};
	$threads = $#models + 1 if ( $threads > $#models + 1);

	# It is not necessary to collect the included relations from
	# the runs if no parallelism has been used or if only one run
	# has been performed. Now there is allways parallelism! see
	# tool::run and Readme.txt

	my @included_relations = ();
	foreach my $return ( @{$self -> {'results'}->[0]{'own'}} ) {
	  if ( $return -> {'name'} eq 'included.relations' ){
	    $self -> {'included_relations'} = [$return -> {'values'}];
	  }
	  if ( $return -> {'name'} eq 'base.criteria.values' ){
	    $self -> base_criteria_values( $return -> {'values'} );
	  }
	}
      }
end modelfit_post_fork_analyze

# }}}

# {{{ gof_crc

start gof_crc
      {
	my ($direction, $basic_model, $model_number) = @_;
	my ( $chosen_parameter, $chosen_covariate );
	my %p_values;

	#For unlimited stepping: p = 100% /JR
	$p_values{'1'}     = {1=>0,
			      2=>0,
			      3=>0,
			      4=>0,
			      5=>0,
			      6=>0,
			      7=>0,
			      8=>0,
			      9=>0,
			      10=>0};
	## Set the critical values to use
	## p= 0.05
	$p_values{'0.05'}  = {1=>3.84,
			      2=>5.99,
			      3=>7.81,
			      4=>9.49,
			      5=>11.07,
			      6=>12.59,	
			      7=>14.07,
			      8=>15.51,	
			      9=>16.92,	
			      10=>18.31};
	## p=0.01
	$p_values{'0.01'}  = {1=>6.63,
			      2=>9.21,
			      3=>11.34,
			      4=>13.28,
			      5=>15.09,
			      6=>16.81,
			      7=>18.48,
			      8=>20.09,
			      9=>21.67,
			      10=>23.21};
	## p=0.005
	$p_values{'0.005'} = {1=>7.88,
			      2=>10.60,
			      3=>12.84,
			      4=>14.86,
			      5=>16.75,
			      6=>18.55,
			      7=>20.28,
			      8=>21.95,
			      9=>23.59,
			      10=>25.19};
	## p=0.001
	$p_values{'0.001'} = {1=>10.83,
			      2=>13.82,
			      3=>16.27,
			      4=>18.47,
			      5=>20.52,
			      6=>22.46,
			      7=>24.32,
			      8=>26.12,
			      9=>27.88,
			      10=>29.59};

	my $resulting_model;
	my @ofv_drops;
	foreach my $return ( @{$self -> {'results'}[$model_number-1]{'own'}} ) {
	  @ofv_drops = @{$return -> {'values'}[0][0]} if ( $return -> {'name'} eq 'ofv.drop' );
	}
	my $base_n_param = $self ->
	  {'models'}[$model_number-1] -> nthetas( problem_number => 1 );
	print "Orig model NPARAM: $base_n_param\n";
	if ( defined $self -> {'included_relations'}[$model_number-1] ) {
	  my %included_relations = %{$self -> {'included_relations'}[$model_number-1]};
	  foreach my $incl_par ( sort keys %included_relations ) {
	    foreach my $incl_cov ( sort keys %{$included_relations{$incl_par}} ) {
	      $base_n_param += $included_relations{$incl_par}{$incl_cov}{'nthetas'};
	      print "Adding $included_relations{$incl_par}{$incl_cov}{'nthetas'}\n";
	    }
	  }
	}
	print "STEP model NPARAM: $base_n_param\n";
	my %sign_drops;
	my @step_relations = @{$self -> {'step_relations'}};
	for ( my $i = 0; $i <= $#step_relations; $i++ ) {
	  my $n_param_diff = 
	    $self -> {'prepared_models'}[$model_number-1]{'own'}[$i] ->
	      nthetas( problem_number => 1 ) - $base_n_param;
	  print "Diff in parameters for model $i: $n_param_diff\n";
	  if( $self -> {'search_direction'} eq 'forward'){
	    if( $ofv_drops[$i] > $p_values{'0.05'}{$n_param_diff} ){ # Significant ?
	      print "FORW SIGNIFICANT! OFV_DROP=$ofv_drops[$i] ,NPARM_DIFF= $n_param_diff ",
		$step_relations[$i]{'parameter'},$step_relations[$i]{'covariate'},"\n";
	      $sign_drops{$i} = $ofv_drops[$i];
	    }
	  } else {
	    print "Testing against p-value: $p_values{'0.01'}{-$n_param_diff}\n";
	    if( $ofv_drops[$i] > -$p_values{'0.01'}{-$n_param_diff} ){ # Significant ?
	      print "BACKW SIGNIFICANT! OFV_DROP=$ofv_drops[$i] ,NPARM_DIFF= $n_param_diff ",
		$step_relations[$i]{'parameter'},$step_relations[$i]{'covariate'},"\n";
	      $sign_drops{$i} = $ofv_drops[$i];
	    }
	  }
	}

	my %omega_number;
	$omega_number{'iiv_CL'} = 1;
	$omega_number{'iiv_V'} = 2;
	my %crc_changes;
	foreach my $crit ( 'iiv_CL', 'iiv_V' ) {
	  foreach my $model ( @{$self -> {'prepared_models'}[$model_number-1]{'own'}} ) {
	    $crc_changes{$crit} = $model -> outputs -> [0] -> omegas -> [0][0][$omega_number{$crit}];
	  }
	}

	my %clin_sign;
	# Clinical relevance
	for ( my $i = 0; $i <= $#step_relations; $i++ ) {
	  next unless abs($sign_drops{$i});
	  foreach my $crit ( 'iiv_CL', 'iiv_V' ) {
#	    if ( $self -> {'search_direction'} eq 'forward'){
#	      if ( 
#		  $clin_sign{$crit}{$i}
	  }
	}
	return ( $resulting_model, $chosen_parameter, $chosen_covariate,
		 'CRC', {'BASE_MODEL_TVCL' => $base_cl[0][0],
			 'CHOSEN_MODEL_TVCL' => $chosen_cl } );
      }
end gof_crc

# }}}

# {{{ gof_ofv

start gof_ofv
      {
	my ( $direction, $basic_model, $model_number, $ofv_ch_ref ) = @_;
	my @ofv_changes = @{$ofv_ch_ref};
	my $base_ofv;
	if ( defined $self -> {'base_criteria_values'} and
	     defined $self -> {'base_criteria_values'} ->
	     [$model_number-1] -> {'ofv'} ) {
	  $base_ofv = $self -> {'base_criteria_values'} ->
	    [$model_number-1] -> {'ofv'};
	} elsif ( $direction eq 'backward' ) {
	  'debug' -> die( message => "Backward search needs a 'base' OFV estimate" );
	} else {
	  if ( defined $self -> {'models'} -> [$model_number-1] -> outputs -> [0] ->
	       ofv -> [0][0] ) {
	    $base_ofv = $self -> {'models'} -> [$model_number-1] -> outputs -> [0] ->
	      ofv -> [0][0];
	  } else {
	    'debug' -> die( message => "OFV estimates not available from model" . 
			    $self -> {'models'} -> [$model_number-1] -> full_name );
	  }
	}

	my @models = @{$self -> {'prepared_models'} -> [$model_number-1]{'own'}};
	my @ofvs;
	foreach my $model ( @models ) {
	  push( @ofvs, $model -> outputs -> [0] -> ofv ->[0][0] );
	}

	my ( @ofv_drops, @log_texts );
	my $base_n_param = $self ->
	  {'models'}[$model_number-1] -> nthetas( problem_number => 1 );
	if ( defined $self -> {'included_relations'}[$model_number-1] ) {
	  my %included_relations = %{$self -> {'included_relations'}[$model_number-1]};
#	  while ( my( $incl_par, $cov_ref ) = each %included_relations ) {
#	    while ( my( $incl_cov, $state_ref ) = each %{$included_relations{$incl_par}} ) {
	  foreach my $incl_par ( sort keys %included_relations ) {
	    foreach my $incl_cov ( sort keys %{$included_relations{$incl_par}} ) {
	      $base_n_param += $included_relations{$incl_par}{$incl_cov}{'nthetas'};
	    }
	  }
	}

	open( LOG, ">>".$self -> {'logfile'} -> [$model_number-1] );
	my $un = $direction eq 'backward' ? '(UN)' : '';
	print LOG sprintf("%-8s",'MODEL'),
	  sprintf("%12s",'TEST NAME'),
	    sprintf("%12s",'BASE VAL'),
	      sprintf("%12s",'NEW VAL'),
		sprintf("%50s",'TEST VAL (DROP)'),
		  sprintf("%10s","GOAL"),
		    sprintf("%14s"," $un".'SIGNIFICANT'),"\n";
	my ( %drop_sign, @n_param_diffs );
	my @step_relations = @{$self -> {'step_relations'}};
	for ( my $i = 0; $i <= $#step_relations; $i++ ) {
	  my $n_param_diff = 
	    $self -> {'prepared_models'}[$model_number-1]{'own'}[$i] ->
	      nthetas( problem_number => 1 ) - $base_n_param;
	  push( @n_param_diffs, $n_param_diff );
	  my $change = $direction eq 'forward' ?
	    $ofv_changes[$model_number-1]{$n_param_diff} :
	      -$ofv_changes[$model_number-1]{-$n_param_diff};
	  my $test_val;
	  my $ofv;
	  unless( defined( $ofvs[$i] ) ){
	    $test_val = ' ' x 43 . 'FAILED';
	    $ofv = ' ' x 4 . 'FAILED';
	  } else {
	    $test_val = $base_ofv - $ofvs[$i];
	    $test_val = sprintf("%47.5f",$test_val);
	    $ofv = sprintf("%12.5f",$ofvs[$i])
	  }

	  push ( @ofv_drops, $test_val );
	  my $log_text = sprintf("%-8s",$step_relations[$i]{'parameter'}.
				 $step_relations[$i]{'covariate'}.'-'.
				 $step_relations[$i]{'state'}).
				 sprintf("%12s",'OFV  ').
				 sprintf("%12.5f",$base_ofv).
				 $ofv.
				 $test_val. '  >'.
				 sprintf("%10.5f",$change); 
	  print LOG $log_text;
            # Significant ?
	  if( defined $ofvs[$i] and $test_val > $change ){
	    my $yes_text = sprintf("%12s",'YES!  ');
	    $log_text = $log_text.$yes_text;
	    print LOG $yes_text;
	    $drop_sign{$i} = 1;
	  }
	  print LOG "\n";
	  push( @log_texts, $log_text."\n" );
	}
	print LOG "\n";
	close ( LOG );

	my ( %sign, %l_text );
	for ( my $i = 0; $i <= $#models; $i++ ) {
	  my $od = defined $drop_sign{$i} ? 1 : 0;
	  $sign{$i} = $ofv_drops[$i] if ($od);
	  $l_text{$i} = $log_texts[$i] if ($od);
	}

	my $chosen_ofv;
	my $resulting_model;
	my ( $chosen_parameter, $chosen_covariate, $chosen_log_text );
	if ( scalar keys %sign > 0 ) {
	  my @sorted_ids = sort { $sign{$b} <=> $sign{$a} } keys %sign;
	  $resulting_model = $self ->
	    {'prepared_models'}[$model_number-1]{'own'}[$sorted_ids[0]];
	  $chosen_ofv = $self ->
	    {'prepared_models'}[$model_number-1]{'own'}[$sorted_ids[0]] ->
	      outputs -> [0] -> ofv -> [0][0];
	  $chosen_log_text = $l_text{$sorted_ids[0]};
	  $chosen_parameter = $step_relations[$sorted_ids[0]]{'parameter'};
	  $chosen_covariate = $step_relations[$sorted_ids[0]]{'covariate'};;
	}
	
	return ( $resulting_model,
		 $chosen_parameter,
		 $chosen_covariate,
		 \%drop_sign,
		 \@ofv_drops,
		 'OFV',
		 {'BASE_MODEL_OFV' => $base_ofv,
		  'CHOSEN_MODEL_OFV' => $chosen_ofv },
		 {'ofv' => $chosen_ofv},
#		 \@n_param_diffs,
		 $chosen_log_text,
		 \%l_text );
      }
end gof_ofv

# }}}

# {{{ cov_code_print

start cov_code_print
      {
	my $cov = $self -> {'labels'}{$label};
	my $basic_model = @{$self -> {'models'}}[0];
	my $data = @{$basic_model -> datas}[0];
	my $basic_theta_num;
	my $real_theta_num;
	# Print the start line
	push @TX, "\n;;; $label".$level." START\n";
	my $comment;
	if( $print_oldstyle ){
	  $comment = ";;; ";
	  $basic_theta_num = 0;
	} else {
	  $comment = "";

	  # This assumes one problem per file:
	  $basic_theta_num = $basic_model -> nthetas;
	}
	if( $self -> {'scm_file'} and -e $self -> {'scm_file'}){
	  if( defined $self -> {'spec_code'}{$label}{$level} ){
	    foreach my $code_row( @{$self -> {'spec_code'}{$label}{$level}} ){
	      $code_row =~ s/^\s*;;;\s*//;
	      if( $code_row =~ /THETA/ ){
		$theta_numbering++;
		$real_theta_num = $basic_theta_num + $theta_numbering;
		$code_row =~ s/THETA\(\d+\)/THETA($real_theta_num)/;
	      }
	      push @TX, $code_row;
	    }
	  } else {
	    if( $level == 1 ){
	      push @TX, "$label = 0\n";
	    }
	  }
	} else {
	  # IF at first level
	  if( $level == 1 ){
	    push @TX, "$comment$label = 0\n";
	  } else {
	    if($self -> {'label_type'}{$label} eq "con") {
	      # {{{ Code for continuous covariates
	      # {{{ If at second level

	      if($level == 2) {
		my $med = $self -> {'median'}{$cov};
		$med = sprintf "%6.2f", $med;
		$med =~ s/\s*//;
		if($data -> have_missing_data('column_head' => $cov)) {
		  my $miss_data_lbl = $self -> {'missing_data_token'};
		  push @TX, $comment."IF($cov.EQ.$miss_data_lbl) THEN\n";
		  push @TX, "$comment   $label = 0\n";
		  push @TX, $comment."ELSE\n";
		  $theta_numbering++;
		  $real_theta_num = $basic_theta_num + $theta_numbering;
		  my $sign = '-';
		  if ( $med < 0 ) { $sign = '+'; $med = -$med; }
		  push @TX, "$comment   $label = THETA($real_theta_num)*($cov $sign $med)\n";
		  push @TX, $comment."ENDIF\n";
		} else {
		  $theta_numbering++;
		  $real_theta_num = $basic_theta_num + $theta_numbering;
		  my $sign = '-';
		  if ( $med < 0 ) { $sign = '+'; $med = -$med; }
		  push @TX, "$comment$label = THETA($real_theta_num)*($cov $sign $med)\n";
		}
	      }

	      # }}}
	      # {{{ If at third level

	      if($level == 3) {
		my $med = $self -> {'median'}{$cov};
		$med = sprintf "%6.2f", $med;
		$med =~ s/\s*//;
		$theta_numbering++;
		$real_theta_num = $basic_theta_num + $theta_numbering;
		my $sign = '-';
		if ( $med < 0 ) { $sign = '+'; $med = -$med; }
		push @TX, $comment."IF($cov.LE.$med) $label = THETA($real_theta_num)*($cov $sign $med)\n";
		$theta_numbering++;
		$real_theta_num = $basic_theta_num + $theta_numbering;
		my $sign = '-';
		if ( $med < 0 ) { $sign = '+'; $med = -$med; }
		push @TX, $comment."IF($cov.GT.$med) $label = THETA($real_theta_num)*($cov $sign $med)\n";
		if ($data -> have_missing_data('column_head' => $cov)) {
		  push @TX, $comment."IF($cov.EQ.$self -> {'missing_data_token'})   $label = 0\n";
		}
	      }

	      # }}}
	      # }}}
	    } else {
	      # {{{ Code for categorical variables

	      my %levels;

	      $levels{$cov} = $data -> factors( 'return_occurences' => 1,
						'unique_in_individual' => 0,
						'column_head' => $cov );
	      my @levels = sort {$a<=>$b} keys %{$levels{$cov}};
	      my $numlvs = scalar @levels;
	      $numlvs = $numlvs -1 if $data -> have_missing_data('column_head' => $cov);
	      # If there are two levels and -f center it
	      if ($self -> {'fix'}[$model_number] and $numlvs == 2) {
		# {{{ If fixing and at second level

		my ($lev1, $lev2);
		my %fractions = $data -> fractions( 'unique_in_individual' => 1,
						    'column_head' => $cov,
						    'ignore_missing' => 1);
		my $fraction;
		if ($fractions{$levels[0]} > $fractions{$levels[1]}) {
		  $lev1 = 0;
		  $lev2 = 1;
		  $fraction = $fractions{$levels[0]};
		} else {
		  $lev1 = 1;
		  $lev2 = 0;
		  $fraction = $fractions{$levels[0]};
		}
		$fraction = sprintf("%4.2f", $fraction);
		if ($data -> have_missing_data('column_head' => $cov)) {
		  push @TX,  $comment."IF($cov.EQ.$levels[$lev1].OR.$cov.EQ.$self -> {'missing_data_token'})";
		  $theta_numbering++;
		  $real_theta_num = $basic_theta_num + $theta_numbering;
		  push @TX,  "$comment$label = -1*THETA($real_theta_num)*(1-$fraction)\n";
		  push @TX,  $comment."IF($cov.EQ.$levels[$lev2])";
		  $theta_numbering++;
		  $real_theta_num = $basic_theta_num + $theta_numbering;
		  push @TX,  "$comment$label = $fraction*THETA($real_theta_num)\n";
		} else {
		  push @TX, $comment."IF($cov.EQ.$levels[$lev1])";
		  $theta_numbering++;
		  $real_theta_num = $basic_theta_num + $theta_numbering;
		  push @TX, "$comment$label = -1*THETA($real_theta_num)*(1-$fraction)\n";
		  push @TX, $comment."IF($cov.EQ.$levels[$lev2])";
		  $theta_numbering++;
		  $real_theta_num = $basic_theta_num + $theta_numbering;
		  push @TX, "$comment$label = $fraction*THETA($real_theta_num)\n";
		}

		# }}}
	      } elsif ($self -> {'fix'}[$model_number] and $numlvs > 2) {
		# {{{ If at fixing and third level 

		# Here we have two cases:
		# 1) The median is the same as the highest or lowest level
		# 2) The median is in the middle
		my $med = sprintf "%6.2f",$self -> {'median'}{$cov};
		$med =~ s/\s*//;
		# Second level
		if ($med == $levels[0] or $med == $levels[$#levels]) {
		  if ($data -> have_missing_data('column_head' => $cov)) {
		    push @TX, $comment."IF($cov.EQ.$self -> {'missing_data_token'}) THEN\n";
		    push @TX, "$comment    $label = 0\n";
		    push @TX, $comment."ELSE\n";
		    $theta_numbering++;
		    $real_theta_num = $basic_theta_num + $theta_numbering;
		    my $sign = '-';
		    if ( $med < 0 ) { $sign = '+'; $med = -$med; }
		    push @TX, "$comment    $label = THETA($real_theta_num)*($cov $sign $med)\n";
		    push @TX, $comment."ENDIF\n";
		  } else {
		    $theta_numbering++;
		    $real_theta_num = $basic_theta_num + $theta_numbering;
		    my $sign = '-';
		    if ( $med < 0 ) { $sign = '+'; $med = -$med; }
		    push @TX, "$comment$label = THETA($real_theta_num)*($cov $sign $med)\n";
		  }
		} else {
		  $theta_numbering++;
		  $real_theta_num = $basic_theta_num + $theta_numbering;
		  my $sign = '-';
		  if ( $med < 0 ) { $sign = '+'; $med = -$med; }
		  push @TX, $comment."IF($cov.LE.$med) $label = THETA($real_theta_num)*($cov $sign $med)\n";
		  $theta_numbering++;
		  $real_theta_num = $basic_theta_num + $theta_numbering;
		  my $sign = '-';
		  if ( $med < 0 ) { $sign = '+'; $med = -$med; }
		  push @TX, $comment."IF($cov.GT.$med) $label = THETA($real_theta_num)*($cov $sign $med)\n";
		  if ($data -> have_missing_data('column_head' => $cov)) {
		    push @TX, $comment."IF($cov.EQ.$self -> {'missing_data_token'})   $label = 0\n";
		  }
		}

		# }}}
	      } else {
		# {{{ Otherwise

		if ($level == 2) {
		  my $first = 0;
		  ## Print out the code. Note that for the missing covariate to work
		  ## it has to have a value lower than all the other levels.
		  my $havemiss;
		  foreach my $lev (@levels) {
		    if ($first == 0) {
		      # Register that there are a missing value
		      if ($lev == $self -> {'missing_data_token'}) {
			$havemiss = 1;
			next;
		      }
		      # OK, there is a missing
		      if ($havemiss) {
			push @TX, $comment."IF($cov.EQ.$lev.OR.$cov.EQ.$self -> {'missing_data_token'}) $label = 0\n";
			$havemiss = 0;
		      } else {
			push @TX, $comment."IF($cov.EQ.$lev) $label = 0\n";
		      }
		      $first = 1;
		    } else {
		      # This if statement is probably not necessary
		      if ($havemiss) {
			$theta_numbering++;
			$real_theta_num = $basic_theta_num + $theta_numbering;
			push @TX, $comment."IF($cov.EQ.$lev.OR.$cov.EQ.$self -> {'missing_data_token'}) $label = THETA($real_theta_num)\n";
			$havemiss = 0;
		      } else {
			$theta_numbering++;
			$real_theta_num = $basic_theta_num + $theta_numbering;
			push @TX, $comment."IF($cov.EQ.$lev) $label = THETA($real_theta_num)\n";
		      }
		    }
		  }

		  # }}}
		}

		# }}}
	      }
	    }
	  }

	}
  
	push @TX, ";;; $label".$level." END\n\n";
}

end cov_code_print

# }}}

# {{{ cov_comb_print
start cov_comb_print
    {
      # Combining of covariates
    foreach my $par (@{$self -> {'parameters'}}) {
	my $ind = 0;
	my $parmind = 0;
	my @covmods;
	
	foreach( @{$self -> {'parm_cov_labels'}{$par}} ){
	    $ind++;
	    push( @{$covmods[$parmind]}, $_ );
	    if( $ind == 4 ){
		$parmind ++;
		$ind = 0;
	    }
	}
	foreach my $i (0.. $#covmods) {
	    if($i == 0) {
		push @TX, "$par"."COV=";
	    } else {
		push @TX, "$par"."COV=$par"."COV*";
	    }
	    my @tmp = @{$covmods[$i]};
	    if($self -> {'exp'}) {
		@tmp = map( $_ = "EXP($_)", @tmp);
		push( @TX, join('*', @tmp) );
	    } else {
		@tmp = map( $_ = "(1+$_)", @tmp);
		push( @TX, join('*', @tmp) );
	    }
	    push @TX, "\n";
	}
	push @TX, "\n\n";
    }
}
end cov_comb_print
# }}}

# {{{ _write

start _write
    {
    my @TX;
    push @TX, "\n";
    # {{{ Print declarations
    { 
	my $ind = 0;
	my $tmp;
	foreach( keys %{$self -> {'labels'}}) {
	    $tmp .= " $_";
	    $ind ++;
	    if( $ind == 4 ) {
		push @TX, ';;; DECL:'.$tmp,"\n";
		$ind = 0;
		$tmp = '';
	    }
	}
	if( $ind > 0 ){
	    push @TX, ';;; DECL:'. $tmp . "\n";
	} 
    }  
    # }}}
    # Print Bounds
    foreach( keys %{$self -> {'labels'}} ){
	my ($lower, $upper) = @{$self -> {'bounds'}{$_}};
	push @TX, ';;; BOUNDS: '."($lower,,$upper)  ; $_\n";
    }
    # {{{ Print the extra information before the actual models

    ## The missing data label
    push @TX, ';;; MISS: '. $self -> {'missing_data_token'} ."\n";
    ## P-values
    if( $self -> {'gof_description'} ){
	push @TX, ';;; GOF: '.$self->{'gof_description'}."\n";
    } else {
	push @TX, ';;; GOF: EXTERN\n';
    }
    #push @TX, ';;; PFORW: '. $self -> {'pforward'} . "\n";
    #push @TX, ';;; PBACK: '. $self -> {'pbackward'} . "\n";
    ## List file    
    # push @TX, ';;; LST: '. @{@{$self -> {'models'}}[0] -> outputs}[0] -> filename . "\n";
    ## Log file    
    # push @TX, ';;; LOG: '. $self -> logfile . "_scm\n";
    ## Task
    push @TX, ';;; TASK: '. $self -> {'search_direction'} . "\n";
    ## If the thetas were to be fixed
    push @TX, ';;; FIX: '. $self -> {'fix'}[$model_number] . "\n";

    # }}}

    foreach( keys %{$self -> {'labels'}} ) {
	my ($code, undef) = $self -> cov_code_print( model_number => $model_number,
						     'label' => $_,
						     'level' => 1,
						     'print_oldstyle' => 1,
						     'theta_numbering' => 0);
	push( @TX, @{$code} );
	($code, undef) = $self -> cov_code_print( model_number => $model_number,
						  'label' => $_, 
						  'level' => 2,
						  'print_oldstyle' => 1,
						  'theta_numbering' => 0);
	push( @TX, @{$code} );
	($code, undef) = $self -> cov_code_print( model_number => $model_number,
						  'label' => $_, 
						  'level' => 3,
						  'print_oldstyle' => 1,
						  'theta_numbering' => 0);
	push( @TX, @{$code} );
    }
    my $outmodel = @{$self -> {'models'}}[0] -> copy(filename => $filename,
						     target => 'mem',
						     ignore_missing_files => 1);
    push( @TX, @{$outmodel -> pk()} );
    $outmodel -> pk( 'new_pk' => \@TX );
    $outmodel -> write;
}
end _write

# }}}

# {{{ _create_models

start _create_models
      {
	# Set names of tested parameter-covariate-state combinations in results
	# Done in loop below after state change
	my %return_section;
	$return_section{'name'} = 'combinations';
	$return_section{'values'} = [];
	$return_section{'labels'} = [];

	my $done = ( -e $self -> {'directory'}."/m$model_number/done" ) ? 1 : 0;

	if ( not $done ) {
	  open( DONE_LOG, '>'.$self -> {'directory'}."/m$model_number/done.log" );
	  foreach my $parameter ( sort keys %relations ) {
	    foreach my $covariate ( sort keys %{$relations{$parameter}} ) {
	      # Is this covariate continuous or not?
	      my $continuous = 1;
	      foreach my $cat ( @{$self -> {'categorical_covariates'}} ) {
		$continuous = 0 if ( $covariate eq $cat );
	      }
	      my @valid_states;
	      if ( $continuous ) {
		@valid_states = @{$self -> {'valid_states'}{'continuous'}};
	      } else {
		@valid_states = @{$self -> {'valid_states'}{'categorical'}};
	      }
	      my $state;
	      # Important: just setting $state to $self->incl_rel....{'state'} initiates
	      # included_relations for this parameter and covariate. Avoid this.
	      if ( defined $included_relations{$parameter}{$covariate} ) {
		$state = $included_relations{$parameter}{$covariate}{'state'};
	      }
	      $state = defined $state ? $state : $valid_states[0];
	      #1. Create a new model if the state is not yet included and at highest level
	      # (forward search) or base level (backward search).
	      next if ( ( $self -> {'search_direction'} eq 'forward' and
			  $state == $valid_states[$#valid_states] ) or
			( $self -> {'search_direction'} eq 'backward' and
			  $state == $valid_states[0] ) );
	      my $old_state = $state;
	      # Increment (forward search) or decrement (backward search) the state
	      if ( $self -> {'search_direction'} eq 'forward' ) {
		my $flag = 0;
		for( my $s_idx = 0; $s_idx <= $#valid_states; $s_idx++ ) {
		  if ( $flag ) {
		    $state = $valid_states[$s_idx];
		    last;
		  }
		  $flag = 1 if( $state == $valid_states[$s_idx] );
		}
	      } elsif ( $self -> {'search_direction'} eq 'backward' ) {
		my $flag = 0;
		for( my $s_idx = $#valid_states; $s_idx >= 0; $s_idx-- ) {
		  if ( $flag ) {
		    $state = $valid_states[$s_idx];
		    last;
		  }
		  $flag = 1 if( $state == $valid_states[$s_idx] );
		}
	      }

	      # Only one problem and one sub problem
	      push( @{$return_section{'values'}[0][0]}, "$parameter$covariate-$state" );

	      my ( $dir, $filename ) =
		  OSspecific::absolute_path( $self -> {'directory'}.
					     '/m'.$model_number.'/',
					     $parameter.$covariate.$state.".mod" );
	      my ( $odir, $outfilename ) =
		  OSspecific::absolute_path( $self -> {'directory'}.
					     '/m'.$model_number.'/',
					     $parameter.$covariate.$state.".lst" );
	      my $applicant_model;
	      $applicant_model = $orig_model ->
		  copy( filename    => $dir.$filename,
			copy_data   => 0,
			copy_output => 0 );

	      $applicant_model -> ignore_missing_files(1);
	      $applicant_model -> outputfile( $odir.$outfilename );
	      my @table_names = @{$applicant_model -> table_names};
	      for ( my $i = 0; $i <= $#table_names; $i++ ) {
		for ( my $j = 0; $j < scalar @{$table_names[$i]}; $j++ ) {
		  $table_names[$i][$j] = $self -> {'directory'}.
		      '/m'.$model_number.'/'.
		      $filename.'.'.
		      OSspecific::nopath($table_names[$i][$j]);
		}
	      }
	      $applicant_model -> table_names( new_names            => \@table_names,
					       ignore_missing_files => 1);
	      #	    print "$parameter-$covariate: new state: $state\n";
	      my @used_covariates = ();
	      # $included_relations is a reference to $self -> included_relations
	      # and should only be initialized for truly incorporated relations
	      # see beginning of loop above.
	      foreach my $incl_par ( sort keys %included_relations ) {
		foreach my $incl_cov ( sort keys %{$included_relations{$incl_par}} ) {
		  next if ( $incl_par eq $parameter and $incl_cov eq $covariate );
		  $self -> 
		      add_code( definition_code => $included_relations{$incl_par}{$incl_cov}{'code'},
				nthetas         => $included_relations{$incl_par}{$incl_cov}{'nthetas'},
				inits           => $included_relations{$incl_par}{$incl_cov}{'inits'},
				bounds          => $included_relations{$incl_par}{$incl_cov}{'bounds'},
				applicant_model => $applicant_model,
				parameter       => $incl_par,
				covariate       => $incl_cov );
		  push( @used_covariates, $incl_cov );
		}
	      }
	      # If the new state is base level (backward search) don't add this relation, otherwise:
	      unless ( $state == $valid_states[0] ) {
		$self -> add_code( definition_code => $relations{$parameter}{$covariate}{'code'}{$state},
				   nthetas         => $relations{$parameter}{$covariate}{'nthetas'}{$state},
				   inits           => $relations{$parameter}{$covariate}{'inits'}{$state},
				   bounds          => $relations{$parameter}{$covariate}{'bounds'}{$state},
				   applicant_model => $applicant_model,
				   parameter       => $parameter,
				   covariate       => $covariate );
		push( @used_covariates, $covariate );
	      }
	      my @all_covariates;
	      if ( defined $self -> {'categorical_covariates'} ) {
		push( @all_covariates, @{$self -> {'categorical_covariates'}});
	      }
	      if ( defined $self -> {'continuous_covariates'} ) {
		push( @all_covariates, @{$self -> {'continuous_covariates'}});
	      }
	      $self -> drop_undrop_covariates( applicant_model => $applicant_model,
					       used_covariates => \@used_covariates,
					       all_covariates  => \@all_covariates,
					       do_not_drop     => $self -> {'do_not_drop'});

	      if ( defined $base_model ) {
		$applicant_model -> update_inits( from_model    => $base_model,
						  update_thetas => 1,
						  update_omegas => 1,
						  update_sigmas => 1,
						  ignore_missing_parameters => 1 );
	      } else {
		$applicant_model -> update_inits( from_model    => $orig_model,
						  update_thetas => 1,
						  update_omegas => 1,
						  update_sigmas => 1,
						  ignore_missing_parameters => 1 );
	      }
	      $applicant_model -> _write;
	      push( @new_models, $applicant_model );
	      my $model_id  = $applicant_model -> register_in_database;
	      $self -> register_tm_relation( model_ids       => [$model_id],
					     prepared_models => 1 );
	      my %st_rel;
	      $st_rel{'parameter'} = $parameter;
	      $st_rel{'covariate'} = $covariate;
	      $st_rel{'state'}     = $state;
	      $st_rel{'continuous'} = $continuous;
	      push( @step_relations, \%st_rel );
	      print DONE_LOG "$parameter $covariate $continuous $old_state $state\n";
	    }
	  }
	  open( TMP, ">".$self -> {'directory'}."/m$model_number/done" );
	  close( TMP );
	  close( DONE_LOG );
	} else {
	  ui -> print( category => 'scm',
		       message  => "Recreating models from previously run step" );
	  debug -> warn( level => 2,
			   message => "Creating applicant model from file on disk" );
	  if ( not -e $self -> {'directory'}."/m$model_number/done.log" ) {
	    debug -> die( message => "No file ".$self -> {'directory'}.
			  "/m$model_number/done.log seem to exist although the existance".
			  " of the file ".$self -> {'directory'}.
			  "/m$model_number/done indicates so.");
	  }
	  open( DONE_LOG, $self -> {'directory'}."/m$model_number/done.log" );
	  my @rows = <DONE_LOG>;
	  close( DONE_LOG );
	  for( my $i = 0; $i <= $#rows; $i++ ) { # skip first row
	    chomp( $rows[$i] );
	    my ( $parameter, $covariate, $continuous, $old_state, $state ) =
		split(' ',$rows[$i],5);
	    my @valid_states;
	    if ( $continuous ) {
	      @valid_states = @{$self -> {'valid_states'}{'continuous'}};
	    } else {
	      @valid_states = @{$self -> {'valid_states'}{'categorical'}};
	    }
	    #1. Recreate the model if the state is not yet included and at highest level
	    # (forward search) or base level (backward search).
	    next if ( ( $self -> {'search_direction'} eq 'forward' and
			$old_state == $valid_states[$#valid_states] ) or
		      ( $self -> {'search_direction'} eq 'backward' and
			$old_state == $valid_states[0] ) );

	    my ( $dir, $filename ) = OSspecific::absolute_path( $self -> {'directory'}.'/m'.$model_number.'/',$parameter.$covariate.$state.".mod");
	    my ( $odir, $outfilename ) = OSspecific::absolute_path( $self -> {'directory'}.'/m'.$model_number.'/',$parameter.$covariate.$state.".lst");

	    my $applicant_model = model -> new( reference_object     => $orig_model,
						outputs              => undef,
						datas                => undef,
						synced               => undef,
						problems             => undef,
						active_problems      => undef,
						filename   => $dir.$filename,
						outputfile => $odir.$outfilename,
						target     => 'disk',
						ignore_missing_files => 1 );
	    # Set the correct data file for the object
	    my $moddir = $orig_model -> directory;
	    my @datafiles = @{$orig_model -> datafiles};
	    for( my $df = 0; $df <= $#datafiles; $df++ ) {
	      $datafiles[$df] = $moddir.'/'.$datafiles[$df];
	    }
	    $applicant_model -> synchronize;
	    $applicant_model -> datafiles( new_names => \@datafiles );
	    push( @new_models, $applicant_model );
	    my %st_rel;
	    $st_rel{'parameter'} = $parameter;
	    $st_rel{'covariate'} = $covariate;
	    $st_rel{'state'}     = $state;
	    $st_rel{'continuous'} = $continuous;
	    push( @step_relations, \%st_rel );
	    my $nl = $i == $#rows ? "" : "\r"; 
	    ui -> print( category => 'scm',
			 message  => ui -> status_bar( sofar => $i+1,
						       goal  => $#rows+1 ).$nl,
			 wrap     => 0,
			 newline  => 0 )
	  }
	  ui -> print( category => 'scm',
		       message  => " ... done." );
	}
	push( @{$self -> {'results'}[$model_number-1]{'own'}},\%return_section );
      }
end _create_models

# }}} _create_models

# {{{ calculate_categorical_statistics

start calculate_categorical_statistics
    {
    # Levels = the number of unique factors of a covariate
    # my @levels = sort {$a<=>$b} keys %factors;
    # Sort by frequency
    my @sorted = sort {$factors{$a}<=>$factors{$b}} keys (%factors);
    # These lines will set the most common value in $medians{$cov}
    $median = $sorted[0]; # First element of the sorted array
	                  # (the factor that most subjects have)

#	$min = $sorted[$#sorted];    # The first element of the (also sorted) level array
#	$max = $sorted[0];           # The last element of the (also sorted) level array 

    }
end calculate_categorical_statistics

# }}} calculate_categorical_statistics

# {{{ calculate_continuous_statistics

start calculate_continuous_statistics
      {
	# Max, median and min
	$median = $model -> median( problem_number => 1,
				   'column_head' => $covariate,
				   'unique_in_individual' => 1);
	$max = $model -> max( problem_number => 1,
			 'column_head' => $covariate );
	$min = $model -> min( problem_number => 1,
			 'column_head' => $covariate );

	$median = sprintf("%6.2f", $median );
      }
end calculate_continuous_statistics

# }}} calculate_continuous_statistics

# {{{ create_linear_code

start create_linear_code
      {
	# Creates code for linear continuous and categorical
	# parameter-covariate relations Returns code, the number of
	# thetas necessary for the relation plus initial values and
	# boundaries for these.
	if ( defined $bounds{'upper'} and ref $bounds{'upper'} ne 'ARRAY' ) {
	  debug -> die( message => "Wrong data type of upper bounds. The upper bounds should be kept in an array" );
	}
	if ( defined $bounds{'lower'} and ref $bounds{'lower'} ne 'ARRAY' ) {
	  debug -> die( message => "Wrong data type of lower bounds. The lower bounds should be kept in an array" );
	}

        my $theta_number = defined $start_theta ? $start_theta : 1;
	my $comment = "";
	my $median = $statistics{'median'};
	my $min = $statistics{'min'};
	my $max = $statistics{'max'};
	$median = sprintf "%6.2f", $median;
	$median =~ s/\s*//;
	if ( $continuous ) {
	  if ( scalar @code < 1 or ( scalar @code == 1 and $code[0] eq '' ) ) {
	    if( $statistics{'have_missing_data'} ) {
	      $code[0] = $comment."IF($covariate.EQ.$missing_data_token) THEN\n";
	      $code[1] = "$comment   $parameter$covariate = 1\n";
	      $code[2] = $comment."ELSE\n";
	      my $sign = '-';
	      if ( $median < 0 ) { $sign = '+'; $median = -$median; }
	      $code[3] = "$comment   $parameter$covariate = ( 1 + THETA(".$theta_number++.")*($covariate $sign $median))\n";
	      $code[4] = $comment."ENDIF\n";
	    } else {
	      my $sign = '-';
	      if ( $median < 0 ) { $sign = '+'; $median = -$median; }
	      $code[0] = "$comment$parameter$covariate = ( 1 + THETA(".$theta_number++.")*($covariate $sign $median))\n";
	    }
	  } else {
	    # count the thetas. One theta per row is allowed
	    for ( @code ) {
	      $theta_number++ if /THETA\((\d+)\)/;
	    }
	  }
	  # Boundaries
	  if ( $median-$min == 0 ) {
	    debug -> die( message => "median($median)-min($min)",
			  " for column $covariate is zero. Cannot calculate an upper boundary." );
	  }
	  if ( $median-$max == 0 ) {
	    debug -> die( message => "median($median)-min($min)",
			  " for column $covariate is zero. Cannot calculate an lower boundary." );
	  }
	  
	  ## Boundaries:
	  unless ( defined $bounds{'upper'} and defined $bounds{'upper'}[0] ) {
	    my $upper_bound     = 1/($median-$min);
	    my ($big,$small) = split('\.',$upper_bound);
	    $small           = substr($small,0,3);
	    $upper_bound     = $big.'.'.$small;
	    $upper_bound     = '0' if eval($upper_bound) == 0;
	    $bounds{'upper'}[0] = $upper_bound;
	  }
	  unless ( defined $bounds{'lower'} and defined $bounds{'lower'}[0] ) {
	    my $lower_bound     = 1/($median - $max);
	    my ($big,$small)    = split('\.',$lower_bound);
	    $small           = substr($small,0,3);
	    $lower_bound     = $big.'.'.$small;
	    $lower_bound     = '0' if eval($lower_bound) == 0;
	    $bounds{'lower'}[0] = $lower_bound;
	  }

	} else {
	  my %factors = %{$statistics{'factors'}};
	  my @levels = sort {$a<=>$b} keys %factors;
	  my @sorted = sort {$factors{$b}<=>$factors{$a}} keys(%factors);
	  my $numlvs = scalar @levels;
	  $numlvs = $numlvs -1 if $statistics{'have_missing_data'};

	  my $first_non_missing = 1;
	  if ( scalar @code < 1 or ( scalar @code == 1 and $code[0] eq '' ) ) {
	    @code = ();
	    for ( my $i = 0; $i <= $#sorted; $i++ ) {
	      if ( $statistics{'have_missing_data'} and
		   ( $self -> {'missing_data_token'} eq $sorted[$i] ) ) {
		push @code, $comment."IF($covariate.EQ.$sorted[$i]) $parameter$covariate".
		    " = 1  ; Missing data\n";
	      } else {
		if ( $first_non_missing ) {
		  push @code, $comment."IF($covariate.EQ.$sorted[$i]) $parameter$covariate".
		      " = 1  ; Most common\n";
		  $first_non_missing = 0;
		} else {
		  push @code, $comment."IF($covariate.EQ.$sorted[$i]) $parameter$covariate".
		      " = ( 1 + THETA(".$theta_number++."))\n";
		}
	      }
	    }
	  } else {
	    # count the thetas. One theta per row is allowed
	    for ( @code ) {
	      $theta_number++ if /THETA\((\d+)\)/;
	    }
	  }
	  ## Boundaries:
	  for ( my $i = 0; $i < $theta_number - $start_theta; $i++ ) {
	    $bounds{'upper'}[$i] = 5  unless ( defined $bounds{'upper'}[$i] );
	    $bounds{'lower'}[$i] = -1 unless ( defined $bounds{'lower'}[$i] );
	  }
	}
	# Initial values
	for ( my $i = 0; $i < $theta_number - $start_theta; $i++ ) {
	  next if ( defined $inits[$i] );
	  my $fraction = $self -> {'global_init'};
	  my $tmp;
	  if( ( abs($bounds{'upper'}[$i]) >= 1000000 or 
		not defined $bounds{'upper'}[$i] ) and
	      ( abs($bounds{'lower'}[$i]) >= 1000000 or
		not defined $bounds{'lower'}[$i] ) ) {
	    $tmp = $fraction*100;
	  } else {
	    if ( abs($bounds{'upper'}[$i]) < abs($bounds{'lower'}[$i]) ) {
	      $tmp = $bounds{'upper'}[$i] == 0 ? $bounds{'lower'}[$i]*$fraction :
		  $bounds{'upper'}[$i]*$fraction;
	    } else {
	      $tmp = $bounds{'lower'}[$i] == 0 ? $bounds{'upper'}[$i]*$fraction :
		  $bounds{'lower'}[$i]*$fraction;
	    }
	  }
	  $inits[$i] = $tmp;
	}
	$end_theta = --$theta_number;
      }
end create_linear_code

# }}} create_linear_code

# {{{ create_exponential_code

start create_exponential_code
      {
	my $theta_number = $start_theta;
	my $median = $statistics{'median'};
	$median = sprintf "%6.2f", $median;
	$median =~ s/\s*//;
	if ( $continuous ) {
	  if ( scalar @code < 1 or ( scalar @code == 1 and $code[0] eq '' ) ) {
	    my $sign = '-';
	    if ( $median < 0 ) { $sign = '+'; $median = -$median; }
	    $code[0] = "   $parameter$covariate = EXP(THETA(".$theta_number++.")*($covariate $sign $median))\n";
	    if ( $statistics{'have_missing_data'} ) {
	      $code[1] = "IF($covariate.EQ.$missing_data_token)   ".
		  "$parameter$covariate = 1\n";
	    }
	  } else {
	    # count the thetas. One theta per row is allowed
	    for ( @code ) {
	      $theta_number++ if /THETA\((\d+)\)/;
	    }
	  }
	  ## Boundaries:
	  unless ( defined $bounds{'upper'} and defined $bounds{'upper'}[0] ) {
	    $bounds{'upper'}[0] = 1000000;
	  }
	  unless ( defined $bounds{'lower'} and defined $bounds{'lower'}[0] ) {
	    $bounds{'lower'}[0] = -1000000;
	  }
	}
	# Initial values
	for ( my $i = 0; $i < $theta_number - $start_theta; $i++ ) {
	  next if ( defined $inits[$i] );
	  $inits[$i] = $self -> {'global_init'};
	}
	$end_theta = --$theta_number;
      }
end create_exponential_code

# }}} create_exponential_code

# {{{ create_hockey_stick_code

start create_hockey_stick_code
      {
	my $theta_number = $start_theta;
	my $median = $statistics{'median'};
	$median = sprintf "%6.2f", $median;
	my $min = $statistics{'min'};
	my $max = $statistics{'max'};
	$median =~ s/\s*//;
	if ( $continuous ) {
	  if ( scalar @code < 1 or ( scalar @code == 1 and $code[0] eq '' ) ) {
	    my $sign = '-';
	    if ( $median < 0 ) { $sign = '+'; $median = -$median; }
	    $code[0] = "IF($covariate.LE.$median) $parameter$covariate = ( 1 + THETA(".
		$theta_number++.")*($covariate $sign $median))\n";
	    $code[1] = "IF($covariate.GT.$median) $parameter$covariate ".
		"= ( 1 + THETA(".$theta_number++.")*($covariate $sign $median))\n";
	    if ( $statistics{'have_missing_data'} ) {
	      $code[2] = "IF($covariate.EQ.$missing_data_token)   ".
		  "$parameter$covariate = 1\n";
	    }
	  } else {
	    # count the thetas. One theta per row is allowed
	    for ( @code ) {
	      $theta_number++ if /THETA\((\d+)\)/;
	    }
	  }
	  ## Boundaries:
	  if ( not defined $bounds{'upper'} ) {
	    if ( not defined $bounds{'upper'}[0] ) {
	      my $upper_bound     = 1/($median-$min);
	      my ($big,$small) = split('\.',$upper_bound);
	      $small           = substr($small,0,3);
	      $upper_bound     = $big.'.'.$small;
	      $upper_bound     = '0' if eval($upper_bound) == 0;
	      $bounds{'upper'}[0] = $upper_bound;
	    }
	    $bounds{'upper'}[1] = 1000000 if ( not defined $bounds{'upper'}[1] );
	  }
	  if ( not defined $bounds{'lower'} ) {
	    $bounds{'lower'}[0] = -1000000 if ( not defined $bounds{'lower'}[0] );
	    if ( not defined $bounds{'lower'}[1] ) {
	      my $lower_bound     = 1/($median - $max);
	      my ($big,$small)    = split('\.',$lower_bound);
	      $small           = substr($small,0,3);
	      $lower_bound     = $big.'.'.$small;
	      $lower_bound     = '0' if eval($lower_bound) == 0;
	      $bounds{'lower'}[1] = $lower_bound;
	    }
	  }
	}
	# Initial values
	for ( my $i = 0; $i < $theta_number - $start_theta; $i++ ) {
	  next if ( defined $inits[$i] );
	  my $fraction = $self -> {'global_init'};
	  my $tmp;
	  if( ( abs($bounds{'upper'}[$i]) >= 1000000 or 
		not defined $bounds{'upper'}[$i] ) and
	      ( abs($bounds{'lower'}[$i]) >= 1000000 or
		not defined $bounds{'lower'}[$i] ) ) {
	    $tmp = $fraction*100;
	  } else {
	    if ( abs($bounds{'upper'}[$i]) < abs($bounds{'lower'}[$i]) ) {
	      $tmp = $bounds{'upper'}[$i] == 0 ? $bounds{'lower'}[$i]*$fraction :
		  $bounds{'upper'}[$i]*$fraction;
	    } else {
	      $tmp = $bounds{'lower'}[$i] == 0 ? $bounds{'upper'}[$i]*$fraction :
		  $bounds{'lower'}[$i]*$fraction;
	    }
	  }
	  $inits[$i] = $tmp;
	}
	$end_theta = --$theta_number;
      }
end create_hockey_stick_code

# }}} create_hockey_stick_code

# {{{ create_code

start create_code
      {
	my $new_code = [];
	my $new_inits = [];
	my $new_bounds = {};
	if ( $state == 1 ) {
	  # This is the default basic state. No relation included
	} elsif ( $state == 2 ) {
	  # First state
	  ( $new_code, $end_theta, $new_inits, $new_bounds ) =
	    $self -> create_linear_code( start_theta        => $start_theta,
					 parameter          => $parameter,
					 covariate          => $covariate,
					 continuous          => $continuous,
					 bounds             => \%bounds,
					 inits              => \@inits,
					 code               => \@code,
					 statistics         => \%statistics,
					 missing_data_token => $missing_data_token,
					 fix                => $fix );
	} elsif ( $state == 3 ) {
	  ( $new_code, $end_theta, $new_inits, $new_bounds ) =
	    $self -> create_hockey_stick_code( start_theta        => $start_theta,
					       parameter          => $parameter,
					       covariate          => $covariate,
					       continuous          => $continuous,
					       bounds             => \%bounds,
					       inits              => \@inits,
					       code               => \@code,
					       statistics         => \%statistics,
					       missing_data_token => $missing_data_token,
					       fix                => $fix );
	} elsif ( $state == 4 ) {
	  ( $new_code, $end_theta, $new_inits, $new_bounds ) =
	    $self -> create_exponential_code( start_theta        => $start_theta,
					      parameter          => $parameter,
					      covariate          => $covariate,
					      continuous          => $continuous,
					      bounds             => \%bounds,
					      inits              => \@inits,
					      code               => \@code,
					      statistics         => \%statistics,
					      missing_data_token => $missing_data_token,
					      fix                => $fix );
	} else {
	  debug -> die( message => "No such state $state\n" );
	}
	@inits = @{$new_inits};
	%bounds = %{$new_bounds};
	@code = @{$new_code};
      }
end create_code

# }}} create_code

# {{{ add_code

start add_code
      {
	my @labels;
	for ( my $i = 1; $i <= $nthetas; $i++ ) {
	  push( @labels, $parameter.$covariate.$i );
	}

	my $start_theta = $applicant_model -> nthetas + 1;
	my $end_theta = $start_theta + $nthetas - 1;

	my $tmp = $start_theta;
	for ( @definition_code ) {
	  if ( /THETA\((\d+)\)/ ) {
	    s/THETA\((\d+)\)/THETA\($tmp\)/;
	    $tmp++;
	  }
	}

	my $start_theta = $applicant_model -> nthetas + 1;

	# Add the definition_code to the PK or PRED block
	my @code;
	@code = @{$applicant_model -> pk( problem_number => 1 )};
	my $use_pred = 0;
	unless ( $#code > 0 ) {
	  @code = @{$applicant_model -> pred( problem_number => 1 )};
	  $use_pred = 1;
	}
	if ( $#code <= 0 ) {
	  debug -> die( message => "Neither PK or PRED defined in " .
			$applicant_model -> filename . "\n" );
	}

	my $found_REL = 0;
	my $i = 0;
	my $relationarea = 0;
	my @row;
	my $found_correct_REL = 0;
	for ( @code ) {
	  if ( /^;;; (\w+)-RELATION START/ and $1 eq $parameter ) {
	    $relationarea = 1;
	    $i++;
	    next;
	  }
	  if ( /^;;; (\w+)-RELATION END/ and $1 eq $parameter ) {
	    $relationarea = 0;
	    last;
	  }
	  if ( $relationarea ) {
	    $found_REL = $i;
	    debug -> warn( level => 2,
			     message => $parameter . "COV has already been added to the code" );
	    $found_correct_REL = 1 and last if ( /$parameter$covariate/ );
	    @row = split(/\)\*\(/);
	  }
	  $i++;
	}

	# If we have old scm code present.
	if ( $found_REL ) {
	  unless ( $found_correct_REL ) {
	    if ( $#row > 2 ) {
	      @code =  (@code[0..$found_REL],
			    "$parameter"."COV=$parameter"."COV*$parameter$covariate\n",
			    @code[$found_REL+1..$#code]);
	    } else {
	      chomp($code[$found_REL]);
	      $code[$found_REL] = $code[$found_REL]."*$parameter$covariate\n";
	    }
	  }
	} else {
	  @code = ( ";;; $parameter-RELATION START\n",
		    "$parameter"."COV=$parameter$covariate\n",
		    ";;; $parameter-RELATION END\n\n",
		    @code );
	}

	@code = ( "\n;;; $parameter$covariate-DEFINITION START\n",
		  @definition_code,
		  ";;; $parameter$covariate-DEFINITION END\n\n",
		  @code );

	# Add to the parameter code
 	unless ( $found_REL ) {
 	  my $success = 0;
	  for ( @code ) {
	    if ( /^\s*TV(\w+)\s*=\s*/ and $1 eq $parameter ) {
	      my ($line,$comment) = split( ';', $_, 2 );
	      $_ = $line;
#	    if ( /^\s*TV(\w+)\s*=\s*THETA\((\d+)\)/ and $1 eq $parameter ) {
#	      s/^(\s*)TV(\w+)\s*=\s*THETA\((\d+)\)/$1TV$2 = $2COV*THETA($3)/;
	      chomp;
	      s/^(\s*)TV(\w+)\s*=\s*(.*)/$1TV$2 = $2COV*\($3\)/;
	      $success = 1;
	      $_ = $_.'  ;'.$comment."\n";
	    }
	  }
	  unless ( $success ) {
	    debug -> die( message => "Could not determine a good place to add the covariate relation.\n".
			  " i.e. No TV$parameter was found\n" );
	  }
	}
	if ( $use_pred ) {
	  $applicant_model -> pred( problem_number => 1,
				    new_pred       => \@code );
	} else {
	  $applicant_model -> pk( problem_number => 1,
				  new_pk         => \@code );
	}


	$applicant_model -> initial_values( parameter_numbers => [[$start_theta..$end_theta]],
					    new_values        => [\@inits],
					    add_if_absent     => 1,
					    parameter_type    => 'theta',
					    problem_numbers   => [1]);
	$applicant_model -> upper_bounds( parameter_type    => 'theta',
					  parameter_numbers => [[$start_theta..$end_theta]],
					  problem_numbers   => [1],
					  new_values        => [$bounds{'upper'}] );
	$applicant_model -> lower_bounds( parameter_type    => 'theta',
					  parameter_numbers => [[$start_theta..$end_theta]],
					  problem_numbers   => [1],
					  new_values        => [$bounds{'lower'}] );
	$applicant_model -> labels( parameter_type    => 'theta',
				    parameter_numbers => [[$start_theta..$end_theta]],
				    problem_numbers   => [1],
				    new_values        => [\@labels] );
      }
end add_code

# }}} add_code

# {{{ write_log

start write_log
      {
	my %parm = @_;
	my $direction = $parm{'direction'};
	my $logfile = $parm{'logfile'};
	my $included_relations = $parm{'included_relations'};
	my $chosen_parameter = $parm{'chosen_parameter'};
	my $chosen_covariate = $parm{'chosen_covariate'};
	my $results = $parm{'results'};
	my $criterion = $parm{'criterion'};
	my $test_log_ref = $parm{'test_log'};
	my $append  = $parm{'append'};
	my %test_log = %{$test_log_ref} if defined ( $test_log_ref );
	my @names = ();
	my @drops = ();
	my $chosen;
	foreach my $result ( @{$results} ) {
	  @names = @{$result -> {'values'}} if ($result -> {'name'} eq 'combinations');
	  @drops = @{$result -> {'values'}} if ($result -> {'name'} eq 'ofv.drop');
	  $chosen = $result -> {'values'} if ($result -> {'name'} eq 'relation.chosen.in.step');
	}

	open( LOG, ">>$logfile" );
#	for( my $i = 0; $i <= $#names; $i++ ) {
#	  print LOG 'DROP  ',sprintf("%10s",$names[$i]),sprintf("%10.1f",$drops[$i]),"\n";
#	}
	
	if ( defined $chosen ) {
	  print LOG "Parameter-covariate relation chosen in this $direction step: ",
	    "$chosen_parameter-$chosen_covariate\n";
	  print LOG sprintf("%-15s",'CRITERION'),uc( $criterion ),"\n";
	  my @names = sort keys %test_log;
	  foreach my $name ( @names ) {
	    my $val = $test_log{$name};
#	  while ( my ( $name, $val ) = sort {$a <=> $b} each %test_log ) {
	    if ( ref($val) eq 'HASH' ) {
	      foreach my $name2 ( sort keys %{$val} ) {
#	      while ( my ( $name2, $val2 ) = each %{$val} ) {
		print LOG sprintf("%-20s",uc($name)),sprintf("%-30s",uc( $name2) ),
		sprintf("%12.5f",uc( $val -> {$name2} )),"\n";
	      }
	    } else {
	      print LOG sprintf("%-20s",uc($name)),sprintf("%12.5f",uc( $val )),"\n";
	    }
	  }
	}
	if (defined $included_relations and scalar %{$included_relations} > 0 ) {
	  print LOG "Relations included after this step:\n";
	  foreach my $kind ( 'categorical', 'continuous' ) {
	    print LOG sprintf("%-15s",'KIND'),uc( $kind ),"\n";
	    print LOG sprintf("%-15s",'VALID_STATES');
	    foreach my $state ( @{$self -> {'valid_states'}{$kind}} ) {
	      print LOG sprintf("%-4s",$state)
	    }
	    print LOG "\n";
	    print LOG sprintf("%-15s",'COVARIATES');
	    foreach my $cov ( @{$self -> {lc($kind).'_covariates'}} ) {
	      print LOG sprintf("%-8s",$cov)
	    }
	    print LOG "\n";
	    foreach my $par ( sort keys %{$included_relations} ) {
	      print LOG sprintf("%-15s",'INCL'),sprintf("%-8s",$par);
	      foreach my $cov ( sort keys %{$included_relations -> {$par}} ) {
		# Is this covariate continuous or not?
		my $continuous = 1;
		foreach my $cat ( @{$self -> {'categorical_covariates'}} ) {
		  $continuous = 0 if ( $cov eq $cat );
		}
		if ( ( $continuous and $kind eq 'continuous' ) or
		   not $continuous and $kind eq 'categorical' ) {
		  print LOG sprintf("%-8s",$cov.'-'.$included_relations -> {$par}{$cov}{'state'});
		}
	      }
	      print LOG "\n";
	    }
	  }
	}
	print LOG "--------------------\n\n";
	close( LOG );
      }
end write_log

# }}}

# This method is not used at the moment and a method with the same name exists in tool.pm
# Keep the code for now but rename the method if it is to be used again.
# # {{{ read_log

# start read_log
#       {
# 	my @log;
# 	open( LOG, $self -> {'logfile'} );
# 	@log = <LOG>;
# 	close( LOG );
# 	my @step_indexes;
# 	for ( my $i = 0; $i <= $#log; $i++ ) {
# 	  push( @step_indexes, $i ) if ( $log[$i] =~ /--------------------/ );
# 	}
# #	my %incl_relations;
# #	my %valid_states;
# 	my ( $junk, $kind );
# 	for ( my $i = $step_indexes[$#step_indexes-1]; $i <= $step_indexes[$#step_indexes]; $i++ ) {
# 	  if ( $log[$i] =~ /^KIND/ ) {
# 	    ( $junk, $kind ) = split( ' ', $log[$i] );
# 	  }
# 	  if ( $log[$i] =~ /^VALID_STATES/ ) {
# 	    my @valid_states;
# 	    @valid_states = split( ' ', $log[$i] );
# 	    shift @valid_states;
# 	    $valid_states{lc($kind)} = \@valid_states;
# 	  }
# 	  if ( $log[$i] =~ /^INCL/ ) {
# 	    my @row = split( ' ', $log[$i] );
# 	    my $par = $row[1];
# 	    my @cov_states = @row[2..$#row];
# 	    foreach my $covstate ( @cov_states ) {
# 	      my ( $cov, $state ) = split('-',$covstate);
# 	      if ( not defined $state or $state eq '' ) {
# 		$state = $valid_states{$kind}[0];
# 	      }
# 	      $incl_relations{$par}{$cov}{'state'} = $state;
# 	    }
# 	  }
# 	}
# 	print;
#       }
# end read_log

# # }}}

# {{{ drop_undrop_covariates

start drop_undrop_covariates
      {
OUTER:	foreach my $cov ( @all_covariates ) {
	  my $used = 0;
	  foreach my $do_not_cov ( @do_not_drop ) {
	    next OUTER if ( $cov eq $do_not_cov );
	  }
	  foreach my $used_cov ( @used_covariates ) {
	    $used = 1 if ( $cov eq $used_cov );
	  }
	  if ( $used ) {
	    debug -> warn( level   => 2,
			   message => "undropping $cov" );
	    $applicant_model -> _option_val_pos ( problem_numbers  => [1],
						  instance_numbers => [[1]],
						  name             => $cov,
						  record_name      => 'input',
						  new_values       => [['']],
						  exact_match      => 1 );
	  } else {
	    debug -> warn( level   => 2,
			   message => "dropping $cov" );
	    $applicant_model -> _option_val_pos ( problem_numbers  => [1],
						  instance_numbers => [[1]],
						  name             => $cov,
						  record_name      => 'input',
						  new_values       => [['DROP']],
						  exact_match      => 1 );
	  }
	}
      }
end drop_undrop_covariates

# }}}
