# {{{ include statements

start include statements
use Cwd;
use File::Copy 'cp';
use tool::llp;
use tool::modelfit;
use Math::Random;
use ext::Math::MatrixReal;
use Data::Dumper;
if ( $PsN::config -> {'_'} -> {'use_database'} ) {
# Testing DBD::mysql:
  require DBI;
}
end include statements

# }}} include statements

# {{{ new

start new
      {
	foreach my $attribute ( 'logfile', 'raw_results_file' ) {
	  if ( not( ref($this -> {$attribute}) eq 'ARRAY' or
		    ref($this -> {$attribute}) eq 'HASH' ) ) {
	    my $tmp = $this -> {$attribute};
	    if ( not defined $tmp and $attribute eq 'logfile' ) {
	      $tmp = 'cdd.log';
	    }
	    $this -> {$attribute} = [];
	    for ( my $i = 1; $i <= scalar @{$this -> {'models'}}; $i++ ) {
	      my $name = $tmp;
	      if ( $name =~ /\./ ) {
		$name =~ s/\./$i\./;
	      } else {
		$name = $name.$i;
	      }
	      my $ldir;
	      ( $ldir, $name ) =
		OSspecific::absolute_path( $this -> {'directory'}, $name );
	      push ( @{$this -> {$attribute}}, $ldir.$name ) ;
	    }
	  }
	}
	if ( $PsN::config -> {'_'} -> {'use_database'} ) {
	  my( $found_log, $found_cdd_id ) = $this -> read_cdd_log;
	
	  $this -> register_cdd_in_database unless ( $found_cdd_id );
	  
	  $this -> log_object unless ( $found_log and $found_cdd_id );
	  print "Found ",$this -> {'cdd_id'},"\n";
	}
      }
end new

# }}} new

# {{{ register_cdd_in_database

start register_cdd_in_database
  {
    if ( $PsN::config -> {'_'} -> {'use_database'} ) {
      my $dbh = DBI -> connect("DBI:mysql:host=".$PsN::config -> {'_'} -> {'database_server'}.
			       ";databse=".$PsN::config -> {'_'} -> {'project'},
			       $PsN::config -> {'_'} -> {'user'},
			        $PsN::config -> {'_'} -> {'password'},
			       {'RaiseError' => 1});
      my $sth;
      # bins and case_column can be defined for more than one model. Skip
      # registration of these for now.
#      $sth = $dbh -> prepare("INSERT INTO ".$PsN::config -> {'_'} -> {'project'}.
#			       ".cdd ( tool_id, bins, case_column ) ".
#			     "VALUES (".$self -> {'tool_id'}.", '".$self -> {'bins'}.
#			     "', '".$self -> {'case_column'}."' )");
      $sth = $dbh -> prepare("INSERT INTO ".$PsN::config -> {'_'} -> {'project'}.
			       ".cdd ( tool_id ) ".
			     "VALUES (".$self -> {'tool_id'}." )");
      $sth -> execute;
      $self -> {'cdd_id'} = $sth->{'mysql_insertid'};
      $sth -> finish;
      $dbh -> disconnect;
    }
  }
end register_cdd_in_database

# }}} register_cdd_in_database

# {{{ register_mfit_results

start register_mfit_results
  {
    if ( $PsN::config -> {'_'} -> {'use_database'} ) {
      my $dbh = DBI -> connect("DBI:mysql:host=".$PsN::config -> {'_'} -> {'database_server'}.
			       ";databse=".$PsN::config -> {'_'} -> {'project'},
			       $PsN::config -> {'_'} -> {'user'},
			       $PsN::config -> {'_'} -> {'password'},
			       {'RaiseError' => 1});
      $dbh -> do( "LOCK TABLES ".$PsN::config -> {'_'} -> {'project'}.
			       ".cdd_modelfit_results WRITE" );
      my $sth = $dbh -> prepare( "SELECT MAX(cdd_modelfit_results_id)".
				 " FROM ".$PsN::config -> {'_'} -> {'project'}.
			       ".cdd_modelfit_results" );
      $sth -> execute or debug -> die( message => $sth->errstr ) ;
      my $select_arr = $sth -> fetchall_arrayref;
      $first_res_id = defined $select_arr -> [0][0] ? ($select_arr -> [0][0] + 1) : 0;
      $last_res_id = $first_res_id + $#cook_score;
      $sth -> finish;

      my $insert_values;
      for( my $i = 0; $i <= $#cook_score; $i++ ) {
	$insert_values = $insert_values."," if ( defined $insert_values );
	$insert_values = $insert_values.
	    "('".$self -> {'cdd_id'}."', '".$self -> {'model_ids'}[$model_number-1].
	    "', '".$self -> {'prepared_model_ids'}[($model_number-1)*($#cook_score+1)+$i].
	    "', '".($i+1).
	    "','$cook_score[$i]', '$covariance_ratio[$i]', '$projections[$i][0]', '$projections[$i][01]', '$outside_n_sd[$i]' )";
      }
      $dbh -> do("INSERT INTO ".$PsN::config -> {'_'} -> {'project'}.
		 ".cdd_modelfit_results ".
		 "( cdd_id, orig_model_id, model_id, ".
		 "bin, cook_score, covariance_ratio, ".
		 "pca_component_1, pca_component_2, outside_n_sd ) ".
		 "VALUES $insert_values");
      $dbh -> do( "UNLOCK TABLES" );
      $dbh -> disconnect;
    }
  }
end register_mfit_results

# }}} register_mfit_results

# {{{ read_cdd_log
start read_cdd_log
{
  if( -e $self -> {'directory'}.'object.txt' ) {
    $found_log = 1;
    open( OLOG, '<'.$self -> {'directory'}.'object.txt' );
    my @olog = <OLOG>;
    my $str = "(";
    for ( my $i = 1; $i < $#olog; $i++ ) {
      $str = $str.$olog[$i];
    }
    $str = $str.")";
    my %tmp = eval( $str );
    
    if( exists $tmp{'cdd_id'} ) {
      $self -> {'cdd_id'} = $tmp{'cdd_id'};
      $found_cdd_id = 1;
    }
    close( OLOG );
  }
}
end read_cdd_log
# }}} read_cdd_log

# {{{ llp_pre_fork_setup

start llp_pre_fork_setup
      {
	$self -> modelfit_pre_fork_setup;
      }
end llp_pre_fork_setup

# }}} llp_pre_fork_setup

# {{{ modelfit_pre_fork_setup

start modelfit_pre_fork_setup
      {
	# These attributes can be given as a
	# 1. A scalar       : used for all models and problems
	# 2. A 1-dim. array : specified per problem but same for all models
	# 3. A 2-dim. array : specified per problem and model
	my $bins = $self -> {'bins'};
#	my $idxs = $self -> {'grouping_indexes'};
	my $case_columns = $self -> {'case_columns'};

	if ( defined $bins ) {
	  unless ( ref( \$bins ) eq 'SCALAR' or
		   ( ref( $bins ) eq 'ARRAY' and scalar @{$bins} > 0 ) ) {
	    debug -> die( message => "Attribute bins is ",
			  "defined as a ",ref( $bins ),
			  "and is neither a scalar or a non-zero size array." );
	  } elsif ( ref( \$bins ) eq 'SCALAR' ) {
	    my @mo_bins = ();
	    foreach my $model ( @{$self -> {'models'}} ) {
	      my @pr_bins = ();
	      foreach my $problem ( @{$model -> problems}  ) {
		push( @pr_bins, $bins );
	      }
	      push( @mo_bins, \@pr_bins );
	    } 
	    $self -> {'bins'} = \@mo_bins;
	  } elsif ( ref( $bins ) eq 'ARRAY' ) {
	    unless ( ref( \$bins -> [0] ) eq 'SCALAR' or
		     ( ref( $bins -> [0] ) eq 'ARRAY' and scalar @{$bins -> [0]} > 0 ) ) {
	      debug -> die( message => "Attribute bins is ",
			    "defined as a ",ref( $bins -> [0] ),
			    "and is neither a scalar or a non-zero size array." );
	    } elsif ( ref(\$bins -> [0]) eq 'SCALAR' ) {
	      my @mo_bins = ();
	      foreach my $model ( @{$self -> {'models'}} ) {
		push( @mo_bins, $bins );
	      }
	      $self -> {'bins'} = \@mo_bins;
	    }
	  }
	} else {
	  my @mo_bins = ();
	  foreach my $model ( @{$self -> {'models'}} ) {
	    my @pr_bins = ();
	    foreach my $data ( @{$model -> datas}  ) {
	      push( @pr_bins, $data -> count_ind );
	    }
	    push( @mo_bins, \@pr_bins );
	  }
	  $self -> {'bins'} = \@mo_bins;
	}

	if ( defined $case_columns ) {
	  if ( defined $case_columns ) {
	    unless ( ref( \$case_columns ) eq 'SCALAR' or
		     ( ref( $case_columns ) eq 'ARRAY' and scalar @{$case_columns} > 0 ) ) {
	      debug -> die( message => "Attribute case_columns is ",
			    "defined as a ",ref( $case_columns ),
			    "and is neither a scalar or a non-zero size array." );
	    } elsif ( ref( \$case_columns ) eq 'SCALAR' ) {
	      # SCALAR!
	      my @mo_case_columns = ();
	      foreach my $model ( @{$self -> {'models'}} ) {
		my @pr_case_columns = ();
		for( my $i = 1; $i <= scalar @{$model -> problems}; $i++  ) {
		  if ( not $case_columns =~ /^\d/ ) {
		    # STRING
		    my ( $junk, $column_position ) = $model ->
		      _get_option_val_pos( name            => $case_columns,
					   record_name     => 'input',
					   problem_numbers => [$i] );
		    # We assume that there is no duplicate column names
		    push ( @pr_case_columns, $column_position->[0][0] );
		  } else {
		    # NUMBER
		    push ( @pr_case_columns, $case_columns );
		  }
		}
		push( @mo_case_columns, \@pr_case_columns );
	      }
	      $self -> {'case_columns'} = \@mo_case_columns;
	    } elsif ( ref( $case_columns ) eq 'ARRAY' ) {
	      # ARRAY!
	      unless ( ref( \$case_columns -> [0] ) eq 'SCALAR' or
		       ( ref( $case_columns -> [0] ) eq 'ARRAY' and
			 scalar @{$case_columns -> [0]} > 0 ) ) {
		debug -> die( message => "Second dimension of attribute case_columns is ",
			      "defined as a ",ref( $case_columns -> [0]),
			      "and is neither a scalar or a non-zero size array." );
	      } elsif ( ref(\$case_columns -> [0]) eq 'SCALAR' ) {
	      # ARRAY -> SCALAR!
		my @mo_case_columns = ();
		foreach my $model ( @{$self -> {'models'}} ) {
		  my @pr_case_columns = ();
		  for( my $i = 1; $i <= scalar @{$model -> problems}; $i++  ) {
		    if ( not $case_columns =~ /^\d/ ) {
		      # STRING
		      my ( $junk, $column_position ) = $model ->
			_get_option_val_pos( name            => $case_columns->[$i-1],
					     record_name     => 'input',
					     problem_numbers => [$i] );
		      # We assume that there is no duplicate column names
		      push ( @pr_case_columns, $column_position->[0][0] );
		    } else {
		      # NUMBER
		      push ( @pr_case_columns, $case_columns -> [$i-1] );
		    }
		  }
		  push( @mo_case_columns, \@pr_case_columns );
		}
		$self -> {'case_columns'} = \@mo_case_columns;
	      } elsif ( ref($case_columns -> [0]) eq 'ARRAY' ) {
		# ARRAY -> ARRAY!
		my @mo_case_columns = ();
		for( my $j = 0; $j < scalar @{$self -> {'models'}}; $j++ ) {
		  my @pr_case_columns = ();
		  for( my $i = 1; $i <= scalar @{$self -> {'models'} -> problems}; $i++  ) {
		    if ( not $case_columns =~ /^\d/ ) {
		      # STRING
		      my ( $junk, $column_position ) = $self -> {'models'} -> [$j] ->
			_get_option_val_pos( name            => $case_columns->[$j]->[$i-1],
					     record_name     => 'input',
					     problem_numbers => [$i] );
		      # We assume that there is no duplicate column names
		      push ( @pr_case_columns, $column_position->[0][0] );
		    } else {
		      # NUMBER
		      push ( @pr_case_columns, $case_columns -> [$j]->[$i-1] );
		    }
		  }
		  push( @mo_case_columns, \@pr_case_columns );
		}
		$self -> {'case_columns'} = \@mo_case_columns;
	      }
	    }
	  } else {
	    debug -> die( message => "case_columns is not specified for model $model_number" );
	  }
	}
      }
end modelfit_pre_fork_setup

# }}} modelfit_pre_fork_setup

# {{{ modelfit_setup

start modelfit_setup
      {
	my $subm_threads = ref( $self -> {'threads'} ) eq 'ARRAY' ? 
	  $self -> {'threads'} -> [1]:$self -> {'threads'};
	$self -> general_setup( model_number => $model_number,
				class        => 'tool::modelfit',
			        subm_threads => $subm_threads );
      }
end modelfit_setup

# }}} modelfit_setup

# {{{ llp_setup
start llp_setup
      {
	my @subm_threads;
	if (ref( $self -> {'threads'} ) eq 'ARRAY') {
	  @subm_threads = @{$self -> {'threads'}};
	  unshift(@subm_threads);
	} else {
	  @subm_threads = ($self -> {'threads'});
	}
	$self -> general_setup( model_number => $model_number,
				class        => 'tool::llp',
			        subm_threads => \@subm_threads );
      }
end llp_setup
# }}} llp_setup

# {{{ general_setup

start general_setup
      {
	# Sub tool threads can be given as scalar or reference to an array?
	my $subm_threads = $parm{'subm_threads'};
	my $own_threads = ref( $self -> {'threads'} ) eq 'ARRAY' ? 
	    $self -> {'threads'} -> [0]:$self -> {'threads'};
	# case_column names are matched in the model, not the data!

	my $model = $self   -> {'models'} -> [$model_number-1];
	my @bins  = @{$self -> {'bins'}   -> [$model_number-1]};

	# Check which models that hasn't been run and run them
	# This will be performed each step but will only result in running
	# models at the first step, if at all.

	# If more than one process is used, there is a VERY high risk of interaction
	# between the processes when creating directories for model fits. Therefore
	# the {'directory'} attribute is given explicitly below.


	unless ( $model -> is_run ) {

	  # -----------------------  Run original run  ------------------------------

	  # {{{ orig run

	  my %subargs = ();
	  if ( defined $self -> {'subtool_arguments'} ) {
	    %subargs = %{$self -> {'subtool_arguments'}};
	  }
	  if( $self -> {'nonparametric_etas'} or
	      $self -> {'nonparametric_marginals'} ) {
	    $model -> add_nonparametric_code;
	  }
	  
	  my $orig_fit = tool::modelfit ->
	      new( reference_object      => $self,
		   models                => [$model],
		   threads               => $subm_threads,
		   directory             => $self -> {'directory'}.'/orig_modelfit_dir'.$model_number,
		   subtools              => [],
		   parent_threads        => $own_threads,
		   parent_tool_id        => $self -> {'tool_id'},
		   logfile	         => undef,
		   raw_results           => undef,
		   prepared_models       => undef,
		   top_tool              => 0,
		   %subargs );

#        	$Data::Dumper::Maxdepth=1;
#	  die Dumper $orig_fit;
# 	    ( models         => [$model],

# 	      run_on_lsf       => $self -> {'run_on_lsf'},
# 	      lsf_queue        => $self -> {'lsf_queue'},
# 	      lsf_options      => $self -> {'lsf_options'},
# 	      lsf_job_name     => $self -> {'lsf_job_name'},
# 	      lsf_project_name => $self -> {'lsf_project_name'},

# 	      run_on_nordugrid => $self -> {'run_on_nordugrid'},
# 	      cpu_time         => $self -> {'cpu_time'},

# 	      parent_tool_id   => $self -> {'tool_id'},

# 	      subtools     => [],
# 	      nm_version     => $self -> {'nm_version'},
# 	      picky          => $self -> {'picky'},
# 	      compress       => $self -> {'compress'},
# 	      threads        => $subm_threads,
# 	      retries        => $self -> {'retries'},
# 	      base_directory => $self -> {'directory'},
# 	      directory      => $self -> {'directory'}.'/orig_modelfit_dir'.$model_number,
# 	      parent_threads => $own_threads );

	  ui -> print( category => 'cdd',
		       message => 'Executing base model.' );


	  $orig_fit -> run;

	  # }}} orig run

	}
	
	# ------------------------  Print a log-header  -----------------------------

	# {{{ log header

	open( LOG, ">>".$self -> {'logfile'}[$model_number-1] );
	my $ui_text = sprintf("%-5s",'RUN').','.sprintf("%20s",'FILENAME  ').',';
	print LOG sprintf("%-5s",'RUN'),',',sprintf("%20s",'FILENAME  '),',';
	foreach my $param ( 'ofv', 'theta', 'omega', 'sigma' ) {
	  my $accessor    = $param eq 'ofv' ? $param : $param.'s';
	  my $orig_ests   = $model -> outputs -> [0] -> $accessor;
	  # Loop the problems
	  if( defined $orig_ests ){
	    for ( my $j = 0; $j < scalar @{$orig_ests}; $j++ ) {
	      if ( $param eq 'ofv' ) {
		my $label = uc($param)."_".($j+1);
		$ui_text = $ui_text.sprintf("%12s",$label).',';
		print LOG sprintf("%12s",$label),',';
	      } else {
		# Loop the parameter numbers (skip sub problem level)
		if( defined $orig_ests -> [$j] and 
		    defined $orig_ests -> [$j][0] ){
		  for ( my $num = 1; $num <= scalar @{$orig_ests -> [$j][0]}; $num++ ) {
		    my $label = uc($param).$num."_".($j+1);
		    $ui_text = $ui_text.sprintf("%12s",$label).',';
		    print LOG sprintf("%12s",$label),',';
		  }
		}
	      }
	    }
	  }
	}

	print LOG "\n";

	# }}} log header

	# ------------------------  Log original run  -------------------------------

	# {{{ log orig

	open( LOG, ">>".$self -> {'logfile'}[$model_number-1] );
	$ui_text = sprintf("%5s",'0').','.sprintf("%20s",$model -> filename).',';
	print LOG sprintf("%5s",'0'),',',sprintf("%20s",$model -> filename),',';
	foreach my $param ( 'ofv', 'theta', 'omega', 'sigma' ) {
	  my $accessor    = $param eq 'ofv' ? $param : $param.'s';
	  my $orig_ests   = $model -> outputs -> [0] -> $accessor;
	  # Loop the problems
	  if( defined $orig_ests ) {
	    for ( my $j = 0; $j < scalar @{$orig_ests}; $j++ ) {
	      if ( $param eq 'ofv' ) {
		$ui_text = $ui_text.sprintf("%12f",$orig_ests -> [$j][0]).',';
		print LOG sprintf("%12f",$orig_ests -> [$j][0]),',';
	      } else {
		# Loop the parameter numbers (skip sub problem level)
		if( defined $orig_ests -> [$j] and 
		    defined $orig_ests -> [$j][0] ){
		  for ( my $num = 0; $num < scalar @{$orig_ests -> [$j][0]}; $num++ ) {
		    $ui_text = $ui_text.sprintf("%12f",$orig_ests -> [$j][0][$num]).',';
		    print LOG sprintf("%12f",$orig_ests -> [$j][0][$num]),',';
		  }
		}
	      }
	    }
	  }
	}

	print LOG "\n";

	# }}} log orig

	# ---------------------  Initiate some variables ----------------------------

	# {{{ inits

	my $case_column = $self -> {'case_columns'} -> [$model_number-1] -> [0];

	# Case-deletion Diagnostics will only work for models with one problem.
	my $orig_data = $model -> datas -> [0];

	if ( not defined $orig_data ) {
	  debug -> die( message => "No data file to resample from found in ".$model -> full_name );
	}

	my @problems   = @{$model -> problems};
	my @new_models = ();

	my ( @skipped_ids, @skipped_keys, @skipped_values );

	my %orig_factors = %{$orig_data -> factors( column => $case_column )};
	my $maxbins      = scalar keys %orig_factors;
	my $pr_bins      = ( defined $bins[0] and $bins[0] <= $maxbins ) ?
	  $bins[0] : $maxbins;

	my $done = ( -e $self -> {'directory'}."/m$model_number/done" ) ? 1 : 0;

	my ( @seed, $new_datas, $skip_ids, $skip_keys, $skip_values, $remainders );

	# }}} inits

	if ( not $done ) {

	  # --------------  Create new case-deleted data sets  ----------------------

	  # {{{ create new

	  # Keep the new sample data objects i memory and write them to disk later
	  # with a proper name.

	  ( $new_datas, $skip_ids, $skip_keys, $skip_values, $remainders )
	    = $orig_data -> case_deletion( case_column => $case_column,
					   selection   => $self -> {'selection_method'},
					   bins        => $pr_bins,
					   target      => 'mem',
					   directory   => $self -> {'directory'}.'/m'.$model_number );
	  my $ndatas = scalar @{$new_datas};
	  open( DB, ">".$self -> {'directory'}."m$model_number/done.database.models" );
	  my @model_ids;
	  for ( my $j = 1; $j <= $ndatas; $j++ ) {
	    my @names = ( 'cdd_'.$j, 'rem_'.$j );
	    my @datasets = ( $new_datas -> [$j-1], $remainders -> [$j-1] );
	    foreach my $i ( 0, 1 ) {
	      my $dataset = $datasets[$i];
	      my ($data_dir, $data_file) = OSspecific::absolute_path( $self -> {'directory'}.'/m'.$model_number,
								      $names[$i].'.dta' );
#	      $dataset -> directory( $data_dir );
#	      $dataset -> filename( $data_file );
#	      $dataset -> flush;
	      my $newmodel = $model -> copy( filename => $data_dir.$names[$i].'.mod',
					     copy_data   => 0,
					     copy_output => 0);
	      $newmodel -> ignore_missing_files(1);
	      $newmodel -> datafiles( new_names => [$names[$i].'.dta'] );
	      $newmodel -> outputfile( $data_dir.$names[$i].".lst" );
	      $newmodel -> datas -> [0] = $dataset;
	      if( $i == 1 ) {
		# set MAXEVAL=0. Again, CDD will only work for one $PROBLEM
		$newmodel -> maxeval( new_values => [[0]] );
	      }

	      if( $self -> {'nonparametric_etas'} or
		  $self -> {'nonparametric_marginals'} ) {
		$newmodel -> add_nonparametric_code;
	      }
	  
	      $newmodel -> _write;
	      push( @{$new_models[$i]}, $newmodel );
	      $model_ids[$j*$i+$j-1] = $newmodel -> register_in_database( force => 1 ) ;
	      print DB $model_ids[$j*$i+$j-1],"\n";
	      $self -> {'prepared_model_ids'}[($model_number-1)*$ndatas*2+$j*$i+$j-1] =
		  $model_ids[$j*$i+$j-1];
	    }
	  }

# 	    my $new_data = $new_datas -> [$j-1];
# 	    my ($data_dir, $data_file) = OSspecific::absolute_path( $self -> {'directory'}.'/m'.$model_number,
# 								      'cdd_'.$j.'.dta' );
# 	    $new_data -> directory( $data_dir );
# 	    $new_data -> filename( $data_file );
# 	    $new_data -> flush;
# 	    my $newmodel = $model -> copy( filename => $data_dir."cdd_$j.mod",
# 					   copy_data   => 0,
# 					   copy_output => 0);
# 	    $newmodel -> ignore_missing_files(1);
# 	    $newmodel -> datafiles( new_names => ["cdd_$j.dta"] );
# 	    $newmodel -> outputfile( $data_dir."cdd_$j.lst" );
# 	    $newmodel -> datas -> [0] = $new_data;
# 	    $newmodel -> _write;
# 	    push( @new_models, $newmodel );
# 	    $model_ids[$j-1]  = $newmodel -> register_in_database( force => 1 );
# 	    print DB $model_ids[$j-1],"\n";
# 	    $self -> {'prepared_model_ids'}[($model_number-1)*$ndatas+$j-1] =
# 		$model_ids[$j-1];
# 	  }
	  close( DB );
	  if ( not -e $self -> {'directory'}."m$model_number/done.database.tool_models" ) {
	    $self -> register_tm_relation( model_ids       => \@model_ids,
					   prepared_models => 1 );
	    open( DB, ">".$self -> {'directory'}."m$model_number/done.database.tool_models" );
	    print DB "";
	    close( DB );
	  }
	  # Create a checkpoint. Log the samples and individuals.
	  open( DONE, ">".$self -> {'directory'}."/m$model_number/done" ) ;
	  print DONE "Sampling from ",$orig_data -> filename, " performed\n";
	  print DONE "$pr_bins bins\n";
	  print DONE "Skipped individuals:\n";
	  for( my $k = 0; $k < scalar @{$skip_ids}; $k++ ) {
	    print DONE join(',',@{$skip_ids -> [$k]}),"\n";
	  }
	  print DONE "Skipped keys:\n";
	  for( my $k = 0; $k < scalar @{$skip_keys}; $k++ ) {
	    print DONE join(',',@{$skip_keys -> [$k]}),"\n";
	  }
	  print DONE "Skipped values:\n";
	  for( my $k = 0; $k < scalar @{$skip_values}; $k++ ) {
	    print DONE join(',',@{$skip_values -> [$k]}),"\n";
	  }
	  @seed = random_get_seed;
	  print DONE "seed: @seed\n";
	  close( DONE );

	  open( SKIP, ">".$self -> {'directory'}."skipped_individuals".$model_number.".csv" ) ;
	  for( my $k = 0; $k < scalar @{$skip_ids}; $k++ ) {
	    print SKIP join(',',@{$skip_ids -> [$k]}),"\n";
	  }
	  close( SKIP );
	  open( SKIP, ">".$self -> {'directory'}."skipped_keys".$model_number.".csv" ) ;
	  for( my $k = 0; $k < scalar @{$skip_keys}; $k++ ) {
	    print SKIP join(',',@{$skip_keys -> [$k]}),"\n";
	  }
	  close( SKIP );

	  # }}} create new

	} else {

	  # ---------  Recreate the datasets and models from a checkpoint  ----------

	  # {{{ resume

	  ui -> print( category => 'cdd',
		       message  => "Recreating models from a previous run" );
	  open( DONE, $self -> {'directory'}."/m$model_number/done" );
	  my @rows = <DONE>;
	  close( DONE );
	  my ( $junk, $junk, $stored_filename, $junk ) = split(' ',$rows[0],4);
	  my ( $stored_bins, $junk ) = split(' ',$rows[1],2);
	  my ( @stored_ids, @stored_keys, @stored_values );
	  for ( my $k = 3; $k < 3+$stored_bins; $k++ ) {
	    chomp($rows[$k]);
	    my @bin_ids = split(',', $rows[$k] );
	    push( @stored_ids, \@bin_ids );
	  }
	  for ( my $k = 4+$stored_bins; $k < 4+2*$stored_bins; $k++ ) {
	    chomp($rows[$k]);
	    my @bin_keys = split(',', $rows[$k] );
	    push( @stored_keys, \@bin_keys );
	  }
	  for ( my $k = 5+2*$stored_bins; $k < 5+3*$stored_bins; $k++ ) {
	    chomp($rows[$k]);
	    my @bin_values = split(',', $rows[$k] );
	    push( @stored_values, \@bin_values );
	  }
	  @seed = split(' ',$rows[5+3*$stored_bins]);
	  $skip_ids    = \@stored_ids;
	  $skip_keys   = \@stored_keys;
	  $skip_values = \@stored_values;
	  shift( @seed ); # get rid of 'seed'-word

	  # Reinitiate the model objects
	  my @model_ids;
	  my $reg_relations = 0;
	  if ( -e $self -> {'directory'}."m$model_number/done.database.models" ) {
	    open( DB, $self -> {'directory'}."m$model_number/done.database.models" );
	    @model_ids = <DB>;
	    chomp( @model_ids );
	  } else {
	    open( DB, ">".$self -> {'directory'}."m$model_number/done.database.models" );
	    $reg_relations = 1;
	  }
	  for ( my $j = 1; $j <= $stored_bins; $j++ ) {
	    my @names = ( 'cdd_'.$j, 'rem_'.$j );
	    foreach my $i ( 0, 1 ) {
	      my ($model_dir, $filename) = OSspecific::absolute_path( $self -> {'directory'}.'/m'.
								      $model_number,
								      $names[$i].'.mod' );
	      my ($out_dir, $outfilename) = OSspecific::absolute_path( $self -> {'directory'}.'/m'.
								       $model_number,
								       $names[$i].'.lst' );
	      my $new_mod = model ->
		  new( directory   => $model_dir,
		       filename    => $filename,
		       outputfile  => $outfilename,
		       extra_files => $model -> extra_files,
		       target      => 'disk',
		       ignore_missing_files => 1,
		       quick_reload => 1,
		       model_id     => $model_ids[$j*$i+$j-1] );
	      push( @{$new_models[$i]}, $new_mod );
	      my $model_id;
	      if( not defined $model_ids[$j*$i+$j-1] ) {
		$model_ids[$j*$i+$j-1] = $new_mod -> register_in_database;
		print DB $model_ids[$j-1],"\n";
	      }
	      $self -> {'prepared_model_ids'}[($model_number-1)*
					      $stored_bins+$j*$i+$j-1] =
					      $model_ids[$j*$i+$j-1];
	    }

# 	    my ($model_dir, $filename) = OSspecific::absolute_path( $self -> {'directory'}.'/m'.
# 								    $model_number,
# 								    'cdd_'.$j.'.mod' );
# 	    my ($out_dir, $outfilename) = OSspecific::absolute_path( $self -> {'directory'}.'/m'.
# 								     $model_number,
# 								     '/cdd_'.$j.'.lst' );
# 	    my $new_mod = model ->
# 	      new( directory   => $model_dir,
# 		   filename    => $filename,
# 		   outputfile  => $outfilename,
# 		   extra_files => $model -> extra_files,
# 		   target      => 'disk',
# 		   ignore_missing_files => 1,
# 		   quick_reload => 1,
# 		   model_id     => $model_ids[$j-1] );
# 	    push( @new_models, $new_mod );
# 	    my $model_id;
# 	    if( not defined $model_ids[$j-1] ) {
# 	      $model_ids[$j-1] = $new_mod -> register_in_database;
# 	      print DB $model_ids[$j-1],"\n";
# 	    }
# 	    $self -> {'prepared_model_ids'}[($model_number-1)*$stored_bins+$j-1] =
#		$model_ids[$j-1];
	    my $nl = $j == $stored_bins ? "" : "\r"; 
	    ui -> print( category => 'cdd',
			 message  => ui -> status_bar( sofar => $j+1,
						       goal  => $stored_bins+1 ).$nl,
			 wrap     => 0,
			 newline  => 0 );
	  }
	  close( DB );
	  if ( not -e $self -> {'directory'}."m$model_number/done.database.tool_models" ) {
	    $self -> register_tm_relation( model_ids       => \@model_ids,
					   prepared_models => 1 ) if ( $reg_relations );
	    open( DB, ">".$self -> {'directory'}."m$model_number/done.database.tool_models" );
	    print DB "";
	    close( DB );
	  }
	  ui -> print( category => 'cdd',
		       message  => " ... done." );
	  random_set_seed( @seed );
	  ui -> print( category => 'cdd',
		       message  => "Using $stored_bins previously sampled case-deletion sets ".
		       "from $stored_filename" )
	    unless $self -> {'parent_threads'} > 1;

	  # }}} resume

	}
	push( @skipped_ids,    $skip_ids );
	push( @skipped_keys,   $skip_keys );
	push( @skipped_values, $skip_values );

	# Use only the first half (the case-deleted) of the data sets.
	$self -> {'prepared_models'}[$model_number-1]{'own'} = $new_models[0];

	# The remainders are left until the analyze step
	$self -> {'prediction_models'}[$model_number-1]{'own'} = $new_models[1];

	# ---------------------  Create the sub tools  ------------------------------

	# {{{ sub tools

	my $subdir = $class;
	$subdir =~ s/tool:://;
	my @subtools = @{$self -> {'subtools'}};
	shift( @subtools );
	my %subargs = ();
	if ( defined $self -> {'subtool_arguments'} ) {
	  %subargs = %{$self -> {'subtool_arguments'}};
	}
	push( @{$self -> {'tools'}},
	      $class ->
 	      new( reference_object      => $self,
		   models                => $new_models[0],
		   threads               => $subm_threads,
		   directory             => $self -> {'directory'}.'/'.$subdir.'_dir'.$model_number,
		   _raw_results_callback => $self ->
		          _modelfit_raw_results_callback( model_number => $model_number ),
		   subtools              => \@subtools,
		   parent_threads        => $own_threads,
		   parent_tool_id        => $self -> {'tool_id'},
		   logfile	         => undef,
		   raw_results           => undef,
		   prepared_models       => undef,
		   top_tool              => 0,
		   %subargs ) );


#	      ( models         => $new_models[0],
#		nm_version     => $self -> {'nm_version'},
# 		run_on_lsf       => $self -> {'run_on_lsf'},
# 		lsf_queue        => $self -> {'lsf_queue'},
# 		lsf_options      => $self -> {'lsf_options'},
# 		lsf_job_name     => $self -> {'lsf_job_name'},
# 		lsf_project_name => $self -> {'lsf_project_name'},
		
# 		run_on_nordugrid => $self -> {'run_on_nordugrid'},
# 		cpu_time         => $self -> {'cpu_time'},
		
# 		parent_tool_id   => $self -> {'tool_id'},

# 		picky          => $self -> {'picky'},
# 		compress       => $self -> {'compress'},
#		threads        => $subm_threads,
# 		retries        => $self -> {'retries'},
# 		base_directory => $self -> {'directory'},
#  		directory      => $self -> {'directory'}.'/'.$subdir.'_dir'.$model_number,
# 		_raw_results_callback => $self ->
# 		_modelfit_raw_results_callback( model_number => $model_number ),
# 		subtools       => \@subtools,
# 		parent_threads => $own_threads,
# 		%subargs ) );

	# }}} sub tools

	open( SKIP, ">".$self -> {'directory'}."skipped_values".$model_number.".csv" ) ;
	for( my $k = 0; $k < scalar @{$skip_values}; $k++ ) {
	  print SKIP join(',',@{$skip_values -> [$k]}),"\n";
	}
	close( SKIP );

      }
end general_setup

# }}} general_setup

# {{{ llp_analyze

start llp_analyze
      {
	print "POSTFORK\n";
	my %proc_results;
#	$proc_results{'skipped.section.identifiers'} = $self -> {'skipped.section.identifiers'};
#	$proc_results{'skipped_ids'}      = $self -> {'skipped_ids'};
#	$proc_results{'skipped_keys'}     = $self -> {'skipped_keys'};
#	$proc_results{'skipped_values'}   = $self -> {'skipped_values'};
	
	push( @{$self -> {'results'} -> {'own'}}, \%proc_results );
      }
end llp_analyze

# }}} llp_analyze

# {{{ _modelfit_raw_results_callback

start _modelfit_raw_results_callback
      {

	# Use the cdd's raw_results file.  
	# The cdd and the bootstrap's callback methods are identical
	# in the beginning, then the cdd callback adds cook.scores and
	# cov.ratios.

	my ($dir,$file) = 
	  OSspecific::absolute_path( $self -> {'directory'},
				     $self -> {'raw_results_file'}[$model_number-1] );
	my $orig_mod = $self -> {'models'}[$model_number-1];
	my $xv = $self -> {'cross_validate'};
	$subroutine = sub {
	  my $modelfit = shift;
	  my $mh_ref   = shift;
	  my %max_hash = %{$mh_ref};
	  $modelfit -> raw_results_file( $dir.$file );
	  if( $cross_validation_set ) {
	    $modelfit -> raw_results_append( 1 ) if( not $self -> {'bca_mode'} ); # overwrite when doing a jackknife
	    my ( @new_header, %param_names );
	    foreach my $row ( @{$modelfit -> {'raw_results'}} ) {
	      unshift( @{$row}, 'cross_validation' );
	    }
	    $modelfit -> {'raw_results_header'} = undef; # May be a bit silly to do...
	  } else {

	    my %dummy;

	    my ($raw_results_row,$nonp_rows) = $self -> create_raw_results_rows( max_hash => $mh_ref,
										 model => $orig_mod,
										 raw_line_structure => \%dummy );

	    $orig_mod -> outputs -> [0] -> flush;

	    unshift( @{$modelfit -> {'raw_results'}}, @{$raw_results_row} );
	    
	    &{$self -> {'_raw_results_callback'}}( $self, $modelfit )
		if ( defined $self -> {'_raw_results_callback'} );

	    if( $xv and not $self -> {'bca_mode'} ) {
	      foreach my $row ( @{$modelfit -> {'raw_results'}} ) {
		unshift( @{$row}, 'cdd' );
	      }
	      unshift( @{$modelfit -> {'raw_results_header'}}, 'method' );
	    }
	  }
	}; 
	return $subroutine;
      }
end _modelfit_raw_results_callback

# }}} _modelfit_raw_results_callback

# {{{ modelfit_analyze

start modelfit_analyze
      {
	# Only valid for one problem and one sub problem.

	if ( $self -> {'cross_validate'} ) {

	  # ---  Evaluate the models on the remainder data sets  ----

	  # {{{ eval models

	  for ( my $i = 0;
		$i < scalar @{$self -> {'prediction_models'}[$model_number-1]{'own'}};
		$i++ ) {
	    $self -> {'prediction_models'}[$model_number-1]{'own'}[$i] ->
		update_inits( from_model => $self ->
			      {'prepared_models'}[$model_number-1]{'own'}[$i]);
	    $self -> {'prediction_models'}[$model_number-1]{'own'}[$i] ->
		remove_records( type => 'covariance' );
	    $self -> {'prediction_models'}[$model_number-1]{'own'}[$i] -> _write;
	  }
	  my ($dir,$file) = 
	      OSspecific::absolute_path( $self -> {'directory'},
					 $self -> {'raw_results_file'}[$model_number-1] );
	  my $xv_threads = ref( $self -> {'threads'} ) eq 'ARRAY' ? 
	      $self -> {'threads'} -> [1]:$self -> {'threads'};
	  my $mod_eval = tool::modelfit ->
	      new( reference_object => $self,
		   models           => $self ->
  		       {'prediction_models'}[$model_number-1]{'own'},
		   base_directory   => $self -> {'directory'},
		   directory        => $self -> {'directory'}.
		       'evaluation_dir'.$model_number, 
		   threads          => $xv_threads,
		   _raw_results_callback => $self ->
		          _modelfit_raw_results_callback( model_number => $model_number,
							  cross_validation_set => 1 ),
		   parent_tool_id   => $self -> {'tool_id'},
		   logfile	    => undef,
		   raw_results      => undef,
		   prepared_models  => undef,
		   top_tool         => 0,
		   retries          => 1 );
	  $Data::Dumper::Maxdepth = 2;
	  print "Running xv runs\n";
	  $mod_eval -> run;

	  # }}} eval models

	}

	# ------------  Cook-scores and Covariance-Ratios  ----------

	# {{{ Cook-scores and Covariance-Ratios

	# ----------------------  Cook-score  -----------------------
	
	# {{{ Cook-score

	my ( @cook_score, @cov_ratio );
	if( $self -> models -> [$model_number-1] ->
	    outputs -> [0] -> covariance_step_successful -> [0][0]) {

	  ui -> print( category => 'cdd',
		       message  => "Calculating the cook-scores" );
	  my @orig_ests;
	  my @changes;

	  my $output_harvest = $self ->
	      harvest_output( accessors => ['est_thetas','est_omegas','est_sigmas'],
			      search_output => 1 );
	  
	  # Calculate the changes
	  foreach my $param ( 'est_thetas', 'est_omegas', 'est_sigmas' ) {
	    my $orig_est = $self -> models -> [$model_number-1] -> outputs -> [0] -> $param;
	    my @mod_ests;
	    my $est = defined $output_harvest -> {$param} ?
		$output_harvest -> {$param} -> [$model_number-1]{'own'} : [];
	    if( defined $est ) {
	      for ( my $i = 0; $i < scalar @{$est}; $i++ ) {
		if( defined $est->[$i][0][0] ) {
		  my $n_par = scalar @{$est->[$i][0][0]};
		  # Since we use the _estimated_ parameters there should be no undefined elements
		  # Not sure what to do if we find one... /Lasse
		  for( my $j = 0; $j < $n_par; $j++ ) {
		    push( @{$changes[$i]}, $orig_est->[0][0][$j]-$est->[$i][0][0][$j]);
		  }
		}
	      }
	    }
	  }

	  my $inverse_covariance_matrix  = $self -> models -> [$model_number-1] ->
	      outputs -> [0] -> inverse_covariance_matrix -> [0][0];
	 
	  # Equation: sqrt((orig_est-est(i))'*inv_cov_matrix*(orig_est-est(i)))
 	  for ( my $i = 0; $i <= $#changes; $i++ ) {
	    if( defined $changes[$i] and
		scalar @{$changes[$i]} > 0 and 
		defined $inverse_covariance_matrix ) {
	      my $vec_changes = Math::MatrixReal ->
		  new_from_cols( [$changes[$i]] );
	      $cook_score[$i] = $inverse_covariance_matrix*$vec_changes;
	      $cook_score[$i] = ~$vec_changes*$cook_score[$i];
	    } else {
	      $cook_score[$i] = undef;
	    }

	    my $nl = $i == $#changes ? "" : "\r"; 
	    ui -> print( category => 'cdd',
			 message  => ui -> status_bar( sofar => $i+1,
						       goal  => $#changes+1 ).$nl,
			 wrap     => 0,
			 newline  => 0 );
	  }
	    
	  # Calculate the square root 
	  # The matrixreal object holds a 1x1 matrix in the first position of its array.
	  for ( my $i = 0; $i <= $#cook_score; $i++ ) {
	    if( defined $cook_score[$i] and 
		$cook_score[$i][0][0][0] >= 0 ) {
	      $cook_score[$i] = sqrt($cook_score[$i][0][0][0]);
	    } else {
	      open( LOG, ">>".$self -> {'logfile'}[$model_number-1] );
	      my $mes;
	      if( defined $cook_score[$i] ) {
		$mes = "Negative squared cook-score ",$cook_score[$i][0][0][0];
	      } else {
		$mes = "Undefined squared cook-score";
	      }
	      $mes .= "; can't take the square root.\n",
	      "The cook-score for model $model_number and cdd bin $i was set to zero\n";
	      print LOG $mes;
	      close( LOG );
	      debug -> warn( level => 1,
			  message => $mes );
	      
	      $cook_score[$i] = 0;
#	      $cook_score[$i] = undef;
	    }
	  }
	}
	
	$self -> {'cook_scores'} = \@cook_score;

	ui -> print( category => 'cdd',
		     message  => " ... done." );

	# }}} Cook-score

	# -------------------  Covariance Ratio  --------------------

	# {{{ Covariance Ratio

	if( $self -> models -> [$model_number-1] ->
	    outputs -> [0] -> covariance_step_successful -> [0][0]) {

	  # {{{ sub clear dots

	  sub clear_dots {
	    my $m_ref = shift;
	    my @matrix = @{$m_ref};
	    # get rid of '........'
	    my @clear;
	    foreach ( @matrix ) {
	      push( @clear, $_ ) unless ( $_ eq '.........' );
	    }
	    #	  print Dumper \@clear;
	    return \@clear;
	  }

	  # }}}
	  
	  # {{{ sub make square
	  
	  sub make_square {
	    my $m_ref = shift;
	    my @matrix = @{$m_ref};
	    # Make the matrix square:
	    my $elements = scalar @matrix; # = M*(M+1)/2
	    my $M = -0.5 + sqrt( 0.25 + 2 * $elements );
	    my @square;
	    for ( my $m = 1; $m <= $M; $m++ ) {
	      for ( my $n = 1; $n <= $m; $n++ ) {
		push( @{$square[$m-1]}, $matrix[($m-1)*$m/2 + $n - 1] );
		unless ( $m == $n ) {
		  push( @{$square[$n-1]}, $matrix[($m-1)*$m/2 + $n - 1] );
		}
	      }
	    }
	    return \@square;
	  }
	  
	  # }}}

	  ui -> print( category => 'cdd',
		       message  => "Calculating the covariance-ratios" );
	  
	  # Equation: sqrt(det(cov_matrix(i))/det(cov_matrix(orig)))
	  my $cov_linear  = $self -> models -> [$model_number-1] ->
	      outputs -> [0] ->  raw_covmatrix -> [0][0];
	  my $orig_det;
	  if( defined $cov_linear ) {
	    my $orig_cov = Math::MatrixReal ->
		new_from_cols( make_square( clear_dots( $cov_linear ) ) );
	    $orig_det = $orig_cov -> det();
	  }
	  # AUTOLOAD: raw_covmatrix

	  my $output_harvest = $self -> harvest_output( accessors => ['raw_covmatrix'],
							search_output => 1 );
	  
	  my $est_cov = defined $output_harvest -> {'raw_covmatrix'} ? $output_harvest -> {'raw_covmatrix'} -> [$model_number-1]{'own'} : [];

	  my $mods = scalar @{$est_cov};
	  for ( my $i = 0; $i < scalar @{$est_cov}; $i++ ) {
	    if ( $orig_det != 0 and defined $est_cov->[$i][0][0] ) {
	      my $cov = Math::MatrixReal ->
		  new_from_cols( make_square( clear_dots( $est_cov->[$i][0][0] ) ) );
	      my $ratio = $cov -> det() / $orig_det;
	      if( $ratio > 0 ) {
		push( @cov_ratio, sqrt( $ratio ) );
	      } else {
		open( LOG, ">>".$self -> {'logfile'}[$model_number-1] );
		print LOG "Negative covariance ratio ",$ratio,
		"; can't take the square root.\n",
		"The covariance ratio for model $model_number and cdd bin $i was set to one (1)\n";
#	      "The covariance ratio for model $model_number and cdd bin $i was set to undef\n";
		close( LOG );
		push( @cov_ratio, 1 );
#	      push( @cov_ratio, undef );
	      }	      
	    } else {
	      open( LOG, ">>".$self -> {'logfile'}[$model_number-1] );
	      print LOG "The determinant of the cov-matrix of the original run was zero\n",
	      "or the determinant of cdd bin $i was undefined\n",
	      "The covariance ratio for model $model_number and cdd bin $i was set to one (1)\n";
#	    "The covariance ratio for model $model_number and cdd bin $i was set to undef\n";
	      close( LOG );
	      push( @cov_ratio, 1 );
#	    push( @cov_ratio, undef );
	    }
	    
	    my $nl = $i == $mods-1 ? "" : "\r"; 
	    ui -> print( category => 'cdd',
			 message  => ui -> status_bar( sofar => $i+1,
						       goal  => $mods ).$nl,
			 wrap     => 0,
			 newline  => 0 );
	  }
	}

	$self -> {'covariance_ratios'} = \@cov_ratio;

	ui -> print( category => 'cdd',
		     message  => " ... done." );
	
	# }}} Covariance Ratio

	# -  Perform a PCA on the cook-score:covariance-ratio data --

	# {{{ PCA

	my ( @outside_n_sd, $eig_ref, $eig_vec_ref, $proj_ref, $std_ref );

	if( $self -> models -> [$model_number-1] ->
	    outputs -> [0] -> covariance_step_successful -> [0][0]) {
	  
	  ( $eig_ref, $eig_vec_ref, $proj_ref, $std_ref ) =
#	    die Dumper [\@cook_score,\@cov_ratio];
	      $self -> pca( data_matrix => [\@cook_score,\@cov_ratio] );
	  my @projections = @{$proj_ref};
	  my @standard_deviation = @{$std_ref};

	  # }}}

	  # ----  Mark the runs with CS-CR outside N standard deviations of the PCA ----

	  # {{{ mark runs

	  for( my $i = 0; $i <= $#projections; $i++ ) {
	    my $vector_length = 0;
	    for( my $j = 0; $j <= 1; $j++ ) {
	      $vector_length += $projections[$i][$j]**2;
	    }
	    $vector_length = sqrt( $vector_length );
	    my $n_sd = 0;
	    for( my $j = 0; $j <= 1; $j++ ) {
	      $n_sd += (($projections[$i][$j]/$vector_length)*$standard_deviation[$j])**2;
	    }
	    $n_sd = ( $self -> {'outside_n_sd_check'} * sqrt( $n_sd ) );
	    $outside_n_sd[$i] = $vector_length > $n_sd ? 1 : 0;
	  }
        }

	$self -> {'outside_n_sd'} = \@outside_n_sd;

	# }}} mark runs

	my %covariance_return_section;
	$covariance_return_section{'name'} = 'Diagnostics';
	$covariance_return_section{'labels'} = [[],['cook.scores','covariance.ratios','outside.n.sd']];

	my @res_array;
	for( my $i = 0; $i <= $#cov_ratio; $i ++ ){
	  push( @res_array , [$cook_score[$i],$cov_ratio[$i],$outside_n_sd[$i]] );
	}

	$covariance_return_section{'values'} = \@res_array;

	push( @{$self -> {'results'}[$model_number-1]{'own'}},\%covariance_return_section );

	# }}}

	# ---------  Relative estimate change and Jackknife bias  ----------

	# {{{ Relative change of the parameter estimates

	my $output_harvest = $self -> harvest_output( accessors => ['ofv', 'thetas', 'omegas', 'sigmas','sethetas', 'seomegas', 'sesigmas'],
						      search_output => 1 );

	my %return_section;
	$return_section{'name'} = 'relative.changes';
	$return_section{'labels'} = [[],[]];

	my %bias_return_section;
	$bias_return_section{'name'} = 'Jackknife.bias.estimate';
	$bias_return_section{'labels'} = [['bias','relative.bias'],[]];

	my ( @bias, @bias_num, @b_orig, @rel_bias );
	my $k = 0;
	foreach my $param ( 'thetas', 'omegas', 'sigmas' ) {
	  my $orig_est = $self -> {'models'} -> [$model_number-1] -> outputs -> [0] -> $param;
	  if ( defined $orig_est->[0][0] ) {
	    for ( my $j = 0; $j < scalar @{$orig_est->[0][0]}; $j++ ) {
	      $b_orig[$k++] = $orig_est->[0][0][$j];
	    }
	  }
	}

	my @rel_ests;	

	for ( my $i = 0; $i < scalar @{$output_harvest -> {'ofv'} -> [$model_number-1]{'own'}}; $i++ ) {
	  my @values;
	  my $k = 0;
	  foreach my $param ( 'ofv', 'thetas', 'omegas', 'sigmas',
			      'sethetas', 'seomegas', 'sesigmas',) {
	    
	    my $orig_est = $self -> {'models'} -> [$model_number-1] -> outputs -> [0] -> $param;
	    my $est = defined $output_harvest -> {$param} ? $output_harvest -> {$param} -> [$model_number-1]{'own'} : [];
	    
	    if ( $param eq 'ofv' ) {
	      if ( defined $orig_est->[0][0] and $orig_est->[0][0] != 0 ) {
		push( @values, ($est->[$i][0][0]-$orig_est->[0][0])/$orig_est->[0][0]*100 );
	      } else {
		push( @values, 'INF' );
	      }
	      if( $i == 0 ){
		push( @{$return_section{'labels'} -> [1]}, $param );
	      }
	    } else {
	      my @in_rel_ests;
	      if( defined $est->[$i][0][0] ){
		for ( my $j = 0; $j < scalar @{$est->[$i][0][0]}; $j++ ) {
		  if ( defined $orig_est->[0][0][$j] and $orig_est->[0][0][$j] != 0 ) {
		    push( @values, ($est->[$i][0][0][$j]-$orig_est->[0][0][$j])/$orig_est->[0][0][$j]*100);
		    if( substr($param,0,2) ne 'se' ) {
		      $bias[$k] += $est->[$i][0][0][$j];
		      $bias_num[$k++]++;
		    }
		  } else {
		    push( @values, 'INF' );
		    if( substr($param,0,2) ne 'se' ) {
		      $k++;
		    }		  }
		  if( $i == 0 ){
		    if( substr($param,0,2) eq 'se' ) {
		      push( @{$return_section{'labels'} -> [1]}, uc(substr($param,0,4)).($j+1) );
		    } else {
		      my $lbl = uc(substr($param,0,2)).($j+1);
		      push( @{$bias_return_section{'labels'} -> [1]}, $lbl );
		      push( @{$return_section{'labels'} -> [1]}, $lbl );
		    }
		  }
		}
	      }
	    }
	    
	  }
	  push( @rel_ests, \@values );
	}
	
	# Jackknife bias

	for( my $i = 0; $i <= $#bias_num; $i++ ) {
	  # The [0] is there to handle the fact that thw bins
	  # attribute is restructured per model and problem
	  next if( not defined $bias[$i] );
	  $bias[$i] = ($self -> {'bins'}[$model_number-1][0]-1)*
	      ($bias[$i]/$bias_num[$i]-$b_orig[$i]);
	  if( defined $b_orig[$i] and $b_orig[$i] != 0 ) {
	    $rel_bias[$i] = $bias[$i]/$b_orig[$i]*100;
	  } else {
	    $rel_bias[$i] = undef;
	  }
	}
	$bias_return_section{'values'} = [\@bias,\@rel_bias];

	$return_section{'values'} = \@rel_ests ;
	push( @{$self -> {'results'}[$model_number-1]{'own'}},\%return_section );
	push( @{$self -> {'results'}[$model_number-1]{'own'}},\%bias_return_section );

	# }}} Relative change of the parameter estimates


	$self -> update_raw_results(model_number => $model_number);

	# -------------  Register the results in a Database  ----------------

	if( not -e $self -> {'directory'}."m$model_number/done.database.results" ) {
	  open( DB, ">".$self -> {'directory'}."m$model_number/done.database.results" );
	  my ( $start_id, $last_id ) = $self ->
	      register_mfit_results( model_number     => $model_number,
				     cook_score       => \@cook_score,
				     covariance_ratio => \@cov_ratio,
				     projections      => $proj_ref,
				     outside_n_sd      => \@outside_n_sd );
	  print DB "$start_id-$last_id\n";
	  close( DB );
	}

	# experimental: to save memory
	$self -> {'prepared_models'}[$model_number-1]{'own'} = undef;
	if( defined $PsN::config -> {'_'} -> {'R'} and
	    -e $PsN::lib_dir . '/R-scripts/cdd.R' ) {
	  # copy the cdd R-script
	  cp ( $PsN::lib_dir . '/R-scripts/cdd.R', $self -> {'directory'} );
	  # Execute the script
	  system( $PsN::config -> {'_'} -> {'R'}." CMD BATCH cdd.R" );
	}
      }
end modelfit_analyze

# }}} modelfit_analyze

# {{{ pca
start pca

my $D = Math::MatrixReal ->
    new_from_rows( \@data_matrix );
my @n_dim = @{$data_matrix[0]};
my @d_dim = @data_matrix;
my $n = scalar @n_dim;
my $d = scalar @d_dim;
map( $_=(1/$n), @n_dim );
my $frac_vec_n = Math::MatrixReal ->
    new_from_cols( [\@n_dim] );
map( $_=1, @n_dim );
map( $_=1, @d_dim );
my $one_vec_n = Math::MatrixReal ->
    new_from_cols( [\@n_dim] );
my $one_vec_d = Math::MatrixReal ->
    new_from_cols( [\@d_dim] );
my $one_vec_d_n = $one_vec_d * ~$one_vec_n;
my $M = $D*$frac_vec_n;
my $M_matrix = $M * ~$one_vec_n;

# Calculate the mean-subtracted data
my $S = $D-$M_matrix;

# compue the empirical covariance matrix
my $C = $S * ~$S;

# compute the eigenvalues and vectors
my ($l, $V) = $C -> sym_diagonalize();

# Project the original data on the eigenvectors
my $P = ~$V * $S;


# l, V and projections are all MatrixReal objects.
# We need to return the normal perl equivalents.
@eigenvalues = @{$l->[0]};
@eigenvectors = @{$V->[0]};
@std = @{$self -> std( data_matrix => $P -> [0] )};
# Make $P a n * d matrix
$P = ~$P;
@projections = @{$P->[0]};

end pca
# }}} pca

# {{{ std
start std

my ( @sum, @pow_2_sum );
if ( defined $data_matrix[0] ) {
  my $n = scalar @{$data_matrix[0]};
  for( my $i = 0; $i <= $#data_matrix; $i++ ) {
    for( my $j = 0; $j < $n; $j++ ) {
      $sum[$i] = $sum[$i]+$data_matrix[$i][$j];
      $pow_2_sum[$i] += $data_matrix[$i][$j]*$data_matrix[$i][$j];
    }
    $std[$i] = sqrt( ( $n*$pow_2_sum[$i] - $sum[$i]*$sum[$i] ) / ($n*$n) );
  }
}

end std
# }}} std

# {{{ modelfit_post_fork_analyze

start modelfit_post_fork_analyze
      {
#	my @modelfit_results = @{ $self -> {'results'} -> {'subtools'} };
	my @modelfit_results = @{ $self -> {'results'} };
	
	ui -> print( category => 'cdd',
		     message => "Soon done" );
      }
end modelfit_post_fork_analyze

# }}} modelfit_post_fork_analyze

# {{{ modelfit_results

start modelfit_results
      {
	my @orig_models  = @{$self -> {'models'}};
	my @orig_raw_results = ();
	foreach my $orig_model ( @orig_models ) {
	  my $orig_output = $orig_model -> outputs -> [0];
	  push( @orig_raw_results, $orig_output -> $accessor );
	}
#	my @models  = @{$self -> {'prepared_models'}};
	my @outputs = @{$self -> {'results'}};
	
	my @raw_results = ();

	foreach my $mod ( @outputs ) {
	  my @raw_inner = ();
	  foreach my $output ( @{$mod -> {'subset_outputs'}} ) {
	    push( @raw_inner, $output -> $accessor );
	  }
	  push( @raw_results, \@raw_inner );
	}
	if ( $format eq 'relative' or $format eq 'relative_percent' ) {
	  @results = ();
	  for ( my $i = 0; $i <= $#orig_raw_results; $i++ ) {
	    print "Model\t$i\n";
	    my @rel_subset = ();
	    for ( my $i2 = 0; $i2 < scalar @{$raw_results[$i]}; $i2++ ) {
	      print "Subset Model\t$i2\n";
	      my @rel_prob = ();
	      for ( my $j = 0; $j < scalar @{$orig_raw_results[$i]}; $j++ ) {
		print "Problem\t$j\n";
		if( ref( $orig_raw_results[$i][$j] ) eq 'ARRAY' ) {
		  my @rel_subprob = ();
		  for ( my $k = 0; $k < scalar @{$orig_raw_results[$i][$j]}; $k++ ) {
		    print "Subprob\t$k\n";
		    if( ref( $orig_raw_results[$i][$j][$k] ) eq 'ARRAY' ) {
		      my @rel_instance = ();
		      for ( my $l = 0; $l < scalar @{$orig_raw_results[$i][$j][$k]}; $l++ ) {
			print "Instance\t$l\n";
			my $orig = $orig_raw_results[$i][$j][$k][$l];
			my $res  = $raw_results[$i][$i2][$j][$k][$l];
			if( defined $orig and ! $orig == 0 ) {
			  print "ORIGINAL $orig\n";
			  print "SUBSET   $res\n";
			  print "RELATIVE ",$res/$orig,"\n";
			  if ( $format eq 'relative_percent' ) {
			    push( @rel_instance, ($res/$orig-1)*100 );
			  } else {
			    push( @rel_instance, $res/$orig );
			  }
			} else {
			  push( @rel_instance, 'NA' );
			}
			push( @rel_subprob,\@rel_instance );
		      }
		    } elsif( ref( $orig_raw_results[$i][$j][$k] ) eq 'SCALAR' ) {
		      print "One instance per problem\n";
		      my $orig = $orig_raw_results[$i][$j][$k];
		      my $res  = $raw_results[$i][$i2][$j][$k];
		      if( defined $orig and ! $orig == 0 ) {
			print "ORIGINAL $orig\n";
			print "SUBSET   $res\n";
			print "RELATIVE ",$res/$orig,"\n";
			if ( $format eq 'relative_percent' ) {
			  push( @rel_subprob, ($res/$orig-1)*100 );
			} else {
			  push( @rel_subprob, $res/$orig );
			}
		      } else {
			push( @rel_subprob, 'NA' );
		      }
		    } else {
		      print "WARNING: tool::cdd -> modelfit_results: neither\n\t".
			"array or scalar reference found at layer 4 in result data\n\t".
			  "structure (found ",ref( $orig_raw_results[$i][$j][$k] ),")\n";
		    }
		  }
		  push( @rel_prob, \@rel_subprob );
		} elsif( ref( $orig_raw_results[$i][$j] ) eq 'SCALAR' ) {
		  print "One instance per problem\n";
		  my $orig = $orig_raw_results[$i][$j];
		  my $res  = $raw_results[$i][$i2][$j];
		  if( defined $orig and ! $orig == 0 ) {
		    print "ORIGINAL $orig\n";
		    print "SUBSET   $res\n";
		    print "RELATIVE ",$res/$orig,"\n";
		    if ( $format eq 'relative_percent' ) {
		      push( @rel_prob, ($res/$orig-1)*100 );
		    } else {
		      push( @rel_prob, $res/$orig );
		    }
		  } else {
		    push( @rel_prob, 'NA' );
		  }
		} else {
		  print "WARNING: tool::cdd -> modelfit_results: neither\n\t".
		    "array or scalar reference found at layer 3 in result data\n\t".
		      "structure (found ",ref( $orig_raw_results[$i][$j] ),")\n";
		}
	      }
	      push( @rel_subset, \@rel_prob );
	    }
	    push( @results, \@rel_subset );
	  }
	} else {
	  @results = @raw_results;
	}
      }
end modelfit_results

# }}} modelfit_results

# {{{ relative_estimates

start relative_estimates
      {
	my $accessor = $parameter.'s';
	my @params = $self -> $accessor;

# 	print "Parameter: $parameter\n";
# 	sub process_inner_results {
# 	  my $res_ref = shift;
# 	  my $pad     = shift;
# 	  $pad++;
# 	  foreach my $res ( @{$res_ref} ) {
# 	    if ( ref ( $res ) eq 'ARRAY' ) {
# 	      process_inner_results( $res, $pad );
# 	    } else {
# 	      print "RELEST $pad\t$res\n";
# 	    }
# 	  }
# 	}
# 	process_inner_results( \@params, 0 );

	my @orig_params = $self -> $accessor( original_models => 1 );
	#                       [?][model][prob][subp][#]
	# print "ORIG TH1: ",$orig_params[0][0][0][0][0],"\n";
	for ( my $i = 0; $i < scalar @params; $i++ ) {
	  # Loop over models
	  my @mod = ();
	  for ( my $j = 0; $j < scalar @{$params[$i]}; $j++ ) {
	    # Loop over data sets
	    my @prep = ();
	    for ( my $k = 1; $k < scalar @{$params[$i]->[$j]}; $k++ ) {
	      # Loop over problems (sort of, at least)
	      my @prob = ();
	      for ( my $l = 0; $l < scalar @{$params[$i]->[$j]->[$k]}; $l++ ) {
		# Loop over sub problems (sort of, at least)
		my @sub = ();
		for ( my $m = 0; $m < scalar @{$params[$i]->[$j]->[$k]->[$l]}; $m++ ) {
		  # Loop over the params
		  my @par = ();
		  for ( my $n = 0; $n < scalar @{$params[$i][$j][$k][$l][$m]}; $n++ ) {
		    my $orig = $orig_params[$i][$j][$l][$m][$n];
#		    my $orig = $params[$i][$j][0][$l][$m][$n];
		    my $prep = $params[$i][$j][$k][$l][$m][$n];
		    if ( $orig != 0 ) {
		      if ( $percentage ) {
			push( @par, ($prep/$orig*100)-100 );
		      } else {
			push( @par, $prep/$orig );
		      }
		    } else {
		      push( @par, $PsN::out_miss_data );
		    }
		  }
		  push( @sub, \@par );
		}
		push( @prob, \@sub );
	      }
	      push( @prep, \@prob );
	    }
	    push( @mod, \@prep );
	  }
	  push( @relative_estimates, \@mod );
	}
      }
end relative_estimates

# }}} relative_estimates

# {{{ relative_confidence_limits

start relative_confidence_limits
      {
	my @params = @{$self -> confidence_limits( class     => 'tool::llp',
						   parameter => $parameter )};
	for ( my $i = 0; $i < scalar @params; $i++ ) {
	  # Loop over models
	  my @mod = ();
	  for ( my $j = 1; $j < scalar @{$params[$i]}; $j++ ) {
	    # Loop over data sets
	    my %num_lim;
	    my @nums = sort {$a <=> $b} keys %{$params[$i][$j]};
	    foreach my $num ( @nums ) {
	      my @prob_lim = ();
	      for ( my $n = 0; $n < scalar @{$params[$i][$j]->{$num}}; $n++ ) {
		my @side_lim = ();
		for ( my $o = 0; $o < scalar @{$params[$i][$j]->{$num}->[$n]}; $o++ ) {
		  # OBS: the [0] in the $j position points at the first element i.e
		  # the results of the tool run on the original model 
		  my $orig = $params[$i][0]->{$num}->[$n][$o];
		  my $prep = $params[$i][$j]->{$num}->[$n][$o];
		  print "ORIG: $orig, PREP: $prep\n";
		  if ( $orig != 0 ) {
		    if ( $percentage ) {
		      push( @side_lim, ($prep/$orig*100)-100 );
		    } else {
		      push( @side_lim, $prep/$orig );
		    }
		  } else {
		    push( @side_lim, $PsN::out_miss_data );
		  }
		}
		push( @prob_lim, \@side_lim );
	      }
	      $num_lim{$num} = \@prob_lim;
	    }
	    push( @mod, \%num_lim );
	  }
	  push( @relative_limits, \@mod );
	}
      }
end relative_confidence_limits

# }}} relative_confidence_limits

# {{{ llp_print_results

start llp_print_results
      {
	# NOTE! Only valid for models with one problem and one sub problem!

	my %relative_values;
	$relative_values{'theta_cis'} = $self ->
	  relative_confidence_limits( parameter  => 'theta',
				      percentage => 1 );
	$relative_values{'omega_cis'} = $self ->
	  relative_confidence_limits( parameter  => 'omega',
				      percentage => 1 );
	$relative_values{'sigma_cis'} = $self ->
	  relative_confidence_limits( parameter  => 'sigma',
				      percentage => 1 );
	$relative_values{'thetas'} = $self ->
	  relative_estimates( parameter => 'theta',
			     percentage => 1 );
	$relative_values{'omegas'} = $self ->
	  relative_estimates( parameter => 'omega',
			     percentage => 1 );
	$relative_values{'sigmas'} = $self ->
	  relative_estimates( parameter => 'sigma',
			     percentage => 1 );
	$relative_values{'sethetas'} = $self ->
	  relative_estimates( parameter => 'setheta',
			     percentage => 1 );
	$relative_values{'seomegas'} = $self ->
	  relative_estimates( parameter => 'seomega',
			     percentage => 1 );
	$relative_values{'sesigmas'} = $self ->
	  relative_estimates( parameter => 'sesigma',
			     percentage => 1 );

	my %prep_values;
	$prep_values{'theta_cis'} = $self -> confidence_limits( class     => 'tool::llp',
								parameter => 'theta' );;
	$prep_values{'omega_cis'} = $self -> confidence_limits( class     => 'tool::llp',
								parameter => 'omega' );;
	$prep_values{'sigma_cis'} = $self -> confidence_limits( class     => 'tool::llp',
								parameter => 'sigma' );;
	$prep_values{'thetas'}   = $self -> thetas;
	$prep_values{'omegas'}   = $self -> omegas;
	$prep_values{'sigmas'}   = $self -> sigmas;
	$prep_values{'sethetas'} = $self -> sethetas;
	$prep_values{'seomegas'} = $self -> seomegas;
	$prep_values{'sesigmas'} = $self -> sesigmas;



	open( RES, ">".$self -> {'results_file'} );
	print RES "Case-Deletion Diagnostic with Log-Likelihood Profiling\n";
	# Loop over models
	for ( my $i = 0; $i < scalar @{$relative_values{'theta_cis'}}; $i++ ) {
	  print RES "MODEL:;",$i+1,"\n";
	  foreach my $param ( 'theta_cis', 'omega_cis', 'sigma_cis' ) {
	    print RES "\n",uc($param),":\n";
	    # Loop over data sets

	    my @nums = sort {$a <=> $b} keys %{$relative_values{$param}[$i][0]};
	    print RES ";";
	    foreach my $num ( @nums ) {
		printf RES "$num;;;;";
	    }
	    print RES "\n";
	    foreach my $num ( @nums ) {
	      for ( my $o = 0; $o < scalar @{$relative_values{$param}[$i][0]->{$num}[0]}; $o++ ) {
		my $side = $o == 0 ? 'lower' : 'upper';
		printf RES ";$side;rel diff (%)";
	      }
	    }
	    print RES "\n";
	    print RES "orig";
	    foreach my $num ( @nums ) {
	      for ( my $o = 0; $o < scalar @{$relative_values{$param}[$i][0]->{$num}[0]}; $o++ ) {
		printf RES ";%7.5f",$prep_values{$param}[$i][0]->{$num}[0][$o];
		print RES ";0";
	      }
	    }
	    print RES "\n";
	    for ( my $j = 0; $j < scalar @{$relative_values{$param}[$i]}; $j++ ) {
	      printf RES "%-7d",$j+1;
	      my @nums = sort {$a <=> $b} keys %{$relative_values{$param}[$i][$j]};
	      foreach my $num ( @nums ) {
		for ( my $n = 0; $n < scalar @{$relative_values{$param}[$i][$j]->{$num}}; $n++ ) {
		  for ( my $o = 0; $o < scalar @{$relative_values{$param}[$i][$j]->{$num}[$n]}; $o++ ) {
		    my $rel = $relative_values{$param}[$i][$j]->{$num}[$n][$o];
		    my $prep = $prep_values{$param}[$i][$j+1]->{$num}[$n][$o];
		    printf RES ";%7.5f",$prep;
		    printf RES ";%3.0f",$rel;
		  }
		}
	      }
	      print RES "\n";
	    }
	  }
	}		
	# Skipped id's, keys and values:
	# Loop over models


#  	sub process_inner_results {
#  	  my $res_ref = shift;
#  	  my $pad     = shift;
#  	  $pad++;
#  	  foreach my $res ( @{$res_ref} ) {
#  	    if ( ref ( $res ) eq 'ARRAY' ) {
# 	      print "$pad ARRAY size ",scalar @{$res},"\n";
# 		process_inner_results( $res, $pad );
#  	    } elsif ( ref ( $res ) eq 'HASH' ) {
# 	      print "$pad HASH keys ",keys %{$res},"\n";
#  	    } else {
#  	      print "$pad OTHER\n";
#  	    }
#  	  }
#  	}
#  	process_inner_results( $self -> {'results'}, 0 );

	my $i = 1;
	foreach my $own ( @{$self -> {'results'} -> {'own'}} ) {
# 	  print "REF1: ",ref($mod),"\n";
# 	  foreach my $prob ( @{$mod} ) {
# 	    print "REF2: ",ref($prob),"\n";
# 	    foreach my $subprob ( @{$prob} ) {
# 	      print "REF3: ",ref($subprob),"\n";
# 	      print "KEYS: ",keys %{$subprob},"\n";
# 	    }
# 	  }
	  print RES "MODEL $i\n";
	  foreach my $param ( 'skipped_ids', 'skipped_keys', 'skipped_values' ) {
	    print RES uc($param),"\n";
	    my $j = 1;
	    foreach my $prep ( @{$own -> {$param}} ) {
	      print RES "Bin no;$j;";
	      foreach my $val ( @{$prep} ) {
		print RES ";$val";
	      }
	      print RES "\n";
	      $j++;
	    }
	  }
	  $i++;
	}


# 	  for ( my $j = 0; $j < scalar @{$relative_values{'thetas_cis'}->[$i]}; $j++ ) {
# 	    print RES "MODEL:;",$j+1,"\n";
# 	    # Loop over problems (sort of, at least)
# 	    for ( my $l = 0; $l < scalar @{$relative_values{'thetas_cis'}->[$i]->[$j]->[0]}; $l++ ) {
# 	      # Loop over sub problems (sort of, at least)
# 	      for ( my $m = 0; $m < scalar @{$relative_values{'thetas_cis'}->[$i]->[$j]->[0]->[$l]}; $m++ ) {
# #		foreach my $param ( 'thetas', 'omegas', 'sigmas',
# #				  'sethetas', 'seomegas', 'sesigmas' ) {
# 		foreach my $param ( 'theta_cis' ) {
# 		  print RES uc($param),":\n\n";
# 		  # Here one could add printing of parameter names, i.e. 'CL V...' or 'TH1 TH2...'
# 		  # Loop over data sets
# 		  for ( my $k = 0; $k < scalar @{$relative_values{$param}->[$i]->[$j]}; $k++ ) {
# 		    printf RES "%-7d",$k+1;
# 		    for ( my $n = 0; $n < scalar @{$relative_values{$param}[$i][$j][$k][$l][$m]}; $n++ ) {
# 		      for ( my $o = 0; $o < scalar @{$relative_values{$param}[$i][$j][$k][$l][$m][$n]}; $o++ ) {
# 			my $rel  = $relative_values{$param}->[$i][$j][$k][$l][$m][$n];
# 			my $prep = $prep_values{$param}->[$j][$k][$l][$m][$n];
# 			printf RES ";%7.5f",$prep;
# 			printf RES ";%3.0f",$rel;
# 		      }
# 		      print RES "\n";
# 		    }
# 		    print RES "\n";
# 		  }
# 		  print RES "\n";
# 		}
# 	      }
# 	      print "\n";
# 	    }
# 	    print "\n\n";
# 	  }
# 	}
	close( RES );
      }
end llp_print_results

# }}} llp_print_results

# {{{ general_print_results

start general_print_results
      {
	unless ( defined $self -> {'results'} ) {
	  print "WARNING: cdd->general_print_results: no return values defined;\n"
	    ."cannot print results\n";
	  return;
	}
	my %results = %{$self -> {'results'}};
	
	open( RES, ">".$self -> {'results_file'} );
	print RES "Case-Deletion Diagnostic\n";

	# Print meta data

	unless ( defined $results{'own'} ) {
	  print "WARNING: cdd->general_print_results: no own return values defined;\n"
	    ."cannot print results\n";
	  return;
	}
	
	
	my @own_results = @{$results{'own'}};
	foreach my $result_unit ( @own_results ) {
	  print RES $result_unit -> {'name'},"\n";
	  print RES $result_unit -> {'comment'},"\n";
	  my @values = defined $result_unit{'values'} ? @{$result_unit{'values'}} : ();
	  my @labels = defined $result_unit{'labels'} ? @{$result_unit{'labels'}} : ();
	  # Loop the models
	  for ( my $i = 0; $i <= $#values; $i++ ) {
	    # Loop the problems
	    for ( my $j = 0; $j <= $#values[$i]; $j++ ) {	
	      # Loop the sub problems
	      for ( my $k = 0; $k <= $#values[$i][$j]; $k++ ) {
		# Loop the first result dimension
		for (my $l = 0; $l <= $#values[$i][$j][$k]; $l++ ) {
		  # Loop the second result dimension
		  for ( my $m = 0; $m <= $#values[$i][$j][$k][$l]; $m++ ) {
		    # Loop the second result dimension
		    for ( my $m = 0; $m <= $#values[$i][$j]$k][$l]; $m++ ) {
		  foreach my $model_res ( @values ) {
	    foreach my $prob_res ( @{$model_unit} ) {
	      foreach my $subprob_res ( @{$prob_unit} ) {
		foreach my $subprob_res ( @{$prob_unit} ) {
	
	close ( RES );
      }
end general_print_results

# }}} general_print_results

# {{{ modelfit_print_results

start modelfit_print_results
      {
	my @parameters = ( 'theta', 'omega', 'sigma',
			   'setheta', 'seomega', 'sesigma' );
	my %relative_values;
	my %prep_values;
	my %orig_values;
	foreach my $parameter ( @parameters ) {
	  my $accessor = $parameter.'s';
	  $relative_values{$parameter} = $self ->
	    relative_estimates( parameter => $parameter,
				percentage => 1 );
	  $prep_values{$parameter} = $self -> $accessor;
	  $orig_values{$parameter} = $self -> $accessor( original_models => 1 );
	}
# 	  sub process_results {
# 	    my $res_ref = shift;
# 	    my $pad     = shift;
# 	    $pad++;
# 	    foreach my $res ( @{$res_ref} ) {
# 	      if ( ref ( $res ) eq 'ARRAY' ) {
# 		process_results( $res, $pad );
# 	      } else {
# 		print "final $pad\t$res\n";
# 	      }
# 	    }
# 	  }
# 	  process_results( $relative_values{'thetas'}, 0 );


	my %nparam;
	print "Calling nthetas\n";
	$nparam{'thetas'} = $self -> nthetas( original_models => 1 );
	print "Done that\n";
	open( RES, ">".$self -> {'results_file'} );
	print RES "Case-Deletion Diagnostic\n";
	# Date information to be added
	#	print RES "Date:;;;;",$self -> {'date'},"\n";

	print RES "Modelfiles:";
	foreach my $model ( @{$self -> {'models'}} ) {
	  print RES ";;;;",$model -> filename,"\n";
	}

# Based on columns and number of datasets might better be shown if split by
# model and problem:
	print RES "Based on columns:";
	my $vars = $self -> {'case_columns'};
	if ( ref( $vars ) eq 'ARRAY' ) {
	  foreach my $vars2 ( @{$vars} ) {
	    if ( ref( $vars2 ) eq 'ARRAY' ) {
	      foreach my $vars3 ( @{$vars2} ) {
		print RES ";;;;$vars3\n";
	      }
	    } else {
	      print RES ";;;;$vars2\n";
	    }
	  }
	} else {
	  print RES ";;;;$vars\n";
	}

	print RES "Number of data sets:";
	my $bins = $self -> {'bins'};
	if ( ref( $bins ) eq 'ARRAY' ) {
	  foreach my $bins2 ( @{$bins} ) {
	    if ( ref( $bins2 ) eq 'ARRAY' ) {
	      foreach my $bins3 ( @{$bins2} ) {
		print RES ";;;;$bins3\n";
	      }
	    } else {
	      print RES ";;;;$bins2\n";
	    }
	  }
	} else {
	  print RES ";;;;$bins\n";
	}

	print RES "Selection:;;;;",$self-> {'selection_method'},"\n";
	if ( defined $self -> {'seed'} ) {
	  print RES "Seed number:;;;;",$self -> {'seed'},"\n";
	} else {
	  print RES "No seed number specified\n";
	}

# TODO: $skip_keys etc from data->case_deletion must be transferred back to
# the main process and appropriate attributes set.

	print RES "\n\n\n\n";

#	  process_results( $relative_values{'thetas'}->[0]->[0]->[0], 0 );

	for ( my $i = 0; $i < scalar @{$relative_values{'theta'}}; $i++ ) {
	  # Loop over models
	  for ( my $j = 0; $j < scalar @{$relative_values{'theta'}->[$i]}; $j++ ) {
	    print RES "MODEL:;",$j+1,"\n";
	    # Loop over problems (sort of, at least)
	    for ( my $l = 0; $l < scalar @{$relative_values{'theta'}->[$i]->[$j]->[0]}; $l++ ) {
	      # Loop over sub problems (sort of, at least)
	      for ( my $m = 0; $m < scalar @{$relative_values{'theta'}->[$i]->[$j]->[0]->[$l]}; $m++ ) {
		foreach my $param ( @parameters ) {
		  print RES uc($param),":\n\n";
		  # Here one could add printing of parameter names, i.e. 'CL V...' or 'TH1 TH2...'
		  # Loop over data sets
		  print RES ";";
		  for ( my $n = 1; $n <=scalar @{$relative_values{$param}[$i][$j][0][$l][$m]}; $n++ ) {
		    printf RES "estimate;rel diff (%);";
		  }
		  print RES "\n;";
		  for ( my $n = 1; $n <= scalar @{$relative_values{$param}[$i][$j][0][$l][$m]}; $n++ ) {
		    printf RES "$n;;";
		  }
		  print RES "\n";
		  print RES "orig";
		  for ( my $n = 0; $n < scalar @{$relative_values{$param}[$i][$j][0][$l][$m]}; $n++ ) {
		    printf RES ";%7.5f",$orig_values{$param}[$j][$l][$m][$n];
		    print RES ";0";
		  }
		  print RES "\n";
		  for ( my $k = 0; $k < scalar @{$relative_values{$param}->[$i]->[$j]}; $k++ ) {
		    printf RES "%-7d",$k+1;
		    for ( my $n = 0; $n < scalar @{$relative_values{$param}[$i][$j][$k][$l][$m]}; $n++ ) {
		      my $rel  = $relative_values{$param}->[$i][$j][$k][$l][$m][$n];
		      my $prep = $prep_values{$param}->[$j][$k+1][$l][$m][$n];
		      printf RES ";%7.5f",$prep;
		      printf RES ";%3.0f",$rel;
		    }
		    print RES "\n";
		  }
		  print RES "\n";
		}
	      }
	      print "\n";
	    }
	    print "\n\n";
	  }
	}

#  	sub process_inner_results {
#  	  my $res_ref = shift;
#  	  my $pad     = shift;
#  	  $pad++;
#  	  foreach my $res ( @{$res_ref} ) {
#  	    if ( ref ( $res ) eq 'ARRAY' ) {
# 	      print "$pad ARRAY size ",scalar @{$res},"\n";
# 		process_inner_results( $res, $pad );
#  	    } elsif ( ref ( $res ) eq 'HASH' ) {
# 	      print "$pad HASH keys ",keys %{$res},"\n";
#  	    } else {
#  	      print "$pad OTHER\n";
#  	    }
#  	  }
#  	}
#  	process_inner_results( $self -> {'results'}, 0 );
# die;
	# Skipped id's, keys and values:
	# Loop over models
	my $i = 1;
	foreach my $own ( @{$self -> {'results'} -> {'own'}} ) {
	  print RES "MODEL $i\n";
	  foreach my $param ( 'skipped_ids', 'skipped_keys', 'skipped_values' ) {
	    print RES uc($param),"\n";
	    my $j = 1;
	    foreach my $prep ( @{$own -> {$param}} ) {
	      print RES "Bin no;$j;";
	      foreach my $val ( @{$prep} ) {
		print RES ";$val";
	      }
	      print RES "\n";
	      $j++;
	    }
	  }
	  $i++;
	}

	close ( RES );
      }
end modelfit_print_results

# }}} modelfit_print_results

# {{{ prepare_results

start prepare_results
{
  if ( not defined $self -> {'raw_results'} ) {
    $self -> read_raw_results();
  }
}
end prepare_results

# }}}

# {{{ print_summary
start print_summary
{
  my ($outside_n_sd, $cook, $covrat );
  for( my $i = 0; $i < scalar @{$self -> {'raw_results_header'} -> [0]} ; $i++) {
    if( $self -> {'raw_results_header'} -> [0][$i] eq 'outside.n.sd' ){
      $outside_n_sd = $i;
    }
    if( $self -> {'raw_results_header'} -> [0][$i] eq 'cook.scores' ){
      $cook = $i;
    }
    if( $self -> {'raw_results_header'} -> [0][$i] eq 'cov.ratios' ){
      $covrat = $i;
    }
  }

  sub acknowledge {
    my $name    = shift;
    my $outcome = shift;
    my $file    = shift;
    my $l = (7 - length( $outcome ))/2;
    my $text = sprintf( "%-66s%2s%7s%-5s", $name, '[ ', $outcome. ' ' x $l, ' ]' );
    print $text, "\n\n";
    print $file $text if defined $file;
  }

  my ( @num, @cs, @cr ) ;
  for( my $model_i = 0; $model_i <= $#{$self -> {'raw_results'}}; $model_i++ ){
    for( my $prep_mod_i = 0; $prep_mod_i <= $#{$self -> {'raw_results'} -> [$model_i]}; $prep_mod_i++ ){
      my $test_val = $self -> {'raw_results'} -> [$model_i] -> [$prep_mod_i] -> [$outside_n_sd];
      if( defined($test_val) and $test_val == 1 ){
	push( @num, ($prep_mod_i) ); # prep_mod_i includes the original run as 0
	push( @cs, $self -> {'raw_results'} -> [$model_i] -> [$prep_mod_i] -> [$cook] );
	push( @cr, $self -> {'raw_results'} -> [$model_i] -> [$prep_mod_i] -> [$covrat] );
      }
    }
  }

  if ( $#num < 0 ) {
    acknowledge( 'No outlying case-deleted data set was found', 'OK' );
  } else {
    print "\n";
    acknowledge( (scalar @num).' case-deleted data sets were marked as outliers', 'WARNING' );
    printf( "\t%-20s%20s\t%20s\n", 'Data set', 'cook-score', 'covariance-ratio' );
    for( my $i = 0; $i <= $#num; $i++ ) {
      printf( "\t%-20s%14s%3.3f\t%14s%3.3f\n", $num[$i], ' ', $cs[$i], ' ', $cr[$i] );
    }
    print "\n";
  }

}
end print_summary
# }}} print_summary

# {{{ update_raw_results

start update_raw_results
    {
      my $cook_scores;
      my $cov_ratios;
      my $outside_n_sd;

#      foreach my $section( @{$self -> {'results'}[0] -> {'own'}} ){
#	if( $section -> {'name'} eq 'cook.scores' ){
#	  $cook_scores = $section -> {'values'};
#	}
#	if( $section -> {'name'} eq 'cov.ratio' ){
#	  $cov_ratios = $section -> {'values'};
#	}
#	if( $section -> {'name'} eq 'outside.n.sd' ){
#	  $outside_n_sd = $section -> {'values'};
#	}
#      }
      
      my ($dir,$file) =
	  OSspecific::absolute_path( $self -> {'directory'},
				     $self -> {'raw_results_file'}[$model_number-1] );
      open( RRES, $dir.$file );
      my @rres = <RRES>;
      close( RRES );
      open( RRES, '>',$dir.$file );

      chomp( $rres[0] );
      print RRES $rres[0] . ",cook.scores,cov.ratios,outside.n.sd\n";
      chomp( $rres[1] );
      print RRES $rres[1] . ",0,1,0\n";

      my @new_rres;
      for( my $i = 2 ; $i <= $#rres; $i ++ ) {
	my $row_str = $rres[$i];
	chomp( $row_str );
	$row_str .= sprintf( ",%.5f,%.5f,%1f\n" ,
			     $self -> {'cook_scores'} -> [$i-2],
			     $self -> {'covariance_ratios'} -> [$i-2],
			     $self -> {'outside_n_sd'} -> [$i-2] );
	print RRES $row_str;
      }
      close( RRES );
    }
end update_raw_results

# }}} update_raw_results

# {{{ create_R_scripts
start create_R_scripts
    {
      unless( -e $PsN::lib_dir . '/R-scripts/cdd.R' ){
	'debug' -> die( message => 'CDD R-script are not installed, no matlab scripts will be generated.' );
	return;
      }
      cp ( $PsN::lib_dir . '/R-scripts/cdd.R', $self -> {'directory'} );
    }
end create_R_scripts
# }}}
