# {{{ description, examples, synopsis, see also

# No method, just documentation
start description


end description

start examples
end examples

start synopsis


end synopsis

start see_also
    # =begin html
    #
    # <a HREF="../data.html">data</a>, <a
    # HREF="../model.html">model</a> <a
    # HREF="../output.html">output</a>, <a
    # HREF="../tool.html">tool</a>
    #
    # =end html
    #
    # =begin man
    #
    # data, model, output, tool
    #
    # =end man
end see_also

# }}}

# {{{ include
start include statements
    {
	use Carp;
	use Carp qw(cluck);
	use tool::xv_step;
    }
end include statements
# }}}

# {{{ new

start new
    {
	my $model;
	$model = $this -> models -> [0];
	unless( defined $model -> datas ){
	    'debug' -> die ( message => "No data object in modelobject\n" );
	}
    }
end new

# }}} new

# {{{ xv_step_pre_fork_setup

start xv_step_pre_fork_setup
    {
	my $subtools = undef;
	if( scalar @{$self -> {'subtools'}} > 1 ){
	    my @subtools = @{$self -> {'subtools'}};
	    shift( @subtools );
	    $subtools = \@subtools;
	}
	
	my $xv_step = tool::xv_step -> new( models => [$self -> {'models'} -> [0]], 
					    subtools => $subtools,
					    %{$self -> {'subtool_arguments'} -> {'xv_step'}},
					    subtool_arguments => $self -> {'subtool_arguments'});

	$xv_step -> create_data_sets;
	push( @{$self -> {'xv_steps'}}, $xv_step );
    }
end xv_step_pre_fork_setup

# }}}

# {{{ xv_step_setup

start xv_step_setup
    {
	unless( $model_number == 1 ){
	    my $subtools = undef;
	    if( scalar @{$self -> {'subtools'}} > 1 ){
		my @subtools = @{$self -> {'subtools'}};
		shift( @subtools );
		$subtools = \@subtools;
	    }

	    my $first_xv_step = $self -> {'xv_steps'} -> [0];
	    my $xv_step = tool::xv_step -> new( models => [$self -> {'models'} -> [$model_number - 1]],
						prediction_data => $first_xv_step -> prediction_data,
						estimation_data => $first_xv_step -> estimation_data, 
						subtools => $subtools,
						%{$self -> {'subtool_arguments'} -> {'xv_step'}},
						subtool_arguments => $self -> {'subtool_arguments'});

	    push( @{$self -> {'xv_steps'}}, $xv_step );
	} 
	
	$self -> {'tools'} = [$self -> {'xv_steps'} -> [$model_number-1]];
    }	
end xv_step_setup

# }}}

# {{{ xv_step_post_subtool_analyze

start xv_step_post_subtool_analyze
    {
      my $subtools = undef;
      if( scalar @{$self -> {'subtools'}} > 1 ){
	my @subtools = @{$self -> {'subtools'}};
	shift( @subtools );
	$subtools = \@subtools;
      }
      my $first_xv_step = $self -> {'xv_steps'} -> [0];
      if( $self -> {'xv_steps'} -> [$model_number - 1] -> cont ){
	$self -> {'xv_steps'} -> [$model_number -1] = tool::xv_step -> new( models => [$self -> {'models'} -> [$model_number - 1]],
									    prediction_data => $first_xv_step -> prediction_data,
									    estimation_data => $first_xv_step -> estimation_data, 
									    subtools => $subtools,
									    %{$self -> {'subtool_arguments'} -> {'xv_step'}},
									    subtool_arguments => $self -> {'subtool_arguments'} );
	push( @{$self -> {'tools'}}, $self -> {'xv_steps'} -> [$model_number-1] );
      }
    }
end xv_step_post_subtool_analyze

# }}}

