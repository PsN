# {{{ include

start include statements 
      {
	# Perl libraries #
	use Config;
	use Cwd;
	use Data::Dumper;
	use File::Copy qw/cp mv/;
	use File::Path;
	use FindBin qw($Bin);
	# External #
	use Storable;
	use Math::Random;
	# PsN includes #
	use fcon;
	use grid::nordugrid::xrsl_file;
	if( $PsN::config -> {'_'} -> {'use_keyboard'} ) {
	  require hotkey;
	}
	use nonmem;
	use POSIX ":sys_wait_h";
	use output;
	use OSspecific;
	use ui;
	use moshog_client;
	use Time::HiRes;
	use ext::IPC::Run3;
      }
end include statements

# }}} include statements

# {{{ description, examples, synopsis, see also

# No method, just documentation
start description
    #
    # In PsN versions < 2.0, the functionality for actually running
    # NONMEM on models and data PsN objects was provided by the model
    # class. As of PsN versions 2.0 and higher, this functinality has
    # been moved to the separate class I<modelfit> in order to make the
    # class responsibilities clearer.
    #
    # Fitting a model can be viewed as a special case of a more
    # general set of tools for population PK/PD. In PsN, the
    # modelfit class is therefore a specialization of a general PsN
    # tool class. The tool class itself is not capable of much at
    # all but to define a common structure for all PsN tools.
    #
    # All attributes and (class) methods specified for the general
    # tool class are inherited in the modelfit class. Some (class) methods
    # are defined in both classes (e.g. the L</run>) and in these
    # cases it is the modelfit version that will be used.
    #
    # =begin html
    #
    # <tr>Please look at the documentation for the <a
    # href="../tool.html">general tool class</a> as well as the <a
    # href="#examples">examples</a> section of this document for
    # descriptions of the setting that are available for all
    # tools.</tr>
    #
    # =end html
    # 
    # =begin man
    #
    # Please look at the documentation for the general I<tool> class
    # as well as the L</examples> section of this document for
    # descriptions of the setting that are available for all tools.
    #
    # =end man
    # 
end description

start examples
    # The following code may be used to create a simple modelfit
    # object and to run it.
    #
    #   use lib qw(path to PsN installation/lib);
    #   use tool::modelfit;
    #   use model;
    #   
    #   my $model = model -> new( filename => 'run1.mod' );
    #   my $modelfit = tool::modelfit -> new( models => [$model] );
    #   my %results = %{$modelfit -> run};
    #
    # To illustrate a more complex use of modelfit, we can run a
    # bootstrap of 200 resampled data sets of same size as the
    # original data set. In this example we assume that the modelfile
    # we use contains only one problem and one sub problem.
    #
    #   use lib qw(path to PsN installation/lib);
    #   use tool::modelfit;
    #   use data;
    #   use model;
    #   use output;
    #   Math::Random;       # For perturbation of initial estimates
    #                       # after unsuccessful minimizations
    #
    #   # set these to appropriate values for your own run
    #   my $samples = 200;
    #   my $modelfile = 'run1.mod';
    #   my $boot_sample_file = 'boot_ind.csv';
    #   my $boot_results_file = 'boot_results.csv';
    #
    #   # set the seed from a phrase (consistent over different hardware,
    #   # see Math::Random )
    #   random_set_seed_from_phrase('testing');
    #
    #   # ignore missing data and (especially) output files
    #   my $model = model -> new( filename => $modelfile,
    #                             ignore_missing_files => 1 );
    #   
    #   my $data = $model -> datas -> [0];
    #   
    #   # Create the bootstrap data sets. The default name for each
    #   # new resampled data file will be bs###.dta, where ### is a
    #   # number from 1 to $samples.
    #   my ( $dataref, $incl_ind_ref, $incl_keys_ref ) =
    #       $data -> bootstrap ( samples => $samples );
    #   
    #   # Save the resampled ID numbers in a file, one row for each
    #   # bootstrap data set
    #   open ( LOG, ">$boot_sample_file" );
    #   foreach my $sample ( @{$incl_ind_ref} ) {
    #     print LOG join(';', @{$sample} ),"\n";
    #   }
    #
    #   # Create the boostrap models and set the correct resampled
    #   # data file in $DATA. Save them in @bs_models.
    #   my @bs_models  = ();
    #   for ( my $i = 1; $i <= $samples; $i++ ) {
    #     my $bs_mod = $model -> copy( filename    => 'bs'.$i.'.mod',
    #                                  copy_data   => 0,
    #                                  copy_output => 0);
    #     $bs_mod -> datafiles( new_names     => ['bs'.$i.'.dta'],
    #                           absolute_path => 1 );
    #     $bs_mod -> _write;
    #     push( @bs_models, $bs_mod );
    #   }
    #
    #   # Create a modelfit object with the bootstrap models as
    #   # input. Set the number of parallel runs to 2.
    #   my $mfitobj = tool::modelfit -> new ( models     => \@bs_models,
    #                                           threads    => 2 );
    #
    #   # Run the model fit. Since the bootstrap models are named
    #   # bs###.mod, the default names for the output files will be
    #   # bs###.lst.
    #   $mfitobj -> run;
    #
    #   # We'll save the OFV plus the theta, omega and sigma estimates
    #   # for each run in a file.
    #   open( RESULT, ">$boot_results_file" );
    # 
    #   for ( my $i = 1; $i <= $samples; $i++ ) {
    #     my $bs_out = output -> new( filename => 'bs'.$i.'.lst' );
    #     my @ofv    = @{$bs_out -> ofv};
    #     my @thetas = @{$bs_out -> thetas};
    #     my @omegas = @{$bs_out -> omegas};
    #     my @sigmas = @{$bs_out -> sigmas};
    #     # We know that we only have one problem and one sub problem in this
    #     # example. ([0][0] comes from that fact)
    #     my @print_strings = ();
    #     push( @print_strings, $ofv[0][0] );
    #     push( @print_strings, @{$thetas[0][0]} );
    #     push( @print_strings, @{$omegas[0][0]} );
    #     push( @print_strings, @{$sigmas[0][0]} );
    #     print RESULT join( ';', @print_strings ),"\n";
    #   }
    #   
    #   close( RESULT );
    #
    #   # We're done!
end examples

start synopsis
    #   use tool::modelfit;
    #   use model;
    #
    #   my $model_obj = model -> new ( filename => 'run1.mod' );
    #   
    #   my $modelfit_obj = tool::modelfit -> new ( models => [$model_obj] );
    #   
    #   my $output_obj = $modelfit_obj -> run;
end synopsis

start see_also
    # =begin html
    #
    # <a HREF="../data.html">data</a>, <a
    # HREF="../model.html">model</a> <a
    # HREF="../output.html">output</a>, <a
    # HREF="../tool.html">tool</a>
    #
    # =end html
    #
    # =begin man
    #
    # data, model, output, tool
    #
    # =end man
end see_also

# }}}

# {{{ new

start new
    # Usage:
    # 
    #   $modelfit_object = tool::modelfit -> new( models  => [$model_object],
    #                                               retries => 5 );
    #
    # This is the basic usage and it creates a modelfit object that
    # can later be run using the L</run> method. I<models> is an array
    # of PsN model objects.
    #
    #   $modelfitObject = $tool::modelfit -> new( 'retries' => 5 );
    #   $modelfitObject -> add_model( init_data => { filename => $modelFileName } );
    #
    # This way of using modelfit is suitable if you have a list with
    # filenames of modelfiles. "add_model> will create modelfitobject
    # for you.
    #
    # A more interresting attribute is I<threads> which sets how many
    # parallel executions of NONMEM that will run. Some tips are:
    # Setting the number of threads higher than the number of nodes in
    # your cluster/supercomputer can make your runs slower. The
    # biggest limiting factor is the amount of memory needed by
    # NONMEM. With smaller runs, just set the thread number to the
    # number of nodes available.
    #
    # The I<directory> is the folder where the tools stores
    # temporary data and runs subtools (or in the modelfit case,
    # runs NONMEM). Each NONMEM run will have its own sub directory
    # NM_run[X] where [X] is an index running from 1 to the number of
    # runs. If unsure of what this means, leave it undefined and a
    # default will be used, e.g. modelfit_dir3 or something.
    #
    # Next, the I<compress> and I<remove_temp_files> attributes are good
    # if you want to save some hard disk space. I<compress> set to 1
    # will put all NONMEM output in to an tar/gz archive named
    # I<nonmem_files.tgz> placed in the I<NM_run[X]> directory
    # described above. If I<remove_temp_files> is set to 1,  the NONMEM
    # files: 'FCON', 'FDATA', 'FSTREAM', 'PRDERR' will be removed.
    #
    # I<clean> is a stronger version of I<remove_temp_files>; it will also
    # remove I<NM_run[X]> and all that is in these.
    #
    # I<retries> is the number of times L</run> will alter initial
    # values and (re)execute NONMEM when executions fail. I<retries>
    # can either be an integer, specifying the number of retries for
    # all models, or it can be an array with the number of retries
    # specific for each modelfile as elements. The default value is
    # B<5>. The algorithm for altering the initial values works
    # roughly like this: For each each new try, a random new initial
    # value is drawn from a uniform distribution with limits +-n*10%
    # of the original intial estimate and where n i equal to the retry
    # number. I.e. the first retry, the borders of the distribution
    # are +-10%. The algorithm ensures that the new values are within
    # specified boundaries.
    #
    # =begin html
    #
    # For a full dexcription of the algorithm, see <a
    # href="../model/problem/record/init_option.html#set_random_init">set_random_init</a>
    # of the <a
    # href="../model/problem/record/init_option.html">init_option
    # class</a>.
    #
    # =end html
    #
    # =begin man
    #
    # For a full dexcription of the algorithm, see I<set_random_init>
    # of the I<init_option> class.
    #
    # =end man
    #
    # If I<picky> is set to 1, the output from NONMEM will be checked
    # more thoroughly. If any of the lines below are found in the
    # minimization message, a rerun is initiated.
    #
    #    COVARIANCE STEP ABORTED
    #    PROGRAM TERMINATED BY OBJ
    #    ESTIMATE OF THETA IS NEAR THE BOUNDARY AND
    #    PARAMETER ESTIMATE IS NEAR ITS BOUNDARY
    #    R MATRIX ALGORITHMICALLY SINGULAR
    #    S MATRIX ALGORITHMICALLY SINGULAR
    #
    # I<nm_version> is a string with the version number of NONMEM that
    # will be used. The installed versions of NONMEM must be specified
    # in OSspecific.pm, the class responsible for system specific
    # features settings.
    #
    # I<logfile> specifies the name of the logfile.
    #
    # if I<silent_logfile> is defined all NONMEM output will
    # be written to I<NM_run[X]/xxx>, where xxx is the defined value.
    #
    # I<extra_files> is an array of strings where each string is a
    # file needed for NONMEM execution. Those file will be moved
    # to the I<NM_run[X]> directory.
    #
    # I<seed> is just a way to set a random seed number.
    #
    # If I<run_on_nordugrid> is set to true modelfit will submit the nonmem
    # runs to a grid. A group of related parameters are also
    # specified.

    # I<cpuTime> Is an estimated execution time for each individual
    # modelfile. It should preferably be a bit longer than reality. If
    # you specify a cpuTime that is to short, you risk that the grid
    # kills your jobs prematurely. The unit of I<cpuTime> is minutes.

    # I<grid_cpuTime> is the time of the actual grid job. It should be
    # used to group modelfiles together. For example, if you set
    # I<cpuTime> to ten minutes, I<grid_cpuTime> to 60 minutes and the
    # number of modelfiles is 14 modelfit will create three grid jobs,
    # two with six model files each and one with two modelfiles.

    # I<grid_adress> is the URL of the grid submission server,
    # e.g. hagrid.it.uu.se.


      {
	if ( defined $this -> {'logfile'} ) {
	  my $dir;
	  $this -> {'logfile'} = join('', OSspecific::absolute_path( $this -> {'directory'}, 
								     $this -> {'logfile'}) );
	}
	if ( $this -> {'ask_if_fail'} ) {
	  eval( 'use Tk' );
	}

	$this -> {'run_local'} = 1;

	if( $this -> {'run_on_lsf'} or
	    $this -> {'run_on_ud'} or
	    $this -> {'run_on_umbrella'} or
	    $this -> {'run_on_zink'} or
	    $this -> {'run_on_sge'} ){
	  $this -> {'run_local'} = 0;
	}

	if( $this -> {'handle_msfo'} ){
	  $this -> {'handle_crashes'} = 1;
	}

	if( $this -> {'handle_crashes'} ){
	  if( $this -> {'crash_restarts'} > 0 ){
	    $this -> {'crash_restarts'}++;
	  }
	}

	$this -> calculate_raw_results_width();

	$this -> {'raw_line_structure'} = ext::Config::Tiny -> new();

      }

end new

# }}} new

# {{{ calculate_raw_results_width

start calculate_raw_results_width
  {

    # 2008-01-24
    # This code comes largely from "prepare_raw_results" which should
    # be split over several functions to fit the serialized version of
    # PsN.

    # Some column in "raw_results_header" are meta-columns, they
    # will be replaced by several columns. For example, the
    # 'theta' column will be replaced with TH1, TH2, TH3 in the
    # general case. (Actually it will be replaced with the
    # thetas labels from the model file. Here we search the meta
    # column of the raw_results_header to find the maximum
    # number of real columns.

    my %max_hash;

    foreach my $model ( @{$self -> {'models'}} ){
    
      foreach my $category ( @{$self -> {'raw_results_header'}},'npomega' ) {
	if ( $category eq 'setheta' or $category eq 'seomega' or $category eq 'sesigma' ){
	  next;
	} elsif ( $category eq 'theta' or $category eq 'omega' or $category eq 'sigma' or
		  $category eq 'npomega' or $category eq 'shrinkage_etas' or $category eq 'eigen') {
	  my $numpar = 0;
	  if( $category eq 'npomega' or $category eq 'shrinkage_etas' ) {
	    my $nomegas = $model -> nomegas(with_correlations => 1);
	    if( defined $nomegas ) {
	      for( my $j = 0; $j < scalar @{$nomegas}; $j++ ) {
		if( $category eq 'npomega' ) {
		  my $npar = $nomegas -> [$j];
		  $npar = $npar*($npar+1)/2;
		  $numpar = $numpar >= $npar ? $numpar : $npar;
		} else {
		  $numpar = $numpar >= $nomegas -> [$j] ? $numpar : $nomegas -> [$j];
		}
	      }
	    }
	  } elsif( $category eq 'eigen') {

	    # This is an upper limit on the number of eigenvalues in
	    # the output file. The accessors should not count "SAME"
	    # but should count offdiagonals. It also counts "FIX"
	    # which is not ideal.

	    my $max_sigmas = 0;
	    foreach my $prob( @{$model -> nsigmas(with_correlations => 1 )} ) {
	      if( $prob > $max_sigmas ){
		$max_sigmas = $prob;
	      }
	    }

	    my $max_omegas = 0;
	    foreach my $prob( @{$model -> nomegas(with_correlations => 1 )} ) {
	      if( $prob > $max_omegas ){
		$max_omegas = $prob;
	      }
	    }

	    $numpar = $model -> nthetas() + $max_omegas + $max_sigmas;
	    
	  } else {

	    # Here we assume that $category is 'theta', 'omega' or
	    # 'sigma'. We also asume that 'SAME' estimates have a zero
	    # size.
	    
	    my $accessor = 'n'.$category.'s';
	    if( $category eq 'theta' ){
	      $numpar = $model -> $accessor();
	    } else {
	      # Here accessor must be omega or sigma.
	      $numpar = $model -> $accessor(with_correlations => 1);
	    }
	    
	    # omega and sigma is an array, we find the biggest member

	    if( ref($numpar) eq 'ARRAY' ){
	      my $max = 0;
	      foreach my $prob ( @{$numpar} ){
		if( $prob > $max ){
		  $max = $prob;
		}
	      }
	      $numpar = $max;
	    }
	    
	  }
	  if ( $max_hash{ $category } < $numpar ) {
	    $max_hash{ $category } = $numpar;
	    $max_hash{ 'se'.$category } = $numpar;
	  }
	} else {
	  $max_hash{ $category } = 1;
	}
      }
    }

    $self -> {'max_hash'} = \%max_hash;

    if( 0 ) {
      
      # {{{ Create a header
      my @params = ( 'theta', 'omega', 'sigma', 'npomega', 'shrinkage_etas', 'shrinkage_wres','eigen' );
      my @new_header;
      foreach my $category ( @{$self -> {'raw_results_header'}},'npomega' ){
	my @new_names = ();
	foreach my $param ( @params ) {
	  if ( $category eq $param ) {
	    if( defined $max_hash{$param} ){
	      for ( my $i = 1; $i <= $max_hash{ $category }; $i++ ) {
		push ( @new_names, uc(substr($category,0,2)).$i );
	      }
	    }
	    last;
	  }
	  if ( $category eq 'se'.$param ) {
	    if( defined $max_hash{$param} ){
	      for ( my $i = 1; $i <= $max_hash{ $category }; $i++ ) {
		push ( @new_names, uc(substr($category,2,2)).$i );
	      }
	      map ( $_ = 'se'.$_, @new_names );
	    }
	    last;
	  }
	}
	if ( $#new_names >= 0 ) {
	  push( @new_header, @new_names );
	} else {
	  push( @new_header, $category );
	}
	
      }
      
      # }}} 
     
    }

  }		      
end calculate_raw_results_width

# }}} calculate_raw_results_width

# {{{ register_in_database

start register_in_database
  {
    if ( $PsN::config -> {'_'} -> {'use_database'} ) {
      my $dbh = DBI -> connect("DBI:mysql:host=".$PsN::config -> {'_'} -> {'database_server'}.
			       ";databse=".$PsN::config -> {'_'} -> {'project'},
			       $PsN::config -> {'_'} -> {'user'},
			       $PsN::config -> {'_'} -> {'password'},
			       {'RaiseError' => 1});
      my $sth;

      # register modelfit (execute) tool
#      print("INSERT INTO ".$PsN::config -> {'_'} -> {'project'}.
#	    ".execute ( comment ) ".
#	    "VALUES ( '' )");
      $sth = $dbh -> prepare("INSERT INTO ".$PsN::config -> {'_'} -> {'project'}.
			     ".execute ( comment ) ".
			     "VALUES ( 'test' )");
      $sth -> execute;
      $self -> execute_id($sth->{'mysql_insertid'});

      $sth -> finish;
      $dbh -> disconnect;
      tool::register_in_database( $self, execute_id => $self -> execute_id() );

    }
  }
end register_in_database

# }}} register_in_database

# {{{ prepare_raw_results

start prepare_raw_results
      {
	# As of PsN version 2.1.8, we don't handle problems and
	# subproblems in any of the tools but modelfit.

	@{$self -> {'raw_results'}} = sort( {$a->[0] <=> $b->[0]} @{$self -> {'raw_results'}} );

	my %max_hash = %{$self -> {'max_hash'}};

	&{$self -> {'_raw_results_callback'}}( $self, \%max_hash )
	    if ( defined $self -> {'_raw_results_callback'} );

	# ---------------------------  Create a header  ----------------------------

	# {{{ header

	my %param_names;
	my @params = ( 'theta', 'omega', 'sigma' );
	foreach my $param ( @params ) {
	  my $labels = $self -> models -> [0] -> labels( parameter_type => $param );
	  $param_names{$param} = $labels -> [0] if ( defined $labels );
	}

	my @new_header;
	foreach my $name ( @{$self -> {'raw_results_header'}} ) {
	  my $success;
	  my @params = ( 'theta', 'omega', 'sigma', 'npomega', 'shrinkage_etas', 'shrinkage_wres','eigen' );
	  foreach my $param ( @params ) {
	    if ( $name eq $param ){
	      if ( $name eq 'shrinkage_etas' ){
		for ( my $i = 1; $i <= $max_hash{ $name }; $i++ ) {
		  push ( @new_header, 'shrinkage_eta'.$i );
		}
	      } elsif ( $name eq 'shrinkage_wres' ){
		push ( @new_header, 'shrinkage_wres' );
	      } else {
		for ( my $i = 1; $i <= $max_hash{ $name }; $i++ ) {

		  my $label = undef;
		  if( defined $param_names{$name} -> [$i-1] ){
		    $label = $param_names{$name} -> [$i-1] ;
		  }
		  if( defined $label ){
		    push( @new_header, $label );
		  } else {
		    push( @new_header, uc(substr($name,0,2)).$i );
		  }
		}
	      }
	      $success = 1;
	      last;
	    } elsif ( $name eq 'se'.$param ) {
	      for ( my $i = 1; $i <= $max_hash{ $name }; $i++ ) {
		my $label = undef;
		
		if( defined $param_names{$param} -> [$i-1] ){
		  $label = $param_names{$param} -> [$i-1] ;
		}
		if( defined $label ){
		  push( @new_header, 'se' . $label );
		} else {
		  push( @new_header, 'se' . uc(substr($name,2,2)).$i );
		}
	      }
	      $success = 1;
	      last;
	    } 
	  }
	  unless( $success ){
	    push( @new_header, $name );
	  }
	}
      
	$self -> {'raw_results_header'} = \@new_header;

	# }}} header
      }
end prepare_raw_results

# }}} prepare_raw_results

# {{{ print_raw_results

start print_raw_results
{

  ## print raw_results array

  if ( defined $self -> {'raw_results'} ) {

    my $raw_file;

    if( ref $self -> {'raw_results_file'} eq 'ARRAY' ){
      $raw_file = $self -> {'raw_results_file'} -> [0];
    } else {
      $raw_file = $self -> {'raw_results_file'};
    }

    my ($dir,$file) = OSspecific::absolute_path( $self -> {'directory'},
						 $raw_file );

    my $append = $self -> {'raw_results_append'} ? '>>' : '>';
    open( RRES, $append.$dir.$file );
    
    if( (not $self -> {'raw_results_append'}) and $PsN::output_header ){
      print RRES join(',',@{$self -> {'raw_results_header'}} ),"\n";
    }
    
    for ( my $i = 0; $i < scalar @{$self -> {'raw_results'}}; $i++ ) {
      my @result_array = @{$self -> {'raw_results'} -> [$i]};
      map( $_ = defined $_ ? $_ : $PsN::out_miss_data, @result_array );
      print RRES join(',',@result_array ),"\n";
    }
    close( RRES );
  }

  ## print raw_nonp_results
  
  if( defined $self ->{'raw_nonp_results' } ) {
    my ($dir,$file) = OSspecific::absolute_path( $self -> {'directory'},
						 $self -> {'raw_nonp_file'} );
    my $append = $self -> {'raw_results_append'} ? '>>' : '>';
    open( RRES, $append.$dir.$file );
    for ( my $i = 0; $i < scalar @{$self -> {'raw_nonp_results'}}; $i++ ) {
      my @result_array;
      if ( defined $self -> {'raw_nonp_results'} -> [$i] ) {
	@result_array = @{$self -> {'raw_nonp_results'} -> [$i]};
	map( $_ = defined $_ ? $_ : $PsN::out_miss_data, @result_array );
      }
      print RRES join(',',@result_array ),"\n";
    }
    close( RRES );
  }
  
  ## raw_line_structure should be printed to disk here for fast
  ## resumes. In the future
  
  $self -> {'raw_line_structure'} -> write( 'raw_results_structure' );
  
}
end print_raw_results

# }}} print_raw_results

# {{{ create_sub_dir

start create_sub_dir
    {
	my $file;
#	($tmp_dir, $file) = OSspecific::absolute_path( './' .
	($tmp_dir, $file) = OSspecific::absolute_path( $self -> {'directory'}.'/' .
						       $subDir, '');

	unless( -e $tmp_dir ){
	    mkdir( $tmp_dir );
	}
    }
end create_sub_dir

# }}}

# {{{ copy_model_and_input

start copy_model_and_input
    {
      # Fix new short names (i.e. No path)
      my @new_data_names;

      if( defined $model -> datas ){
	foreach my $data ( @{$model -> datas} ) {
	  my $filename = $data -> filename;

	  push( @new_data_names, $filename );
	}
      } else {
	debug -> die( message => 'No datafiles set in modelfile.' );
      }
      
      # Fix new, short names (i.e. No Path)
      my @new_extra_data_names;
      my @problems = @{$model -> problems};
      for ( my $i = 1; $i <= $#problems + 1; $i++ ) {
	my $extra_data = $problems[$i-1] -> extra_data;
	if ( defined $extra_data ) {
	  my $filename = $extra_data -> filename;
	  push( @new_extra_data_names, $filename );
	}
      }

      # Set the table names to a short version 
      my @new_table_names = ();
      my @table_names = @{$model -> table_names( ignore_missing_files => 1 )};
      # Loop the problems
      for ( my $i = 0; $i <= $#table_names; $i++ ) {
	my @new_arr;
	# Loop the table files within each problem
	for ( my $j = 0; $j < scalar @{$table_names[$i]}; $j++ ) {
	  my ( $dir, $filename ) = OSspecific::absolute_path( '.', $table_names[$i][$j] );
	  push( @new_arr,  $filename );
	}
	push( @new_table_names, \@new_arr );
      }


      # Copy input files ( msfo, msfi, subroutines and extra at the
      # moment)

      foreach my $file( @{$model -> input_files} ){

	# $file is a ref to an array with two elements, the first is a
	# path, the second is a name.

	cp( $file->[0] . $file -> [1], $file -> [1] );

      }


      # Copy the model object. Set the new (shorter) data file names.
      # There's no need to physically copy these here since we took care of that above.
      $candidate_model = $model -> copy( filename              => 'psn.mod',
				     data_file_names       => \@new_data_names,
				     extra_data_file_names => \@new_extra_data_names,
				     copy_data             => 1 );

      $candidate_model -> shrinkage_modules( $model -> shrinkage_modules );

      $candidate_model -> register_in_database;
      $model -> flush_data;

      if( $self -> {'handle_msfo'} ){

	# Initialize sequence of msfi/msfo files.

	my $msfo_names = $candidate_model -> msfo_names;
	my $msfi_names = $candidate_model -> msfi_names;
	my $msfi_in;

	if( defined $msfo_names ){
	  $msfi_in = $msfo_names -> [0][0];
	} elsif ( defined $msfi_names ){
	  $msfi_in = $msfi_names -> [0][0];
	}
	  
	if( -e $msfi_in ){
	  mv( $msfi_in, 'psn_msfo-0' );
	  $candidate_model->set_records(type=>'msfi',
					record_strings => ['psn_msfo-0']);
	  $candidate_model->remove_records(type=>'theta');
	  $candidate_model->remove_records(type=>'omega');
	  $candidate_model->remove_records(type=>'sigma');
	  
	} else {
	  # Probably an error, will be caught by NONMEM
	}

	if( scalar @{$candidate_model -> record(record_name => 'estimation')} > 0 ){
	  $candidate_model -> set_option( record_name => 'estimation',
					  option_name => 'MSFO',
					  option_value=> 'psn_msfo' );
	}
	
      }

      my $maxeval = $model -> maxeval -> [0][0];
      if ( $maxeval > 9999 ) {
	$candidate_model -> maxeval( new_values => [[9999]],
				 problem_numbers => [1] );
      }
      
      $candidate_model -> table_names( new_names            => \@new_table_names,
				   ignore_missing_files => 1 );
#      $candidate_model -> clean_extra_data_code;
      $candidate_model -> drop_dropped if ( $self -> {'drop_dropped'} );
      $candidate_model -> wrap_data if ( $self -> {'wrap_data'} );
      $candidate_model -> add_extra_data_code;
      $candidate_model -> write_get_subs;
      $candidate_model -> write_readers;
      $candidate_model -> _write( filename   => 'psn.mod' );# write_data => 1 );  #Kolla denna, den funkar inte utan wrap!!
      $candidate_model -> flush_data;
      $candidate_model -> store_inits;

    }
end copy_model_and_input

# }}}

# {{{ copy_model_and_output

start copy_model_and_output
{
  my $outfilename = $model -> outputs -> [0] -> full_name;

  my ($dir, $model_filename) = OSspecific::absolute_path($model -> directory,
							 $model -> filename );

  # This is used with 'prepend_model_file_name'
  my $dotless_model_filename = $model_filename;
  $dotless_model_filename =~ s/\.[^.]+$//;

  if( $self -> unwrap_table_files() ) {
    if( defined $final_model -> table_names ){
      foreach my $table_files( @{$final_model -> table_names} ){
	foreach my $table_file( @{$table_files} ){

	  open( TABLE, '<'.$table_file );
	  my @table = <TABLE>;
	  close( TABLE );
	  open( TABLE, '>'.$table_file );
	  my ( $j, $cont_column ) = ( 0, -1 );
	  for( my $i = 0; $i <= $#table; $i++ ) {
	    print TABLE $table[$i] and next if( $i == 0 );
	    chomp($table[$i]);
	    my @row = split(' ',$table[$i]);
	    if( $i == 1 ) {
	      for( my $k = 0; $k <= $#row; $k++ ) {
		$cont_column = $k if( $row[$k] eq 'CONT' );
	      }
	    }
	    for( my $k = 0; $k <= $#row; $k++ ) {
	      next if( $k == $cont_column );
	      print TABLE sprintf( "%12s",$row[$k] );
	    }
	    print TABLE "\n";
	  }
	  close( TABLE );
	}
      }
    }
    $final_model -> table_names( new_names => $model -> table_names );
  }

  my @output_files = @{$final_model -> output_files};

  foreach my $filename ( @output_files, 'compilation_output.txt' ){

    my $use_name = $self -> get_retry_name( filename => $filename,
					    retry => $use_run-1 );

    # Copy $use_run files to files without numbers, to avoid confusion.
    cp( $use_name, $filename );

    # Don't prepend the model file name to psn.lst, but use the name
    # from the $model object.
    if( $filename eq 'psn.lst' ){
      cp( $use_name, $outfilename );
      next;
    }

    if( $self -> {'prepend_model_file_name'} ){
      cp( $use_name, $dir . $dotless_model_filename . '.' . $filename );
    } else {
      cp( $use_name, $dir .$filename );
    }

  }

  # TODO check if this is necessary
  my $final_output = output -> new( filename => $outfilename,
				    model_id => $model -> model_id );
  $final_output -> register_in_database( model_id => $model -> model_id,
					 force    => 1 ); # If we are here, the model has been run
  $final_model -> outputs( [$final_output] );


  # Keep files if debugging

  if( 'debug' -> level == 0) {
    unlink 'nonmem', 'nonmem6', 'nonmem5', 
           'nonmem.exe', 'nonmem5_adaptive','nonmem6_adaptive', 'nonmem_adaptive',
           'FDATA';
    # TODO
    # If we delete these, we can't resume /Lasse :
    # (pheno is not a good testing example for this)
    # unlink( @{$model -> datafiles}, @{$model -> extra_data_files} );
  }

  if( $self -> {'clean'} >= 1 and 'debug' -> level == 0 ){
    unlink 'nonmem', 'nonmem'.$self -> {'nm_version'}, 
           'nonmem.exe','FDATA', 'FREPORT', 'FSUBS', 'FSUBS.f', 
           'FSUBS.for', 'LINK.LNK', 'FSTREAM', 'FCON.orig', 'FLIB', 'FCON','PRDERR';
    

    if( defined $final_model -> extra_files ){
      foreach my $x_file( @{$final_model -> extra_files} ){
	my ( $dir, $filename ) = OSspecific::absolute_path( $final_model -> directory,
							    $x_file );
	unlink( $filename );
      }
    }


    if( $self -> {'clean'} >= 2 ){
      for ( my $i = 1; $i <= $self -> {'retries'}; $i++ ) {
	foreach my $filename ( @output_files ){

	  my $use_name = $self -> get_retry_name( filename => $filename,
						  retry => $i-1 );
	  unlink( $use_name );
	}
	
        unlink( "psn-$i.mod" );
	unlink( "compilation_output-$i.txt." );

      }
      unlink( @{$model -> datafiles}, @{$model -> extra_data_files} );
    }
  }

  if ( $self -> {'clean'} >= 3 ) {
    # Do nothing. "run_nonmem" will remove entire work directory
    # before returning.
  } else {
    system('tar cz --remove-files -f nonmem_files.tgz *')
	if ( $self -> {'compress'} and $Config{osname} ne 'MSWin32' );
    system('compact /c /s /q > NUL')
	if ( $self -> {'compress'} and $Config{osname} eq 'MSWin32' );
  }
}
end copy_model_and_output

# }}}

# {{{ get_retry_name

start get_retry_name
{
  $retry++;
  unless( $filename =~ s/\.([^.]+)$/-$retry.$1/ ){
    $filename .= "-$retry";
  }
}
end get_retry_name

# }}}

# {{{ set_msfo_to_msfi

start set_msfo_to_msfi
    {
      
      my $msfo = $self -> get_retry_name( 'filename' => 'psn_msfo',
					  'retry' => $retry );

      my $msfi;

      if( $candidate_model -> outputs -> [0] -> msfo_has_terminated() ){
	
	$msfi = $msfo . '-' . ($queue_info -> {'crashes'}-1);
	
	$candidate_model->remove_records( type => 'estimation' );
	
      } else {

	$msfi = $msfo . '-' . $queue_info -> {'crashes'};
      
      }

      if( -e $msfo ){
	mv( $msfo, $msfi );
      }
      
      $candidate_model->set_records(type=>'msfi',
				    record_strings => [$msfi]);
      
      $candidate_model->remove_records(type=>'theta');
      $candidate_model->remove_records(type=>'omega');
      $candidate_model->remove_records(type=>'sigma');
      $candidate_model->_write;

    }
end set_msfo_to_msfi

# }}}

# {{{ reset_msfo

start reset_msfo
    {
      my @data_ref = @{$candidate_model -> record( record_name => 'msfi' )};
      # Check if there is a msfi record and then delete it
      if (scalar(@data_ref)!=0) {
	$candidate_model->remove_records(type=>'msfi');
	
	# Set the intial values + boundaries to the first  values (update theta, omega, sigma)
	
	my @old_problems = @{$basic_model -> problems};
	my @new_problems = @{$candidate_model -> problems};
	for ( my $i=0; $i <= $#old_problems; $i++ ) {
	  foreach my $param ( 'thetas', 'omegas', 'sigmas' ) {
	    $new_problems[$i] -> $param( Storable::dclone( $old_problems[$i] -> $param ) );
	  }
	}						 
	
	$model_modified = 1;
      }
      
    }
end reset_msfo

# }}}

# {{{ cut_thetas

start cut_thetas
    { 
      $candidate_model -> update_inits( from_output => $output_file,
				    update_omegas => 1,
				    update_sigmas => 1,
				    update_thetas => 1);
      
      foreach my $th_num ( @cutoff_thetas ) {
	my $init_val = $candidate_model ->
	    initial_values( parameter_type    => 'theta',
			    parameter_numbers => [[$th_num]])->[0][0];
	if (abs($init_val)<=$self->{'cutoff'}) {
	  $candidate_model->initial_values(parameter_type => 'theta',
				       parameter_numbers => [[$th_num]],
				       new_values =>[[0]]);
	  $candidate_model->fixed(parameter_type => 'theta',
			      parameter_numbers => [[$th_num]],
			      new_values => [[1]] );
	}
      }
    }
end cut_thetas

# }}}

# {{{ ask_user

start ask_user
    {
      # rand should not be used here. find some other way to create unique file names.
      my $num = rand;
      open( TMP, ">/tmp/$num" );
      print TMP "START MODEL FILE NAME\n";
      print TMP $basic_model -> filename,"\n";
      print TMP "END MODEL FILE NAME\n";
      foreach my $prob ( @reruns ) {
	my @theta_labels = @{$candidate_model -> labels( parameter_type => 'theta' )};
	my @omega_labels = @{$candidate_model -> labels( parameter_type => 'omega' )};
	my @sigma_labels = @{$candidate_model -> labels( parameter_type => 'sigma' )};
	my @theta_inits = @{$candidate_model -> initial_values( parameter_type => 'theta' )};
	my @omega_inits = @{$candidate_model -> initial_values( parameter_type => 'omega' )};
	my @sigma_inits = @{$candidate_model -> initial_values( parameter_type => 'sigma' )};
	print TMP "START PROBLEM NUMBER\n";
	print TMP $prob,"\n";
	print TMP "END PROBLEM NUMBER\n";
	print TMP "START MINIMIZATION_MESSAGE\n";
	print TMP $minimization_message -> [$prob-1][0],"\n";
	print TMP "END MINIMIZATION_MESSAGE\n";
	print TMP "START FINAL GRADIENT\n";
	print TMP join( " ",@{$output_file -> final_gradients -> [$prob-1][0]}),"\n";
	print TMP "END FINAL GRADIENT\n";
	print TMP "START OFV\n";
	print TMP $output_file -> ofv -> [$prob-1][0],"\n";
	print TMP "END OFV\n";
	print TMP "START INITIAL VALUES THETA\n";
	print TMP join(" ", @{$theta_inits[$prob-1]}),"\n";
	print TMP "END INITIAL VALUES THETA\n";
	print TMP "START INITIAL VALUES OMEGA\n";
	print TMP join(" ", @{$omega_inits[$prob-1]}),"\n";
	print TMP "END INITIAL VALUES OMEGA\n";
	print TMP "START INITIAL VALUES SIGMA\n";
	print TMP join(" ", @{$sigma_inits[$prob-1]}),"\n";
	print TMP "END INITIAL VALUES SIGMA\n";
	print TMP "START LABELS\n";
	print TMP join(" ", (@{$theta_labels[$prob-1]},@{$omega_labels[$prob-1]},
			     @{$sigma_labels[$prob-1]})),"\n";
	print TMP "END LABELS\n";
      }
      close( TMP );
      my $out = readpipe( "/users/lasse/PsN/Diagrams/test/scm_comm.pl $num ".$output_file->filename );
      my @out_per_prob = split("\n",$out);
      foreach my $prob ( @reruns ) {
	my ( $choice, $rest ) = split( ' ', shift( @out_per_prob ), 2 );
	if ( $choice == 0 ) {
	  $retries = $tries + $rest;
	} elsif ( $choice == 1 ) {
	  my ($theta_str,$omega_str,$sigma_str) = split(':',$rest);
	  print "thstr $theta_str\n";
	  print "omstr $omega_str\n";
	  print "sistr $sigma_str\n";
	  my @thetas = split( ' ', $theta_str );
	  print "$prob: @thetas\n";
	  $candidate_model -> initial_values( parameter_type  => 'theta',
					  problem_numbers => [$prob],
					  new_values      => [\@thetas],
					  add_if_absent   => 0 );
	  $retries = $tries+1;
	} else {
	  last RETRY;
	}
	$candidate_model -> _write;
      }
      $return_value = $retries;
    }
end ask_user

# }}}

# {{{ umbrella_submit

start umbrella_submit
    {
	if( $prepare_jobs ){

	    my $fsubs = join( ',' , @{$model -> subroutine_files} );
	    my $nm_command = ($PsN::config -> {'_'} -> {'remote_perl'} ? $PsN::config -> {'_'} -> {'remote_perl'} : 'perl') . " -I" .
		$PsN::lib_dir ."/../ " . 
		$PsN::lib_dir . "/nonmem.pm" . 
		" psn.mod psn.lst " . 
		$self -> {'nice'} . " ". 
		$nm_version . " " .
		"1 " .
		"1 " .
		$fsubs . " " .
		$self -> {'nm_directory'};
	    
	    my $directory = $model -> directory();

	    push( @{$self -> {'__umbrella_insert'}}, [$nm_command,$directory] );

	    $queue_info->{$directory}{'candidate_model'} = $model;

	} else {

	    my ($nonmem_insert, $tool_insert, $job_insert);

	    foreach my $row( @{$self -> {'__umbrella_insert'}} ){
		$nonmem_insert .= ',' if( defined $nonmem_insert );
		$nonmem_insert .= "('". $row -> [0] ."')";
	    }

	    my $dbh = DBI -> connect("DBI:mysql:host=".$PsN::config -> {'_'} -> {'database_server'}.
				     ";databse=".$PsN::config -> {'_'} -> {'project'},
				     $PsN::config -> {'_'} -> {'user'},
				     $PsN::config -> {'_'} -> {'password'},
				     {'RaiseError' => 1});

	    my $project = $PsN::config -> {'_'} -> {'project'};
	    $dbh -> do( "LOCK TABLES $project.nonmem WRITE,$project.tool WRITE,$project.job WRITE");

	    my $sth = $dbh -> prepare( "SELECT max(nonmem_id) FROM $project.nonmem" );
	    $sth -> execute or 'debug' -> die( message => $sth -> errstr );

	    my $nonmem_old_max = $sth -> fetch;
	    
	    # register nonmem tool
	    $sth -> finish;
	    my $sth = $dbh -> prepare( "INSERT INTO $project.nonmem ( command ) ".
				       "VALUES $nonmem_insert");

	    $sth -> execute or 'debug' -> die( message => $sth->errstr ) ;
	    $sth -> finish;

	    my $sth = $dbh -> prepare( "SELECT max(nonmem_id) FROM $project.nonmem" );
	    $sth -> execute or 'debug' -> die( message => $sth -> errstr );
	    my $nonmem_new_max = $sth -> fetch;

	    my $tool_insert;

	    for( my $i = ($nonmem_old_max -> [0] + 1); $i <= $nonmem_new_max -> [0]; $i++ ){
		$tool_insert .= ',' if (defined $tool_insert);
		$tool_insert .= "('".$self -> {'tool_id'}."','".$i."','".
		    $self -> {'__umbrella_insert'} -> [ $i-($nonmem_old_max->[0]+1) ]->[1]. "')";
	    }
	    $sth -> finish;

	    my $sth = $dbh -> prepare( "SELECT max(tool_id) FROM $project.tool" );
	    $sth -> execute or 'debug' -> die( message => $sth -> errstr );

	    my $tool_old_max = $sth -> fetch;

	    # register generic tool
	    $sth -> finish;
	    my $sth = $dbh -> prepare( "INSERT INTO $project.tool ( parent_tool_id, nonmem_id, directory ) ".
				       "VALUES $tool_insert" );
	    $sth -> execute or 'debug' -> die( message => $sth->errstr ) ;
	    $sth -> finish;
	    my $sth = $dbh -> prepare( "SELECT max(tool_id) FROM $project.tool" );
	    $sth -> execute or 'debug' -> die( message => $sth -> errstr );

	    my $tool_new_max = $sth -> fetch;

	    my $job_insert;

	    for( my $i = ($tool_old_max -> [0] + 1); $i <= $tool_new_max -> [0]; $i++ ){
		$job_insert .= ',' if (defined $job_insert);
		$job_insert .= "('" . $i . "','1')";
	    }

	    # register job

	    $sth -> finish;
	    my $sth = $dbh -> prepare( "INSERT INTO $project.job (tool_id, state) ".
				       "VALUES $job_insert");
	    $sth -> execute or 'debug' -> die( message => $sth->errstr ) ;

	    
	    $dbh -> do( "UNLOCK TABLES" );

	    $sth -> finish;
	    $dbh -> disconnect;
	}

    }
end umbrella_submit

# }}}

# {{{ lsf_submit

start lsf_submit
    {

      # This method will submit the nonmem.pm file as a script to the
      # LSF system.

      my $fsubs = join( ',' , @{$model -> subroutine_files} );
      
      # Check for vital lsf options.
      unless( $self -> {'lsf_queue'} ){
	if( $PsN::config -> {'_'} -> {'lsf_queue'} ){
	  $self -> {'lsf_queue'} = $PsN::config -> {'_'} -> {'lsf_queue'};
	} else {
	  'debug' -> die( message => 'No queue specified for lsf run' );
	}
      }
      
      foreach my $lsf_option ( 'lsf_project_name', 'lsf_job_name', 'lsf_resources', 'lsf_ttl', 'lsf_options' ){
	unless( $self -> {$lsf_option} ){
	  if( $PsN::config -> {'_'} -> {$lsf_option} ){
	    $self -> {$lsf_option} = $PsN::config -> {'_'} -> {$lsf_option};
	  }
	}
      }

      my ($lsf_out, $lsf_err);
      for( my $i = 1; $i <= 5; $i++ ){
	my $str = "bsub -e stderr -o stdout " .
	    "-q " . $self -> {'lsf_queue'} .
	    ($self -> {'lsf_project_name'} ? " -P " . $self -> {'lsf_project_name'} : ' ') .
	    ($self -> {'lsf_job_name'} ? " -J " . $self -> {'lsf_job_name'} : ' ') .
	    ($self -> {'lsf_ttl'} ? " -c " . $self -> {'lsf_ttl'} : ' ') .
	    ($self -> {'lsf_resources'} ? " -R " . $self -> {'lsf_resources'} : ' ') .
	    $self -> {'lsf_options'} . " \"sleep 3 && " .
	    ($PsN::config -> {'_'} -> {'remote_perl'} ? ' ' . $PsN::config -> {'_'} -> {'remote_perl'} : ' perl ') . " -I" .
	    $PsN::lib_dir ."/../ " . 
	    $PsN::lib_dir . "/nonmem.pm" . 
	    " psn.mod psn.lst " . 
	    $self -> {'nice'} . " ". 
	    $nm_version . " " .
	    1 . ' ' .
	    1 . ' ' .
	    $fsubs . " " .
	    $self -> {'nm_directory'} . "\"";
	
	run3( $str, undef, \$lsf_out, \$lsf_err );
	if ($lsf_out=~/Job \<(\d+)\> is submitted to queue/) {
	  $jobId=$1;
	}

	unless( $lsf_err =~ /System call failed/ or
		$lsf_err =~ /Bad argument/ or
		$lsf_err =~ /Request aborted by esub/ or
		$lsf_err =~ /Bad user ID/ ) {
	  last;
	}

	print "$lsf_err\n";
	if( $lsf_err =~ /Bad argument/ or
	    $lsf_err =~ /Request aborted by esub/ or
	    $lsf_err =~ /Bad user ID/ ) {
	  sleep(($i+1)**2);
	} else {
	  chdir( $work_dir );
	}
	print "bsub command was not successful, trying ",(5-$i)," times more\n";
      }
      sleep(3);
    }
end lsf_submit

# }}}

# {{{ lsf_monitor

start lsf_monitor
    {
      my ($stdout, $stderr);
      run3("bjobs $jobId",undef,\$stdout, \$stderr );
      
      if ($stdout=~/DONE/m) {
	return $jobId; # Return the jobId found.
      }
      
      return 0;
    }
end lsf_monitor

# }}}

# {{{ zink_submit
start zink_submit
{
  require File::Temp;# qw/tempfile tempdir/;
  require Sys::Hostname;
  require LockFile::Simple;# qw/lock unlock trylock/;  # Non-standard module
  
  ###################################################################################################
  ###### JobSpecification code and variables                                           ##############
  ###################################################################################################

  ## Specifies the top level directory of the Zink directory structure. Should be specified via psn.conf
  my $ZinkDir = $PsN::config -> {'_'} -> {'zink_dir'};

  ## Specify the queing directory/Job Specification drop zone. I suggest this is hardcoded and not specifiable in psn.conf.
  my $ZinkJobDir =$ZinkDir."/ZinkJobs";
  
  ## $JobName: Name of job. Default could be model file name.
  ## $JobPriority: Priority of job. Between 0-5 (0=low). Default should be 3.
  ## $ExePath: Directory in which the run is to be executed.
  ## $Command: String with command to be executed, e.g. "nmfe6 run1.mod run1.lst"
  
  my $host = hostname();
  my $fsubs = join( ',' , @{$model -> subroutine_files} );
  my $command = ($PsN::config -> {'_'} -> {'remote_perl'} ? ' ' . $PsN::config -> {'_'} -> {'remote_perl'} : ' perl ') . " -I" .
                 $PsN::lib_dir ."/../ " . 
		 $PsN::lib_dir . "/nonmem.pm" . 
		 " psn.mod psn.lst " . 
		 $self -> {'nice'} . " ". 
		 $nm_version . " " .
		 1 . " " . # compilation
		 1 . " " . # execution
		 $fsubs . " " .
		 $self -> {'nm_directory'};

  my $path = getcwd();
  my $jobname = $queue_info -> {'model'} -> filename;
  my $prio = $self -> {'zink_prio'};
  my ($FH,$jobId) = tempfile("$host-XXXXXXXXXXXXXXXXX",DIR => $ZinkJobDir,SUFFIX=>'.znk');
  lock $jobId;
  print $FH "SUBMITHOST: $host\n";
  print $FH "JOBNAME:  $jobname\n";
  print $FH "PRIORITY: 3\n";
  print $FH "EXEPATH: $path\n";
  print $FH "COMMAND: $command\n";
  close $FH;
  unlock $jobId;

}
end zink_submit
# }}} zink_submit

# {{{ zink_monitor
start zink_monitor
{
  ## Specifies the top level directory of the Zink directory structure. Should be specified via psn.conf
  my $ZinkDir = $PsN::config -> {'_'} -> {'zink_dir'};
  
  ## Specify the queing directory/Job Specification drop zone. I suggest this is hardcoded and not specifiable in psn.conf.
  my $ZinkDoneDir =$ZinkDir."/ZinkDone";

  if( -e "$ZinkDoneDir$jobId" ){
    return $jobId;
  } else {
    return 0;
  }

}
end zink_monitor
# }}} zink_monitor

# {{{ ud_submit

start ud_submit
{
  my $script;
  unless( defined $PsN::config -> {'_'} -> {'ud_nonmem'} ){
    if( $Config{osname} eq 'MSWin32' ) {
      $script = 'nonmem.bat';
    } else {
      $script = 'nonmem.sh';
    }
  } else {
    $script = $PsN::config -> {'_'} -> {'ud_nonmem'};
  }
  
  if( system( "$script -s " . $model -> filename . "> nonmem_sh_stdout"  ) ){
    'debug' -> die( message => "UD submit script failed, check that $script is in your PATH.\nSystem error message: $!" );
  }
  
  open(JOBFILE, "JobId") or 'debug' -> die( message => "Couldn't open UD grid JobId file for reading: $!" );
  $jobId = <JOBFILE>;
  close(JOBFILE);
  
}
end ud_submit

# }}}
    
# {{{ ud_monitor

start ud_monitor
{
# unless( $self -> {'ud_native_retrieve'} ){
  
  my $script;
  unless( defined $PsN::config -> {'_'} -> {'ud_nonmem'} ){
    if( $Config{osname} eq 'MSWin32' ) {
      $script = 'nonmem.bat';
    } else {
      $script = 'nonmem.sh';
    }
  } else {
    $script = $PsN::config -> {'_'} -> {'ud_nonmem'};
  }


  my $stdout; # Variable to store output from "nonmem.bat"
  
  unless( run3( "$script -l " . $jobId, undef, \$stdout ) ){ # run3 will capture the output
    'debug' -> die( message => "UD submit script failed, check that $script is in your PATH.\nSystem error message: $!" );
  }
  my @output_lines = split( /\n/, $stdout ); # I'm splitting the output into an array for easier handling.
  debug -> warn( level => 2,
		 message => "$stdout" );
  foreach my $line( @output_lines ){ # loop over lines
    if( $line =~ /Job State:\s+Completed/ ){ # regexp to find finished jobs.
      debug -> warn( level => 1,
		     message => "Returning $jobId" );
      return $jobId; # Return the jobId found.
    }
  }

  return 0;

# {{{ ud_native_retrieve

#    } else { # ud_native_retrieve
    
#     require SOAP::Lite;             # MGSI SOAP interface
#     require LWP::UserAgent;         # MGSI Fileservice interface
    
#     my %confparams;
    
#     # MGSI constants
#     uduserconf_read_file("./uduserconf", $ENV{HOME} ? ("$ENV{HOME}/uduserconf", "$ENV{HOME}/.uduserconf") : ());
#     my $mgsisoapurl = $confparams{MGSI_SOAP_URL};
#     my $mgsifilesvr = $confparams{MGSI_FILESVR_URL};
#     my $mgsiuser    = $confparams{MGSI_USERNAME};
#     my $mgsipwd     = $confparams{MGSI_PASSWORD};
    
#     # MGSI objects
#     my $server = new SOAP::Lite
# 	-> uri('urn://ud.com/mgsi')
# 	-> proxy($mgsisoapurl);
#     my $ua = new LWP::UserAgent; # mgsi filesvr HTTP object
    
#     ##############################
#     ##  LOG IN TO MGSI SERVER   ##
#     ##############################
#     my $auth = soapvalidate($server->login($mgsiuser, $mgsipwd)); # mgsi authentication token
    
#     ################################
#     ##  RETRIEVE JOB INFORMATION  ##
#     ################################
#     my $job = soapvalidate($server->getJobById($auth, $jobId));
    
#     my $jobstep;
#     my $workunits;
#     do {
#       my $jobsteps = soapvalidate($server->getJobStepsByJob($auth, $$job{job_gid}));
      
#       $jobstep = @$jobsteps[1]; # this job only has one jobstep.
#       my $workunits_array = soapvalidate($server->getWorkunits($auth,
# 							       {job_step_gid_match => [$$jobstep{job_step_gid}]},
# 							       "", 0, -1)); # no ordering, start at record 0, give me all wus
#       $workunits = $$workunits_array{records};
#       my $output_file = $$job{description};
      
#       #print "job $$job{job_gid}; jobstep $$jobstep{job_step_gid}\n" if $mgsidebug;
#       #print "jobstep state is $$jobstep{state_id}\n" if $mgsidebug;
      
#       my $jobstepstatus = soapvalidate($server->getJobStepStatus($auth, $$jobstep{job_step_gid}));
      
#       sleep($self -> {'ud_sleep'});
#     } while( $$jobstep{state_id} != 3 );
    
#     # Retrieve all results by going through every workunit in this job
    
#     foreach my $workunit (@$workunits) {
#       my $results = soapvalidate($server->getResults($auth, {
# 	workunit_gid_match => [$$workunit{workunit_gid}], # filter by workunit guid
# 	success_active => 1, success_value => 1 # only retrieve successful results
# 						     },
# 						     "", 0, 1)); # no ordering, start at record 0, give me 1 result
#       # if you want to actually compare redundant results to see if there is a quorum
#       # here would be a good place to retrieve all results and 'diff' them
#       # for now, I just retrieve 1 results through the queue and go with that
#       if (not exists $$results{records}) {
# 	'debug' -> die( message => 'Found a Workunit without any successful Results, aborting retrieval.' );
	
#       }
#       my $result = $$results{records}[0];
#       my $tempfile = "package.tar"; #mktemp("tmpresXXXXXX");
#       # open(PACKAGE, ">package.tar");
#       my $resulturl = "$mgsifilesvr?auth=$auth&hash=$$result{result_hash}";
#       my $request = HTTP::Request->new('GET', $resulturl);
#       my $response = $ua->request($request, $tempfile);
#       if ($response->is_error() ) {
# 	'debug' -> die( message => "Couldn't retrieve result file, server returned ".$response->status_line );
#       } elsif ($response->header('Content-Length') != -s $tempfile) {
# 	'debug' -> die( message => "Incomplete file returned from server (expected ".$response->header('Content-Length')." but received ".(-s $tempfile).")." );
#       }
      
#       require Archive::Tar;
      
#       my $tar = Archive::Tar->new;
      
#       $tar->read('package.tar',0, {extract => 1});
      
#       if( $Config{osname} ne 'MSWin32' ){
# 	cp('psn.LST','psn.lst');
# 	unlink('psn.LST');
#       }
      
#       # add data to total frequency list
#     }
    
#     # Optional deletion of job
#     #if ($deletejob eq '-d') {
#     #print "now deleting job $jobId...";
#     #soapvalidate($server->deleteJob($auth, $$job{job_gid}));
#     #}
    
#     # helper subroutines
    
#     # read in configuration
#     sub uduserconf_read_file {
#       my @files = @_;
      
#       my $file = undef;
#       foreach (@files) {
# 	if (-f($_)) {
# 	  $file = $_;
# 	  last;
# 	}
#       }
#       defined($file) or 'debug' -> die(message => "Could not find any of: " . join(' ', @files) );
#       open(FH,$file) or 'debug' -> die(message => "Could not open $file: $!");
      
#       # Main parsing loop for the file's contents.
#       while (<FH>) {
# 	if (/^\s*(\w+)\s*=\s*\"([^\"]*)\"/ or /^\s*(\w+)\s*=\s*(\S+)\s*/) {
# 	  $confparams{uc($1)} = $2;
# 	}
#       }
      
#       close(FH);
      
#       foreach ("MGSI_FILESVR_URL",
# 	       "MGSI_SOAP_URL",
# 	       "MGSI_USERNAME",
# 	       "MGSI_PASSWORD")
#       {
# 	if (!defined($confparams{$_})) {
# 	  'debug' -> die (message => "$_ must be defined in $file" );
# 	}
#       }
#     }
    
#     # soap response validation routine
#     sub soapvalidate {
#       my ($soapresponse) = @_;
#       if ($soapresponse->fault) {
# 	'debug' -> die(message => "fault: ", $soapresponse->faultcode, " ", $soapresponse->faultstring );
#       } else {
# 	return $soapresponse->result;
#       }
#     }
#   }

# }}}

}
end ud_monitor

# }}}
    
# {{{ ud_retrieve

start ud_retrieve
{
  my $script;
  unless( defined $PsN::config -> {'_'} -> {'ud_nonmem'} ){
    if( $Config{osname} eq 'MSWin32' ) {
      $script = 'nonmem.bat';
    } else {
      $script = 'nonmem.sh';
    }
  } else {
    $script = $PsN::config -> {'_'} -> {'ud_nonmem'};
  }

  my $subDir = "NM_run".($run_no+1);
  my ($tmp_dir, $file) = OSspecific::absolute_path( $self -> {'directory'}.'/' .
						    $subDir, '');
  if( system("$script -b -c -d ".$tmp_dir." -r $jobId > nonmem_bat_stdout") ){
    'debug' -> die( message => "UD submit script failed.\nSystem error message:$!" );
  }
  
  if( $Config{osname} ne 'MSWin32' ){
    cp($tmp_dir.'/psn.LST',$tmp_dir.'/psn.lst');
    unlink($tmp_dir.'/psn.LST');
  }
}
end ud_retrieve

# }}}

# {{{ sge_submit

start sge_submit
{
  my $fsubs = join( ',' , @{$model -> subroutine_files} );
  if( system( 'qsub -cwd -b y -N ' . 
	      $queue_info -> {'model'} -> filename .
	      ($self -> {'sge_resource'} ? '-l '.$self -> {'sge_resource'}.' ' : ' ') .
	      ($self -> {'sge_queue'} ? '-q '.$self -> {'sge_queue'}.' ' : ' ') .
	      ($PsN::config -> {'_'} -> {'remote_perl'} ? ' ' . $PsN::config -> {'_'} -> {'remote_perl'} : ' perl ') . " -I" .
	      $PsN::lib_dir ."/../ " . 
	      $PsN::lib_dir . "/nonmem.pm" . 
	      " psn.mod psn.lst " . 
	      $self -> {'nice'} . " ". 
	      $nm_version . " " .
	      1 . " " . # compilation
	      1 . " " . # execution
	      $fsubs . " " .
	      $self -> {'nm_directory'} . ' > JobId' ) ){
    'debug' -> die( message => "Grid submit failed.\nSystem error message: $!" );
  }
  
  open(JOBFILE, "JobId") or 'debug' -> die( message => "Couldn't open grid JobId file for reading: $!" );
  while( <JOBFILE> ){
    if( /Your job (\d+)/ ){
      $jobId = $1;
    }
  }
  close(JOBFILE);
}
end sge_submit

# }}}

# {{{ sge_monitor

start sge_monitor
{
  my ($stdout, $stderr);
  run3("qstat -j $jobId 2> JobStat",undef,\$stdout, \$stderr );
  open(JOBFILE, "JobStat") or 'debug' -> die( message => "Couldn't open grid JobStat file for reading: $!" );
  while( <JOBFILE> ){
    if( /Following jobs do not exist:/ ){ # regexp to find finished jobs.
      close(JOBFILE);
      unlink( "JobStat" );
      return $jobId; # Return the jobId found.
    }
  }
  close(JOBFILE);
  unlink( "JobStat" );
  
  return 0;
}
end sge_monitor

# }}}

# {{{ run_nonmem

start run_nonmem
    {
      my $candidate_model = $queue_info -> {'candidate_model'};
      my $tries = $queue_info -> {'tries'};
      # my $compile_only = $queue_info -> {'compile_only'};
      my $model = $queue_info -> {'model'};
      
      # We do not expect any values of rerun lower than 1 here. (a bug otherwise...)
      if( not -e 'psn-' . ( $tries + 1 ) . '.lst' or $self -> {'rerun'} >= 2 ){
	
	# {{{ Execution step 

	if( $self -> {'run_local'} ) {

	  # Normal local execution

	  my $fsubs = join( ',' , @{$candidate_model -> subroutine_files} );

	  my $command_line_options = " -I" .
	      $PsN::lib_dir ."/../ " . 
	      $PsN::lib_dir . "/nonmem.pm" . 
	      " psn.mod psn.lst " . 
	      $self -> {'nice'} . " ". 
	      $nm_version . " " .
	      1 . " " . # compilation
	      1 . " " . # execution
	      $fsubs . " " .
	      $self -> {'nm_directory'} ;

	  if( $Config{osname} eq 'MSWin32' ){

	    # {{{ Windows execution

	    my $perl_bin = ($PsN::config -> {'_'} -> {'perl'} ? $PsN::config -> {'_'} -> {'perl'} : 'C:\Perl\bin\perl.exe');

	    require Win32::Process;
	    require Win32;
	    sub ErrorReport{ print Win32::FormatMessage( Win32::GetLastError() ); }
	    my $proc;
	    Win32::Process::Create($proc,$perl_bin,$perl_bin . $command_line_options,0,$Win32::Process::NORMAL_PRIORITY_CLASS,'.') || die ErrorReport();
	    
	    $queue_info->{'winproc'}=$proc;
	    $queue_map->{$proc->GetProcessID()} = $run_no;

	    # }}}
	    
	  } else { #Asume *nix
	    
	    # {{{ Unix execution

	    my $perl_bin = ($PsN::config -> {'_'} -> {'perl'} ? $PsN::config -> {'_'} -> {'perl'} : ' perl ');

	    my $pid = fork();
	    if( $pid == 0 ){
	      exec( $perl_bin . $command_line_options );
	      exit; # Die Here if exec failed. Probably happens very rarely.
	    }
	    $queue_map->{$pid} = $run_no;

	    # }}}
	    
	  }

	  $queue_info->{'start_time'} = time;

	} elsif( $self -> {'run_on_lsf'} ) {
	  
	  # lsf_submit will call the "nonmem" module that will figure
	  # out that we want to run remotely. If we are also compiling
	  # remotely, it will be done from here as well.
	  
	  my $jobId = $self -> lsf_submit( model => $candidate_model,
					   nm_version => $nm_version,
					   work_dir   => $self -> {'directory'} . "/NM_run$run_no/");
	  
	  $queue_map->{$jobId} = $run_no;
	  
	} elsif( $self -> {'run_on_umbrella'} ) {
	  
	  # umbrella_submit will submit a request for execution
	  # using the umbrella system.
	  #sleep(1);
	  $self -> umbrella_submit( queue_info => $queue_info,
				    model      => $candidate_model,
				    nm_version => $nm_version,
				    prepare_jobs => 1);
	} elsif ( $self -> run_on_ud() ) {
	  debug -> warn( level   => 1,
			 message => "Submitting to the UD system" );  
	  my $jobId = $self -> ud_submit( model => $candidate_model );
	  
	  #$queue_info->{'compile_only'} = 0;
	  $queue_map->{$jobId} = $run_no;
	  
	} elsif ( $self -> run_on_sge() ) {
	  my $jobId = $self -> sge_submit( model => $candidate_model,
					   queue_info => $queue_info,
					   nm_version => $nm_version );
	  
	  #$queue_info->{'compile_only'} = 0;
	  $queue_map->{$jobId} = $run_no;
	} elsif ( $self -> run_on_zink() ) {
	  my $jobId = $self -> zink_submit( model => $candidate_model,
					    queue_info => $queue_info,
					    nm_version => $nm_version );
	  
	  $queue_map->{$jobId} = $run_no;
	}

	# }}}
	
      } elsif( $self -> {'rerun'} >= 1 ){
	
	# We are not forcing a rerun, but we want to recheck the
	# output files for errors. Therefore we put a fake entry in
	# queue_map to trigger "restart_nonmem()". We also need to
	# copy psn-x.lst to psn.lst to make sure that we don't stop
	# the next time we enter run_nonmem"
	
	cp( 'psn-'.($tries+1).'.lst','psn.lst' );
	if( defined $model -> table_names ){
	  foreach my $table_files( @{$model -> table_names} ){
	    foreach my $table_file( @{$table_files} ){
	      cp( $table_file.'-'.( $tries+1 ), $table_file );
	    }
	  }
	}
	if( defined $model -> extra_output() ){
	  foreach my $file( @{$model -> extra_output()} ){
	    cp( $file.'-'.( $tries+1 ), $file );
	  }
	}

	# $queue_info->{'compile_only'} = 0;
	$queue_map->{'rerun_'.$run_no} = $run_no; #Fake pid
      } # end of "not -e psn-$tries.lst or rerun"

    }
end run_nonmem

# }}}

# {{{ restart_needed

start restart_needed
{

  # -------------- Notes about automatic pertubation and retries -----------------
  
  # Automatic pertubation of initial estimates are useful for two
  # purposes. One reason is when nonmem failes to produce a successful
  # minimization. In this case, we can try to direct the search by
  # selecting other estimates. It is also possible to get a successful
  # minimization in a local minima. In this case, we have no way of
  # knowing that it is a local minima without trying other initial
  # estimates. Two modelfit members govern the pertubation process;
  # "retries" which is a number giving the maximum number of retries
  # when nonmem failes and "min_retries" which is the number of runs
  # we want to do to get a global minima. If min_retries is 2 and
  # retries is 5 we will stop after 3 runs if we have reached a
  # successful minimization but continue until 5 if necessary.
  
  # It is important to set $marked_for_rerun to 1 if $tries is
  # incremented!  Otherwise $tries can be incremented twice for
  # one run. The opposite is not true however, for instance a reset
  # of maxevals is not a retry but sets $marked_for_rerun to 1.
  
  # We need the trail of files to select the most appropriate at the end
  # (see copy_model_and_output)
  
  unless( defined $parm{'queue_info'} ){
    # The queue_info must be defined here!
    'debug' -> die( message => "Internal run queue corrupt\n" );
  }  
  my $queue_info_ref = $parm{'queue_info'};
  my $run_results = $queue_info_ref -> {'run_results'};
  my $tries = \$queue_info_ref -> {'tries'};
  my $model = $queue_info_ref -> {'model'};
  my $candidate_model = $queue_info_ref -> {'candidate_model'};
  my $modelfile_tainted = \$queue_info_ref -> {'modelfile_tainted'};

  my $lstsuccess = 0;
  for( my $lsttry = 1; $lsttry <= 5; $lsttry++ ){
    if( -e 'psn.lst' ){

      my ( $output_file );

      if( not -e 'psn-' . ( ${$tries} + 1 ) . '.lst' or $self -> {'rerun'} >= 2 ){

	$output_file = $candidate_model -> outputs -> [0];
	$output_file -> abort_on_fail($self -> {'abort_on_fail'});
	$output_file -> _read_problems;    

	foreach my $filename ( @{$candidate_model -> output_files}, 'psn.mod', 'compilation_output.txt' ){

	  my $new_name = $self -> get_retry_name( filename => $filename,
						  retry => ${$tries} );
	  mv( $filename, $new_name )

	}
	
      } else {

	# This is rerun==1, i.e. re-evaluate the stuff that has been
	# run and (possibly) run extra runs to fix any problems left.
	# In this "else" statement it is true that psn-X.lst exists
	# and we copy it to psn.lst to make it the current version.
	cp( 'psn-'.(${$tries}+1).'.lst', 'psn.lst' );

	$output_file = $candidate_model -> outputs -> [0];
	$output_file -> abort_on_fail($self -> {'abort_on_fail'});
	$output_file -> _read_problems;    	

      }

      # {{{ Create and write intermediate raw results

      open( INTERMED, '>>intermediate_results.csv' );
      open( INTERMEDNONP, '>>intermediate_nonp_results.csv' ); 

      my ($raw_results_row, $nonp_row) = $self -> create_raw_results_rows( max_hash => $self -> {'max_hash'},
									   model => $candidate_model,
									   model_number => $run_no + 1,
									   raw_line_structure => $self -> {'raw_line_structure'} );

      $queue_info_ref -> {'raw_results'} -> [${$tries}] = $raw_results_row;
      $queue_info_ref -> {'raw_nonp_results'} -> [${$tries}] = $nonp_row;

      foreach my $row ( @{$raw_results_row} ){
	print INTERMED join( ',', @{$row} ), "\n";
      }
      
      foreach my $row ( @{$nonp_row} ){
	print INTERMEDNONP join( ',', @{$nonp_row} ), "\n";
      }

      close( INTERMED );
      close( INTERMEDNONP );

      # }}}

      # {{{ Check for minimization successfull an try to find out if lst file is truncated

      my ( $minimization_successful, $minimization_message );

      if( $output_file -> parsed_successfully() and
	  not defined $output_file -> problems ){
	# This should not happen if we are able to parse the output file correctly
	$run_results -> [${$tries}] -> {'failed'} = 1;
	return(0);
      }

      if( not $output_file -> parsed_successfully() ){
	
	if( $self -> {'handle_crashes'} and $queue_info_ref -> {'crashes'} < $self -> crash_restarts() ) {

	  if( $self -> {'max_runtime'} > 0 
	      and 
	      (time() > $queue_info_ref -> {'start_time'} + $self -> {'max_runtime'})  ){
	    $output_file -> flush;
	    return 0;
	  }
	
	  # If the output file could not be parsed successfully, this is
	  # a sign of a crashed run. This is not a NONMEM error as such
	  # but probably an operating system problem. To handle this, we
	  # mark this for rerunning but do not increase the $tries
	  # variable but instead increase $crashes and check whether
	  # this value is below or equal to $crash_restarts.
	  debug -> warn( level => 1,
			 message => "Restarting crashed run ".
			 $output_file -> full_name().
			 "\n".$output_file -> parsing_error_message() );
	  
	  
	  $queue_info_ref -> {'crashes'}++;
	  
	  my $message = "\nModel in NM_run".($run_no+1)." crashed, try nr ". ($queue_info_ref -> {'crashes'} );
	  ui -> print( category => 'all',  message  => $message,
		       newline => 1);

	  
	  if( $self -> {'handle_msfo'} ){
	    $self -> set_msfo_to_msfi( candidate_model => $candidate_model,
				       retry => ${$tries},
				       queue_info => $queue_info_ref);
	  } else {
	    cp( 'psn-'.(${$tries}+1).'.mod', 'psn.mod' );
	  }
	  
	  mv( 'psn-'.(${$tries}+1).'.lst', 'psn_crash-'.(${$tries}+1).'-'.$queue_info_ref -> {'crashes'}.'.lst' );

	  $output_file -> flush;

	  return(1); # Return a one (1) to make run() rerun the
		     # model. By returning here, we avoid the
		     # perturbation of the initial estimates later on in
		     # this method.
	} else {
	  my $message = "\nModel in NM_run".($run_no+1)." crashed ".(($queue_info_ref -> {'crashes'}+1)." times. Not restarting." );
	  ui -> print( category => 'all',  message  => $message,
		       newline => 1);

	  $output_file -> flush;
	  
	  return(0);
	}
      }
	
      # If the output file was parsed successfully, we (re)set the $crashes
      # variable and continue
      
      $queue_info_ref -> {'crashes'} = 0;

      $minimization_successful = $output_file -> minimization_successful();
      $minimization_message    = $output_file -> minimization_message();

      unless( defined $minimization_successful ) {
	debug -> die( message => "No minimization status found in " . $output_file ->filename );
      }
      
      # {{{ log the stats of this run

      foreach my $category ( 'minimization_successful', 'covariance_step_successful',
			     'covariance_step_warnings', 'estimate_near_boundary',
			     'significant_digits', 'ofv' ){
	my $res = $output_file -> $category;
	$run_results -> [${$tries}] -> {$category} = defined $res ? $res -> [0][0] : undef;
      }
      $run_results -> [${$tries}] -> {'pass_picky'} = 0;

      # }}}

      # }}}

      # {{{ Check if maxevals is reached and copy msfo to msfi

      if ( not $marked_for_rerun and $handle_maxevals ) {
	
	for ( @{$minimization_message -> [0][0]} ) {
	  if ( /\s*MAX. NO. OF FUNCTION EVALUATIONS EXCEEDED\s*/) {

	    my $queue_info_ref -> {'evals'} += $output_file -> feval -> [0][0];
	    my $maxeval = $model -> maxeval -> [0][0];

	    if( $maxeval > $queue_info_ref -> {'evals'} ){
	      $self -> set_msfo_to_msfi( candidate_model => $candidate_model );
	      
	      $candidate_model -> _write;
	      ${$modelfile_tainted} = 1;
	      $marked_for_rerun = 1;
	      last;
	    }
	  }
	}
      }

      # }}}
      
      # {{{ Check for rounding errors and/or hessian_npd messages

      if ( not $marked_for_rerun and $handle_rounding_errors || $handle_hessian_npd) {
	my $round_rerun;
	my $hessian_rerun;
	for ( @{$minimization_message -> [0][0]} ) {
	  $round_rerun = 1 and last if ( /\s*DUE TO ROUNDING ERRORS\s*/);
	  $hessian_rerun = 1 and last if ( /\s*NUMERICAL HESSIAN OF OBJ. FUNC. FOR COMPUTING CONDITIONAL ESTIMATE IS NON POSITIVE DEFINITE\s*/);
	}
	
	if ( ($round_rerun && $handle_rounding_errors) or ($hessian_rerun && $handle_hessian_npd)) {
	  
	  if( $self -> {'use_implicit_msfo'} ) {
	    $self -> reset_msfo( basic_model => $model,
				 candidate_model => $candidate_model );
	  }
	  
	  $self -> cut_thetas( candidate_model => $candidate_model,
			       cutoff_thetas => \@cutoff_thetas,
			       output_file => $output_file );
	  
	  $candidate_model -> _write;
	  ${$modelfile_tainted} = 1;
	  #print "ROUND\n";
	  $marked_for_rerun = 1;
	  
	  ${$tries} ++; # This is a precaution, handle_rounding and handle_hessian should have
	  # their own termination checks
	}
	
      }

      # }}}      

      # {{{ Check for failed problems and possibly check for picky errors.

      if ( not $marked_for_rerun and $tweak_inits ) {
	
	my @reruns;
	my @problems = @{$candidate_model -> problems};
	for ( my $problem = 1; $problem <= scalar @problems; $problem++ ) {
	  unless( $candidate_model -> is_simulation( problem_number => $problem ) ){
	    if ( $minimization_successful -> [$problem-1][0] ) {
	      if ( $picky ) {
		$run_results -> [${$tries}] -> {'pass_picky'} = 1;
		for ( @{$minimization_message -> [$problem-1][0]} ) {
		  if ( /0COVARIANCE STEP ABORTED/ or
		       /0PROGRAM TERMINATED BY OBJ/ or
		       /0ESTIMATE OF THETA IS NEAR THE BOUNDARY AND/ or
		       /0PARAMETER ESTIMATE IS NEAR ITS BOUNDARY/ or
		       /0R MATRIX ALGORITHMICALLY SINGULAR/ or
		       /0S MATRIX ALGORITHMICALLY SINGULAR/ ) {
		    push( @reruns, $problem );
		    $run_results -> [${$tries}] -> {'pass_picky'} = 0;
		    last;
		  }
		}
	      }
	    } else {
	      my $significant_digits = $output_file -> significant_digits;
	      if ( not ( $significant_digits_rerun and $significant_digits -> [$problem-1][0] > $significant_digits_rerun ) ) {
		push( @reruns, $problem );
	      }
	    }
	  }
	}		
	
	if( ${$tries} < ($retries -1) and scalar @reruns > 0 ) {
	  $marked_for_rerun = 1;
	  ${$tries} ++;
	  
	  if( ${$tries} >= $self -> {'min_retries'} and $self -> {'verbose'} ){
	    my $message = "R:".($run_no+1).":". (${$tries}+1) . " ";
	    ui -> print( category => 'all',  message  => $message,
			 newline => 0);
	  }
	  
	  if( $self -> {'ask_if_fail'} ) {
	    $retries = $self -> ask_user( basic_model => $model,
					  candidate_model => $candidate_model,
					  reruns => \@reruns,
					  minimization_message => $minimization_message,
					  output_file => $output_file,
					  retries => $retries,
					  tries => ${$tries} );
	    $candidate_model->_write;
	    ${$modelfile_tainted} = 1;
	  } else {
	    
	    # This code must be adjusted for multiple problems!!
	    my $degree = 0.1*${$tries};
	    if( $self -> {'handle_msfo'} ) {
	      $self -> reset_msfo( basic_model => $model,
				   candidate_model => $candidate_model );
	      
	      foreach my $prob ( @reruns ) {
		$problems[$prob-1] -> set_random_inits ( degree => $degree );
	      }
	      
	      $candidate_model->_write;
	      ${$modelfile_tainted} = 1;
	      
	    } else {
	      foreach my $prob ( @reruns ) {
		$problems[$prob-1] -> set_random_inits ( degree => $degree );
	      }
	      
	      $candidate_model->_write;
	      ${$modelfile_tainted} = 1;
	      # The fcon code does not parse the dofetilide model correctly
# 		my $fcon = fcon -> new( filename => 'FCON.orig' );
# 		$fcon -> parse;
# 		$fcon -> pertubate_all( fixed_thetas => $final_model -> fixed( 'parameter_type' => 'theta' ),
# 					fixed_omegas => $final_model -> fixed( 'parameter_type' => 'omega' ),
# 					fixed_sigmas => $final_model -> fixed( 'parameter_type' => 'sigma' ),
# 					degree       => $degree );
# 		$fcon -> write( filename => 'FCON' );
	    }
	  }
	}
      }

      # }}}

      # {{{ Perturb estimates if min_retries not reached

      # This "if" block should conceptually be last, since it is
      # something we should only do if the model succeeds. In
      # practise it could swap places with at least the tweak inits
      # block, but for simplicities sake, lets leave it at the
      # bottom.
      
      if( not $marked_for_rerun and ${$tries} < $self -> {'min_retries'} ) {
	#Here we force pertubation when the model is successful.
	
	${$tries} ++;
	$marked_for_rerun = 1;
	my $degree = 0.1 * ${$tries};
	if( $self -> {'use_implicit_msfo'} and
	    $self -> reset_msfo( basic_model => $model,
				 candidate_model => $candidate_model ) ){
	  
	  foreach my $prob ( @{$candidate_model -> problems} ) {
	    $prob -> set_random_inits ( degree => $degree );
	  }
	  
	  $candidate_model->_write;
	  
	} else {
	  foreach my $prob ( @{$candidate_model -> problems} ) {
	    $prob -> set_random_inits ( degree => $degree );
	  }
	  
	  $candidate_model->_write;
	}
	
	${$modelfile_tainted} = 1;
      }

      # }}}

      $output_file -> flush;
      
      $lstsuccess = 1; # We did find the lst file.
      last;
    } else {
      sleep(($lsttry+1)**2);
      # print " The lst-file is not present, trying ".(5-$lsttry)." times more\n";
    } # Did the lst file exist?
  } # The loop trying to read the lst file
  unless( $lstsuccess ) { # psn.lst doesn't exist.
    $run_results -> [${$tries}] -> {'failed'} = 1;
  }
  
}
end restart_needed

# }}}

# {{{ select_best_model

start select_best_model      
{
  # -------------- Notes about Final model selection -----------------
  
  # Since we have reruns with pertubation and now also forced (or
  # automatic) pertubation the final model is not equal to the
  # original model. We consider four implicit subsets. Those that pass
  # the picky test, those that don't pass the picky test but have
  # minimization successful, those that don't pass the minimization
  # step but produce an ofv and, finaly, those that doesn't produce an
  # ofv. The final model will be the model that passes the most tests
  # and have the lowest ofv value, and if no ofv value is produced, it
  # will be the basic model.
      
  # Get all runs that passed the picky test (if picky is used)
  
  # The order of categories is important. Highest priority goes last.

  unless( defined $parm{'queue_info'} ){
    # The queue_info must be defined here!
    'debug' -> die( message => "Internal run queue corrupt\n" );
  }
  my $queue_info_ref = $parm{'queue_info'};
  my $run_results = $queue_info_ref -> {'run_results'};
  my $model = $queue_info_ref -> {'model'};
  my $candidate_model = $queue_info_ref -> {'candidate_model'};

  my @selection_categories = ('really_bad','terminated','normal','picky');
  my ( %selection, $selected );
  foreach my $category ( @selection_categories ) {
    $selection{$category}{'best_significant_digits'} = 0;
  }
  
  # First pass to get lowest OFV's
  $selection{'normal'}{'lowest_OFV'} = 999999999;
  $selection{'terminated'}{'lowest_OFV'} = 999999999;
  $selection{'really_bad'}{'lowest_OFV'} = 999999999;
  for(my $i = 0; $i < scalar @{$run_results}; $i++ ){
    if ( $run_results -> [$i] -> {'minimization_successful'} ) {
      if( defined( $run_results -> [$i] -> {'ofv'} ) ) {
	$selection{'normal'}{'lowest_OFV'} = $run_results -> [$i] -> {'ofv'} < $selection{'normal'}{'lowest_OFV'} ?
	    $run_results -> [$i] -> {'ofv'} : $selection{'normal'}{'lowest_OFV'};
      }
    }
    if( defined( $run_results -> [$i] -> {'ofv'} ) ) {
      if( defined( $run_results -> [$i] -> {'significant_digits'} ) ) {
	$selection{'terminated'}{'lowest_OFV'} = $run_results -> [$i] -> {'ofv'} < $selection{'terminated'}{'lowest_OFV'} ?
	    $run_results -> [$i] -> {'ofv'} : $selection{'terminated'}{'lowest_OFV'};
      }
      $selection{'really_bad'}{'lowest_OFV'} = $run_results -> [$i] -> {'ofv'} < $selection{'really_bad'}{'lowest_OFV'} ?
	  $run_results -> [$i] -> {'ofv'} : $selection{'really_bad'}{'lowest_OFV'};
    }
  }
  
  my $accepted_OFV_diff = 5;
  
  # Loop through all categories, the order is not important here
  for(my $i = 0; $i < scalar @{$run_results}; $i++ ){
    foreach my $category ( @selection_categories ) {
      
      if( $category eq 'picky' ) {
	
	if ( $run_results -> [$i] -> {'pass_picky'} ) {
	  if( $run_results -> [$i] -> {'significant_digits'} >
	      $selection{$category}{'best_significant_digits'} and
	      $run_results -> [$i] -> {'ofv'} < ($selection{'normal'}{'lowest_OFV'}+$accepted_OFV_diff) ){
	    $selection{$category}{'selected'} = ($i+1);
	    $selection{$category}{'best_significant_digits'} = $run_results -> [$i] -> {'significant_digits'};
	  }
	}
      } elsif( $category eq 'normal' ) {
	
	if ( $run_results -> [$i] -> {'minimization_successful'} ) {
	  if( $run_results -> [$i] -> {'significant_digits'} >
	      $selection{$category}{'best_significant_digits'} and
	      $run_results -> [$i] -> {'ofv'} < ($selection{'normal'}{'lowest_OFV'}+$accepted_OFV_diff) ){
	    $selection{$category}{'selected'} = ($i+1);
	    $selection{$category}{'best_significant_digits'} = $run_results -> [$i] -> {'significant_digits'};
	  }
	}
      } elsif( $category eq 'terminated' ) {
	if ( defined( $run_results -> [$i] -> {'ofv'} ) and
	     $run_results -> [$i] -> {'ofv'} < ($selection{'terminated'}{'lowest_OFV'}+$accepted_OFV_diff) ) {
	  if( defined( $run_results -> [$i] -> {'significant_digits'} ) ) {
	    if ( $run_results -> [$i] -> {'significant_digits'} >
		 $selection{$category}{'best_significant_digits'} ){
	      $selection{$category}{'selected'} = ($i+1);
	      $selection{$category}{'best_significant_digits'} = $run_results -> [$i] -> {'significant_digits'};
	    }
	  }
	}
      } else {
	if ( defined( $run_results -> [$i] -> {'ofv'} ) and
	     $run_results -> [$i] -> {'ofv'} < ($selection{'really_bad'}{'lowest_OFV'}+$accepted_OFV_diff) ) {
	  $selection{$category}{'selected'} = ($i+1);
	  $selection{$category}{'best_significant_digits'} = $run_results -> [$i] -> {'significant_digits'};
	}
      }
    }
  }
  
  # Loop through all categories from less strict to strict and
  # replace the selected run as we find better runs. (I know that
  # this is a bit awkward but it is working.)
  foreach my $category ( @selection_categories ) {
    $selected = defined $selection{$category}{'selected'} ?
	$selection{$category}{'selected'} : $selected;
  }
  $selected = defined $selected ? $selected : 1;
  
  open( STAT, '>stats-runs.csv' );
  print STAT Dumper \@{$run_results};
  print STAT "Selected $selected\n";
  close( STAT );

  unless( $run_results -> [$selected-1] -> {'failed'} ){

    my @raw_results_rows = @{$queue_info_ref -> {'raw_results'} -> [$selected-1]};
    
    foreach my $row ( @raw_results_rows ){
      shift( @{$row} );
      unshift( @{$row}, $run_no+1 );
    }


    push( @{$self -> {'raw_results'}}, @raw_results_rows );
    push( @{$self -> {'raw_nonp_results'}}, @{$queue_info_ref -> {'raw_nonp_results'} -> [$selected-1]} );
    
    
    $self -> copy_model_and_output( final_model   => $candidate_model,
				    model         => $model,
				    use_run       => $selected ? $selected : '' );
    
  }

  if( $self -> {'nonparametric_etas'} and 
      ( not -e 'np_etas.lst' or $self -> {'rerun'} >=2 )  ) {
    
    # ---------------------  Create nonp eta model  -----------------------------
    
    # {{{ nonp eta model

    if( not -e 'np_etas.lst' or $self -> {'rerun'} >= 2 ){
      
      ui -> print( category => 'execute',
		   message  => 'Creating NPETA model' );
      
      my $np_eta_mod = $candidate_model ->
	  copy( filename    => $self -> {'directory'}.
		'NM_run'.($run_no+1).'/np_etas.mod',
		target      => 'mem',
		copy_data   => 0,
		copy_output => 0);
      
      my ( $msfo_ref, $junk ) = $candidate_model ->
	  _get_option_val_pos( name            => 'MSFO',
			       record_name     => 'estimation' );
      # We should have an MSFO file here
      for( my $i = 0; $i < scalar @{$msfo_ref}; $i++ ) {
	my $msfi = $msfo_ref->[$i][0];
	$np_eta_mod -> set_records( problem_numbers => [($i+1)],
				    type            =>'msfi',
				    record_strings  => ["$msfi"]); 
	$np_eta_mod -> set_records( problem_numbers => [($i+1)],
				    type           => 'nonparametric',
				    record_strings => [ 'ETAS UNCONDITIONAL '.
							'MSFO=npmsfo'.($i+1) ] );
      }
      $np_eta_mod -> remove_option( record_name => 'estimation',
				    option_name => 'MSFO' );
      $np_eta_mod -> remove_records( type => 'theta' );
      $np_eta_mod -> remove_records( type => 'omega' );
      $np_eta_mod -> remove_records( type => 'sigma' );
      $np_eta_mod -> remove_records( type => 'table' );
      my @nomegas = @{$candidate_model -> nomegas};
      
      my @max_evals;
      for( my $i = 0; $i <= $#nomegas; $i++ ) {
	my $marg_str = 'ID';
	for( my $j = 1; $j <= $nomegas[$i]; $j++ ) {
	  $marg_str = $marg_str.' ETA'.$j;
	}
	$marg_str = $marg_str.' FILE='.$model -> filename.'.nonp_etas'.
	    ' NOAPPEND ONEHEADER NOPRINT FIRSTONLY';
	$np_eta_mod -> add_records( problem_numbers => [($i+1)],
				    type            => 'table',
				    record_strings  => [ $marg_str ] );	  
	$max_evals[$i] = [0];
      }
      
#	my @table_names = @{$np_eta_mod -> table_names};
#	for ( my $i = 0; $i <= $#table_names; $i++ ) {
#	  foreach my $table ( @{$table_names[$i]} ) {
#	    $table = 'nonp_'.$table;
#	  }
#	}
#	$np_eta_mod -> table_names( new_names => \@table_names );
      
      $np_eta_mod -> maxeval( new_values => \@max_evals );
      $np_eta_mod -> _write;

      # }}} nonp eta model
      
      # ----------------------  run nonp eta model  -------------------------------
      
      # {{{ run eta model

      ui -> print( category => 'execute',
		   message  => 'Running NPETA model' );
      
      my $nonmem_object = nonmem -> new( adaptive => $self -> {'adaptive'},
				      modelfile => 'np_etas.mod',
				      version => $nm_version,
				      nm_directory => $self -> {'nm_directory'},
				      nice => $self -> {'nice'},
				      show_version => not $self -> {'run_on_lsf'});
      
      $nonmem_object -> fsubs( $np_eta_mod -> subroutine_files );
      unless( $nonmem_object -> compile() ){
	debug -> die( message => "NONMEM compilation failed:\n" .$nonmem_object -> error_message );
      }
      if( $nonmem_object -> nmtran_message =~ /WARNING/s and $self -> {'verbose'} ){
	ui -> print(category => 'all',
		    message => "NONMEM Warning: " . $nonmem_object -> nmtran_message );
      }
      $nonmem_object -> execute();
      
      foreach my $table_files( @{$np_eta_mod -> table_names} ){
	foreach my $table_file( @{$table_files} ){
	  my $dir = $model -> directory;
	  cp( $table_file, $dir  );
	}
      }
      
      unlink 'nonmem', 'nonmem6', 'nonmem5','nonmem.exe', 'nonmem6_adaptive', 'nonmem5_adaptive';
    }

    # }}} run eta model
    
  }
}
end select_best_model

# }}}

# {{{ print_finish_message

start print_finish_message
    {
      my $ui_text;
      # Log the run
      $ui_text .= sprintf("%3s",$run+1) . sprintf("%25s",$self -> {'models'} -> [$run] -> filename);
      my $log_text = $run+1 . ',' . $self -> {'models'} -> [$run] -> filename . ',';
      if( $self -> {'verbose'} or $self -> {'quick_summarize'} ){
	foreach my $param ( 'ofv', 'covariance_step_successful', 'minimization_message' ) {
	  if( $param eq 'minimization_message' ){
	    $ui_text .= "\n    ---------- Minimization Message ----------\n";
	  }
	  if( defined $candidate_model ){
	    my $ests = $candidate_model -> outputs -> [0] -> $param;
	    # Loop the problems
	    for ( my $j = 0; $j < scalar @{$ests}; $j++ ) {
	      if ( ref( $ests -> [$j][0] ) ne 'ARRAY' ) {
		$ests -> [$j][0] =~ s/^\s*//;
		$ests -> [$j][0] =~ s/\s*$//;
		$log_text .= $ests -> [$j][0] .',';
		#chomp($ests -> [$j][0]);
		$ui_text .= sprintf("%10s",$ests -> [$j][0]);
	      } else {
		
		# Loop the parameter numbers (skip sub problem level)
		for ( my $num = 0; $num < scalar @{$ests -> [$j][0]}; $num++ ) {
		  #$ests -> [$j][0][$num] =~ s/^\s*//;
		  #$ests -> [$j][0][$num] =~ s/\s*$/\n/;
		  $log_text .= $ests -> [$j][0][$num] .',';
		  #chomp($ests -> [$j][0][$num]);
		  if( $param eq 'minimization_message' ){
		    $ui_text .= "    ";
		  }
		  $ui_text .= sprintf("%12s",$ests -> [$j][0][$num]);
		}
	      }
	    }
	  }
	  if( $param eq 'minimization_message' ){
	    $ui_text .= "    ------------------------------------------\n\n";
	  }
	}
	ui -> print( category => 'all',
		     message  => $ui_text,
		     wrap     => 0,
		     newline => 0); 
      }	

      open( LOG, ">>".$self -> {'logfile'} );
      print LOG $log_text;
      print LOG "\n";
      close LOG;

    }
end print_finish_message

# }}}

# {{{ run

start run
    {

      my $cwd = getcwd();
      chdir( $self -> {'directory'} );
      
      # {{{ sanity checks

      my @models;
      if ( defined $self -> {'models'} ) {
	@models = @{ $self -> {'models'} };
      } else {
	debug -> die( message => "Have no models!" );
      }
      
      my $threads = $self -> {'threads'};
      $threads = $#models + 1 if ( $threads > $#models + 1); 

      # Unless we store "run_no" in the database umbrella can't be parallel.

      $threads = 1 if $self -> {'run_on_umbrella'};

      # }}}
      
      # {{{ print starting messages 

      ui -> print( category => 'all',
		   message  => 'Starting ' . scalar(@models) . ' NONMEM executions. '. $threads .' in parallel.' ) 
	  unless $self -> {'parent_threads'} > 1;
      ui -> print( category => 'all',
		   message  => "Run number\tModel name\tOFV\tCovariance step successful." )  if $self -> {'verbose'};

      # }}}
      
      # {{{ Print model-NM_run translation file

      open( MNT, ">model_NMrun_translation.txt");
      for ( my $run = 0; $run <= $#models; $run ++ ) {
	print MNT sprintf("%-40s",$models[$run]->filename),"NM_run",($run+1),"\n";
      }
      close( MNT );

      # }}}
      
      # {{{ Setup of moshog

      my $moshog_client;
      
      if( $self -> {'adaptive'} and $self -> {'threads'} > 1 ) {
	
	# Initiate the moshog client
	$moshog_client = moshog_client -> new(start_threads => $self -> {'threads'});

      }
      # }}}
      
      # {{{ Local execution

      # %queue_map is a mapping from nonmem.pm pID to run number.
      
      my %queue_map;

      # %queue_info is keyed on run number and contains information
      # about each nonmem run.

      my %queue_info;

      # @queue is an array of NM_run directory numbers. If "X" is in
      # @queue, then psn.mod in "NM_runX" is scheduled to be run. It
      # initialized to all models in the tool. Note that if X is in
      # the queue, it doesn't mean NM_runX exists.

      my @queue = (0..$#models);
      my $all_jobs_started = 0;

      # We loop while there is content in the queue (which shrinks and grows)
      # and while we have jobs running (represented in the queue_info)

      my $poll_count = 0;
      while( @queue or (scalar keys %queue_map > 0) ){
	$poll_count++;
	# We start by looking for jobs that have been started and
	# finished. If we find one, we set $pid to that job.
	
	my $pid = 0;

	# {{{ Get potiential finished pid

	foreach my $check_pid( keys %queue_map ){

	  if( $check_pid =~ /^rerun_/ ){

	    # A pid that starts with "rerun" is a rerun and is always
	    # "finished".
	    
	    $pid = $check_pid;
	    last;
	  }

	  # Diffrent environments have diffrent ways of reporting
	  # job status. Here we check which environment we are in
	  # and act accordingly.
	    
	  if( $self -> {'run_on_ud'} ){
	    
	    $pid = $self -> ud_monitor( jobId => $check_pid );
	    
	    if( $pid ){
	      $self -> ud_retrieve( jobId => $check_pid,
				    run_no => $queue_map{$check_pid} );
	    }
	      
	  } elsif( $self -> {'run_on_sge'} ) {
	      
	    $pid = $self -> sge_monitor( jobId => $check_pid );
	    
	  } elsif( $self -> {'run_on_zink'} ) {

	    $pid = $self -> zink_monitor( jobId => $check_pid );

	  } elsif( $self -> {'run_on_lsf'} ) {
	    
	    $pid = $self -> lsf_monitor( jobId => $check_pid );

	  } else { # Local process poll

	    if( $Config{osname} eq 'MSWin32' ){

	      my $exit_code;

	      # GetExitCode is supposed to return a value indicating
	      # if the process is still running, however it seems to
	      # allways return 0. $exit_code however is update and
	      # seems to be nonzero if the process is running.

	      $queue_info{$queue_map{$check_pid}}{'winproc'}->GetExitCode($exit_code);
	      
	      if( $exit_code == 0 ){
		$pid = $check_pid;
	      }

	    } else {

	      $pid = waitpid($check_pid,WNOHANG);
	      
	      # Waitpid will return $check_pid if that process has
	      # finished and 0 if it is still running.
	      
	      if( $pid == -1 ){
		# If waitpid return -1 the child has probably finished
		# and has been "Reaped" by someone else. We treat this
		# case as the child has finished. If this happens many
		# times in the same NM_runX directory, there is probably
		# something wrong and we die(). (I [PP] suspect that we
		# never/seldom reach 10 reruns in one NM_runX directory)
		
		my $run = $queue_map{$check_pid};
		
		$queue_info{$run}{'nr_wait'}++;
		if( $queue_info{$run}{'nr_wait'} > 10 ){
		  debug -> die(message => "Nonmem run was lost\n");
		}
		$pid = $check_pid;
	      }
	      
	      # If the pid is not set, the job is not finished and we
	      # check if it has been running longer than it should, and we
	      # kill it.
	      unless( $pid ){
		if( $self -> {'max_runtime'} 
		    and 
		    time > $queue_info{$queue_map{$check_pid}}->{'start_time'} + $self -> {'max_runtime'}  ){
		  
		  # Only works on windows
		  
		  if( $Config{osname} ne 'MSWin32' ){
		    
		    print "Job ", $check_pid, " exceeded run time, trying to kill\n";
		    my $try=1;
		    while( kill( 0, $check_pid ) ){
		      if( $try <= 5 ){
			kill( 'TERM', $check_pid );
		      } else {
			kill( 'KILL', $check_pid );
		      }
		      waitpid( $check_pid, 0);
		      sleep(1);
		    }
		    print "Job ", $check_pid, " killed successfully on try nr $try\n";
		  }
		}
	      }
	    }
	  }
	  
	  last if $pid;

	}

	# }}}
	
	if( not $pid ){

	  # No process has finished. 

	  # {{{ Return to polling if queue is empty or we have max number of jobs running.

	  if( (scalar @queue == 0) or scalar keys %queue_map >= $threads ){

	    # In that case we should not start another job. (Also
	    # sleep to make polling less demanding).
	   
	    if( defined $PsN::config -> {'_'} -> {'job_polling_interval'} and
		$PsN::config -> {'_'} -> {'job_polling_interval'} > 0 ) {
	      sleep($PsN::config -> {'_'} -> {'job_polling_interval'});
	    } else {
	      sleep(1);
	    }
	    my $make_request = 0;
	    if ( defined $PsN::config -> {'_'} -> {'polls_per_moshog_request'} and
		 $self -> {'adaptive'} and
		 $self -> {'threads'} > 1 and
		 not ( $poll_count % $PsN::config -> {'_'} -> {'polls_per_moshog_request'} ) ) {
	      $make_request = 1;
	    }

	    if( $make_request and
		scalar @queue > 0 ) {

	      # Make a moshog request.
	      
	      $moshog_client -> request( request => scalar @queue );
	      $threads += $moshog_client -> granted();
	      
	    }
	    
	    next; # Return to polling for finished jobs.
	  }

	  # }}}
	  
	  # This is where we initiate a new job:

	  my $run = shift( @queue );
	  
	  # {{{ check for no run conditions. (e.g. job already run)

	  if ( -e $self -> {'models'} -> [$run] -> outputs -> [0] -> full_name 
	       and 
	       $self -> {'rerun'} < 1 ) {
	    
	    if( not -e './NM_run' . ($run+1) . '/done' ){
	      # here we have an .lst file, no done file and we are not
	      # rerunning NONMEM. Which means we must create fake NM_run and
	      # "done" files. (Usually this case occurs if we want to
	      # use execute to create a summary or run table).
	      
	      mkdir( "./NM_run" . ($run+1) );
	      open( DONE, ">./NM_run". ($run+1) ."/done.1" );
	      print DONE "This is faked\nseed: 1 1\n" ;
	      close( DONE );
	      
	      # TODO Must copy tablefiles if they exist.
	      
	      # We use the existing .lst file as the final product.
	      cp( $self -> {'models'} -> [$run] -> outputs -> [0] -> full_name, './NM_run' . ($run+1) .'/psn.lst' );
	    }
	    
	    # TODO Should check for tablefiles.

	    my $modulus = (($#models+1) <= 10) ? 1 : (($#models+1) / 10)+1;
	    
	    if ( $run % $modulus == 0 or $run == 0 or $run == $#models ) {
	      ui -> print( category => 'all', wrap => 0, newline => 0,
			   message  => 'D:'.( $run + 1 ).' .. ' )
		  unless( $self -> {'parent_threads'} > 1 or $self -> {'verbose'} );
	    }
	    
	    $queue_info{$run}{'candidate_model'} =
		model -> new( filename             => "./NM_run".($run+1)."/psn.mod",
			      target               => 'disk',
			      ignore_missing_files => 1,
			      quick_reload         => 1,
			      cwres                => $models[$run] -> cwres() );
	    $self -> print_finish_message( candidate_model => $queue_info{$run}{'candidate_model'},
					   run => $run );

	    push( @{$self -> {'prepared_models'}[$run]{'own'}}, $queue_info{$run}{'candidate_model'} );
	    push( @{$self -> {'ntries'}}, 0 );
	    # push( @{$self -> {'evals'}}, \@evals );

	    next; # We are done with this model. It has already been run. Go back to polling.

	  }

	  # }}}

	  # {{{ delay code (to avoid overload of new processes)
	  
	  if( $threads > 1 ){
	    
	    if( $run > 1 ){
	      my $start_sleep = Time::HiRes::time();
	      
	      my ($min_sleep, $max_sleep); # min_sleep is in microseconds and max_sleep is in seconds.
	      
	      if( defined $PsN::config -> {'_'} -> {'min_fork_delay'} ) {
		$min_sleep = $PsN::config -> {'_'} -> {'min_fork_delay'};
	      } else {
		$min_sleep = 0;
	      }
	      
	      if( defined $PsN::config -> {'_'} -> {'max_fork_delay'} ) {
		$max_sleep = $PsN::config -> {'_'} -> {'max_fork_delay'};
	      } else {
		$max_sleep = 0;
	      }
	      
	      if( $min_sleep > $max_sleep * 1000000 ){
		$max_sleep = $min_sleep;
	      }
	      
	      # Dont wait for psn.lst if clean >= 3 it might have been
	      # removed.
	      
	      while( not( -e 'NM_run'.($run-1).'/psn.lst' ) and not $self -> {'clean'} >= 3
		     and 
		     (Time::HiRes::time() - $start_sleep) < $max_sleep ){
		Time::HiRes::usleep($min_sleep);
		}
	    }
	    
	  }

	  # }}}
	  
	  # {{{ Call to run_nonmem 

	  # This will stop nasty prints from model, output and data
	  # which are set to print for the scm.
	  my $old_category = ui -> category();
	  
	  ui -> category( 'modelfit' );
	  
	  $self -> create_sub_dir( subDir => '/NM_run'.($run+1) );
	  chdir( 'NM_run'.($run+1) );

	  # Initiate queue_info entry (unless its a restart)

	  unless( exists $queue_info{$run} ){
	    $queue_info{$run}{'candidate_model'} = $self -> copy_model_and_input( model => $models[$run], source => '../' );
	    $queue_info{$run}{'model'} = $models[$run];
	    $queue_info{$run}{'modelfile_tainted'} = 1;
	    $queue_info{$run}{'tries'} = 0;
	    $queue_info{$run}{'crashes'} = 0;
	    $queue_info{$run}{'evals'} = 0;
	    $queue_info{$run}{'run_results'} = [];
	    $queue_info{$run}{'raw_results'} = [];
	    $queue_info{$run}{'raw_nonp_results'} = [];
	    $queue_info{$run}{'start_time'} = 0;

	    # {{{ printing progress

	    # We don't want to print all starting models if they are
	    # more than ten. But we allways want to print the first
	    # and last
	    
	    my $modulus = (($#models+1) <= 10) ? 1 : (($#models+1) / 10);
	    
	    if ( $run % $modulus == 0 or $run == 0 or $run == $#models ) {
	      
	      # The unless checks if tool owning the modelfit is
	      # running more modelfits, in wich case we should be
	      # silent to avoid confusion. The $done check is made do
	      # diffrentiate between allready run processes.
	      
	      ui -> print( category => 'all', wrap => 0, newline => 0,
			   message  => 'S:'.($run+1).' .. ' )
		  unless ( $self -> {'parent_threads'} > 1 or $self -> {'verbose'} );
	    }

	    # }}}
	  }

	  my %options_hash = %{$self -> _get_run_options(run_id => $run)}; 

	  $self -> run_nonmem ( run_no          => $run,
				nm_version      => $options_hash{'nm_version'},
				queue_info      => $queue_info{$run},
				queue_map       => \%queue_map );

	  chdir( '..' );

	  if( $self -> {'run_on_umbrella'} ){
	    $queue_info{'NM_run'.($run+1)}{'run_no'} = $run;
	  }

	  ui -> category( $old_category );

	  # }}}

	} else { 

	  # {{{ Here, a process has finished and we check for restarts.

	  my $run = $queue_map{$pid};

	  my $candidate_model = $queue_info{$run}{'candidate_model'};
	  
	  my $work_dir = 'NM_run' . ($run+1) ;
	  chdir( $work_dir );
	  
#	  if( $queue_info{$run}{'compile_only'} ){
#	    $queue_info{$run}{'compile_only'} = 0;
#	    $self -> run_nonmem ( run_no          => $run,
#				  nm_version      => $options_hash{'nm_version'},
#				  queue_info      => $queue_info{$run},
#				  queue_map       => \%queue_map );
#
#	    delete( $queue_map{$pid} );
#	    chdir( '..' );
#	  } else {
	    $self -> compute_cwres( queue_info => $queue_info{$run},
				    run_no     => $run );

	    $self -> compute_iofv( queue_info => $queue_info{$run},
				   run_no     => $run );

	    # Make sure that each process gets a unique random sequence:
	    my $tmpseed = defined $self->seed() ? $self->seed() : random_uniform_integer(1,1,99999999);
	    my $tmptry  = exists $queue_info{$run}{'tries'} ? $queue_info{$run}{'tries'} : 0;
	    random_set_seed(($tmpseed+100000*($run+1)),($tmptry+1));
 
	    my %options_hash = %{$self -> _get_run_options(run_id => $run)};
	    
	    my $do_restart = $self -> restart_needed( %options_hash,
						      queue_info        => $queue_info{$run},
						      run_no            => $run );

	    if( $do_restart ){
	      unshift(@queue, $run);
	      delete($queue_map{$pid});
	      chdir( '..' );	      
	    } else {
	      $self -> select_best_model(run_no          => $run,
					 nm_version      => $options_hash{'nm_version'},
					 queue_info      => $queue_info{$run});
	      
	      # {{{ Print finishing messages

	      if( scalar @queue == 0 ){
		if( $all_jobs_started == 0 ){
		  
		  ui -> print( category => 'all',
			       message  => " done\n") if( $self -> {'parent_threads'} <= 1 and not $self -> {'verbose'} );
		  ui -> print( category => 'all',
			       message  => "Waiting for all NONMEM runs to finish:\n" ) if( $self -> {'parent_threads'} <= 1 and $threads > 1 and not $self -> {'verbose'} );
		  
		  $all_jobs_started = 1;
		} 
		
		my $modulus = (($#models+1) <= 10) ? 1 : (($#models+1) / 10)+1;
		
		if ( $run % $modulus == 0 or $run == 0 or $run == $#models ) {
		  ui -> print( category => 'all',
			       message  => 'F:'.($run+1).' .. ',
			       wrap => 0,
			       newline => 0)
		      unless ( $self -> {'parent_threads'} > 1 or $self -> {'verbose'} );
		}
	      }

	      # }}}	      

	      chdir( '..' );
	      
	      # {{{ cleaning and done file

	      if( $self -> {'clean'} >= 3 ){
		unlink( <$work_dir/*> );
		unless( rmdir( $work_dir ) ){debug -> warn( message => "Unable to remove $work_dir directory: $! ." )};
		sleep(2);
	      } else {
		
#		unless( $queue_info{$run}{'tries'} > 0 ){
#		  $self -> _print_done_file( run       => $run,
#					     tries     => $queue_info{$run}{'tries'},
#					     evals_ref => \@evals );
#		}
		
	      }

	      # }}}

	      $self -> print_finish_message( candidate_model => $candidate_model,
					     run => $run );
	      
	      if( $threads <= 1 ){
		push( @{$self -> {'prepared_models'}[$run]{'own'}}, $candidate_model );
		push( @{$self -> {'ntries'}}, $queue_info{$run}{'tries'} );
	      }
	      
	      delete( $queue_info{$run} );
	      delete( $queue_map{$pid} );
	    }
	    
	    next;
#	  }

	  # }}}

	}
      } # queue loop ends here

      if( $self -> {'run_on_umbrella'} ){
	# {{{ Umbrella code

	$self -> umbrella_submit( prepare_jobs => 0 );
	
	my $start_time = time();
	
	my $dbh = DBI -> connect("DBI:mysql:host=".$PsN::config -> {'_'} -> {'database_server'}.
				 ";databse=".$PsN::config -> {'_'} -> {'project'},
				 $PsN::config -> {'_'} -> {'user'},
				 $PsN::config -> {'_'} -> {'password'},
				 {'RaiseError' => 1});
	
	while( scalar keys %queue_info ){
	  
	  # Get new "finished"
	  
	  if( time() > ($start_time + $self -> {'umbrella_timeout'}) ) {
	    debug -> die( message => "Waiting for NONMEM run timed out using the umbrella system" );
	    last;
	  }
	  my $project = $PsN::config -> {'_'} -> {'project'};
	  my $sth = $dbh -> prepare( "SELECT t.directory,j.tool_id, t.parent_tool_id, t.tool_id,j.state ".
				     "FROM $project.job j, $project.tool t ".
				     "WHERE j.tool_id=t.tool_id ".
				     "AND t.parent_tool_id = ". $self -> tool_id() ." ".
				     "AND j.state = 4" );
	  
	  $sth -> execute or 'debug' -> die( message => $sth->errstr ) ;
	  
	  my $select_arr = $sth -> fetchall_arrayref;
	  $sth -> finish;
	  
	  foreach my $row ( @{$select_arr} ){
	    if( exists $queue_info{$row -> [0]} ){
	      
	      my $work_dir = $row -> [0];
	      my $id = $work_dir;
	      chdir( $work_dir );
	      my $modelfile_tainted = 0;
	      
	      unless( defined $queue_info{$id}{'tries'} ){
		$queue_info{$id}{'tries'} = 0;
	      }
	      
	      my %options_hash = %{$self -> _get_run_options(run_id => $queue_info{$id}{'run_no'})};
	      
	      if( $self -> restart_needed( %options_hash,
					   queue_info    => $queue_info{$id},
					   run_no            => $id ) ){
		
		my $new_job_id;
		
		$self -> run_nonmem ( nm_version        => $options_hash{'nm_version'},
				      model             => $models[$queue_info{$id}{'run_no'}],
				      candidate_model   => $queue_info{$id}{'candidate_model'},
				      work_dir          => $work_dir,
				      tries             => \$queue_info{$id}{'tries'},
				      job_id            => \$new_job_id,
				      modelfile_tainted => \$modelfile_tainted);
		
		%{$queue_info{$new_job_id}} = %{$queue_info{$id}};
		
		delete $queue_info{$id};
	      } else {
		
		$self -> select_best_model(run_no          => $queue_info{$id}{'run_no'},
					   nm_version      => $options_hash{'nm_version'},
					   queue_info      => $queue_info{$id});
		delete $queue_info{$id};
		
	      }
	      
	      chdir( '..' );
	      
	    }
	  }
	}
	$dbh -> disconnect;

	# }}}
      }

      # {{{ Print finish message
      
      ui -> print( category => 'all',
		   message  => " done\n" ) if( $self -> {'parent_threads'} <= 1 and $threads > 1 and not $self -> {'verbose'});

      # }}}

      # }}} local execution

      # {{{ Database code

#       my $dbh;
#       if ( $PsN::config -> {'_'} -> {'use_database'} ) {
# 	$dbh = DBI -> connect("DBI:mysql:host=".$PsN::config -> {'_'} -> {'database_server'}.";databse=psn",
# 			      "psn", "psn_test",
# 			      {'RaiseError' => 1});
      
# 	for( my $i = 1; $i <= scalar @{$self -> {'models'}}; $i++ ) {
# 	  my $sth;
# 	  foreach my $model ( @{$self -> {'prepared_models'}[$i-1]{'own'}} ) {
# 	    my $model_id = $model -> model_id;
# 	    if ( defined $model_id ) {
# 	      $sth = $dbh -> prepare("INSERT INTO psn.tool_model (tool_id,".
# 				     "model_id,prepared_model) ".
# 				     "VALUES ('".$self -> {'tool_id'}."', ".
# 				     $model_id.", 1 )");
# 	      $sth -> execute;
# 	    }
# 	  }
# 	  $sth -> finish if ( defined $sth );
# 	}
# 	$dbh -> disconnect;
#       }

      # }}}

      $self -> prepare_raw_results();

      $self -> print_raw_results();

      chdir( $cwd );

      # {{{ clean $self -> directory 
      if( not $self -> {'top_tool'} and $self -> {'clean'} >= 3 ){
	my $dir = $self -> {'directory'};
	unlink( <$dir/NM_run*/*> );
	for( <$dir/NM_run*> ) {
	  rmdir();
	}
	unlink( <$dir/*> );
	rmdir( $dir );
      }
      # }}}

    }
end run

# }}} run

# {{{ _get_run_options

start _get_run_options
{
  
  %options_hash = ( 'handle_rounding_errors' => undef, # <- Handle rounding errors a bit more intelligently
		    'handle_hessian_npd' => undef,	  # <- Handle hessian not postiv definite a bit more intelligently             
		    'handle_maxevals' => undef,        # <- Restart after maxeval
		    'cutoff_thetas' => 'ARRAY',
		    'tweak_inits' => undef,
		    'retries' => undef,
		    'picky' => undef,
		    'significant_digits_rerun' => undef,
		    'nm_version' => undef );
  
  foreach my $option ( keys %options_hash ) {
    
    # This loops allows run specific parameters to be
    # specified. We check that the parameter given is an
    # array we take out one element of that array and pass it
    # as a run specific parameter. If the option is specified
    # as being an "ARRAY" type in the has above, but there are
    # no subarrays, we send the entire array as an parameter.
    
    if( ref( $self -> {$option} ) eq 'ARRAY' ) {
      if( ref( $self -> {$option} -> [$run_id] ) ne 'ARRAY' and $options_hash{$option} eq 'ARRAY' ) {
	$options_hash{$option} = $self -> {$option};
      } else {
	$options_hash{$option} = $self -> {$option} -> [$run_id];
      }
    } else {
      $options_hash{$option} = $self -> {$option};
    }
  }
}
end _get_run_options

# }}}

# {{{ summarize

start summarize
    {

      my %raw_header_map;

      for( my $i = 0 ; $i <= scalar @{$self -> {'raw_results_header'}}; $i++ ){
	$raw_header_map{$self -> {'raw_results_header'} -> [$i]} = $i;
      }

      my %model_problem_structure;

      foreach my $raw_row ( @{$self -> {'raw_results'}} ){
	my $model = $raw_row -> [$raw_header_map{'model'}];
	my $problem = $raw_row -> [$raw_header_map{'problem'}];
	my $subproblem = $raw_row -> [$raw_header_map{'subproblem'}];

	$model_problem_structure{ $model }{ $problem }{ $subproblem } = 1;
      }

      # ------------------------------------------------------------------#

      # Model evaluation. Part one. Version 0.1

      # This method checks a nonmem output file for flaws.  The parts
      # concerning parts of the covariance step obviously needs the
      # $COV # record. In addition, the conditioning number test needs
      # the PRINT=E # option of the $COV record. The limits for the
      # relative tests may be # changed below:

      my $correlation_limit = $self -> {'correlation_limit'};      # All correlations above this number
                                                                   # will be listed.
      my $condition_number_limit = $self -> {'condition_number_limit'};    # An error will be rised if the
                                                                           # condition number is greater
                                                                           # than this number.
      my $near_bound_sign_digits = $self -> {'near_bound_sign_digits'};   # If a parameter estimate is equal
                                                                          # to a bound with this many
                                                                          # significant digits, a warning
                                                                          # will be printed.
      my $near_zero_boundary_limit = $self -> {'near_zero_boundary_limit'};  # When the bound is zero, the
                                                                             # check above is not valid. Use
                                                                             # this limit instead.
      my $sign_digits_off_diagonals =  $self -> {'sign_digits_off_diagonals'};   # The off-diagonal elements are
                                                                                 # checked against +-1 with this
                                                                                 # many significant digits.
      my $large_theta_cv_limit = $self -> {'large_theta_cv_limit'};   # Coefficients of variation larger
                                                                      # than these numbers will produce
                                                                      # warnings.
      my $large_omega_cv_limit = $self -> {'large_omega_cv_limit'};
      my $large_sigma_cv_limit = $self -> {'large_omega_cv_limit'};

      my $confidence_level = $self -> {'confidence_level'};   # Percentage value;
      my $precision = $self -> {'precision'};   # Precision in output.

      sub acknowledge {
	my $name    = shift;
	my $outcome = shift;
	my $file    = shift;
	my $l = (7 - length( $outcome ))/2;
	my $text = sprintf( "%-66s%2s%7s%-5s", $name, '[ ', $outcome. ' ' x $l, ' ]' );
	ui -> print( category => 'all', message => $text, wrap => 0 );
	print $file $text if defined $file;
      }

      my $raw_line_structure = ext::Config::Tiny -> read( $self -> {'directory'} . "/raw_file_structure" );
      my $raw_res_index = 0;

      for( my $model = 1; $model <= scalar keys %model_problem_structure; $model ++ ){
	
#	my $output = $self -> {'models'} -> [ $model - 1 ] -> outputs -> [0];

#	$output -> _read_problems;
       	
	open( my $log, ">test.log" );

	my $eigen_run     = 1;
	my $corr_matr_run = 1;
	
	my $raw_res = $self -> {'raw_results'};

	for ( my $problem = 1; $problem <= scalar keys %{$model_problem_structure{ $model }} ; $problem++ ) {
	  print "PROBLEM ",$problem,"\n" if scalar keys %{$model_problem_structure{ $model }} > 1;
	  for ( my $subproblem = 1; $subproblem <= scalar keys %{$model_problem_structure{ $model }{ $problem }}; $subproblem++ ) {
	    print "SUBPROBLEM ",$subproblem,"\n" if scalar keys %{$model_problem_structure{ $model }{ $problem }} > 1;
	    
	    if ( $raw_res -> [ $raw_res_index ][$raw_header_map{'minimization_successful'}] == 1 ) { # Is in raw
	      acknowledge( 'Successful minimization', 'OK', $log );
	    } else {
	      acknowledge( 'Termination problems', 'ERROR', $log );
	    }
	    if ( $raw_res -> [ $raw_res_index ][$raw_header_map{'rounding_errors'}] eq '0' ) { # Is in raw
	      acknowledge( 'No rounding errors', 'OK', $log );
	    } else {
	      acknowledge( 'Rounding errors', 'ERROR', $log );
	    }

	    if ( $raw_res -> [ $raw_res_index ][$raw_header_map{'zero_gradients'}] eq '0' ) { # Is in raw
	      acknowledge( 'No zero gradients', 'OK', $log );
	    } else {
	      acknowledge( 'Zero gradients found '.
			   $raw_res -> [ $raw_res_index ][$raw_header_map{'zero_gradients'}].
			   ' times', 'ERROR', $log );
	    }

	    if ( $raw_res -> [ $raw_res_index ][$raw_header_map{'final_zero_gradients'}] eq '0' ) { # Is in raw
	      acknowledge( 'No final zero gradients', 'OK', $log );
	    } else {
	      acknowledge( 'Final zero gradients', 'ERROR', $log );
	    }

	    if ( $raw_res -> [ $raw_res_index ][$raw_header_map{'hessian_reset'}] eq '0' ) { # Is in raw
	      acknowledge( 'Hessian not reset', 'OK', $log );
	    } else {
	      acknowledge( 'Hessian reset '.
			   $raw_res -> [ $raw_res_index ][$raw_header_map{'hessian_reset'}].
			   ' times', 'WARNING', $log );
	    }

	    if ( $raw_res -> [ $raw_res_index ][$raw_header_map{'estimate_near_boundary'}] eq '0' ){
	      acknowledge( 'Estimate not near boundary', 'OK', $log );
	    } else {
	      acknowledge( 'Estimate near boundary','WARNING',$log);
	    }

	    my ( $n_b, $f_b, $f_e );# =
#		$output -> near_bounds( zero_limit         => $near_zero_boundary_limit,
#					significant_digits => $near_bound_sign_digits,
#					off_diagonal_sign_digits => $sign_digits_off_diagonals ); # Is in raw

	    if ( defined $n_b -> [$problem] and 
		 defined $n_b -> [$problem][$subproblem] ) {
	      my @near_bounds = @{$n_b -> [$problem][$subproblem]};
	      my @found_bounds = @{$f_b -> [$problem][$subproblem]};
	      my @found_estimates = @{$f_e -> [$problem][$subproblem]};
	      if ( $#near_bounds < 0 ) {
		acknowledge( 'No parameter near boundary', 'OK', $log );
	      } else {
		acknowledge( 'Parameter(s) near boundary', 'WARNING', $log );
		for ( my $problem = 0; $problem <= $#near_bounds; $problem++ ) {
		  printf( "\t%-20s%10g   %10g\n", $near_bounds[$problem],
			  $found_estimates[$problem], $found_bounds[$problem] );
		  print $log sprintf( "\t%-20s%10g   %10g\n", $near_bounds[$problem],
				      $found_estimates[$problem], $found_bounds[$problem] );
		  print "\n" if ( $problem == $#near_bounds );
		  print $log "\n" if ( $problem == $#near_bounds );
		}
	      }
	    }

	    if ( $raw_res -> [ $raw_res_index ][ $raw_header_map{'covariance_step_run'} ] ) { # Is in raw
	      if (  $raw_res -> [ $raw_res_index ][$raw_header_map{'covariance_step_successful'}] eq '0' ) {
		acknowledge( 'Covariance step', 'ERROR', $log );
	      } else {
		if ( $raw_res -> [ $raw_res_index ][$raw_header_map{'covariance_step_successful'}] eq '1' ) {
		  acknowledge( 'Covariance step ', 'OK', $log );
		}else {
		  acknowledge( 'Covariance step', 'WARNING', $log );
		}

		my ( $l_se, $f_cv );# =
#		    $output -> large_standard_errors( theta_cv_limit => $large_theta_cv_limit,
#						      omega_cv_limit => $large_omega_cv_limit,
#						      sigma_cv_limit => $large_sigma_cv_limit ); # Is NOT in raw
		if ( defined $l_se -> [$problem] and 
		     defined $l_se -> [$problem][$subproblem] ) {
		  my @large_standard_errors = @{$l_se -> [$problem][$subproblem]};
		  my @found_cv = @{$f_cv -> [$problem][$subproblem]};
		  if ( $#large_standard_errors < 0 ) {
		    acknowledge( 'No large standard errors found', 'OK', $log );
		  } else {
		    acknowledge( 'Large standard errors found', 'WARNING', $log );
		    for ( my $problem = 0; $problem <= $#large_standard_errors; $problem++ ) {
		      printf( "\t%-20s%10g\n", $large_standard_errors[$problem], $found_cv[$problem] );
		      print $log sprintf( "\t%-20s%10g\n", $large_standard_errors[$problem], $found_cv[$problem] );
		      print "\n" if ( $problem == $#large_standard_errors );
		      print $log "\n" if ( $problem == $#large_standard_errors );
		    }
		  }
		}

		my $eigens = $raw_res -> [ $raw_res_index ][$raw_header_map{'eigens'}]; # Is in raw
		if ( $eigens ){# defined $eigens and
		    # defined $eigens -> [$problem][$subproblem] and
		    # scalar @{$eigens -> [$problem][$subproblem]} > 0 ) { 
		  if ( $raw_res -> [ $raw_res_index ][$raw_header_map{'condition_number'}] < $condition_number_limit ) {
		    acknowledge( 'Condition number ', 'OK', $log );
		  } else {
		    acknowledge( 'Large condition number ('.
				 sprintf("%6.0f",
					 $raw_res -> [ $raw_res_index ][$raw_header_map{'condition_number'}]).
				 ')', 'WARNING', $log );
		  }
		} else {
		  $eigen_run = 0;
		}

		if ( $raw_res -> [ $raw_res_index ][$raw_header_map{'s_matrix_singular'}] ne '1' ) { # Is in raw
		  acknowledge( 'S-matrix ', 'OK', $log );
		} else {
		  acknowledge( 'S-matrix is singular', 'WARNING', $log );
		}

		my ( $h_c, $f_c );# = $output high_correlations( limit => $correlation_limit ); # Is NOT in raw
		if ( defined $h_c -> [$problem] and 
		     defined $h_c -> [$problem][$subproblem] ) {
		  my @high_correlations = @{$h_c -> [$problem][$subproblem]};
		  my @found_correlations = @{$f_c -> [$problem][$subproblem]};
		  if ( $#high_correlations < 0 ) {
		    acknowledge( 'Correlations', 'OK', $log );
		  } else {
		    acknowledge( 'Large correlations between parameter estimates found', 'WARNING', $log );	
		    for ( my $problem = 0; $problem <= $#high_correlations; $problem++ ) {
		      printf( "\t%-20s%10g\n", $high_correlations[$problem],
			      $found_correlations[$problem] );
		      print $log sprintf( "\t%-20s%10g\n", $high_correlations[$problem],
					  $found_correlations[$problem] );
		      print "\n" if ( $problem == $#high_correlations );
		      print $log "\n" if ( $problem == $#high_correlations );
		    }
		  }
		}
	      }
	    } else {
	      print "No Covariance step run\n";
	      print $log "No Covariance step run\n";
	    }
	    
	    # sumo script
	    
	    my $confidence_interval = 0;
	    my $csv = 0;
	    my $form = '%.' . $precision . 'g';
	    
	    my @output_matrix;
	    my @output_matrix_sizes;
	    
	    my %c_levels = ( '90'   => 1.6449,
			     '95'   => 1.96,
			     '99'   => 2.5758,
			     '99.9' => 3.2905 );
	    
	    if( $confidence_interval ) {
	      if( not defined $c_levels{$confidence_level} ) {
		debug -> die( message =>  "Sorry, confidence intervals for level ".$confidence_level.
			      " can not be output. Valid levels are: ".join(',', keys %c_levels),
			      level => 1 );
		
	      }
	    }
	    
	    my ( %nam, %est, %cest, %ses );
	    
	    
	    foreach my $estimate ( 'theta','omega','sigma' ){
	      my @pos_length = split( /,/ , $raw_line_structure -> {($problem-1)}{$estimate});
	      my @estimates = @{$raw_res -> [ $raw_res_index ]}[ $pos_length[0] .. $pos_length[0] + $pos_length[1] ];
	      my @names;
	      foreach my $num ( 0 .. $pos_length[1] - 1){
		my $template = uc(substr($estimate,0,2));
		push( @names, $template.($num+1));
	      }

	      my @pos_length = split( /,/ , $raw_line_structure -> {($problem-1)}{'se'.$estimate});
	      my @se_estimates = @{$raw_res -> [ $raw_res_index ]}[ $pos_length[0] .. $pos_length[0] + $pos_length[1] ];
	      
	      my @pos_length = split( /,/ , $raw_line_structure -> {($problem-1)}{'cvse'.$estimate});
#	      my @cvse_estimates = @{$raw_res -> [ $raw_res_index ]}[ $pos_length[0] .. $pos_length[0] + $pos_length[1] ];
	      my @cvse_estimates = ();
	      
	      $nam{$estimate} = \@names;
	      $est{$estimate} = \@estimates;
	      $est{'cvse'.$estimate} = \@cvse_estimates;
	      $ses{$estimate} = \@se_estimates;
	    }
	    
	    my $ofv = $raw_res -> [ $raw_res_index ][ $raw_header_map{'ofv'} ];
	    
	    if ( defined $ofv ) {
	      print "Objective function value: ",$ofv,"\n\n";
	    } else {
	      print "Objective function value: UNDEFINED\n\n";
	    }
	    
	    push( @output_matrix, ["","THETA","","","OMEGA","","","SIGMA", ""] );
	    for( my $i = 0; $i <= $#{$output_matrix[0]}; $i++ ){
	      if( $output_matrix_sizes[$i] < length( $output_matrix[0][$i] ) ){
		$output_matrix_sizes[$i] = length( $output_matrix[0][$i] );
	      }
	    }
	    
	    my $max_par = $#{$est{'theta'}};
	    $max_par = $#{$est{'omega'}} if ( $#{$est{'omega'}} > $max_par );
	    $max_par = $#{$est{'sigma'}} if ( $#{$est{'sigma'}} > $max_par );
	    
	    for ( my $max = 0; $max <= $max_par; $max++ ) {
	      my ( @row, %cis );
	      if( $confidence_interval ) {
		foreach my $param ( 'theta', 'omega', 'sigma' ) {
		  if ( defined $est{$param}[$max] ) {
		    my $diff = $c_levels{$confidence_level}*$ses{$param}[$max];
		    my ( $lo, $up );
		    if( defined $diff ) {
		      $lo = $est{$param}[$max]-$diff;
		      $up = $est{$param}[$max]+$diff;
		    }
		    my $cis = sprintf( "($form - $form)", $lo, $up );
		    push( @row, $nam{$param}[$max],
			  sprintf( $form, $est{$param}[$max] ), 
			  $cis );
		  } else {
		    push( @row, '','','' ); 
		  }
		}
	      } else {
		foreach my $estimate ( 'theta', 'omega','sigma' ){ # Loop over comegas instead
		  if ( defined $nam{$estimate}->[$max] ) {
		    push( @row, $nam{$estimate}->[$max], defined $est{$estimate}->[$max] ? sprintf( $form, $est{$estimate}->[$max] ) : '........', 
			  $est{'cvse'.$estimate}->[$max] ? sprintf( "($form)", $est{'cvse'.$estimate}->[$max] ) : '(........)' );
		  } else {
		    push( @row, '','','' ); 
		  }
		}
	      }

	      push(@output_matrix, \@row);
	      for( my $i = 0; $i <= $#row; $i++ ){
		if( $output_matrix_sizes[$i] < length( $row[$i] ) ){
		  $output_matrix_sizes[$i] = length( $row[$i] );
		}
	      }
	    }
	
	    foreach my $row ( @output_matrix ){
	      for( my $i = 0; $i <= $#{$row}; $i++ ){
		my $spaces = $output_matrix_sizes[$i] - length($row -> [$i]);
		if( $csv ){
		  print $row -> [$i], ",";
		} else {
		  print " " x $spaces, $row -> [$i], "  ";
		}
	      }
	      print "\n";
	    }
	    
	    #$output -> flush;
	    $raw_res_index ++;
	  }
	}
	close( $log );

      }
    }
end summarize

# }}}

# {{{ compute_cwres

start compute_cwres
my $queue_info_ref = defined $parm{'queue_info'} ? $parm{'queue_info'} : {};
my $model = $queue_info_ref -> {'model'};
if( defined $PsN::config -> {'_'} -> {'R'} ) {
  my $probs = $model -> problems();
  if( defined $probs ) {
    foreach my $prob ( @{$probs} ) {
      if( $prob -> {'cwres_modules'} ){
	my $sdno = $prob -> {'cwres_modules'} -> [0] -> sdno();
	my $sim = $prob -> {'cwres_modules'} -> [0] -> mirror_plots ? ',sim.suffix="sim"' : '';
# Create the short R-script to compute the CWRES.
	open( RSCRIPT, ">compute_cwres.R" );
	print RSCRIPT "library(xpose4)\ncompute.cwres($sdno $sim)\n";
	close( RSCRIPT );
	
# Execute the script
	system( $PsN::config -> {'_'} -> {'R'}." CMD BATCH compute_cwres.R" );
      }
    }
  }
}

end compute_cwres

# }}}

# {{{ compute_iofv

start compute_iofv
{
  my $queue_info_ref = defined $parm{'queue_info'} ? $parm{'queue_info'} : {};
  my $model = $queue_info_ref -> {'model'};
  
  if( defined $model -> iofv_modules ){
    $model -> iofv_modules -> [0] -> post_run_process;
  }
}
end compute_iofv

# }}}
