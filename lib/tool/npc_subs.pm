# {{{ include_statements
start include statements
use Data::Dumper;
use Math::Random;
use strict;
use tool::modelfit;
use model;
use ui;
end include
# }}}

# {{{ new

start new
{

  if (defined $this->{'samples'}){
    if ( $this->{'samples'} < 20){
      'debug' -> die(message => "Program does not allow less than 20 samples.");
    }
  } else {
    'debug' -> die(message => "Option -samples is required.");
  }
  
  if ( scalar (@{$this -> {'models'}->[0]-> problems}) != 1 ){
    'debug' -> die(message => 'Tool can only handle modelfiles with exactly one problem.');
  }
  
  unless ($this->{'dv'} =~ /^(IPRED|IWRES|CWRES)$/ ){
    if (length($this->{'dv'})>4){
      'debug' -> die(message => "The dependent variable name (option -dv) can have at most".
		     " 4 letters, unless it is IPRED/IWRES/CWRES, due to a limitation in NONMEM.");
    }
  }
  
  my @check_cols;
  push (@check_cols, $this->{'bin_on'}) if (defined $this->{'bin_on'});
  push (@check_cols, $this->{'stratify_on'}) 
      if ((defined $this->{'stratify_on'}) && ('STRT' ne $this->{'stratify_on'}));
  foreach my $col (@check_cols){
    my $var_text = ($col eq $this->{'stratify_on'})? 'stratification' : 'independent';
    if ($col =~ /^(IPRED|IWRES|IRES|RES|WRES|DV|CWRES)$/){
      'debug' -> die(message => "It is not allowed to use $col as the $var_text ".
		     "variable, since it it is dependent and hence differs between ".
		     "simulations of the same observation.");
    }
    
    unless ($col =~ /^(PRED)$/){
      #may be synonym
      if ($this -> {'models'}->[0]-> is_option_set(record=>'input',name=>$col)){
	my $val = $this -> {'models'}->[0]-> get_option_value(record_name => 'input',
							      option_name => $col);
	if ((defined $val) && ($val =~ /(SKIP|DROP)/ )){
	  'debug' -> die(message => "Cannot SKIP/DROP the $var_text variable ".
			 "in the \$INPUT record.");
	}
      } else {
	#check synonyms
	my $input_record = $this -> {'models'}->[0]-> record(record_name => 'input' );
	my $found=0;
	if( scalar(@{$input_record}) > 0 ){ #always true
	  foreach my $line ( @{$input_record -> [0]} ){
	    if ( $line =~ /([\w]+)=([\w]+)[^\w]/ ){
	      if ($col eq $2){
		$found=1;
		last;
	      }
	    }
	  }
	}
	'debug' -> die(message => "In order to use $col as the $var_text variable, ".
		       "it must be in the \$INPUT record ") if !($found);
      }
    }
  }
  
  foreach my $opt_name ('LIKELIHOOD','-2LOGLIKELIHOOD','-2LLIKELIHOOD','LAPLACIAN'){
    if( $this -> {'models'}->[0]-> is_option_set( record => 'estimation',
						  name => $opt_name,fuzzy_match => 1 )){
      print "\n\n******\nWarning: Option $opt_name found in \$ESTIMATION. Please note that script\n".
	  "is not able to perform a proper analysis of e.g. count/categorical data.\n";
      print "\$ESTIMATION will be removed for simulations, this may produce erroneous results.\n".
	  "Use option -dv to circumvent deletion of \$ESTIMATION.\n" unless ($this->{'do_estimation'});
      print "******\n\n";

      #possible change, see to_do 61. Is current handling of NONP wrong?????
      #    $this->{'do_estimation'}=1;
    }
  }
  
  if ($this->{'dv'} =~ /^(PRED|MDV|TIME|DATE|EVID|AMT|RATE|ID)$/){
    'debug' -> die(message => "It is not allowed to use $this->{'dv'} as the dependent".
		   " variable,\nbecause it".
		   " is the same for all simulations of a single observation.");
  }
  
  my @needed_variables=();
  push (@needed_variables,('IWRES','IPRED','IRES')) if ($this->{'do_estimation'});
  push (@needed_variables,'STRT') if ($this->{'stratify_on'} eq 'STRT');
  push (@needed_variables,$this->{'dv'}) unless 
      ($this->{'dv'} =~ /^(IPRED|IWRES|IRES|RES|WRES|DV|CWRES)$/);
  
  if (scalar(@needed_variables)>0){
    my @line_array;
    push (@line_array,@{$this ->{'models'}->[0]->problems->[0]->errors->[0]->code})
	if (defined ($this ->{'models'}->[0]->problems->[0]->errors));
    
    push (@line_array,@{$this ->{'models'}->[0]->problems->[0]->preds->[0]->code})	
	if (defined $this ->{'models'}->[0]->problems->[0]->preds);
    
    push (@line_array,@{$this ->{'models'}->[0]->problems->[0]->pks->[0]->code})
	if (defined $this ->{'models'}->[0]->problems->[0]->pks);
    
    my $missing_variables='';
    foreach my $var (@needed_variables){
      my $found=0;
      foreach my $line (@line_array){
	#variable must be left hand side, i.e. first set of non-blank characters
	if ($line =~ /^ *$var[ =]/){
	  $found=1;
	  last;
	} 
      }
      $missing_variables = "$missing_variables $var" unless $found;
    }
    if (length($missing_variables)>0){
      'debug' -> die(message => "The selected input options require the user to define".
		     "$missing_variables in the modelfile,\nbut this was not done in ".
		     "neither \$ERROR, \$PRED nor \$PK.");
    }
  }
  
#check for ICALL .EQ. 4

  my @line_array;
  push (@line_array,@{$this ->{'models'}->[0]->problems->[0]->errors->[0]->code})
      if (defined ($this ->{'models'}->[0]->problems->[0]->errors));
    
  push (@line_array,@{$this ->{'models'}->[0]->problems->[0]->preds->[0]->code})	
      if (defined $this ->{'models'}->[0]->problems->[0]->preds);
  
  push (@line_array,@{$this ->{'models'}->[0]->problems->[0]->pks->[0]->code})
      if (defined $this ->{'models'}->[0]->problems->[0]->pks);

  push (@line_array,@{$this ->{'models'}->[0]-> record(record_name => 'des')->[0]})
      if (defined $this ->{'models'}->[0]-> record(record_name => 'des')->[0]);
  
  my $found=0;
  foreach my $line (@line_array){
    if ($line =~ /ICALL\s?\.EQ\.\s?4/){
      $found=1;
      last;
    } 
  }

  if ($found){
    print "\n\nWarning: String ICALL.EQ.4 found in modelfile. Simulation statements\n".
	"will influence NPC/VPC simulations and might produce unwanted results.\n\n";
  }
  
 ##end ICALL check



  if (defined $this->{'mirrors'}){
    if ( $this->{'mirrors'} > 20){
      'debug' -> die(message => "Program does not allow more than 20 mirror plots.");
    }
  }
  
  if ( scalar (@{$this -> {'models'}->[0]-> record(record_name => 'simulation' )}) > 1 ){
    'debug' -> die(message => "Tool does not allow more than one \$SIMULATION".
		   " record in the modelfile.");
  }
  
  unless ( scalar (@{$this -> {'models'}->[0]-> record( record_name => 'estimation' )}) == 1 ){
    'debug' -> die(message => 'Tool requires exactly one $ESTIMATION record in '.
		   'the modelfile.');
  }
  
  if ( scalar (@{$this -> {'models'}->[0]-> record(record_name => 'table' )}) > 0 ){
    'debug' -> warn(level => 1,
		    message => 'Tool will delete existing $TABLE records in the modelfile.');
  }
  
  my $np_record = $this -> {'models'}->[0]-> record(record_name => 'nonparametric' );
#    if(( scalar(@{$np_record}) > 0 )&&($this->{'dv'} eq 'CWRES')){
#	'debug' -> die(message => "CWRES cannot be computed when there is a \$NONPARAMETRIC record".
#		       ' in the modelfile.');
#    }

  if (defined $this->{'msfo_file'}){
    if (defined $this->{'lst_file'}){
      'debug' -> die(message => 'Tool does not allow using both -lst '.
		     'and -msfo options simultaneously.');
    }
  } else {
#	my $np_record = $this -> {'models'}->[0]-> record(record_name => 'nonparametric' );
    if( scalar(@{$np_record}) > 0 ){
      'debug' -> die(message => "Tool requires an msfo file when there is a \$NONPARAMETRIC record".
		     ' in the modelfile.');
    }
  }
  
  #Synonym forbidden for MDV,ID
  
  foreach my $variable ('MDV','ID'){
    if ($this -> {'models'}->[0]-> is_option_set(record=>'input',name=>$variable)){
      my $value = $this -> {'models'}->[0]-> get_option_value(record_name => 'input',
							      option_name => $variable);
      if ((defined $value) && !($value =~ /(SKIP|DROP)/ )){
	'debug' -> die(message => "It is forbidden to use a synonym for $variable ".
		       'in the $INPUT record.');
      }
    } else {
      my $input_record = $this -> {'models'}->[0]-> record(record_name => 'input' );
      if( scalar(@{$input_record}) > 0 ){ #always true
	foreach my $line ( @{$input_record -> [0]} ){
	  if ( $line =~ /([\w]+)=(MDV|ID)[^\w]/ ){
	    if (($variable eq $2) && !($1 =~ /(SKIP|DROP)/ )){
	      'debug' -> die(message => "It is forbidden to use a synonym ".
			     "for $variable in the \$INPUT record.");
	    }
	    last;
	  }
	}
      }
    }
    
  }
  
  ##Checks for VPC
  if ($this->{'is_vpc'}){
    
    unless (defined $this->{'bin_on'}){
      'debug' -> die(message => "VPC requires an independent variable column.");
    }
    
    my $option_count=0;
    if (defined $this->{'no_of_bins'}){
      $option_count++;
      unless (defined $this->{'bin_by_count'}){
	'debug' -> die(message =>"Option bin_by_count must be defined when ".
		       "option no_of_bins is used.");
      }
    }
    if (scalar(@{$this->{'bin_array'}}>0)){
      if ($option_count>0){
	'debug' -> die(message =>"Incompatible set of binning options. Use vpc -h for help.");
      }
      $option_count++;
      unless (defined $this->{'bin_by_count'}){
	'debug' -> die(message =>"Option bin_by_count must be defined when ".
		       "option bin_array is used.");
      }
      if ($this->{'bin_by_count'}==1){
	#check at least two values and all counts larger than 0
	unless (scalar(@{$this->{'bin_array'}}) > 1){
	  'debug' -> die(message =>"Must define at least two counts in bin_array ".
			 "when binning by count.");
	}
	foreach my $c ($this->{'bin_array'}){
	  if ($c<1){
	    'debug' -> die(message =>"Number of observations in each bin must ".
			   "be at least 1.");
	  }
	}
      }else {
	#check at least one value and sorted in ascending order and increasing
	unless (scalar(@{$this->{'bin_array'}}) > 0){
	  'debug' -> die(message =>"Must define at least one boundary in bin_array ".
			 "when binning by width.");
	}
	for (my $i=1; $i< scalar(@{$this->{'bin_array'}}); $i++){
	  unless ($this->{'bin_array'}->[$i] > $this->{'bin_array'}->[$i-1]){
	    'debug' -> die(message =>"List of bin boundaries must be sorted and ".
			   "increasing.");
	  }
	}
      }
      
    }
    
    if (defined $this->{'single_bin_size'}){
      if ($option_count>0){
	'debug' -> die(message =>"Incompatible set of binning options. Use vpc -h for help.");
      }
      $option_count++;
      unless ($this->{'single_bin_size'}>0){
	'debug' -> die(message =>"Option single_bin_size must be larger than 0.");
      }
      unless (defined $this->{'bin_by_count'}){
	'debug' -> die(message =>"Option bin_by_count must be defined when ".
		       "option single_bin_size is used.");
      }
      if (defined $this->{'overlap_percent'}){
	unless ($this->{'overlap_percent'}>0){
	  'debug' -> die(message =>"Option overlap must be larger than 0\%.");
	}
	unless ($this->{'overlap_percent'}<100){
	  'debug' -> die(message =>"Option overlap must be smaller than 100\%.");
	}
      }
      #else handle by translating to no_of_bins-alternative after know width/no of observations
    } else {
      if (defined $this->{'overlap_percent'}){
	'debug' -> die(message =>"Option single_bin_size must be defined when ".
		       "option overlap is used.");
      }
    }

    if (defined $this->{'bin_by_count'}){
      unless ($option_count > 0){
	'debug' -> die(message =>"Option bin_by_count is forbidden without ".
		       "additional binning options.");
      }
      unless ($this->{'bin_by_count'} eq '0' || $this->{'bin_by_count'} eq '1'){
	'debug' -> die(message =>"Option bin_by_count must be either 1 or 0.");
      }
    }
  }

  my $command_file = $this -> {'directory'}."/command.txt";
  if ( -e $command_file ){
    #this is a restart run, check if calls are compatible
    open( CMD, $command_file ) or 
	'debug'->die(message=> "Could not open command.txt in restart directory.");
    my $row = <CMD>;
    close(CMD);
    my $str = $this -> {'models'}->[0]->filename();
    unless ($row =~ /($str)/){
      'debug'->die(message=> "Name of modelfile $str on command line does not match name of ".
		   "modelfile \nin restart directory ".$this -> {'directory'});
    }
    if (defined $this->{'lst_file'}){
      unless ($row =~ /($this->{'lst'})/){
	'debug'->die(message=> "lst-file on command line was not used in call\n$row in".
		     "restart directory ".$this -> {'directory'});
      }
    } elsif ($row =~ /-lst=/){
      'debug'->die(message=> "No lst-file was specified on command line, but an lst-file was used ".
		   "in call\n$row in restart directory ".$this -> {'directory'});
    }
    if (defined $this->{'msfo_file'}){
      unless ($row =~ /($this->{'msfo'})/){
	'debug'->die(message=> "msfo-file on command line was not used in call\n$row".
		     "in restart directory ".$this -> {'directory'});
      }
    } elsif ($row =~ /-msfo=/){
      'debug'->die(message=> "No msfo-file was specified on command line, but an msfo-file was used ".
		   "in call\n$row in restart directory ".$this -> {'directory'});
    }
    if (defined $this->{'samples'}){
      my $str="-samples=$this->{'samples'}";
      unless ($row =~ /$str\s/){
	'debug'->die(message=> "$str on command line is different from call\n$row".
		     "in restart directory ".$this -> {'directory'});
      }
    }
    if ($this->{'do_estimation'}){
      my $str="-dv=";
      unless (($row =~ /$str/)||($row =~ /=PRED\s/)){
	'debug'->die(message=> "Estimation was not requested by call\n$row".
		     "in restart directory ".$this -> {'directory'}.
		     "\n(i.e. neither -dv option used nor binning/stratifying on PRED)".
		     "\nand therefore cannot be used here.");
      }
    }
    unless ($this->{'dv'} =~ /^(IPRED|IWRES|IRES|RES|WRES|DV)$/ ){
      my $str="-dv=$this->{'dv'}";
      unless ($row =~ /$str/){
	'debug'->die(message=> "$this->{'dv'} is not part of default variable set from estimation,\n".
		     "nor was $str on command line used in call\n$row".
		     "in restart directory ".$this -> {'directory'}.
		     "\nTherefore $this->{'dv'} is not available.");
      }
    }
  }
  
}
end new

# }}}

# {{{ modelfit_setup
start modelfit_setup
{
  my $type='npc';
  if ($self->{'is_vpc'}){
    $type='vpc';
    $self->logfile('vpc.log');
    $self->results_file('vpc_results.csv');
    
  }


  my $model_orig = $self-> {'models'}->[0] -> copy(filename=>$type.'_original.mod',
						   directory=>'m1');

#Always force rerun of original vpc model by removing lst file of original model
#For NPC only force rerun if are reusing old runs, but want to do stratify, 

  if ($self->{'is_vpc'}){
    my $oldlst = $self -> {'directory'}."/m1/vpc_original.lst";
    unlink $oldlst;
    unlink $self -> {'directory'}."NM_run1/psn-1.lst";
    unlink $self -> {'directory'}."NM_run1/psn.lst";
  } else {
    if (defined $self -> {'stratify_on'}){
      my $oldlst = $self -> {'directory'}."/m1/npc_original.lst";
      unlink $oldlst;
      unlink $self -> {'directory'}."NM_run1/psn-1.lst";
      unlink $self -> {'directory'}."NM_run1/psn.lst";
    }
  }

#logic for use of MDV column as basis for finding which rows are observations
#request MDV in $TABLE if no $PRED record or if there is a $PRED record and 
#MDV is in the input
#if there is a $PRED but no MDV in input then all rows will be observations

  my $MDV='';
  if (scalar @{$model_orig-> record( record_name => 'pred' )} < 1){
    $MDV='MDV';
  } else {
    if ($model_orig-> is_option_set(record=>'input',name=>'MDV')){
      my $mdv_val = $model_orig -> get_option_value(record_name => 'input',
						    option_name => 'MDV');
      #have already checked no synonym. If value defined then SKIP/DROP ->don't include
      unless (defined $mdv_val){
	$MDV='MDV'; #MDV in input, no synynom
      }
    }
    #have already checked no synonym. If value defined then SKIP/DROP ->don't include
  }

######## fix $TABLE record: remove any existing. create a new correct one.

  #store $TABLE if FILE=cwtab<>.deriv, i.e. dv is CWRES, add it back to models later
  my @extra_table_record;
  if ($self->{'dv'} eq 'CWRES'){
    my $table_record = $model_orig -> record(record_name => 'table',
					     problem_number => 0);
    
    my $rec_count = scalar(@{$table_record});
    if( $rec_count > 0 ){
      for (my $i=0; $i<$rec_count; $i++){
	@extra_table_record=();
	my $keep=0;
	foreach my $table_line ( @{$table_record -> [$i]} ){
	  push(@extra_table_record,$table_line);
	  if ( $table_line =~ /(cwtab[0-9]*)\.deriv/){
	    $self->dv_table_name($1); 
	    $keep=1;
	  }
	}
	last if ($keep==1);
      }
    }    
  }

  $model_orig -> remove_records(type => 'table');



  my @rec_strings = ('ID',$MDV);
  if (defined $self->{'stratify_on'}){
    push (@rec_strings,$self->{'stratify_on'}) unless ($self->{'stratify_on'} eq 'PRED');
  }
  if ($self->{'is_vpc'}){
    push (@rec_strings,$self->{'bin_on'}) unless ($self->{'bin_on'} eq 'PRED');
  }
  if ($self->{'do_estimation'}){
    unless ($self->{'dv'} =~ /^(IPRED|IWRES|IRES|RES|WRES|DV|CWRES)$/ ){
      push (@rec_strings,$self->{'dv'});
    }
    push (@rec_strings,('IPRED','IWRES','IRES','ONEHEADER','NOPRINT'));
  }else{
    push (@rec_strings,$self->{'dv'});
    push (@rec_strings,('NOAPPEND','ONEHEADER','NOPRINT'));
  }
  push (@rec_strings,('FILE=npctab.dta'));
  $model_orig -> set_records(type => 'table',
			     record_strings => \@rec_strings);

  $model_orig -> add_records(type => 'table',
			     record_strings => \@extra_table_record) 
      if (scalar(@extra_table_record)>0);


  #check if synonyms used for DV/strat/bin. If so, replace reserved variable with synonym.

  my @reserved_labels=('ID','L1','L2','MDV','RAW_','MRG_','RPT_','TIME','DATE');
  push (@reserved_labels,('DAT1','DAT2','DAT3','EVID','AMT','RATE','SS','II','ADDL'));
  push (@reserved_labels,('CMT','PCMT','CALL','CONT','SKIP','DROP'));

  my @check_var;
  push (@check_var,'DV') if ($self->{'dv'} eq 'DV');

  #collect variables to check
  my @cvar;
  push (@cvar,$self->{'stratify_on'}) if ((defined $self->{'stratify_on'})
					  && !($self->{'stratify_on'} =~ /(STRT|PRED)/));
  push (@cvar,$self->{'bin_on'}) if ((defined $self->{'bin_on'}) 
				     && ($self->{'bin_on'} ne 'PRED'));
  #check if strat/bin reserved
  foreach my $var (@cvar){
    foreach my $lab (@reserved_labels){
      if ($var eq $lab){
	push (@check_var,$var) ;
	last;
      }
    }
  }

  foreach my $reserved_name (@check_var){
    my $synonym=$reserved_name;
    if ($model_orig-> is_option_set(record=>'input',name=>$reserved_name)){
      my $value = $model_orig -> get_option_value(record_name => 'input',
						  option_name => $reserved_name);
      if (defined $value){
	unless ($value =~ /(SKIP|DROP)/ ){
	  $synonym=$value;
	}
      } 
    } else {
      my $input_record = $model_orig -> record(record_name => 'input' );
      if( scalar(@{$input_record}) > 0 ){ #always true
	foreach my $line ( @{$input_record -> [0]} ){
	  if ( $line =~ /([\w]+)=([\w]+)[^\w]/ ){
	    unless ($1 =~ /(SKIP|DROP)/ ){ #synonym
	      $synonym=$1 if ($2 eq $reserved_name);
	    }
	    last;
	  }
	}
      }
    }
    if ($synonym ne $reserved_name){
      #found synonym
      if ('DV' eq $reserved_name){
	$self->{'dv'}=$synonym;
      } elsif ($reserved_name eq $self->{'bin_on'}){
	$self->{'bin_on'}=$synonym;
      } elsif ($reserved_name eq $self->{'stratify_on'}){
	$self->{'stratify_on'}=$synonym;
      }
    }
  }

##### fix $ESTIMATION for original model
  
  $model_orig -> set_option(record_name => 'estimation',
			    option_name => 'MAXEVALS',
			    fuzzy_match => 1,
			    option_value => '0');
  
  $model_orig -> remove_option(record_name => 'estimation',
			       option_name => 'MSFO',
			       fuzzy_match => 1);

  if ($self->{'do_estimation'}){
    $model_orig -> set_option(record_name => 'estimation',
			      option_name => 'POSTHOC',
			      fuzzy_match => 1);
  }


##### remove $COVARIANCE
  $model_orig -> remove_records(type => 'covariance');

###update initial estimates/input data, if given###

  if (defined $self->{'lst_file'}){
    #create output object to check that can be parsed correctly, and to 
    #extract data for error checking
    my $outputObject= output -> new(filename => '../'.$self -> {'lst_file'});
    unless ($outputObject->parsed_successfully()){
      'debug'-> die (message=>"lst file $self->{'lst_file'} could not be parsed.");
    }
    #update initial values in model
    $model_orig -> update_inits ( from_output => $outputObject);
    
  } elsif (defined $self->{'msfo_file'}){
    #add msfi record, and remove $THETA, $OMEGA, $SIGMA
    $model_orig -> set_records(type => 'msfi',
			       record_strings => [$self -> {'msfo_file'}]);
    $model_orig -> remove_records(type => 'theta');
    $model_orig -> remove_records(type => 'omega');
    $model_orig -> remove_records(type => 'sigma');
  }

####end update initial estimates############


#####Copy to simulation model

  my $model_sim = $model_orig -> copy(filename=>$type.'_simulation.mod',
				      directory=>'m1');

  $model_orig -> remove_records(type => 'simulation');

  unless ($self->{'do_estimation'}){
    $model_sim -> remove_records(type => 'estimation');
  }

###Create simulation record######

  
  #Check if existing simulation record
  my $sim_record = $model_sim-> record(record_name => 'simulation',problem_number => 0);

  if (scalar (@{$sim_record}) > 0){
    #this option will be set below
    foreach my $altopt ('SUBPROBLEMS','SUBPROBS','NSUBPROBLEMS','NSUBPROBS','NSUBS'){
      #NONMEM accepts a heck of a lot of alternatives...
      $model_sim -> remove_option(record_name => 'simulation',
				  option_name => $altopt,
				  fuzzy_match => 1);
    }

    #unless $do_simulation this option will be set below
    $model_sim -> remove_option(record_name => 'simulation',
				option_name => 'ONLYSIMULATION',
				fuzzy_match => 1);

    if (defined $self->{'msfo_file'}){ #always if $nonp, but even if not $nonp
      my $val= $model_sim -> get_option_value(record_name => 'simulation',
					      option_name => 'TRUE');
      unless ((defined $val)&& ($val eq 'FINAL')){
	'debug'->die(message=>"Error in \$SIMULATION record in modelfile: when using an msfo-file\n".
		     "the option TRUE=FINAL must be set.");

      }
    }

    #find and replace seeds
    my $short_record = $model_sim-> record(record_name => 'simulation',problem_number => 0);
    @rec_strings =();
    my $set_seeds=0;
    #Simply look for numbers. Since got rid of NSUBPROBS this is ok.
    foreach my $sim_line ( @{$short_record -> [0]} ){
      my $new_line;
      while ( $sim_line =~ /(\D*)(\d+)(\D.*)/g ){
	my $seed = random_uniform_integer(1,0,2147483560 );
	$new_line .= "$1$seed";
	$sim_line = $3;
	$set_seeds += 1;
      }
      push (@rec_strings,$new_line.$sim_line);
    }
    if (($set_seeds < 1)||($set_seeds > 2)){
      'debug'->die(message=>"Error in \$SIMULATION record in modelfile: wrong number of seeds.");
    }
  } else {
    my $seed1 = random_uniform_integer(1,0,2147483560 );
    my $seed2 = random_uniform_integer(1,0,2147483560 );
    @rec_strings = ('('.$seed1.')');
    my $np_record = $model_sim -> record(record_name => 'nonparametric' );
    if( scalar(@{$np_record}) > 0 ){
      push (@rec_strings ,'('.$seed2.' NONPARAMETRIC)');
      $model_sim -> remove_records(type => 'nonparametric');
    }
    if (defined $self->{'msfo_file'}){ #always if $nonp, but even if not $nonp
      push (@rec_strings ,'TRUE=FINAL');
    }
  }

  unless ($self->{'do_estimation'}){
    push (@rec_strings,('ONLYSIMULATION'));
  }
  push (@rec_strings,('NSUBPROBLEMS='.$self->{'samples'}));
  $model_sim -> set_records(type => 'simulation',
			    record_strings => \@rec_strings);



###end simulation record######


#det som koer modellerna aer ett modelfit-objekt. Detta maoste skapas explicit.

  $self->{'original_model'} = $model_orig;
  $self->{'simulation_model'} = $model_sim;
  
  push( @{$self -> {'tools'}},
	tool::modelfit ->
	new( reference_object      => $self,
	     models		 => [$model_orig,$model_sim],
	     parent_threads        => 1,
	     raw_results           => undef,
	     prepared_models       => undef,
	     top_tool              => 0,
	     prepend_model_file_name => 1
	     ) );
  
  

}
end modelfit_setup
# }}}

# {{{ _modelfit_raw_results_callback

#start _modelfit_raw_results_callback

#end _modelfit_raw_results_callback

# }}}

# {{{ create_unique_values_hash

start create_unique_values_hash
{
  #in @data_column
  #out %value_hash
  my $value_index = 0;

  foreach my $val (sort {$a <=> $b} @data_column){
    if ($value_index == 0){
      $value_hash{$value_index}=$val;
      $value_index++;
      next;
    }
    unless ($val == $value_hash{($value_index-1)}){
      $value_hash{$value_index}=$val;
      $value_index++;
    }
  }

}
end create_unique_values_hash

# }}}

# {{{ get_bin_boundaries_overlap_count

start get_bin_boundaries_overlap_count
{
  # input value_hash
  # overlap_percent and scalar count
  # data_column, and data_indices
  # return bin_floors, lower boundary not part of upper bin
  # and bin_ceilings, list of upperlimits, upperlimit is part of lower bin!!!

  my $value_index;
  my $n_values = scalar(keys %value_hash);
  my @obs_count=(0) x $n_values;
  my $check_count=0;

  #Count how many values we have in input for each unique value
  for ($value_index=0; $value_index < $n_values;$value_index++){
    foreach my $index (@data_indices){
      if ($data_column[$index] == $value_hash{$value_index}){
	$obs_count[$value_index] +=1;
	$check_count+=1;
      }
    }
  }

  unless ($check_count == scalar(@data_indices)){
    'debug'->die(message=>"Did not find all data values in hash.");
  }

  my $local_error=-$count;
  my $overlap=$count*$overlap_percent/100; 
  my $overlap_error;
  push(@bin_floors,($value_hash{0}));
  $value_index = 0;
  my $prev_start_index=0;

  while ($value_index < $n_values ) {
    if ($obs_count[$value_index] == 0){
      $value_index++; #no observations for this value, skip to next	    
    } elsif ($local_error == -$count){
      #bin empty since error equals initial error, must add observations
      $local_error += $obs_count[$value_index];
      $prev_start_index=$value_index; 
      $value_index++;
    }elsif (abs($local_error)>abs($local_error+$obs_count[$value_index])){
      #bin not empty and adding observations will reduce error
      $local_error += $obs_count[$value_index];
      $value_index++;
    } else {
      #bin not empty and best to switch to new bin
      push(@bin_ceilings,$value_hash{$value_index-1}); #last index was previous index
      $local_error=-$count;
      $overlap_error=-$overlap;
      #compute overlap error if starting at $prev_start_index+1
      for (my $k=($value_index-1);$k>$prev_start_index;$k--){
	$overlap_error += $obs_count[$k]; 
      }	    
      $value_index= $prev_start_index+1; #must have nonzero overlap
      #check if increasing value_index, i.e. moving start of next bin, will reduce overlap errror
      while (abs($overlap_error)>abs($overlap_error-$obs_count[$value_index])){
	$overlap_error -= $obs_count[$value_index];
	$value_index++;
      }
      push(@bin_floors,$value_hash{$value_index-1});
    }
  }
  push(@bin_ceilings,$value_hash{$n_values-1});

}
end get_bin_boundaries_overlap_count

# }}}

# {{{ get_bin_ceilings_from_count

start get_bin_ceilings_from_count
{
  # input value_hash
  # n_bins or list_counts or single_bin_size
  # data_column, and data_indices
  # return bin_ceilings, list of upperlimits, upperlimit is part of lower bin!!!

  my @ideal_count=();
  my $value_index;
  my $n_values = scalar(keys %value_hash);
  my @obs_count=(0) x $n_values;
  my $nonzero_counts=0;


  #Count how many values we have in input for each unique value
  #and how many of the unique values have nonzero count
  for ($value_index=0; $value_index < $n_values;$value_index++){
    foreach my $index (@data_indices){
      if ($data_column[$index] == $value_hash{$value_index}){
	$obs_count[$value_index] +=1;
      }
    }
  }
  foreach my $c (@obs_count){
    if ($c > 0){
      $nonzero_counts++;
    }
  }



  if (defined $n_bins){
    if ($n_bins < 1){
      'debug'->die(message=>"Number of bins must be at least 1.");
    }
    if (scalar(@list_counts)>0){
      'debug'->die(message=>"Cannot input both number of bins and set of bin counts.");
    }

    if ($n_bins >= $nonzero_counts){#one bin per unique value with nonzero obs => no binning
					return;
				      }
    my $count = scalar(@data_indices)/$n_bins;
    @ideal_count = ($count) x $n_bins;
    
  } elsif (defined $single_bin_size){
    #translate to $n_bins
    my $nb = int (scalar(@data_indices)/$single_bin_size); #round down
    $nb++ if ((scalar(@data_indices)/$single_bin_size)-$nb >= 0.5);
    if ($nb >= $nonzero_counts){#one bin per unique value with nonzero obs => no binning
				    return;
				  }
    my $count = scalar(@data_indices)/$nb;
    @ideal_count = ($count) x $nb;	
  } elsif (scalar(@list_counts)>0 && scalar(@list_counts)<$nonzero_counts ){
    #check that requested counts matches total number of observations
    my $checksum=0;
    foreach my $c (@list_counts){
      $checksum += $c;
    }
    unless ($checksum == scalar(@data_indices)){
      'debug'->die(message=>"Sum of observations requested in each bin must equal ".
		   "total number of observations (in strata).");
    }

    push (@ideal_count,@list_counts);
  }  else {
    return;
  }

  my $global_error=0;
  my $bin_index=0;
  my $local_error=-$ideal_count[$bin_index];

  for ($value_index = 0; $value_index < $n_values; $value_index++) {
#	print "index=$value_index obs=$obs_count[$value_index] local=$local_error global=$global_error \n";
    if ($bin_index == $#ideal_count){
      #have reached last bin
      #last upper limit is largest unique value, include all remaining observations
      push(@bin_ceilings,$value_hash{($n_values-1)});
      last;
    }elsif ($local_error == -$ideal_count[$bin_index]){
      #bin empty since error equals initial error
      $local_error += $obs_count[$value_index];
    }elsif ($obs_count[$value_index] == 0){
      next;
    }elsif (abs($global_error+$local_error)>
	    abs($global_error+$local_error+$obs_count[$value_index])
	    && (($n_values-$value_index-1)> ($#ideal_count - $bin_index))){
      #adding observations will reduce global error
      #and more than one unique value remaining per remaining bin.
      $local_error += $obs_count[$value_index];

    } else {
      #bin not empty and best add observations to new bin
      push(@bin_ceilings,$value_hash{$value_index-1});
      $global_error += $local_error;
      $bin_index++;
      $local_error=-$ideal_count[$bin_index]+$obs_count[$value_index];
    }


  }

}

end get_bin_ceilings_from_count

# }}}

# {{{ get_bin_boundaries_overlap_value

start get_bin_boundaries_overlap_value
{
  # @data_column, @data_indices
  # width, overlap_percent
  #return bin_floors, bin_ceilings

  my @local_values=();
  for (my $i=0; $i< scalar(@data_indices); $i++){
    push(@local_values,$data_column[$data_indices[$i]]);
  }
  
  my @temp = sort {$a <=> $b} @local_values;
  my $minval = $temp[0];
  my $maxval = $temp[$#temp];
  my $non_overlap_width=$width*(100-$overlap_percent)/100;

  my $next_ceiling=$minval+$width;
  my $next_floor=$minval;
  while ($next_ceiling < $maxval){
    push(@bin_floors,$next_floor);
    push(@bin_ceilings,$next_ceiling);
    $next_ceiling += $non_overlap_width;
    $next_floor = $next_ceiling - $width;
  }
  push(@bin_floors,$next_floor);
  push(@bin_ceilings,$maxval);

}
end get_bin_boundaries_overlap_value

# }}}

# {{{ get_bin_ceilings_from_value

start get_bin_ceilings_from_value
{
  # @data_column, @data_indices
  # n_bins or list_boundaries or single_bin_size
  #return bin_ceilings, upper limits of intervals, upperlimit part of lower bin

  my @local_values=();
  for (my $i=0; $i< scalar(@data_indices); $i++){
    push(@local_values,$data_column[$data_indices[$i]]);
  }
  
  my @temp = sort {$a <=> $b} @local_values;
  my $minval = $temp[0];
  my $maxval = $temp[$#temp];
  
  if (defined $n_bins){
    if (scalar(@list_boundaries)>0){
      'debug'->die(message=>"Cannot input both number of intervals and set of boundaries");
    }
    my $width = ($maxval-$minval)/$n_bins;
    for (my $i=1; $i<=$n_bins; $i++){
      push (@bin_ceilings,($width*$i+$minval));
    }
  } elsif (defined $single_bin_size){
    #translate to $n_bins
    my $nb = int(($maxval-$minval)/$single_bin_size); #round down
    $nb++ if ((($maxval-$minval)/$single_bin_size)-$nb >= 0.5);
    my $width = ($maxval-$minval)/$nb;
    for (my $i=1; $i<=$nb; $i++){
      push (@bin_ceilings,($width*$i+$minval));
    }

  } elsif (scalar(@list_boundaries)>0){
    push (@bin_ceilings,@list_boundaries);
    if ($maxval > $list_boundaries[$#list_boundaries]){
      push (@bin_ceilings,$maxval);
    }
  }

}
end get_bin_ceilings_from_value

# }}}


# {{{ index_matrix_binned_values

start index_matrix_binned_values
{
  #based on either unique values hash or bin_ceilings, 
  #create array of arrays of data matrix row indices
  #one index array for each unique value
  #input %value_hash, @data_column, @data_indices, 
  #optional input @bin_ceilings, if empty then unique values
  #optional input @bin_floors, only allowed if ceilings given. 
  #If floors empty then ceilings are floors , if given then first value is inclusive, the rest exclusive
  #output index_matrix
  
  my @ceilings=();
  my @floors=();
  if (scalar(@bin_ceilings)>0){
    push(@ceilings,@bin_ceilings);
    if (scalar(@bin_floors)>0){
      push(@floors,@bin_floors);
    }else {
      push(@floors,$value_hash{0});
      for (my $i=0; $i<$#ceilings; $i++){
	push(@floors,$ceilings[$i]);
      }
    }
  }else {
    #check that floors not given as input
    if (scalar(@bin_floors)>0){
      'debug'->die(message=>"Cannot give bin floor input to index_matrix_binned_values ".
		   "unless ceilings given.");
    }
    #no_binning, base on unique values
    my $no_of_values = scalar (keys %value_hash);
    for (my $value_index=0; $value_index<$no_of_values; $value_index++){
      push(@ceilings,$value_hash{$value_index});
    }
    push(@floors,$value_hash{0} - 0.2);
    for (my $i=0; $i<$#ceilings; $i++){
      push(@floors,$ceilings[$i]);
    }
  }

  for (my $bin_index=0; $bin_index<scalar(@ceilings); $bin_index++){
    my @index_row=();
    my $this_floor=$floors[$bin_index];
    $this_floor -= 0.2 if ($bin_index==0); #first floor is inclusive
    foreach my $row_index (@data_indices){
      if ($data_column[$row_index]>$this_floor && 
	  $data_column[$row_index]<=$ceilings[$bin_index] ){
	push (@index_row,$row_index);
      }
    }
    push (@index_matrix,\@index_row);
  }

}
end index_matrix_binned_values

# }}}


# {{{ get_data_matrix

start get_data_matrix
{
  my $signal_file = $self -> {'directory'}."/m1/".$self -> {'dv'}."_matrix_saved";
  my $matrix_saved = ( -e $signal_file ) ? 1 : 0;
  my $no_sim=$self->{'samples'};
  my $type = ($self->{'is_vpc'}) ? 'vpc' : 'npc';

  if ($matrix_saved){
    open( MATRIX, $self -> {'directory'}."/m1/".$self -> {'dv'}."_matrix.csv" ) or 
	'debug'->die(message=> "Could not find saved matrix.");
    my $nsim=0;
    while (my $row = <MATRIX>){
      chomp $row;
      push (@matrix,$row);
      $nsim= (split(/,/,$row)) -1 if ($nsim==0);
    }
    close(MATRIX);
    #check that found correct number of columns in matrix
    unless ($nsim == $no_sim){
      'debug'->die(message=> "Number of simulated data sets $nsim in saved matrix\n".
		   "is different from number $no_sim in input (-samples option).");
    }
    return \@matrix;
  }


  my @datearr=localtime;
  my $the_time=sprintf "%2.2d:%2.2d:%2.2d",$datearr[2],$datearr[1],$datearr[0];
  my $prev_sec=$datearr[2]*3600+$datearr[1]*60+$datearr[0];
  my $this_sec;
  ui -> print (category=>'npc', 
	       message=>"\nReading and formatting $self->{'dv'} data. This can take a while...");

  my $orig_file = (defined $self->{'dv_table_name'}) ? $self->{'dv_table_name'}:
      $self->{'original_model'}-> get_option_value(record_name => 'table',
						   option_name => 'FILE');

  $orig_file = $self->{'original_model'}->directory().$type.'_original.'.$orig_file;

  unless ( -e $orig_file ){
    my $file_to_check = $self -> {'directory'}."NM_run1/compute_cwres.Rout";
    print "\nCheck installation of R and XPose, check file $file_to_check\n" 
	if ($self->{'dv'} eq 'CWRES');
    $file_to_check = $self -> {'directory'}."NM_run1/psn-1.lst";
    'debug'->die(message=> "File $orig_file \nwith table output for original data does not exist. ".
		 "It is recommended to check lst-file $file_to_check for NONMEM error messages.");
  }
  my $d = data -> new(filename=>$orig_file); #läser in allt
  
  my $no_individuals= scalar(@{$d->{'individuals'}});

  my $filt = $d -> create_row_filter('no_individuals'=>$no_individuals);
  my $all_rows_observations = (scalar(@{$filt}) > 0)? 0 : 1;

  #NONMEM only prints first 4 letters of variable name as header
  #exception for CWRES
  my $dv_header = ($self->{'dv'} eq 'CWRES') ? $self->{'dv'} : substr($self->{'dv'},0,4);

  @matrix = @{$d -> column_to_array('column'=>$dv_header,'filter'=>$filt)};
  my $no_observations = scalar(@matrix);
  
  unless ($no_observations >0){
    'debug' -> die(message => "No $self->{'dv'} values found after filtering original data.");
  }
  
  #want to always read whole simulation, i.e. whole table. Compute how many 
  #simulations can be read in each round given this condition and a limit of 400000 lines
  #count lines in original tablefile
  my $linecount=0;
  open(ORIG, $orig_file ) or 'debug'->die(message=> "Could not open tablefile.");
  while (<ORIG>){
    $linecount++;
  }
  close(ORIG);
  my $max_read_sim = ($linecount >= 400000) ? 1 : int (400000/$linecount);
  my $max_ind=$max_read_sim*$no_individuals; #individuals to read in one round

  $filt = undef;
  $d = undef;

  my $sim_file = (defined $self->{'dv_table_name'}) ? $self->{'dv_table_name'}:
      $self->{'simulation_model'}-> get_option_value(record_name => 'table',
						     option_name => 'FILE');
  
  $sim_file = $self->{'simulation_model'}->directory().$type.'_simulation.'.$sim_file;
  unless ( -e $sim_file ){
    my $file_to_check = $self -> {'directory'}."NM_run2/compute_cwres.Rout";
    print "\nCheck installation of R and XPose, check file $file_to_check\n" 
	if ($self->{'dv'} eq 'CWRES');
    $file_to_check = $self -> {'directory'}."NM_run2/psn-1.lst";
    'debug'->die(message=> "File $sim_file \nwith table output for original data does not exist. ".
		 "It is recommended to check lst-file\n$file_to_check \nfor NONMEM error messages.");
  }

  $d = data -> new(filename=>$sim_file,target =>'disk');
  $d -> _read_header();
  
  #read portions of the simdatafiles, until all expected records read
  #build data matrix so that there is one column for each simulation 
  #(real data is first column) and one row for each data point.
  #Store as one-dimensional array of rows, each row is comma-separated string of values
  
  my $skip_tab=0;
  while ( $skip_tab<$no_sim){

    @datearr=localtime;
    $the_time=sprintf "%2.2d:%2.2d:%2.2d",$datearr[2],$datearr[1],$datearr[0];
    $this_sec=$datearr[2]*3600+$datearr[1]*60+$datearr[0];
    unless ($skip_tab == 0){
      my $sec_rem = $self->ceil('number'=>(($no_sim-$skip_tab)/$max_read_sim))*
	  ($this_sec-$prev_sec);
      my $percent_done=int(100*$skip_tab/$no_sim);
      my $minutes_remaining=$self->ceil('number'=>($sec_rem/60));	    
      my $plural = ($minutes_remaining == 1)? '' : 's';
      my $mess="Time $the_time, $percent_done\% done. ".
	  "Approximately $minutes_remaining minute$plural of formatting remaining.";
      ui -> print (category=>'npc', message=>$mess);
    }
    $prev_sec=$this_sec;


#	my $mess=sprintf "Time $the_time. Processing %d to %d out of %d simulated datasets...",
#	($skip_tab+1),
#	(($skip_tab+$max_read_sim)>$no_sim) ? $no_sim :($skip_tab+$max_read_sim),$no_sim;

    $d-> _read_individuals('skip_tables'=>$skip_tab,'max_individuals'=>$max_ind);

    ##error check
    my $tables_to_read= ($max_read_sim > ($no_sim-$skip_tab))? ($no_sim-$skip_tab): $max_read_sim;
    my $individuals_expected = $tables_to_read*$no_individuals;
    unless (scalar(@{$d->{'individuals'}}) == $individuals_expected){
      my $file_to_check = $self -> {'directory'}."NM_run2/psn-1.lst";
      'debug' -> die(message => "Did not find the expected number of individuals to read in \n".
		     "$sim_file. \nA possible cause is that some simulations failed. ".
		     "Check lst-file\n $file_to_check  \nfor error messages. Also check in \n$sim_file \n".
		     "that number of TABLE rows is equal to -samples ".
		     "requested on command-line, and that all TABLE sections contain ".
		     "observations for all individuals.");
    }

    $d -> synced(1);
    
    if ($skip_tab == 0){ #if first read from simdata, create filter
      $filt = $d -> create_row_filter('no_individuals'=>$no_individuals);
      my $test_count=0;
      unless ($all_rows_observations){
	foreach (@{$filt}){
	  $test_count++ if ($_ > 0);
	}
	unless ($test_count == $no_observations){
	  'debug' -> die(message => "Number of observations $test_count after filtering ".
			 "simulated data set were not equal to number after ".
			 "filtering $no_observations original data set.");
	}
      }
    }
    my $column_data = $d -> column_to_array('column'=>$dv_header,'filter'=>$filt);
    
    unless (scalar @{$column_data} >0){
      'debug' -> die(message => "No $self->{'dv'} values found after filtering simulated data portion.");
    }
    
    $d-> flush();
    
    data -> append_columns_to_matrix('matrix'=>\@matrix,'columns'=>$column_data);
    $column_data = undef; 
    $skip_tab += $max_read_sim;
  }

  #check simulations read
  my $no_read_sim= (split(/,/,$matrix[0])) -1;
  unless ($no_sim == $no_read_sim){
    'debug'-> die(message =>"Number of read simulated datasets $no_read_sim is\n".
		  "different from expected number $no_sim.");
  }

  
  #print matrix
  open( MATRIX, ">".$self -> {'directory'}."/m1/".$self -> {'dv'}."_matrix.csv" ) ;
  foreach my $row (@matrix){
    print MATRIX "$row"."\n";
  }
  close (MATRIX);
  #create signal file
  open( DONE, ">".$signal_file ) ;
  my $tmp = $no_sim+1;
  print DONE "Created $no_observations rows by $tmp columns data matrix ".
      "and wrote to file $self -> {'dv'}_matrix.csv\n";
  print DONE "First column is original data, columns 2 to $tmp are simulated data.\n";
  close( DONE );


  ui -> print (category=>'npc', message=> "Done reading and formatting $self->{'dv'} data, finishing run.");

}
end get_data_matrix

# }}}


# {{{ modelfit_analyze
start modelfit_analyze
{

  my $type='npc';
  if ($self->{'is_vpc'}){
    $type='vpc';
  }
  my $matrix = $self -> get_data_matrix();
  my $no_sim= (split(/,/,$matrix ->[0])) -1;
  unless ($no_sim == $self->{'samples'}){
    'debug'-> die(message =>"Number of simulated datasets in matrix file $no_sim is\n".
		  "different from number $self->{'samples'} in input (option -samples).");
  }
  print "$no_sim simulations \n" if ($self->{'verbose'});

  my $no_of_strata = 1;
  my $strat_hash;
  my $strat_array;
  my $strat_matrix;
  my @strata_labels=();
  my $no_observations = scalar(@{$matrix});
  my @all_indices= (0 .. ($no_observations-1));

  if (defined $self -> {'stratify_on'}){
    my $col = $self -> {'stratify_on'};
    #NONMEM only prints first 4 letters of variable name as header
    #no CWRES exception since not allowed stratify on CWRES

    my $strat_header = substr($self->{'stratify_on'},0,4);

    #add new: If ((defined $self->{dv_table_name }) && (not $self->{'dv} eq 'CWRES'))
    #use dv_table_name instead
    #OR require that stratification and binning variables are in vpc-generated table

    my $orig_file =  $self->{'original_model'}->directory().$type.'_original.'.
	$self->{'original_model'}-> get_option_value(record_name => 'table',
						     option_name => 'FILE');
    
    my $d = data -> new(filename=>$orig_file); #läser in allt

    my $no_individuals= scalar(@{$d->{'individuals'}});
    my $filt = $d -> create_row_filter('no_individuals'=>$no_individuals);

    $strat_array = $d -> column_to_array('column'=>$strat_header,'filter'=>$filt);
    unless (scalar @{$strat_array} > 0){
      'debug'-> die(message=>"Could not find column to stratify on in original data file.");
    }
    unless (scalar @{$strat_array} == $no_observations){
      'debug'-> die(message=>"Number of observations in stratification column after filtering ".
		    "does not match number of observations in matrix file.");
    }

    $strat_hash = $self -> create_unique_values_hash('data_column'=>$strat_array);
    #compute strata limits. Currently only no_of_strata based on counts allowed.
    #ok with undefined no_of_strata, will return empty array, which 
    #index_matrix_binned_values interprets as stratify on unique values.

    my $strata_ceilings = $self -> get_bin_ceilings_from_count('data_column'=>$strat_array,
							       'value_hash'=>$strat_hash,
							       'data_indices'=>\@all_indices,
							       'n_bins'=>$self->{'no_of_strata'});
    $strat_matrix = $self -> index_matrix_binned_values('data_column'=>$strat_array,
							'value_hash'=>$strat_hash,
							'data_indices'=>\@all_indices,
							'bin_ceilings'=>$strata_ceilings);

    #create label array for printed report.
    if (defined $self->{'no_of_strata'} && $self->{'no_of_strata'}< scalar(keys %{$strat_hash})){
      $no_of_strata=$self->{'no_of_strata'};
      my $low= sprintf "[%g;",${$strat_hash}{0};
      foreach my $high (@{$strata_ceilings}){
	my $lab = sprintf "strata $col %s %g]",$low,$high;
	push (@strata_labels,$lab);
	$low=sprintf "(%g;",$high;
      }
    } else{
      $no_of_strata = scalar (keys %{$strat_hash});
      for (my $strat_ind=0; $strat_ind<$no_of_strata; $strat_ind++){
	my $lab = sprintf "strata $col = %g",${$strat_hash}{$strat_ind};
	push (@strata_labels,$lab);
      }
    }


    if ($self->{'verbose'}){
      print "\n strata $no_of_strata\n";
      foreach my $k (keys %{$strat_hash}){
	print "key $k and value ${$strat_hash}{$k}\n";
      }
    }
    $d-> flush();
  } else {
    $strat_matrix = [\@all_indices];
    push (@strata_labels," ");

  }

  
  if ($no_of_strata > 10){
    ui -> print (category=>'npc', message=>"Warning: The number of stratification levels is $no_of_strata");
  }

  if ($self->{'is_vpc'}){
    $self->vpc_analyze('matrix'=>$matrix,
		       'strat_matrix'=>$strat_matrix,
		       'strata_labels'=>\@strata_labels,
		       'strata_variable'=> $strat_array);
  } else {
    $self->npc_analyze('matrix'=>$matrix,
		       'strat_matrix'=>$strat_matrix,
		       'strata_labels'=>\@strata_labels);
  }


}
end modelfit_analyze
# }}} modelfit_analyze

# {{{ round
start round
{
  my $floor=int($number);
  my $rem=$number-$floor;
  if ($rem >= 0){
    $integer_out = ($rem >= 0.5)? $floor+1 : $floor;
  } else {
    $integer_out = (abs($rem) >= 0.5)? $floor-1 : $floor;
  }

}
end round
#}}} round

# {{{ ceil
start ceil
{
  my $floor=int($number);
  my $rem=$number-$floor;
  if ($rem > 0){
    $integer_out = $floor+1;
  } else {
    #equal or  neg
    $integer_out = $floor;
  } 

}
end ceil
#}}} ceil

# {{{ median
start median
{
  my $len= scalar( @{$sorted_array} );

  if( $len  % 2 ){
    $result = $sorted_array->[($len-1)/2];
  } else {
    $result = ($sorted_array->[$len/2]+$sorted_array->[($len-2)/2])/ 2;
  }
}
end median
#}}} median

# {{{ vpc_analyze
start vpc_analyze
{
  #input refs strat_matrix, strata_labels(fix no_of_strata from labels)
  #matrix (fix no_observations and no_sim from matrix)
  
  my $no_sim= (split(/,/,$matrix ->[0])) -1;
  my $no_observations = scalar(@{$matrix});
  my $no_of_strata = scalar(@{$strata_labels});

  
  #Recreate basic observation filter, and hash for binning. Copy paste from modelfit_analyze

  my $col = $self -> {'bin_on'};
  #NONMEM only prints first 4 letters of variable name as header
  #no exception for CWRES since not allowed to bin on CWRES
  my $bin_header = substr($self->{'bin_on'},0,4);

  #add new: If ((defined $self->{dv_table_name }) && (not $self->{'dv} eq 'CWRES'))
  #use dv_table_name instead
  #OR require that stratification and binning variables are in vpc-generated table

  my $orig_file =  $self->{'original_model'}->directory().'vpc_original.'.
      $self->{'original_model'}-> get_option_value(record_name => 'table',
						   option_name => 'FILE');
  
  my $d = data -> new(filename=>$orig_file); #läser in allt

  my $no_individuals= scalar(@{$d->{'individuals'}});
  my $filter = $d -> create_row_filter('no_individuals'=>$no_individuals);

  my $bin_array = $d -> column_to_array('column'=>$bin_header,'filter'=>$filter);
  unless (scalar @{$bin_array} > 0){
    'debug'-> die(message=>"Could not find column to bin on in original data file.");
  }
  unless (scalar @{$bin_array} == $no_observations){
    'debug'-> die(message=>"Number of observations in binning column after filtering ".
		  "does not match number of observations in matrix file.");
  }

###### print data for VPC plots, prepare mirror parameters
  #add ID 
  my $id_array = $d -> column_to_array('column'=>'ID','filter'=>$filter);
  unless (scalar @{$id_array} > 0){
    'debug'-> die(message=>"Could not find ID column in original data file.");
  }
  unless (scalar @{$id_array} == $no_observations){
    'debug'-> die(message=>"Number of observations in ID column after filtering ".
		  "does not match number of observations in matrix file.");
  }

  #add translated stratacol
  my @translated_strata = (0) x $no_observations;
  for (my $strat_ind=0; $strat_ind<$no_of_strata; $strat_ind++){
    foreach my $j (@{$strat_matrix->[$strat_ind]}){
      $translated_strata[$j] = ($strat_ind +1);
    }
  }

  #filename vpctabN - find number N
  my $tabno='';
  if ($self -> {'models'}->[0]->filename() =~ /run(\d+)\.mod/){
    $tabno=$1;
  }

  
  #mirrors
  my @mirror_set=();
  my @mirror_labels=();
  if (defined $self->{'mirrors'}){
    my @rand_indices=random_permuted_index($self -> {'samples'});
    @mirror_set = @rand_indices[0 .. ($self->{'mirrors'}-1)];
    for my $j (1 .. $self->{'mirrors'}){
      push (@mirror_labels,"mirror_$j");
    }
  }

  open( ST, ">".$self -> {'directory'}."/vpctab".$tabno);
  #header row
  print ST "ID,".$self->{'dv'}.",".$self->{'bin_on'};
  print ST ",strata_no,".$self->{'stratify_on'} if (defined $self->{'stratify_on'});
  foreach my $j (@mirror_labels){
    print ST ",$j";
  }
  print ST "\n";

  for (my $i=0;$i<$no_observations;$i++){ 
    my $idno = sprintf "%d",$id_array->[$i] ;
    my $row = $matrix->[$i];
    my $idv=$bin_array->[$i];
    my $orig_value;
    my @tmp;
    ($orig_value,@tmp) = split(/,/,$row); 
    print ST "$idno,$orig_value,$idv";
    print ST ",$translated_strata[$i],".$strata_variable->[$i] if (defined $self->{'stratify_on'});
    foreach my $j (@mirror_set){
      print ST ",$tmp[$j]";
    }
    print ST "\n";
  }
  close (ST);

#################

  my $bin_hash = $self -> create_unique_values_hash('data_column'=>$bin_array);


  my @pred_int = sort {$a <=> $b} 0,40,80,90,95;

  my @perc_limit;
  foreach my $pi (@pred_int){
    if ($pi == 0){
      push (@perc_limit,50);
    }else {
      push (@perc_limit,(100-$pi)/2);
      push (@perc_limit,(100-(100-$pi)/2));
    }
  }

  my $no_perc_limits = scalar(@perc_limit);
  my @limit = (0) x $no_perc_limits;
  my @lower_limit_ci = (0) x $no_perc_limits;
  my @upper_limit_ci = (0) x $no_perc_limits;
  my @limit_real = (0) x $no_perc_limits;
  my @limit_index= (0) x $no_perc_limits;
  my @limit_singlesim;
  for (my $i=0; $i<$no_perc_limits; $i++){
    @{$limit_singlesim[$i]} = (0) x ($no_sim);
  }
  my @limit_mirrors;
  if (defined $self->{'mirrors'}){
    for (my $i=0; $i<$no_perc_limits; $i++){
      @{$limit_mirrors[$i]} = (0) x ($self->{'mirrors'});
    }
  }


  my $c_i=95;
  my $low_ci_ind = $self->round('number'=>((100-$c_i)*($no_sim-1)/200));
#    my $low_ci_ind = int(((100-$c_i)*($no_sim-1)/200)+0.5); #index of start of ci % interval
  my $high_ci_ind = $no_sim - $low_ci_ind - 1; #index of end  of  c_i% interval


  ## Prepare general run info for output file, and bin labels
  my %return_section;
  $return_section{'name'} = 'VPC run info';
  my $modelname=$self-> {'models'}->[0] ->filename();
  my $extra_value=$modelname;
  if (defined $self->{'msfo_file'}){
    $extra_value='msfo-file '.$self->{'msfo_file'};
  } elsif (defined $self->{'lst_file'}){
    $extra_value='lst-file '.$self->{'lst_file'};
  }

  $return_section{'labels'} = [[],['Date','observations','simulations','Modelfile',
				   'parameter values from','Independent variable','Dependent variable',
				   'PsN version','NONMEM version']];
  my @datearr=localtime;
  my $the_date=($datearr[5]+1900).'-'.($datearr[4]+1).'-'.($datearr[3]);
  
  $return_section{'values'} = [[$the_date,$no_observations,$no_sim,$modelname,$extra_value,
				$self->{'bin_on'},$self->{'dv'},$PsN::version,$self->{'nm_version'}]];
  push( @{$self -> {'results'}[0]{'own'}},\%return_section );

  my @result_column_labels=("< $col",'<=','no. of obs');
  for (my $i=0; $i<$no_perc_limits; $i++){
    push (@result_column_labels,"$perc_limit[$i]\% real");
    foreach my $lab (@mirror_labels){
      push (@result_column_labels,"$perc_limit[$i]\% ".$lab);
    }
    push (@result_column_labels,"$perc_limit[$i]\% sim","$c_i\%CI for $perc_limit[$i]\% from",
	  "$c_i\%CI for $perc_limit[$i]\% to");
  }

  #For VPC diagnostics

  my $npc_pi_offset=0;
  foreach my $pi (@pred_int){
    if ($pi == 0){
      $npc_pi_offset=1;
    }else {
      push (@result_column_labels,"PI $pi\% False pos (\%)","PI $pi\% False neg (\%)")
	}
  }

  my ($npc_lower_index,$npc_upper_index,$npc_low_ind,$npc_high_ind);
  
  ($npc_lower_index,$npc_upper_index,$npc_low_ind,$npc_high_ind)= 
      $self->get_npc_indices('ci' => $c_i,
			     'no_sim' => $no_sim,
			     'pred_intervals' => \@pred_int);

  my ($npc_result_column_labels,$npc_result_row_labels);
  ($npc_result_column_labels,$npc_result_row_labels) = 
      $self->get_npc_result_labels('ci' => $c_i,'pred_intervals' => \@pred_int);
  my @npc_result_labels = ($npc_result_row_labels,$npc_result_column_labels);
#    my @npc_section_array; have insdie strata loop

  #end prep VPC diagnostics

  ##done general run info and labels

  #Loop over strata.
  #Do the binning. Copy paste from modelfit_analyze stratification, 
  #very similar but more options and parameters...

  for (my $strat_ind=0; $strat_ind<$no_of_strata; $strat_ind++){
    #report strata header, use labels...
    my @result_row_labels = ('first interval is closed');
    my %return_section;
    my @result_values=();
    my $no_strata_obs=scalar(@{$strat_matrix->[$strat_ind]});
    my $result_name = "\nVPC results ".$strata_labels -> [$strat_ind].
	"\n$no_strata_obs observations out of $no_observations";
    $return_section{'name'} = $result_name;
    my @npc_section_array; #possibly move out again


    my ($bin_floors,$bin_ceilings);
    if ($self->{'bin_by_count'} eq '1') {
#	    print "Binning by count\n";
      if (defined $self->{'overlap_percent'}){
	($bin_floors,$bin_ceilings) = 
	    $self -> get_bin_boundaries_overlap_count('data_column'=>$bin_array,
						      'value_hash'=>$bin_hash,
						      'data_indices'=>$strat_matrix->[$strat_ind],
						      'count'=>$self->{'single_bin_size'},
						      'overlap_percent' =>$self->{'overlap_percent'});
      } else {
	#add input param single_bin_size
	$bin_ceilings = 
	    $self -> get_bin_ceilings_from_count('data_column'=>$bin_array,
						 'value_hash'=>$bin_hash,
						 'data_indices'=>$strat_matrix->[$strat_ind],
						 'n_bins'=>$self->{'no_of_bins'},
						 'single_bin_size'=>$self->{'single_bin_size'},
						 'list_counts' =>$self->{'bin_array'});
      }
    } elsif ($self->{'bin_by_count'} eq '0') {
      if (defined $self->{'overlap_percent'}){
	($bin_floors,$bin_ceilings) = 
	    $self -> get_bin_boundaries_overlap_value('data_column'=>$bin_array,
						      'data_indices'=>$strat_matrix->[$strat_ind],
						      'width'=>$self->{'single_bin_size'},
						      'overlap_percent' =>$self->{'overlap_percent'});
      } else {
	#add input param single_bin_size
	$bin_ceilings = 
	    $self -> get_bin_ceilings_from_value('data_column'=>$bin_array,
						 'data_indices'=>$strat_matrix->[$strat_ind],
						 'n_bins'=>$self->{'no_of_bins'},
						 'single_bin_size'=>$self->{'single_bin_size'},
						 'list_boundaries' =>$self->{'bin_array'});
      }
    }
    my $bin_matrix = $self -> index_matrix_binned_values('data_column'=>$bin_array,
							 'value_hash'=>$bin_hash,
							 'data_indices'=>$strat_matrix->[$strat_ind],
							 'bin_floors'=>$bin_floors,
							 'bin_ceilings'=>$bin_ceilings);
    my $no_bins = scalar(@{$bin_matrix});

###
    if (defined $bin_ceilings ){
      unless (defined $bin_floors ){
	$bin_floors->[0]=${$bin_hash}{0};
	for (my $bi=1; $bi<$no_bins; $bi++){
	  $bin_floors->[$bi]=$bin_ceilings->[$bi-1];
	}
      }
    } else{ #unique value binning
      for (my $bi=0; $bi<$no_bins; $bi++){
	$bin_ceilings->[$bi]=${$bin_hash}{$bi};
#		my $lab = sprintf "\n$type results bin $col = %g",${$bin_hash}{$bi};
#		push (@bin_labels,$lab);
      }
    }
    

###



    my @sum_falsepos= (0) x scalar(@pred_int);
    my @sum_falseneg= (0) x scalar(@pred_int);
    my $sum_no_obs=0;

    for (my $bin_index=0;$bin_index<$no_bins;$bin_index++){
      #Loop over bins. Check URS
      
      #push (@result_row_labels,' ') unless ($bin_index==0); #avoid NA in labels

      my $no_simvalues=0;
      my @simvalues=();
      my $orig_value;
      my @tmp=();
      my @singleset=();
      my $no_bin_observations=0;

      #a) pool all simulated values in bin
      #sort simvalues, compute indices for this particular $no_simvalues;
      #and save perclimit
      foreach my $row_index (@{$bin_matrix->[$bin_index]}){
	my $row = $matrix->[$row_index];
	($orig_value,@tmp) = split(/,/,$row); 
	push(@simvalues,@tmp);
	push(@singleset,$orig_value);
	$no_simvalues += $no_sim;
	$no_bin_observations++;
      }
      next if ($no_bin_observations == 0);

      my @sorted_sim_values = sort {$a <=> $b} @simvalues;
      for (my $i=0; $i<$no_perc_limits; $i++){
	if ($perc_limit[$i]==50){
#		if (int($limit_index[$i]+0.5)>int($limit_index[$i])){
	  #take median
	  $limit[$i]= $self->median('sorted_array' => \@sorted_sim_values);
#			($sorted_sim_values[int($limit_index[$i]+0.5)]+$sorted_sim_values[int($limit_index[$i])])/2;
	}else{
	  $limit_index[$i] = $self->round('number'=>$perc_limit[$i]*($no_simvalues-1)/100);
	  $limit[$i]=$sorted_sim_values[$limit_index[$i]];
	}
      }	    

      for (my $i=0; $i<$no_perc_limits; $i++){
	$limit_index[$i] = $self->round('number'=>($perc_limit[$i]*($no_bin_observations-1)/100));
      }


      ##VPC diagnostics. Before b,c) because need singleset of real values
      
      #outside bin-loop: call get_npc_indices with correct PI:s and no_sim (and ci)
      #here call subset_npc_analyze with this bin
      my ($npc_result_values,$npc_realpos,$npc_stats_warnings);
      ($npc_result_values,$npc_realpos,$npc_stats_warnings)=
	  $self->subset_npc_analyze('matrix' => $matrix,
				    'row_indices' => $bin_matrix->[$bin_index],
				    'low_ind' => $npc_low_ind,
				    'high_ind' => $npc_high_ind,
				    'lower_index' => $npc_lower_index,
				    'upper_index' => $npc_upper_index, 
				    'pred_intervals' => \@pred_int);
      #realpos has one arrayref per PI, each array one integer per obs. -1 below, 0 in, 1 above
      
      my @diagnostics=();
      #the offset is to skip interval 50-50 which is of course empty
      for (my $i=$npc_pi_offset; $i<scalar(@pred_int); $i++){
	my $false_pos=0;
	my $false_neg=0;
	for (my $j=0; $j<$no_bin_observations; $j++){
	  if ($npc_realpos->[$i]->[$j] == 0){
	    #NPC inside
	    if (($singleset[$j] < $limit[($i*2-$npc_pi_offset)]) ||
		($singleset[$j] > $limit[($i*2-$npc_pi_offset+1)])){
	      #VPC outside
	      $false_pos++;
	    }
	  } else {
	    #NPC outside
	    if (($singleset[$j] >= $limit[($i*2-$npc_pi_offset)]) &&
		($singleset[$j] <= $limit[($i*2-$npc_pi_offset+1)])){
	      #VPC inside
	      $false_neg++;
	    }
	  }
	}
	push (@diagnostics,($false_pos*100/$no_bin_observations,$false_neg*100/$no_bin_observations));
	$sum_falsepos[$i] += $false_pos;
	$sum_falseneg[$i] += $false_neg;
      }
      $sum_no_obs += $no_bin_observations;

      my %npc_return_section;
      $npc_return_section{'name'} = "\nNPC results ".$strata_labels -> [$strat_ind].
	  "\nbin ".($bin_index+1).": ".$no_bin_observations." observations";
      $npc_return_section{'labels'} = \@npc_result_labels;
      $npc_return_section{'values'} = $npc_result_values;
      push( @npc_section_array,\%npc_return_section );


      #end VPC diagnostics (print below)


      #b) compute PI:s for original, sort values and save lower and upper limits
      my @sorted_singleset = sort {$a <=> $b} @singleset;

      for (my $i=0; $i<$no_perc_limits; $i++){
	if ($perc_limit[$i]==50){
	  $limit_real[$i] = $self->median('sorted_array' => \@sorted_singleset);
#		if (int($limit_index[$i]+0.5)>int($limit_index[$i])){
#		    $limit_real[$i] = 
#			($sorted_singleset[int($limit_index[$i]+0.5)]+$sorted_singleset[int($limit_index[$i])])/2;
	}else{
	  $limit_real[$i] = $sorted_singleset[$limit_index[$i]];
	}
      }


      #c) compute PI:s for each simset. Use clever indexing in @simvalues to extract values.
      #then determine and save ci:s for each PI boundary	    
      for (my $col=0;$col<$no_sim; $col++){
	@singleset=();
	for (my $row=0; $row<$no_bin_observations; $row++){
	  push(@singleset,$simvalues[$col+$row*$no_sim]);
	}
	@sorted_singleset = sort {$a <=> $b} @singleset;
	for (my $i=0; $i<$no_perc_limits; $i++){
	  if ($perc_limit[$i]==50){
	    $limit_singlesim[$i]->[$col]  = $self->median('sorted_array' => \@sorted_singleset);
#		    if (int($limit_index[$i]+0.5)>int($limit_index[$i])){
#			$limit_singlesim[$i]->[$col] = 
#			    ($sorted_singleset[int($limit_index[$i]+0.5)]+$sorted_singleset[int($limit_index[$i])])/2;
	  }else{
#			$limit_singlesim[$i]->[$col] = $sorted_singleset[int($limit_index[$i])];
	    $limit_singlesim[$i]->[$col] = $sorted_singleset[$limit_index[$i]];
	  }
	}
      }

      #for mirror plots
      if (defined $self->{'mirrors'}){
	for my $mi (0 .. ($self->{'mirrors'}-1)){
	  my $col = $mirror_set[$mi];
	  for (my $i=0; $i<$no_perc_limits; $i++){
	    $limit_mirrors[$i]->[$mi]=$limit_singlesim[$i]->[$col];
	  }
	  $mi++;
	}
      }

      for (my $i=0; $i<$no_perc_limits; $i++){
	my @val_arr = sort {$a <=> $b} @{$limit_singlesim[$i]};
	$lower_limit_ci[$i] = $val_arr[$low_ci_ind];
	$upper_limit_ci[$i] = $val_arr[$high_ci_ind];		
      }

      my $st= ' ';
      $st = $bin_floors->[$bin_index] if (defined $bin_floors );
      my @result_row_values=($st,$bin_ceilings->[$bin_index],$no_bin_observations);

      for (my $i=0; $i<$no_perc_limits; $i++){
	push (@result_row_values,$limit_real[$i]);
	for my $mi (0 .. ($self->{'mirrors'}-1)){
	  push (@result_row_values,$limit_mirrors[$i]->[$mi]);
	}
	push (@result_row_values,($limit[$i],$lower_limit_ci[$i],$upper_limit_ci[$i]));
      }

      #VPC diagnostics
      push (@result_row_values,@diagnostics);
      #end diagnostics
      

      push (@result_values,\@result_row_values);

    } #end loop over bins

    my @result_labels = (\@result_row_labels,\@result_column_labels);
    $return_section{'labels'} = \@result_labels;
    $return_section{'values'} = \@result_values;
    push( @{$self -> {'results'}[0]{'own'}},\%return_section );

    my @diag_labels=();
    my @diag_vals=();
    
    my %diag_section;
    $diag_section{'name'}="\nDiagnostics VPC ".$strata_labels -> [$strat_ind];
    for (my $i=$npc_pi_offset; $i<scalar(@pred_int); $i++){
      push (@diag_labels,"PI $pred_int[$i]\% ");
      push (@diag_vals,[$sum_falsepos[$i]*100/$sum_no_obs,$sum_falseneg[$i]*100/$sum_no_obs]);
    }
    $diag_section{'labels'}=[\@diag_labels,["False pos (\%)","False neg (\%)"]];
    $diag_section{'values'}=\@diag_vals;
    push( @{$self -> {'results'}[0]{'own'}},\%diag_section );
    push( @{$self -> {'results'}[0]{'own'}},@npc_section_array);

  } #end loop over strata


}
end vpc_analyze
# }}} vpc_analyze


# {{{ subset_npc_analyze
start subset_npc_analyze
{
  #start function, in is row_indices,matrix,lower_index,upper_index,
  #pred_intervals,$high_ind,$low_ind
  #out is ref to result_values and real_positions and stats_warnings
  #npoints not needed, length of input @row_indices

  my $no_sim= (split(/,/,$matrix ->[0])) -1;
  my $point_counter=scalar(@{$row_indices});
  if ($point_counter < 1){ #empty set of observations
    return;
  }

  #need to guarantee sorted pred_ints
  my @pred_int = sort {$a <=> $b} @{$pred_intervals};
  my $no_pred_ints = scalar(@pred_int);
  my @upper_limit = (0) x $no_pred_ints;
  my @lower_limit = (0) x $no_pred_ints;
  my (@values,@sorted_sim_values);
  my $orig_value;
  my @sum_warnings = (0) x ($no_sim+1);
  my (@upper_count, @lower_count);
  for (my $i=0; $i<$no_pred_ints; $i++){
    @{$upper_count[$i]} = (0) x ($no_sim+1);
    @{$lower_count[$i]} = (0) x ($no_sim+1);    
    @{$real_positions[$i]} = (0) x ($point_counter);    
  }

  my $obs_counter=0;

#    print "\n\n\n";
  foreach my $row_index (@{$row_indices}){
    my $row = $matrix->[$row_index];
    ($orig_value,@values) = split(/,/,$row); 

    @sorted_sim_values = sort {$a <=> $b} @values; #sort numerically ascending
    
    if ($self->{'verbose'}){
      printf "\n%.3f ",$orig_value;
      foreach my $v (@values){
	printf "%.3f ",$v;
      }
      printf "\n%.3f ",$orig_value;
      foreach my $v (@sorted_sim_values){
	printf "%.3f ",$v;
      }
      print "\n\n";
    }
    
    for (my $i=0; $i<$no_pred_ints; $i++){
      $lower_limit[$i] = $sorted_sim_values[($lower_index->[$i])];
      $upper_limit[$i] = $sorted_sim_values[($upper_index->[$i])];	
    }
    
    unshift @values,($orig_value); #put original value back at the beginning
    
    #Since the pred_ints are sorted in ascending order (e.g 80, 90, 95),
    #if value is below limit of first prediction interval, cannot be above
    #limit of any interval. Then if value is not below limit of 2nd, 3rd... 
    #interval it cannot be below limit of any of the remaining.
    
    for (my $j=0; $j<= $no_sim; $j++){
      if ($values[$j] < $lower_limit[0]){
	$lower_count[0]->[$j] +=1;
	for (my $i=1; $i<$no_pred_ints; $i++){
	  if ($values[$j] < $lower_limit[$i]){
	    $lower_count[$i]->[$j] +=1;
	  } else {
	    last; #goto next column (next value in @values)
	  }
	}
      } elsif ($values[$j] > $upper_limit[0]){
	$upper_count[0]->[$j] +=1;
	for (my $i=1; $i<$no_pred_ints; $i++){
	  if ($values[$j] > $upper_limit[$i]){
	    $upper_count[$i]->[$j] +=1;
	  } else {
	    last; #goto next $value
	  }
	}
      }

    }


    ##For VPC diagnostics: build integer matrix w/ under/above/in info for real data only
    # 0 means inside.

    if ($values[0] < $lower_limit[0]){
      $real_positions[0]->[$obs_counter]=-1;
      for (my $i=1; $i<$no_pred_ints; $i++){
	if ($values[0] < $lower_limit[$i]){
	  $real_positions[$i]->[$obs_counter]=-1;
	} else {
	  last;
	}
      }
    } elsif ($values[0] > $upper_limit[0]){
      $real_positions[0]->[$obs_counter]= 1;
      for (my $i=1; $i<$no_pred_ints; $i++){
	if ($values[0] > $upper_limit[$i]){
	  $real_positions[$i]->[$obs_counter]= 1;
	} else {
	  last;
	}
      }
    }


    $obs_counter++;
    ### end VPC diagnostics
    
    if ($self->{'verbose'}){
      for (my $i=0; $i<$no_pred_ints; $i++){
	printf "%.0f %.3f %.3f\n",$pred_int[$i],$lower_limit[$i],$upper_limit[$i] if ($self->{'verbose'});
	for (my $j=0; $j<= $no_sim; $j++){
	  print "$lower_count[$i]->[$j] ";
	}
	print "\n";
	for (my $j=0; $j<= $no_sim; $j++){
	  print "$upper_count[$i]->[$j] ";
	}
	print "\n\n";
      }
      print "\n";
    }
  } #end foreach rowindex 

  my $const=100/$point_counter; #conversion from count to percent
  
  print "$low_ind $high_ind $const\n" if ($self->{'verbose'});
  
  #loop over prediction intervals, compute results for each interval
  for (my $i=0; $i<$no_pred_ints; $i++){
    my $warn=' '; 
    my $realcount = shift @{$lower_count[$i]};
    my @count_arr = sort {$a <=> $b} @{$lower_count[$i]};
    
    if (( $realcount<$count_arr[$low_ind]) ||($realcount>$count_arr[$high_ind])){
      $warn='*';
      $sum_warnings[0] += 1; #NPC diagnostics
    }

    #For NPC diagnostics:
    for (my $si=0; $si<$no_sim; $si++){
      if (( $lower_count[$i]->[$si] < $count_arr[$low_ind]) ||($lower_count[$i]->[$si] > $count_arr[$high_ind])){
	$sum_warnings[(1+$si)] += 1; # +1 because 0 is real data.
      }
    }
    #end NPC diagnostics


    my @result_row_values =($realcount,$realcount*$const,$warn,$count_arr[$low_ind]*$const,
			    $count_arr[$high_ind]*$const);
    if ($self->{'verbose'}){
      print "$realcount ";
      for (my $j=0; $j<= $no_sim; $j++){
	print "$lower_count[$i]->[$j] ";
      }
      print "\n  ";
      for (my $j=0; $j<$no_sim; $j++){
	print "$count_arr[$j] ";
      }
      print "\n\n";
    }
    
    $warn=' ';
    $realcount = shift @{$upper_count[$i]};
    @count_arr = sort {$a <=> $b} @{$upper_count[$i]};
    unless (scalar(@count_arr)==$no_sim){  #kolla no_sim vaerden!
      'debug' -> die(message => "Crash.");
    }
    
    if (( $realcount<$count_arr[$low_ind]) ||($realcount>$count_arr[$high_ind])){
      $warn='*';
      $sum_warnings[0] += 1; #NPC diagnostics
    }
    #For NPC diagnostics:
    for (my $si=0; $si<$no_sim; $si++){
      if (( $upper_count[$i]->[$si] < $count_arr[$low_ind]) ||($upper_count[$i]->[$si] > $count_arr[$high_ind])){
	$sum_warnings[(1+$si)] += 1; # 1+ because 0 is real data
      }
    }
    #end NPC diagnostics
    
    push (@result_row_values,($realcount, $realcount*$const,$warn,$count_arr[$low_ind]*$const,
			      $count_arr[$high_ind]*$const));
    
    if ($self->{'verbose'}){
      print "$realcount ";
      for (my $j=0; $j<= $no_sim; $j++){
	print "$upper_count[$i]->[$j] ";
      }
      print "\n  ";
      for (my $j=0; $j<$no_sim; $j++){
	print "$count_arr[$j] ";
      }
      print "\n\n";
    }

    push (@result_values,\@result_row_values);

  } #end loop over prediction intervals

  #warning statistics
  my $i = shift @sum_warnings; #real count
  push(@stats_warnings,$i); 
  my $sum_sums=0;
  foreach my $i (@sum_warnings){
    $sum_sums += $i;
  } 
  push(@stats_warnings,$sum_sums/$no_sim); #mean
  #theoretical mean, 
  #number of PI times times 2*2 for above and below lower and upper PI limit
  #number absolute number outside a limit ($low_ind) div by total number of values
  push(@stats_warnings,($no_pred_ints*2*2*$low_ind/$no_sim));

  my @sorted_sums = sort {$a <=> $b} @sum_warnings;
#    print "\n $sum_sums $low_ind $high_ind\n";
#    foreach my $i (@sorted_sums){
#	print "$i ";
#    } 
#    print "\n";

  #median
  push(@stats_warnings,$self->median('sorted_array'=>\@sorted_sums));
#
#    if( scalar( @sorted_sums ) % 2 ){
#	push(@stats_warnings,$sorted_sums[$#sorted_sums/2]);
#    } else {
#	push(@stats_warnings,($sorted_sums[@sorted_sums/2]+$sorted_sums[(@sorted_sums-2)/2]) / 2);
#    }

  push(@stats_warnings,$sorted_sums[$low_ind]); #start 95% CI
  push(@stats_warnings,$sorted_sums[$high_ind]); #end 95% CI

}
end subset_npc_analyze
# }}} subset_npc_analyze

# {{{ get_npc_indices
start get_npc_result_labels
{
  #in pred_intervals ci
  #out result_column_labels result_row_labels

  my @pred_int = sort {$a <=> $b} @{$pred_intervals};
  my $no_pred_ints = scalar(@pred_int);

  @result_column_labels=('points below PI (count)','points below PI (%)','outside CI for below PI',
			 "$ci\% CI below: from (\%)","$ci\% CI below: to (\%)" ); 
  push (@result_column_labels,('points above PI (count)','points above PI (%)','outside CI for above PI',
			       "$ci\% CI above: from (\%)","$ci\% CI above: to (\%)"));
  
  for (my $i=0; $i<$no_pred_ints; $i++){
    push (@result_row_labels,"$pred_int[$i]% PI");
  }

}
end get_npc_result_labels
# }}} get_npc_result_labels

# {{{ get_npc_indices
start get_npc_indices
{
  #in pred_intervals ci no_sim
  #out lower_index upper_index low_ind high_ind
  
  #For each prediction interval pred_int: Compute lower_index, i.e. index 
  #of first value in sorted array which is inside this interval. 
  #Compute upper index, index of last value inside interval

  my @pred_int = sort {$a <=> $b} @{$pred_intervals};
  my $no_pred_ints = scalar(@pred_int);

#    $low_ind = int(((100-$ci)*($no_sim-1)/200)+0.5); #index of start of ci % interval
  $low_ind = $self->round('number'=>((100-$ci)*($no_sim-1)/200)); #index of start of ci % interval
  $high_ind = $no_sim - $low_ind - 1; #index of end  of  c_i% interval

  for (my $i=0; $i<$no_pred_ints; $i++){
    push (@lower_index, $self->round('number'=>((100-$pred_int[$i])*($no_sim-1)/200))); 
    push (@upper_index, $no_sim - $lower_index[$i] -1);
  }

}
end get_npc_indices
# }}} get_npc_indices


# {{{ npc_analyze
start npc_analyze
{
  #input refs strat_matrix, strata_labels(fix no_of_strata from labels)
  #matrix (fix no_observations and no_sim from matrix)
  
  my $c_i = 95; 
  my @pred_int = sort {$a <=> $b} 0,20,40,50,60,80,90,95;
  my $no_sim= (split(/,/,$matrix ->[0])) -1;
  my $no_observations = scalar(@{$matrix});
  my $no_of_strata = scalar(@{$strata_labels});

  my $no_pred_ints = scalar(@pred_int);
  my ($lower_index,$upper_index,$low_ind,$high_ind);
  my ($result_column_labels,$result_row_labels);

  ## Prepare general run info for output file
  my %return_sec;
  $return_sec{'name'} = 'NPC run info';
  my $modelname=$self-> {'models'}->[0] ->filename();
  
  my $extra_value=$modelname;
  if (defined $self->{'msfo_file'}){
    $extra_value='msfo-file '.$self->{'msfo_file'};
  } elsif (defined $self->{'lst_file'}){
    $extra_value='lst-file '.$self->{'lst_file'};
  }

  $return_sec{'labels'} = [[],['Date','observations','simulations','Modelfile',
			       'parameter values from','Dependent variable','PsN version','NONMEM version']];
  
  my @datearr=localtime;
  my $the_date=($datearr[5]+1900).'-'.($datearr[4]+1).'-'.($datearr[3]);
  
  $return_sec{'values'} = [[$the_date,$no_observations,$no_sim,$modelname,$extra_value,
			    $self->{'dv'},$PsN::version,$self->{'nm_version'}]];
  
  push( @{$self -> {'results'}[0]{'own'}},\%return_sec );

  ##done general run info
  
  ($result_column_labels,$result_row_labels) = $self->get_npc_result_labels('ci' => $c_i,
									    'pred_intervals' => \@pred_int);
  my @result_labels = ($result_row_labels,$result_column_labels);


  ($lower_index,$upper_index,$low_ind,$high_ind)= $self->get_npc_indices('ci' => $c_i,
									 'no_sim' => $no_sim,
									 'pred_intervals' => \@pred_int);

  if ($self->{'verbose'}){
    for (my $i=0; $i<$no_pred_ints; $i++){
      print "$pred_int[$i] $lower_index->[$i] $upper_index->[$i]\n";
    }
    print "\n" ;
  }

  my $analyzed_points=0;
  my $realpos; #dirt
  for (my $strat_ind=0; $strat_ind<$no_of_strata; $strat_ind++){
    my %return_section;
    my ($result_values,$stats_warnings);
    ($result_values,$realpos,$stats_warnings)=$self->subset_npc_analyze('matrix' => $matrix,
									'row_indices' => $strat_matrix->[$strat_ind],
									'low_ind' => $low_ind,
									'high_ind' => $high_ind,
									'lower_index' => $lower_index,
									'upper_index' => $upper_index, 
									'pred_intervals' => \@pred_int);
    
    my $point_counter=scalar(@{$strat_matrix->[$strat_ind]});
    my $result_name = "\nNPC results ".$strata_labels -> [$strat_ind].
	"\n$point_counter observations out of $no_observations";
    $return_section{'name'} = $result_name;

    $return_section{'labels'} = \@result_labels;
    $return_section{'values'} = $result_values;
    push( @{$self -> {'results'}[0]{'own'}},\%return_section );
    
    my %diag_section;
    $diag_section{'name'} = "\nNPC * (warning) statistics";
    $diag_section{'labels'} = [[],['Real data * count','Sim. data * count mean','Theoretical mean','Sim. data * count median',
				   'Sim. data * count 95% CI from','to']];
    $diag_section{'values'} = [$stats_warnings];
    push( @{$self -> {'results'}[0]{'own'}},\%diag_section );

    $analyzed_points+=$point_counter;
  }

  #check that all individuals were grouped to a strata
  unless ($analyzed_points == $no_observations){
    'debug' -> die(message => "Sum of observations $analyzed_points in each strata does not\n".
		   "equal total number of observations $no_observations. Something went wrong.");
  }
}
end npc_analyze
# }}}

