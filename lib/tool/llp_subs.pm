# Det finns ingen mening med att kunna specificera subproblem eftersom vi bara kan fixera
# parameterv�rden p� problemniv�.
# Det finns dessutom lite v�rde i att kunna spec. problem eftersom vi �n s� l�nge inte kan
# k�ra problem var f�r sig via PsN och om man har flera problem i en modellfil s� skulle
# alla k�ras �ven om man bara �r intresserad av ett. S� man f�r helt enkelt editera
# modellfilen och ta bort on�diga problem.

# {{{ include

start include statements
use File::Copy 'cp';
use ext::Math::SigFigs;
use tool::modelfit;
use Data::Dumper;
end include

# }}} include statements

# {{{ new

start new
      {
	my @various_input_formats = ( 'run_thetas', 'run_omegas', 'run_sigmas',
				      'rse_thetas', 'rse_omegas', 'rse_sigmas' );
	foreach my $var ( @various_input_formats ) {
	  if ( defined $this -> {$var} ) {
	    if ( ref( $this -> {$var} ) eq 'ARRAY' and ref( $this -> {$var} -> [0] ) eq 'ARRAY' ) {
	      if ( scalar @{$this -> {$var}} != scalar @{$this -> {'models'}} ) {
		debug -> die( message => 'If you define the thetas per model, the first '.
			      'dimension of run_thetas must match the number of models' );
	      }
	    } else {
	      my $variable = $this -> {$var};
	      $this -> {$var} = [];
	      if ( scalar @{$this -> {'models'}} > 1) {
		for ( my $j = 1; $j <= scalar @{$variable}; $j++ ) {
		  debug -> warn( level   => 2,
				 message => "All models: using the same $var $j" );
		}
	      }
	      foreach my $model ( @{$this -> {'models'}} ) {
		push ( @{$this -> {$var}}, $variable ) ;
	      }
	      foreach my $param ( 'theta', 'omega', 'sigma' ) {
		if ( defined $this -> {$var} and defined $this -> {'rse_'.$param}
		     and $var eq 'rse_'.$param.'s' ) {
		  for ( my $i = 0; $i < scalar @{$this -> {$var}}; $i++ ) {
		    debug -> die( message => "The number of run_".$param."s (".
				  scalar @{$this -> {'run_'.$param}[$i]}.
				  ") does not match the number of supplied ".$var.
				  " (".scalar @{$this -> {$var}[$i]}.")" )
		      if ( not scalar @{$this -> {'run_'.$param}[$i]} ==
			   scalar @{$this -> {$var}[$i]} );
		  }
		}
	      }
	    }
	  }
	}

	# First check if parameters are of BLOCK type, then we must adjust numbers.
	# Then  check if parameters are of SAME or FIX type, then we will print an ERROR!!
	if( 0 ) { #-- PONTUS -- This is work in progress. Don't remove please -- PONTUS --
	  my %run;
	  $run{'thetas'} = (ref( $this -> {'run_thetas'} -> [0] ) eq 'ARRAY') ? 
	      $this -> {'run_thetas'} -> [0]:$this -> {'run_thetas'};
	  $run{'omegas'} = (ref( $this -> {'run_omegas'} -> [0] ) eq 'ARRAY') ? 
	      $this -> {'run_omegas'} -> [0]:$this -> {'run_omegas'};
	  $run{'sigmas'} = (ref( $this -> {'run_sigmas'} -> [0] ) eq 'ARRAY') ? 
	      $this -> {'run_sigmas'} -> [0]:$this -> {'run_sigmas'};
	  
	  my $model = $this -> {'models'} -> [0];
	  
	  # Loop over the parameters
	  foreach my $parameter_type ( keys %run ) {
	    # jump to next parameter if no parameter of this type should be run
	    next unless ( defined $run{$parameter_type} and
			  scalar @{$run{$parameter_type}} > 0 and
			  $run{$parameter_type}->[0] ne '' );
	    
	    my @parameter_numbers = @{$run{$parameter_type}};
	    
	    foreach my $parameter_number ( @parameter_numbers ){
	      
	      if( defined( $model -> problems -> [0] -> $parameter_type )) {
		my $global_parameter_number = 0;
		for( my $i = 0; $i < scalar @{$model -> problems -> [0] -> $parameter_type} ; $i++ ){
		  my $init_record = $model -> problems -> [0] -> $parameter_type -> [$i];
		  unless( defined $init_record ){
		    debug -> die( message => "The LLP tool can not be run on $parameter_type number $parameter_number because it does not exist.");
		  }
		  print "$parameter_type $init_record $parameter_number\n";
		  for( my $j = 0 ; $j < scalar @{$init_record -> options}; $j++ ){
		    if( $parameter_number - 1 == $global_parameter_number ){
		      if( $init_record -> same ){
			debug -> die( message => "The LLP tool can not be run on $parameter_type number $parameter_number because it is \"SAME\"" );
		      } elsif( $init_record -> fix ){
			debug -> die( message => "The LLP tool can not be run on $parameter_type number $parameter_number because it is \"FIX\"" );
		      }
		    }
		    $global_parameter_number ++;
		  }
		}
	      }	    
	    }
	  }
	}
	
	foreach my $file ( 'logfile', 'unstacked_logfile', 'raw_results_file' ) {
	  if ( not( ref($this -> {$file}) eq 'ARRAY' or
		    ref($this -> {$file}) eq 'HASH' ) ) {
	    my ($ldir, $filename) = OSspecific::absolute_path( $this -> {'directory'},
							       $this -> {$file} );
	    $this -> {$file} = [];
	    for ( my $i = 1; $i <= scalar @{$this -> {'models'}}; $i++ ) {
	      my $tmp = $filename;
	      $tmp =~ s/\./$i\./;
	      push ( @{$this -> {$file}}, $ldir.$tmp ) ;
	    }
	  }
	}
      }
end new

# }}}


# {{{ modelfit_setup

start modelfit_setup
      {
	my $model = $self -> {'models'} -> [$model_number-1];

	my $mfit_threads = ref( $self -> {'threads'} ) eq 'ARRAY' ? 
	  $self -> {'threads'} -> [1]:$self -> {'threads'};
	my $own_threads = ref( $self -> {'threads'} ) eq 'ARRAY' ?
	  $self -> {'threads'} -> [0]:$self -> {'threads'};

	# Check which models that hasn't been run and run them
	# This will be performed each step but will only result in running
	# models at the first step, if at all.

	# If more than one process is used, there is a VERY high risk of interaction
	# between the processes when creating directories for model fits. Therefore
	# the {'directory'} attribute is given explicitly below.

	unless ( $model -> is_run ) {

	  # -----------------------  Run original run  ------------------------------

	  # {{{ orig run

	  my %subargs = ();
	  if ( defined $self -> {'subtool_arguments'} ) {
	    %subargs = %{$self -> {'subtool_arguments'}};
	  }
	  my $orig_fit = tool::modelfit ->
	      new( reference_object      => $self,
		   models                => [$model],
		   threads               => $mfit_threads,
		   base_directory        => $self -> {'directory'},
		   directory             => $self -> {'directory'}.'/orig_modelfit_dir'.$model_number,
		   subtools              => [],
		   parent_threads        => $own_threads,
		   parent_tool_id        => $self -> {'tool_id'},
		   logfile	         => undef,
		   raw_results           => undef,
		   prepared_models       => undef,
		   %subargs );
# 	    ( models                => [$model],
# 	      subtools              => [],

# 	      run_on_nordugrid => $self -> {'run_on_nordugrid'},
# 	      cpu_time	 => $self -> {'cpu_time'},
	      
# 	      run_on_lsf       => $self -> {'run_on_lsf'},

# 	      lsf_queue        => $self -> {'lsf_queue'},
# 	      lsf_options      => $self -> {'lsf_options'},
# 	      lsf_job_name     => $self -> {'lsf_job_name'},
# 	      lsf_project_name => $self -> {'lsf_project_name'},

# 	      clean                 => $self -> {'clean'},
# 	      nm_version            => $self -> {'nm_version'},
# 	      picky                 => $self -> {'picky'},
# 	      compress              => $self -> {'compress'},
# 	      threads               => $mfit_threads,
# 	      retries               => $self -> {'retries'},
# 	      base_directory        => $self -> {'directory'},
# 	      directory             => $self -> {'directory'}.'/orig_modelfit_dir'.$model_number.'/',
# 	      parent_tool_id        => $self -> {'tool_id'},
# 	      parent_threads        => $own_threads );
	  ui -> print( category => 'llp',
		       message  => "Evaluating basic model" ) unless $self -> {'parent_threads'} > 1;
	  $orig_fit -> run;

	  # }}} orig run

	}

	my $first = 0;
	# Prepare the step
	unless ( defined $self -> {'theta_log'} or
		 defined $self -> {'omega_log'} or
		 defined $self -> {'sigma_log'} ) {
	  # First step, register the estimates from the original runs
	  debug -> warn( level   => 2,
			 message => "First llp step, register estimates from original run" );
	  $self -> _register_estimates( first        => 1,
				        model_number => $model_number);
	  $first = 1;
	}

	# If we are resuming, there should exist a 'done' file
	my $done = 0;
	if ( -e $self -> {'directory'}.'/m'.$model_number."/done" ) {
	  # Recreate the datasets and models from a checkpoint
	  $done = 1;
	}

	# Make new guesses for the parameter values
	$self -> _make_new_guess( first        => $first,
				  model_number => $model_number,
				  done         => $done );

	# Construct new models for this step from the observed OFV's from the
	# previous step
	# They should be placed in m1, m2 and so on for each model.
	$self -> {'prepared_models'} =
	  $self -> _create_models( model_number => $model_number,
				   done         => $done );

	# ---------------------  Create the modelfit  -------------------------------

	# {{{ modelfit

	if( defined $self -> {'prepared_models'} and scalar @{$self -> {'prepared_models'}} > 0 ) {
	  # Create a modelfit tool for all the models of this step.
	  # This is the last setup part before running the step.
	  my %subargs = ();
	  if ( defined $self -> {'subtool_arguments'} ) {
	    %subargs = %{$self -> {'subtool_arguments'}};
	  }
	  push( @{$self -> {'tools'}},
		tool::modelfit ->
		new( reference_object      => $self,
		     models                => $self -> {'prepared_models'},
		     threads               => $mfit_threads,
		     base_directory        => $self -> {'directory'},
		     directory             => $self -> {'directory'}.'/modelfit_dir'.$model_number,
		     _raw_results_callback => $self -> _modelfit_raw_results_callback,
		     subtools              => [],
		     parent_threads        => $own_threads,
		     parent_tool_id        => $self -> {'tool_id'},
		     raw_results_file      => $self -> {'raw_results_file'}[$model_number-1],
		     logfile	         => undef,
		     raw_results           => undef,
		     prepared_models       => undef,
		     raw_results_header    => undef,
		     tools                 => undef,
		     %subargs ) );
	}
#	    $Data::Dumper::Maxdepth = 3;
#	    die Dumper $self -> {'tools'} if not $first;

# 	      ( raw_results_file     => $self -> {'raw_results_file'}[$model_number-1],

# 		run_on_lsf       => $self -> {'run_on_lsf'},

# 		lsf_queue        => $self -> {'lsf_queue'},
# 		lsf_options      => $self -> {'lsf_options'},
# 		lsf_job_name     => $self -> {'lsf_job_name'},
# 		lsf_project_name => $self -> {'lsf_project_name'},
		
# 		run_on_nordugrid => $self -> {'run_on_nordugrid'},
# 		cpu_time         => $self -> {'cpu_time'},
		
# 		parent_tool_id   => $self -> {'tool_id'},
		
# 		clean          => $self -> {'clean'},
# 		models         => $self -> {'prepared_models'},
# 		nm_version     => $self -> {'nm_version'},
# 		picky          => $self -> {'picky'},
# 		compress       => $self -> {'compress'},
# 		threads        => $mfit_threads,
# 		retries        => $self -> {'retries'},
# 		base_directory => $self -> {'directory'},
#  		directory      => $self -> {'directory'}.'/modelfit_dir'.$model_number.'/',
# 		subtools       => [],
# 		parent_tool_id => $self -> {'tool_id'},
# 		parent_threads => $own_threads,
# 		_raw_results_callback => $self -> _modelfit_raw_results_callback,
# 		%subargs ) );

	# }}} modelfit

      }
end modelfit_setup

# }}}

# {{{ _create_models

start _create_models
      {

	# --------------------  Initiate the run parameters -------------------------

	# {{{ initiate params

	my %run;
	$run{'thetas'} = (ref( $self -> {'run_thetas'} -> [0] ) eq 'ARRAY') ? 
	  $self -> {'run_thetas'} -> [$model_number-1]:$self -> {'run_thetas'};
	$run{'omegas'} = (ref( $self -> {'run_omegas'} -> [0] ) eq 'ARRAY') ? 
	  $self -> {'run_omegas'} -> [$model_number-1]:$self -> {'run_omegas'};
	$run{'sigmas'} = (ref( $self -> {'run_sigmas'} -> [0] ) eq 'ARRAY') ? 
	  $self -> {'run_sigmas'} -> [$model_number-1]:$self -> {'run_sigmas'};

	my $model = $self -> {'models'} -> [$model_number-1];

	# }}} initiate params

	# ------------------  Create the fixed parameter models ---------------------

	# {{{ create models

	# Loop over the parameters
	foreach my $param ( 'theta', 'omega', 'sigma' ) {
	  # jump to next parameter if no parameter of this type should be run
	  next unless ( defined $run{$param.'s'} and
			scalar @{$run{$param.'s'}} > 0 and
			$run{$param.'s'}->[0] ne '' );
	  my @par_nums = @{$run{$param.'s'}};
	  my %par_log  = %{$self -> {$param.'_log'}};
	  # Loop over the parameter numbers of interest
	  foreach my $num ( @par_nums ) {
	    my @active = @{$model -> active_problems};
	    my $skip_model = 1;
	    foreach my $val ( @active ) {
	      $skip_model = 0 if ( $val );
	    }
	    foreach my $side ( 'lower', 'upper' ) {
	      # todo: Maybe not necessary to copy data file as well. This is done by
	      # default in model->copy.
	      my $filename = substr($param,0,2).$num.$side.'.mod';
	      my $model_dir = $self -> {'directory'}.'/m'.$model_number.'/';
	      my ($output_dir, $outputfilename) =
		  OSspecific::absolute_path( $self -> {'directory'}.'/m'.$model_number.'/',
					     substr($param,0,2).$num.
					     $side.'.lst');
	      if ( not $done ) {

		# ----------------------  Create model  -----------------------------

		# {{{ create

		my $new_mod = $model -> copy( filename    => $filename,
					      directory   => $model_dir,
					      copy_data   => 0,
					      copy_output => 0 );
		# According to Lars 2005-01-31 removing tables and covariances is a bug.
		#foreach my $problem ( @{$new_mod -> problems} ) {
		#  $problem -> tables([]);
		#  $problem -> covariances([]);
		#}
		$new_mod -> extra_files( $model -> extra_files ),
		$new_mod -> ignore_missing_files( 1 );
		$new_mod -> outputfile( $output_dir . $outputfilename );
		$new_mod -> ignore_missing_files( 0 );

		$new_mod -> _write;

		$new_mod -> update_inits( from_output => $model -> outputs -> [0] );
		my $active_flag = 0;
		# Loop over the problems:
		for ( my $j = 1; $j <= scalar @{$par_log{$num}}; $j++ ) {
		  # Is this side of the problem finished?
		  debug -> warn( level   => 2,
				 message => "This side is finished!" )
		      if ( $self->{$param.'_log'}->{$num}->[$j-1]->[2]->{$side} );
		  next if $self->{$param.'_log'}->{$num}->[$j-1]->[2]->{$side};
		  my $sofar = scalar @{$par_log{$num}->[$j-1]->[0]};
		  my $guess;
		  if ( $side eq 'lower' ) {
		    $guess = $par_log{$num}->[$j-1]->[0]->[0];
		  } else {
		    $guess = $par_log{$num}->[$j-1]->[0]->[$sofar-1];
		  }
		  my @diagnostics =
		      @{$new_mod -> initial_values( parameter_type    => $param,
						    parameter_numbers => [[$num]],
						    problem_numbers   => [$j],
						    new_values        => [[$guess]] )};
		  if ( $side eq 'lower' ) {
		    $par_log{$num}->[$j-1]->[0]->[0] = $diagnostics[0][2];
		  } else {
		    $par_log{$num}->[$j-1]->[0]->[$sofar-1] = $diagnostics[0][2];
		  }
		  debug -> warn( level   => 2,
				 message => "Registering used value ".
				 $diagnostics[0][2]." for the $side side" );
		  $new_mod -> fixed( parameter_type    => $param,
				     parameter_numbers => [[$num]],
				     problem_numbers   => [$j],
				     new_values        => [[1]] );
		  $active_flag = 1;
		}
		if ( $active_flag ) {
		  $new_mod -> _write;
		  push( @new_models, $new_mod );
		  $self->{$param.'_models'}->{$num}->{$side} = $new_mod;
		}

		# }}} create

	      } else {

		# -------------------------  Resume  --------------------------------

		# {{{ resume

		my $active_flag = 0;
		# Loop over the problems:
		for ( my $j = 1; $j <= scalar @{$par_log{$num}}; $j++ ) {
		  # Is this side of the problem finished?
		  debug -> warn( level   => 2,
				 message => "This side is finished!" )
		      if ( $self->{$param.'_log'}->{$num}->[$j-1]->[2]->{$side} );
		  next if $self->{$param.'_log'}->{$num}->[$j-1]->[2]->{$side};
		  $active_flag = 1;
		}
		if ( $active_flag ) {
		  my $new_mod = model -> new( directory   => $model_dir,
					      filename    => $filename,
					      outputfile  => $outputfilename,
					      extra_files => $model -> extra_files,
					      target      => 'disk',
					      ignore_missing_files => 1 );
		  # Set the correct data file for the object
		  my $moddir = $model -> directory;
		  my @datafiles = @{$model -> datafiles};
		  for( my $df = 0; $df <= $#datafiles; $df++ ) {
		    $datafiles[$df] = $moddir.'/'.$datafiles[$df];
		  }
		  $new_mod -> datafiles( new_names => \@datafiles );
		  push( @new_models, $new_mod );
		  $self->{$param.'_models'}->{$num}->{$side} = $new_mod;
		}

		# }}} resume

	      }
	    }
	  }
	}

	# }}} create models

      }	
end _create_models

# }}} _create_models

# {{{ _register_estimates

start _register_estimates
      {

	# F�renkla loggen: param_log{parameternr}[prob][[estimates...][ofv...]]
	# Antag att man spec. paramnr som g�ller ? alla modeller och problem,
	# dvs att run_param = [2,4,3] tex.
	my %run;
	$run{'thetas'} = (ref( $self -> {'run_thetas'} -> [0] ) eq 'ARRAY') ? 
	  $self -> {'run_thetas'} -> [$model_number-1]:$self -> {'run_thetas'};
	$run{'omegas'} = (ref( $self -> {'run_omegas'} -> [0] ) eq 'ARRAY') ? 
	  $self -> {'run_omegas'} -> [$model_number-1]:$self -> {'run_omegas'};
	$run{'sigmas'} = (ref( $self -> {'run_sigmas'} -> [0] ) eq 'ARRAY') ? 
	  $self -> {'run_sigmas'} -> [$model_number-1]:$self -> {'run_sigmas'};
	
	my %models;
	my @mod_variants;

	my @prep_models;
	my ( $orig_output, $orig_ofvs );
	my $ui_text;

	# Global activity flag for this model:
	$active = 0;

	my $orig_model = $self -> {'models'} -> [$model_number-1];	

	my $mode = $first ? '>' : '>>';
	open( LOG, $mode.$self -> {'logfile'}[$model_number-1] );
	open( LOG2, $mode.$self -> {'unstacked_logfile'}[$model_number-1] );
	print LOG2 sprintf("%10s",'Parameter'),',',sprintf("%10s",'Side'),
	',',sprintf("%10s",'Value'),',',sprintf("%10s",'OFV.diff'),"\n" if( $first );

	if ( $first ) {
	  'debug' -> die( message => "No output object defined through model" )
	      unless ( defined $orig_model -> outputs -> [0] );
	  $orig_output = $orig_model -> outputs -> [0];
	  $orig_ofvs   = $orig_output -> ofv;
	  # Save the ofvs in {'orig_ofvs'}
	  $self -> {'orig_ofvs'} = $orig_ofvs;

	  # Print a log-header
	  foreach my $param ( 'theta', 'omega', 'sigma' ) {
	    next unless ( defined $run{$param.'s'} and
			  scalar @{$run{$param.'s'}} > 0 and
			  $run{$param.'s'}->[0] ne '' );
	    my @par_nums    = @{$run{$param.'s'}};
	    my $accessor    = $param.'s';
	    my $orig_ests    = $orig_output -> $accessor;
	    # Loop the parameter numbers
	    foreach my $num ( @par_nums ) {
	      # Loop the problems
	      for ( my $j = 0; $j < scalar @{$orig_ests}; $j++ ) {
		my $label = uc($param).$num."_".($j+1);
		$ui_text = $ui_text.sprintf("%10s",$label).','.sprintf("%10s",'OFV_DIFF').',';
		print LOG sprintf("%10s",$label),',',sprintf("%10s",'OFV_DIFF'),',';
	      }
	    }
	  }
	  ui -> print( category => 'llp',
		       message  => $ui_text,
		       wrap     => 0 );
	  print LOG "\n";

	  # It is much more time efficient to store all bounds, estimates etc in
	  # an array and access that when doing the registration than calling the
	  # tools within the loop:
	  # Loop over the parameter names
	  $ui_text = '';
	  foreach my $param ( 'theta', 'omega', 'sigma' ) {
	    # jump to next parameter if no parameter of this type should be run
	    next unless ( defined $run{$param.'s'} and
			  scalar @{$run{$param.'s'}} > 0 and
			  $run{$param.'s'}->[0] ne '' );
	    my $accessor    = $param.'s';
	    my @par_nums    = @{$run{$param.'s'}};
	    my $orig_ests    = $orig_output -> $accessor;
	    my $se_accessor  = 'se'.$param.'s';
	    my $orig_se_ests = $orig_output -> $se_accessor;
	    # Loop over the parameter numbers of interest
	    my %all_num_est = ();
	    foreach my $num ( @par_nums ) {
	      my @all_prob_est = ();
	      # Loop over the problems:
	      for ( my $j = 0; $j < scalar @{$orig_ests}; $j++ ) {
		debug -> die( message => "Subproblems are not allowed for the log-likelihood profiling tool" )
		    if ( scalar @{$orig_ests->[$j]} > 1 );
		# For the original estimates, we need to use the estimates from the original output file.
		my $orig  = $orig_ests->[$j][0][$num-1];
#		my $orig = $orig_model -> initial_values( parameter_type => 'theta' ) -> [0][$num - 1];
		# Store original estimates. The [0] is there for print_results.
		push(@{$self -> {'original_estimates'}[$model_number-1][$j][0]}, $orig);
		# all_prob_est: [fixed estimates][ofv_diffs][finished_flag(lower,upper)]
		my %finished = ( 'lower' => 0, 'upper' => 0 );
		push( @all_prob_est, [[$orig],[0],\%finished]);
		# If we end up here, the the llp of this model is still active
		$active = 1; 
		$ui_text = $ui_text.sprintf("%10f",$orig).','.sprintf("%10f",0).',';
		print LOG sprintf("%10f",$orig),',',sprintf("%10f",0),',';
		print LOG2 sprintf("%10s",$param.$num),',',sprintf("%10s",'orig'),
		',',sprintf("%10f",$orig),',',sprintf("%10f",0),"\n";
	      }
	      $all_num_est{$num} = \@all_prob_est;
	    }
	    $self -> {$param.'_log'} = \%all_num_est;
	  }
	  ui -> print( category => 'llp',
		       message  => $ui_text,
		       wrap     => 0 );
	  print LOG "\n";
	} else {
	  @prep_models = @{$self -> {'prepared_models'}};

	  # It is much more time efficient to store all bounds, estimates etc in
	  # an array and access that when doing the registration than calling the
	  # tools within the loop:
	  # Loop over the parameter names
	  foreach my $side ( 'lower', 'upper' ) {
	    # reset the user interface text
	    $ui_text = '';
	    foreach my $param ( 'theta', 'omega', 'sigma' ) {
	      # jump to next parameter if no parameter of this type should be run
	      next unless ( defined $run{$param.'s'} and
			    scalar @{$run{$param.'s'}} > 0 and
			    $run{$param.'s'}->[0] ne '' );
	      my %bounds;
	      $bounds{'lower'} =
		$orig_model -> lower_bounds( parameter_type => $param );
	      $bounds{'upper'} =
		$orig_model -> upper_bounds( parameter_type => $param );
	      
	      my $accessor    = $param.'s';
	      my @par_nums    = @{$run{$param.'s'}};
	      # get the stored ofvs
	      $orig_ofvs = $self -> {'orig_ofvs'};
	      # Loop over the parameter numbers of interest
	      foreach my $num ( @par_nums ) {
		my ( $model, $output, $ofvs, $ests );
		# Check if there exists any model for this side (if not, it is
		# probably (hopefully) finished
		if (defined $self->{$param.'_models'} -> {$num} -> {$side}) {
		  $model    = $self->{$param.'_models'} -> {$num} -> {$side};
		  $output   = $model  -> outputs -> [0];
		  $ofvs     = $output -> ofv;
		  my $accessor = $param.'s';
		  $ests     = $output -> $accessor;
		}
		# Loop over the problems:
		for ( my $j = 0; $j < scalar @{$orig_ofvs}; $j++ ) {
		  # Is this side of the problem finished?
		  if ( $self->{$param.'_log'}->{$num}->[$j]->[2]->{$side} ) {
		    $ui_text = $ui_text.sprintf("%10s",$PsN::out_miss_data).','.sprintf("%10s",$PsN::out_miss_data).',';
		    print LOG sprintf("%10s",$PsN::out_miss_data),',',sprintf("%10s",$PsN::out_miss_data),',';
		    next;
		  }
		  # Collect the model outputs
		  my $diff = $ofvs->[$j][0]-$orig_ofvs->[$j][0];
		  #my $est   = $ests -> [$j][0][$num-1];
		  my $est = $model -> initial_values( parameter_type => $param ) -> [0][$num - 1];
		  $ui_text = $ui_text.sprintf("%10f",$est).','.sprintf("%10f",$diff).',';
		  print LOG sprintf("%10f",$est),',',sprintf("%10f",$diff),',';
		  print LOG2 sprintf("%10s",$param.$num),',',sprintf("%10s",$side),',',
		  sprintf("%10f",$est),',',sprintf("%10f",$diff),"\n";
		  my $bound = $bounds{$side}->[$j][$num-1];
		  debug -> warn( level   => 2,
				 message => "Registering diff $diff" );
		  if ( $side eq 'lower' ) {
		    unshift( @{$self->{$param.'_log'}->{$num}->[$j]->[1]}, $diff );
		    $est = $self->{$param.'_log'}->{$num}->[$j]->[0]->[0];
		  } else {
		    push( @{$self->{$param.'_log'}->{$num}->[$j]->[1]}, $diff );
		    my $sofar = scalar @{$self->{$param.'_log'}->{$num}->[$j]->[0]};
		    $est = $self->{$param.'_log'}->{$num}->[$j]->[0]->[$sofar-1];
		  }
		  my $finished = $self -> _test_sigdig( number => $diff,
							goal   => $self -> {'ofv_increase'},
							sigdig => $self -> {'significant_digits'} );
		  my $print_diff = &FormatSigFigs($diff, $self -> {'significant_digits'} );
		  debug -> warn( level   => 2,
				 message => "New OFV diff: $print_diff" );
		  debug -> warn( level   => 2,
				 message => " equals goal ".$self->{'ofv_increase'}.
				 ". This side is finished" ) if $finished;
		  if ( $finished ) {
		    $self->{$param.'_log'}->{$num}->[$j]->[2]->{$side} = 1;
		  } elsif ( defined $bound and
			    $self -> _test_sigdig( number => $est,
						   goal   => $bound,
						   sigdig => 2 ) ) {
		    debug -> warn( level   => 1,
				   message => "Estimate $est too close to $side boundary $bound".
				   " terminating search" );
		    $self->{$param.'_log'}->{$num}->[$j]->[2]->{$side} = 2;
		  } else {
		    $active = 1;
		  }
		  $model -> datas -> [0] -> flush;
		  $model -> outputs -> [0] -> flush;
		}
	      }
	    }
	    # Print to screen if we are in the right context
	    ui -> print( category => 'llp',
			 message  => $ui_text,
			 wrap     => 0 );
	    print LOG "\n";
	  }
	}
	close ( LOG );
	close ( LOG2 );
      }
end _register_estimates

# }}} _register_estimates

# {{{ modelfit_analyze

start modelfit_analyze
      {
	my $own_threads = ref( $self -> {'threads'} ) eq 'ARRAY' ?
	  $self -> {'threads'} -> [0]:$self -> {'threads'};

	# 1. Register fixed values and ofvs
	#    and check if ofvs are close enough to target
	my $active = $self -> _register_estimates( model_number => $model_number );

	my ( $returns, $prep_models );
	# 3. Return logged values-ofv pairs to calling tool
	if ( $active ) {
	  if ( $self -> {'max_iterations'} > 1 ) {
	    $self -> update_raw_results( model_number => $model_number );
	    # !!! The results_file attribute should not be set when calling
	    # llp recursively. If set, a result file will be written for each
	    # recurrence level which is redundant.
	    my %run;
	    $run{'thetas'} = (ref( $self -> {'run_thetas'} -> [0] ) eq 'ARRAY') ? 
	      $self -> {'run_thetas'} -> [$model_number-1]:$self -> {'run_thetas'};
	    $run{'omegas'} = (ref( $self -> {'run_omegas'} -> [0] ) eq 'ARRAY') ? 
	      $self -> {'run_omegas'} -> [$model_number-1]:$self -> {'run_omegas'};
	    $run{'sigmas'} = (ref( $self -> {'run_sigmas'} -> [0] ) eq 'ARRAY') ? 
	      $self -> {'run_sigmas'} -> [$model_number-1]:$self -> {'run_sigmas'};
	    my %subargs = ();
	    if ( defined $self -> {'subtool_arguments'} ) {
	      %subargs = %{$self -> {'subtool_arguments'}};
	    }
	    my $internal_llp =
	      tool::llp ->
	      new( reference_object   => $self,
		   raw_results_file   => [$self -> {'raw_results_file'}[$model_number-1]],
		   parent_tool_id     => $self -> {'tool_id'},
		   base_directory     =>  $self -> {'directory'},
		   directory	      =>  $self -> {'directory'}.'/llp_dir'.$model_number,
		   logfile	      => [$self -> {'logfile'}[$model_number-1]],
		   unstacked_logfile  => [$self -> {'unstacked_logfile'}[$model_number-1]],
		   max_iterations     => ($self -> {'max_iterations'} - 1),
		   iteration	      => ($self -> {'iteration'} + 1),
		   models	      => [$self -> {'models'} -> [$model_number-1]],
		   run_thetas	      => [$self -> {'run_thetas'} -> [$model_number-1]],
		   run_omegas	      => [$self -> {'run_omegas'} -> [$model_number-1]],
		   run_sigmas	      => [$self -> {'run_sigmas'} -> [$model_number-1]],
		   parent_tool_id     =>  $self -> {'tool_id'},
		   parent_threads     =>  $own_threads,
		   raw_results        => undef,
		   prepared_models    => undef,
		   rse_thetas         => undef,
		   rse_omegas         => undef,
		   rse_sigmas         => undef,
		   raw_results_header => undef,
		   tools              => undef,
		   prepared_models    => undef,
		   results            => undef,
		   %subargs );

#	    $Data::Dumper::Maxdepth = 2;
#	    die Dumper $internal_llp;
			
# 		  new ( raw_results_file   => [$self -> {'raw_results_file'}[$model_number-1]],
# 			nm_version	   =>  $self -> {'nm_version'},
			
# 			run_on_lsf       => $self -> {'run_on_lsf'},
# 			lsf_queue        => $self -> {'lsf_queue'},
# 			lsf_options      => $self -> {'lsf_options'},
# 			lsf_job_name     => $self -> {'lsf_job_name'},
# 			lsf_project_name => $self -> {'lsf_project_name'},
			
# 			run_on_nordugrid => $self -> {'run_on_nordugrid'},
# 			cpu_time         => $self -> {'cpu_time'},

# 			parent_tool_id   => $self -> {'tool_id'},

# 			picky		   =>  $self -> {'picky'},
# 			compress	   =>  $self -> {'compress'},
# 			threads		   =>  $self -> {'threads'},
# 			retries		   =>  $self -> {'retries'},
# 			base_directory	   =>  $self -> {'directory'},
# 			directory	   =>  $self -> {'directory'}.'/llp_dir'.$model_number.'/',
# 			logfile		   => [$self -> {'logfile'}[$model_number-1]],
# 			unstacked_logfile  => [$self -> {'unstacked_logfile'}[$model_number-1]],
# 			max_iterations	   => ($self -> {'max_iterations'} - 1),
# 			iteration	   => ($self -> {'iteration'} + 1),
# 			significant_digits =>  $self -> {'significant_digits'},
# 			normq		   =>  $self -> {'normq'},
# 			ofv_increase	   =>  $self -> {'ofv_increase'},
# 			models		   => [$self -> {'models'} -> [$model_number-1]],
# 			subtools	   =>  $self -> {'subtools'},
# 			run_thetas	   => [$self -> {'run_thetas'} -> [$model_number-1]],
# 			run_omegas	   => [$self -> {'run_omegas'} -> [$model_number-1]],
# 			run_sigmas	   => [$self -> {'run_sigmas'} -> [$model_number-1]],
# 			theta_log	   =>  $self -> {'theta_log'},
# 			omega_log	   =>  $self -> {'omega_log'},
# 			sigma_log	   =>  $self -> {'sigma_log'},
# 			orig_ofvs	   =>  $self -> {'orig_ofvs'},
# 			parent_tool_id	   =>  $self -> {'tool_id'},
# 			parent_threads	   =>  $own_threads );

	    ( $returns, $prep_models ) = $internal_llp -> run;

	    if ( defined $prep_models ) {
	      debug -> warn( level   => 2,
			     message => "Inside ".ref($self).
			     " have called internal llp ".
			     scalar @{$prep_models} );
	      push ( @{$self -> {'prepared_models'}[$model_number-1]{'subtools'}},
		     $prep_models );
	    } else {
	      debug -> warn( level   => 1,
			     message => "Inside analyze".ref($self).
			     " but no prep_models defined from internal llp" );
	    }

	    #push( @{$self -> {'raw_results'}[$model_number-1]},
		#  @{$internal_llp -> raw_results -> [0]} );

	  } else {
	    $self -> {'raw_results'}[$model_number-1] =
	      $self -> {'tools'} -> [0] -> raw_results;
	    $self -> update_raw_results( model_number => $model_number );
	  }
	} else {
	  $self -> {'raw_results'}[$model_number-1] =
		$self -> {'tools'} -> [0] -> raw_results if( defined $self -> {'tools'} -> [0] );
	}
	if( $self -> iteration() < 2 and
	    defined $PsN::config -> {'_'} -> {'R'} and
	    -e $PsN::lib_dir . '/R-scripts/llp.R' ) {
	  # copy the llp R-script
	  cp ( $PsN::lib_dir . '/R-scripts/llp.R', $self -> {'directory'} );
	  # Execute the script
	  system( $PsN::config -> {'_'} -> {'R'}." CMD BATCH llp.R" );
	}
      }
end modelfit_analyze

# }}} modelfit_analyze

# {{{ prepare_results

start prepare_results
      {
#	if ( not defined $self -> {'raw_results'} ) {
	  $self -> read_raw_results();
#	}

	for ( my $i = 0; $i < scalar @{$self->{'raw_results'}}; $i++ ) { # All models
	  my $orig_mod = $self -> {'models'}[$i];
	  my @params = ( 'theta', 'omega', 'sigma' );
	  my ( %param_nums, %labels, %orig_estimates );
	  foreach my $param ( @params ) {
	    my $modlabels = $orig_mod -> labels( parameter_type => $param );
	    $labels{$param} = $modlabels -> [0]; # Only one problem
	    $param_nums{$param} = scalar @{$modlabels -> [0]} if ( defined $modlabels );
	    my $accessor = $param.'s';
	    $orig_estimates{$param} = $orig_mod -> outputs -> [0] -> $accessor -> [0][0];
	  }
	  my ( %ci, %near_bound, %max_iterations, %interval_ratio,
	       %skewed, %within_interval );

	  # The 9 on the row below is offset for iteration,
	  # parameter.type, parameter.number, side, finish.message,
	  # model, problem, subproblem, ofv

	  my $cols = scalar @{$self -> {'diagnostic_parameters'}} + 9;

	  # my $cols = scalar @{$self -> {'raw_results'}[$i][0]}; # first non-header row
	  # Skip original run:
	  for ( my $j = 1; $j < scalar @{$self -> {'raw_results'}[$i]}; $j++ ) {
	    my $num;
	    my $row = $self -> {'raw_results'}[$i][$j];
	    if ( $row -> [1] eq 'theta' ) {
	      $num = $cols + ($row -> [2]  - 1);
	    } elsif ( $row -> [1] eq 'omega' ) {
	      $num = $cols + $param_nums{'theta'} + ($row -> [2]  - 1);
	    } else { 
	      $num = $cols + $param_nums{'theta'} + $param_nums{'omega'} + ($row -> [2] - 1);
	    }
	    my $row = $self -> {'raw_results'}[$i][$j];
	    $ci{$row -> [1]}{$row -> [2]}{$row -> [3]} = $row -> [$num];
	    if ( $row -> [4] eq 'near.boundary' ) {
	      $near_bound{$row -> [1]}{$row -> [2]}{$row -> [3]} = 1;
	    } elsif ( $row -> [4] eq 'max.iterations' ) {
	      $max_iterations{$row -> [1]}{$row -> [2]}{$row -> [3]} = 1;
	    }
	  }
	  my ( @ci_labels, @ci_values, @li_values );
	  $ci_labels[1] = [ 'lower', 'maximum.likelihood.estimate',
			    'upper', 'interval.ratio', 'near.bound','max.iterations' ];
	  foreach my $param ( @params ) {
	    next if ( not defined $ci{$param} );
	    my @nums = sort { $a <=> $b } keys %{$ci{$param}};
	    foreach my $num ( @nums ) {
	      push( @{$ci_labels[0]}, $labels{$param}[$num-1] );
	      if ( defined $ci{$param}{$num}{'lower'} and 
		   defined $ci{$param}{$num}{'upper'} ) {
		if( abs( $ci{$param}{$num}{'lower'} - $orig_estimates{$param}[$num-1] ) == 0 ){
		  $interval_ratio{$param}{$num} = 'INF';
		} else {
		  $interval_ratio{$param}{$num} =
		      abs( $ci{$param}{$num}{'upper'} - $orig_estimates{$param}[$num-1] ) /
		      abs( $ci{$param}{$num}{'lower'} - $orig_estimates{$param}[$num-1] );
		  if ( $interval_ratio{$param}{$num} > $self -> {$param.'_interval_ratio_check'} or
		       $interval_ratio{$param}{$num} < 1/$self -> {$param.'_interval_ratio_check'} ) {
		    $skewed{$param}{$num} = 1;
		  } else {
		    $skewed{$param}{$num} = 0;
		  }
		  if ( $self -> {'within_interval_check'} < $ci{$param}{$num}{'upper'} and
		       $self -> {'within_interval_check'} > $ci{$param}{$num}{'lower'} ) {
		    $within_interval{$param}{$num} = 1;
		  } else {
		    $within_interval{$param}{$num} = 0;
		  }
		}
	      }
	      my @row;
	      push( @row, $ci{$param}{$num}{'lower'} );
	      push( @row, $orig_estimates{$param}[$num-1] );
	      push( @row, $ci{$param}{$num}{'upper'} );
	      push( @row, $interval_ratio{$param}{$num} );
	      push( @row, $near_bound{$param}{$num}{'upper'} ? 1 : $near_bound{$param}{$num}{'lower'} ? 1 : 0 );
	      push( @row, $max_iterations{$param}{$num}{'upper'} ? 1 : $max_iterations{$param}{$num}{'lower'} ? 1 : 0 );
	      push( @ci_values, \@row );
	    }
	  }
	  #print Dumper \%ci;die;
	  $self -> {'confidence_intervals'}[$i]	= \%ci;
	  $self -> {'interval_ratio'}[$i]	= \%interval_ratio;
	  $self -> {'skewed_intervals'}[$i]	= \%skewed;
	  $self -> {'hit_max_iterations'}[$i]	= \%max_iterations;
	  $self -> {'near_boundary'}[$i]	= \%near_bound;
	  $self -> {'within_interval'}[$i]	= \%within_interval;
 	  my %return_section;
	  $return_section{'name'} = 'confidence.intervals';
	  $return_section{'labels'} = \@ci_labels;
	  $return_section{'values'} = \@ci_values;
 	  unshift( @{$self -> {'results'}[$i]{'own'}},\%return_section );
	}
# 	# Lasse 2005-04-29: This section may be reduced when the results array turns into a hash
# 	my ( @ofv_log1, @labels0 );
# 	#	  print Dumper $self -> {'theta_log'};
# 	foreach my $param ( 'theta', 'omega', 'sigma' ) {
# 	  my $accessor = $param.'names';
# 	  my @param_names = @{$self -> models -> [$model_number -1] -> outputs -> [0] -> $accessor};
# 	  my @par_nums = keys %{$self -> {$param.'_log'}};
# 	    if ( defined $par_nums[0] ) {
# 	      # Loop the problems
# 	      for ( my $i = 0; $i < scalar @{$self -> {$param.'_log'}{$par_nums[0]}}; $i++ ) {
# 		foreach my $num ( @par_nums ) {
# 		  for ( my $j = 0; $j < scalar @{$self -> {$param.'_log'}{$num}[$i][0]}; $j++ ) {
# 		    push( @{$ofv_log1[$i][0]}, [ $self -> {$param.'_log'}{$num}[$i][0][$j],
# 						$self -> {$param.'_log'}{$num}[$i][1][$j] ] );
# 		    push( @{$labels0[$i][0][0]}, $param_names[$i][$num-1] );
# 		  }
# 		}
# 		$labels0[$i][0][1] = ['fixed.value','OFV'];
# 	      }
# 	    }
# 	  }
# #	  print Dumper \@ofv_log1;
# 	  my %return_section;
# 	  $return_section{'name'} = 'ofv.trace';
# 	  $return_section{'labels'} = \@labels0;
# 	  $return_section{'values'} = \@ofv_log1;
# 	  push( @{$self -> {'results'}[$model_number-1]{'own'}},\%return_section );

# 	  my ( @last_values, @labels1, @labels2, @l_dist_ratio, @labels3, @all, @labels4 );
# 	  foreach my $param ( 'theta', 'omega', 'sigma' ) {
# 	    my $accessor = $param.'names';
# 	    my @param_names = @{$self -> models -> [$model_number -1] -> outputs -> [0] -> $accessor};
# 	    $accessor = $param.'s';
# 	    my @origs = @{$self -> models -> [$model_number -1] -> outputs -> [0] -> $accessor};
# 	    my @par_nums = keys %{$self -> {$param.'_log'}};
# 	    if ( defined $par_nums[0] ) {
# 	      # Loop the problems
# 	      for ( my $i = 0; $i < scalar @{$self -> {$param.'_log'}{$par_nums[0]}}; $i++ ) {
# 		foreach my $num ( @par_nums ) {
# 		  my $last = scalar @{$self -> {$param.'_log'}{$num}[$i][0]} - 1;
# 		  # the first [0] in last_values is there only to add a subproblem level
# 		  # for the general print_results method. The first [0] of the param_log
# 		  # indicates the log of the fixed parameter values.
# 		  # Lower end:
# 		  push( @{$last_values[$i][0][0]}, $self -> {$param.'_log'}{$num}[$i][0][0] );
# 		  # Upper end:
# 		  push( @{$last_values[$i][0][1]}, $self -> {$param.'_log'}{$num}[$i][0][$last] );
# 		  my $ratio = (abs( $self -> {$param.'_log'}{$num}[$i][0][$last] -
# 				    $origs[$i][0][$num-1] ) / $origs[$i][0][$num-1] ) /
# 				      (abs( $self -> {$param.'_log'}{$num}[$i][0][0] -
# 					    $origs[$i][0][$num-1] ) / $origs[$i][0][$num-1] );
# 		  push( @{$l_dist_ratio[$i][0][0]}, $ratio );
# 		  push( @{$all[$i][0]},[ $origs[$i][0][$num-1],
# 					 $self -> {$param.'_log'}{$num}[$i][0][0],
# 					 $self -> {$param.'_log'}{$num}[$i][0][$last],
# 					 $ratio ] );
# 		  push( @{$labels1[$i][0][1]}, $param_names[$i][$num-1] );
# 		  push( @{$labels2[$i][0][1]}, $param_names[$i][$num-1] );
# 		  push( @{$labels3[$i][0][1]}, $param_names[$i][$num-1] );
# 		  push( @{$labels4[$i][0][0]}, $param_names[$i][$num-1] );
# 		}
# 		$labels1[$i][0][0] = ['lower', 'upper'];
# 		$labels2[$i][0][0] = [];
# 		$labels3[$i][0][0] = [];
# 		$labels4[$i][0][1] = ['original', 'lower', 'upper', 'ratio'];
# 	      }
# 	    }
# 	  }
# 	  my %return_section;
# 	  $return_section{'name'} = '';
# 	  $return_section{'labels'} = \@labels4;
# 	  $return_section{'values'} = \@all;
# 	  unshift( @{$self -> {'results'}[$model_number-1]{'own'}},\%return_section );
# 	  my %return_section;
# 	  $return_section{'name'} = 'limit.distance.ratio';
# 	  $return_section{'labels'} = \@labels3;
# 	  $return_section{'values'} = \@l_dist_ratio;
# 	  unshift( @{$self -> {'results'}[$model_number-1]{'own'}},\%return_section );
# 	  my %return_section;
# 	  $return_section{'name'} = 'confidence.intervals';
# 	  $return_section{'labels'} = \@labels1;
# 	  $return_section{'values'} = \@last_values;
# 	  unshift( @{$self -> {'results'}[$model_number-1]{'own'}},\%return_section );
# 	  my %return_section;
# 	  $return_section{'name'} = 'original.estimates';
# 	  $return_section{'labels'} = \@labels2;
# 	  $return_section{'values'} = $self -> {'original_estimates'};
# 	  unshift( @{$self -> {'results'}[$model_number-1]{'own'}},\%return_section );
# #	  print Dumper $self -> {'results'}[$model_number-1]{'own'};
# 	}
      }
end prepare_results

# }}} prepare_results

# {{{ _modelfit_raw_results_callback

start _modelfit_raw_results_callback
      {

	# This functions creates a subrouting which will be called by
	# the modelfit subtool. The subroutine will add columns to the
	# result rows from the modelfit. The result rows will then be
	# printed to the "raw_results" file by the modelfit.

	# First we create some variables that the subroutine needs. To
	# avoid confuision we avoid using the $self variable in the
	# subroutine, instead we create these variables which become
	# available to the subroutine.

	my $iteration = $self -> {'iteration'};
	my %run;
	my @models = @{$self -> {'models'}};
	my $model_number = $self -> {'model_number'};
	my ($dir,$file) = 
	  OSspecific::absolute_path( $self -> {'directory'},
				     $self -> {'raw_results_file'}[$model_number-1] );

	  $run{'thetas'} = (ref( $self -> {'run_thetas'} -> [0] ) eq 'ARRAY') ? 
	    $self -> {'run_thetas'} -> [$model_number-1]:$self -> {'run_thetas'};
	  $run{'omegas'} = (ref( $self -> {'run_omegas'} -> [0] ) eq 'ARRAY') ? 
	    $self -> {'run_omegas'} -> [$model_number-1]:$self -> {'run_omegas'};
	  $run{'sigmas'} = (ref( $self -> {'run_sigmas'} -> [0] ) eq 'ARRAY') ? 
	    $self -> {'run_sigmas'} -> [$model_number-1]:$self -> {'run_sigmas'};
	my %log;
	foreach my $param ( 'theta', 'omega', 'sigma' ) {
	  $log{$param} = $self -> {$param.'_log'};
	}

	# The raw results of the original model should be put as the first row
	my $orig_mod = $self -> {'models'}[$model_number-1];

	# And here is the subroutine which is given to the modelfit.
	my $callback = sub {
	  # The modelfit object is given to us. It could have been
	  # retrieved from $self but this was easier.

	  my $modelfit = shift;
	  my $mh_ref   = shift;
	  my $raw_results_header = $modelfit -> raw_results_header;
	  my $raw_results = $modelfit -> raw_results;

	  $modelfit -> raw_results_append( 1 ) if( $iteration > 1 );

	  if ( $iteration == 1 ) {

	    my %dummy;

	    my ($raw_results_rows, $nonp_rows) = $self -> create_raw_results_rows( max_hash => $mh_ref,
										   model => $orig_mod,
										   raw_line_structure => \%dummy );

	    $orig_mod -> outputs -> [0] -> flush;

	    unshift( @{$raw_results_rows -> [0]}, ( 0, undef, undef, undef, undef ));
	    
	    unshift( @{$raw_results}, @{$raw_results_rows} );

	    # Set the header once.
	    
	    unshift( @{$raw_results_header} , ('iteration', 'parameter.type',
					       'parameter.number', 'side', 'finish.message' ) );
	  }

	  # {{{ New header

	  # First prepend llp specific stuff.

	  # It is implicitly true that the inner loop will execute once
	  # for each row in the raw_results array. We keep track of its
	  # the row index with $result_row.

 	  my $result_row = $iteration == 1 ? 1 : 0; # skip the original results row
	  
 	  foreach my $param ( 'theta', 'omega', 'sigma' ) {
 	    foreach my $num ( @{$run{$param.'s'}} ) {
 	      foreach my $side ( 'lower', 'upper' ) {
 		next unless ( defined $run{$param.'s'} and
 			      scalar @{$run{$param.'s'}} > 0 );
 		next if $log{$param}{$num}[0][2]{$side};
		if ( defined $raw_results -> [$result_row] ) {
		  unshift( @{$raw_results -> [$result_row]}, ($iteration, $param, $num, $side, undef ) );
		}
 		$result_row++;
 	      }
 	    }
	  }

	  # }}} New header

	};
	return $callback;
      }
end _modelfit_raw_results_callback

# }}} _modelfit_raw_results_callback

# {{{ update_raw_results

start update_raw_results
      {
	my ($dir,$file) = OSspecific::absolute_path( $self -> {'directory'},
						     $self -> {'raw_results_file'}[$model_number-1] );
	open( RRES, $dir.$file );
	my @rres = <RRES>;
	close( RRES );
	open( RRES, '>',$dir.$file );
	my @new_rres;
	foreach my $row_str ( @rres ) {
	  chomp( $row_str );
	  my @row = split( ',', $row_str );
	  if ( $row[0] eq $self -> {'iteration'} ) {
	    # The [0] is the problem level, should be removed
	    if ( $self -> {$row[1].'_log'}{$row[2]}[0][2]{$row[3]} == 1 ) {
	      $row[4] = 'limit.found';
	    } elsif ( $self -> {$row[1].'_log'}{$row[2]}[0][2]{$row[3]} == 2 ) {
	      $row[4] = 'near.boundary';
	    } elsif ( $self -> {'max_iterations'} <= 1 ) {
	      $row[4] = 'max.iterations';
	    }
	  }
	  push( @new_rres, \@row );
	  print RRES join(',',@row ),"\n";
      }
	close( RRES );
	$self -> {'raw_results'}[$model_number-1] = \@new_rres;
      }
end update_raw_results

# }}} update_raw_results

# {{{ print_summary

start print_summary
      {
	sub acknowledge {
	  my $name    = shift;
	  my $outcome = shift;
	  my $file    = shift;
	  my $l = (7 - length( $outcome ))/2;
	  my $c_num = '00';
	  $c_num    = '07' if ( $outcome eq 'OK' );
	  $c_num    = '13' if ( $outcome eq 'WARNING' );
	  $c_num    = '05' if ( $outcome eq 'ERROR' );
	  # my $text = sprintf( "%-66s%2s%7s%-5s\n\n", $name, '[ ', $outcome. ' ' x $l, ' ]' );
	  my $text = sprintf( "%-66s%2s%7s%-5s", $name, '[ ', $outcome. ' ' x $l, ' ]' );
	  #  cprintf( "%-66s%2s\x03$c_num%7s\x030%-5s", $name, '[ ', $outcome. ' ' x $l, ' ]' );
	  #  my $text = cprintf( "%-66s%2s\x03$c_num%7s\x030%-5s", $name, '[ ', $outcome. ' ' x $l, ' ]' );
	  print $text, "\n\n";
	  print $file $text if defined $file;
	}

	my @params = ( 'theta', 'omega', 'sigma' );

	sub sum {
	  my $arr = shift;
	  my $sum = 0;
	  foreach my $param ( @params ) {
	    next if ( not defined $arr -> {$param} );
	    my @nums = sort { $a <=> $b } keys %{$arr -> {$param}};
	    foreach my $num ( @nums ) {
	      if ( ref($arr -> {$param}{$num}) eq 'HASH' ) {
		my @sides = sort { $a <=> $b } keys %{$arr -> {$param}{$num}};
		foreach my $side ( @sides ) {
		  $sum += $arr -> {$param}{$num}{$side};
		}
	      } else {
		$sum += $arr -> {$param}{$num};
	      }
	    }
	  }
	  return $sum;
	}
	my $diag_number = scalar @{$self -> {'diagnostic_parameters'}} - 1;
	my %diag_idxs;
	for ( my $i = 0; $i <= $diag_number; $i++ ) {
	  $diag_idxs{$self -> {'diagnostic_parameters'} -> [$i]} = $i;
	}

	open( my $log, ">test.log" );
	for ( my $i = 0; $i < scalar @{$self -> {'raw_results'}} ; $i++ ) { # All models
	  print "MODEL ",$i+1,"\n" if ( scalar @{$self -> {'raw_results'}} > 1 );
	  my $sum = sum( $self -> {'within_interval'}[$i] );
	  if ( not defined $sum or $sum < 1 ) {
	    acknowledge( 'No confidence intervals include '.
			 $self -> {'within_interval_check'}, 'OK', $log );
	  } else {
	    acknowledge( "$sum confidence intervals include ".
			 $self -> {'within_interval_check'}, 'WARNING', $log );
	    foreach my $param ( @params ) {
	      next if ( not defined $self -> {'within_interval'}[$i]{$param} );
	      my @nums = sort { $a <=> $b } keys %{$self -> {'within_interval'}[$i]{$param}};
	      foreach my $num ( @nums ) {
		printf( "\t%-20s%3.3f\t%3.3f\n", uc(substr($param,0,2)).$num,
			$self -> {'confidence_intervals'}[$i]{$param}{$num}{'lower'},
			$self -> {'confidence_intervals'}[$i]{$param}{$num}{'upper'} )
		  if ( $self -> {'within_interval'}[$i]{$param}{$num} );
		print $log sprintf( "\t%-20s\n",uc($param).$num )
		   if ( $self -> {'within_interval'}[$i]{$param}{$num} );
	      }
	    }
	  }

	  my $sum = sum( $self -> {'near_boundary'}[$i] );
	  if ( not defined $sum or $sum < 1 ) {
	    acknowledge( 'No confidence interval limit search ended near a boundary '.
			 $self -> {'near_boundary_check'}, 'OK', $log );
	  } else {
	    acknowledge( "$sum searches for confidence intervals limits ended near a boundary ",
			 'WARNING', $log );
	    foreach my $param ( @params ) {
	      next if ( not defined $self -> {'near_boundary'}[$i]{$param} );
	      my @nums = sort { $a <=> $b } keys %{$self -> {'near_boundary'}[$i]{$param}};
	      foreach my $num ( @nums ) {
		next if ( not defined $self -> {'near_boundary'}[$i]{$param} );
		foreach my $side ( 'lower', 'upper' ) {
		  printf( "\t%-20s%-8s%3.3f\n", uc(substr($param,0,2)).$num, $side,
			  $self -> {'confidence_intervals'}[$i]{$param}{$num}{$side} )
		    if ( $self -> {'near_boundary'}[$i]{$param}{$num}{$side} );
		  print $log sprintf( "\t%-20s%-8s%3.3f\n", uc(substr($param,0,2)).$num, $side,
				      $self -> {'confidence_intervals'}[$i]{$param}{$num}{$side} )
		    if ( $self -> {'near_boundary'}[$i]{$param}{$num}{$side} );
		}
	      }
	    }
	  }

	  my $sum = sum( $self -> {'hit_max_iterations'}[$i] );
	  if ( not defined $sum or $sum < 1 ) {
	    acknowledge( 'No confidence interval limit search reached the maximum number of iterations '.
			 $self -> {'hit_max_iterations_check'}, 'OK', $log );
	  } else {
	    acknowledge( "$sum searches for confidence intervals limits reached the maximum number of iterations ",
			 'WARNING', $log );
	    foreach my $param ( @params ) {
	      next if ( not defined $self -> {'hit_max_iterations'}[$i]{$param} );
	      my @nums = sort { $a <=> $b } keys %{$self -> {'hit_max_iterations'}[$i]{$param}};
	      foreach my $num ( @nums ) {
		next if ( not defined $self -> {'hit_max_iterations'}[$i]{$param} );
		foreach my $side ( 'lower', 'upper' ) {
		  printf( "\t%-20s%-8s\n", uc(substr($param,0,2)).$num, $side )
		    if ( $self -> {'hit_max_iterations'}[$i]{$param}{$num}{$side} );
		  print $log sprintf( "\t%-20s%-8s\n", uc(substr($param,0,2)).$num, $side )
		    if ( $self -> {'hit_max_iterations'}[$i]{$param}{$num}{$side} );
		}
	      }
	    }
	  }

	  my $sum = sum( $self -> {'skewed_intervals'}[$i] );
	  if ( not defined $sum or $sum < 1 ) {
	    acknowledge( 'No CI limit distance ratio exceeds the given thresholds', 'OK', $log );
	  } else {
	    acknowledge( "$sum CI limit distance ratios exceed the given thresholds", 'WARNING', $log );
	    foreach my $param ( @params ) {
	      next if ( not defined $self -> {'skewed_intervals'}[$i]{$param} );
	      my @nums = sort { $a <=> $b } keys %{$self -> {'skewed_intervals'}[$i]{$param}};
	      foreach my $num ( @nums ) {
		printf( "\t%-15s%3.3f  is less than %3.3f or larger than %3.3f\n", uc(substr($param,0,2)).$num,
			$self -> {'interval_ratio'}[$i]{$param}{$num},
			(1/$self -> {$param.'_interval_ratio_check'}),
			$self -> {$param.'_interval_ratio_check'} )
		  if ( $self -> {'skewed_intervals'}[$i]{$param}{$num} );
		print $log sprintf( "\t%-20s\n",uc($param).$num )
		   if ( $self -> {'skewed_intervals'}[$i]{$param}{$num} );
	      }
	    }
	  }
	}
      }
end print_summary

# }}} print_summary

# {{{ _try_bounds

start _try_bounds
      {
	# Anv䮤 sort f?tt f堦ram l䧳ta och h?a pr?e v?et
	# anv䮤 de f?tt fixa estimat utanf?r䮳erna.
	
	if ( ( $side eq 'lower' and $guess < $bound ) or
	     ( $side eq 'upper' and $guess > $bound ) ) {
	  my @s_log = sort { $a <=> $b } @param_log;
	  if ( $side eq 'lower' and $guess < $bound ) {
	    $guess = ( $bound - $s_log[0])*0.9+$s_log[0];
	    debug -> warn( level   => 1,
			   message => "Corrected lower guess to $guess" );
	  }
	  if ( $side eq 'upper' and $guess > $bound ) {
	    $guess = ( $bound - $s_log[$#s_log])*0.9+$s_log[$#s_log];
	    debug -> warn( level   => 1,
			   message => "Corrected upper guess to $guess" );
	  }
	}

	$new_value = $guess;
      }
end _try_bounds

# }}} _try_bounds

# {{{ _aag

start _aag
      {
	my $total = scalar(@x);
	debug -> die( message => "No data supplied to the polynomial approximation".
		      " algorithm in the log-likelihood profiling tool" ) if ( $total < 1 );

	my $y=0;    my $y2=0;    my $x1=0;    my $x2=0;
	my $x3=0;   my $x4=0;    my $x1y=0;   my $x2y=0;

	my $count=0;
	while ($count<$total){
	  $y+=$y[$count];
	  $y2+=($y[$count])**2;
	  $x1+=$x[$count];
	  $x2+=($x[$count])**2;
	  $x3+=($x[$count])**3;
	  $x4+=($x[$count])**4;
	  $x1y+=$x[$count]*$y[$count];
	  $x2y+=(($x[$count])**2)*$y[$count];
	  ++$count;
	}
	my $a = $x1y - $x1*$y/$total;
	my $b = $x3 - $x1*$x2/$total;
	my $c = $x2 - $x1**2/$total;
	my $d = $x2y - $x2*$y/$total;
	my $e = $x4 - $x2**2/$total;

	# Try to avoid division by zero
	$c = $c == 0 ? 0.00001 : $c;
	my $tmp = ($b**2/$c - $e);
	$tmp = $tmp == 0 ? 0.00001 : $tmp;

	my $apr1 = ($a*$b/$c - $d) / $tmp;
	my $apr2 = $a/$c - $b*$apr1/$c;
	my $apr3 = ($y - $apr2*$x1 - $apr1*$x2)/$total;

	@polynomial = ($apr1,$apr2,$apr3);
      }
end _aag

# }}} _aag

# {{{ _guess

start _guess
      {
	my @x = @{$param_log[0]};
	my @y = @{$param_log[1]};
	
	debug -> die( message => 'The number of logged parameter values ('.
		      scalar @{$param_log[0]}.
		      ') does not match the number of logged ofv-diffs ('.
		      scalar @{$param_log[1]}.')' ) if ( scalar @{$param_log[0]} !=
							 scalar @{$param_log[1]} );
	my ( @x1, @y1 );
	
	my $points = scalar(@x);
	
	my $zero = 0;

	while ($y[$zero] > 0){
	  $zero++;
	}

	if ( $side eq 'lower' ) {
	  @x1 = @x[0..2];
	  @y1 = @y[0..2];
	  
	} else {
	  @x1 = @x[$points-3..$points-1];
	  @y1 = @y[$points-3..$points-1];
	}

	my $goal = $y[$zero]+ $self -> {'ofv_increase'};
	
	my @pol = @{$self -> _aag( x => \@x1,
				   y => \@y1 ) };

#	print "X: @x1\n";
#	print "Y: @y1\n";
#	print "pol:  @pol\n";
#	print "goal: $goal\n";
	if( $pol[0] == 0 ) {
	  print "The log-likelihood profile could not be approximated by a second order polynomial\n".
	      "The output may not be correct\n";
	  $pol[0] = 0.00001;
	}

	if ( $side eq 'lower' ){
	  if ($pol[0] > 0){
	    $guess = -$pol[1]/2/$pol[0] -
	      (($pol[1]/2/$pol[0])**2 - ($pol[2]-$goal)/$pol[0])**(0.5);
#	    print "guess: $guess\n";
	  } else {
	    $guess = -$pol[1]/2/$pol[0] +
	      (($pol[1]/2/$pol[0])**2 - ($pol[2]-$goal)/$pol[0])**(0.5);
	  }
	  
	} else {
	  if ($pol[0] > 0){
	    $guess = -$pol[1]/2/$pol[0] +
	      (($pol[1]/2/$pol[0])**2 - ($pol[2]-$goal)/$pol[0])**(0.5);
#	    print "guess: $guess\n";
	  } else {
	    $guess = -$pol[1]/2/$pol[0] -
	      (($pol[1]/2/$pol[0])**2 - ($pol[2]-$goal)/$pol[0])**(0.5);
	  }
	}
	
	if ($guess eq 'nan' or $guess eq '-1.#IND' ){ #'-1.#IND' - is that a compiler spec. signal?
	  if ( ($y[0] - $y[1]) == 0 or ($x[0] - $x[1]) == 0 or
	       ($y[$points-1] - $y[$points-2]) == 0 or ($x[$points-1] - $x[$points-2]) == 0 ) {
	    $guess = undef;
	  } else {
	    if ( $side eq 'lower' ){
	      $guess = $x[0] - ($goal - $y[0]) / ( ($y[0] - $y[1])/($x[0] - $x[1]));
	    } else {
	      $guess = $x[$points-1] + ($goal - $y[$points-1]) / ( ($y[$points-1] - $y[$points-2])/($x[$points-1] - $x[$points-2]));
	    }
	  }
	}

#	print "Goal: $goal\n";
#	print LOG "X1: @x1, Y1: @y1\n";
#	print "Polynom: @pol\n";
#	print "Guess side $side: $guess\n";
      }
end _guess

# }}} _guess

# {{{ _test_sigdig

start _test_sigdig
      {
	$number = &FormatSigFigs($number, $sigdig );
	if ( $goal == 0 ) {
	  $number = sprintf( "%.4f", $number );
	  $goal = sprintf( "%.4f", $goal );
	} else {
	  $goal = &FormatSigFigs($goal, $sigdig );
	}
	$test = $number eq $goal ? 1 : 0;
      }
end _test_sigdig

# }}} _test_sigdig

# {{{ _make_new_guess

start _make_new_guess
      {
#	if ( not $done ) {
	  my %run;
	  $run{'thetas'} = (ref( $self -> {'run_thetas'} -> [0] ) eq 'ARRAY') ? 
	    $self -> {'run_thetas'} -> [$model_number-1]:$self -> {'run_thetas'};
	  $run{'omegas'} = (ref( $self -> {'run_omegas'} -> [0] ) eq 'ARRAY') ? 
	    $self -> {'run_omegas'} -> [$model_number-1]:$self -> {'run_omegas'};
	  $run{'sigmas'} = (ref( $self -> {'run_sigmas'} -> [0] ) eq 'ARRAY') ? 
	    $self -> {'run_sigmas'} -> [$model_number-1]:$self -> {'run_sigmas'};

	  my $orig_output;
	  my $orig_model = $self -> {'models'} -> [$model_number-1];
	  if ( $first ) {
	    'debug' -> die( message => "No output object defined through model" )
		unless ( defined $orig_model -> outputs -> [0] );
	    $orig_output = $orig_model -> outputs -> [0];
	  }
	  # Loop over the parameter names
	  foreach my $param ( 'theta', 'omega', 'sigma' ) {
	    # jump to next parameter if no parameter of this type should be run
	    next unless ( defined $run{$param.'s'} and
			  scalar @{$run{$param.'s'}} > 0 and
			  $run{$param.'s'}->[0] ne '' );
	    my %bounds;
	    $bounds{'lower'} =
	      $orig_model -> lower_bounds( parameter_type => $param );
	    $bounds{'upper'} =
	      $orig_model -> upper_bounds( parameter_type => $param );
	    my $accessor    = $param.'s';
	    my @par_nums    = @{$run{$param.'s'}};
	    if ( $first ) {
	      my $orig_ests    = $orig_output -> $accessor;
	      my $se_accessor  = 'se'.$param.'s';
	      my $orig_se_ests = $orig_output -> $se_accessor;
	      # Loop over the parameter numbers of interest
	      foreach my $num ( @par_nums ) {
		# Loop over the problems:
		for ( my $j = 0; $j < scalar @{$orig_ests}; $j++ ) {
		  die "Subproblems are not allowed for the log-likelihood profiling tool\n"
		    if ( scalar @{$orig_ests->[$j]} > 1 );
		  my $orig  = $orig_ests->[$j][0][$num-1];
		  my $upbnd = $bounds{'upper'}->[$j][$num-1];
		  my $lobnd = $bounds{'lower'}->[$j][$num-1];
		  my $width;
		  if ( defined $orig_se_ests->[$j][0][$num-1] ) {
		    $width = abs( $orig_se_ests->[$j][0][$num-1] *
				  $self -> {'normq'} );
		  } elsif ( defined $self -> {'rse_'.$param.'s'}->[$model_number-1]{$num} ) {
		    $width = abs( $self -> {'rse_'.$param.'s'}->[$model_number-1]{$num}/100*abs($orig) *
				  $self -> {'normq'} );
		  } else {
		    die "No estimate of the standard error of $param $num is available from the output file nor from the command line\n";
		  }
		  my $upper = $orig + $width;
		  my $lower = $orig - $width;
#		  print "Upper: $upper, lower: $lower\n";
		  $lower = ( defined $lobnd and $lower < $lobnd  ) ?
		    ($lobnd-$orig)*0.9+$orig : $lower;
		  $upper = ( defined $upbnd and $upper > $upbnd ) ?
		    ($upbnd-$orig)*0.9+$orig : $upper;
#		  print "Upbnd: $upbnd, lobnd: $lobnd\n";
#		  print "Upper: $upper, lower: $lower\n";
		  unshift( @{$self->{$param.'_log'}->{$num}->[$j]->[0]}, $lower );
		  push( @{$self->{$param.'_log'}->{$num}->[$j]->[0]}, $upper );
		}
	      }
	    } else {
	      # Loop over the parameter numbers of interest
	      foreach my $num ( @par_nums ) {
		# Loop over the problems:
		for ( my $j = 0; $j < scalar @{$bounds{'lower'}}; $j++ ) {
		  my %guesses;
		  foreach my $side ( 'lower', 'upper' ) {
		    # Is this side of the problem finished?
		    next if $self->{$param.'_log'}->{$num}->[$j]->[2]->{$side};
		    # Collect the model outputs
		    debug -> warn( level   => 2,
				   message => "Making new guess for $param number $num on the $side side" );
		    my $bound = $bounds{$side}->[$j][$num-1];
		    my $guess =
		      $self -> _guess( param_log => $self->{$param.'_log'}->{$num}->[$j],
				       side => $side );
#		    print "G1: $num   :   $guess\n";
		    if ( defined $bounds{$side}->[$j][$num-1] ) {
		      $guess =
			$self -> _try_bounds( guess     => $guess,
					      side      => $side,
					      bound     => $bounds{$side}->[$j][$num-1],
					      param_log => $self->{$param.'_log'}->
					      {
					       $num}->[$j]->[0]);
#		      print "G2: $guess\n" if $num == 9;
		    }
		    $guesses{$side} = $guess;
		    if ( not defined $guess ) {
		      print "Warning: The search for the $side CI-limit for $param $num ".
			  "could not continue due to numerical difficulties\n";
		      $self->{$param.'_log'}->{$num}->[$j]->[2]->{$side} = 1;
		    }
		  }
		  unshift( @{$self->{$param.'_log'}->{$num}->[$j]->[0]}, $guesses{'lower'} )
		    if ( defined $guesses{'lower'} );
		  push( @{$self->{$param.'_log'}->{$num}->[$j]->[0]}, $guesses{'upper'} )
		    if ( defined $guesses{'upper'} );
		}
	      }
	    }
	  }
	  # Logging must be done fairly quick, therefore this loop by itself
	  open( DONE, '>'.$self -> {'directory'}."/m$model_number/done" );
	  foreach my $param ( 'theta', 'omega', 'sigma' ) {
	    next unless ( defined $self->{$param.'_log'} );
	    while ( my ( $num, $probs ) = each %{$self->{$param.'_log'}} ) {
	      # Loop over the problems:
	      for ( my $prob = 0; $prob < scalar @{$probs}; $prob++ ) {
		foreach my $side ( 'lower', 'upper' ) {
		  next if $self->{$param.'_log'}->{$num}->[$prob]->[2]->{$side};
		  my $log_size = scalar @{$probs -> [$prob] -> [0]};
		  if ( $side eq 'lower' ) {
		    print DONE "$param $num $prob $side ",
		      $probs -> [$prob] -> [0] -> [0],"\n";
		  } elsif ( $side eq 'upper' ) {
		    print DONE "$param $num $prob $side ",
		      $probs -> [$prob] -> [0] -> [$log_size-1],"\n";
		  }
		}
	      }
	    }
	  }
	  close( DONE );
# 	} else {
# 	  open( DONE, $self -> {'directory'}."/m$model_number/done" );
# 	  my @rows = <DONE>;
# 	  close( DONE );
# 	  for( my $i = 0; $i <= $#rows; $i++ ) { # skip first row
# 	    my ( $param, $num, $prob, $side, $guess ) = split(' ',$rows[$i],5);
# 	    next if $self->{$param.'_log'}->{$num}->[$prob]->[2]->{$side};
# 	    unshift( @{$self->{$param.'_log'}->{$num}->[$prob]->[0]}, $guess )
# 		if ( $side eq 'lower' );
# 	    push( @{$self->{$param.'_log'}->{$num}->[$prob]->[0]}, $guess )
# 		if ( $side eq 'upper' );
# 	  }
# 	}
      }
end _make_new_guess

# }}} _make_new_guess

# {{{ confidence_limits

start confidence_limits
      {
	if ( defined $self -> {'results'} ){
	  for ( my $i = 0; $i < scalar @{$self -> {'results'} -> {'own'}}; $i++ ) {
	    my %num_lim;
	    if ( defined $self->{'results'} -> {'own'}->[$i]->{$parameter.'_log'} ) {
# 	      print "$parameter defined\n";
# 	      print "REF: ",ref($self->{'results'}->
# 				[$i]->{$parameter.'_log'}),"\n";
# 	      print "KEYS: ",keys %{$self->{'results'}->
# 				      [$i]->{$parameter.'_log'}},"\n";
	      my @nums = sort {$a <=> $b} keys %{$self->{'results'} -> {'own'} ->
						   [$i]->{$parameter.'_log'}};
	      foreach my $num ( @nums ) {
		my @prob_lim = ();
# 		print "REF: ",ref($self->{'results'}->
# 				  [$i]->{$parameter.'_log'}->{$num}),"\n";
		for ( my $j = 0; $j < scalar @{$self->{'results'} -> {'own'}->
						 [$i]->{$parameter.'_log'}->{$num}}; $j++ ) {
		  my @last_lvl = @{$self->{'results'} -> {'own'}->
				     [$i]->{$parameter.'_log'}->{$num}->[$j]};
		  push( @prob_lim, [$last_lvl[0][0],$last_lvl[0][scalar @{$last_lvl[0]}-1]] );
# 		  print "REF: ",ref($self->{'results'}->
# 				    [$i]->{$parameter.'_log'}->{$num}->[$j]),"\n";
# 		  print "UP fini: ",$last_lvl[2]->{'upper'},"\n";
# 		  print "LO limit: ",$last_lvl[0][0],"\n";
# 		  print "UP limit: ",$last_lvl[0][scalar @{$last_lvl[0]}-1],"\n";
		}
		$num_lim{$num} = \@prob_lim;
	      }
	    }
	    push( @limits, \%num_lim );
	  }
	}
      }
end confidence_limits

# }}} confidence_limits

# {{{ print_results

start print_results
      {
	# Run the print_results specific for the subtool
	my $sub_print_results = $self -> {'subtools'} -> [0];
	if ( defined $sub_print_results ) {
	  sub get_dim {
	    my $arr      = shift;
	    my $dim      = shift;
	    my $size_ref = shift;
	    $dim++;
	    if ( defined $arr and ref($arr) eq 'ARRAY' ) {
	      push( @{$size_ref}, scalar @{$arr} );
	      ( $dim, $size_ref ) = get_dim( $arr->[0], $dim, $size_ref );
	    }
	    return ( $dim, $size_ref );
	  }
	  sub format_value {
	    my $val = shift;
	    if ( not defined $val or $val eq '' ) {
	      return sprintf("%10s",$PsN::output_style).',';
	    } else {
	      $_ = $val;
	      my $nodot = /.*\..*/ ? 0 : 1;
	      $_ =~ s/\.//g;
	      if ( /.*\D+.*/ or $nodot) {
		return sprintf("%10s",$val).',';
	      } else {
		return sprintf("%10.5f",$val).',';
	      }
	    }
	  }
	  debug -> die( message => "No results_file defined" )
	    unless ( defined $self -> {'results_file'} );
	  open ( RES, ">".$self -> {'directory'}.'/'.$self -> {'results_file'} );
	  if ( defined $self -> {'results'} ) {
	    my @all_results = @{$self -> {'results'}};
	    for ( my $i = 0; $i <= $#all_results; $i++ ) {
	      if ( defined $all_results[$i]{'own'} ) {
		my @my_results = @{$all_results[$i]{'own'}};
		for ( my $j = 0; $j <= $#my_results; $j++ ) {
		  # These size estimates include the problem and sub_problem dimensions:
		  my ( $ldim, $lsize_ref ) = get_dim( $my_results[$j]{'labels'}, -1, [] );
		  my ( $vdim, $vsize_ref ) = get_dim( $my_results[$j]{'values'}, -1, [] );
		  print RES $my_results[$j]{'name'},"\n" if ( $vdim > 1 );
		  if ( defined $my_results[$j]{'values'} and
		       scalar @{$my_results[$j]{'values'}} >= 0 ) {
		    my @values  = @{$my_results[$j]{'values'}};
		    my @labels;
		    if ( defined $my_results[$j]{'labels'} and
			 scalar @{$my_results[$j]{'labels'}} >= 0 ) {
		      @labels = @{$my_results[$j]{'labels'}};
		    }
		    # Print Header Labels
		    if ( $ldim == 0 ) {
		      my $label = \@labels;
		      print RES ','.format_value($label),"\n";
		    } elsif ( $ldim == 2 ) {
		      print RES ',';
		      for ( my $n = 0; $n < scalar @{$labels[1]}; $n++ ) {
			my $label = $labels[1][$n];
			print RES format_value($label);
		      }
		      print RES "\n" if ( scalar @{$labels[1]} );
		    }
		    # Print the values:
		    if ( $vdim == 0 ) {
		      print RES ','.format_value(\@values),"\n";
		    } elsif ( $vdim == 1 ) {
		      for ( my $m = 0; $m < scalar @{\@values}; $m++ ) {
			my $label = $labels[$m];
			print RES ','.format_value($label);
			my $val = $values[$m];
			print RES ','.format_value($val),"\n";
		      }
		    } elsif ( $vdim == 2 ) {
		      for ( my $m = 0; $m < scalar @{\@values}; $m++ ) {
			my $label;
			if ( $ldim == 1 ) {
			  $label = $labels[$m];
			} elsif ( $ldim == 2 ) {
			  $label = $labels[0][$m];
			}
			print RES format_value($label);
			if ( defined $values[$m] ) {
			  for ( my $n = 0; $n < scalar @{$values[$m]}; $n++ ) {
			    print RES format_value($values[$m][$n]);
			  }
			}
			print RES "\n";
		      }
		    }
		  }
		}
	      }
	    }
	  }
	  close( RES );
	} else {
	  debug -> warn( level   => 2,
			 message => "No subtools defined".
			 ", using default printing routine" );
	}
      }
end print_results

# }}}

# {{{ create_matlab_scripts
start create_matlab_scripts
    {
      if( defined $PsN::lib_dir ){
	unless( -e $PsN::lib_dir . '/profiles.m') {
	  'debug' -> die( message => 'LLP matlab template scripts are not installed, no matlab scripts will be generated.' );
	  return;
	}

	open( PROF, $PsN::lib_dir . '/profiles.m' );
	my @file = <PROF>;
	close( PROF );	
      	my $found_code;
	my $code_area_start=0;
	my $code_area_end=0;
	
	for(my $i = 0;$i < scalar(@file); $i++) {
	  if( $file[$i] =~ /% ---------- Autogenerated code below ----------/ ){
	    $found_code = 1;
	    $code_area_start = $i;
	  }
	  if( $file[$i] =~ /% ---------- End autogenerated code ----------/ ){
	    unless( $found_code ){
	      'debug' -> die ( message => 'LLP matlab template script is malformated, no matlab scripts will be generated' );
	      return;
	    }
	    $code_area_end = $i;
	  }
	}
	
	my @auto_code;
	push( @auto_code, "str_format = '%30s';\n\n" );
	
	my %param_names;

	push( @auto_code, "col_names = [ " );
	
	foreach my $param ( 'theta','omega','sigma' ) {
	  my $labels = $self -> {'models'} -> [0] -> labels( parameter_type => $param );
	  if ( defined $labels ){
	    foreach my $label ( @{$labels -> [0]} ){
	      push( @auto_code, "	      sprintf(str_format,'",$label,"');\n" );
	    }
	  }
	}
        push( @auto_code, "	      ];\n\n" );
        push( @auto_code, "goal = 3.84;\n\n" );
	
        push( @auto_code, "filename = 'llplog1_no_header.csv';\n" );
	
	splice( @file, $code_area_start, ($code_area_end - $code_area_start), @auto_code );	
	open( OUTFILE, ">", $self -> {'directory'} . "/profiles.m" );
	print OUTFILE "addpath " . $PsN::lib_dir . ";\n";
	print OUTFILE @file ;
	close OUTFILE;
	
	open( LOGFILE, "<", $self -> {'directory'} . "/llplog1.csv" );
	my @log = <LOGFILE>;
	close LOGFILE;
	
	open( OUTFILE, ">", $self -> {'directory'} . "/llplog1_no_header.csv" );
	for( my $i = 1; $i <= $#log; $i ++ ){ #Skip header
	  print OUTFILE $log[$i];
	}
	close OUTFILE;

      } else {
	'debug' -> die( message => 'matlab_dir not configured, no matlab scripts will be generated.');
	return;
      }
    }
end create_matlab_scripts
# }}}

# {{{ create_R_scripts
start create_R_scripts
    {
      unless( -e $PsN::lib_dir . '/R-scripts/llp.R' ){
	'debug' -> die( message => 'LLP R-script are not installed, no R scripts will be generated.' );
	return;
      }
      cp ( $PsN::lib_dir . '/R-scripts/llp.R', $self -> {'directory'} );
    }
end create_R_scripts
# }}}
