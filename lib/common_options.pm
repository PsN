package common_options;

use FindBin qw($Bin);
use lib "$Bin/../lib";
use Getopt::Long;
use Text::Wrap;

## Configure the command line parsing
Getopt::Long::config("auto_abbrev");

my @tool_options = ( "abort_on_fail",
		     "adaptive!",
		     "clean:i",
		     "compress",
		     "condition_number_limit:f",
		     "correlation_limit:f",
		     "crash_restarts:i",
		     "directory:s",
		     "drop_dropped",
		     "handle_maxevals",
		     "handle_msfo",
		     "handle_crashes!",
		     "large_theta_cv_limit:f",
		     "large_omega_cv_limit:f",
		     "large_sigma_cv_limit:f",
		     "lsf_job_name:s",
		     "lsf_options:s",
		     "lsf_project_name:s",
		     "lsf_queue:s",
		     "lsf_resources:s",
		     "lsf_ttl:s",
		     "max_runtime:i",
		     "min_retries:i",
		     "missing_data_token:s",
		     "near_bound_sign_digits:i",
		     "near_zero_boundary_limit:f",
		     "nice:i",
		     "nm_version:s",
		     "nm_directory:s",
		     "nonparametric_etas",
		     "nonparametric_marginals",
		     "picky",
		     "prepend_model_file_name",
		     "quick_summarize|quick_summary",
		     "rerun:i",
		     "retries:i",
		     "run_on_lsf!",
                     "run_on_ud!",
		     "run_on_sge!",
		     "run_on_zink!",
		     "sge_resource:s",
		     "sge_queue:s",
		     "seed:s",
		     "shrinkage",
		     "significant_digits_rerun:f",
		     "sign_digits_off_diagonals:i",
		     "summarize|summary",
		     "threads:i",
		     "tweak_inits:i",
		     "ud_native_retrieve",
		     "ud_sleep:i",
		     "verbose!",
		     "wrap_data",
		     "unwrap_table_files",
		     "near_bound_sign_digits:i",
		     "near_zero_boundary_limit:f",
		     "large_theta_cv_limit:f",
		     "large_omega_cv_limit:f",
		     "large_sigma_cv_limit:f",
		     "confidence_level:f",
		     "precision:i",
		     );


my @model_options = ("extra_data_files:s@",
		     "extra_files:s",
		     "extra_output:s",
		     "sde",
		     "cwres",
		     "mirror_plots:i",
		     "iofv",
		     "mirror_from_lst!",
		     "nm_version:s",
		     "outputfile:s",
		     );

my @script_options = ( "debug:i",
		       "debug_package:s",
		       "debug_subroutine:s",
		       "h|?",
		       "help",
		       "html_help",
#		       "project:s",
		       "silent",
#		       "user:s",
		       "version",
		       "warn_with_trace:i"
		       );

@get_opt_strings = (sort(@tool_options), sort(@model_options), sort(@script_options));

sub options_to_parameters {
  my $opts = shift;
  my @options = @{$opts};

  my $parameter_string = '( ';

  foreach my $opt ( @options ){
    $opt =~ s/[!:|].*//g;
    $parameter_string .= "$opt => \$options{'$opt'},\n";
  }
  $parameter_string .= ' )';
  return $parameter_string;
}

$parameters = options_to_parameters([@tool_options,'top_tool']);


@extra_files;
@extra_output;

sub set_globals {
  my $opts = shift;
  my $command = shift;
  my %options = %{$opts};

  random_set_seed_from_phrase( $options{'seed'} ) if ( defined $options{'seed'} );

  ui -> category( $command );
  ui -> silent(1) if( $options{'silent'} );
  
  debug -> level( $options{'debug'} );
  debug -> package( $options{'debug_package'} );
  debug -> subroutine( $options{'debug_subroutine'} );
  debug -> warn_with_trace( $options{'warn_with_trace'} );

  if ( $PsN::config -> {'_'} -> {'use_database'} ) {
    
    $PsN::config -> {'_'} -> {'project'}  =
	defined $options{'project'} ? $options{'project'} :
	$PsN::config -> {'_'} -> {'default_project'};
    $PsN::config -> {'_'} -> {'user'}  =
	defined $options{'user'} ? $options{'user'} :
	$PsN::config -> {'_'} -> {'default_user'};
    print "Using database ".$PsN::config -> {'_'} -> {'project'}."\n";
    if ( defined $options{'password'} ) {
      $PsN::config -> {'_'} -> {'password'} = $options{'password'};
    } elsif( defined $PsN::config -> {'_'} -> {'password'} ) {
      $PsN::config -> {'_'} -> {'password'} =
	  $PsN::config -> {'_'} -> {'default_password'};
    } else {
      system( "stty -echo" );
      print "Database password for ".$PsN::config -> {'_'} -> {'user'}.": ";
      my $word;
      chomp($word = <STDIN>);
      print "\n";
      system( "stty echo" );
      $PsN::config -> {'_'} -> {'password'} = $word;
    }
  }
}

sub get_defaults {
  my $options = shift;
  my $tool    = shift;
  foreach my $default_option ( keys %{$PsN::config -> {'default_'.$tool.'_options'}} ){
    unless( exists $options -> {$default_option} ){
      $options -> {$default_option} = $PsN::config -> {'default_'.$tool.'_options'} -> {$default_option};
    }
    
  }


  foreach my $default_option ( keys %{$PsN::config -> {'default_options'}} ){
    unless( exists $options -> {$default_option} ){
      $options -> {$default_option} = $PsN::config -> {'default_options'} -> {$default_option};
    }
    
  }
  $options -> {'top_tool'} = 1;
}

sub sanity_checks {
  my $options = shift;
  my $tool = shift;

  if( $options -> {'max_runtime'} ){
    if( $Config{osname} eq 'MSWin32' ){
      die "--max_runtime is not allowed when running on Windows";
    }
    if( $options -> {'run_on_sge'} ){
      die "--max_runtime is not allowed when running on SGE";
    }
    if( $options -> {'run_on_lsf'} ){
      die "--max_runtime is not allowed when running on LSF";
    }
    if( $options -> {'run_on_ud'} ){
      die "--max_runtime is not allowed when running on UD Grid";
    }
  }

}

sub print_help {
  my( $command, $required, $optional ) = @_;
  my %is_required;
  my %all_options = (%{$required},%{$optional});

  foreach my $req( keys %{$required} ){
    $is_required{$req} = 1;
  }

  my $option_help;

  $option_help .= "[ -h | -? ] [ --help ]\n" . ' ' x (1+length($command));
  
  my @loop_array;
  if( $command eq 'execute' ){
    @loop_array = sort(@get_opt_strings);
  } else {
    @loop_array = (keys %{$required}, keys %{$optional});
  }

  foreach my $help( @loop_array ) {
    next if( $help eq 'help' or $help eq 'h|?' );
    unless( $is_required{$help} ){
      $option_help .= "[ ";
    } else {
      $option_help .= "  ";
    }
    if( $all_options{$help} ne '' ){
      $help =~ /^([^:]+)/;
      $option_help .= "--$1=\'" . $all_options{$help} . "\'";
    } elsif( $help =~ /(.+):s/ ){
      $option_help .= "--$1=\'string\'";
    } elsif( $help =~ /(.+):i/ ){
      $option_help .= "--$1=\'integer\'";
    } elsif( $help =~ /(.+):f/ ){
      $option_help .= "--$1=\'number\'";
    } elsif( $help =~ /(.+):(\d)/ ){
      $option_help .= "--$1=$2";
    } elsif( $help =~ /(.+)[^:]$/ ){
      $option_help .= "--$help";
    }
    unless( $is_required{$help} ){
      $option_help .= " ]";
    }
    $option_help .= "\n".' ' x (1+length($command));
  }

  return $option_help;
}

sub model_parameters {
  my $options = shift;
  #my %options = %{$opt};

  if( defined $options -> {'extra_data_files'} ){
    for( my $i=0; $i < scalar(@{$options -> {'extra_data_files'}}) ; $i++ ){
      my @arr = split( /,/ , @{$options -> {'extra_data_files'}}[$i] );
      if( @arr < 2 ){
	die "extra_data_file must be of form: \"filename, head1, head2\"\n" ;
      }

      @{$options -> {'extra_data_files'}}[$i] = $arr[0];
      my @subarray = @arr[1..$#arr];
      push( @{$options -> {'extra_data_headers'}}, \@subarray )
    }
  }

  push(@model_options, 'extra_data_headers');

  if( defined $options -> {'extra_files'} ){
    my @array = split( /,/ , $options -> {'extra_files'} );
    $options -> {'extra_files'} = \@array;
  }

  if( defined $options -> {'extra_output'} ){
    my @array = split( /,/ , $options -> {'extra_output'} );
    $options -> {'extra_output'} = \@array;
  }

  return options_to_parameters(\@model_options);

}


sub online_help {

  my $command = shift;
  my $opts = shift;
  my $help_text = shift;
  my $required_options = shift;
  my $optional_options = shift;
  my %options = %{$opts};
  
  my %help_hash;

  $help_hash{Pre_help_message} = << 'EOF';
  <h3 class="heading1">execute</h3>

      Perl script running one or more modelfiles using PsN.
      
  <h3 class="heading1">Usage:</h3>
EOF

    $help_hash{Description} = <<'EOF';
  <h3 class="heading1">Description:</h3>

    The execute utility is a Perl script that allows you to run multiple
    modelfiles either sequentially or in parallel. It is more or less an
    nmfe replacement.
    <br><br>
    The execute utility creates subdirectories where it puts NONMEMs
    input and output files, to make sure that parallel NONMEM runs do not
    interfere with each other. The top directory is by default named
    'modelfit_dirX' where 'X' is a number that starts at 0 and is
    increased by one each time you run the execute utility.
    <br><br>
    When the NONMEM runs are finished, the output and table files will be
    copied to the directory where execute started in which means that you
    can normaly ignore the 'modelfit_dirX' directory. If you need to
    access any special files you can find them inside the
    'modelfit_dirX'. Inside the 'modelfit_dirX' you find a few
    subdirectories named 'NM_runY'. For each model file you
    specified on the command line there will be one 'NM_runY' directory in
    which the actual NONMEM execution takes place. The order of the
    'NM_runY' directories corresponds to the order of the modelfiles given
    on the command line. The first run will take place inside 'NM_run1',
    the second in 'NM_run2' and so on.
EOF

    $help_hash{Examples} = <<'EOF';    
  <h3 class="heading1">Example:</h3>

    <p align="justify" class="style2">$ execute pheno.mod </p>

    <p align="justify">Runs one model file and accepts all default values.</p>

    <p align="justify" class="style2">$ execute -threads=2  -retries=5 phenobarbital.mod pheno_alternate.mod</p>

    <p align="justify">Runs two model files in parallel using 5 possible retries.</>p
EOF

    $help_hash{Options} = <<'EOF';
  <h3 class="heading1">Options:</h3>

    The options are given here in their long form. Any option may be
    abbreviated to any nonconflicting prefix. The <span class="style2">-threads</span> option may
    be abbreviated to <span class="style2">-t</span> (or even <span class="style2">-thr</span>) but <span class="style2">-debug</span> may not be
    abbreviated to <span class="style2">-d</span> because it conflicts with <span class="style2">-debug_packages</span> and
    <span class="style2">-debug_subroutines</span>.
    <br><br>
    The following options are valid:
EOF

    $help_hash{'-?'} = <<'EOF';
    <p class="style2">-h | -?</p>

    With <span class="style2">-h</span> or <span class="style2">-?</span> execute.pl prints the list of available options 
    and exit.
EOF

    $help_hash{-help} = <<'EOF';
    <p class="style2">-help</p>

    With <span class="style2">-help</span> execute will print a longer help message.
EOF

    $help_hash{-nm_version} = <<'EOF';
    <p class="style2">-nm_version='integer'</p>

    If you have more than one installation of NONMEM you can choose
    between them using the <span class="style2">-nm_version</span> option. The installations must be
    specified in the psn.conf file. The default value is 5.
EOF

    $help_hash{-threads} = <<'EOF';
    <p class="style2">-threads='integer'</p>

    Use the threads option to enable parallel execution of multiple
    NONMEM runs. On a desktop computer it is recommended to set
    <span class="style2">-threads</span> to the number of CPUs in the system plus one. You can
    specify more threads, but it will probably not increase the
    performance. If you are running on a computer cluster, you should
    consult your systems administrator to find out how many threads
    you can specify. The <span class="style2">-threads</span> option will be ignored if you run on
    a grid system, since gridshave ther own scheduling algoritms. The
    default value for the <span class="style2">-threads</span> option is 1.
EOF

    $help_hash{-nice} = <<'EOF';
    <p class="style2">-nice='integer'</p>

    This option only has effect on unix like operating systems. It
    sets the priority (or nice value) on a process. You can give any
    value that is legal for the "nice" command, likely it is between 0
    and 19, where 0 is the highest priority. Check "man nice" for
    details.
EOF

    $help_hash{-directory} = <<'EOF';
    <p class="style2">-directory='string'</p>

    The directory option sets the directory in which execute will run
    NONMEM. The default directory name is 'modelfit_dirX' where X will
    be increased by one each time you run the execute utility. You do
    not have to create the directory, it will be done for you.

    If you abort execute or if your system crashes you can use the
    '<span class="style2">-directory</span>' option set to the directory of the execute run that
    crashed. Execute will then not run the modelfiles that had
    finished before the crash, thereby saving some time. Notice that
    is important that you give exactly the same options that you gave
    the first time.
EOF

    $help_hash{-drop_dropped} = <<'EOF';
    <p class="style2">-drop_dropped</p>

    If there are drop columns in your control file and <span class="style2">-drop_dropped</span> 
    is used, PsN will remove those columns from the data set used
    internally. It saves both diskspace and conserves memory
    usage. Note that PsN does NOT alter your original data set, only
    those used internally in PsN.
EOF

    $help_hash{-extra_data_files} = <<'EOF';
    <p class="style2">-extra_data_files='extra_data1.dta, COLUMN1, COLUMN2'</p>

    NONMEM only allows 20 column datasets, but PsN can add code to
    control files that reads extra data columns from a separate
    file. To use this feature you must create a new data file which
    has the same ID row as the main data file. Then you specify a
    comma separated list with <span class="style2">-extra_data_files</span>. The first element 
    in the list is the filename and the rest of the list is the header
    of the extra data file. You can have multiple extra files if neccesary.
EOF

    $help_hash{-extra_files} = <<'EOF';
    <p class="style2">-extra_files='extra_file1.dta, extra_file2.dta'</p>

    If you need extra files in the directory where NONMEM is run you
    specify them in list to the <span class="style2">-extra_files</span> list. It could for 
    example be fortran subroutines you need compiled with NONMEM.
EOF

    $help_hash{-handle_maxevals} = <<'EOF';
    <p class="style2">-handle_maxevals='number'</p>

    NONMEM only allows 9999 function evaluations. PsN can expand this
    limit by adding an MSFO option to $ESTIMATION, later when NONMEM
    hits the max number of function evaluations(9999) PsN will remove
    intial estimates from the modelfile and add $MSFI and restart
    NONMEM. PsN will do this until the number of evaluations specified
    with <span class="style2">-handle_maxevals</span> is reached.
EOF

    $help_hash{-seed} = <<'EOF';
    <p class="style2">-seed='string'</p>

    If you use the <span class="style2">-retries='integer'</span> option, execute will use a
    random number to create new intial estimates for the model
    parameters. To make sure that the same result is produced if you
    redo the same run, you can set your own random seed with the <span class="style2">-seed</span>
    option.

EOF

    $help_hash{'-summarize|summary'} = <<'EOF';
    <p><span class="style2">-summarize</span> or <span class="style2">-summary</span></p>

    <span class="style2">summarize</span> or <span class="style2">-summary</span> will do a set of diagnostics test 
    and print minimization message for each model run.
EOF

    $help_hash{-verbose} = <<'EOF';
    <p class="style2">-verbose</p>

    With <span class="style2">verbose</span> set to 1, PsN will print
    more details about NONMEM runs. More precisely PsN will print the
    minimization message for each successfull run and a R:X for each
    retry PsN makes of a failed run, where X is the run number.
EOF

    $help_hash{-wrap_data} = <<'EOF';
    <p class="style2">-wrap_data</p>

    NONMEM only allows 20 column datasets, but it is possible to wrap
    observation lines into multiple rows by adding a CONT column. With
    <span class="style2">wrap_data</span> PsN does it automatically.
EOF

    $help_hash{-lsf_job_name} = <<'EOF';
    <p class="style2">-lsf_job_name='string'</p>

    <span class="style2">lsf_job_name</span> sets the name of the LSF job name of every NONMEM run, 
    they all get the same name.
EOF

    $help_hash{-lsf_options} = <<'EOF';
    <p class="style2">-lsf_options='string'</p>

    LSF jobs are submitted using bsub and all LSF related options are
    translated to corresponding bsub options. For maximum flexibility
    we allow any string to be passed as options to bsub, so if a specific 
    bsub feature not available through any ot the other -lsf_ options 
    is needed, use <span class="style2">lsf_options</span> to pass any option to bsub.
EOF

    $help_hash{-lsf_project_name} = <<'EOF';
    <p class="style2">-lsf_project_name='string'</p>

    Use <span class="style2">lsf_project_name</span> to assign a
    project name to your LSF runs.
EOF

    $help_hash{-lsf_resources} = <<'EOF';
    <p class="style2">-lsf_resources='string'</p>

    <span class="style2">lsf_resources</span> specifies which LSF resources is required when submiting
    NONMEM runs.
EOF

    $help_hash{-lsf_ttl} = <<'EOF';
    <p class="style2">-lsf_ttl='string'</p>

    <span class="style2">lsf_ttl</span> sets the maximum time a NONMEM run should be allowed to run on 
    the LSF grid.
EOF

    $help_hash{-lsf_queue} = <<'EOF';
    <p class="style2">-lsf_queue='string'</p>

    <span class="style2">lsf_queue</span> specifies which LSF queue PsN should submit NONMEM runs 
    to and is used in conjuction with <span class="style2">-run_on_lsf</span>
EOF

    $help_hash{-min_retries} = <<'EOF';
    <p class="style2">-min_retries='string'</p>

    <span class="style2">min_retries</span> forces the PsN to try
    several initial values for each estimate and selecting the best
    one. The best model is chosen in the following maner: if <span class="style2">-picky</span>
    is used the model must pass the picky test. Then the one with
    highest number of significant digits and an ofv value no more than
    five units above than the lowest ofv value among all models.  
EOF

    $help_hash{-clean} = <<'EOF';
    <p class="style2">-clean</p>

    The <span class="style2">-clean</span> clean option can take four different values:
    0 - means that nothing is removed, 1 - NONMEM intermediate files and binary is removed (this is the default), 
    2 - model and output files generated by PsN restarts are removed, 3 - the wholde NM_run directory is removed 
    and if it is not an "execute" command, all modelfit_dir's will be removed.
EOF

    $help_hash{-missing_data_token} = <<'EOF';
    <p class="style2">-missing_data_token='string'</p>

    <span class="style2">missing_data_token</span> sets the string
    that PsN accepts as missing data, default is -99.
EOF

    $help_hash{-nm_directory} = <<'EOF';
    <p class="style2">-nm_directory='string'</p>

    The argument of <span class="style2">nm_directory</span> is
    directory where NONMEM is installed. Normally its easiest to setup
    a version in psn.conf and use <span class="style2">-nm_version</span> to access it.
EOF

    $help_hash{-no_remote_compile} = <<'EOF';
    <p class="style2">-no_remote_compile</p>

    When running on LSF it is no guaranteed that NONMEM is available
    on the computing node, then <span class="style2">-no_remote_compile</span> allows you to compile 
    NONMEM localy and only submit the NONMEM executable to the grid.
EOF

    $help_hash{-no_remote_execution} = <<'EOF';
    <p class="style2">-no_remote_execution</p>

    <span class="style2">no_remote_execution</span> prohibits execution on the LSF grid. Used together 
    with <span class="style2">-no_remote_compile</span> it cancels out <span class="style2">-run_on_lsf</span>
EOF

    $help_hash{-compress} = <<'EOF';
    <p class="style2">-compress</p>

    The execute utility will compress the contents of 'NM_runX' to the
    file 'nonmem_files.tgz' if the <span class="style2">-compress</span> option is used and if you
    have the archive and compress programs <strong>tar</strong> and <strong>gzip</strong> installed. If
    you use the <span class="style2">-clean</span> options, run files will be
    removed before the compression. The <span class="style2">-compress</span> option obviously has
    no effect if you also use the <span class="style2">-clean</span> option.
EOF

    $help_hash{-tweak_inits} = <<'EOF';
    <p class="style2">-tweak_inits</p>

    <!--/>If NONMEM terminates nonsuccessfully, PsN can perturb the initial
    estimates and run NONMEM again. The generation of new initial
    estimates init_i for the i:th retry are performed according to

    init_i = init_0 + rand_uniform(+-0.1*i*init_0)

    where init_0 are the initial estimates of the original run. The
    updating procedure makes sure that boundary conditions on the
    parameters are still valid. For this option to have effect, the
    -retries option must be set to number larger than zero. The
    default setting uses tweak_inits.<-->
    <?php print '<p>  If NONMEM terminates nonsuccessfully, PsN can perturb the initial estimates  and run NONMEM again. The generation of new initial estimates <img src="images/init1.gif"> for the <em>i</em>:th retry are performed according to</p><p align="center"><img src="images/perturb1.gif" width="236" height="32"></p> <p>where <img src="images/init_orig1.gif" width="29" height="28"> are the initial estimates of the original run. The updating procedure makes sure that boundary conditions on the parameters are still valid. For this option to be valid, the <span class="style2">-retries</span> option must be set to a number larger than zero. The default setting uses tweak_inits. </p>'; ?>
EOF

    $help_hash{-outputfile} = <<'EOF';
    <p class="style2">-outputfile</p>

    The <span class="style2">-outputfile</span> option specifies the output file name for the
    NONMEM run. Currently This option is only valid when a single
    model is supplied to the execute utility.
EOF

    $help_hash{-picky} = <<'EOF';
    <p class="style2">-picky</p>

    The <span class="style2">-picky</span> option is only valid together with <span class="style2">-tweak_inits</span>. 
    Normally PsN only tries new initial estimates if 
    '<span class="style2">MINIMZATION SUCCESSFUL</span>' is not found in the NONMEM output
    file. With the <span class="style2">-picky</span> option, PsN will regard any of the
    following messages as a signal for rerunning:
<p class="style2">
    0ESTIMATE OF THETA IS NEAR THE BOUNDARY<br>
    0PARAMETER ESTIMATE IS NEAR ITS BOUNDARY<br>
    0R MATRIX ALGORITHMICALLY SINGULAR<br>
    0S MATRIX ALGORITHMICALLY SINGULAR</p>
EOF

    $help_hash{'-quick_summarize|quick_summary'} = <<'EOF';
    <p><span class="style2">-quick_summarize</span> or <span class="style2">-quick_summary</span></p>

    If either of <span class="style2">quick_summarize</span> and <span class="style2">quick_summary</span> is used, PsN will print 
    the ofv value and minimization message for each NONMEM run.
EOF

    $help_hash{-rerun} = <<'EOF';
    <p class="style2">-rerun</p>

    PsN can redo or resume a run using information in PsN run 
    directory(see documentation for <span class="style2">-directory</span>). It is called 
    a rerun. During a rerun PsN will consider to redo parts of 
    the run. With the <span class="style2">-rerun</span> option you can control which parts 
    will be redone. The default value of <span class="style2">-rerun</span> is 1. 
    With rerun set to 1 PsN will rerun any model with a missing 
    list file. Notice that every "retry" (see the documentation 
    for <span class="style2">-retries</span> and <span class="style2">-min_retries</span>) will be considered for a rerun.
    This means you can change the value of the <span class="style2">-retries</span> and 
    <span class="style2">-min_retries</span> options if you like more or less retries. 
    Setting <span class="style2">-rerun</span> to 0 means that PsN will not check for 
    missing or incomplete "retry" list files. This is usefull
    if you have one or more run modelfiles and you wish to have 
    a PsN raw_results file or a PsN summary, you do a "execute" 
    run with them as arguments and specify <span class="style2">-rerun=0</span>, PsN will not 
    do any NONMEM run, but produce usefull output summary. 
    You can also set <span class="style2">-rerun</span> to 2, and PsN will ignore any existing 
    list files and rerun everything, creating raw_results and 
    summaries from the new listfiles.
EOF

    $help_hash{-run_on_lsf} = <<'EOF';
    <p class="style2">-run_on_lsf</p>

    PsN connects with Platform Load Sharing Facility (LsF). With 
    <span class="style2">-run_on_lsf</span>. PsN will submit to the queue defined in "psn.conf" 
    unless specified with <span class="style2">-lsf_queue</span>.
EOF

    $help_hash{-run_on_ud} = <<'EOF';
    <p class="style2">-run_on_ud</p>

    PsN connects with United Devices Grid MP. With <span class="style2">-run_on_ud</span> PsN will submit to the UD grid
    with parameters defined in the "uduserconf" file.
EOF

    $help_hash{-retries} = <<'EOF';
    <p class="style2">-retries='integer'</p>

    The <span class="style2">-retries</span> option tells the execute utility how many times it
    shall try to rerun a NONMEM job if it fails according to given criterias.. In
    the current version of PsN (2.2), the <span class="style2">-retries</span> option is only
    valid together with <span class="style2">-tweak_inits</span>. The default value of the
    <span class="style2">-retries</span> option is 6.
EOF

    $help_hash{-crash_restarts} = <<'EOF';
    <p class="style2">-crash_restarts='integer'</p>

    If a NONMEM outputfile is produced but PsN is unable to read it
    properly it is assumed that NONMEM crashed, probably due to
    something in the operating system, and PsN will start the run
    again. But PsN will not consider it a retry and will not change
    initial estimates. The default value is 4.
EOF
    $help_hash{-significant_digits_rerun} = <<'EOF';
    <p class="style2">-significant_digits_rerun='number'</p>

    The <span class="style2">-picky</span> option is only valid together with <span class="style2">-tweak_inits</span>. 
    Normally PsN only tries new initial estimates if 
    '<span class="style2">MINIMZATION SUCCESSFUL</span>' is not found in the NONMEM output
    file. With the <span class="style2">-significant_digits_rerun</span>, PsN will rerun if 
    the resulting significant digits is lower than the value 
    specified with this option.
EOF

    $help_hash{-abort_on_fail} = <<'EOF';
    <p class="style2">-abort_on_fail</p>

    If the <span class="style2">-abort_on_fail</span> option is set and one of the NONMEM runs
    fails, execute will stop scheduling more runs and try to stop
    those that are currently running. A run is considered failed if it
    fails to produce a list file which PsN can read. This can occure
    if a nonmem run crashes or gets killed.
EOF

    $help_hash{-adaptive} = <<'EOF';
    <p class="style2">-adaptive</p>

    <span class="style2">-adaptive</span> enables a highly experimental feature to dynamically 
    assign the number of threads depending on the number of running
    nonmem processes on the computer. It requires a server program
    which is not distributed with PsN. If you are interrested in this
    feature, contact the PsN developers.
EOF

    $help_hash{-run_on_nordugrid} = <<'EOF';
    <p class="style2">-run_on_nordugrid</p>

    !! Currently only valid for Linux system !!
    execute will run on nordugrid clusters listed in ~/.ng_cluster .
    If you do not know about Nordugrid, you can safely ignore this option.
    Read more on http://www.nordugrid.org
EOF

    $help_hash{-cpu_time} = <<'EOF';
    <p class="style2">-cpu_time='integer'</p>

    !! Currently only valid for Linux system !!
    This option specifies the number of minutes allocated for a
    gridjob. The default value is 120 minutes. This option is only
    valid together with the <span class="style2">-run_on_nordugrid</span> option.
EOF

    $help_hash{-grid_batch_size} = <<'EOF';
    <p class="style2">-grid_batch_size='integer'</p>

    This option specifies the number of nonmem runs that will be
    grouped together into one grid job. The default number is 5. This
    option is only valid together with the '<span class="style2">-run_on_nordugrid'</span> option.
EOF

    $help_hash{-silent} = <<'EOF';
    <p class="style2">-silent</p>

    The silent option turns off all output from PsN. Results and log
    files are still written to disk, but nothing is printed to the
    screen.
EOF

    $help_hash{-debug} = <<'EOF';
    <p class="style2">-debug='integer'</p>

    The <span class="style2">-debug</span> option is mainly intended for developers who whish to
    debug PsN. By default <span class="style2">-debug</span> is set to zero but you can try
    setting it to '1' to enable warning messages. If you run in to
    problems that require support, you may have to increase this
    number to 2 or 3 and send the output to us.
EOF

    $help_hash{-debug_package} = <<'EOF';
    <p class="style2">-debug_package='string'</p>

    When used together with <span class="style2">-debug</span>, the <span class="style2">-debug_package</span> option makes is
    possible to choose which part of PsN you want to see debug
    messages for. Again, this option is mostly for developers.
EOF

    $help_hash{-debug_subroutine} = <<'EOF';
    <p class="style2">-debug_subroutine='string'</p>
    
    Default value is: empty string

    With this option it is possible to specify, with even finer
    granularity, which part of PsN you want to see debug messages
    from. This is definitly only for developers.
EOF

    $help_hash{-warn_with_trace} = <<'EOF';
    <p class="style2">-warn_with_trace</p>

    If the <span class="style2">-debug</span> level is bigger than zero PsN will print warning 
    messages. If <span class="style2">-warn_with_trace</span> is set, PsN will print a stack 
    trace from the point where the warning message was printed. 
    This is definitly only for developers.

EOF

    $help_hash{-sde} = <<'EOF';
    <p class="style2">-sde</p>

    If you are running SDE models, you must use this option, otherwise
    PsN will destroy the formatting of the models, and the NONMEM runs
    will fail.
EOF

    $help_hash{'-h'} = $help_hash{'-?'};


  if( defined $help_text ){
    %help_hash = %{$help_text};
  }

  if( $options{'version'} ){
    print "PsN version: $PsN::version\n";
    exit;
  }

  my $help;

  if($options{'h'} or $options{'?'} or $options{'help'} ) {

    if( $options{'html_help'} ){
      
      open(EXAMPLES, '>', 'html/' . $command . '_examples.php' );
      print EXAMPLES $help_hash{Examples};
      close( EXAMPLES );

      open(SYNOPSIS, '>', 'html/' . $command . '_synopsis.php' );
      print SYNOPSIS $help_hash{Pre_help_message},"\n";
      print SYNOPSIS "<h3 class=\"heading1\">Synopsis</h3>\n";
      print SYNOPSIS "<span class=\"option\">\n";
      print SYNOPSIS "<pre>$command " . common_options::print_help($command,$required_options, $optional_options)."\n</pre></span>\n" ;
      close( SYNOPSIS );

      open(OPTIONS, '>', 'html/' . $command . '_options.php' );
      my $opt_help;

      if( $command eq 'execute' ){
	@loop_array = @get_opt_strings;
      } else {
	@loop_array = (sort(keys %{$required_options}), sort(keys %{$optional_options}));
      }
      
      foreach my $option( @loop_array ){
	#foreach my $option(keys %help_hash){
	$option =~ s/[^\w]*$|:.*//;
	if( exists $help_hash{'-'.$option}){
	  $opt_help .= $help_hash{'-'.$option}."\n\n";
	} else {
	  $opt_help .= "      <p class=\"option\">-$option</p>     <p>No help available for '$option'</p>";
	}
      }
      print OPTIONS $help_hash{Options} . $opt_help;
      close( OPTIONS );
      
      open(DESC, '>', 'html/' . $command . '_description.php' );
      print DESC $help_hash{Description};
      close( DESC );

      exit;
    } else {

      if( scalar( @ARGV ) > 0 ){
	foreach my $option ( @ARGV ){
	  
	  if( exists $help_hash{'-'.$option} ){
	    $help .= "\n".$help_hash{'-'.$option}. "\n";
	  } else {
	    $help .= "\nNo help available for '$option'\n\n";
	  }
	} 

	$help =~ s/<\?.*\?>//g;
        $help =~ s/<[^>]*>//g;
	print $help;
	exit;
      }
      
      $help .= "\n" . $help_hash{Pre_help_message} . "\n";
      $help .= "\t$command ";
      $help .= common_options::print_help($command,$required_options, $optional_options);

      
      if( $options{'help'} ){
	
	$help .= "\n\n".$help_hash{Description}."\n\n";
	$help .= $help_hash{Examples}."\n\n";
	$help .= $help_hash{Options}."\n\n";
	
	my @loop_array;
	
	if( $command eq 'execute' ){
	  @loop_array = @get_opt_strings;
	} else {
	  @loop_array = (sort(keys %{$required_options}), sort(keys %{$optional_options}));
	}
	
	foreach my $option( @loop_array ){
	  #print "special case: $option\n" if ( $option =~ /\W+$|:.*/ );
	  $option =~ s/[^\w]*$|:.*//;
	  #$option = '-'.$option unless( $option =~ /^-/ );
	  if( exists $help_hash{'-'.$option}){
	    $help .= $help_hash{'-'.$option}."\n\n";
	  } else {
	    $help .= "      -$option\n\n      No help available for '$option'\n\n\n";
	  }
	}
	
	$help .= $help_hash{Post_help_message} . "\n";
	
      } else { 
	$help .= "\n  Use '$command -help' for a longer desctription.\n\n"; 
      } 

      $help =~ s/<\?.*\?>//g;
      $help =~ s/<[^>]*>//g;
      print $help;

      exit;
    }
  }
}



1;
