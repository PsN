# {{{ include

start include statements
    # No brackets!
	use Data::Dumper;
	my $nrec_exp = '^\s*NO. OF DATA RECS IN DATA SET:\s*(\d*)|^\s*TOT. NO. OF DATA RECS:\s*(\d*)';
	my $nobs_exp = ' TOT. NO. OF OBS RECS:\s*(\d*)';
	my $nind_exp = ' TOT. NO. OF INDIVIDUALS:\s*(\d*)';
	my $subprob_exp = '^ PROBLEM NO\.:\s*\d+\s*SUBPROBLEM NO\.:\s*(\d+)';
        use Config;
end include

# }}} include statements

# {{{ new

start new
      {
	# Read Global data
	$this -> _read_nrecs();
	$this -> _read_nobs()             if ( $this -> parsed_successfully() and not
					       $this -> finished_parsing() );
	$this -> _read_nind()             if ( $this -> parsed_successfully() and not
					       $this -> finished_parsing() );
	$this -> _read_msfo_status()      if ( $this -> parsed_successfully() and not
					       $this -> finished_parsing() );
	$this -> _read_block_structures() if ( $this -> parsed_successfully() and not
					       $this -> finished_parsing() );
	$this -> _read_inits()            if ( $this -> parsed_successfully() and not
					       $this -> finished_parsing() );
	$this -> _read_eststep()          if ( $this -> parsed_successfully() and not
					       $this -> finished_parsing() );
	$this -> _read_nonpstep()         if ( $this -> parsed_successfully() and not
					       $this -> finished_parsing() );
	$this -> _read_covstep()          if ( $this -> parsed_successfully() and not
					       $this -> finished_parsing() );
	$this -> _read_tablesstep()       if ( $this -> parsed_successfully() and not
					       $this -> finished_parsing() );
	$this -> _read_prior()            if ( $this -> parsed_successfully() and not
					       $this -> finished_parsing() );
	$this -> _read_steps_allowed()    if ( $this -> parsed_successfully() and not
					       $this -> finished_parsing() );
	$this -> _read_subproblems()	  if ( $this -> parsed_successfully() and not
					       $this -> finished_parsing() );
	my $mes = $this -> parsing_error_message();
	if( defined $this -> subproblems() ) {
	  foreach my $subp ( @{$this -> subproblems()} ) {
	    $mes .= $subp -> parsing_error_message();
	    $this -> parsed_successfully($this -> parsed_successfully() *
					 $subp -> parsed_successfully());
	  }
	}

	$this -> parsing_error_message( $mes );

	if ( defined $this -> {'subproblems'} and $this -> parsed_successfully() ) {
	  $this -> _set_labels;
	}

	delete $this -> {'lstfile'};
      }
end new

# }}} new

# {{{ parsing_error
start parsing_error
    $self -> parsed_successfully( 0 );
$self -> parsing_error_message( $message );
end parsing_error
# }}} parsing_error

# {{{ register_in_database

start register_in_database
    if ( $PsN::config -> {'_'} -> {'use_database'} ) { 
      my ( $date_str, $time_str );
      if ( $Config{osname} eq 'MSWin32' ) {
	$date_str = `date /T`;
	$time_str = ' '.`time /T`;
      } else {
	# Assuming UNIX
	$date_str = `date`;
      }
      chomp($date_str);
      chomp($time_str);
      my $date_time = $date_str.$time_str;
      my $dbh = DBI -> connect("DBI:mysql:host=".$PsN::config -> {'_'} -> {'database_server'}.
			       ";databse=".$PsN::config -> {'_'} -> {'project'},
			       $PsN::config -> {'_'} -> {'user'},
			        $PsN::config -> {'_'} -> {'password'},
			       {'RaiseError' => 1});
      my $sth;
      my @mod_str = ('','');
      if ( defined $self -> {'model_id'} ) {
	@mod_str = ('model_id, ',"$self->{'model_id'}, ");
      }
      $sth = $dbh -> prepare("INSERT INTO ".$PsN::config -> {'_'} -> {'project'}.
			       ".oproblem ".
			     "(output_id,".
			     $mod_str[0].
			     "nrecs,nobs,nind) ".
			     "VALUES ( '$output_id' ,".
			     $mod_str[1].
			     "'$self->{'nrecs'}' ".
			     ",'$self->{'nobs'}' ,'$self->{'nind'}' )");
      $sth -> execute;
      $self -> {'problem_id'} = $sth->{'mysql_insertid'};
      $sth -> finish;
      $dbh -> disconnect;
      if ( defined $self -> {'problem_id'} ) {
	foreach my $problem ( @{$self -> {'subproblems'}} ) {
	  $problem -> register_in_database( output_id  => $output_id,
					    problem_id => $self -> {'problem_id'},
					    model_id   => $model_id );
	}
      }
    }
end register_in_database

# }}} register_in_database

# {{{ _read_subproblems
start _read_subproblems
    my $subproblem_start;
while ( $_ = @{$self -> {'lstfile'}}[ $self -> {'lstfile_pos'}++ ] ) {
  if( /$subprob_exp/ or $self -> {'lstfile_pos'} > $#{$self -> {'lstfile'}} ){
    if( defined $subproblem_start ){
      my @subproblem_lstfile =
	  @{$self -> {'lstfile'}}[$subproblem_start .. $self -> {'lstfile_pos'} - 2];
      my $subproblems;
      if( $self -> {'lstfile_pos'} > $#{$self -> {'lstfile'}} ) {
	$subproblems = $2;
      } else {
	$subproblems = $2 - 1; # Assuming problems come in order
      }
      $self -> add_subproblem
	  ( 'init_data' => {lstfile                    => \@subproblem_lstfile,
			    estimation_step_initiated  => $self -> estimation_step_initiated(),
			    estimation_step_run        => $self -> estimation_step_run(),
			    nonparametric_step_run     => $self -> {'nonparametric_step_run'},
			    covariance_step_run        => $self -> {'covariance_step_run'},
			    msfi_used                  => $self -> msfi_used(),
			    omega_block_structure_type => $self -> {'omega_block_structure_type'},
			    sigma_block_structure_type => $self -> {'sigma_block_structure_type'},
			    omega_block_structure      => $self -> {'omega_block_structure'},
			    sigma_block_structure      => $self -> {'sigma_block_structure'},
			    omega_block_sets           => $self -> {'omega_block_sets'},
			    sigma_block_sets           => $self -> {'sigma_block_sets'},
			    estimated_thetas           => $self -> {'estimated_thetas'},
			    estimated_omegas           => $self -> {'estimated_omegas'},
			    estimated_sigmas           => $self -> {'estimated_sigmas'},
#				  lower_theta_bounds         => $self -> {'lower_theta_bounds'},
#				  upper_theta_bounds         => $self -> {'upper_theta_bounds'},
			    tablename                  => @{$self -> {'tablenames'}}[$subproblems],
			    tableidcolumn              => @{$self -> {'tableidcolumns'}}[$subproblems],
			    model_id                   => $self -> {'model_id'},
			    problem_id                 => $self -> {'problem_id'},
			    output_id                  => $self -> {'output_id'} });
    }
    unless ( $self -> {'lstfile_pos'} > $#{$self -> {'lstfile'}} ){
      $subproblem_start = $self -> {'lstfile_pos'};
    }
  }
}

unless( defined $subproblem_start ) { # No subproblems. Try to make one from the whole file.
  $self -> add_subproblem
      ( 'init_data' => {lstfile                    => $self -> {'lstfile'},
			estimation_step_initiated  => $self -> estimation_step_initiated(),
			estimation_step_run        => $self -> estimation_step_run(),
			nonparametric_step_run     => $self -> {'nonparametric_step_run'},
			covariance_step_run        => $self -> {'covariance_step_run'},
			msfi_used                  => $self -> msfi_used(),
			omega_block_structure_type => $self -> {'omega_block_structure_type'},
			sigma_block_structure_type => $self -> {'sigma_block_structure_type'},
			omega_block_structure      => $self -> {'omega_block_structure'},
			sigma_block_structure      => $self -> {'sigma_block_structure'},
			omega_block_sets           => $self -> {'omega_block_sets'},
			sigma_block_sets           => $self -> {'sigma_block_sets'},
			estimated_thetas           => $self -> {'estimated_thetas'},
			estimated_omegas           => $self -> {'estimated_omegas'},
			estimated_sigmas           => $self -> {'estimated_sigmas'},
#			      lower_theta_bounds         => $self -> {'lower_theta_bounds'},
#			      upper_theta_bounds         => $self -> {'upper_theta_bounds'},
			tablename                  => @{$self -> {'tablenames'}}[0],
			tableidcolumn              => @{$self -> {'tableidcolumns'}}[0],
			model_id                   => $self -> {'model_id'},
			problem_id                 => $self -> {'problem_id'},
			output_id                  => $self -> {'output_id'} } );
}

end _read_subproblems
# }}} _read_subproblems

# {{{ _read_nrecs
start _read_nrecs
# The data recs statement should always be present
# Raise parsing error if not found
    my $errmess = "Error in reading the number of data records!\n";
my $start_pos = $self -> {'lstfile_pos'};
my $success  = 0;

while ( $_ = @{$self -> {'lstfile'}}[$start_pos++] ) {
  if ( /$nobs_exp/ or ($start_pos + 1) == scalar @{$self -> {'lstfile'}} ) {
    debug -> warn( level => 1,
		   message => $errmess."$!" );
    $self -> parsing_error( message => $errmess."$!" );
    return;
  }
  if ( /$nrec_exp/ ) {
    $self -> nrecs($1);
    $success = 1;
    last;
  }
}
if ( $success ) {
  $self -> {'lstfile_pos'} = $start_pos;
} else {
    debug -> warn( level => 1,
		   message => $errmess."$!" );
    $self -> parsing_error( message => $errmess."$!" );
}
end _read_nrecs
# }}} _read_nrecs

# {{{ _read_nobs
start _read_nobs
# The no of obs recs statement should always be present
# Raise parsing error if not found
    my $errmess = "Error in reading the number of observation records!\n";
my $start_pos = $self -> {'lstfile_pos'};
my $success  = 0;

while ( $_ = @{$self -> {'lstfile'}}[$start_pos++] ) {
  if ( /$nind_exp/ or ($start_pos + 1) == scalar @{$self -> {'lstfile'}} ) {
    debug -> warn( level => 1,
		   message => $errmess."$!" );
    $self -> parsing_error( message => $errmess."$!" );
    return;
  }
  if ( /$nobs_exp/ ) {
    $self -> nobs($1);
    $success = 1;
    last;
  }
}
if ( $success ) {
  $self -> {'lstfile_pos'} = $start_pos;
} else {
  debug -> warn( level => 1,
		 message => $errmess."$!" );
  $self -> parsing_error( message => $errmess."$!" );
}
end _read_nobs
# }}} _read_nobs

# {{{ _read_nind
start _read_nind
# The no of individuals statement should always be present
# Raise parsing error if not found
    my $errmess = "Error in reading the number of individuals!\n";
my $start_pos = $self -> {'lstfile_pos'};
my $success  = 0;

while ( $_ = @{$self -> {'lstfile'}}[$start_pos++] ) {
  if ( /^0LENGTH OF THETA/ or
       /^0MODEL SPECIFICATION FILE INPUT/ or
       ($start_pos + 1) == scalar @{$self -> {'lstfile'}} ) {
    debug -> warn( level => 1,
		   message => $errmess."$!" );
    $self -> parsing_error( message => $errmess."$!" );
    return;
  }
  if ( /$nind_exp/ ) {
    $self -> nind($1);
    $success = 1;
    last;
  }
}
if ( $success ) {
  $self -> {'lstfile_pos'} = $start_pos;
} else {
  debug -> warn( level => 1,
		 message => $errmess."$!" );
  $self -> parsing_error( message => $errmess."$!" );
}
end _read_nind
# }}} _read_nind

# {{{ _read_arbitrary

start _read_arbitrary
{
    my $start_pos = $self -> {'lstfile_pos'};
    my $success  = 0;

    while ( $_ = @{$self -> {'lstfile'}}[$start_pos++] ) {
	last if ( /^ PROBLEM.*SUBPROBLEM/ or /^ PROBLEM NO\.:\s+\d/ );
	if ( /$regexp/ ) {
	    $self -> { $member } = $1;
	    $success = 1;
	    last;
	}
    }
    if ( $success ) {
	$self -> {'lstfile_pos'} = $start_pos;
    } else {
	debug -> warn( level   => 1,
		       message => "rewinding to first position..." );
    }
}
end _read_arbitrary

# }}} _read_arbitrary

# {{{ _read_msfo_status
start _read_msfo_status
{
  my $start_pos = $self -> {'lstfile_pos'};
  while( $_ = @{$self -> {'lstfile'}}[ $start_pos++ ] ) {
    
    if( /^0SEARCH WITH ESTIMATION STEP WILL NOT PROCEED BEYOND PREVIOUS TERMINATION POINT/ ){
      $self -> msfo_has_terminated(1); # Means that $ESTIMATION
                                            # must be removed to enable continuation.
      $self -> finished_parsing(1);
    }
    
    if( /^0MODEL SPECIFICATION FILE IS EMPTY/ ){
      $self -> {'msfo_file_empty'} = 1;
      $self -> finished_parsing(1);
    }
  }
}
end _read_msfo_status
# }}}

# {{{ _read_block_structures

start _read_block_structures
# These structures should always be present if no model specification file input is used
# Raise parsing error if not found
# $success is not used, really, with the latest fix
      {
	my $errmess = "Error in reading the block structures!";
	my $start_pos = $self -> {'lstfile_pos'};
	my $success  = 1;
	
	my $obarea = 0;
	my $sbarea = 0;
	
	my $oblock_set = -1;
	my $sblock_set = -1;
	while( $_ = @{$self -> {'lstfile'}}[ $start_pos++ ] ) {
	  if ( /^0MODEL SPECIFICATION FILE INPUT/ ) {
	    # We can't find anything but that's ok
	    $success = 1;
	    $start_pos--;
	    last;
	  }

	  if ( /^0INITIAL ESTIMATE/ or /^0DEFAULT OMEGA BOUNDARY TEST OMITTED:/ ) {
	    # We want to find this if we are currently reading omega
	    # or sigma block structures
	    $success = 1 if ( $sbarea or $obarea ); 
	    $start_pos --;
	    last;
	  }

	  if ( ($start_pos + 1) == scalar @{$self -> {'lstfile'}} ) {
	    #EOF This should not happen, raise error
	    my $errmess = "Reached end of file while parsing block structures\n";
	    debug -> warn( level => 1,
			   message => $errmess."$!" );
	    $self -> parsing_error( message => $errmess."$!" );
	    return;
	  }
	  
	  if(/0OMEGA HAS BLOCK FORM:/) {
	    $self -> {'omega_block_structure_type'} = 'BLOCK';
	    $obarea = 1;
	    $success = 1;
	    next;
	  }
	  if(/0SIGMA HAS BLOCK FORM:/) {
	    $self -> {'sigma_block_structure_type'} = 'BLOCK';
	    $sbarea = 1;
	    $obarea = 0;
	    $success = 1;
	    next;
	  }
	  if ( /^0OMEGA HAS SIMPLE DIAGONAL FORM/ ) {
	    $self -> {'omega_block_structure_type'} = 'DIAGONAL';
	    $success = 1;
	    next;
	  }
	  if ( /^0SIGMA HAS SIMPLE DIAGONAL FORM/ ) {
	    $self -> {'sigma_block_structure_type'} = 'DIAGONAL';
	    $success = 1;
	    last;
	  }
	  if ( $obarea ) {
	    my @row = split;
	    # All rows with the last but one element set to 0 indicate the start of a new block
	    # $#row == 0 indicates the first row of the matrix.
	    if ( $#row == 0 or $row[$#row-1] == 0 ) {
	      # If the same number as previous set
	      if ( $oblock_set == $row[$#row] ) {
		$self -> {'omega_block_sets'}{$oblock_set}{'size'}++;
	      } else {
		$oblock_set = $row[$#row];
		$self -> {'omega_block_sets'}{$oblock_set}{'size'} = 1;
	      }
	      # Always set dimension to 1 when starting a new block
	      $self -> {'omega_block_sets'}{$oblock_set}{'dimension'} = 1;
	    } else {
	      $self -> {'omega_block_sets'}{$oblock_set}{'dimension'}++;
	    }
	    push( @{$self -> {'omega_block_structure'}}, \@row );
	  }
	  if ( $sbarea ) {
	    my @row = split;
	    # All rows with the last but one element set to 0 indicate the start of a new block
	    if ( $#row == 0 or $row[$#row-1] == 0 ) {
	      # If the same number as previous set
	      if ( $sblock_set == $row[$#row] ) {
		$self -> {'sigma_block_sets'}{$sblock_set}{'size'}++;
	      } else {
		$sblock_set = $row[$#row];
		$self -> {'sigma_block_sets'}{$sblock_set}{'size'} = 1;
	      }
	      # Always set dimension to 1 when starting a new block
	      $self -> {'sigma_block_sets'}{$sblock_set}{'dimension'} = 1;
	    } else {
	      $self -> {'sigma_block_sets'}{$sblock_set}{'dimension'}++;
	    }
	    push( @{$self -> {'sigma_block_structure'}}, \@row );
	  }
	}

	unless( defined $self -> {'omega_block_structure_type'} ){
	  $self -> {'omega_block_structure_type'} = 'DIAGONAL';
	}
	unless( defined $self -> {'sigma_block_structure_type'} ){
	  $self -> {'sigma_block_structure_type'} = 'DIAGONAL';
	}

	unless ( $success ) {
	  debug -> warn( level => 1,
			 message => $errmess." 2 $!" );
	  $self -> parsing_error( message => $errmess." 2 $!" );
	} else {
	  $self -> {'lstfile_pos'} = $start_pos;
	}
      }
end _read_block_structures

# }}} _read_block_structures

# {{{ _read_steps_allowed
start _read_steps_allowed
# These statements are optional. Return to start_pos if not found
    my $start_pos = $self -> {'lstfile_pos'};
my $est_allowed  = 1;
my $cov_allowed  = 1;
my $nonp_allowed  = 1;
my $tables_allowed  = 1; # I am not sure that this is actually something which can be marked as not valid

while( $_ = @{$self -> {'lstfile'}}[ $start_pos++ ] ) {
  if ( /^ PROBLEM.*SUBPROBLEM/ or /^ PROBLEM NO\.:\s+\d/ or 
       /^0ITERATION NO./ or /^0MINIMIZATION/) {
    # This is ok, we should end up here
    last;
  }
  
  if( /0ESTIMATION STEP NOT ALLOWED/ ) {
    $est_allowed = 0;
  }

  if( /0COVARIANCE STEP NOT ALLOWED/ ) {
    $cov_allowed = 0;
  }

  if( /0NONPARAMETRIC STEP NOT ALLOWED/ ) {
    $nonp_allowed = 0;
  }

  if( /0TABLES STEP NOT ALLOWED/ ) { # As indicated above, this is unsure but this coding should not harm
    $tables_allowed = 0;
  }

  if( /0INPUT MODEL SPECIFICATION FILE GENERATED FROM A NON-TERMINATING ESTIMATION STEP/ ) {
    if( @{$self -> {'lstfile'}}[ $start_pos ] =~ / BUT CONTINUING ESTIMATION STEP NOT IMPLEMENTED/ ) {
      # If this happens, NONMEM aborts so we are finished reading
      $self -> finished_parsing(1);
    }
  }

  if( /0MODEL SPECIFICATION FILE IS EMPTY/ ) {
    # If this happens, NONMEM aborts so we are finished reading
    $self -> finished_parsing(1);
  }
}

unless( ( $self -> estimation_step_initiated()    * $est_allowed ) or
	( $self -> covariance_step_run()    * $cov_allowed ) or
	( $self -> nonparametric_step_run() * $nonp_allowed ) or
	( $self -> tables_step_run() * $tables_allowed ) ) {
  # If this happens, NONMEM aborts so we are finished reading
  $self -> finished_parsing(1);
}
end _read_steps_allowed
# }}} _read_steps_allowed

# {{{ _read_tablesstep
start _read_tablesstep
# The tables step is optional
    my $start_pos = $self -> {'lstfile_pos'};
my $success  = 0;

while( $_ = @{$self -> {'lstfile'}}[ $start_pos++ ] ) {
  if ( /^1\s*$/ ) {
    # This is ok, the tables step was not used.
    $start_pos -= 2;
    $success = 1;
    last;
  }
  if( /^ PROBLEM NO\.:\s+\d/ or 
      /^0MINIMIZATION/ ) {
    # This should not happen, raise error
    my $errmess = "Found $_ while searching for the (optional) ".
	"tables step indicator\n";
    debug -> warn( level => 1,
		   message => $errmess."$!" );
    $self -> parsing_error( message => $errmess."$!" );
    return;
  }

  if ( ($start_pos + 1) == scalar @{$self -> {'lstfile'}} ) {
    #EOF This should not happen, raise error
    my $errmess = "Reached end of file while  searching for the ".
	"(optional) tables step indicator\n";
    debug -> warn( level => 1,
		   message => $errmess."$!" );
    $self -> parsing_error( message => $errmess."$!" );
    return;
  }

  if(/^0TABLES STEP OMITTED:\s*\b(.*)\b/) {
    $self -> {'tables_step_run'} = 0 if $1 eq 'YES';
    $self -> {'tables_step_run'} = 1 if $1 eq 'NO';
    $success = 1;
    last;
  }
}

unless ( $success ) {
  debug -> warn( level   => 2,
		 message => "rewinding to first position..." );
} else {
  $self -> {'lstfile_pos'} = $start_pos;
}
end _read_tablesstep
# }}} _read_tablesstep

# {{{ _read_prior

start _read_prior
{
  my $start_pos = $self -> {'lstfile_pos'};
  my $success  = 0;
  while( $_ = @{$self -> {'lstfile'}}[ $start_pos++ ] ) {
#    if ( /1DOUBLE PRECISION PREDPP/ ) { This is not always printed 
    if ( /^1\s*$/ or
       /0SEARCH WITH ESTIMATION STEP WILL NOT PROCEED/ ) {
      # This is ok, no user defined prior was used.
      $start_pos -= 2;
      $success = 1;
      last;
    }
    if( /^ PROBLEM NO\.:\s+\d/ or 
	/^0MINIMIZATION/ ) {
      # This should not happen, raise error
      my $errmess = "Found $_ while searching for the (optional) ".
	  "user defined prior indicator\n";
      debug -> warn( level => 1,
		     message => $errmess."$!" );
      $self -> parsing_error( message => $errmess."$!" );
      return;
    }
    
    if ( ($start_pos + 1) == scalar @{$self -> {'lstfile'}} ) {
      #EOF This should not happen, raise error
      my $errmess = "Reached end of file while  searching for the ".
	  "(optional) user defined prior indicator\n";
      debug -> warn( level => 1,
		     message => $errmess."$!" );
      $self -> parsing_error( message => $errmess."$!" );
      return;
    }
    
    if(/^ PRIOR SUBROUTINE USER-SUPPLIED/){
      $self -> {'user_defined_prior'} = 1;
      $success = 1;
      last;
    }
  }

  unless ( $success ) {
    debug -> warn( level   => 2,
		   message => "rewinding to first position..." );
  } else {
    $self -> {'lstfile_pos'} = $start_pos;
  }
  
}
end _read_prior

# }}}

# {{{ _read_nonpstep
start _read_nonpstep
# The nonparametric step is optional
    my $start_pos = $self -> {'lstfile_pos'};
my $success  = 0;

while( $_ = @{$self -> {'lstfile'}}[ $start_pos++ ] ) {
  if ( /^0COVARIANCE STEP OMITTED/ or 
       /0TABLES STEP OMITTED/ or
       /1DOUBLE PRECISION PREDPP/ or
       /0SEARCH WITH ESTIMATION STEP WILL NOT PROCEED/ or
       /^1/ or
       /^0MINIMIZATION/ or
       /^ PROBLEM NO\.:\s+\d/ ) {
    # This is ok, the nonp step was not used.
    last;
  }

#  if( /^ PROBLEM NO\.:\s+\d/ or 
#      /^0MINIMIZATION/ ) {
    # This should not happen, raise error
#    my $errmess = "Found $_ while searching for the (optional) ".
#	"nonparametric step indicator\n";
#    debug -> warn( level => 1,
#		   message => $errmess."$!" );
#    $self -> parsing_error( message => $errmess."$!" );
#    return;
#  }

  if ( ($start_pos + 1) == scalar @{$self -> {'lstfile'}} ) {
    #EOF This should not happen, raise error
    my $errmess = "Reached end of file while  searching for the ".
	"(optional) nonparametric step indicator\n";
    debug -> warn( level => 1,
		   message => $errmess."$!" );
    $self -> parsing_error( message => $errmess."$!" );
    return;
  }

  if(/^0NONPARAMETRIC STEP OMITTED:\s*\b(.*)\b/) {
    $self -> {'nonparametric_step_run'} = 0 if $1 eq 'YES';
    $self -> {'nonparametric_step_run'} = 1 if $1 eq 'NO';
    $success = 1;
    last;
  }
}

unless ( $success ) {
  debug -> warn( level   => 2,
		 message => "rewinding to first position..." );
} else {
  $self -> {'lstfile_pos'} = $start_pos;
}
end _read_nonpstep
# }}} _read_nonpstep

# {{{ _read_eststep
start _read_eststep
# A combination of simulation and estimation step indications should always be found, raise error otherwise
    my $start_pos = $self -> {'lstfile_pos'};
my $success  = 0;

while( $_ = @{$self -> {'lstfile'}}[ $start_pos++ ] ) {
  if ( /^0COVARIANCE STEP OMITTED/ or
       /^0NONPARAMETRIC STEP OMITTED/ or
       /^0TABLES STEP OMITTED/ or
       /^1/ or
       /^0MINIMIZATION/ or
       /^ PROBLEM NO\.:\s+\d/ ) {
    unless( $success ) {
      # This should not happen, raise error
      my $errmess = "Found $_ while searching for the simulation/estimation step indicators\n";
      debug -> warn( level => 1,
		     message => $errmess."$!" );
      $self -> parsing_error( message => $errmess."$!" );
    }
    return;
  }

  if ( ($start_pos + 1) == scalar @{$self -> {'lstfile'}} ) {
    #EOF This should not happen, raise error
    my $errmess = "Reached end of file while searching for the simulation/estimation step indicators\n";
    debug -> warn( level => 1,
		   message => $errmess."$!" );
    $self -> parsing_error( message => $errmess."$!" );
    return;
  }

  if(/^ PRIOR SUBROUTINE USER-SUPPLIED/){
    $success = 1;
  }

  if(/^0ESTIMATION STEP OMITTED:\s*\b(.*)\b/) {
    $self -> estimation_step_initiated(1);
    $self -> estimation_step_run(0) if $1 eq 'YES';
    $self -> estimation_step_run(1) if $1 eq 'NO';
    $success = 1;
  }
  if(/^0SIMULATION STEP OMITTED:\s*\b(.*)\b/) {
    $self -> simulation_step_run(0) if $1 eq 'YES';
    $self -> simulation_step_run(1) if $1 eq 'NO';
    $success = 1;
  }
}

unless ( $success ) {
  debug -> warn( level   => 2,
		 message => "rewinding to first position..." );
} else {
  $self -> {'lstfile_pos'} = $start_pos;
}
end _read_eststep
# }}} _read_eststep

# {{{ _read_covstep
start _read_covstep 
      {
	my $start_pos = $self -> {'lstfile_pos'};
	my $success  = 0;
	
	while( $_ = @{$self -> {'lstfile'}}[ $start_pos++ ] ) {
	  
	  if(/0COVARIANCE STEP OMITTED:\s*\b(.*)\b/) {
	    $self -> {'covariance_step_run'} = 0 if $1 eq 'YES';
	    $self -> {'covariance_step_run'} = 1 if $1 eq 'NO';
	    $success = 1;
	    last;
	  }
	  if ( /^ PROBLEM.*SUBPROBLEM/ or /^ PROBLEM NO\.:\s+\d/ or 
	       /^0ITERATION NO./ or /^0MINIMIZATION/) {
	    # This is ok, we should end up here
	    last;
	  }
	}
	
	unless ( $success ) {
	  debug -> warn( level   => 2,
			 message => "rewinding to first position..." );
	} else {
	  $self -> {'lstfile_pos'} = $start_pos;
	}
      }
end _read_covstep
# }}} _read_covstep

# {{{ _read_inits

start _read_inits
# The inits should always be present if no model specification file input is used
# Raise parsing error if not found
      {
	my $errmess = "Error in reading the initial estimates!\n";
	my $start_pos = $self -> {'lstfile_pos'};
	my ( @thetas, @omegas, @sigmas );
	my $thetarea = 0;
	my $omegarea = 0;
	my $sigmarea = 0;
	my $success  = 0;
	my $tmp = $start_pos;

	# Look for a general statement of fixed sigmas and omegas
	my $all_sigmas_fixed = 0;
	my $all_omegas_fixed = 0;
	while( $_ = @{$self -> {'lstfile'}}[ $tmp++ ] ) {
	  if ( /^0SIGMA CONSTRAINED TO BE THIS INITIAL ESTIMATE/ ) {
	    $all_sigmas_fixed = 1;
	  }
	  if ( /^0OMEGA CONSTRAINED TO BE THIS INITIAL ESTIMATE/ ) {
	    $all_omegas_fixed = 1;
	  }
	  if ( /^0ESTIMATION STEP OMITTED/ or
	       /^0SIMULATION STEP OMITTED/ ) {
	    last;
	  }
	  if ( /^ INITIAL ESTIMATE OF OMEGA HAS A NONZERO BLOCK WHICH IS NUMERICALLY NOT POSITIVE DEFINITE/ ) {
	    $self -> finished_parsing(1);
	    $self -> pre_run_errors($_);
	    return;
	  }
	  if ( ($start_pos + 1) == scalar @{$self -> {'lstfile'}} ) {
	    #EOF This should not happen, raise error
	    my $errmess = "Reached end of file while parsing initial estimates\n";
	    debug -> warn( level => 1,
			   message => $errmess."$!" );
	    $self -> parsing_error( message => $errmess."$!" );
	    return;
	  }

	}

	while( $_ = @{$self -> {'lstfile'}}[ $start_pos++ ] ) {

	  if ( /^0MODEL SPECIFICATION FILE INPUT/ ) {
	    # We can't find any initial estimates but that's ok
	    $self -> msfi_used(1);
	    $success = 1;
	    $start_pos--;
	    last;
	  }

	  if ( /^0INITIAL ESTIMATE OF THETA/ ) {
	    # If we find this we must find other stuff too. Set
	    # success = 0 and change this if we find the rest. Nope,
	    # not true. $THETA can be found alone using the LIKE
	    # option in the $EST record.
	    $thetarea = 1;
	  }

	  if ( /^0ESTIMATION STEP OMITTED/ or
	       /^0SIMULATION STEP OMITTED/ ) {
	    # We want to find this if we are currently reading the omega or sigma inits
	    $success = 1 if ( $thetarea or $sigmarea or $omegarea); 
	    $start_pos --;
	    last;
	  }

	  if ( ($start_pos + 1) == scalar @{$self -> {'lstfile'}} ) {
	    #EOF This should not happen, raise error
	    my $errmess = "Reached end of file while parsing the initial estimates\n";
	    debug -> warn( level => 1,
			   message => $errmess."$!" );
	    $self -> parsing_error( message => $errmess."$!" );
	    return;
	  }
	  if ( $thetarea and /^\s*-?\d*\.\d*/ ) {
	    my @T = split(' ',$_);
	    push(@{$self -> {'initthetas'}},eval($T[1]));
	    push(@{$self -> {'lower_theta_bounds'}},eval($T[0]));
	    push(@{$self -> {'upper_theta_bounds'}},eval($T[2]));
	    if ( $T[0] == $T[1] and $T[0] == $T[2] ) {
	      push(@{$self -> {'fixedthetas'}},1);
	      push(@{$self -> {'estimated_thetas'}},0);
	    } else {
	      push(@{$self -> {'fixedthetas'}},0);
	      push(@{$self -> {'estimated_thetas'}},1);
	    }
	  }
	  if ( /^0INITIAL ESTIMATE OF OMEGA:/ ) {
	    $thetarea = 0;
	    $omegarea = 1;
	    $success = 1;
	    if ( defined $self -> {'omega_block_sets'} and
		 scalar keys %{$self -> {'omega_block_sets'}} > 0 ) {
	      # We currently assume that this part is atomic. No parser checks are made
	      $start_pos++;
	      my %om_bl = %{$self -> {'omega_block_sets'}};
	      my @blocks = sort {$a <=> $b} keys %om_bl;
	      foreach my $block ( @blocks ) {
		my @fix_row = split(' ', $self -> {'lstfile'}[ $start_pos++ ]);
		my $fix = $fix_row[1] eq 'YES' ? 1 : 0;
		if( $all_omegas_fixed ) {
		  $fix = 1;
		}
		for ( my $size = 1; $size <= $om_bl{$block}{'size'}; $size++ ) {
		  for ( my $row = $start_pos ; $row < $start_pos + $om_bl{$block}{'dimension'}; $row++ ) {
		    my @init_row = split(' ', $self -> {'lstfile'}[ $row ]);
		    foreach my $init ( @init_row ) {
		      push( @{$self -> {'fixedomegas'}}, $fix );
		      push( @{$self -> {'estimated_omegas'}}, (not $fix and $size == 1) ? 1 : 0 );
		      push(@{$self -> {'initomegas'}}, eval($init) );
		      push(@{$self -> {'lower_omega_bounds'}},0);
		      push(@{$self -> {'upper_omega_bounds'}},1000000);
		    }
		  }
		}
		$start_pos += $om_bl{$block}{'dimension'};
	      }
	    } else {
	      my $om_row = 1;
 	      while( ($start_pos + 1) < scalar @{$self -> {'lstfile'}} ) {
		if( $self -> {'lstfile'}[ $start_pos ] =~ /^0INITIAL ESTIMATE OF SIGMA/ or
		    $self -> {'lstfile'}[ $start_pos ] =~ /^0SIMULATION STEP OMITTED/ or
		    $self -> {'lstfile'}[ $start_pos ] =~ /^0ESTIMATION STEP OMITTED/ or
		    $self -> {'lstfile'}[ $start_pos ] =~ /^0OMEGA CONSTRAINED TO BE THIS INITIAL ESTIMATE/ ) {
		  $start_pos--;
		  last;
		}

		# After ten rows of omegas NONMEM starts wrapping
		# lines. We then need to skip the first part of the
		# wrapped lines. This nice littel formula calculates
		# how many lines to skip.

		my $skip_lines = ($om_row - $om_row % 10)/10;

		$start_pos += $skip_lines if( $om_row > 10 );
		
		my @init_row = split(' ', $self -> {'lstfile'}[ $start_pos++ ]);
		if( not $init_row[$#init_row] =~ /[0-9]?\.[0-9]{4}E[+-][0-9]{2}/ ) {
		  my $errmess = "Error parsing omega initial estimates, found non-number:\n".
		      $self -> {'lstfile'}[ $start_pos-1 ]."\n";
		  debug -> warn( level => 1,
				 message => $errmess."$!" );
		  $self -> parsing_error( message => $errmess."$!" );
		  return;
		}

		my $init = eval($init_row[$#init_row]);
		unless( $init == 0 ) {
		  push( @{$self -> {'fixedomegas'}}, $all_omegas_fixed ? 1 : 0 );
		  push( @{$self -> {'initomegas'}}, eval($init) );
		}
		push( @{$self -> {'estimated_omegas'}}, $all_omegas_fixed ? 0 : 1 );
		push(@{$self -> {'lower_omega_bounds'}},0);
		push(@{$self -> {'upper_omega_bounds'}},1000000);
		$om_row++;
	      }
	    }
	  }
	  if ( /^0INITIAL ESTIMATE OF SIGMA:/ ) {
	    $thetarea = 0;
	    $omegarea = 0;
	    $sigmarea = 1;
	    if ( defined $self -> {'sigma_block_sets'} and
		 scalar keys %{$self -> {'sigma_block_sets'}} > 0 ) {
	      # We currently assume that this part is atomic. No parser checks are made
	      $start_pos++;
	      my %si_bl = %{$self -> {'sigma_block_sets'}};
	      my @blocks = sort {$a <=> $b} keys %si_bl;
	      foreach my $block ( @blocks ) {
		my @fix_row = split(' ', $self -> {'lstfile'}[ $start_pos++ ]);
		my $fix = $fix_row[1] eq 'YES' ? 1 : 0;
		if( $all_sigmas_fixed ) {
		  $fix = 1;
		}
		for ( my $size = 1; $size <= $si_bl{$block}{'size'}; $size++ ) {
		  for ( my $row = $start_pos ; $row < $start_pos + $si_bl{$block}{'dimension'}; $row++ ) {
		    my @init_row = split(' ', $self -> {'lstfile'}[ $row ]);
		    foreach my $init ( @init_row ) {
		      push( @{$self -> {'fixedsigmas'}}, $fix );
		      push( @{$self -> {'estimated_sigmas'}}, (not $fix and $size == 1) ? 1 : 0 );
		      push(@{$self -> {'initsigmas'}}, eval($init) );
		      push(@{$self -> {'lower_sigma_bounds'}},0);
		      push(@{$self -> {'upper_sigma_bounds'}},1000000);
		    }
		  }
		}
		$start_pos += $si_bl{$block}{'dimension'};
	      }
	    } else {
	      my $sm_row = 1;
 	      while( ($start_pos + 1) < scalar @{$self -> {'lstfile'}} ) {
		if( $self -> {'lstfile'}[ $start_pos ] =~ /^0SIMULATION STEP OMITTED/ or
		    $self -> {'lstfile'}[ $start_pos ] =~ /^0ESTIMATION STEP OMITTED/ or
		    $self -> {'lstfile'}[ $start_pos ] =~ /^0SIGMA CONSTRAINED TO BE THIS INITIAL ESTIMATE/ ) {
		  $start_pos--;
		  last;
		}

		# After ten rows of sigmas NONMEM starts wrapping
		# lines. We then need to skip the first part of the
		# wrapped lines. This nice littel formula calculates
		# how many lines to skip.

		my $skip_lines = ($sm_row - $sm_row % 10)/10;

		$start_pos += $skip_lines if( $sm_row > 10 );

		my @init_row = split(' ', $self -> {'lstfile'}[ $start_pos++ ]);
		if( not $init_row[$#init_row] =~ /[0-9]?\.[0-9]{4}E[+-][0-9]{2}/ ) {
		  my $errmess = "Error parsing sigma initial estimates, found non-number\n".
		      $self -> {'lstfile'}[ $start_pos-1 ];
		  debug -> warn( level => 1,
				 message => $errmess."$!" );
		  $self -> parsing_error( message => $errmess."$!" );
		  return;
		}

		my $init = eval($init_row[$#init_row]);
		unless( $init == 0 ) {
		  push( @{$self -> {'fixedsigmas'}}, $all_sigmas_fixed ? 1 : 0 );
		  push( @{$self -> {'initsigmas'}}, eval($init) );
		}
		push( @{$self -> {'estimated_sigmas'}},  $all_sigmas_fixed ? 0 : 1 );
		push(@{$self -> {'lower_sigma_bounds'}},0);
		push(@{$self -> {'upper_sigma_bounds'}},1000000);
		$sm_row++;
	      }
	    }
	  }
# 	  if ( /^0MINIMIZATION/ ) {
# 	    last;
# 	  }
	}

	unless ( $success ) {
	  debug -> warn( level   => 2,
			 message => "rewinding to first position..." );
	} else {
# Keep this code
# 	  if ( $PsN::config -> {'_'} -> {'use_database'} and
# 	       $self -> {'register_in_database'} ) { 
# 	    my $dbh = DBI -> connect("DBI:mysql:host=".$PsN::config -> {'_'} -> {'database_server'}.
#			       ";databse=".$PsN::config -> {'_'} -> {'project'},
# 				     $PsN::config -> {'_'} -> {'user'},
#			        $PsN::config -> {'_'} -> {'password'},
# 				     {'RaiseError' => 1});
# 	    my $sth;
# 	    my @mod_str = ('','');
# 	    if ( defined $self -> {'model_id'} ) {
# 	      @mod_str = ('model_id,',"$self->{'model_id'},");
# 	    }
# 	    foreach my $param ( 'theta', 'omega', 'sigma' ) {
# 	      foreach my $par_str ( @{$self -> {'init'.$param.'s'}} ) {
# 		$sth = $dbh -> prepare("INSERT INTO ".$PsN::config -> {'_'} -> {'project'}.
#			       ".estimate ".
# 				       "(subproblem_id,problem_id,output_id,".
# 				       $mod_str[0].
# 				       "type,value,init) ".
# 				       "VALUES ( 1 ,".
# 				       "'$self->{'problem_id'}' ,".
# 				       "'$self->{'output_id'}' ,".
# 				       $mod_str[1].
# 				       "'$param','$par_str','1')");
# 		$sth -> execute;
# 		push( @{$self -> {'estimate_ids'}}, $sth->{'mysql_insertid'} );
# 	      }
# 	    }
# 	    $sth -> finish;
	    
# 	    $dbh -> disconnect;
# 	  }

	  $self -> {'lstfile_pos'} = $start_pos;
	}
      }
end _read_inits

# }}} _read_inits

# {{{ access_any

start access_any
      {
	unless( $#subproblems > 0 ){
	  debug -> warn( level => 2,
			   message => "subproblems undefined, using all." );
	  if( defined $self -> {'subproblems'} ) {
	    @subproblems = (1 .. scalar @{$self -> {'subproblems'}});
	  } else {
	    debug -> warn( level   => 1,
			   message => "No subproblems defined in this problem." );
	    @subproblems = ();
	  }
	}
	
	
	my @own_subproblems = defined $self -> {'subproblems'} ? @{$self -> {'subproblems'}} : ();
	foreach my $i ( @subproblems ) {
	  if ( defined $own_subproblems[$i-1] ) {
	    debug -> warn( level   => 2,
			   message => "subproblems: $i" );
	    debug -> warn( level   => 2,
			   message => "Attribute: ".$own_subproblems[$i-1] -> $attribute );
	    my $meth_ret = $own_subproblems[$i-1] -> $attribute;

	    # Test if the returned value is an array (with hashes we
	    # can't allow selection based on parameter numbers, since
	    # a hash is not ordered)
	    if ( ref ( $meth_ret ) eq 'ARRAY' ) {
	      #my @subprob_attr = @{$own_subproblems[$i-1] -> $attribute};
	      my @subprob_attr = @{$meth_ret};
	      if ( scalar @parameter_numbers > 0 ) {
		my @tmp_arr = ();
		foreach my $num ( @parameter_numbers ) {
		  if ( $num > 0 and $num <= scalar @subprob_attr ) {
		    push( @tmp_arr, $subprob_attr[$num-1] );
		  } else {
		    debug -> die( message => "( $attribute ): no such parameter number $num!".
				  "(".scalar @subprob_attr." exists)" );
		  }
		}
		@subprob_attr = @tmp_arr;
	      }
	      push( @return_value, \@subprob_attr );
	    } else {
#	      push( @return_value, $meth_ret ) if defined $meth_ret;
	      push( @return_value, $meth_ret );
	    }
	  } else {
	    debug -> die( message => "No such subproblem ".($i-1) );
	  }
	}
        # Check the return_value to see if we have empty arrays
	if ( $#return_value == 0  and ref ($return_value[0]) eq 'ARRAY' and scalar @{$return_value[0]} < 1 ) {
	   @return_value = ();
	}
      }
end access_any

# }}} access_any

# {{{ _set_labels

start _set_labels
      {
	if ( defined $self -> {'subproblems' } ) {
	  foreach my $type ( ('theta','omega','sigma') ) { 
	    my $first_sub = @{$self -> {'subproblems'}}[0];
	    my $accessor = $type eq 'theta' ? $type.'s' : 'raw_'.$type.'s';
	    if( defined $first_sub -> $accessor ) {
	      my @param = @{$first_sub -> $accessor};
	      if ( scalar @param > 0 ) {
		my ( @names, @indexes );
		my ($j,$ndiags);
		foreach $j (1..scalar @param) {
		  if ( $type eq 'theta' ) {
		    push( @names, "TH$j" );
		  } else {
		    if ( $first_sub -> _isdiagonal('index' => $j) ) {
		      push @names, uc(substr($type,0,2)).++$ndiags;
		      if ( $type eq 'omega' ) {
			push ( @{$self -> {'omega_indexes'}}, [$ndiags, $ndiags] );
		      } else {
			push ( @{$self -> {'sigma_indexes'}}, [$ndiags, $ndiags] );
		      }
		      next;
		    } elsif ($param[$j-1] !=0) {
		      @indexes = $first_sub -> _rowcolind( index => $j);
		      push @names,uc(substr($type,0,2)).$indexes[0].'_'.$indexes[1];
		      if ( $type eq 'omega' ) {
			push ( @{$self -> {'omega_indexes'}}, [$indexes[0], $indexes[1]] );
		      } else {
			push ( @{$self -> {'sigma_indexes'}}, [$indexes[0], $indexes[1]] );
		      }
		    }
		  }
		}
		$self ->{$type.'names'} = \@names;
	      }
	    }
	  }
	}
      }
end _set_labels

# }}} _set_labels
