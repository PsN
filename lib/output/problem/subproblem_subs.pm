# {{{ include

start include statements
    use Config;
    use ext::Math::MatrixReal;
    use Data::Dumper;
    use debug;
end include

# }}} include statements

# {{{ new

start new
      {
	if ( defined $this -> {'tablename'} ) {
	  $this -> add_data( 'init_data' => {'filename' => $this -> {'tablename'},
					     'idcolumn' => $this -> {'tableidcolumn'},
					     'debug' => $this -> {'debug'}} );
	}

	$this -> _read_simulation;

	if( $this -> estimation_step_run() ) {
	  $this -> _read_iteration_path;
	  $this -> _read_term()                 if ($this -> parsed_successfully() and not
						    $this -> finished_parsing());
	}

	if( $this -> estimation_step_initiated() ) {
	  $this -> _read_ofv()                  if ($this -> parsed_successfully() and not
						    $this -> finished_parsing());
	  $this -> _read_thomsi()               if ($this -> parsed_successfully() and not
						    $this -> finished_parsing());
	  if ($this -> covariance_step_run() ) {
	    $this -> _read_sethomsi()             if ($this -> parsed_successfully() and not
						      $this -> finished_parsing());
	    if ($this -> parsed_successfully()) {
	      $this -> _compute_cvsetheta();
	      $this -> _compute_cvseomega();
	      $this -> _compute_cvsesigma();
	    }
	  }
	  if ($this -> parsed_successfully() and defined $this -> thetas() ) {
	    $this -> _set_thetanames();
	  }
	  if ($this -> parsed_successfully() and defined $this -> omegas() ) {
	    $this -> _compute_comegas();
	    $this -> _set_omeganames();
	  }
	  if ($this -> parsed_successfully() and defined $this -> sigmas() ) {
	    $this -> _compute_csigmas();
	    $this -> _set_sigmanames();
	  }
	}

	if ($this -> covariance_step_run()) {
	  $this -> _read_covmatrix()          if ($this -> parsed_successfully() and not
						  $this -> finished_parsing());
	  $this -> _read_eigen()              if ($this -> parsed_successfully() and not
						  $this -> finished_parsing());
	}

	if( $this -> nonparametric_step_run() ) {
	  $this -> _read_npomegas()             if ($this -> parsed_successfully() and not
						    $this -> finished_parsing());
	}

	delete $this -> {'lstfile'};

      }
end new

# }}} new

# {{{ parsing_error
start parsing_error
    $self -> parsed_successfully( 0 );
$self -> parsing_error_message( $message );
end parsing_error
# }}} parsing_error

# {{{ register_in_database

start register_in_database
    if ( $PsN::config -> {'_'} -> {'use_database'} ) { 
      my $dbh = DBI -> connect("DBI:mysql:host=".$PsN::config -> {'_'} -> {'database_server'}.
			       ";databse=".$PsN::config -> {'_'} -> {'project'},
			       $PsN::config -> {'_'} -> {'user'},
			        $PsN::config -> {'_'} -> {'password'},
			       {'RaiseError' => 1});
      my $sth;
      my $term_str;
      if ( defined $self->{'minimization_message'} ) {
	$term_str = join("\n",@{$self->{'minimization_message'}});
      }
      my @mod_str = ('','');
      if ( defined $model_id ) {
	@mod_str = ('model_id,',"$model_id,");
      }
      $sth = $dbh -> prepare("INSERT INTO ".$PsN::config -> {'_'} -> {'project'}.
			       ".subproblem ".
			     "(problem_id,output_id,".
			     $mod_str[0].
			     "condition_number,".
			     "covariance_step_successful,".
			     "covariance_step_warnings,".
			     "eval,".
			     "final_zero_gradients,".
			     "hessian_reset,".
			     "ofv,".
			     "rounding_errors,".
			     "s_matrix_singular,".
			     "significant_digits,".
			     "simulation,".
			     "minimization_successful,".
			     "minimization_message,".
			     "zero_gradients ) ".
			     "VALUES ( '$problem_id' ,".
			     "'$output_id' ,".
			     $mod_str[1].
			     "'$self->{'condition_number'}' ,".
			     "'$self->{'covariance_step_successful'}' ,".
			     "'$self->{'covariance_step_warnings'}' ,".
			     "'$self->{'eval'}' ,".
			     "'$self->{'final_zero_gradients'}' ,".
			     "'$self->{'hessian_reset'}' ,".
			     "'$self->{'ofv'}' ,".
			     "'$self->{'rounding_errors'}' ,".
			     "'$self->{'s_matrix_singular'}' ,".
			     "'$self->{'significant_digits'}' ,".
			     "'$self->{'simulation'}' ,".
			     "'$self->{'minimization_successful'}' ,".
			     "'$term_str' ,".
			     "'$self->{'zero_gradients'}' )");
      $sth -> execute;
      $self -> {'subproblem_id'} = $sth->{'mysql_insertid'};
      $self -> {'problem_id'} = $problem_id;
      $self -> {'output_id'} = $output_id;
      $self -> {'model_id'} = $model_id;
      foreach my $param ( 'theta', 'omega', 'sigma' ) {
	my $i = 1;
	foreach my $par_str ( @{$self -> {$param.'s'}} ) {
	  $sth = $dbh -> prepare("INSERT INTO ".$PsN::config -> {'_'} -> {'project'}.
				 ".estimate ".
				 "(subproblem_id,problem_id,output_id,".
				 $mod_str[0].
				 "type,value, number, label) ".
				 "VALUES ( '$self->{'subproblem_id'}' ,".
				 "'$self->{'problem_id'}' ,".
				 "'$self->{'output_id'}' ,".
				 $mod_str[1].
				 "'$param','$par_str', '$i', 'test_label')");
	  $sth -> execute;
	  push( @{$self -> {'estimate_ids'}}, $sth->{'mysql_insertid'} );
	  $i++;
	}
      }
      $sth -> finish;
      $dbh -> disconnect;
    }

# if ( $PsN::config -> {'_'} -> {'use_database'} ) {
#   my $dbh = DBI -> connect("DBI:mysql:host=".$PsN::config -> {'_'} -> {'database_server'}.
# 			       ";databse=".$PsN::config -> {'_'} -> {'project'},
# 			   $PsN::config -> {'_'} -> {'user'},
# 			        $PsN::config -> {'_'} -> {'password'},
# 			   {'RaiseError' => 1});
#   my $sth = $dbh -> prepare( "UPDATE ".$PsN::config -> {'_'} -> {'project'}.
# 			       ".subproblem ".
# 			     "SET condition_number='".
# 			     $self -> {'condition_number'}."'".
# 			     "WHERE model_id='".$self -> {'model_id'}."'" );
#   $sth -> execute or debug -> die( message => $sth->errstr ) ;
#   $sth -> finish;
#   $dbh -> disconnect;
# }
end register_in_database

# }}} register_in_database

# {{{ _compute_comegas_or_csigmas

start _compute_comegas_or_csigmas
{
  # This method transforms omegas or sigmas.

  # First we check if we have omegas or sigmas.
  if ( defined $self -> {'raw_' . $omega_or_sigma . 's'} ) {
    my @raw_omegas_or_sigmas = @{$self -> {'raw_' . $omega_or_sigma . 's'}};
    # If the omega or sigma matrix has no off-diagonal elements, the
    # transformation is quite straightforward.
    if( $self -> { $omega_or_sigma . '_block_structure_type'} eq 'DIAGONAL' ){

      # All diagonal omegas or sigmas will be in
      # @raw_omegas_or_sigmas, so we loop over it and take the square
      # root of non-negative, non-zero numbers (those become
      # undefined).
      my $row_size = 1;
      for( my $i = 0; $i <= $#raw_omegas_or_sigmas; $i+=$row_size ){
	my $omega_or_sigma_value = undef;
	if( $raw_omegas_or_sigmas[$i] >= 0 ) {
	  $omega_or_sigma_value = sqrt( $raw_omegas_or_sigmas[$i] );
	} else {
	  ui -> print( category => 'all',
		       message  => "Warning: cannot take the square root of ".
		       "$omega_or_sigma with value ".$raw_omegas_or_sigmas[$i] );
	}
	push( @{$self -> {'c' . $omega_or_sigma .'s'}}, $omega_or_sigma_value );
	$row_size ++;
      }
      
    } elsif( $self -> {$omega_or_sigma . '_block_structure_type'} eq 'BLOCK' ) {

      # If the omega or sigma matrix has block form, the transformation
      # becomes a bit more complex. The transformation for diagonal
      # elements is the same. But for off-diagonals it takes more into
      # account.

      # $current_row_size is the number of omegas or sigmas in the row
      # in the matrix that we are currently transforming.
      my $current_row_size = 1;

      # $diagonal_index is the index in @raw_omegas_or_sigmas where the current
      # on-diagonal omega or sigma we are transforming.
      my $diagonal_index = 0;

      # $y is the y-coordinate of the element in the matrix we
      # are transforming.
      my $y = 0;

      # First get all block sets sorted.
      my @keys = sort {$a <=> $b} keys( %{$self -> { $omega_or_sigma . '_block_sets'}} );

      foreach my $key ( @keys ) {

	# If this is a block set $block_dimension will be > 1
	# otherwise it will be 1.
	my $block_dimension = $self -> { $omega_or_sigma . '_block_sets'}{$key}{'dimension'};
	my $block_size = $self -> { $omega_or_sigma . '_block_sets'}{$key}{'size'};

#	print "BS: $block_size\n";
#	print "BD: $block_dimension\n";

	# loop over the repeated blocks
	for( my $i = 1; $i <= $block_size; $i++ ) {
	  # loop over the size of the block (i.e. the X in block(X))
	  for( my $j = 1; $j <= $block_dimension; $j++ ) {
	    $y++;

	    # The equation y*(y+1)/2 is a geometric sum that gives the
	    # index of the diagonal element on row y in the compacted
	    # matrixarray @raw_omegas_or_sigmas. -1 to get array
	    # index.
	    my $y_y_idx = ($y*($y+1)/2)-1;
	    
	    my $start_x = ($y - $j + 1);
	    for( my $x = $start_x; $x <= $y; $x++ ) {
#	      print "X: $x, Y: $y\n";
	      my $omega_or_sigma_value;
	      if( $i == 1 ) {
		# This is the first of the repeated blocks, collect
		# the values (otherwise, just leave
		# omega_or_sigma_value undefined)
		if( $x == $y ) {
		  
		  # If we are on the diagonal, just take the square root
		  # of the diagonal element.
		  $omega_or_sigma_value = $raw_omegas_or_sigmas[ $y_y_idx ];
		  $omega_or_sigma_value = sqrt( $omega_or_sigma_value );
#		  print "y_y_idx: $y_y_idx\n";
		  
		} else {
		  # If we are off the diagonal, we need to find two
		  # on-diagonal omegas, one on the same column and one on
		  # the same row.
		  
		  my $x_x_idx = ($x*($x+1)/2)-1; # See explanation for $y_y_idx above
		  
		  # Off diagonal index: $y_y_idx for the row before plus $x
		  my $x_y_idx = (($y-1)*$y/2+$x)-1;
		  
		  $omega_or_sigma_value = $raw_omegas_or_sigmas[$x_y_idx];
		  my $denominator = $raw_omegas_or_sigmas[ $x_x_idx ] 
		      * $raw_omegas_or_sigmas[ $y_y_idx ];
		  if( $denominator <= 0 ){ # To avoiding division by zero
		    $omega_or_sigma_value = undef;
		  } elsif( $omega_or_sigma_value >= sqrt($denominator) ) { 
		    # This rounding handles cases when the offdiagonals
		    # are greater or equal to one.
		    $omega_or_sigma_value = $omega_or_sigma_value/( int( 10000 * sqrt($denominator) )/10000 );
		  } else {
		    $omega_or_sigma_value = $omega_or_sigma_value/sqrt($denominator);
		  }
		}
	      }
	      push( @{$self -> {'c' . $omega_or_sigma . 's'}}, $omega_or_sigma_value );
	      
	    }
	  }
	}
      }
# 	my $loop_limit = $block_dimension > $block_size ? $block_dimension : $block_size;
# 	print "loop limit: $loop_limit\n";
# 	for( my $i = 0; $i < $loop_limit; $i ++ ){
# 	  # x_idx is the x-coordinate of the omega or sigma we are
# 	  # transforming.

# 	  my $x_idx = $y_idx - $i;
# 	  # current row size is equal to the y coordinate
# #	  my $x_idx = $current_row_size - $i;
	  
# 	  # The innner_loop_limit must be 1 when we have SAME blocks,
# 	  # which can be determined from the block_size variable.
# 	  my $inner_loop_limit = $block_size > 1 ? 0 : $i;
# 	  print "inner loop limit: $inner_loop_limit\n";
# 	  # The inner loop is over the row elements in the block
# 	  # set. (1 when we have SAME blocks)
# 	  for( my $j = 0; $j <= $inner_loop_limit; $j++ ){

# 	    # $omega_or_sigma will hold the transformed value.
# 	    my $omega_or_sigma_value;

# 	    print "X: $x_idx, Y: $y_idx\n";
# 	    if( $x_idx == $y_idx ) {

# 	      # If we are on the diagonal, just take the square root
# 	      # of the diagonal element.
# 	      $omega_or_sigma_value = $raw_omegas_or_sigmas[ $diagonal_index ];
# 	      $omega_or_sigma_value = sqrt( $omega_or_sigma_value );

# 	    } else {
# 	      # If we are off the diagonal, we need to find two
# 	      # on-diagonal omegas, one on the same column and one on
# 	      # the same row.

# 	      # The equation y*(y+1)/2 is a geometric sum that gives
# 	      # the index of the diagonal element on row y in the
# 	      # compacted matrixarray @raw_omegas_or_sigmas. The equation
# 	      # x*(x+1)/2 gives the diagonal element on column x in
# 	      # the same array.

# 	      # We find the omega by finding the index of the diagonal
# 	      # element on row y and subtracting the difference
# 	      # between the number of elements on the row ($i) and the
# 	      # number of elements we have transformed ($j).

# 	      $omega_or_sigma_value = $raw_omegas_or_sigmas[(($y_idx*($y_idx+1))/2)-1 - ($i - $j)];
# 	      my $denominator = $raw_omegas_or_sigmas[ (($x_idx*($x_idx+1))/2)-1 ] 
# 		                 * 
# 				$raw_omegas_or_sigmas[ (($y_idx*($y_idx+1))/2)-1 ];
# 	      if( $denominator <= 0 ){ # To avoiding division by zero
# 		$omega_or_sigma_value = undef;
# 	      } elsif( $omega_or_sigma_value >= sqrt($denominator) ) { 
# 		# This rounding handles cases when the offdiagonals
# 		# are greater or equal to one.
# 		$omega_or_sigma_value = $omega_or_sigma_value/( int( 10000 * sqrt($denominator) )/10000 )
# 	      } else {
# 		$omega_or_sigma_value = $omega_or_sigma_value/sqrt($denominator);
# 	      }
# 	    }
# 	    push( @{$self -> {'c' . $omega_or_sigma . 's'}}, $omega_or_sigma_value );

# 	    # Move the x-coordinate forwards.
# 	    $x_idx ++;
# 	  }
	  
# 	  # Move the y-coordinate forwards.
# 	  $y_idx ++;
# 	  # Calculate the new row size.
# #	  $current_row_size ++;
# 	  # Skip one row and we get the index of the next diagonal.

# 	  $diagonal_index += $y_idx;
# #	  $diagonal_index += $current_row_size;
# 	}
#     }
    } else {
      'debug' -> warn( level => 1,
		       message => $omega_or_sigma . ' matrix has unknown form.' );
    }
  }
}
end _compute_comegas_or_csigmas

# }}} _compute_comegas_or_sigmas

# {{{ _compute_comegas
start _compute_comegas
{
  $self -> _compute_comegas_or_csigmas( omega_or_sigma => 'omega' );
}
end _compute_comegas
  
# This is the old algoritm for transforming omegas. It can't handle
# of-diagonals omegas that are zero, if that should ever happen.

#     my $i;
#     my @raw_omegas;
#     my @computed_omegas;
#     my @diags; 
#     my @indices;
    
#     @raw_omegas = @{$self -> {'raw_omegas'}};
#     @computed_omegas = ();

#     if( $self -> {'omega_structure_type'} eq 'DIAGONAL' ){
#       @diags = @raw_omegas;
#     } else {
#       ## Collect the diagonals
#       foreach $i (0..$#raw_omegas) {
# 	if( $self -> _isdiagonal('index' => $i+1) ){
# 	  push( @diags, $raw_omegas[$i] );
# 	}
#       }
#     }

#     foreach $i (0..$#raw_omegas){
#       if($self -> _isdiagonal('index' => $i+1)) {
# 	if( $raw_omegas[$i] <= 0 ){
# 	  push( @computed_omegas, $raw_omegas[$i] );
# 	  next;
# 	}
# 	push @computed_omegas, sqrt($raw_omegas[$i]);
# 	next;
#       } else {
# 	@indices = $self -> _rowcolind( index => $i+1 );
# 	if( ($raw_omegas[$i] == 0) or ($diags[$indices[0]-1]*$diags[$indices[1]-1]) <= 0 ) {
# 	  next;
# 	} else {
# 	  push @computed_omegas,
# 	  $raw_omegas[$i]/sqrt($diags[$indices[0]-1]*$diags[$indices[1]-1]);
# 	}
#       }
#     }
#   $self -> {'comegas'}= \@computed_omegas;}

# }}}

# {{{ _compute_csigmas
start _compute_csigmas
{
  $self -> _compute_comegas_or_csigmas( omega_or_sigma => 'sigma' );
}
end _compute_csigmas  
# This is the old algoritm for transforming sigmass. It can't handle
# of-diagonals sigmass that are zero, if that should ever happen.

# {
#     if (defined $self -> {'raw_sigmas'} ) {
# 	my $i;
# 	my ( @om, @com, @diags, @indices );
# 	@om = @{$self -> {'raw_sigmas'}};
# 	@com = ();
# 	## Collect the diagonals
# 	foreach $i (0..$#om) {
# 	    push @diags,$om[$i] if $self -> _isdiagonal('index' => $i+1);
# 	}
# 	foreach $i (0..$#om){
# 	    if($self -> _isdiagonal('index' => $i+1)) {
# 	      if( $om[$i] <= 0 ){
# 		next;
# 	      }
# 	      push @com, sqrt($om[$i]);
# 	      next;
# 	    } else {
# 	      @indices = $self -> _rowcolind( index => $i+1 );
# 	      if( ($om[$i] == 0) or ($diags[$indices[0]-1]*$diags[$indices[1]-1] <= 0) ) {
# 		next;
# 	      } else {
# 		push @com,
# 		$om[$i]/sqrt($diags[$indices[0]-1]*$diags[$indices[1]-1]);
# 	      }
# 	    }
# 	}
# 	$self -> {'csigmas'}= \@com;
#     }
# }

# }}} _compute_csigmas

# {{{ _compute_cvseomega

start _compute_cvseomega 
{
  if( defined $self -> raw_omegas() and defined $self -> raw_seomegas() ) {
    my @raw_omegas   = @{$self -> raw_omegas()};
    my @raw_seomegas = @{$self -> raw_seomegas()};
    my @cvseomega;

    if ( scalar @raw_seomegas > 0) {
      my ($j,$i);
      my ( @init_om, @init_seom );
      
      foreach $i (0.. $#raw_seomegas){
	if( $self -> _isdiagonal('index' => $i+1)) {
	  push @init_seom, $raw_seomegas[$i];
	  next;
	} else {
	  if(($raw_seomegas[$i] eq 'NA') or ($raw_seomegas[$i] == 0)) {
	    next;
	  } else {
	    push @init_seom, $raw_seomegas[$i];
	  }
	}
      }
	
      foreach $i (0..$#raw_omegas){
	if( $self -> _isdiagonal('index' => $i+1)) {
	  push @init_om, $raw_omegas[$i];
	  next;
	    } else {
	      if($raw_omegas[$i] == 0) {
		next;
	      } else {
		push @init_om, $raw_omegas[$i];
	      }
	    }
      }
	
      foreach my $i (0..$#init_om) {
	if( $init_seom[$i] ne 'NA' and $init_om[$i] ne 'NA' ){
	  if( ($init_seom[$i] == 'INF' or $init_seom[$i] == '-INF') and
	      ($init_om[$i] == 'INF' or $init_om[$i] == '-INF') ) {
	    push @cvseomega,undef;
	  } elsif ( $init_seom[$i] == 'INF' ) {
	    push @cvseomega,'INF';
	  } elsif ( $init_seom[$i] == '-INF' ) {
	    push @cvseomega,'-INF';
	  } elsif ( $init_om[$i] == 'INF' or
		    $init_om[$i] == '-INF' ) {
	    push @cvseomega,0;
	  } else {
	    push @cvseomega,$init_seom[$i]/abs($init_om[$i]);
	  }
	} else {
	  push @cvseomega,undef;
	}
#	    push @cvseomega,$init_seom[$i]/$init_om[$i]
#		if ($init_seom[$i] && $init_om[$i]);
#	    push @cvseomega,"NA" unless ($init_seom[$i] && $init_om[$i]);
      }

    }

    $self -> cvseomegas([@cvseomega]);
  }
}
end _compute_cvseomega

# }}} _compute_cvseomega

# {{{ _compute_cvsesigma
start _compute_cvsesigma
{
  if( defined $self -> raw_sigmas() and defined $self -> raw_sesigmas() ) {
    my @raw_sigmas   = @{$self -> raw_sigmas()};
    my @raw_sesigmas = @{$self -> raw_sesigmas()};
    my @cvsesigma;

    if ( scalar @raw_sesigmas > 0) {
      my ($j,$i);
      my ( @init_si, @init_sesi );
      
      foreach $i (0.. $#raw_sesigmas){
	if($self -> _isdiagonal('index' => $i+1)) {
	  push @init_sesi, $raw_sesigmas[$i];
	  next;
	} else {
	  if(($raw_sesigmas[$i] eq 'NA') or ($raw_sesigmas[$i] == 0)) {
	    next;
	  } else {
	    push @init_sesi, $raw_sesigmas[$i];
	  }
	}
      }
	
      foreach $i (0.. $#raw_sigmas){
	if($self -> _isdiagonal('index' => $i+1)) {
	  push @init_si, $raw_sigmas[$i];
	  next;
	} else {
	  if($raw_sigmas[$i] == 0) {
	    next;
	  } else {
	    push @init_si, $raw_sigmas[$i];
	  }
	}
      }
      
      foreach my $i (0..$#init_si) {
	if( $init_sesi[$i] ne 'NA' and $init_si[$i] ne 'NA' ){
	  if( ($init_sesi[$i] == 'INF' or $init_sesi[$i] == '-INF') and
	      ($init_si[$i] == 'INF' or $init_si[$i] == '-INF') ) {
	    push @cvsesigma,undef;
	  } elsif ( $init_sesi[$i] == 'INF' ) {
	    push @cvsesigma,'INF';
	  } elsif ( $init_sesi[$i] == '-INF' ) {
	    push @cvsesigma,'-INF';
	  } elsif ( $init_si[$i] == 'INF' ) {
	    push @cvsesigma,0;
	  } else {
	    push @cvsesigma,$init_sesi[$i]/abs($init_si[$i]);
	  }
	} else {
	  push @cvsesigma,undef;
	}
      }
      
    }

    $self -> cvsesigmas([@cvsesigma]);
  }
}
end _compute_cvsesigma
# }}} _compute_cvsesigma

# {{{ _compute_cvsetheta
start _compute_cvsetheta
{
  if( defined $self -> thetas() and defined $self -> sethetas() ) {
    my @thetas   = @{$self -> thetas()};
    my @sethetas = @{$self -> sethetas()};
    my @cvsethetas;
    
    if ( scalar @sethetas > 0 ) {
      foreach my $i (0..$#thetas) {
	if( defined $sethetas[$i] and
	    defined $thetas[$i] ) {
	  if( $thetas[$i] != 0 ) {
	    push(@cvsethetas,$sethetas[$i]/abs($thetas[$i]));
	  } elsif( $sethetas[$i] > 0 ) {
	    push(@cvsethetas,'INF');
	  } else {
	    push(@cvsethetas,'-INF');
	  }
	} else {
	  push @cvsethetas,"NA";
	}
      }
    }
    
    $self -> cvsethetas([@cvsethetas]);
  }
}
end _compute_cvsetheta
# }}} _compute_cvsetheta

# {{{ _isdiagonal
start _isdiagonal 
{
    my $previ = 1;
    my $j;
    return(1) if $index == 1;
    foreach my $j (2..100) {
	return(1) if $index == $previ+$j;
	$previ = $previ+$j;
	last if $index < $previ;
    }
}
end _isdiagonal
# }}} _isdiagonal

# {{{ _read_matrixoestimates
start _read_matrixoestimates
      {
	# Reads one matrix structure and returns the file handle at
	# the beginning of the next structure
	while ( $_ = @{$self -> {'lstfile'}}[ $pos++ ] ) {
	  last if (/^\s*\*/);
	  # Rewind one step if we find something that marks the end of
	  # our structure
	  $pos-- and last if ( /^ PROBLEM.*SUBPROBLEM/ or /^ PROBLEM NO\.:\s+\d/ );
	  $pos-- and last if (/^[a-df-zA-DF-Z]/);

	  next if ( /^\s+TH/ or /^\s+OM/ or /^\s+SG/ );	  # Header row
	  next if ( /^1/ );			  # Those annoying 1's

	  chomp;				# Get rid of line-feed
	  my @row = split;
	  shift( @row ) if ( $row[0] eq '+' );	   # Get rid of +-sign

	  next if ( $#row < 0 );			   # Blank row
	  
	  push( @subprob_matrix, @row );
	}
        $success = 1 if( scalar @subprob_matrix > 0 );
      }
end _read_matrixoestimates
# }}} _read_matrixoestimates

# {{{ _read_covmatrix
start _read_covmatrix
      {
	my $matrix_nr = 0;
	my ( $t_success, $c_success, $corr_success, $i_success ) = (0,0,0,0);
	my $start_pos = $self -> {'lstfile_pos'}-1;
	
	# {{{ sub clear dots

	sub clear_dots {
	  my $m_ref = shift;
	  my @matrix = @{$m_ref};
	  # get rid of '........'
	  my @clear;
          foreach ( @matrix ) {
            unless ( $_ eq '.........' ) {
              if ( $_ eq 'NAN' ) {
                push( @clear, 0 );
              } elsif ( $_ eq 'INF' ) {
                push( @clear, 99999999999999 );
              } elsif ( $_ eq '-INF' ) {
                push( @clear, -99999999999999 );
              } else {
                push( @clear, $_ );
              }
            }
          }
	  return \@clear;
	}

	# }}}
	
	# {{{ sub make square
	
	sub make_square {
	  my $m_ref = shift;
	  my @matrix = @{$m_ref};
	  # Make the matrix square:
	  my $elements = scalar @matrix; # = M*(M+1)/2
	  my $M = -0.5 + sqrt( 0.25 + 2 * $elements );
	  my @square;
	  for ( my $m = 1; $m <= $M; $m++ ) {
	    for ( my $n = 1; $n <= $m; $n++ ) {
	      push( @{$square[$m-1]}, $matrix[($m-1)*$m/2 + $n - 1] );
	      unless ( $m == $n ) {
		push( @{$square[$n-1]}, $matrix[($m-1)*$m/2 + $n - 1] );
	      }
	    }
	  }
	  return \@square;
	}
	
	# }}}

	while ( $_ = @{$self -> {'lstfile'}}[ $start_pos++ ] ) {
	  if (/T MATRIX/) {
	    while ( $_ = @{$self -> {'lstfile'}}[ $start_pos++ ] ) {
	      if (/^ TH (\d)/ or /^\s+TH (\d) \| /) { # Read matrix and get out of inner while loop
		( $start_pos, $self -> {'raw_tmatrix'}, $t_success )  = $self ->
		    _read_matrixoestimates( pos => $start_pos ) and last;
	      }
	    }
	    last;		 # No covariance matrix will be found!
	  }
	  if (/    COVARIANCE MATRIX OF ESTIMATE/) {
	    while( $_ = @{$self -> {'lstfile'}}[ $start_pos++ ] ) {
	      if (/^ TH (\d)/ or /^\s+TH (\d) \| /) { # Read matrix and get out of inner while loop
		( $start_pos, $self -> {'raw_covmatrix'}, $c_success ) = $self ->
		    _read_matrixoestimates( pos => $start_pos ) and last;
	      }
	    }
	  }
	  if (/    CORRELATION MATRIX OF ESTIMATE/) {
	    while( $_ = @{$self -> {'lstfile'}}[ $start_pos++ ] ) {
	      if (/^ TH (\d)/ or /^\s+TH (\d) \| /) { # Read matrix and get out of inner while loop
		( $start_pos, $self -> {'raw_cormatrix'}, $corr_success ) = $self ->
		    _read_matrixoestimates( pos => $start_pos ) and last;
	      }
	    }
	  }
	  if (/    INVERSE COVARIANCE MATRIX OF ESTIMATE/) {
	    while( $_ = @{$self -> {'lstfile'}}[ $start_pos++ ] ) {
	      if (/^ TH (\d)/ or /^\s+TH (\d) \| /) { # Read matrix and get out of inner while loop
		( $start_pos, $self -> {'raw_invcovmatrix'}, $i_success ) = $self ->
		    _read_matrixoestimates( pos => $start_pos ) and last;
	      }
	    }
	    last;					# Last matrix?
	  }
	}
	foreach my $element ( @{$self -> {'raw_tmatrix'}} ) {
	  push( @{$self -> {'t_matrix'}}, eval($element) ) unless ( $element eq '.........' );
	}
	foreach my $element ( @{$self -> {'raw_covmatrix'}} ) {
	  push( @{$self -> {'covariance_matrix'}}, eval($element) ) unless ( $element eq '.........' );
	}
	foreach my $element ( @{$self -> {'raw_cormatrix'}} ) {
	  push( @{$self -> {'correlation_matrix'}}, eval($element) ) unless ( $element eq '.........' );
	}

	if( defined $self -> {'raw_invcovmatrix'} ) {
	    $self -> {'inverse_covariance_matrix'} = Math::MatrixReal ->
		new_from_cols( make_square( clear_dots( $self -> {'raw_invcovmatrix'} ) ) );
	}

#	foreach my $element ( @{$self -> {'raw_invcovmatrix'}} ) {
#	  push( @{$self -> {'inverse_covariance_matrix'}}, eval($element) ) unless ( $element eq '.........' );
#	}

	#If something has gone right!
	$self-> {'lstfile_pos'} = $start_pos if ( $t_success + $c_success + $corr_success + $i_success  );
      }
end _read_covmatrix
# }}} _read_covmatrix

# {{{ _read_eigen

start _read_eigen
      {
	my @eigens;
	my $start_pos = $self -> {'lstfile_pos'};
	my $eig_area  = 0;
	while ( $_ = @{$self -> {'lstfile'}}[ $start_pos++ ] ) {
	  chomp;
	  if ( /EIGENVALUES OF COR MATRIX OF ESTIMATE/ ) {
	    $eig_area = 1;
	    $start_pos = $start_pos + 4 ; # Jump forward to the index numbers
	    debug -> warn( level   => 2,
			   message => "Found the eigenvalue area" );
	  INNER: while ( $_ = @{$self -> {'lstfile'}}[ $start_pos++ ] ) { # Get rid of indexes
	      last INNER if ( not /^\s+\d/ );
#	      $start_pos++ and last INNER if ( not /^\s+\d/ );
	    }
	  }
	  if ( $eig_area ) {
	    $start_pos-- and last if (/^[a-df-zA-DF-Z]/); #Rewind one step
	    last if ( /^\s*\*/ or /^1/ );
	    push( @eigens, split );
	  }
	  $start_pos-- and last if ( /^ PROBLEM.*SUBPROBLEM/ or /^ PROBLEM NO\.:\s+\d/ );
	  $start_pos-- and last if (/^[a-df-zA-DF-Z]/); #Rewind one step
	}
	if ( scalar @eigens > 0 ) {
	  my @list = sort { $a <=> $b } @eigens;
	  $self -> {'condition_number'} = abs($list[$#list]/$list[0]) if ( $list[0] != 0 );
	}
	@{$self -> {'eigens'}} = @eigens;
      }
end _read_eigen

# }}} _read_eigen

# {{{ _read_eval
start _read_eval
{
    my $start_pos = $self -> {'lstfile_pos'};

    while( $_ = @{$self -> {'lstfile'}}[ $start_pos++ ] ) {
      if ( / PROBLEM NO\.:\s*\d+\n/ ) {
	$self -> parsing_error( message => "Error in reading the number of ".
				"function evaluations!\nNext problem found" );
	return;
      }
      
      if ( $start_pos >= scalar @{$self -> {'lstfile'}} ) {
	$self -> parsing_error( message => "Error in reading number of ".
				"function evaluations!\nEOF found\n" );
	return;
      }

      if ( /^1/ or /MINIMUM VALUE OF OBJECTIVE FUNCTION/ ) {
	$self -> parsing_error( message => "Error in reading number of ".
				"function evaluations!\nOFV found" );
	return;
      }
      
      if ( / NO. OF FUNCTION EVALUATIONS USED:\s*(\d*)/ ){
	$self -> feval($1);
	last;
      }
    }
}
end _read_eval
# }}} _read_eval

# {{{ _read_iteration_path
start _read_iteration_path
      {
	my $start_pos = $self -> {'lstfile_pos'};
	my $success = 0;
	my (@func_eval, @parameter_vectors,
	    @gradient_vectors) = ((), (), (), (), ());
	my @numsigdig_vectors;
	my $cumulative_evals = 0;
	my $zero_gradients = 0;
	my $hessian_reset = 0;
	my $found_monitoring = 0;
	while ( $_ = @{$self -> {'lstfile'}}[ $start_pos++ ] ) { #Large reading loop
	  if ( /MONITORING OF SEARCH/ ) {
	    $found_monitoring = 1;
	  }

	  if ( $found_monitoring and
	       $start_pos >= scalar @{$self -> {'lstfile'}} ) {
	    # This is probably (we cannot be really sure but it is
	    # likely) a "NONMEM black hole"-situation. We stop parsing
	    # but do not indicate that we had problems parsing the
	    # file.
	    $self -> parsing_error_message("Found \" MONITORING OF SEARCH:\" but no".
					   " records of the iterations before the end".
					   " of the output file. This is a sign of a".
					   " \"NONMEM black hole\"-situation. We cannot ".
					   "be 100% sure of this based on this type of".
					   " output so please take a good look at the files\n");
	    $self -> parsed_successfully(1);
	    $self -> finished_parsing(1);
	    return;
	  }
	    
	  if ( /0PROGRAM TERMINATED BY OBJ/ ) {
	    # This is an error message which terminates NONMEM. We
	    # return after reading the minimization message
	    $self -> minimization_successful(0);
	    $self -> _read_minimization_message();
	    $self -> finished_parsing(1);
	    return;
	  }	    

	  if (/^0ITERATION NO/) {
	    unless (/0ITERATION NO\.:\s+(\d+)\s+OBJECTIVE VALUE:\s+(\S+)\s+NO\. OF FUNC\. EVALS\.:\s*(.+)/) {
	      $self -> parsing_error( message => "Error in reading iteration path!\n$!" );
	      return;
	    }
	    $success = 1;
	    push(@{$self -> {'iternum'}}, $1);

	    my $ofvpath = $2;
	    unless( $ofvpath eq '**' ){ # If funcion evals are more than 10000, NONMEM will print out two stars.
		push(@{$self -> {'ofvpath'}}, $ofvpath );
	    } # If, in fact, we find stars, the number of evaluations are calculated below

	    my (@parameter_vector, @gradient_vector) = ((), ());
	    my @numsigdig_vector;
	    
	    while ( $_ = @{$self -> {'lstfile'}}[ $start_pos ] ) {
	      if (/^ CUMULATIVE NO\. OF FUNC\. EVALS\.:\s*(\d+)/) {
		my $eval_path = $1; 
		
		if( $ofvpath eq '**' ){
		    my $ofvpath = $eval_path - $cumulative_evals;
		    $cumulative_evals = $eval_path;
		    debug -> warn( level   => 2,
				   message => "Calculated eval_path = $ofvpath" );
		    push(@{$self -> {'ofvpath'}}, $ofvpath );
		}

		push(@{$self -> {'funcevalpath'}}, $eval_path);

		if (/RESET HESSIAN, TYPE (\w+)/) {
		  $hessian_reset++;
		}

		$start_pos++;
	      } elsif ( s/^ PARAMETER:\s*// ) {
		do {
		  push(@parameter_vector, split);
		  $_ = @{$self -> {'lstfile'}}[ ++$start_pos ];
		  if( $start_pos >= scalar @{$self -> {'lstfile'}} ) {
		    $self -> parsing_error( message => "Error in reading iteration path!\nEOF found\n" );
		    return;
		  }
		} while ( not /^ GRADIENT:\s*/ );
	      } elsif (s/^ GRADIENT:\s*//) {
		do {
		  push(@gradient_vector, split);
		  $_ = @{$self -> {'lstfile'}}[ ++$start_pos ];
		  if( $start_pos >= scalar @{$self -> {'lstfile'}} ) {
		    $self -> parsing_error( message => "Error in reading iteration path!\nEOF found\n" );
		    return;
		  }
		} while ( not /[A-D][F-X]/ );
	      } elsif (s/^ NUMSIGDIG:\s*//) {
	        do {
		  push(@numsigdig_vector, split);
		  $_ = @{$self -> {'lstfile'}}[ ++$start_pos ];
		  if( $start_pos >= scalar @{$self -> {'lstfile'}} ) {
		    $self -> parsing_error( message => "Error in reading iteration path!\nEOF found\n" );
		    return;
		  }
		} while ( not /[A-D][F-X]/ );
	      } else {
		last;
	      }
	    }
	    foreach my $grad ( @gradient_vector ) {
	      $zero_gradients++ if $grad == 0;
	    }
	    $self -> {'initgrad'} = \@gradient_vector unless($self -> {'initgrad'});
	    $self -> {'final_gradients'}  = \@gradient_vector;
	    $self -> {'finalparam'} = \@parameter_vector;
	    push(@parameter_vectors, \@parameter_vector);
	    push(@gradient_vectors, \@gradient_vector);
	    $self -> {'parameter_significant_digits'} = \@numsigdig_vector if scalar @numsigdig_vector;

	    if( /0HESSIAN OF POSTERIOR DENSITY IS NON-POSITIVE-DEFINITE DURING SEARCH/ ) {
	      # This is an errror that stops the execution
	      $self -> finished_parsing(1);
	      last;
	    }
	    last unless(/^0ITERATION NO/);
	  }			#End of if iteration no
	}			#End of large reading loop
    
	unless( $self -> finished_parsing() ) {
	  my ($kill_found, $file_end, $kill_message, $search_pos) = (0, 0, "", $start_pos);
	  while ( $_ = @{$self -> {'lstfile'}}[ $search_pos++ ] ) { #Have a look, a few lines down...
	    if( /kill/i ) {
	      $kill_found = 1;
	      $kill_message = $_;
	      last;
	    }
#	  $kill_found = 1 and $kill_message = $_ and last if(/kill/i);
	    if( $search_pos + 1 == scalar @{$self -> {'lstfile'}} ) {
	      $file_end = 1;
	      $search_pos = $start_pos + 4;
	    }
	    last if( $search_pos > $start_pos + 3 )
#	  last if (--$kill_found < -2);
	  }
	  if (($kill_found == 1) or $file_end) { #Crash before last iteration
	    my $errstr = $kill_found ? "NONMEM killed" : "EOF found\n";
	    $self -> parsing_error( message => "Error in reading iteration path!\n$errstr" );
	    return;

# 	  $self -> {'minimization_message'} = ["PsN message:","The output file seems to have an abrupt ending," .
# 					      " before the last","iteration has finished."];
# 	  if ($kill_found == 1) {
# 	    push(@{$self -> {'minimization_message'}}, "String found in output file: $kill_message" );
# 	  } else {
# 	    push(@{$self -> {'minimization_message'}}, " This is probably due to a crash");
# 	  }
	  }
	}
    
	unless ( $success ){
	  debug -> warn( level   => 2,
			 message => "rewinding to first position..." );
	} else {
	  $self -> {'lstfile_pos'} = $start_pos;
	  $self -> {'parameter_path'}    = \@parameter_vectors;
	  $self -> {'gradient_path'} = \@gradient_vectors;
	  $self -> {'zero_gradients'} = $zero_gradients;
	  my $final_zero_gradients = 0;
	  foreach my $grad ( @{$self -> {'final_gradients'}} ) {
	    $final_zero_gradients++ if $grad == 0;
	  }
	  $self -> {'final_zero_gradients'} = $final_zero_gradients;
	  $self -> {'hessian_reset'} = $hessian_reset;
	}
      }
end _read_iteration_path
# }}} _read_iteration_path

# {{{ _read_npomegas

start _read_npomegas
{
    my $start_pos = $self -> {'lstfile_pos'};
    my $success   = 0;
    my $npofvarea   = 0;
    my $nparea	= 0;
    my $npetabararea	= 0;
    my $npomegarea	= 0;
    my ( @npetabar, @npomega, @T, $i );
    
    while( $_ = @{$self -> {'lstfile'}}[ $start_pos++ ] ) {
	$nparea      = 1   if /NONPARAMETRIC ESTIMATE/;
	last if ( /THERE ARE ERROR MESSAGES IN FILE PRDERR/ and $nparea );
	last if ( /^1/ and $nparea );
	last if ( /^1NONLINEAR/ and $nparea );
	last if ( /^[A-W]/ and $nparea );
	
	if (/MINIMUM VALUE OF OBJECTIVE FUNCTION/ and $nparea ){ #Only nonmem6 version
	    $npofvarea      = 1;
	}
	if ( /EXPECTED VALUE OF ETA/ and $nparea ) {
	    $npetabararea = 1;
	    $npofvarea = 0;
	    $success   = 1;
	}
	if ( /COVARIANCE MATRIX OF ETA/ and $nparea ) {
	    $npomegarea = 1;
	    $npetabararea = 0;
	}
	if($npofvarea and /^\s+(-?\d*\.\d*)/) { #Assignment of attribute at the spot
	    $self -> {'npofv'} = $1;
	    $npofvarea = 0;
	}
	if($npetabararea and /^\s*-?\d*\.\d*/) {
	    @T = split(' ',$_);
	    for $i (0..(@T-1)) {$T[$i] = eval($T[$i]);}
	    push(@npetabar,@T);
	}
	if($npomegarea and /^(\+|\s{2,})/) {
	    next if /ET/;
	    @T = split(' ',$_);
	    shift @T if $T[0] eq '+';
	    for  $i (0..(@T-1)) {$T[$i] = eval($T[$i]);}
	    push(@npomega,@T);
	}
    }
    $self -> {'npetabar'} = [@npetabar];
    $self -> {'npomegas'} = [@npomega];
    unless ( $success ) {
	debug -> warn( level   => 2,
		       message => "rewinding to first position..." );
    } else {
	$self -> {'lstfile_pos'} = $start_pos;
    }
}
end _read_npomegas

# }}} _read_npomegas

# {{{ _read_ofv

start _read_ofv
{
    my $start_pos = $self -> {'lstfile_pos'};

    while ( $_ = @{$self -> {'lstfile'}}[ $start_pos++ ] ) {
      if ( / PROBLEM NO\.:\s*\d+\n/ ) {
	$self -> parsing_error( message => "Error in reading  ".
				"the OFV!\nNext problem found" );
	return;
      }
      
      if ( $start_pos >= scalar @{$self -> {'lstfile'}} ) {
	$self -> parsing_error( message => "Error in reading  ".
				"the OFV!\nEOF found\n" );
	return;
      }

      if ( /^\s\*{50}\s+/ ) {
	(undef, my $ofvt, undef) = split(' ',$_,3);
	if ( $ofvt =~ /\*\*\*\*\*\*/ ) {
	  $self -> {'ofv'} = undef;
	} else {
	  $self -> ofv($ofvt);
	}
	last;
      }
    }
    $self -> {'lstfile_pos'} = $start_pos;
}
end _read_ofv

# }}} _read_ofv

# {{{ _rowcolind
start _rowcolind
      {
	my $i = 1;
	my $found = 0;
	while ( not $found ) {
	  my $test = $index - ($i-1)*($i)/2;
	  if ( $test <= $i ) {
	    $row = $i;
	    $col = $test;
	    $found = 1;
	  }
	  $i++;
	}
# 	my ($i,$j);
# 	my @startind = (2,5,9,14,20,27,35,44);
# 	my $prevind;
# 	my $notfirst = 0;
	
#       OUTER: foreach $col (1..8) {
# 	  $prevind = $startind[$col-1];
# 	  foreach $row (($col+1)..8) {
	    
# 	    ## If this is the first element of the column
# 	    if($index == $prevind) {
# 	      last OUTER;
# 	    }
	    
# 	    ## If it is a later element in the column
# 	    if($index == $prevind+$row) {
# 	#      print "$index $col ",$row+1," ",$startind[$col-1],"\n" ;
# 	      $row++;
# 	      last OUTER;
# 	    } else {
# 	      $prevind = $prevind+$row;
# 	      next;
# 	    }
# 	  }	
# 	}
#	print "$index ",$row," $col\n";
	
      }
end _rowcolind
# }}} _rowcolind

# {{{ _read_significant_digits
start _read_significant_digits
{
    my $start_pos = $self -> {'lstfile_pos'};
    
    while( $_ = @{$self -> {'lstfile'}}[ $start_pos++ ] ) {
      if ( / PROBLEM NO\.:\s*\d+\n/ ) {
	$self -> parsing_error( message => "Error in reading the number of ".
				"significant digits!\nNext problem found" );
	return;
      }
      
      if ( $start_pos >= scalar @{$self -> {'lstfile'}} ) {
	$self -> parsing_error( message => "Error in reading the number of ".
				"significant digits!\nEOF found\n" );
	return;
      }
      
      if ( /^1/ or /MINIMUM VALUE OF OBJECTIVE FUNCTION/ ) {
	$self -> parsing_error( message => "Error in reading the number of ".
				"significant digits!\nOFV found" );
	return;
      }

      if ( / NO. OF SIG. DIGITS UNREPORTABLE/ ) {
	# This is ok
	last;
      }

      if ( / NO. OF SIG. DIGITS IN FINAL EST.:\s*(-?\d*\.*\d*)/ ){
	$self -> significant_digits($1);
	last;
      }
    }
    $self -> {'lstfile_pos'} = $start_pos;
}
end _read_significant_digits
# }}} _read_significant_digits

# {{{ _read_sethomsi
start _read_sethomsi
      {
	my $start_pos = $self -> {'lstfile_pos'};
	my $success  = 0;
	
	my $thetarea = 0;
	my $omegarea = 0;
	my $sigmarea = 0;
	my ( @setheta, @seomega, @sesigma, @T, $i, $tmp );
	my ( @raw_setheta, @raw_seomega, @raw_sesigma );
	
	# _read_thomsi should leave us right at where we should start reading
	while ( $_ = @{$self -> {'lstfile'}}[ $start_pos++ ] ) {
	  chomp;
	  if ( ($omegarea or $sigmarea ) and 
	       /COVARIANCE MATRIX OF ESTIMATE/ ) {
	    # This is fine, we should end up here after reading the
	    # estimates
	    last;
	    $success = 1;
	  }

	  if ( /T MATRIX/ or
	       /R MATRIX/ ) {
	    # This is also fine, if those matrices were output, we
	    # should end up here before we could start reading the
	    # estimates
	    last;
	    $success = 1;
	  }

	  if ( /NONPARAMETRIC ESTIMATE/ ) {
	    # This is also fine. If the nonparametric step is run, we
	    # should end up here regardless of the termination status
	    # of the covariance step.
	    last;
	    $success = 1;
	  }

	  if ( /^1NONLINEAR/ ) {
	    $self -> parsing_error( message => "Error in reading the standard error of the parameter ".
				    "estimates!\nFound: $_" );
	    return;
	  }

	  if ( /THERE ARE ERROR MESSAGES IN FILE PRDERR/ ) {
	    # This is an NONMEM error message and is ok (to find), but
	    # it means that we can stop parsing the file
	    $self -> finished_parsing(1);
	    last;
	  }

	  if ( /THETA - VECTOR OF FIXED EFFECTS PARAMETERS/ ) {
	    $thetarea = 1;
	  }
	  if ( /OMEGA - COV MATRIX FOR RANDOM EFFECTS - ETAS/ ) {
	    $omegarea = 1;
	    $thetarea = 0;
	  }
	  if ( /SIGMA - COV MATRIX FOR RANDOM EFFECTS - EPSILONS/ ) {
	    $sigmarea = 1;
	    $omegarea = 0;
	    $thetarea = 0;
	  }

#	    last if ( / COVARIANCE MATRIX OF ESTIMATE/ );
#	    last if ( /THERE ARE ERROR MESSAGES IN FILE PRDERR/ );
	    # The row below checking for a one '1', crashes behaviour with MANY etas (>15)
#	    last if ( /^1/ );
#	    last if ( /^[A-W]/ and $searea );

	  if ( $thetarea and /^\s*-?\d*\.\d*/ ) {
	    @T = split(' ',$_);
	    for $i (0..(@T-1)) {
	      $T[$i] = eval($T[$i]);
	    }
	    push(@setheta,@T);
	  }
	  if($omegarea and /^(\+|\s{2,})/) {
	    next if /ET/;
	    @T = split(' ',$_);
	    shift @T if $T[0] eq '+';
	    if ( not defined $self -> {'omega_block_structure'} ) {
	      for $i (0..(@T-1)) {
		if(($T[$i] ne '.........') and (eval($T[$i]) != 0) ) {
		  push(@seomega,eval($T[$i]));
		}
	      }
	    }
	    for  $i (0..(@T-1)) {
	      if($T[$i] ne '.........') {
		$tmp = eval($T[$i]);
	      } else {
		$tmp = 'NA';
	      }
	      $T[$i] = $tmp ;
	    }
	    push(@raw_seomega,@T);
	  }

	  if($sigmarea and /^(\+|\s{2,})/) {
	    next if /EP/;
	    @T = split(' ',$_);
	    shift @T if $T[0] eq '+';
	    if ( not defined $self -> {'sigma_block_structure'} ) {
	      for  $i (0..(@T-1)) {
		if(($T[$i] ne '.........') and (eval($T[$i]) != 0) ) {
		  push(@sesigma,eval($T[$i]));
		}
	      }
	    }
	    for $i (0..(@T-1)) {
	      if ($T[$i] ne '.........') {
		$tmp = eval($T[$i]);
	      } else {
		$tmp = 'NA';
	      }
	      $T[$i] = $tmp ;
	    }
	    push(@raw_sesigma,@T);
	  }
	  
	  if ( $start_pos >= scalar @{$self -> {'lstfile'}} ) {
	    $self -> parsing_error( message => "Error in reading the standard error of the parameter ".
				    "estimates!\nEOF found\n" );
	    return;
	  }
	}
	
	
	if ( defined $self -> {'omega_block_structure'} ) {
	  my @omblock  = @{$self -> {'omega_block_structure'}}; 
	  debug -> warn( level   => 2,
			 message => "OMEGA BLOCK DEFINED" ) if ( scalar @omblock > 0 );
	  @seomega = ();
	  my $i = 0;
	  foreach my $row ( @omblock ) {
	    foreach my $element ( @{$row} ) {
	      push ( @seomega, $raw_seomega[$i] ) if ( $element );
	      $i++;
	    }
	  }
	}

	if ( defined $self -> {'sigma_block_structure'} ) {
	  my @siblock  = @{$self -> {'sigma_block_structure'}}; 
	  @sesigma = ();
	  my $i = 0;
	  debug -> warn( level   => 2,
			 message => "SIGMA BLOCK DEFINED" ) if ( scalar @siblock > 0 );
	  foreach my $row ( @siblock ) {
	    foreach my $element ( @{$row} ) {
	      push ( @sesigma, $raw_sesigma[$i] ) if ( $element );
	      $i++;
	    }
	  }
	}

	$self -> {'sethetas'}		 = [@setheta];
	$self -> {'raw_seomegas'}	 = [@raw_seomega];
	$self -> {'raw_sesigmas'}	 = [@raw_sesigma];
	$self -> {'seomegas'}		 = [@seomega];
	$self -> {'sesigmas'}		 = [@sesigma];

	if ( scalar @setheta <= 0 ) {
	  $self -> {'covariance_step_successful'} = 0;
	} else {
	  $self -> {'covariance_step_successful'} = 1;
	}

	unless ( $success ) {
	  debug -> warn( level   => 2,
			 message => "No standard errors for thetas, sigmas or omegas." );
	} else {
	  $self -> {'lstfile_pos'} = $start_pos;
	}
      }
end _read_sethomsi
# }}} _read_sethomsi

# {{{ _read_simulation
start _read_simulation
    {
      # The simulation step is optional.
      my $start_pos = $self -> {'lstfile_pos'};
      while ( $_ = @{$self -> {'lstfile'}}[ $start_pos ++ ] ) {
	if ( /MINIMUM VALUE OF OBJECTIVE FUNCTION/ ) {
	  last;
	}
	if ( /^\s*MONITORING OF SEARCH:/) {
	  last;
	}
	if ( /\s*SIMULATION STEP PERFORMED/ ) {
	  $self ->{'simulationstep'} = 1;
	  last;
	}
      }
      
      if ( $self -> {'simulationstep'} ) {
	$self -> {'lstfile_pos'} = $start_pos;
      } 
    }
end _read_simulation
# }}} _read_simulation

# {{{ _read_term

start _read_term
      {
	my $start_pos = $self -> {'lstfile_pos'};
	my $success_pos;
	my $success  = 0;
	my $pred_exit_code = 0;

	$self -> minimization_successful(0);
#	if ( $self -> {'covariance_step_run'} ) {
#	  $self -> covariance_step_successful(1);
#	  # If there are problems in the cov-step they will be caught later
#	}
	while ( $_ = @{$self -> {'lstfile'}}[ $start_pos++ ] ) {
	  $self -> {'s_matrix_singular'} = 1 if ( /^0S MATRIX ALGORITHMICALLY SINGULAR/ );
	  if ( /^0R MATRIX ALGORITHMICALLY SINGULAR/ or 
	       /^0S MATRIX ALGORITHMICALLY SINGULAR/ ) {
	    $self -> covariance_step_warnings(1);
	    next;
	  }
	  if ( /^0ESTIMATE OF THETA IS NEAR THE BOUNDARY AND/ or
	       /0PARAMETER ESTIMATE IS NEAR ITS BOUNDARY/ ) {
	    $self -> estimate_near_boundary(1);
	    next;
	  }
	  if ( /ROUNDING ERRORS/ ) {
	    $self -> rounding_errors(1);
	    next;
	  }
	  if ( /0COVARIANCE STEP ABORTED/ ) {
	    $self -> covariance_step_run(0);
	    next;
	  }

	  if ( / THIS MUST BE ADDRESSED BEFORE THE COVARIANCE STEP CAN BE IMPLEMENTED/ ) {
	    $self -> covariance_step_run(0);
	  }


          # "0ERROR RMATX-  1" should (if it exists) occur after the minim. succ message
          if ( /0ERROR RMATX-  1/ ) {
            $self -> minimization_successful(0);
	    next;
          }

	  if ( /^0MINIMIZATION SUCCESSFUL/ ) {
	    $self -> minimization_successful(1);
	    $success = 1;
	    $self -> {'lstfile_pos'} = $start_pos-1;
	    next;
	  }

	  if ( /^0MINIMIZATION TERMINATED/ ) {
	    $self -> minimization_successful(0);
	    $self -> covariance_step_run(0);
	    $success = 1;
	    $self -> {'lstfile_pos'} = $start_pos-1;
	    next;
	  }

	  if ( /^0SEARCH WITH ESTIMATION STEP WILL NOT PROCEED/ ) {
	    $self -> minimization_successful(0);
	    $self -> covariance_step_run(0);
	    $success = 1;
	    $self -> finished_parsing(1);
	    return;
	  }

	  if ( /0PRED EXIT CODE = 1/ ) {
	    # This is an error message but the severity of it depends
	    # on the origin of it
	    $pred_exit_code = 1;
	    next;
	  }

	  if ( $pred_exit_code and 
	       /MESSAGE ISSUED FROM SIMULATION STEP/ ) {

	    # These are probably not needed if we match the sim. step string above
#		 /ERROR IN TRANS4 ROUTINE: (.*)  IS ZERO/ or
#	         /ERROR IN TRANS4 ROUTINE: (.*)  IS NEGATIVE/ ) ) {

	    # This is an error message which terminates NONMEM. We
	    # return after reading the minimization message
	    $self -> minimization_successful(0);
	    $self -> _read_minimization_message();
	    $self -> finished_parsing(1);
	    return;
	  }

	  if ( /0PROGRAM TERMINATED BY OBJ/ ) {
	    # This is an error message which terminates NONMEM. We
	    # return after reading the minimization message
	    $self -> minimization_successful(0);
	    $self -> _read_minimization_message();
	    $self -> finished_parsing(1);
	    return;
	  }	    

	  if ( /MINIMUM VALUE OF OBJECTIVE FUNCTION/ ) {
	    debug -> warn( level   => 2,
			   message => "Hmmm, reached the OFV area" );
	    last;
	  }
	}
	if ($success) {
	  debug -> warn( level   => 2,
			 message => "Found a minimization statement" );
	  $self -> _read_minimization_message();
	  $self -> _read_eval()                 if ($self -> parsed_successfully());
	  $self -> _read_significant_digits()   if ($self -> parsed_successfully());
	} else {
	  debug -> warn( level   => 1,
			 message => "No minimization/termination statement found" ); #Back to starting line
	  $self -> parsing_error( message => "Error in reading minim/term statement!\n$!" );
	  return;
	}
      }
end _read_term

# }}} _read_term

# {{{ _read_minimization_message

start _read_minimization_message
      {
	# This method is called from _read_term() and the listfile_pos
	# variable should leave us right at the start of the
	# minimization message. _read_eval() and
	# _read_significant_digits() are called after this method so
	# we need to rewind to our starting position
	my $success  = 0;
	my (@mess, @etabar,@pval);
	my $termarea = 0;
    
	my $start_pos = $self -> {'lstfile_pos'};
	while ( $_ = @{$self -> {'lstfile'}}[ $start_pos++ ] ) {
	  if ( / PROBLEM NO\.:\s*\d+\n/ ) {
	    $self -> parsing_error( message => "Error in reading minimization ".
				    "message!\nNext problem found" );
	    return;
	  }
	     
	  if ( $start_pos >= scalar @{$self -> {'lstfile'}} ) {
	    $self -> parsing_error( message => "Error in reading minimization ".
				    "message!\nEOF found\n" );
	    return;
	  }

	  if ( /^1/ or /MINIMUM VALUE OF OBJECTIVE FUNCTION/ ) {
	    # This is ok. We would expect to end up here probably
	    # catching the '1' above
	    debug -> warn( level   => 2,
			   message => "Found minimization area and reached ".$_ );
	    $success = 1;
	    last;
	  }
	  push @mess,$_ ;
	}

	push( @{$self -> {'minimization_message'}}, @mess );

	my @temp;
	my $etabar_found = 0;
	for( my $i=0;$i<=$#mess;$i++ ){
	  my $line = $mess[$i];

	  if ( $etabar_found or ($line =~ /\s+ETABAR:\s+/) ) {
	    $etabar_found=1;
	    $line =~ s/ETABAR://;

	    last unless ( $line =~ s/^\s+//);
	    last unless ( $line =~ /\d/);
	    last if( $line =~ /^1/);
	    last if( $line =~ /[a-zA-DF-Z]/);

	    @temp = split(/\s+/,$line);
	    push @etabar, @temp;	
	  }
	}
	# Initialize the attribute only if we have found any data
	$self -> {'etabar'} = \@etabar if ( $#etabar > 0 );
    
	my $pval_found;
    	for( my $i=0;$i<=$#mess;$i++ ){
	  my $line = $mess[$i];
	  if ( $pval_found or ($line =~ /\s+P VAL\.:\s+/ ) ) {
	    $pval_found=1;
	    $line =~ s/P VAL\.://;

	    last unless ( $line =~ s/^\s+//);
	    last unless ( $line =~ /\d/);
	    last if( $line =~ /^1/);
	    last if( $line =~ /[a-zA-DF-Z]/);
	    @temp = split(/\s+/,$line);
	    push @pval, @temp;	
	  }
	}

	$self -> {'pval'} = \@pval;

	unless ( $success ) {
	  debug -> warn( level   => 2,
			 message => "No minimization message found" );
	}
      }
end _read_minimization_message

# }}} _read_minimization_message

# {{{ _read_thomsi

start _read_thomsi
      {
	my $start_pos = $self -> {'lstfile_pos'};
	my $success = 0;
	
	my $thetarea	 = 0;
	my $omegarea	 = 0;
	my $sigmarea	 = 0;
	my ( @theta, @omega, @raw_omega, @sigma, @raw_sigma, @T, $i, $tmp );

	# _read_ofv should leave us right at where we should start reading
	while ( $_ = @{$self -> {'lstfile'}}[ $start_pos++ ] ) {
	  chomp;
	  if ( ($omegarea or $sigmarea ) and 
	       # For some reason, NONMEM prints a '1' (i.e. /^1$/)
	       # after the thirteenth omega row. I other words, we
	       # cannot use /^1$/ as check for omega area ending.
	       ( 
#/^1\s*$/ or
		 /STANDARD ERROR OF ESTIMATE/ or
		 /NONPARAMETRIC ESTIMATE/ or
		 /T MATRIX/ or
		 /R MATRIX/ or
		 /TABLES OF DATA AND PREDICTIONS/ )) {
	    # This is fine, we should end up here after reading the estimates
	    $success = 1;
	    last;
	  }

	  if ( /^1NONLINEAR/ ) {
	    $self -> parsing_error( message => "Error in reading the parameter ".
				    "estimates!\nFound: $_" );
	    return;
	  }

	  if ( /THERE ARE ERROR MESSAGES IN FILE PRDERR/ ) {
	    # This is an NONMEM error message and is ok (to find), but
	    # it means that we can stop parsing the file
	    $self -> finished_parsing(1);
	    last;
	  }

	  if ( /THETA - VECTOR OF FIXED EFFECTS PARAMETERS/ ) {
	    $thetarea = 1;
	  }
	  if ( /OMEGA - COV MATRIX FOR RANDOM EFFECTS - ETAS/ ) {
	    $omegarea = 1;
	    $thetarea = 0;
	    $sigmarea = 0;
	  }
	  if ( /SIGMA - COV MATRIX FOR RANDOM EFFECTS - EPSILONS/ ) {
	    $sigmarea = 1;
	    $thetarea = 0;
	    $omegarea = 0;
	  }
	  # The row below checking for a one '1', crashes behaviour with MANY etas (>15)
#	last if ( /^1/ and $estarea );
	  # These rows should not be needed. If a valid alternative
	  # exists, add it as a match above or raise a parsing error
	  # message
#	  last if ( /^\s+\*\*\*\*\*\*\*\*\*\*\*\*/ and ( $sigmarea or $thetarea or $omegarea ));
#	  last if ( /^[A-W]/ and $estarea );

	  if( $thetarea and /^\s*-?\d*\.\d*/ ) {
	    @T = split(' ',$_);
	    for $i (0..(@T-1)) {
	      $T[$i] = eval($T[$i]);
	    }
	    push(@theta,@T);
	  }

	  if($omegarea and /^(\+|\s{2,})/) {
	    next if /ET/;
	    @T = split(' ',$_);
	    shift @T if $T[0] eq '+';
	    if ( $self -> {'omega_block_structure_type'} eq 'DIAGONAL' ) {
	      for  $i (0..(@T-1)) {
		if(($T[$i] ne '.........') and (eval($T[$i]) != 0) ) {
		  push(@omega,eval($T[$i]));
		}
	      }
	    }
	    for  $i (0..(@T-1)) {
	      if($T[$i] ne '.........') {
		$tmp = eval($T[$i]);
	      } else {
		$tmp = 'NA';
	      }
	      $T[$i] = $tmp ;
	    }
	    push(@raw_omega,@T);
	  }

	  if($sigmarea and /^(\+|\s{2,})/) {
	    next if /EP/;
	    next if /^\s*$/;
	    @T = split(' ',$_);
	    shift @T if $T[0] eq '+';
	    if ( $self -> {'sigma_block_structure_type'} eq 'DIAGONAL' ) {
	      for  $i (0..(@T-1)) {
		if(($T[$i] ne '.........') and (eval($T[$i]) != 0) ) {
		  push(@sigma,eval($T[$i]));
		}
	      }
	    }
	    for  $i (0..(@T-1)) {
	      if($T[$i] ne '.........') {
		$tmp = eval($T[$i]);
	      } else {
		$tmp = 'NA';
	      }
	      $T[$i] = $tmp ;
	    }
	    push(@raw_sigma,@T);
	    
	  }
	  if ( $start_pos >= scalar @{$self -> {'lstfile'}} ) {
	    # This is a valid match. Sometimes, the list file ends
	    # with the parameter estimates
	    $self -> finished_parsing(1);
	  }
	}

	if ( $self -> {'omega_block_structure_type'} eq 'BLOCK' ) {
	  my @omblock  = @{$self -> {'omega_block_structure'}}; 
	  debug -> warn( level   => 2,
			 message => "OMEGA BLOCK DEFINED" ) if ( scalar @omblock > 0 );
	  @omega = ();
	  my $i = 0;
	  foreach my $row ( @omblock ) {
	    foreach my $element ( @{$row} ) {
	      push ( @omega, $raw_omega[$i] ) if ( $element );
	      $i++;
	    }
	  }
	}

	if ( $self -> {'sigma_block_structure_type'} eq 'BLOCK' ) {
	  my @omblock  = @{$self -> {'sigma_block_structure'}}; 
	  debug -> warn( level   => 2,
			 message => "SIGMA BLOCK DEFINED" ) if ( scalar @omblock > 0 );
	  @sigma = ();
	  my $i = 0;
	  foreach my $row ( @omblock ) {
	    foreach my $element ( @{$row} ) {
	      push ( @sigma, $raw_sigma[$i] ) if ( $element );
	      $i++;
	    }
	  }
	}

	$self -> {'thetas'}	  = \@theta;
	$self -> {'nth'}	  = $#theta + 1;
	$self -> {'raw_omegas'} = \@raw_omega;
	$self -> {'nrom'} = $#raw_omega + 1;
	$self -> {'omegas'}	  = \@omega;
	$self -> {'nom'} = $#omega + 1;
	$self -> {'raw_sigmas'} = \@raw_sigma;
	$self -> {'sigmas'}	  = \@sigma;


	# Gather the "true" estimates, i.e. for the parameters that are not fixed or SAME.
	foreach my $param ( 'theta', 'omega', 'sigma' ) {
	  my @allests = eval( '@'.$param );
	  my @estflags;
	  if( $self -> msfi_used() ) {
	    foreach( @allests ) {
	      push(@estflags,1);
	    }
	  } else {
	    @estflags = @{$self -> {'estimated_'.$param.'s'}};
	  }
	  my @ests;
	  
#	  die "Something is wrong: All $param"."s: ".($#allests+1)." and estimated $param"."s: ".
#	      ($#estflags+1)." do not match\n" unless
#	      ( $#allests == -1 or $#estflags == $#allests );
	  my $defs = 0;
	  for( my $i = 0; $i <= $#allests; $i++ ) {
	    if( $estflags[$i] ) {
	      if ( defined $allests[$i] ) { 
		push( @ests, $allests[$i]);
		$defs++;
	      } else {
		push( @ests, 0 );
	      }
	    }
	  }
	  $self -> {'est_'.$param.'s'} = \@ests;

#	  if( $#ests > -1 and $defs > 0 ) {
#	    $self -> {'est_'.$param.'s'} = Math::MatrixReal ->
#		new_from_cols( [\@ests] );
#	  }
	}

	unless ( $success ) {
	  debug -> warn( level   => 2,
			 message => "No thetas, omegas or sigmas found" );
	} else {
# Keep this code
# 	  if ( $PsN::config -> {'_'} -> {'use_database'} and
# 	       $self -> {'register_in_database'} ) { 
# 	    my $dbh = DBI -> connect("DBI:mysql:host=".$PsN::config -> {'_'} -> {'database_server'}.
#			       ";databse=".$PsN::config -> {'_'} -> {'project'},
# 				     $PsN::config -> {'_'} -> {'user'},
#			        $PsN::config -> {'_'} -> {'password'},
# 				     {'RaiseError' => 1});
# 	    my $sth;
# 	    my @mod_str = ('','');
# 	    if ( defined $self -> {'model_id'} ) {
# 	      @mod_str = ('model_id,',"$self->{'model_id'},");
# 	    }
# 	    foreach my $param ( 'theta', 'omega', 'sigma' ) {
# 	      my $i = 1;
# 	      foreach my $par_str ( eval('@'.$param) ) {
# 		$sth = $dbh -> prepare("INSERT INTO ".$PsN::config -> {'_'} -> {'project'}.
#			       ".estimate ".
# 				       "(subproblem_id,problem_id,output_id,".
# 				       $mod_str[0].
# 				       "type,value, number, label) ".
# 				       "VALUES ( '$self->{'subproblem_id'}' ,".
# 				       "'$self->{'problem_id'}' ,".
# 				       "'$self->{'output_id'}' ,".
# 				       $mod_str[1].
# 				       "'$param','$par_str', '$i', 'test_label')");
# 		$sth -> execute;
# 		push( @{$self -> {'estimate_ids'}}, $sth->{'mysql_insertid'} );
# 		$i++;
# 	      }
# 	    }
# 	    $sth -> finish;
# 	    $dbh -> disconnect;
# 	  }


	  $self -> {'lstfile_pos'} = $start_pos-1;
	}
      }
end _read_thomsi

# }}} _read_thomsi

# {{{ _set_omeganames
start _set_omeganames
      {
	my @raw_omegas = @{$self -> {'raw_omegas'}};
    
	unless ( scalar @raw_omegas > 0 ) {
	  return undef;
	}
	;
    
	my ( @omeganames, %omeganameval, @omegas );
	my ($j,$ndiags);
	my @indices;
	foreach $j (1..scalar @raw_omegas) {
	  push @omeganames, "OM".++$ndiags and next if $self -> _isdiagonal('index' => $j);
	  if ($raw_omegas[$j-1] !=0) {
	    @indices = $self -> _rowcolind( index => $j);
	    push @omeganames,"OM".$indices[0]."\.".$indices[1];
	  }
	}
	@omegas = @{$self -> {'omegas'}};
	for ( my $i = 0; $i <= $#omeganames; $i++ ) {
	  $omeganameval{$omeganames[$i]} = $omegas[$i];
	}
	$self ->{'omeganameval'} = \%omeganameval;
	$self ->{'omeganames'} = \@omeganames;
      }
end _set_omeganames
# }}} _set_omeganames

# {{{ _set_sigmanames
start _set_sigmanames 
      {
	my @sigmas = @{$self -> {'raw_sigmas'}};
	unless ( scalar @sigmas > 0 ) {
	  return undef;
	}

	my ( @sigmanames, %sigmanameval );
	my ($j,$ndiags);
	my @indices;
	foreach $j (1..scalar @sigmas) {
	  push @sigmanames, "SI".++$ndiags and next if $self -> _isdiagonal('index' => $j);
	  if ($sigmas[$j-1] !=0) {
	    @indices = $self -> _rowcolind( index => $j);
	    push @sigmanames,"SI".$indices[0].$indices[1];
	  }
	}
	@sigmas = @{$self -> {'sigmas'}};
	for ( my $i = 0; $i <= $#sigmanames; $i++ ) {
	  $sigmanameval{$sigmanames[$i]} = $sigmas[$i];
	}
	$self ->{'sigmanameval'} = \%sigmanameval;
	$self ->{'sigmanames'} = \@sigmanames;
      }
end _set_sigmanames
# }}} _set_sigmanames

# {{{ _set_thetanames
start _set_thetanames 
      {
	my $nth = $self -> {'nth'};
	my ( @thetanames, %thetanameval, @thetas );
    
	for (1..$nth) {
	  push @thetanames, "TH$_";
	}
	@thetas = @{$self -> {'thetas'}};
	for ( my $i = 0; $i <= $#thetanames; $i++ ) {
	  $thetanameval{$thetanames[$i]} = $thetas[$i];
	}
	$self ->{'thetanameval'} = \%thetanameval;
	$self ->{'thetanames'} = \@thetanames;
      }
end _set_thetanames
# }}} _set_thetanames


