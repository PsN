start _write
    {
      die "ERROR: extra_data -> _write: No filename set in object.\n"
	  if( $filename eq '' );

      if( not defined $self -> {'individuals'} ){
	unless( $filename eq $self -> full_name ){
	  $self -> synchronize;
	} 
      }
      
      open(FILE,">$filename") || 
	  die "Could not create $filename\n";
      my @data = @{$self -> format_data};
      for ( @data ) {
	print ( FILE );
      }
      close(FILE);
    }
end _write
