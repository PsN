start include statements
my $prefix = '/var/lib/moshog';
my $request_directory = "$prefix/requests";
my $grant_directory = "$prefix";
end include statements

start new
end new

start request
    {
      my $start_threads = $self -> {'start_threads'} ;
      my $granted_threads = $self -> {'granted_threads'} ;

      if( -e "$request_directory/$$" ){
	unlink( "$request_directory/$$" );
      }	
      
      if( open( REQUEST, '>moshog_request' ) ){
	#flock( REQUEST, 2 ); # 2 is FLOCK_EX
	print( REQUEST "S:$start_threads R:$request\n" );
	#flock( REQUEST, 8 ); # 8 is FLOCK_UN
	close( REQUEST );
	system( 'ln -s `pwd`/moshog_request ' . "$request_directory/$$" );
      }
      
      # This is a pipe open, therefore we will have an alarm set to
      # revive us if no-one is listening (ie moshog is dead)
      
#      eval {
#	$SIG{ALRM} = sub { die };
#	alarm(60);
#	open( WAKEUP, ">$grant_directory/wakeup" );
#	print WAKEUP "junk\n";
#	close( WAKEUP );
#	alarm(0);
#      };

      $self -> {'last_request'} = $request;
    }
end request

start granted
    {
      $grant = 0;
#      eval {
#	$SIG{ALRM} = sub { die };
#	alarm(60);
      if( -e "$grant_directory/$$" ){
	if( open( GRANT, "<$grant_directory/$$" ) ){
	  my @grant_arr = <GRANT>;
	  close( GRANT );
	  $grant = $grant_arr[0];
	  chomp($grant);
#	alarm(0);
	  $self -> {'granted_threads'} = $grant;
	}
      }
#      };
    }
end granted
  
