use ext::Config::Tiny;
use debug;
use ext::File::HomeDir;

$config = ext::Config::Tiny -> read( $config_file );

unless( $config ){
  debug -> die( message => "In PsN configuration file[" . $config_file . "]:" . $ext::Config::Tiny::errstr );
}

if( -e home() . "/psn.conf" ){
  my $personal_config = ext::Config::Tiny -> read( home() . '/psn.conf' );
  %{$config} = (%{$config}, %{$personal_config});
}

unless( exists $config -> {'low_INF'} ){
  $config -> {'low_INF'} = -1000000;
}

unless( exists $config -> {'high_INF'} ){
  $config -> {'high_INF'} = 1000000;
}

$out_miss_data;
$output_header;
$factorize_strings;
if ( $config -> {'_'} -> {'output_style'} eq 'SPLUS' ) {
  $out_miss_data = 'NA';
  $output_header = 1;
  $factorize_strings = 0;
} elsif ( $config -> {'_'} -> {'output_style'} eq 'MATLAB' ) {
  $out_miss_data = 'NaN';
  $output_header = 0;
  $factorize_strings = 1;
} else { # Default style EXCEL
  $out_miss_data = undef;
  $output_header = 1;
  $factorize_strings = 0;
}
1;
