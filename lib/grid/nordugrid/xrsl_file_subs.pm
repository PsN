# {{{ description and synopsis 

start description

    # This class is used to create xrsl files. Xrsl is short for
    # eXstended Resource Specification Language. Xrsl is used to speak
    # with nordugrid GM's.

end description

start synopsis
    #   use xrsl_file;
    #
    #   my $xrsl_file_obj = xrsl_file -> new ( filename => 'jobDescription.xrsl' );
    #
    #   $xrsl_file_obj -> write( filename => 'anotherOptionalFilename.xrsl' );
end synopsis
    
# }}}

# {{{ _print_relation

start _print_relation
{

    # _print_relation outputs a line of the form 
    # '$keyWord OP [(] value [)] [(values)...]'
    # Where OP defaults to '='. Otherwise it can be specified to any.

    print $filehandle '(' . $keyWord . $operator;
    foreach my $value ( @values ){

	if( $keyWord eq 'outputFiles' or $keyWord eq 'inputFiles' ) {
	    print $filehandle "\n(";
	}

	my $tmp = $value;
	if( $keyWord eq 'inputFiles' 
	    or $keyWord eq 'outputFiles' 
	    or $keyWord eq 'executables'){
	    if( ref($value) eq 'ARRAY' ){
		$tmp = $value -> [1] ;
		$value = $value -> [0];
	    } else {
		$tmp =~ s/[\/\\]/_/g;
	    }
	}
	print $filehandle $tmp;
	
	# Postvalue modifications
	if( $keyWord eq 'outputFiles') {
	    if( defined $self -> out_base_url){
		print $filehandle ' ' . $self -> out_base_url . $value;
	    } else {
		print $filehandle ' ""' ;
	    }
	}
	if( $keyWord eq 'inputFiles' ){
	    print $filehandle ' ' . $self -> in_base_url . $value;
	}
	
	if( $keyWord eq 'outputFiles' or $keyWord eq 'inputFiles' ) {
	    print $filehandle ')';
	}

	if( @values > 1 ){
	    print $filehandle ' ' ;
	}
    }
    print $filehandle ")";
}
end _print_relation

# }}}

# {{{ write

start write
{

    # write outputs the contents of the $self hash to a file. If no
    # filename is specified as argument, it uses the filename member
    # or, if the filename member is undefined, write defaults to
    # STDOUT. write excludes strings in the hash member
    # "excludes". The "excludes" member is initialized in "new" with
    # default values specified in "xrsl_file.dia"

    my $filehandle;

    unless( defined $filename ){
	$filename = $self -> filename;
    }

    if( defined $filename ){
      open($filehandle, ">", $filename );
    } else {
      open($filehandle, ">-");
    }

    print $filehandle "&";

    foreach my $key (keys %{$self}){
      next if( $key =~ /^__/ );
	if( $self -> {$key} ){
	    my @values;

	    if( ref($self -> {$key}) eq 'ARRAY' ){
		@values = $self -> {$key};
	    } else {
		@values = [$self -> {$key}];
	    }

	    unless( $self -> excludes -> {$key} ){
		if( $self -> {$key . 'Operator'} ){
		    $self -> _print_relation(filehandle => $filehandle,
					     keyWord => $key,
					     operator => $self -> {$key . 'Operator'},
					     values => @values)
		} else {
		    $self -> _print_relation(filehandle => $filehandle,
					     keyWord => $key,
					     values => @values);
		}
		print $filehandle "\n";
	    }
	}
    }
    close( $filehandle );
}
end write

# }}}
