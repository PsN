# {{{ include

start include statements
use debug;
use ui;
end include

# }}} include statements


start new
      {
        # The idnumber attribute does not have an explicit default value but
        # is, if no number is given, instead set to the value of the first
        # id column which is assumed only to contain one value.
	# 
	# Either subject_data or init_data must be given. Subject data is
	# a two-dimensional array holding the values of the subject. Init_data
	# is an one-dimensional array of strings, probably extracted from a
	# file where each row is one element in the array. This array is the
	# parsed by the constructor and moved to subject_data.

        die "Error in data::individual -> new: No ID column specified.\n"
           unless ( defined $this -> {'idcolumn'} );
        if ( defined $this -> {'subject_data'} ) {
	  if ( ! defined $this -> {'idnumber'} ) {
	    my @data = split( /,/ , $this -> {'subject_data'} -> [0]);
	    $this -> idnumber(@data[$this -> {'idcolumn'}-1] );
	  }
	} else {
	  die "Error in data::individual -> new: No init_data specified.\n"
	    unless ( defined $this -> {'init_data'} );
	  $this -> _read_data;
	}
      }
end new

start copy
        my @new_data = ();
        foreach my $row ( @{$self -> subject_data} ) {
	  my $new_row = $row;
	  push ( @new_data, $new_row );
	}
        $individual_copy =
           data::individual -> new ( idcolumn     => $self -> {'idcolumn'},
				     idnumber     => $self -> {'idnumber'},
				     subject_data => \@new_data );
end copy

# {{{ diff

start diff
{
  my @data      = @{$self -> {'subject_data'}};
  my @diff_data = @{$against_individual -> subject_data};
  for ( my $i = 0; $i <= $#data; $i++ ) {
    my @data_row = split( /,/ , $data[$i] );
    my @diff_data_row = split( /,/ , $diff_data[$i] );
    if( $largest ) {
      for( my $j = 0; $j <= $#columns; $j++ ) {
	my $diff = $data_row[$columns[$j]-1] - $diff_data_row[$columns[$j]-1];
	$diff = abs($diff) if( $absolute_diff );
	if( $diff_as_fraction ) {
	  if ( defined $diff_data_row[$columns[$j]-1] and not $diff_data_row[$columns[$j]-1] == 0 ) {
	    $diff = $diff/$diff_data_row[$columns[$j]-1];
	  } elsif ( not $diff == 0 ) { # If $diff == 0 we can leave it as it is even if we formally
	    print "ID: ",$self -> {'idcolumn'}," ID2: ",$against_individual -> idcolumn,"\n";
	    print "DATA1: ",join("\n",@data),"\n";
	    print "DATA2: ",join("\n",@diff_data),"\n";
	    # would require a division by the original value
	    debug -> die( message => "The difference of column ".$columns[$j].
			  " was requested as a fraction but the original value was ".
			  "found to be zero: a division is impossible." );
	  }
	}
#	print "T: ",($data_row[$columns[$j]-1] - $diff_data_row[$columns[$j]-1])/$diff_data_row[$columns[$j]-1],"\n" if ( $self -> {'idnumber'} == 1332 );
#	print "\t",$data_row[$columns[$j]-1],"\t",$diff_data_row[$columns[$j]-1],"\t$diff" if ( $self -> {'idnumber'} == 1332 );
	if( not defined $diff_results{$columns[$j]} or 
	    not defined $diff_results{$columns[$j]}{'diff'} or
	    $diff > $diff_results{$columns[$j]}{'diff'} ) {
	  $diff_results{$columns[$j]}{'diff'} = $diff;
	  $diff_results{$columns[$j]}{'self'} = $data_row[$columns[$j]-1];
	  $diff_results{$columns[$j]}{'test'} = $diff_data_row[$columns[$j]-1];
	}
      }
 #     print "\n" if ( $self -> {'idnumber'} == 1332 );
    } else {
      die "individual -> diff is only implemented for finding the largest difference at any observation at this point\n";
    }
  }
}
end diff

# }}} diff

# {{{ drop_columns

start drop_columns
      {
	my @new_data;
	my @data = @{$self -> {'subject_data'}};
	for( my $i = 0; $i <= $#data; $i++ ) {
	  my @new_row;
	  my @data_row = split( /,/, $data[$i] );
	  for( my $j = 0; $j < scalar @data_row; $j++ ) {
	    push( @new_row, $data_row[$j] ) if ( not $drop[$j] );
	  }
	  push( @new_data, join( ',', @new_row ) );
	}
	$self -> {'subject_data'} = \@new_data;
      }
end drop_columns

# }}} drop_columns

start evaluate_expression
        my $new_expr;
        my $data = $self -> {'subject_data'};
        if ( defined $expression ) {
	  if ( $all_rows ) {
	    $result = 1;
	    foreach my $row ( @{$data} ) {
	      my @row = split( /,/ , $row );
	      ( $new_expr = $expression ) =~ s/{}/\$row[ \$column-1 ]/g;
	      $result = $result*eval( $new_expr );
	    }
	  } else {
	    my @row = split( /,/, $data -> [0] );
	    ( $new_expr = $expression ) =~ s/{}/\$row[ \$column-1 ]/g;
	    $result = eval( $new_expr );
	  }
	}
end evaluate_expression

start factors
        my @data = @{$self -> {'subject_data'}};
        for ( my $i = 0; $i <= $#data; $i++ ) {
	  my @data_row = split( /,/ , $data[$i] );
	  push( @{$factors{$data_row[$column-1]}}, $i );
	}
end factors

start idnumber
        if ( defined $parm ) {
	  for( my $i = 0 ; $i < scalar(@{$self -> {'subject_data'}}); $i++ ) {
	    my @row = split( /,/, $self -> {'subject_data'} -> [$i] );
	    $row[ $self -> {'idcolumn'} - 1 ] = $parm;
	    $self -> {'subject_data'} -> [$i] = join( ',', @row);
	  }
	}
end idnumber

start recalc_column
        my ( $new_expr );
        for( my $i = 0 ; $i < scalar(@{$self -> {'subject_data'}}); $i++ ) {
	  my @row = split( /,/, $self -> {'subject_data'} -> [$i] );
          ( $new_expr = $expression ) =~ s/{}/\$row[ \$column-1 ]/g;
          $row[ $column-1 ] = eval( $new_expr );
	  $self -> {'subject_data'} -> [$i] = join( ',', @row);
        }
end recalc_column

start _read_data
        if ( defined $self -> {'init_data'} ) {
	  $self -> {'subject_data'} = $self -> {'init_data'};
	  $self -> {'init_data'} = undef;
#          my @init_data = @{$self -> {'init_data'}};
#	  my @last_row;
#          for ( @init_data ) {
#	    my @elements = split(/\,\s*|\s+/);
#	    push( @{$self -> {'subject_data'}}, \@elements );
#	    @last_row = @elements;
#          }
#          $self -> idnumber($last_row[$self -> {'idcolumn'}-1]);
#	  $self -> {'init_data'} = undef;
        }
end _read_data
