# {{{ Include

start include statements
use Data::Dumper;
my @print_order = ('problem','abbreviated','input','data','msfi','contr','subroutine','prior','model','tol','infn','omega','pk','aesinit','aes','des','error','pred','mix','theta','sigma','simulation','estimation','covariance','nonparametric','table','scatter');
my @sde_print_order = ('problem','abbreviated','input','data','msfi','contr','subroutine','prior','model','tol','infn','theta','omega','sigma','pk','aesinit','aes','des','error','pred','mix','simulation','estimation','covariance','nonparametric','table','scatter');
my %abbreviations;

# Here we intialize a hash used to find long names for abbreviated
# record names. We use the print_order array which contains all
# allowed record types.

foreach my $record_name( @print_order ){
  my $uc_short_type = substr(uc($record_name),0,3);;
  $uc_short_type = $uc_short_type.' ' if ( $record_name eq 'aes' );
  $uc_short_type = $uc_short_type.'I' if ( $record_name eq 'aesinit' );
  $abbreviations{$uc_short_type} = $record_name;
}
end include statements

# }}} include statements

# {{{ new

start new
    {
      
      unless ( defined $parm{'problems'} ) {
	# Parse given problem lines.
	$this -> _read_records();
	delete $this -> {'prob_arr'};
      }

      # Initialize table file objects (if any)
      $this -> _read_table_files( ignore_missing_files =>
				  $this -> {'ignore_missing_output_files'} );

      # Initialize extra_data objects (if any)
      if ( defined $this -> {'extra_data_file_name'} ) {
	$this -> {'extra_data'} = extra_data -> new ( filename => $this -> {'extra_data_file_name'},
						      header   => $this -> {'extra_data_header'},
						      target   => 'disk',
						      ignore_missing_files =>
						      $this -> {'ignore_missing_files'} );
      }
      
      if( $this -> {'cwres'} ){

	$this -> add_cwres_module( 'init_data' => { problem => $this,
						    nm_version => $this -> {'nm_version'},
						    mirror_plots => $this -> {'mirror_plots'} } );
	
      }

    }
end new

# }}} new

# {{{ add_marginals_code

start add_marginals_code

# add_marginals_code takes one argument.
#
# - nomegas which is the number of (diagonal-element)
# omegas.
#
# For each omega, verbatim code is added to make the marginals
# available for printing (e.g. to a table file). COM(1) will hold the
# nonparametric density, COM(2) the marginal cumulative value for the
# first eta, COM(2) the marginal cumulative density for the second eta
# and so on.
# The code is added to the $ERROR record

my $record_ref = $self -> errors;
if( defined $record_ref and defined $record_ref -> [0] ) {
  my ( @first_params, @last_params );
  $last_params[0] = '"   COM(1) = DENM';
  $first_params[0] = '"     X ';
  my $j = 0;
  my $comma;
  for( my $i = 1; $i <= $nomegas; $i++ ) {
    $comma = $i == $nomegas ? '' : ',';
    if( not ($i % 4) ) { # break line every fifth omega
      $j++;
      $first_params[$j] = '"     X ';
    }
    $first_params[$j] = $first_params[$j]."DEN$i$comma";
    push( @last_params, '"   COM('.($i+1).") = DEN$i" );
  }
  my $first_code = $record_ref -> [0] -> verbatim_first;
  push( @{$first_code}, ( '"  COMMON /ROCM18/ DENM,', @first_params,
			  '"  DOUBLE PRECISION DENM,', @first_params ) );
  $record_ref -> [0] -> verbatim_first( $first_code );
  my $last_code = $record_ref -> [0] -> verbatim_last;
  push( @{$last_code}, @last_params );
  $record_ref -> [0] -> verbatim_last( $last_code );
  last; # Only insert the code in the first record found (of the ones specified above)
} else {
  'debug' ->  warn( level => 2, 
		    message => "No \$ERROR record was found. Can't add verbatim code".
		    " to access nonparametric marginals" );
}

end add_marginals_code

# }}} add_marginals_code

# {{{ cont_wrap_columns

start cont_wrap_columns
      {
	if ( defined $self -> {'primary_columns'} ) {
	  $wrap_column = scalar @{$self -> {'primary_columns'}};
	  for ( my $i = 0; $i < $wrap_column ; $i++ ) {
	    $cont_column = ($i+1) if ( $self -> {'primary_columns'}[$i][0] eq 'CONT' );
	  }
	}
      }
end cont_wrap_columns

# }}} cont_wrap_columns

# {{{ contify_tables

start contify_tables

if( defined $self -> {'tables'} ){
  for( my $i = 0; $i < scalar @{$self -> {'tables'}}; $i++ ) {
    my $table = $self -> {'tables'}[$i];
    my $cont_column = $table -> contify;
# I don't know what I had in mind when I wrote this piece of code:
# Maybe I'll remember some day, better leave it as it is for now:

# 	    my @prim;
# 	    for( my $j = 1; $j <= scalar @{$table -> options}; $j++ ) {
# 	      my $option = $options[$j-1];
# 	      my $name = $option -> name;
# 	      last if( $name eq 'BY' or  $name eq 'PRINT' or $name eq 'NOPRINT' or
# 		       $name eq 'FILE' or $name eq 'NOHEADER' or $name eq 'ONEHEADER' or
# 		       $name eq 'FIRSTONLY' or $name eq 'NOFORWARD' or $name eq 'FORWARD' or
# 		       $name eq 'APPEND' or $name eq 'NOAPPEND' or $name eq 'UNCONDITIONAL' or
# 		       $name eq 'CONDITIONAL' or $name eq 'OMITTED' );
# 	      my @col = ( $name, $j, $option -> value );
# 	      push( @prim, \@col );
# 	    }
    if( defined $self -> {'table_files'} and defined $self -> {'table_files'}[$i] ) {
#	      $self -> {'table_files'}[$i] -> primary_columns( \@prim );
      $self -> {'table_files'}[$i] -> cont_column( $cont_column );
    }
  }
}

end contify_tables

# }}} contify_tables

# {{{ dropped_columns

start dropped_columns
      {
	my $inp_ref = $self -> inputs;
	if ( defined $inp_ref and defined $inp_ref -> [0] ) {
	  my $input = $inp_ref -> [0];
	  my $opt_ref = $input -> options;
	  if ( defined $opt_ref ) {
	    my @options = @{$opt_ref};
	    foreach my $option ( @options ) {
	      my $dropped = ( $option -> value eq 'DROP' or
			      $option -> value eq 'SKIP' ) ? 1 : 0;
	      push ( @dropped_columns, $dropped );
	    }
	  }
	}
      }
end dropped_columns

# }}} dropped_columns

# {{{ drop_dropped

start drop_dropped
      {
	my $inp_ref = $self -> inputs;
	# Important that the drop_dropped method of the data class is
	# in sync with this method.
	if ( defined $inp_ref and defined $inp_ref -> [0] ) {
	  my $input = $inp_ref -> [0];
	  my $opt_ref = $input -> options;
	  if ( defined $opt_ref ) {
	    my @options = @{$opt_ref};
	    my @keep;
	    foreach my $option ( @options ) {
	      push ( @keep, $option ) if ( not ($option -> value eq 'DROP' or $option -> value eq 'SKIP') or
					   $option -> name =~ /DAT(E|1|2|3)/ );
	    }
	    $input -> options( \@keep );
	  }
	}
      }
end drop_dropped

# }}} drop_dropped

# {{{ extra_data_header
start extra_data_header
      {
	if ( defined $parm ) {
	  if ( defined $self -> {'extra_datas'} and
	       defined $self -> {'extra_datas'} -> [0] ) {
	    $self -> {'extra_datas'} -> [0] -> header( $parm );
	  }
	}
      }
end extra_data_header
# }}}

# {{{ extra_data_file_name

start extra_data_file_name
      {
	if ( defined $parm ) {
	  $self -> {'extra_data'} = 'extra_data' -> new ( filename => $parm,
							  target   => 'disk',
							  ignore_missing_files =>
							  $self -> {'ignore_missing_files'} );
	}
      }
end extra_data_file_name

# }}}

# {{{ read_table_files

start _read_table_files
      {
	@{$self -> {'table_files'}} = ();
	my ( $table_name_ref, $junk ) = $self -> _option_val_pos( record_name => 'table',
								  name        => 'FILE' );
	if ( defined $table_name_ref and scalar @{$table_name_ref} >= 0 ) {
	  $self -> {'table_files'} = [];
	  foreach my $table_name ( @{$table_name_ref} ) {
	    'debug' -> warn( level => 2, 
			     message => "Creating new table_file object from $table_name" );
	    my $new_table = data -> new( directory            => $self -> {'directory'},
					 filename             => $table_name,
					 ignore_missing_files => $ignore_missing_files,
					 target               => 'disk',
					 table_file           => 1 );
	    push( @{$self -> {'table_files'}}, $new_table );
	  }
	}
      }
end _read_table_files

# }}}

# {{{ add_records
start add_records
    {
      # add_records( type => 'subroutine',
      #               record_strings => ['OTHER=get_cov', 'OTHER=read'] )
      # TODO change name from record to records.

      # To read add a record, we figure out what its full class name
      # is. Then we check if we have an accessor for the record type,
      # if we do then the record is valid and we call the appropriate
      # contructor. Both record_strings an type are mandatory.

      my $rec_class = "model::problem::$type";
      my $accessor = $type.'s';
      if( $self -> can($accessor) ){
	push( @{$self -> {$accessor}}, $rec_class -> new ( record_arr => \@record_strings ));
							   
      } else {
	'debug' -> die( message => "Trying to add unknown record: $type" );
      }
    }
end add_records
# }}} add_records

# {{{ covariance
start covariance
      {
	my @records;
	if( defined $self -> {'covariances'} ) {
	  @records = @{$self -> {'covariances'}} ;
	}
	if ( defined $enabled ) {
	  if ( $enabled and $#records < 0 ) {
	    $self -> add_records( type           => 'covariance',
				  record_strings => [''] );
	  } elsif ( not $enabled and $#records >= 0 ) {
	    $self -> {'covariances'} = undef;
	  }
	} else {
	  if ( $#records >= 0 ) {
	    $indicator = 1;
	  } else {
	    $indicator = 0;
	  }
	}
      }
end covariance
# }}} covariance

# {{{ eigen

start eigen
      {
	my ( $print_ref, $position ) = $self -> _option_val_pos( record_name => 'covariance',
							     name        => 'PRINT' );
#	print Dumper $print_ref;
#	print Dumper $position;
#	die;
#	if ( defined $enabled ) {
#	  if ( $enabled and scalar @{$print_ref} < 1 ) {
#	if ( 
      }
end eigen

# }}} eigen

# {{{ header
start header
      {
	my $inp_ref = $self -> inputs;
	if ( defined $inp_ref and defined $inp_ref -> [0] ) {
	  my $input = $inp_ref -> [0];
	  my $opt_ref = $input -> options;
	  if ( defined $opt_ref ) {
	    my @options = @{$opt_ref};
	    foreach my $option ( @options ) {
	      push ( @header, [$option -> name, $option -> value] );
	    }
	  }
	}
      }
end header
# }}} header

# {{{ indexes

start indexes
  # The Indexes method calculates the index for a parameter. Off-diagonal elements
  # will get a index 'i_j', where i is the row number and j is the column number
      {
	my $row = 1;
	my $accessor = $parameter_type.'s';

	if( defined $self -> {$accessor} ){

	  # If we hit a "SAME" parameter we need to remember the
	  # previous parameter size. ( calculated as "row - previous_row" )

	  my $previous_row = 0;

	  foreach my $record ( @{$self -> $accessor} ) {
	    # If not a block or if the block is of size 1 use normal numbering
	    
	    if( $record -> same ) {

	      if( $previous_row == 0 ){
		'debug' -> die( message => "You can't have an $parameter_type estimate defined as SAME if it's the first estimate" );
	      }
	      my $size = $row - $previous_row;
	      $previous_row = $row;
	      for( 1..$size ){
		push( @indexes, $row++);
	      }

	    } elsif ( ( ! defined $record -> size ) or
		      ( $record -> size < 2 )) {
	      if ( defined $record -> options ) {
		$previous_row = $row;
		foreach my $option ( @{$record -> options} ) {
		  push( @indexes, $row++ );
		}
	      }
	    } else {
	      # ... else use off-diagonal indexing where so is necessary
	      my $size = $record -> size;
	      for ( my $i = $row; $i <= $row + $size - 1; $i++ ) {
		for ( my $j = $row; $j <= $i; $j++ ) {
		  if ( $j == $i ) {
		    push( @indexes, "$i" );
		  } else {
		    push( @indexes, "$i".'_'."$j" );
		  }
		}
	      }
	      $previous_row = $row;
	      $row += $size;
	    }
	  }
	  if ( scalar @parameter_numbers > 0 ) {
	    my @part_indexes = ();
	    foreach my $num ( @parameter_numbers ) {
	      if ( $num < 1 or $num > scalar @indexes ) {
		'debug' -> die( message => "$parameter_type number " . $num . " does not exist in this model::problem\n" .
				"(" . scalar @indexes . " exists)\n" );
	      }
	      push( @part_indexes, $indexes[$num -1] );
	    }
	    @indexes = @part_indexes;
	  } else {
	    'debug' -> warn( level => 2, 
			     message => "Model::problem -> indexes: parameter_numbers undefined, using all." );
	  }
	}
      }
end indexes

# }}} indexes

# {{{ nomegas

start nomegas

foreach my $omega ( @{$self -> {'omegas'}} ) {
  my $size = $omega -> size;
  if( defined $size ) {

    # If the record has a size, it is of block form with diagonal of
    # length given by $size. The actual values in the model file is
    # then the arithmetic sum: (n*(n+1))/2

    if( $with_correlations ){
      $nomegas += ($size*($size+1))/2;

    # But we really only want the diagonal elements here:
    } else {
      $nomegas += $size;
    }
  } else {
    $nomegas += scalar @{$omega -> options};
  }
}

end nomegas

# }}} nomegas

# {{{ nsigmas

start nsigmas

foreach my $sigma ( @{$self -> {'sigmas'}} ) {
  my $size = $sigma -> size;
  if( defined $size ) {

    # If the record has a size, it is of block form with diagonal of
    # length given by $size. The actual values in the model file is
    # then the arithmetic sum: (n*(n+1))/2

    if( $with_correlations ){

      $nsigmas += ($size*($size+1))/2;

    } else {

      # But we really only want the diagonal elements here:

      $nsigmas += $size;
    }
  } else {
    $nsigmas += scalar @{$sigma -> options};
  }
}

end nsigmas

# }}} nsigmas

# {{{ primary_columns

start primary_columns
      {
	if ( defined $parm ) {
	  my $inp_ref = $self -> inputs;
	  if ( defined $inp_ref and defined $inp_ref -> [0] ) {
	    $self -> inputs -> [0] -> options( [] );
	  } else {
	    $self -> inputs( ['input' -> new()] );
	  }
	  foreach my $arr_ref ( @{$parm} ) {
	    my @col = @{$arr_ref};
	    # It is important to keep (almost) all primary columns
	    # undropped.  if one columns is dropped, all the data in
	    # the same columns in the rows with CONT=1 will also be
	    # dropped. In addition the indexing of EVTREC will be
	    # wrong. The only exception to this are the DAT(E|1|2|3)
	    # columns. The must be dropped because the NONMEM data set
	    # may not include non-digits. To handle this, these
	    # columns will be only include dummy variables in the
	    # secondary_columns attribute.
	    my $value;
#	    if( $col[2] eq 'DROP' ) { 
#	      $value = undef;
#	    } else {
#	      $value = $col[2];
	#    }
#	    print "N: $col[0], V: $col[2]\n";
	    my $value = $col[0] =~ /DAT(E|1|2|3)/ ? $col[2] : undef;
            $self -> inputs -> [0] -> add_option ( init_data => { name  => $col[0],
								  value => $value } );
	  }
	  $self -> contify_tables;
	}
      }
end primary_columns

# }}} primary_columns

# {{{ secondary_columns

start secondary_columns
      {
	if ( defined $parm ) {
	  if ( defined $self -> pks ) {
	    $self -> pks -> [0] -> secondary_columns($parm);
	  }
	  if ( defined $self -> preds ) {
	    $self -> preds -> [0] -> secondary_columns($parm);
	  }
	  if ( defined $self -> errors ) {
	    $self -> errors -> [0] -> secondary_columns($parm);
	  }
	  # This is a lengthy bit of code to create a reasonable header and secondary
	  # columns for the table files. When the data file of the problem is wrapped
	  # using the CONT data item, the rows of the table files will be duplicated (or
	  # at least, only every second or third row will hold the table output).
	  if( defined $self -> {'tables'} ){
	    for( my $i = 0; $i < scalar @{$self -> {'tables'}}; $i++ ) {
	      if( defined $self -> {'table_files'} and defined $self -> {'table_files'}[$i] ) {
# 	      my $table = $self -> {'tables'}[$i]; # $TABLE
		my $table_file = $self -> {'table_files'}[$i]; # The actual file
# 	      my $cont_column = $table_file -> cont_column;
# 	      my @options = @{$table -> options};
# 	      my @header_options;
# 	      for( my $j = 1; $j <= scalar @options; $j++ ) {
# 		my $option = $options[$j-1];
# 		my $name = $option -> name;
# 		last if( $name eq 'BY' or  $name eq 'PRINT' or $name eq 'NOPRINT' or
# 			 $name eq 'FILE' or $name eq 'NOHEADER' or $name eq 'ONEHEADER' or
# 			 $name eq 'FIRSTONLY' or $name eq 'NOFORWARD' or $name eq 'FORWARD' or
# 			 $name eq 'APPEND' or $name eq 'NOAPPEND' or $name eq 'UNCONDITIONAL' or
# 			 $name eq 'CONDITIONAL' or $name eq 'OMITTED' );
# 		push( @header_options, $option );
# 	      }
# 	      my $nopt = scalar @header_options;
# 	      my @sec;
# 	      my @header;
# 	      if( defined $cont_column and ref( $parm ) eq 'ARRAY' and scalar @{$parm} > 0 ) {
# 		for( my $k = -1; $k < scalar @{$parm}; $k++ ) {
# 		  my @in_sec;
# 		  my $pos = 1;
# 		  for( my $j = 1; $j <= $nopt; $j++ ) {
# 		    my $option = $header_options[$j-1];
# 		    my $name = $option -> name;
# 		    my $glob_pos;
# 		    if( $name ne 'CONT' ) {
# 		      $name = $k == -1 ? $name : $name.($k+1);
# 		      $glob_pos = (($nopt-1)*($k+1))+$pos++;
# 		      push( @header, $name ); 
# 		    }
# 		    next if ( $k == -1 );
# 		    my @col = ( $name, $glob_pos, $option -> value );
# 		    push( @in_sec, \@col );
# 		  }
# 		  unshift( @sec, \@in_sec );
# 		}
# 	      }
		$table_file -> header( [] );
#	      $table_file -> header( \@header );
#	      $table_file -> secondary_columns( \@sec );
	      }
	    }
	  }
	} else {
	  if ( defined $self -> pks ) {
	    $self -> {'secondary_columns'} = $self -> pks -> [0] -> secondary_columns();
	  } else {
	    $self -> {'secondary_columns'} = $self -> preds -> [0] -> secondary_columns();
	  }
	}
      }
end secondary_columns

# }}} secondary_columns

# {{{ record_count
start record_count
    {
	$return_value = 0;
	if( defined $self -> {$record_name . 's'} ){
	    foreach my $record ( @{$self -> {$record_name . 's'}} ){
		if( defined $record -> options ){
		    $return_value += @{$record -> options};
		}
	    }
	}
    }
end record_count
# }}} record_count

# {{{ restore_inits
start restore_inits
      {
	if ( defined $self -> {'thetas'} ) {
	  foreach my $theta ( @{$self -> {'thetas'}} ) {
	    $theta -> restore_inits;
	  }
	}
	if ( defined $self -> {'omegas'} ) {
	  foreach my $omega ( @{$self -> {'omegas'}} ) {
	    $omega -> restore_inits;
	  }
	}
	if ( defined $self -> {'sigmas'} ) {
	  foreach my $sigma ( @{$self -> {'sigmas'}} ) {
	    $sigma -> restore_inits;
	  }
	}
      }
end restore_inits
# }}} restore_inits

# {{{ set_random_inits
start set_random_inits
      {
	if ( defined $self -> {'thetas'} ) {
	  foreach my $theta ( @{$self -> {'thetas'}} ) {
	    $theta -> set_random_inits( degree => $degree );
	  }
	}
	if ( defined $self -> {'omegas'} ) {
	  foreach my $omega ( @{$self -> {'omegas'}} ) {
	    $omega -> set_random_inits( degree => $degree );
	  }
	}
	if ( defined $self -> {'sigmas'} ) {
	  foreach my $sigma ( @{$self -> {'sigmas'}} ) {
	    $sigma -> set_random_inits( degree => $degree );
	  }
	}
      }
end set_random_inits
# }}} set_random_inits

# {{{ set_records

start set_records
      {
	  my $rec_class = "model::problem::$type";
	  my $accessor = $type.'s';
	  if( $self -> can($accessor) ){
	    $self -> {$accessor} = [$rec_class -> new ( record_arr => \@record_strings) ];
	  } else {
	      die "Error in problem -> set_records: Trying to set unknown record: $type\n";
	  }
      }
end set_records

# }}} set_records
    
# {{{ remove_records
start remove_records
      {
	  my $rec_class = "model::problem::$type";
	  my $accessor = $type.'s';
	  if( $self -> can($accessor) ){
	      $self -> {$accessor} = undef;
	  } else {
	      die "Error in problem -> remove_records: Trying to remove unknown record: $type\n";
	  }
      }
end remove_records
# }}} remove_records

# {{{ add_option

start add_option

my $accessor = $record_name.'s';
unless( $self -> can($accessor) ){
  'debug' -> die( message => "Unknown record name: $record_name" );
}
if( defined $self -> {$accessor} ) {
  my @records = @{$self -> {$accessor}};
  foreach my $record ( @records ) {
    $record -> add_option( init_data => { name  => $option_name,
					  value => $option_value } );
  }
} else {
  if( $add_record ) {
    $self -> add_records( type           => $record_name,
			  record_strings => ["$option_name=$option_value"] );
  } else {
    'debug' ->  warn( level => 2, 
		      message => "No records of type $accessor and add_option ".
		      "set not to add one" );
  }
}

end add_option

# }}} add_option

# {{{ remove_option

start remove_option

my $accessor = $record_name.'s';
unless( $self -> can($accessor) ){
  'debug' -> die( message => "Unknown record name: $record_name" );
}
if( defined $self -> {$accessor} ) {
  my @records = @{$self -> {$accessor}};
  foreach my $record ( @records ) {
    $record -> remove_option( name => $option_name,
			      fuzzy_match => $fuzzy_match );
  }
} else {
  'debug' ->  warn( level => 2, 
		    message => "No records of type $accessor" );
}

end remove_option

# }}} remove_option

# {{{ store_inits
start store_inits
      {
	if ( defined $self -> {'thetas'} ) {
	  foreach my $theta ( @{$self -> {'thetas'}} ) {
	    $theta -> store_inits;
	  }
	}
	if ( defined $self -> {'omegas'} ) {
	  foreach my $omega ( @{$self -> {'omegas'}} ) {
	    $omega -> store_inits;
	  }
	}
	if ( defined $self -> {'sigmas'} ) {
	  foreach my $sigma ( @{$self -> {'sigmas'}} ) {
	    $sigma -> store_inits;
	  }
	}
      }
end store_inits
# }}} store_inits

# {{{ _format_problem

start _format_problem
      {
	# problem::_format_problem()

	# format_problem will return an array of strings of the
	# problem in NONMEM modelfile format.

	# Loop over the print_order array that contains strings of
	# valid record types in the order they should appear in a
	# NONMEM modelfile. So if the order of some records are
	# interchangable and the file from which the object was
	# initialized has records in an order different from
	# print_order, the file will still be valid, but will look
	# different from what it used to.
	my $record_order = $self -> {'sde'} ? \@sde_print_order : \@print_order;
	foreach my $type ( @${record_order} ) {
	  # Create an accessor string for the record being formatted
	  my $accessor = $type.'s';

	  # Se if we have one or more records of the type given in
	  # print_order
	  if ( defined $self -> {$accessor} ) {
	    # Loop over all such records and call on the record object
	    # to format itself.
	    foreach my $record ( @{$self -> {$accessor}} ){
	      push( @formatted,
		    @{$record ->
			  _format_record( nonparametric_code => $self -> {'nonparametric_code'},
					  shrinkage_code     => $self -> {'shrinkage_code'},
					  eigen_value_code   => $self -> {'eigen_value_code'} ) } );
	    }
	  }
	  if( $self -> {'shrinkage_module'} -> enabled and $type eq 'table' ) {
	    push( @formatted,
		  @{$self -> {'shrinkage_module'} -> format_shrinkage_tables } );
	  }	
	}

	if( $self -> {'cwres_modules'} ){
	  $self -> {'cwres_modules'} -> [0] -> post_process;
	}
	
      }
end _format_problem

# }}} _format_problem

# {{{ _init_attr

start _init_attr
      {
	# Private method, should preferably not be used outside model.pm
	# The add_if_absent argument tells the method to add an init (theta,omega,sigma)
	# if the parameter number points to a non-existing parameter with parameter number
	# one higher than the highest presently included. Only applicatble if
	# new_values are set. Default value = 0;
	
	my $accessor = $parameter_type.'s';
	unless( $self -> can($accessor) ){
	    'debug' -> die( message => "problem -> _init_attr: Error unknown parameter type: $parameter_type" );
	}

	my @records;
	if( defined $self -> {$accessor} ){
	  @records = @{$self -> {$accessor}};
	} else {
	  @records = ();
	}
	
	my @options = ();

	# {{{ Check that the size of parameter_numbers and new_values match
	my %num_val;
	if ( $#parameter_numbers >= 0 and $#new_values >= 0 ) {
	  if ( $#parameter_numbers == $#new_values ) {
	    for ( my $i = 0; $i <= $#new_values; $i++ ) {
	      $num_val{$parameter_numbers[$i]} = $new_values[$i];
	    }
	  } else {
	    die "Model::problem -> _init_attr: The number of specified ".
	      "parameters (@parameter_numbers) and values (@new_values) do not match for parameter $parameter_type".
		" and attribute $attribute\n";
	  }
	}
	# }}}

	my $prev_size = 1;
	if ( scalar @new_values > 0 ) {
	  # {{{ Update values

	  # OBS! We are using 'normal' numbering in parameter_numbers, i.e. they begin
	  # at one (1).
	  my $opt_num = 1;
	  # Ugly solution to add non-existing options:
	  my %found;
	  foreach my $num ( @parameter_numbers) {
#	    print "inpn: $num\n";
	    $found{$num} = 0;
	  }

	  my @diagnostics = ();
	  foreach my $record ( @records ) {
	    if ( $record -> same() ) {
	      # SAME == true: Nothing to be done. Just move forward to next $OMEGA but
	      # increase counter first

	      $opt_num += $prev_size;
	    } else {
	      foreach my $option ( @{$record -> options} ) {
		if ( scalar @parameter_numbers > 0 ) {
		  foreach my $num ( @parameter_numbers ) {
		    if ( $num == $opt_num ) {
		      $found{$num}++;
		      if ( $attribute eq 'init' ) {
			push( @diagnostics,
			      $option -> check_and_set_init( new_value => $num_val{$num} ) );
		      } elsif( $attribute eq 'fix' and defined $record -> size() ){
			# size() tells us this is a block and we must fix on record level.
			$record -> fix( $num_val{$num} );
		      } else {
			$option -> $attribute( $num_val{$num} );
		      }
		    }
		  }
		} else {
		  if ( $attribute eq 'init' ) {
		    push( @diagnostics,
			  $option -> check_and_set_init( new_value => shift( @new_values ) ) );
		  } elsif( $attribute eq 'fix' and defined $record -> size() ){
		    # size() tells us this is a block and we must fix on record level.
		    $record -> fix( shift( @new_values ) );
		  } else {
		    $option -> $attribute( shift( @new_values ) );
		  }
		}
		$opt_num++;
	      }
	      if( $parameter_type eq 'theta' ){
		$prev_size = scalar @{$record -> options};
	      } else {
		my $size = $record -> size;
		if( defined $size ) {
		  $prev_size = ($size*($size+1))/2;
		} else {
		  $prev_size = scalar @{$record -> options};
		}
	      }
	    }
	  }
	  # If $add_if_absent is set, any parameters that were not found above are
	  # added below:
	  
	  my @nums = sort {$a<=>$b} keys %found;
	  my $new_record = "model::problem::$parameter_type" -> new();
	  my $do_add_record;
	  foreach my $num ( @nums ) {
	    if ( $add_if_absent and
		 not $found{$num} ) {
	      $do_add_record = 1;
	      unless($num == $opt_num) {
		'debug' -> die( message => "Attempt to add a parameter with higher number ($num) than the number\n".
				           "of parameters + 1 ($opt_num)\n" );
	      }
	      # Get the last record of $parameter_type
	      # my $new_record = $records[$#records];
	      my $option_class;
	      if( $parameter_type eq 'theta' ){
		$option_class = 'model::problem::record::theta_option';
	      } else {
		$option_class = 'model::problem::record::init_option';
	      }

	      # Push a new option to this last record
	      my $option = $option_class -> new;
	      if ( $attribute eq 'init' ) {
		$option -> check_and_set_init( new_value => $num_val{$num} );
	      } elsif( $attribute eq 'fix' and defined $new_record -> size() ){

		# size() tells us this is a block and we must fix on
		# record level. This will never happen, as we can't
		# add BLOCKS, at least not like this.

		$new_record -> fix( $num_val{$num} );
	      } else {
		$option -> $attribute( $num_val{$num} );
	      }
	      push( @{$new_record -> {'options'}}, $option );

	      # So we've added a parameter. Possible to add more,
	      # lets increase the highest found:
	      $opt_num++;
	    }
	  }
	  if ( $attribute eq 'init' ) {
	    # We're updating but might be returning diagnostics
	    # Use the default return parameter parameter_values for this
	    @parameter_values = @diagnostics;
	  }

	  if( $do_add_record ){
	    push( @records, $new_record );
	    $self -> {$accessor} = \@records ;
	  }

	  # }}} Update values
	} else {
	  # {{{ Retrieve values

	  my @prev_values = ();
	  foreach my $record ( @records ) {
	    unless ( $record -> same() ) {
	      @prev_values = ();
	      if ( defined $record -> options ) {
		foreach my $option ( @{$record -> options} ) {
		  push( @prev_values, $option -> $attribute );
		}
	      } else {
		'debug' -> warn( level   => 1,
				 message => "Trying to get attribute $attribute, ".
				 "but no options defined in record ".ref($record) );
	      }
	      $prev_size = $record -> size unless ( $record -> same );
	    }
	    # I am not confortable with the action below, i.e. to just
	    # append the previous value of the attribute if the record
	    # is SAME. An exception is made to labels. Maybe this
	    # should be the default?
	    if( $record -> same() ) {
	      for( my $i = 0; $i <= $#prev_values; $i++ ) {
		$prev_values[$i] = undef;
	      }
	    }
	    push( @parameter_values, @prev_values );
	  }
	  
	  if ( scalar @parameter_numbers > 0 ) {
	    my @part_vals = ();
	    foreach my $num ( @parameter_numbers ) {
	      push( @part_vals, $parameter_values[$num -1] );
	    }
	    @parameter_values = @part_vals;
	  } else {
	    'debug' -> warn( level => 2, 
			     message => "Model::problem -> _init_attr: parameter_numbers undefined, using all." );
	  }
	  
	  # }}} Retrieve values
	}
      }
end _init_attr

# }}} _init_attr

# {{{ name_val

start name_val
#       {
# 	my $accessor = $parameter_type.'_name_vals';
# 	unless( $self -> can($accessor) ){
# 	    die "problem -> _init_attr: Error unknown parameter type: $parameter_type\n";
# 	}
# 	print "P: $parameter_type\n" if $self -> {'debug'};
# 	print "Model::problem -> _init_attr: parameter_type: $parameter_type\n"
# 	    if $self -> {'debug'};
# 	my @records = @{$self -> {$accessor}};
# 	my @options = ();

# 	my $prev_size = 1;	
# 	# {{{ Retrieve values
# 	foreach my $record ( @records ) {
# 	  foreach my $option ( @{$record -> options} ) {
# 	    if ( $record -> same ) {
# 	      for ( my $i = 1; $i <= $prev_size*($prev_size+1)/2 ; $i++ ) {
# 		push( @parameter_values, @{$record -> options}[0] -> $attribute );
# 	      }
# 	    } else {
# 	      push( @parameter_values, $option -> $attribute );
# 	    }
# 	  }
# 	  $prev_size = $record -> size unless ( $record -> same );
# 	}
# 	if ( $#parameter_numbers > 0 ) {
# 	  my @part_vals = ();
# 	  foreach my $num ( @parameter_numbers ) {
# 	    push( @part_vals, $parameter_values[$num -1] );
# 	  }
# 	  @parameter_values = @part_vals;
# 	} else {
# 	  print "Model::problem -> _init_attr: parameter_numbers undefined, using all.\n" if
# 	    $self -> {'debug'};
# 	}
# 	# }}} Retrieve values
#       }
end name_val

# }}} name_val

# {{{ _normalize_record_name

start _normalize_record_name
    {

      # This code takes a recordname (which likely is uppercase and
      # semilong), creates its short uppercase format and looks up the
      # long, lowercase, name in the abbreviations hash that was
      # initialized in "new". The name is assumed to be valid, if its
      # not, an empty string will be returned, but no error produced (
      # a warning might be nice though ) (Errorhandling is now done in
      # "read_records".

      my $uc_short_type = substr(uc($record_name),0,3);
      $uc_short_type = $uc_short_type.' ' if ( $record_name eq 'aes' );
      $uc_short_type = $uc_short_type.'I' if ( $record_name eq 'aesinit' );
      $normalized_name = $abbreviations{$uc_short_type};
    }
end _normalize_record_name

# }}}

# {{{ _read_records

start _read_records
    {

      # We parse the lines of a problem by looping over the them and
      # look for records(lines starting with a $). When a record is
      # found we set its index in the array as the end of the previous
      # record we found. We then know which lines to send to the
      # record object constructor. Then we set the end index of the
      # previous record as the start index of the next record. It is
      # assumed that the first record starts at line zero. The end of
      # the last record is the last line.

      my $start_index = 0;
      my $record_index = 0;
      my $end_index;
      my $first = 1;

      # It may look like the loop takes one step to much, but its a
      # trick that helps parsing the last record.
      for( my $i = 0; $i <= @{$self -> {'prob_arr'}}; $i++ ){

	# This if statement makes sure we dont access the array in the
	# last iteration of the loop. In all other iterations we need
	# a line of code to look for records starting lines.

	if( $i <= $#{$self -> {'prob_arr'}} ){
	  $_ = $self -> {'prob_arr'} -> [$i];
	}

	# In this if statement we use the lazy evaluation of logical
	# or to make sure we only execute search pattern when we have
	# a line to search. Which is all cases but the very last loop
	# iteration.

	if( $i > $#{$self -> {'prob_arr'}} or /^\s*\$(\w+)/ ){
	  $end_index = $i;

	  # The if statement here is only necessary in the first loop
	  # iteration. When start_index == end_index == 0 we want to
	  # skip to the next iteration looking for the actual end of
	  # the first record.

	  if( $end_index > $start_index and not $first){
	    # extract lines of code:
	    my @record_lines = @{$self -> {'prob_arr'}}[$start_index .. $end_index-1];
	    # extract the record name and get its long name:
	    $self -> {'prob_arr'} -> [$record_index] =~ /^\s*\$(\w+)/;
	    my $record_name = $1;
	    my $record_type = $self -> _normalize_record_name( record_name => $record_name );
	    
	    unless( length($record_type) > 0 ){
	      'debug' -> die( message => "Record $record_name is not valid" );
	    }

	    # reset the search for records by moving the record start
	    # forwards:
	    $start_index = $i;
	    
	    # let add_records create the object if appropriate

	    if( $record_type eq 'table' ) {
	      my $et_found = 0;
	      my $wr_found = 0;
	      my $eta_name  = $self -> {'shrinkage_module'} -> eta_tablename;
	      my $wres_name = $self -> {'shrinkage_module'} -> wres_tablename;
	      foreach my $row ( @record_lines ) {
		$et_found++ if( $row =~ /$eta_name/ );
		$wr_found++ if( $row =~ /$wres_name/ );
	      }
	      if( $et_found or $wr_found ) {
		$self -> {'shrinkage_module'} -> enable;
	      } else {
		$self -> add_records( record_strings => \@record_lines, 
				      type => $record_type );
	      }
	    } else {
	      $self -> add_records( record_strings => \@record_lines, 
				    type => $record_type );
	    }
	  }
	  $first = 0;
	  $record_index = $i;
	}  
      }
    }
end _read_records

# }}} _read_records

# {{{ _option_val_pos

start _option_val_pos
      {
	#
	# _option_val_pos( record_name => 'subroutine',
	#                  name => 'OTHER',
	#                  val => 'get_cov')
	#

	# _option_val_pos sets, or gets, the value of an option (given
	# as the 'name' parameter. Name must be uppercase) in a record
	# (given as the 'record_name' parameter. Record name should be
	# the record class name in the model diagram.)
	
	my $accessor = $record_name.'s';
	unless( $self -> can($accessor) ){
	  'debug' -> die( message => "Unknown record name: $record_name" );
	}

	my @records;
	if( defined $self -> {$accessor} ) {
	  @records = @{$self -> {$accessor}} ;
	} else {
	  'debug' ->  warn( level => 2, 
			    message => "No records of type $accessor" );
	  @records = ();
	}
	my @options = ();

	# {{{ Check that the size of instance_numbers and new_values match

	my %num_val;
	if ( $#instance_numbers >= 0 and $#new_values >= 0 ) {
	  if ( $#instance_numbers == $#new_values ) {
	    for ( my $i = 0; $i <= $#new_values; $i++ ) {
	      $num_val{$instance_numbers[$i]} = $new_values[$i];
	    }
	  } else {
	    'debug' -> die( message => "Model::problem -> _option_val_pos: The number of specified " .
			    "parameters " . $#instance_numbers+1 . " and values " .
			    $#new_values+1 . " do not match" );
	  }
	}

	# }}}

	if ( scalar @new_values > 0 ) {
	  # {{{ Update values

	  my $opt_num = 1;
	  foreach my $record ( @records ) {
	    foreach my $option ( @{$record -> options} ) {
	      my $test_name = $exact_match ? uc($option -> name) :
		  uc(substr($option -> name,0,length($name)));
	      if ( $test_name eq $name) {
		if ( scalar @instance_numbers > 0 ) {
		  foreach my $num ( @instance_numbers ) {
		    $option -> value( $num_val{$num} ) if $num == $opt_num;
		  }
		} else {
		  $option -> value( shift( @new_values ) );
		}
		$opt_num++;
	      }
	    }
	  }

	  # }}} Update values
	} else {
	  # {{{ Retrieve values

	  foreach my $record ( @records ) {
	    my $i = 1;
	    if ( defined $record -> options ) {
	      foreach my $option ( @{$record -> options} ) {
		my $test_name = $exact_match ? uc($option -> name) :
		  uc(substr($option -> name,0,length($name)));
		if ( $test_name eq $name) {
		  push( @values, $option -> value );
		  push( @positions, $i );
		}
		$i++;
	      }
	    }
	  }
	  if ( $#instance_numbers > 0 ) {
	    my @part_vals = ();
	    my @part_pos = ();
	    foreach my $num ( @instance_numbers ) {
	      push( @part_vals, $values[$num -1] );
	      push( @part_pos, $positions[$num -1] );
	    }
	    @values = @part_vals;
	    @positions = @part_pos;
	  } 

	  # }}} Retrieve values
	}
      }	
end _option_val_pos

# }}} _option_val_pos

# {{{ eta_shrinkage

start eta_shrinkage

@eta_shrinkage = @{$self -> {'shrinkage_module'} -> eta_shrinkage};

end eta_shrinkage

# }}} eta_shrinkage

# {{{ wres_shrinkage

start wres_shrinkage

@wres_shrinkage = @{$self -> {'shrinkage_module'} -> wres_shrinkage};

end wres_shrinkage

# }}} wres_shrinkage
