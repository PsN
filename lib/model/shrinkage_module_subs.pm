# {{{ include

start include statements

use Data::Dumper;

end include statements

# }}} include

# {{{ enable

start enable

$self -> {'enabled'} = 1;

end enable

# }}} enable

# {{{ disable

start disable

$self -> {'enabled'} = 0;

end disable

# }}} disable

# {{{ format_shrinkage_tables

start format_shrinkage_tables

my $omegas = $self -> {'problem'} -> nomegas;

my $eta_str = 'ID';
my $eps_str = 'ID IWRES EVID';

for( my $j = 1; $j <= $omegas; $j++ ) {
  $eta_str = $eta_str.' ETA'.$j;
}
$eta_str = $eta_str.' FILE='.$self -> eta_tablename.
    ' NOAPPEND ONEHEADER NOPRINT FIRSTONLY';
$eps_str = $eps_str.' FILE='.$self -> wres_tablename.
    ' NOAPPEND ONEHEADER NOPRINT'."\n";

my $eta_table = model::problem::table -> new ( record_arr => [$eta_str] );
my $eps_table = model::problem::table -> new ( record_arr => [$eps_str] );

my $wrapped = ( defined $self -> {'model'} -> datas -> [$self -> problem_number-1] -> wrap_column and
		defined $self -> {'model'} -> datas -> [$self -> problem_number-1] -> cont_column );

if( $wrapped ) {
  # No good contifying the eta table. We use FIRSTONLY which will make
  # CONT=1 on all rows and PsN does not like that.
  $eps_table -> contify;
}

@formatted = ( @{$eta_table -> _format_record},
	       @{$eps_table -> _format_record} );

end format_shrinkage_tables

# }}} format_shrinkage_tables

# {{{ eta_tablename

start eta_tablename

my $probnum = $self -> problem_number;

$filename = 'prob'.'_'.$probnum.'.psn_etas';

end eta_tablename

# }}} eta_tablename

# {{{ wres_tablename

start wres_tablename

my $probnum = $self -> problem_number;

$filename = 'prob'.'_'.$probnum.'.psn_wres';

end wres_tablename

# }}} wres_tablename

# {{{ eta_table_exists

start eta_table_exists

my $directory = $self -> {'model'} -> directory;
my $filename = $self -> eta_tablename;

$exists = -e $directory.$filename ? 1 : 0;

end eta_table_exists

# }}} eta_table_exists

# {{{ wres_table_exists

start wres_table_exists

my $directory = $self -> {'model'} -> directory;
my $filename = $self -> wres_tablename;

$exists = -e $directory.$filename ? 1 : 0;

end wres_table_exists

# }}} wres_table_exists

# {{{ problem_number

start problem_number

if( defined $self -> {'model'} -> problems ) {
  # This is the default
  my @modprobs = @{$self -> {'model'} -> problems};
  for( my $i = 0; $i <= $#modprobs; $i++ ) {
    $problem_number = ($i+1) if( $modprobs[$i] eq $self -> {'problem'} );
  }
} else {
  # This happens when the problems are not yet set in the model
  $problem_number = $self -> {'temp_problem_number'};
}

end problem_number

# }}} problem_number

# {{{ eta_shrinkage

start eta_shrinkage

# We do not handle subproblem eta shrinkage

if( $self -> {'enabled'} ) {
  my $omegas  = $self -> {'model'} -> outputs -> [0] -> omegas;
  my $probnum = $self -> problem_number - 1; # start index at 0
  my @omega_indexes;
  if( defined $self -> {'model'} -> outputs -> [0] and
      defined $self -> {'model'} -> outputs -> [0] -> problems -> [0] and
      defined $self -> {'model'} -> outputs -> [0] -> problems -> [0] -> omega_indexes ) {
    @omega_indexes = @{$self -> {'model'} ->
			   outputs -> [0] -> problems -> [0] -> omega_indexes};
  }
  my $defined_indexes = 0;
  foreach my $index ( @omega_indexes ) {
    $defined_indexes++ if defined $index;
  }

  if( defined $omegas and defined $omegas -> [$probnum] ) {
    if( scalar @{$omegas -> [$probnum]} == 1 ) { # One subprob
      if( $defined_indexes and $self -> eta_table_exists ) {
	my $sh_table = data -> new( directory            => $self -> {'model'} -> directory,
				    filename             => $self -> eta_tablename,
				    ignore_missing_files => 1,
				    target               => 'mem',
				    table_file           => 1 );
	my $diag_omega_idx = 0;
	for( my $j = 0; $j < scalar @{$omegas -> [$probnum][0]}; $j++ ) {
	  next if( $omega_indexes[$j][0] != $omega_indexes[$j][1] );
	  if ( defined $omegas -> [$probnum][0][$j] and defined $sh_table and
	       $omegas -> [$probnum][0][$j] != 0 ) {
	    my $omega = sqrt($omegas -> [$probnum][0][$j]);
	    my $eta_sd = $sh_table -> sd( column => ($diag_omega_idx+2) );
	    my $shrinkage = ($omega - $eta_sd)/$omega;
	    $eta_shrinkage[0][$diag_omega_idx] = $shrinkage;
	  } else {
	    $eta_shrinkage[0][$diag_omega_idx] = undef;
	  }
	  $diag_omega_idx++;
	}
      } else {
	$eta_shrinkage[0] = [];
      }
    } elsif( scalar @{$omegas -> [$probnum]} == 0 ) {
      debug -> die( message => $self -> {'model'} -> full_name. " Call to output->omegas is empty. PsN can not compute shrinkage." );
    } else {
      debug -> die( message => $self -> {'model'} -> full_name . " Call to output->omegas indicates that results ".
		    "exists in multiple subproblems.PsN can not yet compute shrinkage".
		    " on the subproblem level" );
    }
  }
}

end eta_shrinkage

# }}} eta_shrinkage

# {{{ wres_shrinkage

start wres_shrinkage

# We do not handle subproblem wres shrinkage

if( $self -> {'enabled'} ) {
  my $ofv  = $self -> {'model'} -> outputs -> [0] -> ofv; # Use ofv to test success

  my $probnum = $self -> problem_number - 1; # start index at 0
  if( defined $ofv ) {
    if( scalar @{$ofv -> [$probnum]} == 1 ) {
      my $sh_table;
      if( defined $ofv -> [$probnum][0] and $self -> wres_table_exists ) {
	$sh_table = data -> new( directory            => $self -> {'model'} -> directory,
				 filename             => $self -> wres_tablename,
				 ignore_missing_files => 1,
				 target               => 'mem',
				 table_file           => 1 );
      }
      if( defined $sh_table ) {
	my $wres_sd = $sh_table -> sd( column        => 2,
				       subset_column => 3,
				       subset_syntax => '==0',
				       global_sd     => 1 );
	my $shrinkage = 1 - $wres_sd;
	$wres_shrinkage[0] = $shrinkage;
      } else {
	$wres_shrinkage[0] = undef;
      }			  
    } elsif ( @{$ofv -> [$probnum]} < 1 ) {
      debug -> warn( level => 1,
		     message => "There seems to be a problem with the results from ".
		     $self -> {'model'} -> filename().". Cannot compute shrinkage." );
    } else {
      debug -> die( message => "Call to output->omegas indicates that results ".
		    "exists in multiple subproblems.PsN can not yet compute shrinkage".
		    " on the subproblem level" );
    }
  }
}

end wres_shrinkage

# }}} wres_shrinkage







