# {{{ include statements

start include statements
use model::problem::record::init_option;
end include statements

# }}} include statements

# {{{ restore_inits

start restore_inits
      {
	if ( defined $self -> options ) {
	  foreach my $option ( @{$self -> options} ){
	    $option -> restore_init;
	  }
	}
      }
end restore_inits

# }}} restore_inits

# {{{ set_random_inits
start set_random_inits
      {
	if ( defined $self -> options and not $self -> {'same'} and 
	     not $self -> {'fix'} ) {
	  foreach my $option ( @{$self -> options} ){
	    $option -> set_random_init( degree => $degree );
	  }
	}
      }
end set_random_inits
# }}} set_random_inits

# {{{ store_inits
start store_inits
      {
	if ( defined $self -> options ) {
	  foreach my $option ( @{$self -> options} ){
	    if( $option -> can( 'store_init' ) ){
	      $option -> store_init;
	    }
	  }
	}
      }
end store_inits
# }}} store_inits

# {{{ _add_option

start _add_option
      {
	my $opt_obj = 
	  model::problem::record::init_option ->
	      new ( option_string => $option_string,
		    on_diagonal   => $on_diagonal,
		    sd            => $sd,
		    corr          => $corr,
		    fix           => $fix );
	push( @{$self -> {'options'}}, $opt_obj ) if( $opt_obj ); 
      }
end _add_option

# }}} _add_option

# {{{ _format_record

start _format_record
      {
	my @class_names = split('::',ref($self));
	my $fname = uc(pop(@class_names));
	$formatted[0] = "\$".$fname." ";
	my $len;

	my $otype = $self -> {'type'};
	my $same  = $self -> {'same'};
	my $fix   = $self -> {'fix'};
	my $size  = $self -> {'size'};

	if ( defined $otype ) {
	  $formatted[0] = $formatted[0]." $otype";
	  if ( defined $size ) {
	    $formatted[0] = $formatted[0]."($size)";
	  }
	  if ( $self -> sd() ) {
	    $formatted[0] = $formatted[0]." SD";
	  }
	  if ( $self -> corr() ) {
	    $formatted[0] = $formatted[0]." CORRELATION";
	  }
	  if ( $fix) {
	    $formatted[0] = $formatted[0]." FIX";
	  }
	  if ( $same) {
	    $formatted[0] = $formatted[0]." SAME\n";
	  }
	}
	my $i = 0;
	$len = length $formatted[0];
	if ( defined $self -> {'options'} ) {
	  foreach my $option ( @{$self -> {'options'}} ) {
	    $len = 0 if $i++;
	    $formatted[0] = $formatted[0].' '.
	      $option -> _format_option( len => $len)."\n";
	  }
	} else {
	  $formatted[0] = $formatted[0]."\n";
	}

	if ( defined $self -> {'comment'} ) {
	  push( @formatted, @{$self -> {'comment'}} );
	  $formatted[$#formatted] = $formatted[$#formatted];
	}
      }
end _format_record

# }}} _format_record

# {{{ _read_options
start _read_options
      {
	my @inits = ();
	my @row   = ();
	my @digits = ();
	my @fix = ();
	my @sds = ();
	my @corrs = ();
	my @comments = ();
	my ( $any_fixed, $any_sd, $any_corr, $block_sd, $block_corr, $block_fixed ) = ( 0, 0, 0, 0, 0, 0 );
	if ( defined $self -> {'record_arr'} ) {
	  my ( $digit, $comment, $fixed, $sd, $corr ) = ( undef, undef, 0, 0, 0 );
	  for ( @{$self -> {'record_arr'}} ) {
	    chomp;
	    s/^\s+//;
	    s/\s+$//;
	    s/^\s*\$\w+//;
	    next unless( length($_) > 0 );
	    if( /^\s*\;/ ) {
	      # This is a comment row
	      push( @{$self -> {'comment'}}, $_ . "\n" );
	    } else {
	      # Make sure that the labels and units are in one string
	      s/\;\s+/\;/g;
	      # Get rid of unwanted spaces
#	      s/\s*\,\s*/\,/g;
#	      s/\s+FIX/FIX/g;
	      s/\s*\)/\)/g;
	      s/\(\s*/\(/g;
	      my ( $line, $line_comment ) = split( ";", $_, 2 );
	      $_ = $line;
	      $any_fixed++ if /FIX/;
	      $any_sd++    if /SD/;
	      $any_sd++    if /STANDARD/;
	      $any_corr++  if /CORRELATION/;
	      $self -> {'type'} = 'DIAGONAL' if /DIAG\w*/;
	      $self -> {'type'} = 'BLOCK'    if /BLOCK/;
	      $self -> {'size'}  = $2
		if s/^\s*(BLOCK|DIAG\w*)\s*\((\d+)\)\s*//;

	      # If we have a "BLOCK SAME" the regexp above wont remove
	      # 'BLOCK' and we do it here:
	      s/BLOCK//;

	      $self -> {'same'}  = 1         if s/SAME//;
	      push ( @inits, $_ );
	      while( s/\(([\w ]*)\)// ) {
		# We should (if the file is correctly formatted) only
		# find parentheses if the record is NOT of a BLOCK()
		# type. In this code we find all records code like
		# (init options) or (options init)
		my @opt = split( " ",$1 );
		my ( $digit, $fixed, $sd, $corr ) = ( undef, 0, 0, 0 );
		for ( my $i = 0; $i <= $#opt; $i++ ) {
		  if ( $opt[$i] =~ /\d+/ ) {
		    $digit = $opt[$i];
		  } elsif ( $opt[$i] =~ /FIX/ ) {
		    $fixed = 1;
		  } elsif ( $opt[$i] =~ /SD/ or $opt[$i] =~ /STANDARD/ ) {
		    $sd = 1;
		  } elsif ( $opt[$i] =~ /CORRELATION/ ) {
		    $corr = 1;
		  } else {
		    'debug' -> die( message => "init_record_subs -> _read_options: Unknown option $_" );
		  }
		}
		if ( defined $digit ) {
		  $self -> _add_option( option_string => $digit,
					fix           => $fixed,
					sd            => $sd,
					corr          => $corr,
					on_diagonal   => 1 );
		}
	      }
	      @row = split( " ", $_ );
	      for ( my $i = 0; $i <= $#row; $i++ ) {
		# In this code we find all records coded like: init options init options ...
		$_ = $row[$i];
		if ( /\d+/ ) {
		  if ( defined $digit ) {
		    push( @digits, $digit );
		    push( @fix   , $fixed );
		    push( @sds    , $sd );
		    push( @corrs  , $corr );
		    push( @comments, $comment );
		  }
		  ( $fixed, $sd, $corr ) = ( 0, 0, 0 );
		  $digit = $_;
		} elsif ( /FIX/ and not $fixed ) {
		  $fixed = 1;
		} elsif ( /SD/ or /STANDARD/ ) {
		  $sd = 1;
		} elsif ( /CORRELATION/ ) {
		  $corr = 1;
		} else {
		  'debug' -> die( message => "init_record_subs -> _read_options: Unknown option $_" );
		}
		$comment = $i == $#row ? $line_comment : undef;
	      }
	    }
	  }
	  if ( defined $digit ) {
	    push( @digits, $digit );
	    push( @fix   , $fixed );
	    push( @sds    , $sd );
	    push( @corrs  , $corr );
	    push( @comments, $comment );
	  }
	}

	if( $self -> {'type'} eq 'BLOCK' ) {
	  $self -> fix(1)  if ($any_fixed);
	  $self -> sd(1)   if ($any_sd);
	  $self -> corr(1) if ($any_corr);
	}

	my $row = 1;
	for ( my $i = 0; $i <= $#digits; $i++ ) {
	  my $com_str = defined $comments[$i] ? ';'.$comments[$i] : '';
	  if ( $self -> {'type'} eq 'BLOCK' ) {
	    if ( $i+1 == $row*($row+1)/2 ) {
	      $row++;
	      $self -> _add_option( option_string => $digits[$i].$com_str,
				    on_diagonal   => 1 );
	    } else {
	      $self -> _add_option( option_string => $digits[$i].$com_str,
				    on_diagonal   => 0 );
	    }
	  } else {
	    $self -> _add_option( option_string => $digits[$i].$com_str,
				  fix           => $fix[$i],
				  sd            => $sds[$i],
				  corr          => $corrs[$i],
				  on_diagonal   => 1 );
	  }
	}

#	if ( defined $self -> {'record_arr'} ) {
#	  for ( @{$self -> {'record_arr'}} ) {
# 	for ( @inits ) {
# 	  next unless( length($_) > 0 );
# 	  # Split inits and labels/units
# 	  my ( $line, $comment ) = split( ";", $_, 2 );
# 	  # Split the init string to see if we have more than one init.
# 	  @row = split( " ",$line );
# 	  if ( $#row <=0 ) {
# 	    # If we only have one init, send the whole row to option
# 	    $self -> _add_option( option_string => $_ );
# 	    print "LINE: $_\n";
# 	    print "adding one init\n";
# 	    # if ( $self -> {'debug'} );
# 	  } else {
# 	    # If we have more than one init, send one init at a time to option
# 	    print "LINE: $_\n";
# 	    print "adding ",$#row+1," inits\n";
# 	    # if ( $self -> {'debug'} );
# 	    for ( @row ) {
# 	      $self -> _add_option( option_string => $_ );
# 	    }
# 	  }
# 	}
      }
end _read_options
# }}} _read_options

