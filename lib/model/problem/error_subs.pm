start new
end new

start _format_record
      {
	if ( defined $self -> {'verbatim_first'}
	     or defined $self -> {'code'}
	     or defined $self -> {'verbatim_last'} ) {
	  my @class_names = split('::',ref($self));
	  my $fname = uc(pop(@class_names));
	  @formatted = "\$".$fname;
	}
	if ( defined $self -> {'verbatim_first'} ) {
	  push( @formatted, '"FIRST' );
	  push( @formatted, @{$self -> {'verbatim_first'}} );
	}
	my @cont;
	if ( defined $self -> {'secondary_columns'} ) {
	  for ( my $i = 0; $i < scalar @{$self -> {'secondary_columns'}}; $i++ ) {
	    my $k = 1;
	    for ( my $j = 0; $j < scalar @{$self -> {'secondary_columns'}[$i]}; $j++ ) {
	      if ( $self -> {'secondary_columns'}[$i][$j][0] eq 'ID' or
		   $self -> {'secondary_columns'}[$i][$j][0] eq 'CONT' ) {
		$k++;
		next;
	      }
	      next if ( $self -> {'secondary_columns'}[$i][$j][0] =~ /XX(\d+)/ );
	      my $str = "\" ".$self -> {'secondary_columns'}[$i][$j][0].'1 = EVTREC('.($i+1).
		  ','.($k++).")\n";
	      $str = $str.$self -> {'secondary_columns'}[$i][$j][0].' = '.
		  $self -> {'secondary_columns'}[$i][$j][0].'1'."\n";
	      push( @cont, $str );
	    }
	  }
	}

	push( @formatted, @cont );
	if ( defined $self -> {'code'} ) {
	  push( @formatted, @{$self -> {'code'}} );
	}
	if ( defined $self -> {'verbatim_last'} ) {
	  push( @formatted, '"LAST' );
	  push( @formatted, @{$self -> {'verbatim_last'}} );
	}
      }
end _format_record

