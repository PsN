start new
{
  foreach my $option ( @{$this -> {'options'}} ) {
    if ( defined $option and $option -> name eq 'IGNORE') {
      my $value = $option -> value;
      chomp( $value );
      if ( $value =~ /\(*.\)/ ) {
	$value =~ s/\(//g;
	$value =~ s/\)//g;
	my @raw_list = split(',',$value);
	push( @{$this -> {'ignore_list'}}, @raw_list );
      } else {
	$this -> {'ignoresign'} = $value;
      }
    }
  }
#  print "IGS: ",$this -> {'ignoresign'},"\n";
#  print "IGL: ",@{$this -> {'ignore_list'}},"\n";
}
end new

