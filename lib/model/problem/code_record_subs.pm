# {{{ _format_record

start _format_record
      {
	if ( defined $self -> {'verbatim_first'}
	     or defined $self -> {'code'}
	     or defined $self -> {'verbatim_last'} ) {
	  my @class_names = split('::',ref($self));
	  my $fname = uc(pop(@class_names));
	  @formatted = "\$".$fname;
	}

	if ( defined $self -> {'verbatim_first'} ) {
	  push( @formatted, '"FIRST' );
	  push( @formatted, @{$self -> {'verbatim_first'}} );
	}
	if ( defined $self -> {'code'} ) {
	  push( @formatted, @{$self -> {'code'}} );
	}
	if ( defined $self -> {'verbatim_last'} ) {
	  push( @formatted, '"LAST' );
	  push( @formatted, @{$self -> {'verbatim_last'}} );
	}
      }
end _format_record

# }}} _format_record

# {{{ _read_options

start _read_options
      {
	my $in = 0;
	if ( defined $self -> {'record_arr'} ) {
	  @{$self -> {'code'}} = ();
	  my ( $first, $last ) = ( 0, 0 );
	  for ( @{$self -> {'record_arr'}} ) {
	    # Get rid of $RECORD and unwanted spaces
	    s/^\s*\$\w+//;
	    if ( /\" (\w+) = EVTREC\((\d+),(\d+)\)/ ) {
	      $self -> {'secondary_columns'}[$2][$3][0] = $1;
	      next;
	    }
	    if( /^\"\s+FIRST/ ) {
	      $first = 1;
	      next;
	    }
	    if( /^\"\s+LAST/ ) {
	      $first = 0;
	      $last  = 1;
	      next;
	    }
	    if( $first or $last ) {
	      if( /^\"/ ) {
		if( $first ) {
		  push( @{$self -> {'verbatim_first'}},$_ );
		} else {
		  push( @{$self -> {'verbatim_last'}},$_ );
		}		  
	      } else {
		$first = 0;
		$last  = 0;
		push @{$self -> {'code'}},$_;
	      }
	    } else {
	      push @{$self -> {'code'}},$_;
	    }
	  }
	}
      }
end _read_options

# }}} _read_options


