start new
      {
	if ( defined $this -> {'option_string'} ) {
	  $this -> _read_option;
	  delete $this -> {'option_string'};
	}
      }
end new

start _read_option
      {
	my $line = $self -> {'option_string'};
	chomp( $line );
	$line =~ s/^\s+//;
	$line =~ s/\s+$//;
	my @option = split( "=", $line );
	$self -> {'name'} = shift( @option );
	$self -> {'value'} = join( "=",@option );

      }
end _read_option

start _format_option
      {
	$formatted = $self -> {'name'};
	if ( defined $self -> {'value'} and $self -> {'value'} ne '' ) {
#	  if( $self -> {'name'} eq 'FILE' or
#	      $self -> {'name'} eq 'MSFO' or
#	      $self -> {'name'} eq 'MSFI' ) {
#	    $formatted = $formatted.'='."\"".$self -> {'value'}."\"";
#	  } else {
	    $formatted = $formatted.'='.$self -> {'value'};
#	  }
	}
      }
end _format_option
