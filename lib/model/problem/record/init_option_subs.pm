# {{{ new
start new
      {
	if ( defined $this -> {'on_diagonal'} ) {
	  # on_diagonal is only defined for omegas and sigmas
	  # The default lower boundary value is 0. For off-diagonal
	  # elements this must be changed to undef.
	  if ( not $this -> {'on_diagonal'} ) {
	    $this -> {'lobnd'} = undef;
	  }
	}
      }
end new
#}}}

# {{{ include
start include statements
      {
	use Math::Random;
      }
end include statements
# }}}

# {{{ check_and_set_init

start check_and_set_init
      {
	# Error codes:
	# [0,0,0]:    Success
	# [1,0,0]:    Value truncated
	# [0,1,0]:    Value below lower boundary
	# [0,0,1]:    Value above upper boundary

	if ( defined $new_value ) {
	  $success = 1;
	  $new_value = sprintf("%.6f",$new_value);
	  if ( length( $new_value ) > 8 ) {
	    debug -> warn( level   => 1,
			   message => $new_value." is too long, using the truncated ".
			   substr($new_value,0,8)." instead" );
	    $new_value = substr($new_value,0,8);
	    $error_code[1] = 1;
	  }
	  if ( defined $self -> {'lobnd'} and
	       $new_value <= $self -> {'lobnd'} ) {
	    $success = 0;
	    $error_code[2] = 1;
	  }
	  if ( defined $self -> {'upbnd'} and
	       $new_value >= $self -> {'upbnd'} ) {
	    $success = 0;
	    $error_code[3] = 1;
	  }
	  if ( $success ) {
	    $self -> {'init'} = $new_value;
	  }
	}
      }
end check_and_set_init

# }}} check_and_set_init

# {{{ _read_option

start _read_option
      {
	my $optstr = $self -> {'option_string'};

	## Find fix unless it's already defined

	unless ( defined $self -> {'fix'} ) {
	  $self -> {'fix'} = $optstr =~ s/FIX\w*// ? 1 : 0;
	}

	## Split initials from comments
	my ($init,$comment)  = split ";",$optstr,2;

	## Split and store names and units
	my ($label,$unit)     = split ";",$comment,2 if $comment;
	chomp $label if $label;
	chomp $unit if $unit;
	$self -> {'label'} = $label;
	$self -> {'unit'}  = $unit;

	## Should only be one value now
	$init =~ s/\(//  ;
	$init =~ s/\)//  ;
	$init =~ s/\s*//g;
	$self -> {'init'} = $init;
      }
end _read_option

# }}} _read_option

# {{{ _format_option

start _format_option
      {
	my $init = $self -> {'init'};
	my $label= $self -> {'label'};
	my $unit = $self -> {'unit'};
	my $fix  = $self -> {'fix'};

	$formatted = "";
	my $str2   = "";
	my $str3   = "";
	# If FIX
	  $init =~ s/\s*//g;
	if ( defined $self -> init() ) {
	  $formatted = "$init";
	  if ( $self -> fix() ) {
	    $formatted = $formatted.sprintf("%5s",'FIX');
	  }
	  if ( $self -> sd() ) {
	    $formatted = $formatted.sprintf("%4s",'SD');
	  }
	  if ( $self -> corr() ) {
	    $formatted = $formatted.sprintf("%13s",'CORRELATION');
	  }
	} else {
	  $formatted = "";
	}

	## Pad with spaces

	if ( defined $label or defined $unit ) {
	  $str2 = "$label" if defined $label;
	  $str2 = "  ; ".sprintf("%10s",$str2) if defined $label;
	}
	if ( defined $unit ) {
	  $str3 = "  ; ".sprintf("%10s",$unit);
	}
	$formatted = $formatted.$str2.$str3;
      }
end _format_option

# }}} _format_option

# {{{ store_init
start store_init
      {
	$self -> {'stored_init'} = $self -> init;
      }
end store_init
# }}} store_init

# {{{ restore_init
start restore_init
      {
	$self -> {'init'} = $self -> {'stored_init'}
	if ( defined $self -> {'stored_init'} );
      }
end restore_init
# }}} restore_init

# {{{ set_random_init

start set_random_init
      {
	  # The initial estimate perturbation algorithm is quite
	  # important so here is the actual code of the method. Degree
	  # is 0.1*n where n is the retry number.
	  #
	  #   my ( $sign, $est, $form );						  
	  #   unless ( $self -> {'fix'} ) {						  
	  #     my $lobnd = $self -> {'lobnd'};					  
	  #     my $upbnd = $self -> {'upbnd'};					  
	  #     my $init  = $self -> {'init'};					  
	  #     if ( defined $lobnd ) {						  
	  #       $lobnd = $init-$degree*$init < $lobnd ? $lobnd : $init-$degree*$init; 
	  #     } else {								  
	  #       $lobnd = $init-$degree*$init;					  
	  #     }									  
	  #     if ( defined $upbnd ) {						  
	  #       $upbnd = $init+$degree*$init > $upbnd ? $upbnd : $init+$degree*$init; 
	  #     } else {								  
	  #       $upbnd = $init+$degree*$init;					  
	  #     }									  
	  #     $lobnd = 0.01 if ( ( $lobnd < 0.01 and $lobnd > -0.01)		  
	  #   		     and $upbnd >= 0.01001 );				  
	  #     $upbnd = -0.01 if ( ( $upbnd < 0.01 and $upbnd > -0.01)		  
	  #   		      and $lobnd <= -0.01001 );				  
	  #     									  
	  #     if ( $lobnd <= -0.01 and $upbnd >= 0.01 ) {				  
	  #       $est   = random_uniform(1)*( $upbnd - $lobnd - 0.02) + $lobnd;	  
	  #       $est   = $est - 0.02 if ( $est <0.01 );				  
	  #     } else {								  
	  #       $est   = random_uniform(1)*( $upbnd - $lobnd ) + $lobnd;		  
	  #     }									  
	  #     $form  = "%6.4f" if $est < 1000 and $est > -999;			  
	  #     $form  = "%6.1f" if $est >= 1000 or $est <=-999;			  
	  #     $self -> {'init'} = sprintf $form,$est;				  
	  #     if ($self->{'init'}==0)						  
	  #      { $self->{'init'}='0.0001'; }					  
	  #   } else {								  
	  #     print "Init is FIXED, leaving it unchanged\n"				  
	  #         if ( $self -> {'debug'});						  
	  #   }                                                                         

	my ( $sign, $est, $form );
	unless ( $self -> {'fix'} ) {
	  my $lobnd = $self -> {'lobnd'};
	  my $upbnd = $self -> {'upbnd'};
	  my $init  = $self -> {'init'};
	  my $change = abs($degree*$init);

	  if ( defined $lobnd ) {
	    $lobnd = $init-$change < $lobnd ? $lobnd : $init-$change;
	  } else {
	    $lobnd = $init-$change;
	  }
	  if ( defined $upbnd ) {
	    $upbnd = $init+$change > $upbnd ? $upbnd : $init+$change;
	  } else {
	    $upbnd = $init+$change;
	  }
	  $lobnd = 0.01 if ( ( $lobnd < 0.01 and $lobnd > -0.01)
			     and $upbnd >= 0.01001 );
	  $upbnd = -0.01 if ( ( $upbnd < 0.01 and $upbnd > -0.01)
			      and $lobnd <= -0.01001 );
	  
	  
	  if ( $lobnd <= -0.01 and $upbnd >= 0.01 ) {
	    $est = random_uniform(1, $lobnd + 0.02, $upbnd);
	    $est = $est - 0.02 if ( $est <0.01 );
	  } else {
	    $est = random_uniform(1, $lobnd, $upbnd );
	  }
	  $form  = "%6.4f" if $est < 1000 and $est > -999;
	  $form  = "%6.1f" if $est >= 1000 or $est <=-999;
	  $self -> {'init'} = sprintf $form,$est;
	  if ($self->{'init'}==0) { 
	    $self->{'init'}='0.0001';
	  }
	} else {
	  'debug' -> warn( level => 2,
			   message => "Init is FIXED, leaving it unchanged" );
	}                                                                         
      }
end set_random_init

# }}} set_random_init
