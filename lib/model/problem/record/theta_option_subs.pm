start _read_option
      {
	my ( $line, $comment ) =
	  split( ";", $self -> {'option_string'}, 2 );

	## Split and store labels and units
	if ( defined $comment ) {
	  my ($label,$unit) = split( ";",$comment,2 );
	  chomp $label if $label;
	  chomp $unit if $unit;
	  $self -> {'label'} = $label;
	  $self -> {'unit'}  = $unit;
	}

	# $line should be one theta now
	chomp( $line );
	$line =~ s/\)//g;
	$line =~ s/\(//g;
	$line =~ s/\s+//g;

	## Find fix
	$self -> {'fix'} = $line =~ s/FIX\w*// ? 1 : 0;

	my @inits = split( ",", $line );
	if ( $#inits <= 0) {
	  $self -> {'init'}  = $inits[0];
	  $self -> {'lobnd'} = undef;
	  $self -> {'upbnd'} = undef;
	} else {
	  $self -> {'lobnd'} = $inits[0];
	  $self -> {'init'}  = $inits[1];
	  $self -> {'upbnd'} = $inits[2];
	}
	
	if( defined $self -> {'lobnd'} and $self -> {'lobnd'} =~ /INF/ ){
#	  use Data::Dumper;
#	  print Dumper $PsN::config;
	  $self -> {'lobnd'} = $PsN::config -> {'low_INF'};
#	  print $self -> {'lobnd'};
	}

	if( defined $self -> {'upbnd'} and $self -> {'upbnd'} =~ /INF/ ){
	  $self -> {'upbnd'} = $PsN::config -> {'high_INF'};
	}
      }
#        print 'Found lobnd: ',$self -> {'lobnd'},
#	  ', init: ',$self -> {'init'},
#	    ', upbnd: ',$self -> {'upbnd'},
#	      ', label: ',$self -> {'label'},
#		', unit: ',$self -> {'unit'},"\n"
#		  if ( $self -> {'debug'} );
end _read_option

start _format_option
      {
	my $to_long = 0;
	my $modifier = 0;
	my $init = $self -> {'init'};
	if( length($init) > 8 ){
	    if( $init < 1 && $init > -1 ){
		if( length($init) - 2 > 8 ){
		    $to_long = 1;		    
		} else {
		    $init =~ s/0\././;
		}
	    } else {
		$to_long = 1;
		$init = 0.0001;
	    }
	}
	if( $to_long ){
	    debug -> warn( level => 1,
			   message => "Warning: initial theta value: \"".$self -> {'init'}."\"is to loong, replacing with 0.0001" );
	}

	if ( ( defined $self -> {'upbnd'} ) or
	     ( defined $self -> {'lobnd'} ) ) {
	  $formatted = $formatted."(".
	    $self -> {'lobnd'}.",";
	  $formatted = $formatted.$init;
#	  $formatted = $formatted.sprintf("%6f",$init);
#.$init;
	  $formatted = $formatted.",".$self -> {'upbnd'}
	    if ( defined $self -> {'upbnd'} );
	  $formatted = $formatted.")";
	} else {
#	  $formatted = sprintf("%6f",$init);
	  $formatted = $formatted.$init;
	}

	if ( $self -> {'fix'} ) {
	  $formatted = $formatted.sprintf("%5s",'FIX');
#	  $formatted = $formatted." "x(30-length($formatted));
#	  $formatted = $formatted.' FIX ';
	} else {
	  $formatted = $formatted."     ";
	}

#	$formatted = $formatted." "x(40-length($formatted));
	$formatted = $formatted."  ; "
	    if ( ( defined $self -> {'label'} ) or
		 ( defined $self -> {'unit'} ) );
	if ( defined $self -> {'label'} ) {
	  $formatted = $formatted.sprintf("%15.15s",$self -> {'label'});
	}

	if ( defined $self -> {'unit'} ) {
	  $formatted = $formatted."  ;".sprintf("%15.15s",$self -> {'unit'});
#	  $formatted = $formatted." "x(57-length($formatted)).";  ";
#	  $formatted = $formatted.$self -> {'unit'};
	}

#	$formatted = $formatted." "x(64-length($formatted))."\n";
      }
end _format_option
