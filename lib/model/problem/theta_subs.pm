# {{{ include

start include statements
        use model::problem::record::theta_option;
end include statements

# }}} include statements

# {{{ _add_option
start _add_option
      {
	push( @{$self -> {'options'}},
	      model::problem::record::theta_option ->
	      new ( option_string => $option_string ) );
      }
end _add_option
# }}} _add_option

# {{{ _read_options

start _read_options
      {
	$self -> {'same'} = 0;
	my @row      = ();
	@{$self -> {'comment'}} = ();
	if ( defined $self -> {'record_arr'} ) {
	  for ( @{$self -> {'record_arr'}} ) {
	    chomp;
	    s/^\s+//;
	    s/\s+$//;
	    # get rid of $THETA
	    s/^\s*\$\w+//;
	    next unless( length($_) > 0 );

	    if ( /^\s*\;/ ) {
	      # This is a comment row
	      push( @{$self -> {'comment'}}, $_."\n" );
	    } else {
	      # Make sure that the labels and units are in one string
	      s/\;\s+/\;/g;

	      # Get rid of unwanted spaces
	      s/\s*\,\s*/\,/g;
	      s/\s+FIX/FIX/g;
	      s/\s+\)/\)/g;
	      s/\(\s+/\(/g;

	      # Split thetas and labels/units
	      my ( $line, $comment ) = split( ";", $_, 2 );

	      # Split the theta string to see if we have more than one theta.
	      @row = split( " ",$line );
	      if ( $#row <=0 ) {
		# If we only have one theta, send the whole row to option
		push( @{$self -> {'options'}},
		      model::problem::record::theta_option ->
		      new ( option_string => $_ ));
	      } else {
		# If we have more than one theta, send one theta at a time to option
		for ( @row ) {
		  push( @{$self -> {'options'}},
			model::problem::record::theta_option ->
			new ( option_string => $_ ));
# 		  foreach my $option ( @{$self->{'options'}}){
# 		    print "Option class: ",ref($option),"\n";
# 		    print "INIT: ", $option -> init, "\n";
# 		  }
		}
	      }
	    }
	  }
	}
      }
end _read_options

# }}} _read_options
