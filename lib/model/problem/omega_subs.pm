start _add_option
      {
	push( @{$self -> {'options'}},
	      model::problem::record::omega_option ->
	      new ( option_string => $option_string,
		    fix           => $fix,
		    sd            => $sd,
		    corr          => $corr,
		    on_diagonal   => $on_diagonal ) );
      }
end _add_option
