# {{{ include statements

start include statements
if( $0 =~ /nonmem.pm$/ ) {
  use FindBin qw($Bin);
  use lib "$Bin";
}
use File::Copy 'cp';
#use ext::Config::Tiny;
use ext::IPC::Run3;
use Config;
if( $0 =~ /nonmem.pm$/ ) {
  require PsN;
  my $nonmem = nonmem -> new( 'modelfile'       => $ARGV[0],
			      'outputfile'      => $ARGV[1],
			      'nice'            => $ARGV[2], 
			      'version'         => $ARGV[3],
			      'fsubs'           => [split( /,/ , $ARGV[6] )],
			      'nm_directory'    => $ARGV[7],
			      'show_version'    => 0);

  my $compile = $ARGV[4];
  my $execute = $ARGV[5];

  if( $compile ){
    unless( $nonmem -> compile() ){
      debug -> die( message => $nonmem -> error_message );
    }
  }

  if( $execute ){
    $nonmem -> execute;
  }
}
end include statements

# }}}

# {{{ new

start new
    {
      unless( defined $this -> {'outputfile'} ){
	$this -> {'outputfile'} = $this -> {'modelfile'};
	$this -> {'outputfile'} =~ s/\.mod$/\.lst/;
      }

      unless( defined $this -> {'nm_directory'} ){
	my ($nmdir,$version) = split(/,/ , $PsN::config -> { 'nm_versions' } -> { $this -> {'version'} });

	unless( defined $nmdir ){
	  print "Unknown NONMEM version ",$this -> {'version'}," specified.\n";
	  my @nmkeys = keys %{$PsN::config -> { 'nm_versions' }};
	  if ( $#nmkeys == 0 and
	       defined $nmkeys[0] and
	       defined $PsN::config -> { 'nm_versions' } -> {$nmkeys[0]} ) {
	    #print "Using ",$PsN::config -> { 'nm_versions' } -> {$nmkeys[0]}," instead\n";
	    my( $nmdir, $version ) = split(/,/,$PsN::config -> { 'nm_versions' } -> {$nmkeys[0]});;
	  }
	}

	$this -> {'nm_directory'} = $nmdir;

	unless( defined $PsN::config -> {'compiler'} and defined $PsN::config -> {'compiler'} -> {'name'} ){
	  debug -> warn( level => 1,
			 message => "No compiler defined, assuming g77" );
	  $this -> {'compiler'} = 'g77';
	} else {
	  $this -> {'compiler'} = $PsN::config -> {'compiler'} -> {'name'};
	  $this -> {'compiler_options'} = $PsN::config -> {'compiler'} -> {'options'};
	}
      }
    }
end new

# }}}

# {{{ compile

start compile
    { 
      my $version = $self -> {'version'};
      my $nmdir = $self -> {'nm_directory'};
      my $includes;
      my $nmlink;
      my $adaptive = $self -> {'adaptive'} ? '_adaptive' : '';
      if( -e "$nmdir/util/nmlink5.exe" ){
	$nmlink = "$nmdir/util/nmlink5.exe";
	$self -> {'version'} = 5;
	if( $self -> {'show_version'} ){
	  $version = $self -> {'version'};
	} else {
	  $version = '';
	}
      } elsif ( -e "$nmdir/util/nmlink6.exe" ){
	$self -> {'version'} = 6;
	if( $self -> {'show_version'} ){
	  $version = $self -> {'version'};
	} else {
	  $version = '';
	}
	$nmlink = "$nmdir/util/nmlink6.exe";
	if( $Config{osname} eq 'MSWin32' ){
	  $includes = "-I$nmdir\\sizes";
	} else {
	  $includes = "-I$nmdir/sizes";
	}
      } else {
	my $err_version = ( defined $nmdir and $nmdir ne '' ) ? $nmdir : '[not configured]';
	debug -> die( message => "Unable to find a supported version of NONMEM\n".
		      "The NONMEM installation directory is $err_version for version [".
		      $self -> {'version'}."] according to psn.conf." );
      } 

      # first clean up from old compile
      unlink( 'FLIB','FCON', 'FDATA', 'FREPORT','FSUBS', 'FSUBS.f','LINK.LNK','FSTREAM', 'PRDERR', 'nonmem.exe', 'nonmem', 'nonmem5', 'nonmem6', 'nonmem5_adaptive', 'nonmem6_adaptive' );
      
      my $nm;
      
      if( $Config{osname} eq 'MSWin32' ){
	$nm="$nmdir\\nm";
      } else {
	$nm="$nmdir/nm";
      }
      
      my $modelfile = $self -> {'modelfile'};

      if( $Config{osname} eq 'MSWin32' ){
	run3( "$nmdir/tr/nmtran.exe", $modelfile, \$self -> {'nmtran_message'}, \$self -> {'nmtran_message'} );
      } else {
	run3( 'nice -' . $self -> {'nice'} . " $nmdir/tr/nmtran.exe", $modelfile, \$self -> {'nmtran_message'}, \$self -> {'nmtran_message'} );
      }

      open( NMMSG, '>compilation_output.txt' );
      print( NMMSG $self -> {'nmtran_message'}, "\n" );
            
      unless(-e 'FREPORT'){
	close(NMMSG);
	$self -> {'error_message'} = "NMtran failed: \n" . $self -> {'nmtran_message'} ;
	return 0;
      }
	     
      my $nmlink_message;
      run3( "$nmlink", undef, \$nmlink_message, \$nmlink_message );

      print( NMMSG $nmlink_message, "\n" );
      
      my $fsub;

      if(-e 'FSUBS'){ 
	if( $self -> {'compiler'} eq 'g77' or $self -> {'compiler'} eq 'g95' ){
	  rename("FSUBS", "FSUBS.f");
	  $fsub='FSUBS.f';
	} else {
	  rename("FSUBS", "FSUBS.for");
	  $fsub='FSUBS.for';
	}
      }

      if( defined $self -> {'fsubs'} and $self -> {'version'} != 6 ){
	foreach my $sub ( @{$self -> {'fsubs'}} ){
	  $fsub .= " $sub";
	}
      }
      
      open( FH, "<", 'LINK.LNK' );
      my @link;
      while( <FH> ){
	my $tmp = $_;
	$tmp =~ s/^\s+//;
	$tmp =~ s/\s+$//; 
	push(@link, $tmp);
      }
      close( FH );

      my $compile_message;
      my $compile_command;

      my @nmlib;
      
      if( $Config{osname} eq 'MSWin32' ){
	@nmlib = ("$nm\\NONMEM.obj","$nm\\BLKDAT.obj","$nm\\nonmem.lib");
      } else {
	@nmlib = ("$nm/NONMEM.o", "$nm/BLKDAT.o", "$nm/nonmem.a");
      }
      $compile_command = "g77 " . $self -> {'compiler_options'} . " -ononmem$version$adaptive $includes $fsub @link @nmlib 2>&1";
      
      if( $self -> {'compiler'} eq 'g77' ){

	$compile_command = "g77 " . $self -> {'compiler_options'} . " -ononmem$version$adaptive $includes $fsub @link @nmlib 2>&1";

      } elsif( $self -> {'compiler'} eq 'g95' ){

	$compile_command = "g95 " . $self -> {'compiler_options'} . " -ononmem$version$adaptive $includes $fsub @link @nmlib 2>&1";

      } elsif( $self -> {'compiler'} eq 'df' or $self -> {'compiler'} eq 'fl32' ){
	$compile_command = $self -> {'compiler'}." " . $self -> {'compiler_options'} . " /Fenonmem $fsub @nmlib @link";

      } elsif( $self -> {'compiler'} eq 'ifort' ){
 
	$compile_command = "ifort " . $self -> {'compiler_options'} . " -ononmem$version$adaptive $includes $fsub @link @nmlib 2>&1";

      }

      run3( "$compile_command", undef, \$compile_message, \$compile_message );

      print( NMMSG $compile_message, "\n" );
      
      close( NMMSG );
      
      unless(-e "nonmem.exe" or -e "nonmem$version$adaptive" ) {
	$self -> {'error_message'} = "Fortran Compilation failed: \n" . $compile_message ;
	return 0;
      }
    }
end compile

# }}}

# {{{ execute

start execute
    {
      my $version = $self -> {'version'};
      my $nmdir = $self -> {'nm_directory'};
      my $outputfile = $self -> {'outputfile'};
      my $adaptive = $self -> {'adaptive'} ? '_adaptive' : '';

      my ( $start_time, $fin_time );
      if( $Config{osname} eq 'MSWin32' ){
	$start_time = `date /T`;
	chomp($start_time);
	$start_time = $start_time.' '.`time /T`;
	run3( "nonmem.exe", undef, \undef );
	$fin_time = `date /T`;
	chomp($fin_time);
	$fin_time = $fin_time.' '.`time /T`;
	if ( -e "output" ) {
	  cp( "output", $outputfile );
	  unlink( "output" );
	} elsif ( -e "OUTPUT" ) {
	  cp( "OUTPUT", $outputfile );
	  unlink( "OUTPUT" );
	}
      } else {
	unless( $self -> {'show_version'} ){
	  $version = '';
	} 
	$start_time = `date`;
	if ( -e "/proc/self/lock" ) {
	  open (OUTFILE,">/proc/self/lock") || die "Could not unlock myself!\n";
	  print OUTFILE "0";
	  close (OUTFILE);
	}
	run3( "nice -n " . $self -> {'nice'} . " ./nonmem$version$adaptive", "FCON", $outputfile );
	if ( -e "/proc/self/lock" ) {
	  open (OUTFILE,">/proc/self/lock") || die "Could not lock myself!\n";
	  print OUTFILE "1";
	  close (OUTFILE);
	}
	$fin_time = `date`;
      } 
      open( OUT, '>>', $outputfile );
      print( OUT  "This file was created using the NONMEM version in directory $nmdir\n" );
      print( OUT "Started  $start_time" );
      print( OUT "Finished $fin_time" );
      close( OUT );
    }
end execute

# }}}
