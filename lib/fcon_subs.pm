# 2004-12-08: FCON must be adjusted for multiple problems /Lasse

start include statements
# A Perl module for parsing and manipulating NONMEM FCON files
#use Math::Random;
#use ui;
end include statements

# {{{ _get_array
start _get_array
{
    my $items_collected = 0;
    while( $items_collected < $nr_items ){
	my $items_per_line = ($nr_items - $items_collected) < (72 / $tabSize) ? ($nr_items - $items_collected): (72 / $tabSize);

	my $line = $fcon[$index++];

	# strip head
	$line =~ s/^.{8}//;

	for( ; $items_per_line > 0; $items_per_line -- ){
	    $line =~ s/^(.{$tabSize})//;
	    my $val = $1;
	    unless( $val =~ /[\ ]{8}/ ){ # Check for blanks
		$val = $val * 1; # Cheeky trick to make $val a number instead of a string
		push(@values, $val);
	    }
	    $items_collected ++;
	}
    }
    push(@values, $index-1);
}
end _get_array
# }}}

# {{{ _skip_lines
start _skip_lines
{
    while( not $fcon[$index+1] =~ /^[A-Z]{4}[\ ]{3}/ ){
	$index++;
	if( $index > $#fcon ){
	    last;
	}
    }
}
end _skip_lines
# }}}

# {{{ run

start run
{
    for( my $i = $batchStart; $i <  $batchStart+$batchSize; $i++ ){
	for( my $try = 0; $try < $retries[$i - $batchStart]; $try++ ){
	    system( "./NM_run".($i+1) . '_nonmem.sh' ) == 0 or die "Unable to execute NM_run".($i+1) . "_nonmem.sh $!\n";
	    open( my $outfile, "<NM_run".($i+1) . '_psn.lst' );
	    my $success = 1;
	    while( <$outfile> ){
		if((/^0MINIMIZATION TERMINATED/ or /^0PROGRAM TERMINATED/) # If hard termination
		   or # We are picky and check some softer rerun requirements
		   ($picky[$i - $batchStart] and 
		    (/0ESTIMATE OF THETA IS NEAR THE BOUNDARY AND/ or 
		     /0PARAMETER ESTIMATE IS NEAR ITS BOUNDARY/    or
		     /0R MATRIX ALGORITHMICALLY SINGULAR/          or
		     /0S MATRIX ALGORITHMICALLY SINGULAR/ 
		     ) 
		    ) 
		   ){
		    print "NONMEM failed. Preparing rerun number" .  $try+1 . " / " . $retries[$i - $batchStart] . "\n";
		    $success = 0;
		    $self -> whipe();
		    $self -> {'filename'} = "NM_run".($i+1) . '_FCON';
		    $self -> parse('filename' => "NM_run".($i+1) . '_FCON' );
		    $self -> pertubate_all('fixed_thetas' => $fixed_thetas[$i],
					   'fixed_omegas' => $fixed_omegas[$i],
					   'fixed_sigmas' => $fixed_omegas[$i]);
		    $self -> write('filename' => "NM_run".($i+1) . '_FCON' );
		}
	    }
	    close( $outfile );
	    if($success){ last };
	}
    }
}


end run

# }}}

# {{{ whipe

start whipe
{
    $self -> {'fcon_lines'} = undef;
    $self -> {'theta_values'} = undef;
    $self -> {'have_omega_DIAG'} = undef;
    $self -> {'omega_values'} = undef;
    $self -> {'have_sigma_DIAG'} = undef;
    $self -> {'have_omega_BLST'} = undef;
    $self -> {'have_sigma_BLST'} = undef;
}
end whipe

# }}}

# {{{ parse
start parse
{
    my $fcon;
    open($fcon,"<",$self -> {'filename'}) or die "fcon_file: Unable to open ", $self -> {'filename'}, " : $!";
    my @fcon_lines =  <$fcon>;
    $self -> fcon_lines( \@fcon_lines );

    close($fcon);

    my $prob = -1;

    my @have_FIND;
    my @have_initial_STRC;

    my @have_sigma_STRC;
    my @have_omega_STRC;

    my @expect_sigma_BLST;
    my @expect_omega_BLST;

    my @have_sigma_BLST;
    my @have_omega_BLST;
    my @sigma_BLST_lengths;
    my @omega_BLST_lengths;

    my @expect_sigma_DIAG;
    my @expect_omega_DIAG;

    my @have_sigma_DIAG;
    my @have_omega_DIAG;

    my @nr_thetas;
    my @expect_bounds;

    for( my $i = 0; $i <= $#fcon_lines; $i++ ){
	my $line = $fcon_lines[$i];
	if( $line =~ /^FILE/ ){
	    #print "$i " . $line;
	}elsif( $line =~ /^PROB/ ){
	    $prob++;
	    $self -> prob($prob);
	    #print "$i " . $line;
	}elsif ( $line =~ /^DATA/ ){
	    #print "$i " .$line;
	}elsif ( $line =~ /^ITEM/ ){
	    #print "$i " .$line;
	}elsif ( $line =~ /^INDX/ ){
	    #print "$i " .$line;
	    $i = $self -> _skip_lines( 'fcon' => \@fcon_lines,
				       'index' => $i );
	}elsif ( $line =~ /^LABL/ ){
	    #print "$i " .$line;
	    $i = $self -> _skip_lines( 'fcon' => \@fcon_lines,
				       'index' => $i );
	}elsif ( $line =~ /^FORM/ ){
	    #print "$i " .$line;
	    $i = $self -> _skip_lines( 'fcon' => \@fcon_lines,
				       'index' => $i );
	}elsif ( $line =~ /^FIND/ ){
	    #print "$i " .$line;
	    $i = $self -> _skip_lines( 'fcon' => \@fcon_lines,
				       'index' => $i );
	    $have_FIND[$prob] = 1;
	}elsif ( $line =~ /^STRC/ ){
	    #print "$i " .$line;
	    if( $have_FIND[$prob] ){
		die "Malformated FCON (STRC unexpectedly found)\n";
	    }

	    my @values = @{$self -> _get_array( 'fcon' => \@fcon_lines, 
						'index' => $i, 
						'tabSize' => 4, 
						'nr_items' => 9 )};
	    $i = pop(@values);

	    unless( $have_initial_STRC[$prob] ){
		$have_initial_STRC[$prob] = 1;
		$nr_thetas[$prob] = $values[0];
		#$expect_bounds = $values[3];
		$expect_omega_DIAG[$prob] = $values[1];
		$expect_sigma_DIAG[$prob] = $values[2];
		$expect_omega_BLST[$prob] = $values[5] ? 0 : $values[6];
		$expect_sigma_BLST[$prob] = $values[7] ? 0 : $values[8];
	    } else {
		unless( $have_omega_STRC[$prob] ){
		    if( $expect_omega_BLST[$prob] ){
			@omega_BLST_lengths[$prob] = @values;
		    }
		    $have_omega_STRC[$prob] = 1;
		} else {
		    if( $expect_sigma_BLST[$prob] ){
			@sigma_BLST_lengths[$prob] = @values;
		    }
		    $have_sigma_STRC[$prob] = 1;
		}
	    }

	}elsif ( $line =~ /^THTA/ ){
	    #print "$i " .$line;
	    if( $have_initial_STRC[$prob] ){
		my @values = @{$self -> _get_array( 'fcon' => \@fcon_lines, 
						    'index' => $i, 
						    'tabSize' => 8, 
						    'nr_items' => $nr_thetas[$prob] )};
		$i = pop(@values);
		$self -> {'theta_values'} = [] unless defined $self -> {'theta_values'};
		$self -> theta_values -> [$prob] = \@values;
	    } else {
		die "Malformated FCON (THTA unexpectedly found)\n";
	    }
	    
	}elsif ( $line =~ /^LOWR/ ){
	    #print "$i " .$line;
	    #if( $expect_bounds ){
		my @values = @{$self -> _get_array( 'fcon' => \@fcon_lines, 
						    'index' => $i, 
						    'tabSize' => 8, 
						    'nr_items' => $nr_thetas[$prob] )};
		$i = pop(@values);
		$self -> {'lower_bounds'} = [] unless defined $self -> {'lower_bounds'};
		$self -> lower_bounds -> [$prob] = \@values;
	    #} else { ## I'd like to uncomment this.. but most FCONs seams malformated in this way. And its easy to ignore
		#die "Malformated FCON (LOWR unexpectedly found)\n";
	    #}
	}elsif ( $line =~ /^UPPR/ ){
	    #print "$i " .$line;
	    #if( $expect_bounds ){
		my @values = @{$self -> _get_array( 'fcon' => \@fcon_lines, 
						    'index' => $i, 
						    'tabSize' => 8, 
						    'nr_items' => $nr_thetas[$prob] )};
		$i = pop(@values);
		$self -> {'upper_bounds'} = [] unless defined $self -> {'upper_bounds'};
		$self -> upper_bounds -> [$prob] =  \@values;
	    #} else { ## I'd like to uncomment this.. but most FCONs seams malformated in this way. And its easy to ignore
		#die "Malformated FCON (UPPR unexpectedly found)\n";
	    #}
	}elsif ( $line =~ /^DIAG/ ){
	  #print "$i " .$line;
	  if( $expect_omega_DIAG[$prob] and not $have_omega_DIAG[$prob] and not ($expect_omega_BLST[$prob] or $have_omega_BLST[$prob]) ){
	    my @values = @{$self -> _get_array( 'fcon' => \@fcon_lines, 
						'index' => $i, 
						'tabSize' => 8, 
						'nr_items' => $expect_omega_DIAG[$prob] )};
	    $i = pop(@values);
	    $self -> {'omega_values'} = [] unless defined $self -> {'omega_values'};
	    $self -> omega_values -> [$prob] -> [0] = \@values;
	    $have_omega_DIAG[$prob] = 1;
	    $self -> {'have_omega_DIAG'} = [] unless defined $self -> {'have_omega_DIAG'};
	    $self -> have_omega_DIAG -> [$prob] = 1;
	  } elsif( $expect_sigma_DIAG[$prob] and not $have_sigma_DIAG[$prob] and not ($expect_sigma_BLST[$prob] or $have_sigma_BLST[$prob]) ) { 
	    my @values = @{$self -> _get_array( 'fcon' => \@fcon_lines, 
						'index' => $i, 
						'tabSize' => 8, 
						'nr_items' => $expect_sigma_DIAG[$prob] )};
	    $i = pop(@values);
	    $self -> {'sigma_values'} = [] unless defined $self -> {'sigma_values'};
	    $self -> sigma_values -> [$prob] -> [0] = \@values;
	    $have_sigma_DIAG[$prob] = 1;
	    $self -> {'have_sigma_DIAG'} = [] unless defined $self -> {'have_sigma_DIAG'};
	    $self -> have_sigma_DIAG -> [$prob] = 1;
	  }
	}elsif ( $line =~ /^BLST/ ){
	    #print "$i " .$line;
	    if( $expect_sigma_BLST[$prob] or $expect_omega_BLST[$prob] ){
		if( $expect_omega_BLST[$prob] ){
		    $expect_omega_BLST[$prob] --;
		    
		    my @values = @{$self -> _get_array( 'fcon' => \@fcon_lines, 
							'index' => $i, 
							'tabSize' => 8, 
							'nr_items' => 9 )};
		    $i = pop(@values);
		    $self -> omega_values -> [$prob] -> [$have_omega_BLST[$prob]] = \@values;

		    $have_omega_BLST[$prob]++;
		    $self -> {'have_omega_BLST'} = [] unless defined $self -> {'have_omega_BLST'};
		    $self -> have_omega_BLST -> [$prob] = 1;
		} elsif( $expect_sigma_BLST[$prob] ){
		    $expect_sigma_BLST[$prob] --;

		    my @values = @{$self -> _get_array( 'fcon' => \@fcon_lines, 
							'index' => $i, 
							'tabSize' => 8, 
							'nr_items' => 9 )};
		    $i = pop(@values);
		    
		    $self -> sigma_values -> [$prob] -> [$have_sigma_BLST[$prob]] = \@values;

		    $have_sigma_BLST[$prob]++;
		    $self -> {'have_sigma_BLST'} = [] unless defined $self -> {'have_sigma_BLST'};
		    $self -> have_sigma_BLST -> [$prob] = 1;
		}
	    } else {
		die "Malformated FCON (DIAG unexpectedly found)\n";
	    } 
	}elsif ( $line =~ /^ESTM/ ){
	    #print "$i " .$line;
	    $i = $self -> _skip_lines( 'fcon' => \@fcon_lines,
				       'index' => $i );
	}elsif ( $line =~ /^COVR/ ){
	    #print "$i " .$line;
	    $i = $self -> _skip_lines( 'fcon' => \@fcon_lines,
				       'index' => $i );
	}elsif ( $line =~ /^TABL/ ){
	    #print "$i " .$line;
	    $i = $self -> _skip_lines( 'fcon' => \@fcon_lines,
				       'index' => $i );
	}elsif ( $line =~ /^SCAT/ ){
	    #print "$i " .$line;
	    $i = $self -> _skip_lines( 'fcon' => \@fcon_lines,
				       'index' => $i );
	}else {
	     print "Found something new: \n $line \n" if $self -> debug;
	    $i = $self -> _skip_lines( 'fcon' => \@fcon_lines,
				       'index' => $i );
	}
    }
}
end parse
# }}}

# {{{ write

start write
{
    my $skip_line = 0;
    my @have_omega_DIAG;
    my @omega_BLST_count;
    my @sigma_BLST_count;

    my $prob = -1;

    open( my $outfile, ">". $filename ) or die "fcon_file: Unable to open FCON file: $!";

    foreach my $line (@{$self -> fcon_lines}){
	if( $line =~ /^PROB/ ){
	    $prob ++;
	    print $outfile $line;
	} elsif ( $line =~ /^(THTA.{4})/ ){
	    print $outfile $1;
	    my $i = 1;
	    $skip_line = 1;
	    foreach my $theta ( @{$self -> theta_values -> [$prob]} ){
		print $outfile "\n        " unless( $i % 10 );
		$i++;
		printf $outfile ("%8s",$theta);
	    }
	    print $outfile "\n";
	} elsif ( $line =~ /^(DIAG.{4})/ ){
	    print $outfile $1;
	    my $i = 1;
	    my @diag;
	    $skip_line = 1;
	    if( $self -> have_omega_DIAG -> [$prob] && !$have_omega_DIAG[$prob] ){
		@diag = @{$self -> omega_values -> [$prob] -> [0]};
		$have_omega_DIAG[$prob] = 1;
	    } elsif ( $self -> have_sigma_DIAG -> [$prob]) {
		@diag = @{$self -> sigma_values -> [$prob] -> [0]};
	    }
	    foreach my $diag ( @diag ){
		print $outfile "\n        " unless( $i % 10 );
		$i++;
		printf $outfile ("%8s",$diag);
	    }
	    print $outfile "\n";
	} elsif ( $line =~ /^(BLST.{4})/ ) {
 	    print $outfile $1;
 	    my $i = 1;
 	    my @blst;
 	    $skip_line = 1;
 	    if( $self -> have_omega_BLST -> [$prob] && $omega_BLST_count[$prob] <= $#{$self -> omega_values -> [$prob]} ){
		@blst = @{$self -> omega_values -> [$prob] -> [$omega_BLST_count[$prob]++]};
 	    } else {
 		@blst = @{$self -> sigma_values -> [$prob] -> [0]};
 	    }
	    
 	    foreach my $blst ( @blst ){
		print $outfile "\n        " unless( $i % 10 );
 		$i++;
 		printf $outfile ("%8s",$blst);
 	    }
 	    print $outfile "\n";
	} elsif( not $line =~ /^[A-Z]{4}[\ ]{3}/ and $skip_line){
	    next;
	} else {
	    $skip_line = 0;
	    print $outfile $line;
	}
    }
}
end write

# }}}

# {{{ pertubate_all
start pertubate_all
{
    for( my $i = 0; $i <= $self -> prob; $i++ ){
	$self -> _pertubate( 'estimates' => $self -> theta_values -> [$i],
			     'degree' => $degree,
			     'lobnds' => $self -> lower_bounds -> [$i],
			     'upbnds' => $self -> upper_bounds -> [$i],
			     'fixed' => $fixed_thetas[$i]);
	
	foreach my $sigma_line ( @{$self -> sigma_values -> [$i]} ){
	    $self -> _pertubate( 'estimates' => $sigma_line,
				 'degree' => $degree,
				 'fixed' => $fixed_sigmas[$i]);
	}
	
	foreach my $omega_line ( @{$self -> omega_values -> [$i]} ){
	    $self -> _pertubate( 'estimates' => $omega_line,
				 'degree' => $degree,
				 'fixed' => $fixed_omegas[$i]);
	}
    }
}
end pertubate_all
# }}}

# {{{ pertubate

start _pertubate
{
  for( my $i = 0; $i <= $#{$estimates}; $i++ ){
    unless( $fixed[$i] ){
      my ( $sign, $est, $form );
      my $lobnd;
      my $upbnd;
      my $init = $estimates -> [$i];

      my $change = abs($degree*$init);

      if ( defined $lobnds and defined $lobnds -> [$i] ){
	$lobnd = $lobnds -> [$i];
	$lobnd = $init-$change < $lobnd ? $lobnd : $init-$change;
      } else {
	$lobnd = $init-$change;
      }
      
      if ( defined $upbnds and defined $upbnds -> [$i] ) {
	$upbnd = $upbnds -> [$i];
	$upbnd = $init+$change > $upbnd ? $upbnd : $init+$change;
      } else {
	$upbnd = $init+$change;
      }
      
      $lobnd = 0.01 if ( ( $lobnd < 0.01 and $lobnd > -0.01)
			 and $upbnd >= 0.01001 );
      $upbnd = -0.01 if ( ( $upbnd < 0.01 and $upbnd > -0.01)
			  and $lobnd <= -0.01001 );
      

      if ( $lobnd <= -0.01 and $upbnd >= 0.01 ) {
	$est = $lobnd + 0.02 + rand($upbnd - ($lobnd+0.02));
	#$est = random_uniform(1, $lobnd + 0.02, $upbnd);
	$est = $est - 0.02 if ( $est <0.01 );
      } else {
	$est = $lobnd + rand($upbnd - $lobnd);
	#$est = random_uniform(1, $lobnd, $upbnd );
      }
      $form  = "%6.4f" if $est < 1000 and $est > -999;
      $form  = "%6.1f" if $est >= 1000 or $est <=-999;
      

      $estimates -> [$i] = sprintf $form,$est;
      if ( $estimates -> [$i] == 0 ) { 
	$estimates -> [$i] = '0.0001'; 
      }
    }
  }
}
end _pertubate

# }}}
