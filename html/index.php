<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <title>PsN :: Home</title>
<!--Adobe(R) LiveMotion(TM) 1.0 Generated JavaScript. Please do not edit. -->
  <script>

<!--

function newImage(arg) {

if (document.images) {

rslt = new Image();

rslt.src = arg;

return rslt;

}

}

ImageArray = new Array;

var preloadFlag = false;

function preloadImages() {

if (document.images) {

ImageArray[ImageArray.length++] = newImage(/* OWNER('object', 'dflt') *//*URL*/'images/indexhome.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object', 'movr') *//*URL*/'images/indexhomeov.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object1', 'dflt') *//*URL*/'images/indexdocumentation.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object1', 'movr') *//*URL*/'images/indexdocumentationov.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object2', 'dflt') *//*URL*/'images/indexdownload.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object2', 'movr') *//*URL*/'images/indexdownloadov.jpg');

ImageArray[ImageArray.length++] = newImage(/* OWNER('object3', 'dflt') *//*URL*/'images/indexbuglist.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object3', 'movr') *//*URL*/'images/indexbuglistov.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object4', 'dflt') *//*URL*/'images/indexmailing_list.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object4', 'movr') *//*URL*/'images/indexmailing_listov.jpg');
preloadFlag = true;
}
}
function changeImages() {
if (document.images && (preloadFlag == true)) {
for (var i=0; i<changeImages.arguments.length; i+=2) {
document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
}
}
}
// -->
  </script><!-- End generated JavaScript. -->
  <meta http-equiv="Content-Type"
 content="text/html; charset=iso-8859-1">
  <?php
     include("styles.php");
  ?>
  <script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) { //reloads the window if Nav4 resized
if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
  </script>
</head>
<body onLoad="preloadImages();"
 style="background-color: rgb(255, 255, 255);">
<!-- The table is not formatted nicely because some browsers cannot join images in table cells if there are any hard carriage returns in a TD. -->
<div id="Layer1"
 style="position: absolute; left: 335px; top: 42px; width: 388px; height: 43px; z-index: 1; font-size: x-small;">
<div class="style1" align="right">Welcome</div>
</div>
<div id="Layer2"
 style="position: absolute; left: 226px; top: 188px; width: 497px; height: 431px; z-index: 2;">
<div align="justify">
<h3 class="heading1">Latest News</h3>
<p>

Version 2.2.5 is available <a href="http://sourceforge.net/project/showfiles.php?group_id=101419">here</a>.</p>

<p>New features introduced in 2.2.5</p>

<p>- Monte Carlo Simulations (mcs) tool has been renamed to Stochastic
   Simulation and Estimation (sse), and has a bunch of new
   features. More information can be found in the SSE user guide
   distributed with PsN.</p>

<p>- Numerical Predictive Check (npc) and Visual Predictive Check
   (vpc) have been added to the toolbox. More information can be found
   in the NPC/VPC user guide distributed with PsN.</p>

<p>- Much improved results handling. Intermediate results are printed
   each time a NONMEM run has finished. A positive side effect is much
   reduced memory usage over all and in particular when running with
   many threads.</p>

<p>- Added support for G95. A more generic way of configuring compilers
   is on the way.</p>

<p>- When running on SGE PsN will name the job after the modelfile that is being run, previously the jobs were not explicitly named and SGE just reported them as "perl" (Thanks to Darin Perusich for this feature).</p>


<p>New features introduced in 2.2.4</p>
<p>- Simulated data suitable for creating mirror plots can be generated
   by using a new options called --mirror_plots. The option takes a
   value which defines the number of simulations that will be
   generated.</p>
<p>- A new option, --iofv, enables the computation of individual
   objective function values. These values are printed to a table file
   called iotab*, where * is the same number as is defined for any
   sdtab or patab tables. These names correspond to the normal Xpose
   table file format. If no sdtab or patab table is found in the
   NONMEM control stream, the iotab file is given number 1. </p>
<p>- A brand new user command <tt>update_inits</tt> that will put
   final estimates from a NONMEM output file and put into a given
   model file. This is quite experimental. PsN will reformat the model
   file quite a lot and comments in the file might get lost. Your model
   file will be copied with the addition of a ".org" extension, so you
   wont lose your model.</p>
<p>- "--compute_cwres" option renamed to simply "--cwres"</p>

<p>Bug Fixes  </p>
<p>

- As usual, bugs were found and fixed between 2.2.4 and 2.2.5. Please
read the 

<a href="http://sourceforge.net/project/shownotes.php?group_id=101419&release_id=601480">

change logs</a>. These are also available in the README.txt file
included in the release packege as usual. The most important bug fix
is the removal of a memory leak in the results handling.

</p>


<p><span class="heading1">Perl-speaks-NONMEM</span></p>
<p>Perl-speaks-NONMEM (PsN) is a collection of Perl modules and programs aiding in the development
  of non-linear mixed effect models using NONMEM. The functionality ranges from solutions to simpler
  tasks such as parameter estimate extraction from output files, data file sub setting  and resampling,
  to advanced computer-intensive statistical methods. PsN includes stand-alone tools for the end-user
  as well as development libraries for method developers.
</p>
<p>
  Compared to previous versions of PsN 2, the ongoing work on PsN version 2.2 has brought some changes 
  to the overall structure. PsN is now logically divided into three parts. Note, however, that you still 
  only download and install one package.
</p>
<p>
Most of what was included in the 2.0 release of PsN is now included in <b>PsN-Core</b>. PsN-Core is built around NONMEM's model, data and output
files.  Through PsN-Core 
all parts of these files can be controlled in user-written Perl code. </p>
<p>
Using the functionality of PsN-Core we have implemented two new components: <strong>PsN-Toolkit</strong> and <strong>PsN-Utilities</strong>.</p>
<p><strong>PsN-Toolkit</strong> is a collection
  of computer intensive statistical methods for non-linear mixed effect modeling using NONMEM.  The toolkit includes Perl modules for Bootstrapping, Jackknifing, Log-likelihood Profiling, Case-deletion Diagnostics and Stepwise Covariate Model building. Using  a module from PsN-Toolkit requires some basic
  knowledge of Perl and some understanding of the toolkit framework. </p>
<p><b>PsN-Utilities</b> is a set of command line programs
that allows the user to use the functionality of PsN-Core and PsN-Toolkit without the need to write Perl code. Apart from programs for all the tools in PsN-Toolkit, PsN-Utilities includes utilities that perform simpler tasks, such as summarizing the output from NONMEM
runs. 
</p>
<p>You can read more about the different parts under <a href="docs.php">Documentation</a>.</p>

<p>
Perl-speaks-NONMEM is copyright &copy; 2008 by Mats Karlsson, Niclas 
Jonsson and Andrew Hooker.<br>
&copy;2006-2007 by Lars Lindbom.<br>
&copy;2000-2005 by Lars Lindbom and Niclas Jonsson.<br>
All rights reserved.

</p>
<p>
PsN is developed and maintained by <a href="mailto:pontus.pihlgren@farmbio.uu.se">Pontus
Pihlgren</a> and <a href="mailto:kajsa.harling@farmbio.uu.se">Kajsa Harling</a>.
</p>

<p>PsN was originally developed by Niclas Jonsson and continued by Lars 
Lindbom for his doctoral thesis.</p>
<p>Additional implementation done by Jakob Ribbing, Kristin Karlsson, 
Maria Kjellsson, Joakim Nyberg and Andrew Hooker.</p>
<p>Additional contributions by Radojka Savic, Paul Baverel, Martin Bergstrand, Elodie Plan and many more.</p>

<p>The site is designed and implemented
by <a href="mailto:justin.wilkins@farmbio.uu.se">Justin
Wilkins</a>.
</p>
<p>
Perl-speaks-NONMEM is licensed under version 2 of the GNU General
Public License as published by the Free Software Foundation.
</p>
<h3 class="heading1">References</h3>
<ul>
    <li class="reference"> <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=
16023764&query_hl=1&itool=pubmed_docsum">Lindbom L, Pihlgren P, Jonsson EN.</a> <br>
                PsN-Toolkit--a collection of computer intensive statistical methods for non-linear mixed effect modeling using NON
MEM. <br>
                Comput Methods Programs Biomed. 2005 Sep;79(3):241-57. </li>
    <li class="reference"> <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=
15212851&query_hl=1&itool=pubmed_docsum">Lindbom L, Ribbing J, Jonsson EN. </a> <br>
                Perl-speaks-NONMEM (PsN)--a Perl module for NONMEM related programming.<br>
        Comput Methods Programs Biomed. 2004 Aug;75(2):85-94.</li>
</ul>

  <h3 class="heading1">Acknowledgments</h3>
<p>
NONMEM&reg; is a registered trademark of GloboMax. The Perl camel
logo is a registered trademark of O'Reilly Media, Inc. and is used with
permission. All logos and trademarks in this site are property of
their respective owners.
</p>

<p>

Thanks to Jeroen Elassaiss and Stefan Verhoeven for fixing bugs in SGE
submission code.

</p>

<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="20%"><a href="http://www.sourceforge.net/" target="_blank"><img src="gfx/sflogo.php.png" alt="SourceForge" width="88" height="31" vspace="10" border="0"></a></td>
    <td width="80%">PsN is hosted at SourceForge. </td>
  </tr>
</table>
<p>&nbsp;</p>
<p align="justify">&nbsp;</p>
</div>
</div>
<table border="0" cellpadding="0" cellspacing="0" width="780">
  <tbody>
    <tr>
      <td height="600" width="780">
      <table border="0" cellpadding="0" cellspacing="0" width="780">
        <tbody>
          <tr>
            <td colspan="7" height="201" width="780"><img
 src="images/indexpane1_1_.jpg" name="" alt="" border="0" height="201"
 width="780"></td>
          </tr>
          <tr>
            <td height="46" width="26"><img
 src="images/indexpane1_2_.jpg" name="" alt="" border="0" height="46"
 width="26"></td>
            <td height="46" width="73"><a href="index.php"
 onmouseover="changeImages(/*CMP*/ 'object', /*URL*/ 'images/indexhomeov.jpg'); return true;"
 onmouseout="changeImages(/*CMP*/ 'object', /*URL*/ 'images/indexhome.jpg'); return true;"><img
 src="images/indexhome.jpg" name="object" alt="Home" border="0"
 height="46" width="73"></a></td>
            <td height="46" width="11"><img
 src="images/indexpane3_2_.jpg" name="" alt="" border="0" height="46"
 width="11"></td>
            <td height="46" width="25"><img
 src="images/indexpane4_2_.jpg" name="" alt="" border="0" height="46"
 width="25"></td>
            <td height="46" width="15"><img
 src="images/indexpane5_2_.jpg" name="" alt="" border="0" height="46"
 width="15"></td>
            <td height="46" width="40"><img
 src="images/indexpane6_2_.jpg" name="" alt="" border="0" height="46"
 width="40"></td>
            <td height="46" width="590"><img
 src="images/indexpane7_2_.jpg" name="" alt="" border="0" height="46"
 width="590"></td>
          </tr>
          <tr>
            <td colspan="7" height="7" width="780"><img
 src="images/indexpane1_3_.jpg" name="" alt="" border="0" height="7"
 width="780"></td>
          </tr>
          <tr>
            <td height="46" width="26"><img
 src="images/indexpane1_4_.jpg" name="" alt="" border="0" height="46"
 width="26"></td>
            <td colspan="5" height="46" width="164"><a href="docs.php"
 onmouseover="changeImages(/*CMP*/ 'object1', /*URL*/ 'images/indexdocumentationov.jpg'); return true;"
 onmouseout="changeImages(/*CMP*/ 'object1', /*URL*/ 'images/indexdocumentation.jpg'); return true;"><img
 src="images/indexdocumentation.jpg" name="object1" alt="Documentation"
 border="0" height="46" width="164"></a></td>
            <td height="46" width="590"><img
 src="images/indexpane7_4_.jpg" name="" alt="" border="0" height="46"
 width="590"></td>
          </tr>
          <tr>
            <td colspan="7" height="7" width="780"><img
 src="images/indexpane1_5_.jpg" name="" alt="" border="0" height="7"
 width="780"></td>
          </tr>
          <tr>
            <td height="46" width="26"><img
 src="images/indexpane1_6_.jpg" name="" alt="" border="0" height="46"
 width="26"></td>
            <td colspan="3" height="46" width="109"><a
 href="download.php"
 onmouseover="changeImages(/*CMP*/ 'object2', /*URL*/ 'images/indexdownloadov.jpg'); return true;"
 onmouseout="changeImages(/*CMP*/ 'object2', /*URL*/ 'images/indexdownload.jpg'); return true;"><img
 src="images/indexdownload.jpg" name="object2" alt="Download" border="0"
 height="46" width="109"></a></td>
            <td colspan="3" height="46" width="645"><img
 src="images/indexpane5_6_.jpg" name="" alt="" border="0" height="46"
 width="645"></td>
          </tr>
          <tr>
            <td colspan="7" height="7" width="780"><img
 src="images/indexpane1_7_.jpg" name="" alt="" border="0" height="7"
 width="780"></td>
          </tr>
          <tr>
            <td height="46" width="26"><img
 src="images/indexpane1_8_.jpg" name="" alt="" border="0" height="46"
 width="26"></td>
            <td colspan="2" height="46" width="84"><a href="buglist.php"
 onmouseover="changeImages(/*CMP*/ 'object3', /*URL*/ 'images/indexbuglistov.jpg'); return true;"
 onmouseout="changeImages(/*CMP*/ 'object3', /*URL*/ 'images/indexbuglist.jpg'); return true;"><img
 src="images/indexbuglist.jpg" name="object3" alt="Buglist" border="0"
 height="46" width="84"></a></td>
            <td colspan="4" height="46" width="670"><img
 src="images/indexpane4_8_.jpg" name="" alt="" border="0" height="46"
 width="670"></td>
          </tr>
          <tr>
            <td colspan="7" height="7" width="780"><img
 src="images/indexpane1_9_.jpg" name="" alt="" border="0" height="7"
 width="780"></td>
          </tr>
          <tr>
            <td height="46" width="26"><img
 src="images/indexpane1_10_.jpg" name="" alt="" border="0" height="46"
 width="26"></td>
            <td colspan="4" height="46" width="124"><a href="list.php"
 onmouseover="changeImages(/*CMP*/ 'object4', /*URL*/ 'images/indexmailing_listov.jpg'); return true;"
 onmouseout="changeImages(/*CMP*/ 'object4', /*URL*/ 'images/indexmailing_list.jpg'); return true;"><img
 src="images/indexmailing_list.jpg" name="object4" alt="Mailing List"
 border="0" height="46" width="124"></a></td>
            <td colspan="2" height="46" width="630"><img
 src="images/indexpane6_10_.jpg" name="" alt="" border="0" height="46"
 width="630"></td>
          </tr>
          <tr>
            <td colspan="7" height="141" width="780"><img
 src="images/indexpane1_11_.jpg" name="" alt="" border="0" height="141"
 width="780"></td>
          </tr>
        </tbody>
      </table>
      </td>
    </tr>
    <tr>
      <td><img src="images/is_single_pixel_gif.gif" alt="" height="1"
 width="780"></td>
    </tr>
  </tbody>
</table>
<!--Adobe(R) LiveMotion(TM) DataMap1.0 DO NOT EDIT
end DataMap -->
</body>
</html>
