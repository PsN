<!-- $Revision: 2.0 $ -->
<html>
<head>
<title><MM-Presentable-User> Configuration for <MM-List-Name></title>
<style type="text/css">
<!--
body,td,th {
font-family: Helvetica, Arial, serif;
font-size: 12px;
color: #000000;
}
body {
margin-left: 6px;
margin-top: 0px;
margin-right: 0px;
margin-bottom: 0px;
}
.heading1 {
font-size: 16px;
font-weight: bold;
color: #333399;
margin-left: 40px;
}
a:link {
color: #656D9C;
text-decoration: none;
}
a:visited {
color: #656D9C;
text-decoration: none;
}
a:hover {
color: #AF9D49;
text-decoration: none;
}
a:active {
color: #656D9C;
text-decoration: none;
}
-->
    </style>  
</head>
<BODY BGCOLOR="#ffffff">
	<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="5">
	<TR><TD WIDTH="100%"><B>
	<h3> <span class="heading1">
<MM-List-Name></span></h3>
	    <h3> <span class="heading1">Configuration for <MM-Presentable-User></span></h3>
</TD>
	</TR> 
	</TABLE>
<MM-Form-Start>

<p>

<b><MM-Presentable-User></b>'s subscription status,
password, and options for the <MM-List-Name> mailing list.
<MM-Case-Preserved-User>

<MM-Disabled-Notice>

<p>

<a name=unsub>
	<TABLE WIDTH="100%" BORDER="0" CELLSPACING=5" CELLPADDING="5">
	<TR><TD WIDTH="50%"><h3><span class="heading1">Unsubscribing from <MM-List-Name></span></h3>
	  
	<TD WIDTH="50%"><FONT COLOR="#000000">
	</FONT></TD>
	</TR>

	<tr>
	  <td>

<p>To unsubscribe, enter your password and hit the button. (If you've
lost your password, see just below to have it emailed to you.)</p>

<p>
Password: <MM-Unsub-Pw-Box> &nbsp;
<MM-Unsubscribe-Button>
</a>
</TD></TR> </table>

	<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="5">
	<TR><TD COLSPAN=2 WIDTH="100%"><h3><span class="heading1">Your <MM-List-Name> Password</span></h3></TD>
</TR>
<tr valign="TOP">
<td WIDTH="50%">
<a name=reminder>
<h5 align="left">Forgotten Your Password?</h5>
Click this button to have your password emailed to your list delivery
address. <p>
<MM-Umbrella-Notice>
<MM-Email-My-Pw>
</td>
<td WIDTH="50%">
<a name=changepw>
<center>
<h5>Change Your Password:</h5>
      <TABLE BORDER="0" CELLSPACING="2" CELLPADDING="2" WIDTH="70%" COLS=2>
      <TR>
	<TD WIDTH="30%" valign="top" BGCOLOR="#E0E6FF">Old password:</TD>
	<TD WIDTH="70%" valign="top"><MM-Old-Pw-Box></TD>
      </TR>
      <TR>
	<TD valign="top" BGCOLOR="#E0E6FF">New password:</TD>
	<TD valign="top"><MM-New-Pass-Box></TD>
      </TR>
      <TR> 
	<TD valign="top" BGCOLOR="#E0E6FF">Again to confirm:</TD>
	<TD valign="top"><MM-Confirm-Pass-Box></TD>
      </TR>
</table>
           <MM-Change-Pass-Button>
    </TABLE>

<p>

<a name=options>
	<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="5">
	<TR><TD WIDTH="100%">
      <h3 class="heading1">Your <MM-List-Name> Subscription Options
      </h3></TD>
	</TR> 
	</table>

<p>
  <strong>Current values are checked.</strong>
<p>
<p>
<TABLE BORDER="0" CELLSPACING="2" CELLPADDING="3" WIDTH="80%">
<tr><TD BGCOLOR="#E0E6FF">
<a name="disable"><strong>Disable mail delivery </strong> 
Turn this on if you want mail to not be delivered to you for a little while.<br>
<mm-delivery-enable-button> Off
<mm-delivery-disable-button> On <p></a></td></tr>
<tr><TD BGCOLOR="#E0E6FF">
<strong>Set Digest Mode</strong> <br>
If you turn digest mode on, you'll get posts bundled together once a
day, instead singly when they're sent.  If digest mode is changed from 
on to off, you will receive one last digest.<br>
<MM-Undigest-Radio-Button> Off
<MM-Digest-Radio-Button> On<br>
</td></tr>
<tr><TD BGCOLOR="#E0E6FF">
<strong>Get MIME or Plain Text Digests?</strong> <br>
If you have any problems with MIME digests, select plain text. <br>
<MM-Mime-Digests-Button> MIME
<MM-Plain-Digests-Button> Plain Text <p>
</td></tr>
<tr><TD BGCOLOR="#E0E6FF">
<strong>Receive posts you send to the list? </strong><br>
<mm-receive-own-mail-button> Yes
<mm-dont-receive-own-mail-button> No <p>
</td></tr>
<tr><TD BGCOLOR="#E0E6FF">
<strong>Receive acknowledgement mail when you send mail to the list? </strong><br>
<mm-dont-ack-posts-button> No
<mm-ack-posts-button> Yes <p>
</td></tr>
<tr><TD BGCOLOR="#E0E6FF">
<strong> Conceal yourself from subscriber list? </strong><br>
<MM-Public-Subscription-Button> No
<MM-Hide-Subscription-Button> Yes <p>
</td></tr>
<tr><TD BGCOLOR="#E0E6FF">
Password: <MM-Digest-Pw-Box>  <MM-Digest-Submit>
<p>
</td></tr></table>

<p>
<MM-Form-End>

<MM-Mailman-Footer>
</body>
</html>