use strict;
#---------------------------------------------------------------------
#         Perl Class Package
#---------------------------------------------------------------------
package Example;



sub new {
	my $type  = shift;
	my $class = ref($type) || $type;
	my %parm  = @_;
	my @valid_parm = ( 'member_one' );

	my %isvalid = ();
	foreach my $givenp ( keys %parm ) {
		foreach my $validp ( @valid_parm ) {
			$isvalid{$givenp} = 1 if( $givenp eq $validp );
		}
		die "ERROR in Example->new: Parameter $givenp is not valid\n"
			unless( $isvalid{$givenp} );
	}

	my $this = ref($type) ? $type : {};

	die("Example->new: Scalar parameter member_one is of wrong type:" . ref(\$parm{'member_one'}))
		if (defined $parm{'member_one'}) and !(ref(\$parm{'member_one'}) eq 'SCALAR');
	$this -> {'member_one'} = defined $parm{'member_one'} ? $parm{'member_one'} : 1;

	bless $this, $class;

	# Start of Non-Dia code #

        print "Entering \t".ref($this)." -> new\n" if ($this -> {'debug'});
	print "This is code inside the constructor\n";
        print "Leaving \t".ref($this)." -> new\n" if ($this -> {'debug'});

	# End of Non-Dia code #

	return $this;
};

sub member_one {
	my $self = shift;
	my $parm = shift;

	# Start of Non-Dia code #
        print "Entering \t".ref($self)." -> member_one\n" if ($self -> {'debug'});
	print "And this is code inside member_one accessor\n";
        print "Leaving \t".ref($self)." -> member_one\n" if ($self -> {'debug'});

	# End of Non-Dia code #

	if( defined($parm) ){
		$self -> {'member_one'} = $parm;
	} else {
		return $self -> {'member_one'};
	}
}

sub method_one {
	my $self = shift;
	my %parm  = @_;
	my @valid_parm = ( 'parm_one' );

	my %isvalid = ();
	foreach my $givenp ( keys %parm ) {
		foreach my $validp ( @valid_parm ) {
			$isvalid{$givenp} = 1 if( $givenp eq $validp );
		}
		die "ERROR in Example->method_one: Parameter $givenp is not valid\n"
			unless( $isvalid{$givenp} );
	}

	die("Example->method_one: Scalar parameter parm_one is of wrong type:" . ref(\$parm{'parm_one'}))
		if (defined $parm{'parm_one'}) and !(ref(\$parm{'parm_one'}) eq 'SCALAR');
	my $parm_one = $parm{'parm_one'};

	my @return_value;

	# Start of Non-Dia code #

        print "Entering \t".ref($self)." -> method_one\n" if ($self -> {'debug'});
	print "This is code inside method_one\n";
        print "Leaving \t".ref($self)." -> method_one\n" if ($self -> {'debug'});

	# End of Non-Dia code #

	return \@return_value;
}

1;

