<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <title>PsN :: Documentation :: execute</title>
<!--Adobe(R) LiveMotion(TM) 1.0 Generated JavaScript. Please do not edit. -->
  <script>

<!--

function newImage(arg) {

if (document.images) {

rslt = new Image();

rslt.src = arg;

return rslt;

}

}

ImageArray = new Array;

var preloadFlag = false;

function preloadImages() {

if (document.images) {

ImageArray[ImageArray.length++] = newImage(/* OWNER('object', 'dflt') *//*URL*/'images/indexhome.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object', 'movr') *//*URL*/'images/indexhomeov.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object1', 'dflt') *//*URL*/'images/indexdocumentation.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object1', 'movr') *//*URL*/'images/indexdocumentationov.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object2', 'dflt') *//*URL*/'images/indexdownload.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object2', 'movr') *//*URL*/'images/indexdownloadov.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object3', 'dflt') *//*URL*/'images/indexbuglist.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object3', 'movr') *//*URL*/'images/indexbuglistov.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object4', 'dflt') *//*URL*/'images/indexmailing_list.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object4', 'movr') *//*URL*/'images/indexmailing_listov.jpg');
preloadFlag = true;
}
}
function changeImages() {
if (document.images && (preloadFlag == true)) {
for (var i=0; i<changeImages.arguments.length; i+=2) {
document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
}
}
}
// -->
  </script><!-- End generated JavaScript. -->
  <meta http-equiv="Content-Type"
 content="text/html; charset=ISO-8859-1">
  <style type="text/css">
<!--
body,td,th {
font-family: Helvetica, Arial, serif;
font-size: 12px;
color: #000000;
}
body {
margin-left: 6px;
margin-top: 0px;
margin-right: 0px;
margin-bottom: 0px;
background-image: url(gfx/bg.jpg);
}
.style1 {
font-size: 36px;
font-weight: bold;
}
.heading1 {
font-size: 16px;
font-weight: bold;
color: #333399;
margin-left: 40px;
}
a:link {
color: #656D9C;
text-decoration: none;
}
a:visited {
color: #656D9C;
text-decoration: none;
}
a:hover {
color: #AF9D49;
text-decoration: none;
}
a:active {
color: #656D9C;
text-decoration: none;
}
.style2 {font-family: "Courier New", Courier, mono}
-->
  </style>
  <script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) { //reloads the window if Nav4 resized
if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
  </script>
  <meta content="Pntus Pihlgren" name="author">
</head>
<body onload="preloadImages();"
 style="background-color: rgb(255, 255, 255);">
<!-- The table is not formatted nicely because some browsers cannot join images in table cells if there are any hard carriage returns in a TD. -->
<div id="Layer1"
 style="position: absolute; left: 335px; top: 42px; width: 388px; height: 43px; z-index: 1; font-size: x-small;">
<div class="style1" align="right">Execute<br>
</div>
</div>
<div id="Layer2"
 style="position: absolute; left: 226px; top: 188px; width: 497px; height: 431px; z-index: 2;">
<div align="justify">

<?php
include("execute_synopsis.php");
include("execute_description.php");
include("execute_options.php");
include("execute_examples.php");
php?>
<!--
<h3 class="heading1">Synopsis</h3>
<span class="style2">$ execute [ -h | -? ] [ --help ]<br>
[ --abort_on_fail ]<br>
[ --clean ]<br>
[ --compress ]<br>
[ --cpu_time='integer' ]<br>
[ --debug=0 ]<br>
[ --debug_package='string' ]<br>
[ --debug_subroutine='string' ]<br>
[ --directory='string' ]<br>
[ --extra_data_file='string' ]<br>
[ --extra_files='string' ]<br>
[ --grid_batch_size='integer' ]<br>
[ --missing_data_token='string' ]<br>
[ --nm_directory='string' ]<br>
[ --nm_version='string' ]<br>
[ --outputfile='string' ]<br>
[ --picky ]<br>
[ --remove_temp_files ]<br>
[ --results_file='string' ]<br>
[ --retries='integer' ]<br>
[ --run_on_nordugrid ]<br>
[ --seed='string' ]<br>
[ --threads='integer' ]<br>
[ --tweak_inits ]</span><br>
<h3 class="heading1">Description</h3>
<p>The execute utility is a Perl script that allows you to run multiple model files
    either sequentially or in parallel. It is more or less an nmfe replacement.</p>
<p> The execute utility creates sub directories where it puts NONMEM's input-
      and output files, to make sure that parallel NONMEM runs do not interfere with each other. The top directory is by default named
      'modelfit_dirX', where 'X' is a number that starts at 1 and is
      increased by one each time the execute utility is run.</p>
<p> When the NONMEM runs are finished, the output- and table files will be copied
      to the directory where execute started. This means that you can normally ignore
      the 'modelfit_dirX' directory. If you need to access any
      special files these can be found inside the 'modelfit_dirX'. Inside the 'modelfit_dirX' a few
      sub directories named 'NM_runY' and 'mZ' will also be found.  For each model file 
      specified on the command line there will be one 'NM_runY' directory in which the actual NONMEM execution takes place. The order
      of the 'NM_runY' directories corresponds to the order of the
      model files given on the command line. The first run will take place inside 'NM_run1', the second in 'NM_run2' and
      so on. The 'mZ' directories are only used internally by execute so you can safely ignore them.</p>
<h3 class="heading1">Options</h3>

<p class="style2">-h | -?</p>
<p> With -h or -? excute.pl will print the list of options and then exit.</p>

<p class="style2"> -help</p>
<p> With -help execute will print a longer, more detailed message.</p>

<p class="style2"> -nm_version='integer'</p>

<p> If you have more than one installation of NONMEM you can choose
between them using the <span class="style2">-nm_version</span>
option. The installations must be specified in the psn.conf file. The
default value is 5. </p>

<p class="style2"> -threads='integer'</p>

<p> The <span class="style2">-threads</span> 

option enables parallel execution of multiple NONMEM runs. On a
desktop computer it is recommended to set <span
class="style2">-threads</span> to the number of CPUs in the system
plus one. It is possible to specify more threads, but that will
probably not increase the performance. If you are running on a
computer cluster, you should consult your systems administrator to
find out how many threads to specify. The <span
class="style2">-threads</span> option will be ignored if you run on a
grid system, since grids have their own scheduling algorithms. The
default value for the <span class="style2">-threads</span>option is
1.</p>

<p class="style2"> -directory='string'</p>

<p> The directory option defines the directory in which execute will
run NONMEM. The default name is 'modelfit_dirX' where X will be
increased by one each time you run the execute utility. You do not
    have to create the directory since it will be done automatically.</p>

<p> If you abort execute or if your system crashes you can use the 
  '-directory' option set to the directory of the execute run that
  crashed. Execute will then not run the model files that had finished
  before the crash, thereby saving some time. Note that is
  important to give exactly the same options as was given the first
  time.</p>

<p class="style2"> -seed='string'</p>

<p> If you use the -retries='integer' option, execute will
  use a random number to create new initial estimates for the model
parameters. To make sure that
  the same result is produced if  the same model is rerun, you can set 
    your own random seed with the <span class="style2">-seed</span> option.</p>

<p class="style2"> -remove_temp_files</p> <p> If the <span
class="style2">-remove_temp_files</span> option is used, execute will
remove the 'FCON', 'FDATA', 'FREPORT', 'FSUBS', 'FSUBS.f', 'LINK.LNK',
'FSTREAM', 'PRDERR' and 'nonmem' files from the 'NM_runY'
directory. The default value is 0. </p>

<p class="style2"> -clean</p>

<p> The <span class="style2">-clean</span> option is a more thorough
version of '-remove_temp_files'. If the <span
class="style2">-clean</span> option is used, execute will remove the
entire 'NM_runY'
  directory after the NONMEM run is finished.</p>

<p class="style2"> -compress</p>

<p> The execute utility will compress the contents of 'NM_runY' to the
file 'nonmem_files.tgz' if the <span class="style2">-compress</span>
option is used and if you have the archive and compress programs
<strong>tar</strong> and <strong>gzip</strong> installed. If you use
the <span class="style2">-remove_temp_files </span>options,
  temporary files will be removed before the compression. The <span
class="style2">-compress</span> option obviously has no effect if you
also use the <span class="style2">-clean</span> option. </p>

<p> -tweak_inits</p> <p> If NONMEM terminates nonsuccessfully, PsN can
perturb the initial estimates and run NONMEM again. The generation of
new initial estimates <img src="images/init1.gif"> for the
<em>i</em>:th retry are performed according to</p> <p
align="center"><img src="images/perturb1.gif" width="236"
height="32"></p> <p>where <img src="images/init_orig1.gif" width="29"
height="28"> are the initial estimates of the original run. The
updating procedure makes sure that boundary conditions on the
parameters are still valid. For this option to be valid, the <span
class="style2">-retries</span> option must be set to a number larger
than zero. The default setting uses tweak_inits. </p>

<p class="style2"> -outputfile</p> <p> The <span
class="style2">-outputfile</span> option specifies the output file
name for the NONMEM run. Currently this option is only valid when a
single model is supplied to the execute utility.</p>

<p class="style2">-picky</p>
<p> The <span class="style2">-picky</span> option is only valid together with
  <span class="style2">-tweak_inits</span>. Normally PsN only tries new initial estimates if '<span class="style2">MINIMZATION SUCCESSFUL</span>' is not found in the NONMEM output file. With the <span class="style2">-picky</span> option,  PsN
  will  regard any of the following messages as a signal for rerunning:</p>
<p class="style2"> 0ESTIMATE OF THETA IS NEAR THE BOUNDARY<br>
  0PARAMETER ESTIMATE IS NEAR ITS BOUNDARY<br>
  0R MATRIX ALGORITHMICALLY SINGULAR<br>
  0S MATRIX ALGORITHMICALLY SINGULAR</p>

<p class="style2"> -retries='integer'</p>

<p>  The <span class="style2">-retries</span> option tells
the execute utility how many times it shall try to rerun a NONMEM job if it gets an error message. In the current version of PsN (2.2), the <span class="style2">-retries</span> option is only valid together with <span class="style2">-tweak_inits</span>. The default value of the <span class="style2">-retries</span> option is 0. </p>

<p class="style2"> -abort_on_fail</p> <p> If the <span
class="style2">-abort_on_fail</span> option is set and one of the
NONMEM runs fails, execute will stop scheduling
  more runs and try to stop those that are currently running.</p>

<p class="style2">-run_on_nordugrid</p>
<p>!! Currently only valid for Linux system !!<br>
 Using this option, execute will run on the nordugrid clusters listed
in ~/.ng_cluster. If you do not know about Nordugrid, you can safely
ignore this option. You can read more on http://www.nordugrid.org.
</p>

<p class="style2"> -cpu_time='integer'</p>

<p> This option specifies the number of minutes allocated for a
  grid job. The default value is 120 minutes. This option is only
valid together with the <span class="style2">-run_on_nordugrid</span>
option.</p>

<p class="style2"> -grid_batch_size='integer'</p>
<p> This option specifies the number of NONMEM runs that will be grouped 
  together into one grid job. The default number is 5. This option is
only valid together with the '-run_on_nordugrid' option.</p>

<p class="style2"> -debug='integer'</p>

<p> The <span class="style2">-debug </span>option is mainly intended
for developers who wish to debug PsN. By default <span
class="style2">-debug</span> is set to zero but you can try setting it
to '1' to enable warning messages. If you run into problems that
require support, you may have to increase this number to 2 or 3 and
send the output to us.</p>

<p class="style2"> -debug_package='string'</p>

<p> When used together with <span class="style2">-debug</span>, the
<span class="style2">-debug_package</span> option makes is possible to
choose which part of PsN you want to see debug messages for. Again,
this option is mostly useful for developers.</p>

<p class="style2"> -debug_subroutine='string'</p>

<p> With this option it is possible to specify exactly which
subroutines in PsN you want to see debug messages for.</p>
-->
<h3 class="heading1">Output</h3>
<p>The output- and table files of NONMEM will be copied from the temporary run directories to the place specified by the <span class="style2">-outputfile</span> option and the $TABLE record of the model files. </p>
</div>
<!--
<h3 class="heading1">Example</h3>
<p align="justify" class="style2">$ execute pheno.mod </p>
<p align="justify">Runs one model file and accepts all default values.</p>
<p align="justify" class="style2">$ execute -threads=2  -retries=5 phenobarbital.mod pheno_alternate.mod</p>
<p align="justify">Runs two model files in parallel using 5 possible retries.</p>
-->
</div>

<table border="0" cellpadding="0" cellspacing="0" width="780">
  <tbody>
    <tr>
      <td height="600" width="780">
      <table border="0" cellpadding="0" cellspacing="0" width="780">
        <tbody>
          <tr>
            <td colspan="7" height="201" width="780"><img
 src="images/indexpane1_1_.jpg" name="" alt="" border="0" height="201"
 width="780"></td>
          </tr>
          <tr>
            <td height="46" width="26"><img
 src="images/indexpane1_2_.jpg" name="" alt="" border="0" height="46"
 width="26"></td>
            <td height="46" width="73"><a href="index.php"
 onmouseover="changeImages(/*CMP*/ 'object', /*URL*/ 'images/indexhomeov.jpg'); return true;"
 onmouseout="changeImages(/*CMP*/ 'object', /*URL*/ 'images/indexhome.jpg'); return true;"><img
 src="images/indexhome.jpg" name="object" alt="Home" border="0"
 height="46" width="73"></a></td>
            <td height="46" width="11"><img
 src="images/indexpane3_2_.jpg" name="" alt="" border="0" height="46"
 width="11"></td>
            <td height="46" width="25"><img
 src="images/indexpane4_2_.jpg" name="" alt="" border="0" height="46"
 width="25"></td>
            <td height="46" width="15"><img
 src="images/indexpane5_2_.jpg" name="" alt="" border="0" height="46"
 width="15"></td>
            <td height="46" width="40"><img
 src="images/indexpane6_2_.jpg" name="" alt="" border="0" height="46"
 width="40"></td>
            <td height="46" width="590"><img
 src="images/indexpane7_2_.jpg" name="" alt="" border="0" height="46"
 width="590"></td>
          </tr>
          <tr>
            <td colspan="7" height="7" width="780"><img
 src="images/indexpane1_3_.jpg" name="" alt="" border="0" height="7"
 width="780"></td>
          </tr>
          <tr>
            <td height="46" width="26"><img
 src="images/indexpane1_4_.jpg" name="" alt="" border="0" height="46"
 width="26"></td>
            <td colspan="5" height="46" width="164"><a href="docs.php"
 onmouseover="changeImages(/*CMP*/ 'object1', /*URL*/ 'images/indexdocumentationov.jpg'); return true;"
 onmouseout="changeImages(/*CMP*/ 'object1', /*URL*/ 'images/indexdocumentation.jpg'); return true;"><img
 src="images/indexdocumentation.jpg" name="object1" alt="Documentation"
 border="0" height="46" width="164"></a></td>
            <td height="46" width="590"><img
 src="images/indexpane7_4_.jpg" name="" alt="" border="0" height="46"
 width="590"></td>
          </tr>
          <tr>
            <td colspan="7" height="7" width="780"><img
 src="images/indexpane1_5_.jpg" name="" alt="" border="0" height="7"
 width="780"></td>
          </tr>
          <tr>
            <td height="46" width="26"><img
 src="images/indexpane1_6_.jpg" name="" alt="" border="0" height="46"
 width="26"></td>
            <td colspan="3" height="46" width="109"><a
 href="download.php"
 onmouseover="changeImages(/*CMP*/ 'object2', /*URL*/ 'images/indexdownloadov.jpg'); return true;"
 onmouseout="changeImages(/*CMP*/ 'object2', /*URL*/ 'images/indexdownload.jpg'); return true;"><img
 src="images/indexdownload.jpg" name="object2" alt="Download" border="0"
 height="46" width="109"></a></td>
            <td colspan="3" height="46" width="645"><img
 src="images/indexpane5_6_.jpg" name="" alt="" border="0" height="46"
 width="645"></td>
          </tr>
          <tr>
            <td colspan="7" height="7" width="780"><img
 src="images/indexpane1_7_.jpg" name="" alt="" border="0" height="7"
 width="780"></td>
          </tr>
          <tr>
            <td height="46" width="26"><img
 src="images/indexpane1_8_.jpg" name="" alt="" border="0" height="46"
 width="26"></td>
            <td colspan="2" height="46" width="84"><a href="buglist.php"
 onmouseover="changeImages(/*CMP*/ 'object3', /*URL*/ 'images/indexbuglistov.jpg'); return true;"
 onmouseout="changeImages(/*CMP*/ 'object3', /*URL*/ 'images/indexbuglist.jpg'); return true;"><img
 src="images/indexbuglist.jpg" name="object3" alt="Buglist" border="0"
 height="46" width="84"></a></td>
            <td colspan="4" height="46" width="670"><img
 src="images/indexpane4_8_.jpg" name="" alt="" border="0" height="46"
 width="670"></td>
          </tr>
          <tr>
            <td colspan="7" height="7" width="780"><img
 src="images/indexpane1_9_.jpg" name="" alt="" border="0" height="7"
 width="780"></td>
          </tr>
          <tr>
            <td height="46" width="26"><img
 src="images/indexpane1_10_.jpg" name="" alt="" border="0" height="46"
 width="26"></td>
            <td colspan="4" height="46" width="124"><a href="list.php"
 onmouseover="changeImages(/*CMP*/ 'object4', /*URL*/ 'images/indexmailing_listov.jpg'); return true;"
 onmouseout="changeImages(/*CMP*/ 'object4', /*URL*/ 'images/indexmailing_list.jpg'); return true;"><img
 src="images/indexmailing_list.jpg" name="object4" alt="Mailing List"
 border="0" height="46" width="124"></a></td>
            <td colspan="2" height="46" width="630"><img
 src="images/indexpane6_10_.jpg" name="" alt="" border="0" height="46"
 width="630"></td>
          </tr>
          <tr>
            <td colspan="7" height="141" width="780"><img
 src="images/indexpane1_11_.jpg" name="" alt="" border="0" height="141"
 width="780"></td>
          </tr>
        </tbody>
      </table>
      </td>
    </tr>
    <tr>
      <td><img src="images/is_single_pixel_gif.gif" alt="" height="1"
 width="780"></td>
    </tr>
  </tbody>
</table>
<!--Adobe(R) LiveMotion(TM) DataMap1.0 DO NOT EDIT
end DataMap -->
</body>
</html>
