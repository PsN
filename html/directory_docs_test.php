<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>PsN :: Directory Structure</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <style type="text/css">
<!--
body,td,th {
font-family: Helvetica, Arial, serif;
font-size: 12px;
color: #000000;
}
body {
margin-left: 6px;
margin-top: 0px;
margin-right: 0px;
margin-bottom: 0px;
}
.style1 {
font-size: 36px;
font-weight: bold;
}
.heading1 {
font-size: 16px;
font-weight: bold;
color: #333399;
margin-left: 40px;
}
a:link {
color: #656D9C;
text-decoration: none;
}
a:visited {
color: #656D9C;
text-decoration: none;
}
a:hover {
color: #AF9D49;
text-decoration: none;
}
a:active {
color: #656D9C;
text-decoration: none;
}
-->
  </style></head>

<body>
<p>Here follows an example directory listing. This particular run is from an <strong>scm</strong> with five parameter covariate relations. The files and directories are listed in roughly the order they were created. The blue and green boxes provide an explanatory text for the directory content. </p>
<p> The names, order or existance of files and directories are not set in stone, but they should give you an idea of what a PsN tool run can produce. </p>

<table width="100%"  border="0" cellspacing="3" cellpadding="3">
  <tr>
    <td colspan="4"><tt><img src="gfx/folder.open.gif" width="27" height="22" hspace="5" align="absbottom">scm_dir1</tt></td>
  </tr>
  <tr>
    <td width="2%"><tt></tt></td>
    <td colspan="3"><tt><img src="gfx/folder.open.gif" width="27" height="22" hspace="5" align="absbottom">orig_modelfit_dir1</tt></td>
  </tr>
  <tr>
    <td><tt></tt></td>
    <td width="2%"><tt></tt></td>
    <td colspan="2"><tt><img src="gfx/folder.open.gif" width="27" height="22" hspace="5" align="absbottom">NM_run1</tt></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td width="2%">&nbsp;</td>
    <td width="94%"><dl>
      <dt><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">FCON</code></dt>
      <dt><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">FCON.0</code></dt>
      <dt><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">FCON.1</code></dt>
      <dt><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">FCON.orig</code></dt>
      <dt><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">FDATA</code></dt>
      <dt><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">FREPORT</code></dt>
      <dt><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">FSTREAM</code></dt>
      <dt><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">FSUBS</code></dt>
      <dt><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">FSUBS.f</code></dt>
      <dt><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">LINK.LNK</code></dt>
      <dt><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">PRDERR</code></dt>
      <dt><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">compilation_output.txt</code></dt>
      <dt><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">data6_multi.dta</code></dt>
      <dt><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">data6_single.dta</code></dt>
      <dt><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">done</code></dt>
      <dt><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">get_sub0.f</code></dt>
      <dt><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">msf5</code></dt>
      <dt><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">psn.lst</code></dt>
      <dt><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">psn.lst.0</code></dt>
      <dt><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">psn.lst.1</code></dt>
      <dt><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">psn.mod</code></dt>
      <dt><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">reader0.f</code></dt>
    </dl></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="2"><tt><img src="gfx/folder.open.gif" width="27" height="22" hspace="5" align="absbottom">m1</tt></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">prepared_models.log</code></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="2"><dl>
      <dt><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">model_NMrun_translation.txt</code></dt>
      <dt><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">modelfit.log</code></dt>
      <dt><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">resume.file</code></dt>
    </dl></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="3"><tt><img src="gfx/folder.open.gif" width="27" height="22" hspace="5" align="absbottom">m1</tt></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="2"><dl>
      <dt><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">BAAGE2.mod</code></dt>
      <dt><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">BAALKO2.mod</code></dt>
      <dt><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">BASEX2.mod</code></dt>
      <dt><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">BASMOK2.mod</code></dt>
      <dt><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">BAWT2.mod</code></dt>
      <dt><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">BAAGE2.lst</code></dt>
      <dt><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">BAALKO2.lst</code></dt>
      <dt><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">BASEX2.lst</code></dt>
      <dt><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">BASMOK2.lst</code></dt>
      <dt><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">BAWT2.lst</code></dt>
      <dt><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">done</code></dt>
      <dt><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">done.log</code></dt>
      <dt><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">prepared_models.log</code></dt>
    </dl></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="3"><tt><img src="gfx/folder.open.gif" width="27" height="22" hspace="5" align="absbottom">modelfit_dir1</tt></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="2"><p><tt><img src="gfx/folder.gif" width="20" height="22" hspace="5" align="absbottom">NM_run1<br>
          <img src="gfx/folder.gif" width="20" height="22" hspace="5" align="absbottom">NM_run2<br>
          <img src="gfx/folder.gif" width="20" height="22" hspace="5" align="absbottom">NM_run3<br>
          <img src="gfx/folder.gif" width="20" height="22" hspace="5" align="absbottom">NM_run4<br>
          <img src="gfx/folder.gif" width="20" height="22" hspace="5" align="absbottom">NM_run5<br>
          <img src="gfx/folder.gif" width="20" height="22" hspace="5" align="absbottom">m1<br>
          <img src="gfx/folder.gif" width="20" height="22" hspace="5" align="absbottom">m2<br>
          <img src="gfx/folder.gif" width="20" height="22" hspace="5" align="absbottom">m3<br>
          <img src="gfx/folder.gif" width="20" height="22" hspace="5" align="absbottom">m4<br>
          <img src="gfx/folder.gif" width="20" height="22" hspace="5" align="absbottom">m5          <br>
    </tt><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">model_NMrun_translation.txt<br>
    </code><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">resume.file</code></p>    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="3"><tt><img src="gfx/folder.open.gif" width="27" height="22" hspace="5" align="absbottom">scm_dir1</tt></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="2"><tt><img src="gfx/folder.gif" width="20" height="22" hspace="5" align="absbottom">m1<br>
    <img src="gfx/folder.gif" width="20" height="22" hspace="5" align="absbottom">modelfit_dir1 <br>
    <code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">modelfit1.log</code></tt></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="3"><dl>
      <dt><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">modelfit1.log</code></dt>
      <dt><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">psn_results.csv</code></dt>
      <dt><code><img src="gfx/file.gif" width="20" height="21" hspace="5" vspace="1" align="absbottom">scmlog_parallel1.txt</code></dt>
    </dl></td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
