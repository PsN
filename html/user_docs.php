<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <title>PsN :: User Documentation</title>
<!--Adobe(R) LiveMotion(TM) 1.0 Generated JavaScript. Please do not edit. -->
  <script>

<!--

function newImage(arg) {

if (document.images) {

rslt = new Image();

rslt.src = arg;

return rslt;

}

}

ImageArray = new Array;

var preloadFlag = false;

function preloadImages() {

if (document.images) {

ImageArray[ImageArray.length++] = newImage(/* OWNER('object', 'dflt') *//*URL*/'images/indexhome.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object', 'movr') *//*URL*/'images/indexhomeov.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object1', 'dflt') *//*URL*/'images/indexdocumentation.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object1', 'movr') *//*URL*/'images/indexdocumentationov.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object2', 'dflt') *//*URL*/'images/indexdownload.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object2', 'movr') *//*URL*/'images/indexdownloadov.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object3', 'dflt') *//*URL*/'images/indexbuglist.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object3', 'movr') *//*URL*/'images/indexbuglistov.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object4', 'dflt') *//*URL*/'images/indexmailing_list.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object4', 'movr') *//*URL*/'images/indexmailing_listov.jpg');
preloadFlag = true;
}
}
function changeImages() {
if (document.images && (preloadFlag == true)) {
for (var i=0; i<changeImages.arguments.length; i+=2) {
document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
}
}
}
// -->
  </script><!-- End generated JavaScript. -->
  <meta http-equiv="Content-Type"
 content="text/html; charset=ISO-8859-1">
  <style type="text/css">
<!--
body,td,th {
font-family: Helvetica, Arial, serif;
font-size: 12px;
color: #000000;
}
body {
margin-left: 6px;
margin-top: 0px;
margin-right: 0px;
margin-bottom: 0px;
background-image: url(gfx/bg.jpg);
}
.style1 {
font-size: 36px;
font-weight: bold;
}
.heading1 {
font-size: 16px;
font-weight: bold;
color: #333399;
margin-left: 40px;
}
a:link {
color: #656D9C;
text-decoration: none;
}
a:visited {
color: #656D9C;
text-decoration: none;
}
a:hover {
color: #AF9D49;
text-decoration: none;
}
a:active {
color: #656D9C;
text-decoration: none;
}
.style2 {font-family: "Courier New", Courier, mono}
-->
  </style>
  <script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) { //reloads the window if Nav4 resized
if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
  </script>
  <meta content="Pontus Pihlgren" name="author">
</head>
<body onload="preloadImages();"
 style="background-color: rgb(255, 255, 255);">
<!-- The table is not formatted nicely because some browsers cannot join images in table cells if there are any hard carriage returns in a TD. -->
<div id="Layer1"
 style="position: absolute; left: 335px; top: 42px; width: 388px; height: 43px; z-index: 1; font-size: x-small;">
<div class="style1" align="right">User
Documentation</div>
</div>
<div id="Layer2"
 style="position: absolute; left: 226px; top: 188px; width: 497px; height: 431px; z-index: 2;">
<div align="justify">
<h3 class="heading1">PsN-Utilities Introduction</h3>
<p>
PsN-Utilities is a set of command line programs that gives you the control of the main features of PsN. At present PsN-Utilities
consists of:</p>
<dl>
  <dt><b><a href="bootstrap_docs.php">the bootstrap</a></b></dt>
  <dd>	Non-parametric bootstrap using the percentile or BCa methods.</dd>
  <dt><b><a href="cdd_docs.php">cdd</a></b></dt>
  <dd>Case-deletion Diagnostics.</dd>
  <dt><b><a href="execute_docs.php">execute</a></b></dt>
  <dd>The  NONMEM runs are started using one or
multiple model files.</dd>
  <dt><b><a href="jackknife_docs.php">the jackknife</a></b></dt>
  <dd>The Jacknife if used to calculate the bias of the parameter estimates.</dd>
  <dt><b><a href="llp_docs.php">llp</a></b></dt>
  <dd>Log-likelihood Profiling around maximum-likelihood parameter estimates.</dd>
  <dt><b><a href="scm_docs.php">scm</a></b></dt>
  <dd>Stepwise Covariate Model building.</dd>
  <dt><b><a href="sumo_docs.php">sumo</a></b></dt>
  <dd>Summarizes the output
files from NONMEM.</dd>
  <dt><b><a href="data_stats_docs.php">data_stats</a></b></dt>
  <dd>Prints simple statistics for data set columns.</dd>
  <dt><b><a href="create_extra_data_model_docs.php">create_extra_data_model</a></b></dt>
  <dd>This small utility helps you create a model file with more than one data file per problem.</dd>
  <dd><b><a href="create_extra_data_model_docs.php"></a></b></dd>
  <dt><b><a href="single_valued_columns_docs.php">single_valued_columns</a></b></dt>
  <dd>Identifies data-set columns with values that do not vary within an individual. This is useful for creation of extra data files when the number of columns needed are above 20. </dd>
  </dl>
<p>You can click on any of the utilities in the list above for specific documentation.</p>
<p>A utility is run by first typing its
name on the command line and then pressing
enter, e.g.: </p>
<p>
  <b><tt>$ execute</tt></b>
</p>
<p>
  Depending on you Perl installation under Microsoft Windows the command may have to be:
</p>
<p>
  <b>
    <tt>$ C:\path to perl\perl execute</tt>
  </b>
</p>
<p>
  The above command will give you this message:
</p>
<p>
  <b>
    <tt>At least one model file must be specified. Use 'execute -h' for help.</tt>
  </b>
</p>
<p>

  To start the NONMEM execution you must give the name of at least one
model file. This is true for all the utilities except for the <tt>'scm'</tt>,
<tt>'sumo'</tt> and <tt>'data_stats'</tt></p>

<p>
  <b>
    <tt>$ execute file.mod</tt>
  </b>
</p>
<p>
  This will start the NONMEM execution using the model file <tt>"file.mod"</tt>. It will also create a directory called 
  <tt>"modelfit_dirX"</tt>, where "X" is the number of the directory starting from one. The  numbers of any additional directories will  be increased by one 
        for each time you run <tt>"execute"</tt>. This makes it possible  to do multiple runs at 
        the same time in one directory. After the run you will find the  output from NONMEM in the file named <tt>"file.lst"</tt>, and any table files specified in the model file. </p>
<p>
  All utilities accept a set of options which allow you to modify the behaviour of the utility. For example, the bootstrap creates 200 new data sets from the original data by default. However    the number of data sets can be limited to for example 50, by changing the number in the <span class="style2">-sample</span> option in the bootstrap utility.</p>
<p>
  <b><tt>$ bootstrap --samples=50 file.mod</tt></b>
</p>
<p>
  The <tt>'--samples'</tt> option is unique to the bootstrap utility. However, all options that are valid for the 
  <tt>execute</tt> tool are also valid for  the <tt>bootstrap</tt>, <tt>cdd</tt>, 
  <tt>jackknife</tt>, <tt>llp</tt> and <tt>scm</tt>. The <tt>'--threads'</tt> option is an example of a common option:
</p>
<p>
  <b><tt>$ bootstrap --samples=50 --threads=6 file.mod</tt></b>
</p>
<p>
  In this example the threads option makes the bootstrap utility run six NONMEM runs
  in parallel, starting a new run as soon as a previous has terminated.</p>
<p>

  If a PsN utility is stopped before it has finished it is possible to
  resume it from where it stopped. This is handled in PsN by the
  creation of a checkpoint after each NONMEM run. From this checkpoint
  a utility can always be resumed. To resume a utility the PsN
  directory created by the utility that stopped must be specified. In
  addition, the options used in the run that stopped must be
  defined. The options and their values are saved in the file
  <tt>"command.txt"</tt> under the directory from
  which you are resuming. If, for example, a bootstrap run in a
  directory named <tt>bootstrap_dir1</tt> stopped before it was
  finished it can easily be resumed by:

</p>
<p>
  <tt><b>$ bootstrap --samples=50 --threads=6 --directory=bootstrap_dir1</b></tt>
</p>
<p>
  In the example above the bootstrap utility will see that a directory already exists and the information in that directory will therefore be used to continue the bootstrap run.
</p>
<h3 class="heading1">More options</h3>
<p>
  If you need information on any of the PsN utilities the
  <tt>'--h'</tt> option can be used to get a list of valid options. To
  get a description of all the options use the <tt>'--help'</tt> option.
 </p>
<h3 class="heading1">PsN Directory structure</h3>
<p>
  PsN needs to create quite a few files to keep track of its executions as well as to enable resuming. During the execution the tools also create many
  NONMEM model-, data- and output files. In order to keep things manageable
  we have decided to order all files in a generic directory
  structure. You will get the output summaries from the command line tools, but it
  may still be useful to orient yourself in the directory structure. You
  can, for instance, find individual NONMEM run outputs as well as logs
  from PsN. To make it a bit easier we have created <a href="directory_docs.php">The PsN Directory Guide</a>, which is a small guide to the
  structure of the directories. <tt><tt>&nbsp;</tt></tt></p>
<p>&nbsp;</p>
</div>
</div>
<table border="0" cellpadding="0" cellspacing="0" width="780">
  <tbody>
    <tr>
      <td height="600" width="780">
      <table border="0" cellpadding="0" cellspacing="0" width="780">
        <tbody>
          <tr>
            <td colspan="7" height="201" width="780"><img
 src="images/indexpane1_1_.jpg" name="" alt="" border="0" height="201"
 width="780"></td>
          </tr>
          <tr>
            <td height="46" width="26"><img
 src="images/indexpane1_2_.jpg" name="" alt="" border="0" height="46"
 width="26"></td>
            <td height="46" width="73"><a href="index.php"
 onmouseover="changeImages(/*CMP*/ 'object', /*URL*/ 'images/indexhomeov.jpg'); return true;"
 onmouseout="changeImages(/*CMP*/ 'object', /*URL*/ 'images/indexhome.jpg'); return true;"><img
 src="images/indexhome.jpg" name="object" alt="Home" border="0"
 height="46" width="73"></a></td>
            <td height="46" width="11"><img
 src="images/indexpane3_2_.jpg" name="" alt="" border="0" height="46"
 width="11"></td>
            <td height="46" width="25"><img
 src="images/indexpane4_2_.jpg" name="" alt="" border="0" height="46"
 width="25"></td>
            <td height="46" width="15"><img
 src="images/indexpane5_2_.jpg" name="" alt="" border="0" height="46"
 width="15"></td>
            <td height="46" width="40"><img
 src="images/indexpane6_2_.jpg" name="" alt="" border="0" height="46"
 width="40"></td>
            <td height="46" width="590"><img
 src="images/indexpane7_2_.jpg" name="" alt="" border="0" height="46"
 width="590"></td>
          </tr>
          <tr>
            <td colspan="7" height="7" width="780"><img
 src="images/indexpane1_3_.jpg" name="" alt="" border="0" height="7"
 width="780"></td>
          </tr>
          <tr>
            <td height="46" width="26"><img
 src="images/indexpane1_4_.jpg" name="" alt="" border="0" height="46"
 width="26"></td>
            <td colspan="5" height="46" width="164"><a href="docs.php"
 onmouseover="changeImages(/*CMP*/ 'object1', /*URL*/ 'images/indexdocumentationov.jpg'); return true;"
 onmouseout="changeImages(/*CMP*/ 'object1', /*URL*/ 'images/indexdocumentation.jpg'); return true;"><img
 src="images/indexdocumentation.jpg" name="object1" alt="Documentation"
 border="0" height="46" width="164"></a></td>
            <td height="46" width="590"><img
 src="images/indexpane7_4_.jpg" name="" alt="" border="0" height="46"
 width="590"></td>
          </tr>
          <tr>
            <td colspan="7" height="7" width="780"><img
 src="images/indexpane1_5_.jpg" name="" alt="" border="0" height="7"
 width="780"></td>
          </tr>
          <tr>
            <td height="46" width="26"><img
 src="images/indexpane1_6_.jpg" name="" alt="" border="0" height="46"
 width="26"></td>
            <td colspan="3" height="46" width="109"><a
 href="download.php"
 onmouseover="changeImages(/*CMP*/ 'object2', /*URL*/ 'images/indexdownloadov.jpg'); return true;"
 onmouseout="changeImages(/*CMP*/ 'object2', /*URL*/ 'images/indexdownload.jpg'); return true;"><img
 src="images/indexdownload.jpg" name="object2" alt="Download" border="0"
 height="46" width="109"></a></td>
            <td colspan="3" height="46" width="645"><img
 src="images/indexpane5_6_.jpg" name="" alt="" border="0" height="46"
 width="645"></td>
          </tr>
          <tr>
            <td colspan="7" height="7" width="780"><img
 src="images/indexpane1_7_.jpg" name="" alt="" border="0" height="7"
 width="780"></td>
          </tr>
          <tr>
            <td height="46" width="26"><img
 src="images/indexpane1_8_.jpg" name="" alt="" border="0" height="46"
 width="26"></td>
            <td colspan="2" height="46" width="84"><a href="buglist.php"
 onmouseover="changeImages(/*CMP*/ 'object3', /*URL*/ 'images/indexbuglistov.jpg'); return true;"
 onmouseout="changeImages(/*CMP*/ 'object3', /*URL*/ 'images/indexbuglist.jpg'); return true;"><img
 src="images/indexbuglist.jpg" name="object3" alt="Buglist" border="0"
 height="46" width="84"></a></td>
            <td colspan="4" height="46" width="670"><img
 src="images/indexpane4_8_.jpg" name="" alt="" border="0" height="46"
 width="670"></td>
          </tr>
          <tr>
            <td colspan="7" height="7" width="780"><img
 src="images/indexpane1_9_.jpg" name="" alt="" border="0" height="7"
 width="780"></td>
          </tr>
          <tr>
            <td height="46" width="26"><img
 src="images/indexpane1_10_.jpg" name="" alt="" border="0" height="46"
 width="26"></td>
            <td colspan="4" height="46" width="124"><a href="list.php"
 onmouseover="changeImages(/*CMP*/ 'object4', /*URL*/ 'images/indexmailing_listov.jpg'); return true;"
 onmouseout="changeImages(/*CMP*/ 'object4', /*URL*/ 'images/indexmailing_list.jpg'); return true;"><img
 src="images/indexmailing_list.jpg" name="object4" alt="Mailing List"
 border="0" height="46" width="124"></a></td>
            <td colspan="2" height="46" width="630"><img
 src="images/indexpane6_10_.jpg" name="" alt="" border="0" height="46"
 width="630"></td>
          </tr>
          <tr>
            <td colspan="7" height="141" width="780"><img
 src="images/indexpane1_11_.jpg" name="" alt="" border="0" height="141"
 width="780"></td>
          </tr>
        </tbody>
      </table>
      </td>
    </tr>
    <tr>
      <td><img src="images/is_single_pixel_gif.gif" alt="" height="1"
 width="780"></td>
    </tr>
  </tbody>
</table>
<!--Adobe(R) LiveMotion(TM) DataMap1.0 DO NOT EDIT
end DataMap -->
</body>
</html>
