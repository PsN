   <h3 class="heading1">Options:</h3>

      The options are given here in their long form. Any option may be
      abbreviated to any nonconflicting prefix. The -threads option
      may be abbreviated to <span class="style2">-t</span> (or even <span class="style2">-thr</span>) but -<span class="style2">debug</span> may not be
      abbreviated to <span class="style2">-d</span> because it conflicts with <span class="style2">-debug_packages</span> and
      <span class="style2">-debug_subroutines</span>.

      The following options are valid:
      <p class="style2">-bca</p>

      Using the <span class="style2">-bca </span>option, the bootstrap
      utility will calculate the confidence intervals through the BCa
      method. The default approach however, is not to use the BCa (see
      Efron B, An introduction to the Bootstrap, 1993). The BCa is
      intended for calculation of second-order correct confidence
      intervals.  


      <p class="style2">-sample_size</p>

      The number of subjects in each bootstrap data set. The default
      value is set to the number of individuals in the original data
      set.


      <p class="style2">-samples</p>

      The number of bootstrap samples.


      <p class="style2">-stratify_on=integer|string</p>

      It may be necessary to use stratification in the resampling
      procedure. For example, if the original data consists of two
      groups of patients - say 10 patients with full pharmacokinetic
      profiles and 90 patients with sparse steady state concentration
      measurements - it may be wise to restrict the resampling
      procedure to resample within the two groups, producing bootstrap
      data sets that all contain 10 rich + 90 sparse data patients but
      with different compositions. The default is not to use
      stratification. Set <span class="style2">-stratify_on</span> to
      the column that defines the two groups. If a string is used with
      stratify_on the header in the datafile is used to map the string
      to a column number.


