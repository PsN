    Description:

      The Case Deletion Diagnostics tool is run from the command line
      with a few mandatory arguments. CDD is run as a diagnostic after
      a model is regarded finished or at least mature enough to run
      validation tool on. You need to specify the NONMEM modelfile
      with a model that have successful termination. You also have to
      specify the number or name of the datafile column on which to
      select for deletion. You do so with the case_column option.
