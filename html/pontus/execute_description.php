  <h3 class="heading1">Description:</h3>

    The execute utility is a Perl script that allows you to run multiple
    modelfiles either sequentially or in parallel. It is more or less an
    nmfe replacement.
    <br><br>
    The execute utility creates subdirectories where it puts NONMEMs
    input and output files, to make sure that parallel NONMEM runs do not
    interfere with each other. The top directory is by default named
    'modelfit_dirX' where 'X' is a number that starts at 0 and is
    increased by one each time you run the execute utility.
    <br><br>
    When the NONMEM runs are finished, the output and table files will be
    copied to the directory where execute started in which means that you
    can normaly ignore the 'modelfit_dirX' directory. If you need to
    access any special files you can find them inside the
    'modelfit_dirX'. Inside the 'modelfit_dirX' you find a few
    subdirectories named 'NM_runY'. For each model file you
    specified on the command line there will be one 'NM_runY' directory in
    which the actual NONMEM execution takes place. The order of the
    'NM_runY' directories corresponds to the order of the modelfiles given
    on the command line. The first run will take place inside 'NM_run1',
    the second in 'NM_run2' and so on.
