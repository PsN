    <h3 class="heading1">Example:</h3>
      
      <p class="style2">llp -model=run89.mod -thetas='1,2'</p>

      This will make the llp tool try to estimate the confidence
      intervals for thetas one and two of the model in run89.mod. It
      will base the first guesses on the standard error estimates from
      run89.lst.

      <p class="style2">llp -model=run89.mod -thetas='1,2' -rse_thetas='20,30'</p>

      In this example, we explicitly specify the relative standard
      errors which is necessary if we do not have an output file with
      standard error estimates.
