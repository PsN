   <h3 class="heading1">Example:</h3>

      <p class="style2">bootstrap -samples=200 run89.mod</p>

      This will run a non-parametric bootstrap of 200 samples and give
      you good estimates of the standard errors of the parameter
      estimates. You may get some estimates for the confidence
      intervals too, but they will generally not be of high quality.


      <p class="style2">bootstrap -samples=2000 -bca run89.mod</p>

      This will run a non-parametric bootstrap using the BCa technique
      (See An introduction to the bootstrap, Efron, 1993). The BCa is
      intended for caclulation of second-order correct confidence
      intervals.


      <p class="style2">bootstrap -samples=2000 -bca -stratify_on=5 run89.mod </p>

      This is the same BCa approach as above but with stratification
      on the factors of column five.
