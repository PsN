    Options:

      The options are given here in their long form. Any option may be
      abbreviated to any nonconflicting prefix. The -threads option
      may be abbreviated to -t(or even -thr) but -debug may not be
      abbreviated to -d because it conflicts with -debug_packages and
      -debug_subroutines.

      The following options are valid:
      -bins=$number 

      Sets the number of databins, or cdd datasets, to use. If the
      number of unique values, or factors, in the based_on column is
      higher than the number of bins then one or more factors will be
      deleted in each cdd dataset. Specifying $number as higher than
      the number of factors will have no effect. The bin number is
      then set to the number of factors.
      Default value = Number of unique values in the based_on column.


       -case_column=column_name|column_number


      -h | -?
      
      With -h or -? cdd will print a list of options and exit.


      -help
      
      With -help cdd will print this, longer, help message.


      -selection_method='random' or 'consecutive'

      Specifies whether the factors selected for exclusion should be
      drawn randomly or consecutively from the datafile.
      Default value = 'consecutive'


