  <h3 class="heading1">Options:</h3>

    The options are given here in their long form. Any option may be
    abbreviated to any nonconflicting prefix. The -threads option may
    be abbreviated to <span class="style2">-thr</span> but <span class="style2">-debug</span> may not be abbreviated to <span class="style2">-d</span> 
    because it conflicts with <span class="style2">-debug_packages</span> and <span class="style2">-debug_subroutines</span>.

    The following options are valid:
    <p class="style2">-config_file</p>

    A path and file name to an scm configuration file.


    <p class="style2">-do_not_drop</p>

    Since the number of columns are restricted to 20 for NONMEM it is
    necessary to minimize the number of undropped columns. The scm
    utility uses the '=DROP' syntax of NONMEM to exclude the covariate
    columns that are not used. If some covariates are used in the PK
    or PRED code in the basic model you must list them using the
    do_not_drop option to prevent them from being dropped.


    <p class="style2">-fix</p>


    <p class="style2">-global_init</p>


    <p class="style2">-gof</p>


    <p class="style2">-h | -?</p>

    With -h or -? scm will print a list of options and exit.


    <p class="style2">-help</p>

    With -help scm will print this, longer, help message.


    <p class="style2">-logfile</p>

    The name of the logfile.


    <p class="style2">-model</p>

    The name of the basic model file, without any parameter-covariate
    relations included.


    <p class="style2">-p_backward</p>

    Using the p_backward option, you can specify the p-value to use
    for the backward deletion.


    <p class="style2">-p_forward</p>

    Using the p_forward option, you can specify the p-value to use for
    the forward selection.


    <p class="style2">-p_value</p>

    Use this option to set the p_value for both forward and backward
    steps.


    <p class="style2">-search_direction</p>

    Which search task to perform: backward, forward or both is allowed.


