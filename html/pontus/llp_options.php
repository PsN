    <h3 class="heading1">Options:</h3>

      The options are given here in their long form. Any option may be
      abbreviated to any nonconflicting prefix. The <span class="style2">-threads</span> option
      may be abbreviated to <span class="style2">-t</span>(or even <span class="style2">-thr</span>) but <span class="style2">-debug</span> may not be
      abbreviated to <span class="style2">-d</span> because it conflicts with <span class="style2">-debug_packages</span> and
      <span class="style2">-debug_subroutines</span>.
    <br><br>
      The following options are valid:
      <p class="style2">-h | -?</p>

      With -h or -? llp will print a list of options and exit.


      <p class="style2">-help</p>
      
      With -help llp will print this, longer, help message.


      <p class="style2">-max_iterations=integer</p>

      This number limits the number of search iterations for each
      interval limit. If the llp has not found the upper limit for a
      parameter after max_iteration number of guesses it
      terminates. The default value is 10.


      <p class="style2">-model='filename'</p>

      The name of the model file. May be specified with or without the
      "'".


      <p class="style2">-normq=number</p>

      This number is used for calculating the first guess of the
      confidence interval limits. If the standard errors exist, the
      first guess will be
      
      <p class="style2">maximum-likelihood estimate � normq * standard error</p>

      otherwise it will be approximated with 

      <p class="style2">maximum-likelihood estimate � normq * rse_parameter/100 * maximum-likelihood estimate</p>

      where rse_parameter is rse_thetas, rse_omegas or rse_sigmas. The
      default value is 1.96 which translates a 95% confidence interval
      assuming normal distribution of the parameter estimates.


      <p class="style2">-ofv_increase</p>

      The increase in objective function value associated with the
      desired confidence interval. The default value is 3.84.


      <p class="style2">-outputfile='filename'</p>

      The name of the NONMEM output file. The default value is the
      name of the model file with a '.mod' substituted with
      '.lst'. Example: if the modelfile is run89.mod, the default name
      of the output file is run89.lst. If the name of the modelfile is
      cmd123 the default name of the output file is cmd123.lst. If the
      name of your output file does not follow this standard, you have
      to specify it with this option. 


      <p class="style2">-rse_thetas, -rse_omegas, -rse_sigmas='comma-separated list of
      relative standard errors'</p>

      The relative standard errors should be specified in percent (%).


      <p class="style2">-significant_digits=integer</p>

      Specifies the number of significant digits that is required for
      the test of the increase in objective function value.



      <p class="style2">-thetas, -omegas, -sigmas='comma-separated list of parameter
      numbers'</p>

      Specifies the parameters  for which the llp should try to assess
      confidence intervals.


