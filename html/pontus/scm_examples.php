  <h3 class="heading1">Examples:</h3>

    Execute an SCM using parameters set in the config file
    'phenobarbital.scm'.
    
       <p class="style2">$ scm -config_file=phenobarbital.scm</p>

    Execute an SCM using parameters set in the config file
    'phenobarbital.scm'. But override the retries and the seed
    parameter.

       <p class="style2">$ scm -config_file=phenobarbital.scm -retries=5 -seed=12345 phenobarbital.mod</p>
