  <h3 class="heading1">Description:</h3>

    The Stepwise Covariate Model (SCM) building tool of PsN implements
    Forward Selection and Backward Elimination of covariates to a
    model. In short, one model for each relevant parameter-covariate
    relationship is prepared and tested in a univariate manner. In the
    first step the model that gives the best fit of the data according
    to some criteria is retained and taken forward to the next
    step. In the following steps all remaining parameter-covariate
    combinations are tested until no more covariates meet the criteria
    for being included into the model. The Forward Selection can be
    followed by Backward Elimination, which proceeds as the Forward
    Selection but reversely, using stricter criteria for model
    improvement.
    <br><br>
    The Stepwise Covariate Model building procedure is run by the scm
    utility. The options to the scm utility are of three diffrent
    types, first are the common options which works like the common
    options for all tools. Then are the SCM specific command line
    options. And last are options which have a more complex structures
    and are considered to cumbersome to specify on the command
    line. These options must therefore be specified in a separate
    configuration file. It is possible to provide values for all
    options, including the common options, in the configuration file
    but it should be noted that the command line overrides the
    configuration file, if an option is specified at both
    locations.

    <!--/>Read more on our homepage: www.sf.net/psn.<-->

