    Example:
      
      cdd -model=run89.mod -case_column=10
      
      This will perform a Case Deletion Diagnostic on the model
      specified in run89.mod based on the factors in column ten. If,
      for example, column ten holds the ids of the seven centers
      included in the study, this command will create seven copies of
      the dataset, each with individuals included in one specific
      center deleted. Say that the centers are numbered 1 to 7. Then
      dataset 1 will have individuals from center 1 excluded, dataset
      2 individuals from center 2 and so on.
