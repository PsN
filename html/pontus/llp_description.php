    <h3 class="heading1">Description:</h3>

      The Log-likelihood Profiling tool can be used to assess
      confidence interval limits for parameter estimates. The
      -2*log-likelihood of hierarchical models are chi-square
      distributed. Fixing a parameter reduces the number of parameters
      of the model by one. To be able to say, for a given level of
      confidence, that there is a higher likelihood that the data has
      been produced by a system described by the full model than by one
      described by the reduced, the difference in the -2*log-likelihood
      should be at least X. For example, using a confidence level of
      95%, the difference (or X above) should be at least 3.84. The
      minimal number of arguments include a modelfile name and a
      listing of parameters, given that an output file with standard
      error estimates exist.
