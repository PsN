  <h3 class="heading1">Example:</h3>

    <p align="justify" class="style2">$ execute pheno.mod </p>

    <p align="justify">Runs one model file and accepts all default values.</p>

    <p align="justify" class="style2">$ execute -threads=2  -retries=5 phenobarbital.mod pheno_alternate.mod</p>

    <p align="justify">Runs two model files in parallel using 5 possible retries.</>p
