  <h3 class="heading1">Options:</h3>

    The options are given here in their long form. Any option may be
    abbreviated to any nonconflicting prefix. The <span class="style2">-threads</span> option may
    be abbreviated to <span class="style2">-t</span> (or even <span class="style2">-thr</span>) but <span class="style2">-debug</span> may not be
    abbreviated to <span class="style2">-d</span> because it conflicts with <span class="style2">-debug_packages</span> and
    <span class="style2">-debug_subroutines</span>.
    <br><br>
    The following options are valid:
    <p class="style2">-h | -?</p>

    With <span class="style2">-h</span> or <span class="style2">-?</span> execute.pl prints the list of options and exit.


    <p class="style2">-abort_on_fail</p>

    If the <span class="style2">-abort_on_fail</span> option is set and one of the NONMEM runs
    fails, execute will stop scheduling more runs and try to stop
    those that are currently running.


    <p class="style2">-clean</p>

    A more thorough version of <span class="style2">'-remove_temp_files'</span>. If the <span class="style2">-clean</span>
    option is set to 1, execute will remove the entire 'NM_runX'
    directory after the NONMEM run is finished. The dfault value of
    <span class="style2">-clean </span>is 0.


    <p class="style2">-compress</p>

    The execute utility will compress the contents of 'NM_runX' to the
    file 'nonmem_files.tgz' if the <span class="style2">-compress</span> option is used and if you
    have the archive and compress programs <strong>tar</strong> and <strong>gzip</strong> installed. If
    you use the <span class="style2">-remove_temp_files</span> options, temporary files will be
    removed before the compression. The <span class="style2">-compress</span> option obviously has
    no effect if you also use the <span class="style2">-clean</span> option.


    <p class="style2">-cpu_time='integer'</p>

    !! Currently only valid for Linux system !!
    This option specifies the number of minutes allocated for a
    gridjob. The default value is 120 minutes. This option is only
    valid together with the <span class="style2">-run_on_nordugrid</span> option.


    <p class="style2">-debug='integer'</p>

    The <span class="style2">-debug</span> option is mainly intended for developers who whish to
    debug PsN. By default <span class="style2">-debug</span> is set to zero but you can try
    setting it to '1' to enable warning messages. If you run in to
    problems that require support, you may have to increase this
    number to 2 or 3 and send the output to us.


    <p class="style2">-debug_package='string'</p>

    When used together with <span class="style2">-debug</span>, the <span class="style2">-debug_package</span> option makes is
    possible to choose which part of PsN you want to see debug
    messages for. Again, this option is mostly for developers.


    <p class="style2">-debug_subroutine='string'</p>
    
    Default value is: empty string

    With this option it is possible to specify, with even finer
    granularity, which part of PsN you want to see debug messages
    from. This is definitly only for developers.


    <p class="style2">-directory='string'</p>

    The directory option sets the directory in which execute will run
    NONMEM. The default directory name is 'modelfit_dirX' where X will
    be increased by one each time you run the execute utility. You do
    not have to create the directory, it will be done for you.

    If you abort execute or if your system crashes you can use the
    '<span class="style2">-directory</span>' option set to the directory of the execute run that
    crashed. Execute will then not run the modelfiles that had
    finished before the crash, thereby saving some time. Notice that
    is important that you give exactly the same options that you gave
    the first time.


    <p class="style2">-grid_batch_size='integer'</p>

    This option specifies the number of nonmem runs that will be
    grouped together into one grid job. The default number is 5. This
    option is only valid together with the '<span class="style2">-run_on_nordugrid'</span> option.


    <p class="style2">-help</p>

    With <span class="style2">-help</span> execute will print this, longer, help message.


    <p class="style2">-nice='integer'</p>

    This option only has effect on unix like operating systems. It
    sets the priority (or nice value) on a process. You can give any
    value that is legal for the "nice" command, likely it is between 0
    and 19, where 0 is the highest priority. Check "man nice" for
    details.


    <p class="style2">-nm_version='integer'</p>

    If you have more than one installation of NONMEM you can choose
    between them using the <span class="style2">-nm_version</span> option. The installations must be
    specified in the psn.conf file. The default value is 5.


    <p class="style2">-outputfile</p>

    The <span class="style2">-outputfile</span> option specifies the output file name for the
    NONMEM run. Currently This option is only valid when a single
    model is supplied to the execute utility.


    <p class="style2">-picky</p>

    The <span class="style2">-picky</span> option is only valid together with
    <span class="style2">-tweak_inits</span>. Normally PsN only tries new initial estimates if
    '<span class="style2">MINIMZATION SUCCESSFUL</span>' is not found in the NONMEM output
    file. With the <span class="style2">-picky</span> option, PsN will regard any of the following
    messages as a signal for rerunning:
<p class="style2">
    0ESTIMATE OF THETA IS NEAR THE BOUNDARY<br>
    0PARAMETER ESTIMATE IS NEAR ITS BOUNDARY<br>
    0R MATRIX ALGORITHMICALLY SINGULAR<br>
    0S MATRIX ALGORITHMICALLY SINGULAR</p>


    <p class="style2">-remove_temp_files</p>

    If the <span class="style2">-remove_temp_files</span> option is set to 1, execute will remove
    the 'FCON', 'FDATA', 'FREPORT', 'FSUBS', 'FSUBS.f', 'LINK.LNK',
    'FSTREAM', 'PRDERR' and 'nonmem' files from the 'NM_runX'
    directory. The default value is 0.


    <p class="style2">-retries='integer'</p>

    The <span class="style2">-retries</span> option tells the execute utility how many times it
    shall try to rerun a NONMEM job if it gets an error message. In
    the current version of PsN (2.2), the <span class="style2">-retries</span> option is only
    valid together with <span class="style2">-tweak_inits</span>. The default value of the
    <span class="style2">-retries</span> option is 0.


    <p class="style2">-run_on_nordugrid</p>

    !! Currently only valid for Linux system !!
    execute will run on nordugrid clusters listed in ~/.ng_cluster .
    If you do not know about Nordugrid, you can safely ignore this option.
    Read more on http://www.nordugrid.org


    <p class="style2">-sde</p>

    If you are running SDE models, you must use this option, otherwise
    PsN will destroy the formatting of the models, and the NONMEM runs
    will fail.


    <p class="style2">-seed='string'</p>

    If you use the <span class="style2">-retries='integer'</span> option, execute will use a
    random number to create new intial estimates for the model
    parameters. To make sure that the same result is produced if you
    redo the same run, you can set your own random seed with the <span class="style2">-seed</span>
    option.


    <p class="style2">-silent</p>

    The silent option turns off all output from PsN. Results and log
    files are still written to disk, but nothing i printed to the
    screen.


    <p class="style2">-threads='integer'</p>

    Use the threads option to enable parallel execution of multiple
    NONMEM runs. On a desktop computer it is recommended to set
    <span class="style2">-threads</span> to the number of CPUs in the system plus one. You can
    specify more threads, but it will probably not increase the
    performance. If you are running on a computer cluster, you should
    consult your systems administrator to find out how many threads
    you can specify. The <span class="style2">-threads</span> option will be ignored if you run on
    a grid system, since gridshave ther own scheduling algoritms. The
    default value for the <span class="style2">-threads</span> option is 1.


    <p class="style2">-tweak_inits</p>

    <!--/>If NONMEM terminates nonsuccessfully, PsN can perturb the initial
    estimates and run NONMEM again. The generation of new initial
    estimates init_i for the i:th retry are performed according to

    init_i = init_0 + rand_uniform(+-0.1*i*init_0)

    where init_0 are the initial estimates of the original run. The
    updating procedure makes sure that boundary conditions on the
    parameters are still valid. For this option to have effect, the
    -retries option must be set to number larger than zero. The
    default setting uses tweak_inits.<-->

    <?php print '<p>  If NONMEM terminates nonsuccessfully, PsN can perturb the initial estimates  and run NONMEM again. The generation of new initial estimates <img src="images/init1.gif"> for the <em>i</em>:th retry are performed according to</p><p align="center"><img src="images/perturb1.gif" width="236" height="32"></p> <p>where <img src="images/init_orig1.gif" width="29" height="28"> are the initial estimates of the original run. The updating procedure makes sure that boundary conditions on the parameters are still valid. For this option to be valid, the <span class="style2">-retries</span> option must be set to a number larger than zero. The default setting uses tweak_inits. </p>'; ?>


