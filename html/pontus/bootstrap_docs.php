<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <title>PsN :: Documentation :: Bootstrap</title>
<!--Adobe(R) LiveMotion(TM) 1.0 Generated JavaScript. Please do not edit. -->
  <script>

<!--

function newImage(arg) {

if (document.images) {

rslt = new Image();

rslt.src = arg;

return rslt;

}

}

ImageArray = new Array;

var preloadFlag = false;

function preloadImages() {

if (document.images) {

ImageArray[ImageArray.length++] = newImage(/* OWNER('object', 'dflt') *//*URL*/'images/indexhome.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object', 'movr') *//*URL*/'images/indexhomeov.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object1', 'dflt') *//*URL*/'images/indexdocumentation.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object1', 'movr') *//*URL*/'images/indexdocumentationov.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object2', 'dflt') *//*URL*/'images/indexdownload.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object2', 'movr') *//*URL*/'images/indexdownloadov.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object3', 'dflt') *//*URL*/'images/indexbuglist.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object3', 'movr') *//*URL*/'images/indexbuglistov.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object4', 'dflt') *//*URL*/'images/indexmailing_list.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object4', 'movr') *//*URL*/'images/indexmailing_listov.jpg');
preloadFlag = true;
}
}
function changeImages() {
if (document.images && (preloadFlag == true)) {
for (var i=0; i<changeImages.arguments.length; i+=2) {
document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
}
}
}
// -->
  </script><!-- End generated JavaScript. -->
  <meta http-equiv="Content-Type"
 content="text/html; charset=ISO-8859-1">
  <style type="text/css">
<!--
body,td,th {
font-family: Helvetica, Arial, serif;
font-size: 12px;
color: #000000;
}
body {
margin-left: 6px;
margin-top: 0px;
margin-right: 0px;
margin-bottom: 0px;
background-image: url(gfx/bg.jpg);
}
.style1 {
font-size: 36px;
font-weight: bold;
}
.heading1 {
font-size: 16px;
font-weight: bold;
color: #333399;
margin-left: 40px;
}
a:link {
color: #656D9C;
text-decoration: none;
}
a:visited {
color: #656D9C;
text-decoration: none;
}
a:hover {
color: #AF9D49;
text-decoration: none;
}
a:active {
color: #656D9C;
text-decoration: none;
}
.style2 {font-family: "Courier New", Courier, mono}
-->
  </style>
  <script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) { //reloads the window if Nav4 resized
if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
  </script>
  <meta content="Pontus Pihlgren" name="author">
</head>
<body onload="preloadImages();"
 style="background-color: rgb(255, 255, 255);">
<!-- The table is not formatted nicely because some browsers cannot join images in table cells if there are any hard carriage returns in a TD. -->
<div id="Layer1"
 style="position: absolute; left: 335px; top: 42px; width: 388px; height: 43px; z-index: 1; font-size: x-small;">
<div class="style1" align="right">the Bootstrap</div>
</div>
<div id="Layer2"
 style="position: absolute; left: 226px; top: 188px; width: 497px; height: 431px; z-index: 2;">
<div align="justify">
<?php
include("bootstrap_synopsis.php");
include("bootstrap_description.php");
include("bootstrap_options.php");
include("bootstrap_examples.php");
php?>
<!--
<h3 class="heading1">Synopsis</h3>
<p><tt>$                     bootstrap [ -h | -? ] [ --help ]<br>
[ --bca ]<br>
[ --sample_size=integer ]<br>
[ --samples=integer ]<br>
[ --stratify_on='string' ]<br>
[ --abort_on_fail ]<br>
[ --clean ]<br>
[ --compress ]<br>
[ --cpu_time=integer ]<br>
[ --debug=0 ]<br>
[ --debug_package='string' ]<br>
[ --debug_subroutine='string' ]<br>
[ --directory='string' ]<br>
[ --extra_data_file='string' ]<br>
[ --extra_files='string' ]<br>
[ --grid_batch_size=integer ]<br>
[ --missing_data_token='string' ]<br>
[ --nm_directory='string' ]<br>
[ --nm_version='string' ]<br>
[ --outputfile='string' ]<br>
[ --picky ]<br>
[ --remove_temp_files ]<br>
[ --results_file='string' ]<br>
[ --retries=integer ]<br>
[ --run_on_nordugrid ]<br>
[ --seed='string' ]<br>
[ --threads=integer ]<br>
[ --tweak_inits ]</tt>
  </p>
<h3 class="heading1">Description</h3>
<p>
      The bootstrap can be used to calculate bias, standard errors and
      confidence intervals. It does so by resampling with replacement
      from the data, see Efron B, An Introduction to the Bootstrap,
      Chap. & Hall, London UK, 1993. To compute standard errors for
      all parameters of a model using the non-parametric bootstrap
      implemented here, roughly 200 model fits are necessary. To asses
      95% confidence intervals approximately 2000 runs will suffice.
</p>
<h3 class="heading1">Options</h3>
<p>
      The options are given here in their long form. Any option may be
      abbreviated to any nonconflicting prefix. The <span class="style2">-threads</span> option
      may be abbreviated to <span class="style2">-t</span> (or even <span class="style2">-thr</span>) but <span class="style2">-debug</span> may not be
      abbreviated to <span class="style2">-d</span> because it conflicts with <span class="style2">-debug_packages</span> and
      <span class="style2">-debug_subroutines</span>.
</p>
<p>
      The following options are valid:
</p>
      <span class="style2">-model='filename'
      </p>
      </span>
    <p>
      The name of the model file. May be specified with or without the
      "'".
</p>
<p class="style2">
      -samples
</p>
<p>
      The number of bootstrap samples.
</p>
<p class="style2">
      -sample_size
</p>
<p>
      The number of subjects in each bootstrap data set. The default
      value is set to the number of individuals in the original data
      set.
</p>
<p class="style2">
      -bca
</p>
<p>
      Using the <span class="style2">-bca </span>option, the bootstrap utility will calculate  the confidence intervals through the BCa method. The
      default approach however, is not to use the BCa (see Efron B, An introduction to
      the Bootstrap, 1993). The BCa is intended for calculation
      of second-order correct confidence intervals.
</p>
<p class="style2">
      -stratify_on=integer
</p>
<p>
      It may be necessary to use stratification in the resampling
      procedure. For example, if the original data consists of two
      groups of patients - say 10 patients with full pharmacokinetic
      profiles and 90 patients with sparse  concentration
      measurements - it may be wise to restrict the resampling
      procedure to resample within the two groups, producing bootstrap
      data sets that all contain 10 rich + 90 sparse data patients but
      with different compositions. The default is not to use
      stratification. Set stratify_on to the column that defines the
      two groups.
</p>
<p>
      See the <a href="execute_docs.php">execute documentation</a> for a description of common options.
</p>
-->
<h3 class="heading1">Output</h3>
<p>
  The Boostrap creates a log file and a result file. Both are by default placed in the bootstrap_dirX directory
  and are called boostraplog.csv and bootstrap-results.csv.
</p>
<!--
<h3 class="heading1">Example</h3>
<p align="justify" class="style2">$ bootstrap modelfile.mod -samples=200</p>
<p>
  This will run a non-parametric bootstrap of 200 samples and give
  you good estimates of the standard errors of the parameter
  estimates. You will get some estimates for the confidence
  intervals too, but they will generally not be of high quality.</p>
<p class="style2">
  $ bootstrap modelfile.mod -samples=2000 -bca
</p>
<p> This will run a non-parametric bootstrap using the BCa technique.</p>
<p class="style2">
  $ bootstrap modelfile.mod -samples=2000 -bca -stratify_on=5
</p>
<p>
  This is the same BCa approach as above but with stratification
  on the factors of column five.</p>
-->
</div>
</div>
<table border="0" cellpadding="0" cellspacing="0" width="780">
  <tbody>
    <tr>
      <td height="600" width="780">
      <table border="0" cellpadding="0" cellspacing="0" width="780">
        <tbody>
          <tr>
            <td colspan="7" height="201" width="780"><img
 src="images/indexpane1_1_.jpg" name="" alt="" border="0" height="201"
 width="780"></td>
          </tr>
          <tr>
            <td height="46" width="26"><img
 src="images/indexpane1_2_.jpg" name="" alt="" border="0" height="46"
 width="26"></td>
            <td height="46" width="73"><a href="index.php"
 onmouseover="changeImages(/*CMP*/ 'object', /*URL*/ 'images/indexhomeov.jpg'); return true;"
 onmouseout="changeImages(/*CMP*/ 'object', /*URL*/ 'images/indexhome.jpg'); return true;"><img
 src="images/indexhome.jpg" name="object" alt="Home" border="0"
 height="46" width="73"></a></td>
            <td height="46" width="11"><img
 src="images/indexpane3_2_.jpg" name="" alt="" border="0" height="46"
 width="11"></td>
            <td height="46" width="25"><img
 src="images/indexpane4_2_.jpg" name="" alt="" border="0" height="46"
 width="25"></td>
            <td height="46" width="15"><img
 src="images/indexpane5_2_.jpg" name="" alt="" border="0" height="46"
 width="15"></td>
            <td height="46" width="40"><img
 src="images/indexpane6_2_.jpg" name="" alt="" border="0" height="46"
 width="40"></td>
            <td height="46" width="590"><img
 src="images/indexpane7_2_.jpg" name="" alt="" border="0" height="46"
 width="590"></td>
          </tr>
          <tr>
            <td colspan="7" height="7" width="780"><img
 src="images/indexpane1_3_.jpg" name="" alt="" border="0" height="7"
 width="780"></td>
          </tr>
          <tr>
            <td height="46" width="26"><img
 src="images/indexpane1_4_.jpg" name="" alt="" border="0" height="46"
 width="26"></td>
            <td colspan="5" height="46" width="164"><a href="docs.php"
 onmouseover="changeImages(/*CMP*/ 'object1', /*URL*/ 'images/indexdocumentationov.jpg'); return true;"
 onmouseout="changeImages(/*CMP*/ 'object1', /*URL*/ 'images/indexdocumentation.jpg'); return true;"><img
 src="images/indexdocumentation.jpg" name="object1" alt="Documentation"
 border="0" height="46" width="164"></a></td>
            <td height="46" width="590"><img
 src="images/indexpane7_4_.jpg" name="" alt="" border="0" height="46"
 width="590"></td>
          </tr>
          <tr>
            <td colspan="7" height="7" width="780"><img
 src="images/indexpane1_5_.jpg" name="" alt="" border="0" height="7"
 width="780"></td>
          </tr>
          <tr>
            <td height="46" width="26"><img
 src="images/indexpane1_6_.jpg" name="" alt="" border="0" height="46"
 width="26"></td>
            <td colspan="3" height="46" width="109"><a
 href="download.php"
 onmouseover="changeImages(/*CMP*/ 'object2', /*URL*/ 'images/indexdownloadov.jpg'); return true;"
 onmouseout="changeImages(/*CMP*/ 'object2', /*URL*/ 'images/indexdownload.jpg'); return true;"><img
 src="images/indexdownload.jpg" name="object2" alt="Download" border="0"
 height="46" width="109"></a></td>
            <td colspan="3" height="46" width="645"><img
 src="images/indexpane5_6_.jpg" name="" alt="" border="0" height="46"
 width="645"></td>
          </tr>
          <tr>
            <td colspan="7" height="7" width="780"><img
 src="images/indexpane1_7_.jpg" name="" alt="" border="0" height="7"
 width="780"></td>
          </tr>
          <tr>
            <td height="46" width="26"><img
 src="images/indexpane1_8_.jpg" name="" alt="" border="0" height="46"
 width="26"></td>
            <td colspan="2" height="46" width="84"><a href="buglist.php"
 onmouseover="changeImages(/*CMP*/ 'object3', /*URL*/ 'images/indexbuglistov.jpg'); return true;"
 onmouseout="changeImages(/*CMP*/ 'object3', /*URL*/ 'images/indexbuglist.jpg'); return true;"><img
 src="images/indexbuglist.jpg" name="object3" alt="Buglist" border="0"
 height="46" width="84"></a></td>
            <td colspan="4" height="46" width="670"><img
 src="images/indexpane4_8_.jpg" name="" alt="" border="0" height="46"
 width="670"></td>
          </tr>
          <tr>
            <td colspan="7" height="7" width="780"><img
 src="images/indexpane1_9_.jpg" name="" alt="" border="0" height="7"
 width="780"></td>
          </tr>
          <tr>
            <td height="46" width="26"><img
 src="images/indexpane1_10_.jpg" name="" alt="" border="0" height="46"
 width="26"></td>
            <td colspan="4" height="46" width="124"><a href="list.php"
 onmouseover="changeImages(/*CMP*/ 'object4', /*URL*/ 'images/indexmailing_listov.jpg'); return true;"
 onmouseout="changeImages(/*CMP*/ 'object4', /*URL*/ 'images/indexmailing_list.jpg'); return true;"><img
 src="images/indexmailing_list.jpg" name="object4" alt="Mailing List"
 border="0" height="46" width="124"></a></td>
            <td colspan="2" height="46" width="630"><img
 src="images/indexpane6_10_.jpg" name="" alt="" border="0" height="46"
 width="630"></td>
          </tr>
          <tr>
            <td colspan="7" height="141" width="780"><img
 src="images/indexpane1_11_.jpg" name="" alt="" border="0" height="141"
 width="780"></td>
          </tr>
        </tbody>
      </table>
      </td>
    </tr>
    <tr>
      <td><img src="images/is_single_pixel_gif.gif" alt="" height="1"
 width="780"></td>
    </tr>
  </tbody>
</table>
<!--Adobe(R) LiveMotion(TM) DataMap1.0 DO NOT EDIT
end DataMap -->
</body>
</html>
