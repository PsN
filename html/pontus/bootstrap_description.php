   <h3 class="heading1">Description:</h3>

      The Bootstrap can be used to calculate bias, standard errors and
      confidence intervals. It does so by resampling with replacement
      from the data, see Efron B, An Introduction to the Bootstrap,
      Chap. & Hall, London UK, 1993. To compute standard errors for
      all parameters of a model using the non-parametric bootstrap
      implemented here, roughly 200 model fits are necessary. To assess
      95% confidence intervals approximatly 2000 runs will suffice.
