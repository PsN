<!-- $Revision: 2.0 $ -->
<HTML>
  <HEAD>
    <TITLE><MM-List-Name> Info Page</TITLE>  <style type="text/css">
<!--
body,td,th {
font-family: Helvetica, Arial, serif;
font-size: 12px;
color: #000000;
}
body {
margin-left: 6px;
margin-top: 0px;
margin-right: 0px;
margin-bottom: 0px;
}
.heading1 {
font-size: 16px;
font-weight: bold;
color: #333399;
margin-left: 40px;
}
a:link {
color: #656D9C;
text-decoration: none;
}
a:visited {
color: #656D9C;
text-decoration: none;
}
a:hover {
color: #AF9D49;
text-decoration: none;
}
a:active {
color: #656D9C;
text-decoration: none;
}
.style2 {font-weight: bold; margin-left: 40px; font-size: 16px;}
-->
    </style>  
  </HEAD>
  <BODY BGCOLOR="#ffffff">

    <MM-Subscribe-Form-Start>
    <P>
  <TABLE COLS="1" BORDER="0" CELLSPACING="5" CELLPADDING="4">
	  <tr>
	    <TD WIDTH="100%">
	      <h3 class="heading1">About<MM-List-Name></h3>
        </TD>
	  </TR>
	    <tr>
	      <td>
		<P>To see the collection of prior postings to the list,
	      visit the <MM-Archive><MM-List-Name>
		  Archives</MM-Archive>.
	      <MM-Restricted-List-Message>	  </P>
	  </TD>
      </TR>
      <TR>
	<TD WIDTH="100%">
	  <h3 class="heading1">Using<MM-List-Name></h3>
	  </TD>
      </TR>
      <tr>
	<td>
	  To post a message to all the list members, send email to
	  <A HREF="mailto:<MM-Posting-Addr>"><MM-Posting-Addr></A>.

	  <p>You can subscribe to the list, or change your existing
	    subscription, in the sections below.
	</td>
      </tr>
      <TR>
	<TD WIDTH="100%">
	  <h3 class="heading1">Subscribing to <MM-List-Name></h3>
	</TD>
      </TR>
      <tr>
	<td>
	  <P>
	    Subscribe to <MM-List-Name> by filling out the following
	      form.
	  <MM-List-Subscription-Msg>
	  <ul>
	      <TABLE BORDER="0" CELLSPACING="4" CELLPADDING="4"
		WIDTH="100%" HEIGHT= "112">
		<TR>
		  <TD BGCOLOR="#E0E6FF" WIDTH="40%">Your email address:</TD>
		  <TD WIDTH="40%"><MM-Subscribe-Box>
	</TD>
	<TD WIDTH="20%">&nbsp;</TD></TR>
      <TR>
	<TD COLSPAN="3"><p>You must enter a
	    privacy password. This provides only mild security,
	    but should prevent others from messing with your
	    subscription.  <b>Do not use a valuable password</b> as
	    it will occasionally be emailed back to you in cleartext. 
	    <MM-Reminder></p>
	</TD>
      </TR>  
      <TR>
	<TD BGCOLOR="#E0E6FF">Pick a password:</TD>
	<TD><MM-New-Password-Box></TD>
	<TD>&nbsp;</TD></TR>
      <TR> 
	<TD BGCOLOR="#E0E6FF">Reenter password to confirm:</TD>
	<TD><MM-Confirm-Password></TD>
	<TD>&nbsp; </TD></TR>
      <tr>
	<td>Would you like to receive list mail batched in a daily
	  digest?
	  </td>
	<td><MM-Undigest-Radio-Button> No
	    <MM-Digest-Radio-Button>  Yes
	</TD>
      </tr>
      <tr>
	<td colspan="3">
	  <center><MM-Subscribe-Button></P></center>
    </TABLE>
    <MM-Form-End>
    </ul>
    </td>
    </tr>
    <TR>
      <TD WIDTH="100%">
	<a name="subscribers" class="heading1">
        <h3>Subscribers</h3></a>
      </TD>
    </TR>
    <tr>
      <TD WIDTH="100%">
	<MM-Roster-Form-Start>
	<MM-Roster-Option>
	    <MM-Form-End>
	  <p>
	<MM-Subscribe-Form-Start>
	<MM-Editing-Options>
		<MM-Form-End>
      </td>
    </tr>
  </table>
<MM-Mailman-Footer>
</BODY>
</HTML>
