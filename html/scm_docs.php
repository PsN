<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <title>PsN :: Documentation :: scm</title>
<!--Adobe(R) LiveMotion(TM) 1.0 Generated JavaScript. Please do not edit. -->
  <script>
<!--
function newImage(arg) {
if (document.images) {
rslt = new Image();
rslt.src = arg;
return rslt;
}
}
ImageArray = new Array;
var preloadFlag = false;
function preloadImages() {
if (document.images) {
ImageArray[ImageArray.length++] = newImage(/* OWNER('object', 'dflt') *//*URL*/'images/indexhome.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object', 'movr') *//*URL*/'images/indexhomeov.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object1', 'dflt') *//*URL*/'images/indexdocumentation.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object1', 'movr') *//*URL*/'images/indexdocumentationov.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object2', 'dflt') *//*URL*/'images/indexdownload.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object2', 'movr') *//*URL*/'images/indexdownloadov.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object3', 'dflt') *//*URL*/'images/indexbuglist.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object3', 'movr') *//*URL*/'images/indexbuglistov.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object4', 'dflt') *//*URL*/'images/indexmailing_list.jpg');
ImageArray[ImageArray.length++] = newImage(/* OWNER('object4', 'movr') *//*URL*/'images/indexmailing_listov.jpg');
preloadFlag = true;
}
}
function changeImages() {
if (document.images && (preloadFlag == true)) {
for (var i=0; i<changeImages.arguments.length; i+=2) {
document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
}
}
}
// -->
  </script><!-- End generated JavaScript. -->
  <meta http-equiv="Content-Type"
 content="text/html; charset=ISO-8859-1">
  <?php
     include("styles.php");
  ?>
  <script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) { //reloads the window if Nav4 resized
if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
  </script>
  <meta content="Pontus Pihlgren" name="author">
</head>
<body onLoad="preloadImages();"
 style="background-color: rgb(255, 255, 255);">
<!-- The table is not formatted nicely because some browsers cannot join images in table cells if there are any hard carriage returns in a TD. -->
<div id="Layer1"
 style="position: absolute; left: 335px; top: 42px; width: 388px; height: 43px; z-index: 1; font-size: x-small;">
<div class="style1" align="right">Stepwise
Covariate Modeling (<span style="font-family: monospace;">scm</span>)<br>
</div>
</div>
<div id="Layer2"
 style="position: absolute; left: 226px; width: 497px; z-index: 2; top: 188px; height: 1110px;">
<div align="justify">
<?php
include("scm_synopsis.php");
include("scm_description.php");
php?>
<p class="heading2">Valid states for the parameter-covariate relations</p>

<p>The SCM tool in PsN is written to be flexible in the shape of the
parameter-covariate relations. Currently there is a fixed set of
available forms of relations but there is ongoing development aiming
at full flexibility for the user with regard to relation-shape
definitions.</p>

<p>The avaialable shapes or <em>states</em> for the
parameter-covariate relations for continuous covariates are (1) not
included, (2) included as a linear relation and (3) included as a
piece-wise linear relation with two slopes. </p>

<p>Categorical covariates are either (1) not included or (2) included with an extra parameter added for each but the most common category. </p>
<p>The figures, 1, 2 and 3, are used when specifying included relations below. </p>
<?php
include("scm_options.php");
include("scm_examples.php");
php?>

<h3 align="left" class="heading1">SCM configuration file options</h3>

<span class="heading2">General considerations</span> 

<p>The syntax of the simple options is just the name of the option
followed by the equal sign and a valid value for the option. For
example:</p>

<p class="option">retries=5</p>
<p>This states that a maximum of 5 retries will be used in the executions. </p>
Some options accept Perl code:
<p><span class="option">base_criteria_values={ ofv =&gt; -9571.234}</span> </p>
Other take comma-separated lists:
<p><span class="option">continuous_covariates=AGE,WT,ALBU,BILI,AST,ALT,ALKP,POT,MAG,CA,NA,TOTP</span></p>

Some options don't accept a value on the command line. They set the
behavior of the utility simply by being specified or not. In the
configuration file these options need to be followed by 1 or a 0. A 1
indicates that it is used and a 0 that it is not used.

<p class="option">fix=1</p>
You can place a comment anywhere in the file by prefixing the line with a semicolon:
<p><span class="option">;ask_if_fail=0</span></p>
If an option is long you can break the line with a backslash, just a linebreak will not work:
<p class="option">categorical_covariates=SEX,ARR,SHD,NYHA,RACE,ALCO,SMOK,BETA,CGLY,LDIU,\<br>
  TDIU,CANT,ACE,NITR,ACOA,POTA,TWOC,TREA,TWOD,INDU,\<br>
  TUBS,ALQT,ANTP,ANTD,ANTM,ANTH,ANTB,SULP,ANT1,CLIA,\<br>
  CLIB,CLIC,CL3,CIME,METF,FENM,PROC,HEP1,HEP2,HEP3 </p>

<p class="heading2">Simple options</p>

The simple options are the SCM specific command line options and the
command line options common to all PsN Utilties.

<p class="heading2">List options</p>

List options are options that take enumerations of values like the
<span class="option">categorical_covariates</span> option:

<p class="option">categorical_covariates=SEX,ARR,SHD,NYHA,RAC<p>

The list options valid in the configuration file are:

<p class="option">categorical_covariates</p>

The <span class="option">categorical_covariates</span> list option is
neccessary for the scm to know which type of NONMEM code to generate
for the parameter covariate relation.

<p class="option">continuous_covariates</p>

The <span class="option">categorical_covariates</span> list option is
neccessary for the scm to know which type of NONMEM code to generate
for the parameter covariate relation.

<p class="option">do_not_drop</p>

If the dataset used with the scm is to wide, scm will drop the data
columns that correponds to covariate which is not used in a particular
step. However, if you have used that covariate in code that is not
affected by the scm, NONMEM will produce an error. Therefore it is
neccessary for you to specify which those covariates are, using the
<span class="option">do_not_drop</span> list option.

<p class="option">extra_files</p>

If you need some extraordinary file to be copied into the NONMEM
execution directory, specify them with the <span
class="option">extra_files</span> list option.

<p class="heading2">Code options</p>

Code options are almost like a Simple option. With the difference that
the right side is evaluted by the perl interpretor. If you don't
understand the difference, it is probably not important. However, it
is available for a reason. The SCM have hooks for custom goodness of
fit functions. Those functions may need some special input data. And
for flexibility, we allow that data to be just about anything, the
side effect is that it becomes to complex for the configuration file
structure. The solution is to allow options that are small Perl
programs. Those options we refer to as code options.

<p class="option">base_criteria_values</p>

The <span class="option">base_criteria_values</span> overrides default
criterias for the goodness of fit function used. The default goodness
of fit function takes its base_criteria_value to be the ofv of the
initial model. If you would like to change the base ofv to 400 you
would write:

<p class="option">base_criteria_values= { ofv => 400 } }</p>
<p class="heading2">Composite options </p>

<p>The composite options can not be specified on the command line. An
exception is the <span class="option">extra_data_files</span> option
which is valid for all utilities. A composite option section starts
with an option header followed by a set of lines with a left side, an
equal sign and a right side.  </p>
<!--
hash:
	$this -> {'ofv_backward'} = defined $parm{'ofv_backward'} ? $parm{'ofv_backward'} : {} unless defined $this -> {'ofv_backward'};
	$this -> {'ofv_change'} = defined $parm{'ofv_change'} ? $parm{'ofv_change'} : {} unless defined $this -> {'ofv_change'};
	$this -> {'ofv_forward'} = defined $parm{'ofv_forward'} ? $parm{'ofv_forward'} : {} unless defined $this -> {'ofv_forward'};
-->
<dl>

  <dt><b>extra_data_files</b></dt>

  <dd>The <span class="option">-extra_data_files</span> option is used
      to specify files with extra data. As NONMEM only accepts 20
      columns, it is sometimes necessary to split data files. The left
      side of each line holds the extra data file name. The right side
      is a comma-separated list of the column headers.
    <p><span class="option">[extra_data_files]<br>
      sub.dta=ID,GRP,AGE,SEX,ARR,SHD,NYHA,LVEF,WT,HT,RACE,ALCO,SMOK,BETA</span><br>
    </p>
  </dd>

  <dt><b>test_relations</b></dt>

  <dd><span class="option">The -test_relations </span>option is used
      to specify what parameter-covariate relations the scm utility
      will test. The left side of each row is a parameter name and the
      right side is a comma separated list of covariates.

    <p class="option">[test_relations]<br>
      CL=AGE,SEX,ARR,SHD,NYHA,WT,RACE,ALCO,SMOK,BETA,\<br> 
	  ALBU,BILI,AST,ALT,ALKP,POT,MAG,CA,NA,TOTP, \<br> 
	  ANT1,CLIA,CLIB,CLIC,CL3,CIME,METF,PROC<br><br>
	  V=SEX,WT,ALBU,TOTP,LDIU,TDIU,ACOA,SULP<br></p>
  </dd>

  <dt><b>included_relations</b></dt>

  <dd>If you want to start the search with some relations included you
      can add them to the <span
      class="option">included_relations</span>.  The left side of each
      row holds the name of a parameter and the right side a list of
      covariates. For each covariate you can specify a relation
      state. The general syntax is <span class="option">COV-X</span>
      where COV is you covariate and X is the state.  If the state is
      omitted it will be set to 2.

    <p><span class="option">[included_relations]<br>
      CL=TDIU-2,CLIA-2,SEX-2<br>
      V=WT,ACOA-2,SEX-3,TOTP</span><br>
    </p>
  </dd>

  <dt><b>valid_states</b></dt>

  <dd>You can specify what states you want the SCM to test. By default
      it tests three states. 1 - not included, 2 - linear, 3 -
      piecewise linear and 4 - exponential parameterization.  The
      valid states are specified for separately continuous and
      categorical covariates.

    <p class="option">[valid_states]<br>
          categorical=1,2<br>
	  continuous=1,2,3</p>
  </dd>

  <dt><b>code</b></dt>

  <dd>If you are not satisfied with the state specific NONMEM code
      generated by the scm, you can overide it using the code
      option. It lets you specify code for specific parameters,
      specific paramater - covariate relations and even specific
      states. Each line under the <span class="option">[code]</span>
      header defines code for one parameter - covariate - state
      relation. Like this:
      
      <p class="option">[code]<br>
	  CL:WT-1=THETA(1)</p>

      This will change the default code for state 1 of the CL - WT
      relation. You can also use wildcards to redefine the code for
      multiple relations:

      <p class="option">[code]<br>
	  CL:*-1=THETA(1)</p>

      In this example you will redefine the code for all covaritates
      in a relation with CL.

      <p class="option">[code]<br>
	  *:*-1=THETA(1)</p>
	   
      This example will redefine the code for all relations in state
      1. Notice that you can not use a wildcard for the state.
      <p>
      Generally it is useful to include the parameter and/or the
      covariate in the code. So to make it possible when using
      wildcards you can use the generic variables PARM and COV which
      will be replaced with the parameter and covariate in question
      for the current relation.
	  
  </dd>
  <dt><b>lower_bounds, upper_bounds and inits</b></dt>

  <dd>The inits and bounds option lets you set initial estimates for
      thetas introduced in the code and their corresponding bounds.  A
      hockeystick relation introduces two new thetas, so to set the
      thetas value and bounds for the CL - WT relation you would
      write:

      <p class="option">[lower_bounds]<br>
         CL:WT-2=0.1, 0.05</p>
  
      <p class="option">[upper_bounds]<br>
         CL:WT-2=0.3, 0.08</p>

      <p class="option">[inits]<br>
         CL:WT-2=0.15,0.07</p>
	 
      This would correspond to the following lines in a NONMEM modelfile:

	<p class="option">THETA(1) = 0.1,0.15,0.4<br>
	  THETA(2) = 0.05,0.07,0.08</p>

      Just like in the code option you can use wildcards. Notice that
      if you specify to few or to many values, those you exclude will
      be replaced by defaults and the extraneous will be ignored.

   </dd>
  

</dl>

<p></p>
<h3 class="heading1">Output</h3>
<h4>Result file</h4>
Currently, the logfile is the best place to look for the results from the scm utility.
<h4>&nbsp;</h4>
<p></p>
</div>
<p align="justify">&nbsp;</p>
</div>
<table border="0" cellpadding="0" cellspacing="0" width="780">
  <tbody>
    <tr>
      <td height="600" width="780">
      <table border="0" cellpadding="0" cellspacing="0" width="780">
        <tbody>
          <tr>
            <td colspan="7" height="201" width="780"><img
 src="images/indexpane1_1_.jpg" name="" alt="" border="0" height="201"
 width="780"></td>
          </tr>
          <tr>
            <td height="46" width="26"><img
 src="images/indexpane1_2_.jpg" name="" alt="" border="0" height="46"
 width="26"></td>
            <td height="46" width="73"><a href="index.php"
 onmouseover="changeImages(/*CMP*/ 'object', /*URL*/ 'images/indexhomeov.jpg'); return true;"
 onmouseout="changeImages(/*CMP*/ 'object', /*URL*/ 'images/indexhome.jpg'); return true;"><img
 src="images/indexhome.jpg" name="object" alt="Home" border="0"
 height="46" width="73"></a></td>
            <td height="46" width="11"><img
 src="images/indexpane3_2_.jpg" name="" alt="" border="0" height="46"
 width="11"></td>
            <td height="46" width="25"><img
 src="images/indexpane4_2_.jpg" name="" alt="" border="0" height="46"
 width="25"></td>
            <td height="46" width="15"><img
 src="images/indexpane5_2_.jpg" name="" alt="" border="0" height="46"
 width="15"></td>
            <td height="46" width="40"><img
 src="images/indexpane6_2_.jpg" name="" alt="" border="0" height="46"
 width="40"></td>
            <td height="46" width="590"><img
 src="images/indexpane7_2_.jpg" name="" alt="" border="0" height="46"
 width="590"></td>
          </tr>
          <tr>
            <td colspan="7" height="7" width="780"><img
 src="images/indexpane1_3_.jpg" name="" alt="" border="0" height="7"
 width="780"></td>
          </tr>
          <tr>
            <td height="46" width="26"><img
 src="images/indexpane1_4_.jpg" name="" alt="" border="0" height="46"
 width="26"></td>
            <td colspan="5" height="46" width="164"><a href="docs.php"
 onmouseover="changeImages(/*CMP*/ 'object1', /*URL*/ 'images/indexdocumentationov.jpg'); return true;"
 onmouseout="changeImages(/*CMP*/ 'object1', /*URL*/ 'images/indexdocumentation.jpg'); return true;"><img
 src="images/indexdocumentation.jpg" name="object1" alt="Documentation"
 border="0" height="46" width="164"></a></td>
            <td height="46" width="590"><img
 src="images/indexpane7_4_.jpg" name="" alt="" border="0" height="46"
 width="590"></td>
          </tr>
          <tr>
            <td colspan="7" height="7" width="780"><img
 src="images/indexpane1_5_.jpg" name="" alt="" border="0" height="7"
 width="780"></td>
          </tr>
          <tr>
            <td height="46" width="26"><img
 src="images/indexpane1_6_.jpg" name="" alt="" border="0" height="46"
 width="26"></td>
            <td colspan="3" height="46" width="109"><a
 href="download.php"
 onmouseover="changeImages(/*CMP*/ 'object2', /*URL*/ 'images/indexdownloadov.jpg'); return true;"
 onmouseout="changeImages(/*CMP*/ 'object2', /*URL*/ 'images/indexdownload.jpg'); return true;"><img
 src="images/indexdownload.jpg" name="object2" alt="Download" border="0"
 height="46" width="109"></a></td>
            <td colspan="3" height="46" width="645"><img
 src="images/indexpane5_6_.jpg" name="" alt="" border="0" height="46"
 width="645"></td>
          </tr>
          <tr>
            <td colspan="7" height="7" width="780"><img
 src="images/indexpane1_7_.jpg" name="" alt="" border="0" height="7"
 width="780"></td>
          </tr>
          <tr>
            <td height="46" width="26"><img
 src="images/indexpane1_8_.jpg" name="" alt="" border="0" height="46"
 width="26"></td>
            <td colspan="2" height="46" width="84"><a href="buglist.php"
 onmouseover="changeImages(/*CMP*/ 'object3', /*URL*/ 'images/indexbuglistov.jpg'); return true;"
 onmouseout="changeImages(/*CMP*/ 'object3', /*URL*/ 'images/indexbuglist.jpg'); return true;"><img
 src="images/indexbuglist.jpg" name="object3" alt="Buglist" border="0"
 height="46" width="84"></a></td>
            <td colspan="4" height="46" width="670"><img
 src="images/indexpane4_8_.jpg" name="" alt="" border="0" height="46"
 width="670"></td>
          </tr>
          <tr>
            <td colspan="7" height="7" width="780"><img
 src="images/indexpane1_9_.jpg" name="" alt="" border="0" height="7"
 width="780"></td>
          </tr>
          <tr>
            <td height="46" width="26"><img
 src="images/indexpane1_10_.jpg" name="" alt="" border="0" height="46"
 width="26"></td>
            <td colspan="4" height="46" width="124"><a href="list.php"
 onmouseover="changeImages(/*CMP*/ 'object4', /*URL*/ 'images/indexmailing_listov.jpg'); return true;"
 onmouseout="changeImages(/*CMP*/ 'object4', /*URL*/ 'images/indexmailing_list.jpg'); return true;"><img
 src="images/indexmailing_list.jpg" name="object4" alt="Mailing List"
 border="0" height="46" width="124"></a></td>
            <td colspan="2" height="46" width="630"><img
 src="images/indexpane6_10_.jpg" name="" alt="" border="0" height="46"
 width="630"></td>
          </tr>
          <tr>
            <td colspan="7" height="141" width="780"><img
 src="images/indexpane1_11_.jpg" name="" alt="" border="0" height="141"
 width="780"></td>
          </tr>
        </tbody>
      </table>
      </td>
    </tr>
    <tr>
      <td><img src="images/is_single_pixel_gif.gif" alt="" height="1"
 width="780"></td>
    </tr>
  </tbody>
</table>
<!--Adobe(R) LiveMotion(TM) DataMap1.0 DO NOT EDIT
end DataMap -->
</body>
</html>
