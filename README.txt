This version of PsN is tested with perl-5.8.8
Depending on your distribution you may have to install the following
perl packages:

Math::Random
Storable::Store

All available from CPAN ( www.cpan.org )

  Changelog
----------------------------------------------

Changes from 2.2.4 to 2.2.5

Features

 - Monte Carslo Simulations(mcs) tool has been renamed to Stochastic
   Simulation and Estimation(sse), and has a bunch of new
   features. More information can be found in the SSE user guide
   distributed with PsN.

 - Numerical Predictive Check(npc) and Visual Predictive Check(vpc)
   has been added to the toolbox.

 - Much improved results handling. Intermediate results are printed
   each time a NONMEM run has finished. A positive side effect is much
   reduced memory usage over all and in particular when running with
   many threads

 - Sumo tool is more verbose when parsing errors occur.
 
 - General improvement in handling of parsing errors.

 - Functionality added to parsing of data that enables memory
   optimization in npc/vpc.

 - max_runtime option added for unix type systems. This feature will
   be expanded to more systems in the future.

 - By default PsN will restart crashed runs. This feature can now be
   disabled with "--no-handle_crashes"

 - By default execute will copy the tables and lst file to the
   directory where the original model file is placed. By specifying
   "--extra_output=file1,file2" PsN will also copy any file listed. To
   avoid files being overwritten the option
   "--prepend_model_file_name" can be used to add the name of the
   model file (without extension) to the front of the output files.

 - Added support for G95. A more generic way of configuring compilers
   is on the way.

 - Experimental msfi/msfo functionality.

   If the model allready has the MSFO= option or the $MSFI record set
   and one of the files exists, then that file will be used as $MSFI
   file in the first run. If both files exists, then the MSFO file
   will be used as MSFI file. The file will be renamed psn_msfo-0. 
   
   The MSFO option will be set to psn_msfo. If the NONMEM run crashes
   PsN will use the new msfo files as $MSFI file. This functionality
   is considered experimental and must be enabled with "--handle_msfo".

   Notice that any msfo files existing when doing restart will be
   ignored.
   

Bugs

 - Multiline ETABAR and PVAL in output parsed correctly.
	
 - Modelfit did not copy extra subroutine files in some cases.

 - <new script> Add IGNORE=@ to estimation models unless it
   exists. Necessary for ignoring headers in table output.

 - Memory leak in output parser plugged. Output files from simulations 
   could use vast amounts of memory.

 - Bootstrap options "skip_minimization_terminated" can now be disabled
   by typing "-no-skip-minimization_terminated" (or -noskip-...)

 - cdd xv can no be disabled with -no-xv or -noxv

 - Memory leak in modelfit due to intermediate "raw_results.csv" in
   NM_runX directory fixed by reimplementing the way results handling
   is done.

 - A initial estimate of form: $OMEGA BLOCK SAME would not be
   recognized by PsN model file parser. This is now handled
   correctly. In 2.2.X it can be avoided by typing BLOCK(X) SAME,
   where X is the size of the block.

Changes from 2.2.3 to 2.2.4

Features

 - "--compute_cwres" option renamed to simply "--cwres"

 - Simulated data suitable for creating mirror plots can be generated
   by using a new options called --mirror_plots. The option takes a
   value which defines the number of simulations that will be
   geneated.

 - A new option, --iofv, enables the computation of individual
   objective function values. These values are printed to a table file
   called iotab*, where * is the same number as is defined for any
   sdtab or patab tables. These names correspond to the normal Xpose
   table file format. If no sdtab or patab table is found in the
   NONMEM control stream, the iotab file is given number 1.

 - msfo resumes. Basic support for automatically rerunning a model
   that has the MSFO=msffile option on $ESTIMATION. If the option is
   set, the "msffile" exists and the option --msfo_resume is given,
   PsN will remove initial estimates, and add the $MSFI record. This
   is very untested and if you have OMEGA blocks, the model will run
   but PsN will be unable to read the output file properly.

 - "no_remote_compile" and "no_remote_execution" options has been
   removed, as they were quite seldomly used and added a lot of
   complexity to parallization code.

 - Added --prepend_model_file_name option which adds the model file
   name to output files to prevent them from being overwritten.

 - A new option called "crash_restarts" which controls the number of
   times PsN will restart a crashed run. (Simply put, run is
   classified as crashed if the output file looks like it is cut off)

 - PsN will now automatically run R-scripts generated for the
   bootstrap, cdd and llp if R is installed and configured.

 - When the CONT data item is used - either defined by the user or
   automatically through the -wrap_data option - the tables get a
   weird format where the real table content is printed on CONT=0 rows
   and nonsense data printed on CONT=1 rows. A new option called
   -unwrap_table_files reformats the table files and removes the CONT
   column as well as all CONT=1 rows.

 - update_inits script that will put final estimates from a NONMEM
   output file and put into a given model file. This is quite
   experimental. PsN will reformat the model file quite lot and
   comments in the file might get lost. Your model file will be copied
   with the addition of a ".org" extension, so you wont loose your
   model.

Bug Fixes

 - SGE queue and resource options were ignored. Fixed thanks to Jeroen
   Elassaiss and Stefan Verhoeven.

 - LSF Monitor fixed to work with the new serialized structure of PsN,
   again thanks to Jeroen and Stefan.

 - CWRES table file number now defaults to 1 if there is no patab or
   sdtab to take the number from.

 - Fixed run bugs under Windows 2000.

 - Better support for G77 under windows.

 - Parser for output files improved

Changes from 2.2.2 to 2.2.3

Features

 - There has been a lot of requests for a raw_results file for the
   runs in the NM_run catalogs, i.e. a summary of the runs with
   perturbed initial estimates. This is now implemented. One
   raw_results file is now created in each NM_run* catalog.

 - The R-scripts for graphical visualization of bootstrap and cdds
   have been updated. The format for the graphs is now pdf instead of
   postscript. The bootstrap script can now be adjusted to exclude
   bootstrap samples that contain copies of a given set of
   individuals.

 - When the wrap_data option is used, the secondary columns, i.e. the
   data columns that are kept on rows with CONT=1, are now available
   in $ERROR.

Bug Fixes

 - The calculation of CWRES was previously done on the _last_ run of a
   set of retries, not on the _best_ run. This has now been corrected.

 - The restarting of crashed runs was broken. This is now fixed. 

 - The asynchronuous job scheduling broke the handling of seed numbers
   for random sequences (e.g. perturbation of initial estimates). This
   is now fixed. The present version is however not compatible with
   older versions and old runs should therefore not be resumed using
   this version.

 - wrap_data should now work as it should. The functionality was
   broken as a consequence of the move to the asynchronuous job
   handling.

 - Labels for omegas were not handled correctly for SAME blocks. Among
   other things, this had the effect that the updating of initial
   estimates from the original model to the method created models
   (e.g. bootstrap samples) was corrupted. This is now fixed.

 - The transformation of the estimate of the omega matrix from
   variance and covariances to standard deviations and correlations
   was rewritten to address problems with a combination of BLOCK(X)
   ans SAME where X was larger than 1.

 - The selection of a 'best' model fit after a series of perturbations
   of the initial estimates will be changed. In 2.2.2, the lowest OFV
   among fits of the perturbed models was collected across all models
   (still only comparing identical models with perturbed initial
   estimates). In 2.2.3 the lowest OFV is collected in three
   categories of model fits: those that minimized succesfully, those
   that did not minimize successfully but returned an OFV plus an
   estimate of the number of significant digits and finally failed
   runs that managed to return an OFV but not much more. This will
   give PsN a more fine-grained control over the results. For example,
   it would be desirable to be able to return the run with the lowest
   OFV within the selection of runs with a successful minimization
   even though the lowest OFV of all perturbed runs was returned from
   a run that terminated.

 - The sigmas were not parsed correctly when tables were printed to
   the output file. This is not fixed.

 - Terminated covariance steps were sometimes still being parsed (and
   failed). Fixed.

 - Setting the clean level to 3 did not remove the
   NM_run-directories. This is now fixed.

 - The parsing of options for the initial values of omegas and sigmas
   sometimes wrongly recognized letters behind a semi-colon (i.e. a
   comment) as keywords for a block (FIX, SAME, etc). This is now
   corrected.

 - If you "fix" more than one omega or sigma in a block PsN would put
   a FIX after each value, which NONMEM doesn't allow. Now PsN will
   only put one FIX inside a block. However, if a any part of the
   block has been fixed, its likely so that PsN can not(!) unfix
   it.(so, one bug fixed, another introduced)

 - If you an initial estimate was added with "_init_attr", the option
   "add_if_absent" was true and the last initial estimate was "SAME"
   PsN would try to add the estimate after "SAME" which is wrong, now
   PsN adds a new record.

 - Fixed a parsing error of the lst file. If simulation without a
   SIGMA was performed, the parser failed to read OMEGA initial
   estiamtes.
 
 - The internal method "indexes" would not properly handle "SAME" in
   initial estimates. The result was that "update_inits" would skip
   initial values, and get them out of order. Fixed.


Changes from 2.2.2-beta4 to 2.2.2

Features

 - CWRES can now be calculated automatically if you have R and Xpose
   installed. Remember to add a definition of IPRED in your NONMEM model.

 - This version handles non-PsN standard (or absent) suffixes for
   model files better than earlier versions.

Bug Fixes

 - MSFO files are now copied back to the same directory of the model
   file.

 - MSFI files are now automatically copied to the PsN NONMEM run
   directories.

 - Output files from runs where the $MSFI record was used were not
   parsed correctly. This is now fixed.

 - Output files from runs where no $OMEGA _or_ $SIGMA were defined
   were not parsed correctly.

 - The header and the results from problematic runs, i.e. failed
   covariance step, warnings etc, were not handled correctly when the
   raw_results files were compiled. This is now fixed.

 - In 2.2.1, we added a check for a '1' to find the end of the omega
   and sigma estimate areas. this was a mistake since problems with
   more than 13 omegas or sigmas get a '1'-line inserted before the
   14:th estimate (for some reason). This is now removed.

Changes from 2.2.2-beta3 to 2.2.2-beta4

Features

 - No new features

Bug Fixes

 - Adaptive number of threads is now working in the new asynchronuous
   job handling

 - The CWRES computation had some fixes

 - $INFN is now printed before $MODEL

 - $OMEGA is now printed befor $PK

 - PRIOR is now handled in the output module. It is still a bit
   problematic to parse PRIOR output files correctly. Use with
   causion.

 - The initial estimates of large omega and sigma matrices is now
   better handled

 - The order of parsing events in the output module is now working
   better.

 - The handling of retries (or the results of retries) is now working
   as it should.

Changes from 2.2.2-beta2 to 2.2.2-beta3

Features

 - CWRES: Conditional weighted residuals. The necessary verbatim code
   for the computation of CWRES are now automatically added if the
   option -compute_cwres is used.

 - PRIOR: The use of priors in NONMEM VI is now supported in the
   computer intensive methods

 - The behavior of the parallel execution has been revised, moving
   from a synchronuous to an asynchronuous solution. Previously, the
   memory foot-print of a parallel execution of say 100 NONMEM jobs
   was huge (counted in several GB) and did not scale well. Now, it
   will very seldom exceed 100 MB and more or less keep that size,
   regardless of the number of parallel jobs.

Bug Fixes

 - PsN couldn't parse IGNORE=(ITEM=VALUE) options in $DATA. This is
   (again) fixed. It was previously corrected in the development
   branch of PsN but never transferred to the stable branch.

 - Fixed SAME block correlation computation.

Changes from 2.2.2-beta1 to 2.2.2-beta2

Features

 - The parsing of the NONMEM output files has been revised and a
   strict check is now performed after each section has been parsed. A
   parsing error is raised if any non-compliant structures are
   found. By doing this we get a better idea of which runs that really
   finishes and which runs that are pre-maturely terminated
   (e.g. through a operating system error). Remember that this is a
   beta release and that there could still be valid text structures
   that are not yet recognised correctly by PsN.

 - Support for United Devices grip MP has been added and is available using 
   "-run_on_ud" command line option.

Bug Fixes

 - the options directory and backwards_dir can now be specified in the
   scm configuration file

 - It is now possible to skip the definition of either continuous and
   categorical covariates in the scm configuration file

 - The exponential relation is now added correctly in the
   scm. Previously it was incorrectly added as (for weight on volume)
      TVV = THETA(1)*(1+EXP(THETA(2)*(WGT-median(WGT))))
   Now it is added as
      TVV = THETA(1)*EXP(THETA(2)*(WGT-median(WGT)))

 - The output parse now computes correlations for omegas and sigmas correctly
   when you have SAME blocks.

Changes from 2.2.1 to 2.2.2-beta1

Features

 - The number of significant digits per parameter after a terminated
   minimization are now parsed.

Bug Fixes

 - The sample size option of the bootstrap did not work correctly with
   stratified resampling. This is now fixed.


Changes from 2.2.0 to 2.2.1

Features

 - Added support for the Intel Fortran compiler (mostly for those
   using the new Intel Macs).

 - We have added two new model fit quality terms: 'terminated' and
   'really_bad' to the previous 'normal' and 'picky'.

   picky      - Minimization and covariance steps successful, no 
                parameters near boundaries and no warnings from the
                covariance step.

   normal     - Minimization successful

   terminated - Estimates of the objective function value and the
                number of significant digits are available.

   really_bad - An estimate of the OFV is available (but usually not
                much more...).

   Given that more than one run from a run has been performed (with
   different initial estimates but otherwise identical) the following
   selection procedure is done:

   The lowest OFV of all tries is collected. Then, all runs within
   5 units of the lowest OFV is tried according to

   1. Of the runs passing the picky criteria, choose the run with the
      highest number of significant digits.

   2. If no run has passed the criteria above: of the runs passing the
      normal criteria, choose the one with the highest number of
      sginificant digits.

   3. If no run has passed the criteria above: of the runs passing the
      terminated criteria, choose the one with the highest number of
      sginificant digits.

   4. If still no run has passed the criteria above: of all runs that
      at least have an estimate of the OFV, choose the run with the
      lowest OFV.

   5. As a last resort, choose the first run.

   Comments: This is of course a very pragmatic approach but it is
   needed to make the automatic procedures of the PsN tools to
   work. We cannot force the user to manually scrutinise the results
   of every single run of for example a bootstrap. The user has the
   opportunity to do so if he or she wishes. If you do not feel
   comfortable with this selection procedure or if you have comments
   on how to improve it, please contact me (Lars Lindbom) at
   lars.lindbom (at) farmbio.uu.se.

Bugfixes

 - Data files with spaces around commas used for columns separation
   could be problematic to parse. This is now fixed.

 - There was a serious error in the seed handling in version
   2.2.0. Under the following circumstances, the seed number was
   ignored and a default seed number used: If the output file from the
   original run was not present in the same directory as the original
   model file when a PsN tool was started. This affected all PsN
   tools, except the execute tool. This is now fixed.

 - Up to 20 omegas can now be parsed in the output file. This is still
   a limitation and it will be addressed in later releases.

 - On fast distributed systems with a shared file system, the massive
   parallelisation of the PsN tools could cause synchronization
   problems (files could sometimes not be found, read or written). The
   root of this problem is somewhat outside of the scope of PsN but we
   have tried to find a workaround for this for the LSF grid system.

Changes from 2.1.10 to 2.2.0 ( released for PsN course )

Features

 - PsN Output module use a proper Matrix library for handling some of
   the matrices of the outputfile.

 - The bootstrap can now be resumed with a lower number of
   samples. Meaning that if you are in a hurry and want to stop the
   bootstrap and get results after 1000 out of 2000 runs, you can. At
   a later time you can then resume the 2000 runs.

 - Nonmem warnings are now printed if "--verbose" is givven.

 - Automatic incrementation of directory names are a bit smarter (no
   more "directory modelfit_dirX is in the way" messages).

 - Clean is much more stringent and intelligent and now manages to
   remove entire tool directories if desired.

 - option list printed from "-h|-?" only shows utility specific options.

 - Default values configurable on a per utility basis.

 - Online documentation improved
   
 - Parallel run of NONMEM is not aborted as before if a outputfile is
   not correctly parsed. It is just ignored, as if it had not
   minimized successfully. (actually it is the output parser that
   doesn't die, but the effect is the same)

 - Parallel run is a bit nicer to the system when compiling many
   NONMEMS. By adding a configurable delay between starts.

 - Matlab and R scripts for various plots from cdd and bootstrap
   results are distributed with PsN.

 - Vast improvments in results presentations from all tools.

 - Improvements in rerun algoritm with new "min_retries" option.

 - Output from running utilties are much nicer.

 - new "-quick_summary" prints minimization message and ofv value for
   each finished run. As does "-verbose" option which also prints
   "R:X" for each retrie (X is the retrie number).

 - new "-summary" options prints diagnostics about the tool run when
   it is finished. For "execute" the "-summary" option will have the
   same function as the "sumo" utiltiy as well as some diagnostics.

 - PsN now recognizes that control files may require fortran files and
   copies them to the run directory. No need to add fotran files to
   the --extra_files option any more.

 - Added --extra_output which allows you to copy more than the output 
   file from the run directory.

Bugs

 - PsN couldn't parse IGNORE=(ITEM=VALUE) options in $DATA. Fixed :)

 - If star notation was used in the scm config file section [inits],
   [upper_bounds] or [lower_bounds] there was a risk of defining
   relations that was not specified in [test_relations]. Which in turn
   would cause a lot of diffrent errors. Fixed :)

 - Using low_cutoff and hi_cutoff in data::sd resulted in PsN trying to use
   the new compact data strings as the old arrays. fixed.
 
 - Removed unecessary regular expression in output file parser.

 - Not really a bug. But i tried to add "FIX" as a record option to a theta
   records. Then when "store_init" was called on the "FIX" option PsN crashed
   because the option was not a "theta_option". So i added a check for
   "store_init" abilties... if FIX would become a record option in the future.

 - Member accessor "datafile" replaced with "datafiles" in model. Affected
   only the bootstrap which have been updated.

 - An assumptions about outputfiles crashed sumo occationally. Some more
   rigorous contentchecking has been added and fixed the problem.

Internal changes
 - Cleanup of dubious utility options.
 - Option help sorted alphabetically.
 - "flush", "target" removed from model.
 - "remove_temp_files" removed from modelfit.
 - Changed the Makefile considerably, much simpler now.
 - added "column_to_array" function in data module.

Changes from 2.1.9 to 2.1.10

Features
- The 'picky' option of all utilities now triggers on 
        COVARIANCE STEP ABORTED         and
        PROGRAM TERMINATED BY OBJ

Changes from 2.1.8 to 2.1.9

Features
 - Nordugrid support reintroduced (but will probably not be developed further)
 - Sumo now supports multiple problems and subproblems.
 - Sumo has new option "precision" which control the number of digits
   to present estimates with.
 - An extra 'short' logfile is output from the scm with the most important
   information.

Bugs
 - Bootstrap option "stratify_on" doesn't work with columns defined in 
   extra data files given with "extra_data" option. Not Fixed.
 - Resumes of scm's with extra_data_files is now possible. Still some 
   problems with scm's where search_direction=both.
 - LSF options was not communicated properly between tools and subtools
   so submission of bootstraps, llp, cdd and scm did not work. Fixed.
 - Default values for LSF can now be configured in psn.conf.
 - Print order of modelfiles places $MIX before $PRED. Fixed.
 - extra_files option was broken. Fixed.
 
Internal Changes
 - Tool will always fork subtools, this will save memory in the scm (and
   other recursive tools).
 - Common options need only be specified once. In one of three categories:
   model_options, tool_options, script_options.
 - models can now be created with "reference" objects. This solved the
   scm resume problem.
 - Added function to model that cleans extra data code.

Changes from 2.1.7 to 2.1.8

Features
 - Rudimentary support for execution on Platform LSF grid systems is now
   included. The following extra attributes are available to all tools
   for controlling the execution:

       run_on_lsf       - Use LSF
       lsf_queue        - Use this queue on the grid
       lsf_options      - General options for the grid queuing system.
       lsf_project_name - Optional. May be used by the grid system
			  administrator to register control project
			  cpu usage.
       lsf_job_name     - Optional. May be used by a grid system
                          administrator to control multiple runs on a
                          grid system.

 - All tools now have a raw_results file holding the diagnotics and
   parameter estimates for each NONMEM run.
 - All tools now handles model files containing one $PROBLEM and no
   SUBPROBLEMS only.
 - The Bootstrap and CDD routines now create separate files holding
   information about which individuals that were included/excluded
   in/from each created data set.
 - The Bootstrap results file now has warnings section to better
   display which runs that were used for result calculations and which
   were not
 - PsN now tries to read psn.conf from the users home directory. (Will
   require some extra documentation).
 - Optimized modelfit thread operation by removing Storable::Store.
 - The clean options has four levels of operation.

	0 - No cleaning at all.
	1 - Clean more, but make sure resumes work (this is default).
	2 - Clean more, but make sure summarize work.
	3 - Clean everything, keeping only results and logs.

 - Some attributes and accessors of the PsN-Core class 'output' have
   had their names changed to make it more clear what their purposes
   are:
        termination           is now called     minimization_successful
	termination_message   is now called     minimization_message
	covstep_termination   is now called	covariance_step_successful

   these attributes are new:

        covariance_step_warnings (boolean; 0 or 1)
        estimate_near_boundary (boolean; 0 or 1)

   The attribute which was previously called termination could have the
   values 0, 0.5 and 1 for 'minimization terminates', 'minimization
   successful but warnings or errors were printed from the covariance
   step' and finally 'minimization successful'. The new attribute
   'minimization_successful' can only take the values 0 and 1 and the
   new attribute covariance_step_warnings now takes the value 1 when
   covariance step warnings were found. Checks are also perfomed to
   test whether the estimates are close to any boundary.
 - Html documents is now generated from command line help text.
   which ensures that they are consistent and up to date.
 - Personal configuration file now available under Unix like OS:s

Bugs
 - Options specific to modelfiles were ignored in some Utilties.
 - Gradients now output in windows. Just like with nmfe.
 - Bootstrap now consumes considerably less memory.
 - Removed unused/obsolete "resume" function from modelfit.
 - Sorted method entries in modelfit diagram.
 - Fixed bugs in parsing of OFV and eigenvalues.

Changes from 2.1.6 to 2.1.7

Bugs
 - "clean" and "compress" options now work under Windows.
 - SCM now handles crashed runs appropriately.
 - The ofv_forward, ofv_backward, p_forward and p_backward options of
   scm now works as they should.

Features
 - SCM improvements. The final model is now available in the
   first-level scm directory of each step.
 - The Utilties -help option is improved. You can now give a list of
   options to -help for specific documentation of those options, like
   so:
   
   $ scm -help do_not_drop

 - Minor changes in how the [nm_version] section in psn.conf i
   handled. Look in the psn.conf file that is distributed with PsN for
   more information.
 - SDE modelfiles are now supported. (see the -sde_records option)
 - The update_inits method of the model class now accepts an output
   file name as input.
 - Some attributes and accessors of the PsN-Core class 'output' have
   had their names changed to make it more clear what their purposes
   are:
        finalgrad          is now called     final_gradient
        parampath          is now called     parameter_path
        gradientpath       is now called     gradient_path
        covterm            is now called     covstep_termination 
 - The Utilties now saves the command line that started it in a file
   "command.txt" under the corresponding directory.
 - Added a "silent" option to the Utilties that quenches all output.
 - "nice" option added to utilties that allows you to set priority in
   unix.
 - Rudimentary support for NorduGrid reinstated.
 - Installer script rewritten. It now supports Perl installations in
   nonstandard directories. Discrepancies between *nix and MSWin32 is
   smaller.

Internal changes
 - The code that is autogenerated with "dia2code" is optimized a
   bit and saves a hash "__valid_parameters" in each object, which
   allows the constructors to be used as a generic (shallow) copy.

Misc
 - continuous spelled correctly throughout SCM.


Changes from 2.1.5 to 2.1.6

Bugs
 - Stratify_on bug in bootstrap method fixed.
 - Data files in directories different from model file are now
   possible.
 - Path name handling reimplemented. Relative paths are now handled
   better.
 - Models that generate table files would make PsN complain about
   missing files.
 - Its now possible to specify multiple output names as a
   comma-separated list to execute.
 - The scm can now go backwards.
 - Handling of table files now responds to the
   "ignore_missing_output_files" flag.
 - nmtran error messages was lost in Windows 2K. Execution of nmtran
   is more plattform independent now.  It uses a temporary file, but
   it is never bigger than the nmtran messages.
 - Fixed a bug that limited choice of compiler under unix.
 - Typo in scm utility fixed. (included_relations in backwards step).
 
Features
 - Added support for "$NONPARAMETRIC" record in model files.
 - A few improved error messages related to parsing of model files.
 - The SCM config file now features sections for parameter bounds and
   user-defined code

Internal changes
 - psn.conf settings are now available as global variables. psn.conf
   is only parsed once.
