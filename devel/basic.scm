[options]
model=pheno.mod
;ofv_change=25
search_direction=forward
fix=0
logfile=scmlog.txt
p_value=0.05
picky=1
abort_on_fail=0
nm_version=6

[covariates]
continuous=WGT
categoricals=APGR

[test relations]
CL=WGT,APGR
V=WGT,APGR
